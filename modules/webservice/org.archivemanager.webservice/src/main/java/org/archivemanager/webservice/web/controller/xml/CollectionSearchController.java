package org.archivemanager.webservice.web.controller.xml;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.archivemanager.Collection;
import org.archivemanager.Series;
import org.archivemanager.search.CollectionSearchRequest;
import org.heed.openapps.QName;
import org.heed.openapps.dictionary.ClassificationModel;
import org.heed.openapps.dictionary.RepositoryModel;
import org.heed.openapps.SystemModel;
import org.heed.openapps.data.Sort;
import org.heed.openapps.entity.Association;
import org.heed.openapps.entity.AssociationSorter;
import org.heed.openapps.entity.BaseEntityQuery;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityQuery;
import org.heed.openapps.entity.EntityResultSet;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.entity.Property;
import org.heed.openapps.entity.ExportProcessor;
import org.heed.openapps.entity.data.FormatInstructions;
import org.heed.opensearch.search.Clause;
import org.heed.opensearch.search.Parameter;
import org.heed.opensearch.search.SearchResponse;
import org.heed.opensearch.search.SearchService;
import org.heed.openapps.util.NumberUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class CollectionSearchController extends SearchControllerSupport {
	@Autowired private EntityService entityService;
	@Autowired protected SearchService searchService;
	//@Autowired protected SearchAnalyticsService analyticsService;
	
	private QName[] archiveQNames = new QName[]{RepositoryModel.COLLECTION,ClassificationModel.SUBJECT,ClassificationModel.PERSON};
	
	
	@RequestMapping(value="/{code}", method = RequestMethod.GET)
	public ModelAndView collection(HttpServletRequest req, HttpServletResponse res, @PathVariable("code") String code) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		parms.put("code", code);
		EntityQuery query = new BaseEntityQuery(RepositoryModel.COLLECTION, "code", code, "name_e", true);
		EntityResultSet collectionResults = entityService.search(query);
		parms.put("code", code);
		if(collectionResults != null && collectionResults.getResultSize() > 0) {
			parms.put("title", collectionResults.getResult(0).getValue(SystemModel.NAME));
			List<Collection> collections = new ArrayList<Collection>();
			AssociationSorter associationSorter = new AssociationSorter(new Sort(Sort.STRING, SystemModel.NAME.getLocalName(), true), entityService);
			for(Entity collectionEntity : collectionResults.getResults()) {
				//entityService.hydrate(collectionEntity);
				Collection collection = new Collection(collectionEntity);
				List<Association> seriesAssociations = collectionEntity.getSourceAssociations(RepositoryModel.CATEGORIES);
				Collections.sort(seriesAssociations, associationSorter);
				for(Association seriesAssoc : seriesAssociations) {
					Entity target = entityService.getEntity(seriesAssoc.getTarget());
					Series series = new Series(target);
					String description = null;
					for(Association assoc : target.getSourceAssociations(SystemModel.NOTES)) {
						Entity note = entityService.getEntity(assoc.getTarget());
						if(note.getPropertyValue(SystemModel.NOTE_TYPE).equals("Scope and Contents note")) {
							description = note.getPropertyValue(SystemModel.NOTE_CONTENT);
						}
					}
					series.setDescription(description);
					collection.getSeries().add(series);
				}
				collections.add(collection);
			}
			parms.put("collections", collections);
		} 
		return new ModelAndView("services/series", parms);
	}
	@RequestMapping(value="/{code}/search", method = RequestMethod.GET)
	public ModelAndView search(HttpServletRequest req, HttpServletResponse res, @PathVariable("code") String code, @RequestParam("query") String query) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		parms.put("code", code);
		//String fmt = req.getRequestURL().substring(req.getRequestURL().lastIndexOf("."));
		if(query == null || query.equals("")) query = "all results";
		if(query != null && !query.equals("")) {
			//analyticsService.search(req.getQueryString());
			String sizeStr = req.getParameter("size");  //page size for result set (default 10)
			String pageStr = req.getParameter("page");
			String startStr = req.getParameter("start");
			String sort = req.getParameter("sort");
			
			if(sort == null || sort.length() == 0) sort = "localName";
			int page = (NumberUtility.isInteger(pageStr)) ? Integer.valueOf(pageStr) : 1;
			int size = (NumberUtility.isInteger(sizeStr)) ? Integer.valueOf(sizeStr) : 10;
			long start = (startStr != null && NumberUtility.isLong(startStr)) ? Long.valueOf(startStr) : 0;
			
			CollectionSearchRequest searchRequest = null;			
			if(code.equals("archive")) {
				searchRequest = new CollectionSearchRequest(archiveQNames, query);
				searchRequest.setAttributes(true);
				searchRequest.setPage(page);
				searchRequest.setPageSize(size);
				
				searchRequest.getSorts().add(new Sort(Sort.STRING, sort, false));				
				SearchResponse result = searchService.search(searchRequest);
				
				String output = toXml(searchRequest, result, false, false);
				return response("", output, res.getWriter());
			} else {
				searchRequest = getSearchRequest(RepositoryModel.ITEM, start, query, page, size, sort, true);
				
				EntityQuery collectionQuery = new BaseEntityQuery(RepositoryModel.COLLECTION, "code", code, "name", true);
				EntityResultSet collectionResults = entityService.search(collectionQuery);
				if(collectionResults.getResults().size() == 1) {
					Entity collection = collectionResults.getResults().get(0);
					searchRequest.addParameter("path", String.valueOf(collection.getId()));
				} else {
					Clause clause = new Clause();
					clause.setOperator(Clause.OPERATOR_OR);
					for(Entity collection : collectionResults.getResults()) {
						clause.addParamater(new Parameter("path", String.valueOf(collection.getId())));
						searchRequest.setCollection(collection.getId());
					}
					searchRequest.addClause(clause);
				}
				searchRequest.setPrintSources(true);				
				SearchResponse result = searchService.search(searchRequest);
				
				String output = toXml(searchRequest, result, true, false);
				return response("", output, res.getWriter());
			}			
						
		}
		parms.put("status", "0");
		return new ModelAndView("results_xml", parms);
	}
	@RequestMapping(value="/{code}/search/collections", method = RequestMethod.GET)
	public ModelAndView searchCollections(HttpServletRequest req, HttpServletResponse res, @PathVariable("code") String code, @RequestParam("query") String query) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		parms.put("code", code);
		
		String sizeStr = req.getParameter("size");  //page size for result set (default 10)
		String pageStr = req.getParameter("page");
		String startStr = req.getParameter("start");
		String sort = req.getParameter("sort");
		int page = (NumberUtility.isInteger(pageStr)) ? Integer.valueOf(pageStr) : 1;
		int size = (NumberUtility.isInteger(sizeStr)) ? Integer.valueOf(sizeStr) : 10;
		long start = (startStr != null && NumberUtility.isLong(startStr)) ? Long.valueOf(startStr) : 0;
			
		CollectionSearchRequest searchRequest = getSearchRequest(RepositoryModel.COLLECTION, start, query, page, size, sort, false);
		if(query != null && query.equals("all results"))
			searchRequest.setQuery("code:"+code);
		else if(query != null && query.length() > 0) 
			searchRequest.setQuery("code:"+code+" and name_e:"+query);
		else searchRequest.setQuery("code:"+code);
		SearchResponse result = searchService.search(searchRequest);
		
		String output = toXml(searchRequest, result, false, false);
			
		parms.put("status", "0");
		return response("", output, res.getWriter());
	}
	@RequestMapping(value="/search/detail/{id}.{mime}", method = RequestMethod.GET)
	public ModelAndView component(HttpServletRequest req, HttpServletResponse res, @PathVariable("id") long id, @PathVariable("mime") String mime,
			@RequestParam(required=false,defaultValue="false") boolean sources, @RequestParam(required=false,defaultValue="false") boolean targets) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		Entity entity = entityService.getEntity(Long.valueOf(id));
		if(mime.equals("xml")) {
			ExportProcessor processor = entityService.getExportProcessor(RepositoryModel.ITEM.toString());
			if(processor == null) processor = entityService.getExportProcessor("default");
			try {
				parms.put("data", processor.export(new FormatInstructions(true, false, sources, targets), entity));
				parms.put("path", getComponentPath(entity));
			} catch(Exception e) {
				e.printStackTrace();
			}
			parms.put("status", "0");
			return new ModelAndView("results_xml", parms);
		} else {
			parms.put("entity", entity);
			parms.put("status", "0");
			return new ModelAndView("results_html", parms);
		}
	}
	@RequestMapping(value="/collections/{repositoryId}", method = RequestMethod.GET)
	public ModelAndView collectionsByRepository(HttpServletRequest req, HttpServletResponse res,@PathVariable("repositoryId") Long repositoryId) throws Exception {
		//Map<String, Object> parms = new HashMap<String, Object>();
		StringBuffer buff = new StringBuffer("<data>");
		if(repositoryId != null && !repositoryId.equals(0L)) {
			Entity repository = entityService.getEntity(repositoryId);
			for(Association assoc : repository.getSourceAssociations(new QName("openapps.org_repository_1.0","collections"))) {
				Entity collection = entityService.getEntity(assoc.getTarget());
				Property name = collection.getProperty(new QName("openapps.org_system_1.0","name"));
				buff.append("<node id='"+collection.getId()+"'><title><![CDATA["+name.toString()+"]]></title></node>");
			}
		}
		res.getWriter().append(buff.toString()+"</data>");
		return null;
	}
	
	protected String getComponentPath(Entity comp) throws Exception {
		StringBuffer buff = new StringBuffer("<path>");
		List<Entity> path = new ArrayList<Entity>();
		Association parent = comp.getTargetAssociation(RepositoryModel.CATEGORIES);
		while(parent != null) {
			Entity p = entityService.getEntity(parent.getSource());
			path.add(p);
			parent = p.getTargetAssociation(RepositoryModel.CATEGORIES);
		}
		Collections.reverse(path);
		if(path.size() > 0) {
			Long parentId = null;
			for(int i=0; i < path.size(); i++) {
				Entity node = path.get(i);
				String title = node.getPropertyValue(SystemModel.NAME);
				String dateExpression = node.getPropertyValue(RepositoryModel.DATE_EXPRESSION);
				if(!node.getQName().equals(RepositoryModel.REPOSITORY)) {
					if(title != null  && !title.equals("")) buff.append("<node id='"+node.getId()+"' type='"+node.getQName().getLocalName()+"' parent='"+parentId+"'><title><![CDATA["+title+"]]></title></node>");
					else if(dateExpression != null) buff.append("<node id='"+node.getId()+"' type='"+node.getQName().getLocalName()+"' parent='"+parentId+"'><title>"+dateExpression+"</title></node>");
					parentId = node.getId();
				}
			}		
		}
		return buff.append("</path>").toString();
	}
	protected CollectionSearchRequest getSearchRequest(QName qname, long start, String query, int page, int size, String sort, boolean attributes) {
		CollectionSearchRequest sQuery = (qname == null) ? new CollectionSearchRequest(archiveQNames, query) : new CollectionSearchRequest(qname, query);
		sQuery.setAttributes(attributes);
		sQuery.setPage(page);
		sQuery.setPageSize(size);
		if(sort != null) {
			Sort lSort = sort.endsWith("_") ? new Sort(Sort.LONG, sort, true) : new Sort(Sort.STRING, sort, false);
			sQuery.addSort(lSort);
		} else {
			Sort lSort = new Sort(Sort.STRING, "name_e", true);
			sQuery.addSort(lSort);
		}
		if(start != 0) {
			sQuery.addParameter("path", String.valueOf(start));
		}		
		return sQuery;
	}
	
}
