package org.archivemanager.webservice.web.controller.xml;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.heed.openapps.QName;
import org.heed.openapps.data.TreeNode;
import org.heed.openapps.dictionary.DataDictionaryService;
import org.heed.openapps.dictionary.Model;
import org.heed.openapps.dictionary.ModelField;
import org.heed.openapps.entity.BaseEntityQuery;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityResultSet;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.entity.Property;
import org.heed.openapps.entity.ValidationResult;
import org.heed.openapps.scheduling.Job;
import org.heed.openapps.theme.ThemeVariables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class EntityManagerController {
	private EntityService entityService;
	private DataDictionaryService dictionaryService;
	
		
	@Autowired
	public EntityManagerController(EntityService entityService, DataDictionaryService dictionaryService) {
		this.entityService = entityService;
		this.dictionaryService = dictionaryService;
	}
	
	@RequestMapping(value="/manager", method = RequestMethod.GET)
    public ModelAndView manager(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		ThemeVariables vars = new ThemeVariables("administration");
		parms.put("theme", vars);
    	return new ModelAndView("home").addAllObjects(parms);
	}
	@RequestMapping(value="/model/fetch", method = RequestMethod.GET)
	public ModelAndView main(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String parent = request.getParameter("parent");
		List<TreeNode> data = new ArrayList<TreeNode>();
		if(parent == null || parent.equals("null") || parent.equals("0")) {
			data.add(getTreeNode("0", "0", "null", "models", "", "Models", true));
			List<String> namespaces = new ArrayList<String>();
			List<Model> list = dictionaryService.getModels();
			for(Model node : list) {
				if(!namespaces.contains(node.getQName().getNamespace())) {
					data.add(getTreeNode(node.getQName().getNamespace(), node.getQName().getNamespace(), "0", "namespace", "", node.getQName().getNamespace(), true));
					namespaces.add(node.getQName().getNamespace());
				}
				boolean isFolder = node.getFields().size() > 0 ? true : false;
				String parentId = node.getParent() != null ? String.valueOf(node.getParent()) : node.getQName().getNamespace();
				int height = calculateHeight(node);
				data.add(getTreeNode(String.valueOf(node.getId()), String.valueOf(height), parentId, "model", node.getQName().getNamespace(), node.getQName().getLocalName(), isFolder));
			}
		} 
		//response.getWriter().write(xstream.toXML(new RestResponse(0, "", data)));
		return null;
	}
	@RequestMapping(value="/entity/indexAll", method = RequestMethod.GET)
	public ModelAndView index(HttpServletRequest request, HttpServletResponse response) throws Exception {
		List<Model> models = dictionaryService.getModels();
		for(Model model : models) {
			Job job = entityService.indexEntities(model.getQName());
			//System.out.println("indexed "+count+" nodes of type "+model.getQName().toString());
		}
		return null;
	}
	@RequestMapping(value="/entity/index", method = RequestMethod.GET)
	public ModelAndView indexQName(HttpServletRequest request, HttpServletResponse response, @RequestParam("qname") String qname) throws Exception {
		StringBuffer buff = new StringBuffer();
		QName q = QName.createQualifiedName(qname);
		Job job = entityService.indexEntities(q);
		buff.append("<node id='"+job.getId()+"' uid='"+job.getUid()+"' lastMessage='"+job.getLastMessage()+"' isRunning='"+!job.isComplete()+"' />");
		return response(job.getLastMessage(), buff.toString(), response.getWriter());
	}
	@RequestMapping(value="/entity/index/{id}", method = RequestMethod.GET)
	public ModelAndView index(HttpServletRequest request, HttpServletResponse response, @PathVariable("id") Long id) throws Exception {
		entityService.indexEntity(id);
		//response.getWriter().write(xstream.toXML(new RestResponse(0, "", null)));
		return null;
	}
	@RequestMapping(value="/clean", method = RequestMethod.GET)
	public ModelAndView cleanQName(HttpServletRequest request, HttpServletResponse response, @RequestParam("qname") String qname) throws Exception {
		StringBuffer buff = new StringBuffer();
		QName q = QName.createQualifiedName(qname);
				
		Job job = entityService.cleanEntities(q);
		buff.append("<node id='"+job.getId()+"' uid='"+job.getUid()+"' lastMessage='"+job.getLastMessage()+"' isRunning='"+!job.isComplete()+"' />");
		
		return response(job.getLastMessage(), buff.toString(), response.getWriter());
	}
	@RequestMapping(value="/entities/fetch/{id}", method = RequestMethod.GET)
	public ModelAndView fetchEntities(HttpServletRequest request, HttpServletResponse response, @PathVariable("id") Long id) throws Exception {
		Entity entity = entityService.getEntity(id);
		EntityResultSet entities = entityService.search(new BaseEntityQuery(entity.getQName()));
		//response.getWriter().write(xstream.toXML(new RestResponse(0, "", entities.getResults())));
		return null;
	}
	@RequestMapping(value="/fetch/{id}", method = RequestMethod.GET)
	public ModelAndView fetchModel(HttpServletRequest request, HttpServletResponse response, @PathVariable("id") Long id) throws Exception {
		Entity entity = entityService.getEntity(id);	
		//response.getWriter().write(xstream.toXML(new RestResponse(0, "", entity)));
		return null;
	}
	@RequestMapping(value="/form", method = RequestMethod.GET)
	public ModelAndView form(HttpServletRequest request, HttpServletResponse response, @RequestParam("namespace") String namespace, @RequestParam("localname") String localname) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
    	QName qname = QName.createQualifiedName(namespace, localname);
		Model model = dictionaryService.getModel(qname);
    	
		parms.put("formHeight", calculateHeight(model));
    	parms.put("model", model);
    	return new ModelAndView("entityForm").addAllObjects(parms);
	}
	@RequestMapping(value="/add", method = RequestMethod.POST)
	public ModelAndView addEntity(HttpServletRequest request, HttpServletResponse response,@RequestParam("namespace") String namespace, @RequestParam("localname") String localname) throws Exception {
		QName qname = QName.createQualifiedName(namespace, localname);
		Model model = dictionaryService.getModel(qname);
		Entity entity = new Entity(qname);
		for(ModelField field : model.getFields()) {
			String value = request.getParameter(field.getQName().getLocalName());
			if(value != null && !value.equals("null")) {
				if(field.getFormat() == ModelField.FORMAT_PASSWORD) {
					//value = entityService.encodePassword(value);
				} 
				entity.getProperties().add(new Property(field.getQName(), value));
			}
		}
		ValidationResult result = entityService.validate(entity);
		if(result.isValid()) {
			entityService.addEntity(entity);
			//response.getWriter().write(xstream.toXML(new RestResponse(0, "", entity)));
		} else {
			//response.getWriter().write(xstream.toXML(new RestResponse(0, "", result)));			
		}
		return null;
	}
	/*
	@RequestMapping(value="/association/add", method = RequestMethod.POST)
	public ModelAndView associationAdd(HttpServletRequest request, HttpServletResponse response, 
			@RequestParam("start") long start, @RequestParam("end") long end) throws Exception {
		Association assoc = new Association(null, start, end);
		entityService.createAssociation(assoc, true);		
		response.getWriter().write(xstream.toXML(new RestResponse(0, "", assoc)));
		return null;
	}
	*/	
	@RequestMapping(value="/association/types", method = RequestMethod.GET)
	public ModelAndView associationTypes(HttpServletRequest request, HttpServletResponse response, @RequestParam("namespace") String namespace, @RequestParam("localname") String localname) throws Exception {
		//Map<String, Object> parms = new HashMap<String, Object>();
    	QName qname = QName.createQualifiedName(namespace, localname);
		Model model = dictionaryService.getModel(qname);
    	
		//response.getWriter().write(xstream.toXML(new RestResponse(0, "", model.getRelations())));
    	return null;
	}
	@RequestMapping(value="/association/search", method = RequestMethod.POST)
	public ModelAndView associationSearch(HttpServletRequest request, HttpServletResponse response, 
			@RequestParam("qname") String qname, @RequestParam("query") String query) throws Exception {
		//Map<String, Object> parms = new HashMap<String, Object>();
		Model rel = dictionaryService.getModel(QName.createQualifiedName(qname));
		if(rel != null) {
			EntityResultSet entities = entityService.search(new BaseEntityQuery(rel.getQName(), query+"*", null, true));    	
			//response.getWriter().write(xstream.toXML(new RestResponse(0, "", entities.getResults())));
		}
    	return null;
	}
	@RequestMapping(value="/about", method = RequestMethod.GET)
	public ModelAndView repositories(HttpServletRequest req, HttpServletResponse res) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		//ThemeVariables vars = new ThemeVariables("default");
		//parms.put("theme", vars);
		
		return new ModelAndView("about", parms);
	}
	
	protected int calculateHeight(Model model) {
		int height = 100;
		for(ModelField field : model.getFields()) {
			if(field.getType() == ModelField.LONGTEXT) height += 60;
			else height += 35;
		}
		return height;
	}
	protected TreeNode getTreeNode(String id, String uid, Object parent, String type, String namespace, String title, Boolean isFolder) {
		TreeNode node = new TreeNode();
		node.setId(id);
		node.setUid(uid);
		node.setParent(parent.toString());
		node.setType(type);
		node.setNamespace(namespace);
		node.setTitle(title);
		node.setIsFolder(isFolder.toString());
		if(type != null) node.setIcon("/theme/images/tree_icons/"+type+".png");
		return node;
	}
	protected ModelAndView response(String message, String data, PrintWriter out) {
		out.write("<response><status>0</status><totalRows>0</totalRows><startRow>0</startRow><endRow>0</endRow><message>"+message+"</message><data>"+data+"</data></response>");
		return null;
	}
}
