package org.archivemanager.webservice.web;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.heed.openapps.entity.EntityService;
import org.heed.openapps.util.FileUtility;
import org.heed.openapps.util.HttpUtility;
import org.heed.openapps.util.IOUtility;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;


public class FileStreamingServlet extends HttpServlet {
	private static final long serialVersionUID = -347146205972086979L;
	protected EntityService entityService;	
	
	
	public void init() throws ServletException {
		ApplicationContext springContext = WebApplicationContextUtils.getRequiredWebApplicationContext(this.getServletContext());
		entityService = (EntityService)springContext.getBean("entityService");
	}
	
	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String lastPath = HttpUtility.getLastPathName(req);
		if(lastPath.equals("nullpx")) {
			InputStream in = getServletConfig().getServletContext().getResourceAsStream("nullpx");
			if(in != null) IOUtility.pipe(in, res.getOutputStream());	
		} else {
			String id = FileUtility.getExtension(lastPath).length() > 0 ? lastPath.substring(0, lastPath.lastIndexOf(".")) : lastPath;
			String mimetype = FileUtility.getMimetype(lastPath);
			String mime = FileUtility.getExtension(lastPath);
			try {
				res.addHeader("pragma", "no-store,no-cache");
				res.addHeader("cache-control", "no-cache, no-store,must-revalidate");
				res.addHeader("expires", "-1");
				res.setContentType(mimetype);
				FileInputStream in = new FileInputStream("data/reports/"+id+"."+mime);
				if(in != null) {
					res.setContentType(FileUtility.getMimetype(mimetype));
					IOUtility.pipe(in, res.getOutputStream());					
				}
			} catch(Exception e) {
				throw new IOException(e);
			}
		}
	}
		
}
