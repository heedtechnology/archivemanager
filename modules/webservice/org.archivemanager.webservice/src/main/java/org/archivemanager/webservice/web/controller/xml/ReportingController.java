package org.archivemanager.webservice.web.controller.xml;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.archivemanager.webservice.job.FindingAidGenerationJob;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.reporting.ReportingService;
import org.heed.openapps.scheduling.SchedulingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ReportingController {
	@Autowired protected EntityService entityService;
	@Autowired protected SchedulingService scheduleService;
	@Autowired protected ReportingService reportingService;
	
	
	@RequestMapping(value="/reporting/finding_aid/generate", method = RequestMethod.GET)
	public ModelAndView exportCollection(HttpServletRequest request, HttpServletResponse response, 
			@RequestParam("id") Long id, @RequestParam("format") String format, @RequestParam("title") String title,
			@RequestParam("schedule") boolean schedule) throws Exception {
		StringWriter buff = new StringWriter();
		
		FindingAidGenerationJob job = new FindingAidGenerationJob(id, format, title);
		job.setEntityService(entityService);
		job.setReportingService(reportingService);
		if(schedule) scheduleService.run(job);
		else job.execute();
		response.setHeader( "Pragma", "no-cache" );
		response.setHeader( "Cache-Control", "no-cache" );
		response.setDateHeader( "Expires", 0 );
		
		if(job != null && job.getId() != null) buff.append("<node localName='job' id='"+job.getId()+"' uid='"+job.getUid()+"' />");
		else return response(-1, "problem running collection report generation job", buff.toString(), response.getWriter());
		return response(0, "collection report generation job running", buff.toString(), response.getWriter());
	}
	
	protected ModelAndView response(int status, String message, String data, PrintWriter out) {
		out.write("<response><status>"+status+"</status><message>"+message+"</message><data>"+data+"</data></response>");
		return null;
	}
}
