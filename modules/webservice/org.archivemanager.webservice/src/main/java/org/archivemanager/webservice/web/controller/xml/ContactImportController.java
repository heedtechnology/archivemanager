package org.archivemanager.webservice.web.controller.xml;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;
import org.heed.openapps.dictionary.ContactModel;
import org.heed.openapps.SystemModel;
import org.heed.openapps.entity.Association;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.entity.Property;
import org.heed.openapps.entity.ImportProcessor;
import org.heed.openapps.util.XMLUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class ContactImportController {
@Autowired protected EntityService entityService;
	
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/contact/import/add", method = RequestMethod.POST)
	public ModelAndView add(HttpServletRequest req, HttpServletResponse res) throws Exception {
		Map<String,Entity> contacts = (Map<String,Entity>)req.getSession().getAttribute("contacts");
		for(Entity contact : contacts.values()) {
			entityService.addEntity(contact);
			Association addressAssoc = contact.getSourceAssociation(ContactModel.ADDRESSES);
			if(addressAssoc != null && addressAssoc.getTargetEntity() != null) {
				entityService.addEntity(addressAssoc.getTargetEntity());
				addressAssoc.setSource(contact.getId());
				addressAssoc.setTarget(addressAssoc.getTargetEntity().getId());
				entityService.addAssociation(addressAssoc);
			}
			Association phoneAssoc = contact.getSourceAssociation(ContactModel.PHONES);
			if(phoneAssoc != null && phoneAssoc.getTargetEntity() != null) {
				entityService.addEntity(phoneAssoc.getTargetEntity());
				phoneAssoc.setSource(contact.getId());
				phoneAssoc.setTarget(phoneAssoc.getTargetEntity().getId());
				entityService.addAssociation(phoneAssoc);
			}
			for(Association assoc : contact.getSourceAssociations(SystemModel.NOTES)) {
				entityService.addEntity(assoc.getTargetEntity());
				assoc.setSource(contact.getId());
				assoc.setTarget(assoc.getTargetEntity().getId());
				entityService.addAssociation(assoc);
			}
		}
		return response("Success!!!!", "", res.getWriter());
	}
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/contact/import/fetch", method = RequestMethod.GET)
	public ModelAndView fetch(HttpServletRequest req, HttpServletResponse res) throws Exception {
		String source = req.getParameter("_dataSource");
		StringBuffer buff = new StringBuffer();
		Map<String,Entity> contacts = (Map<String,Entity>)req.getSession().getAttribute("contacts");
		String mode = (String)req.getSession().getAttribute("mode");
		String op = req.getParameter("op");
		if(op != null && op.equals("clear")) {
			return response("", res.getWriter());
		}
		//Mapping mapping = model.getMappings().get(mode);
		List<ImportProcessor> parsers = (mode != null) ? entityService.getImportProcessors(mode) : null;
		if(source.equals("treeData")) {
			if(contacts != null) {
				for(Entity contact : contacts.values()) {
					buff.append("<node uid='"+contact.getUid()+"'>");
					for(Property prop : contact.getProperties()) {
						buff.append("<"+prop.getQName().getLocalName()+"><![CDATA["+prop.getValue()+"]]></"+prop.getQName().getLocalName()+">");
					}
					String address1 = "";
					String address2 = "";
					String city = "";
					String state = "";
					String country = "";
					String zip = "";
					Association addressAssoc = contact.getSourceAssociation(ContactModel.ADDRESSES);
					if(addressAssoc != null && addressAssoc.getTargetEntity() != null) {
						if(addressAssoc.getTargetEntity().getPropertyValue(ContactModel.ADDRESS1).length() > 1)
							address1 = addressAssoc.getTargetEntity().getPropertyValue(ContactModel.ADDRESS1);
						if(addressAssoc.getTargetEntity().getPropertyValue(ContactModel.ADDRESS2).length() > 1)
							address2 = addressAssoc.getTargetEntity().getPropertyValue(ContactModel.ADDRESS2);
						if(addressAssoc.getTargetEntity().getPropertyValue(ContactModel.CITY).length() > 1)
							city = addressAssoc.getTargetEntity().getPropertyValue(ContactModel.CITY);
						if(addressAssoc.getTargetEntity().getPropertyValue(ContactModel.STATE).length() > 1)
							state = addressAssoc.getTargetEntity().getPropertyValue(ContactModel.STATE);
						if(addressAssoc.getTargetEntity().getPropertyValue(ContactModel.COUNTRY).length() > 1)
							country = addressAssoc.getTargetEntity().getPropertyValue(ContactModel.COUNTRY);
						if(addressAssoc.getTargetEntity().getPropertyValue(ContactModel.ZIP).length() > 1)
							zip = addressAssoc.getTargetEntity().getPropertyValue(ContactModel.ZIP);
						
					}
					buff.append("<address1><![CDATA["+address1+"]]></address1>");
					buff.append("<address2><![CDATA["+address2+"]]></address2>");
					buff.append("<city><![CDATA["+city+"]]></city>");
					buff.append("<state>"+state+"</state>");
					buff.append("<country><![CDATA["+country+"]]></country>");
					buff.append("<zip><![CDATA["+zip+"]]></zip>");
					Association phoneAssoc = contact.getSourceAssociation(ContactModel.PHONES);
					if(phoneAssoc != null && phoneAssoc.getTargetEntity() != null) {
						buff.append("<phone><![CDATA["+phoneAssoc.getTargetEntity().getPropertyValue(ContactModel.NUMBER)+"]]></phone>");
					}
					buff.append("</node>");
				}
			}
			return response(buff.toString(), res.getWriter());
		} else if(source.equals("node")) {
			String id = req.getParameter("id");
			if(id != null) {				
				Entity node = parsers.get(0).getEntityById(id);
				buff.append("<node id='"+id+"' localName='"+node.getQName().getLocalName()+"'>");
				buff.append("<properties>");
				for(Property prop : node.getProperties()) {
					buff.append("<property name='"+prop.getQName().getLocalName()+"'><value><![CDATA["+prop.getValue()+"]]></value></property>");
				}
				buff.append("</properties>");
				
				return response(buff.toString(), res.getWriter());
			}
		}
		return error("problem fetching resource", res.getWriter());
	}
	@RequestMapping(value="/contact/import/upload", method = RequestMethod.POST)
	public ModelAndView upload(HttpServletRequest req, HttpServletResponse res) throws Exception {
		//Map<String, Object> parms = new HashMap<String, Object>();
		String mode = null;
		//String coll = null;
		//String repo = null;
		try {
			// Create a new file upload handler
			ServletFileUpload upload = new ServletFileUpload();
			// Parse the request
			FileItemIterator iter = upload.getItemIterator(req);
			while(iter.hasNext()) {
			    FileItemStream item = iter.next();
			    String name = item.getFieldName();
			    InputStream stream = item.openStream();
			    if(item.isFormField()) {
			    	if(name.equals("mode")) mode = Streams.asString(stream);
			    	//else if(name.equals("collection")) coll = Streams.asString(stream);
			    	//else if(name.equals("repository")) repo = Streams.asString(stream);
			    	//System.out.println("Form field " + name + " with value " + Streams.asString(tmpStream) + " detected.");
			    } else {
			        //System.out.println("File field " + name + " with file name " + item.getName() + " detected.");
			    	//Entity repository = entityService.getEntity(Long.valueOf(repo));
			    	/*
			    	List<FileImportProcessor> parsers = (FileImportProcessor)entityService.getImportProcessors(mode);
					if(parsers != null) {
						parser.process(stream);
						req.getSession().setAttribute("contacts", parser.getEntities());
						req.getSession().setAttribute("mode", mode);
						//req.getSession().setAttribute("collection", coll);
					}
					*/
			    }
			}
		} catch(FileUploadException e) {
			e.printStackTrace();
		}		
		res.setContentType("text/html");
		res.getWriter().print("<script language='javascript' type='text/javascript'>window.top.window.stopUpload(1);</script>");
		return null;
	}
	
	protected ModelAndView response(String data, PrintWriter out) {
		out.write("<response><status>0</status><data>"+data+"</data></response>");
		return null;
	}
	protected ModelAndView response(String message, String data, PrintWriter out) {
		out.write("<response><status>0</status><message>"+message+"</message><data>"+data+"</data></response>");
		return null;
	}
	protected ModelAndView error(String msg, PrintWriter out) {
		out.write("<response><status>-1</status><message>"+msg+"</message></response>");
		return null;
	}
	public static String toXmlData(Object o) {
		if(o == null) return "";
		else return XMLUtility.escape(o.toString());
	}
}
