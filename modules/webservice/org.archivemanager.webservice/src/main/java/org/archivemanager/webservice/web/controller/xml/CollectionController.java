package org.archivemanager.webservice.web.controller.xml;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.heed.openapps.dictionary.ContactModel;
import org.heed.openapps.QName;
import org.heed.openapps.dictionary.RepositoryModel;
import org.heed.openapps.SystemModel;
import org.heed.openapps.entity.Association;
import org.heed.openapps.entity.BaseEntityQuery;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityQuery;
import org.heed.openapps.entity.EntityResultSet;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.entity.InvalidAssociationException;
import org.heed.openapps.entity.InvalidEntityException;
import org.heed.openapps.entity.ModelValidationException;
import org.heed.openapps.entity.Property;
import org.heed.openapps.entity.ExportProcessor;
import org.heed.openapps.entity.data.FormatInstructions;
import org.heed.openapps.entity.InvalidPropertyException;
import org.heed.openapps.InvalidQualifiedNameException;
import org.heed.openapps.reporting.ReportingService;
import org.heed.openapps.scheduling.SchedulingService;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


public class CollectionController implements ApplicationContextAware {
	@Autowired protected EntityService entityService;
	@Autowired protected SchedulingService scheduleService;
	@Autowired protected ReportingService reportingService;
	
	public void setApplicationContext(ApplicationContext ctx) throws BeansException {
		//CollectionItemDataGenerator generator = new CollectionItemDataGenerator();
		//entityService.registerDataGenerator(RepositoryModel.ITEM.toString(), generator);
		//entityService.registerDataGenerator(RepositoryModel.CATEGORY.toString(), generator);
	}
	
	@RequestMapping(value="/item/{id}", method = RequestMethod.GET)
	public ModelAndView itemDetail(HttpServletRequest req, HttpServletResponse res,@PathVariable("id") Long id) throws Exception {
		StringBuffer buff = new StringBuffer();
		Entity entity = entityService.getEntity(id);
		if(req.getRequestURI().endsWith(".html")) {
			Map<String, Object> parms = new HashMap<String, Object>();
			parms.put("item", entity);
			List<Association> noteDocs = entity.getSourceAssociations(SystemModel.NOTES);
			if(noteDocs.size() > 0) {
				List<Entity> notes = new ArrayList<Entity>();
				for(Association doc : noteDocs) {
					Entity note = entityService.getEntity(doc.getTarget());
					notes.add(note);
				}
				parms.put("notes", notes);
			}
			return new ModelAndView("item", parms);
		} else
			return response(0, "", buff.toString(), res.getWriter());
	}
	@RequestMapping(value="/collection/accession", method = RequestMethod.GET)
	public ModelAndView fetchCollectionAccessionTreeData(HttpServletRequest req, HttpServletResponse res) throws Exception {
		StringBuffer buff = new StringBuffer();
		res.setContentType("text/xml; charset=UTF-8");
		res.setCharacterEncoding("UTF-8");
		String parent = req.getParameter("parent");
		String collectionId = req.getParameter("collection");
		if(parent == null || parent.equals("null")) parent = collectionId;
		if(parent == null || parent.equals("null")) {	
			String query = req.getParameter("query");
			EntityQuery eQuery = (query != null) ? new BaseEntityQuery(RepositoryModel.COLLECTION, query, "name_e", true) : new BaseEntityQuery(RepositoryModel.COLLECTION, null, "name_e", true);
			int startRow = (req.getParameter("_startRow") != null) ? Integer.valueOf(req.getParameter("_startRow")) : 0;
			int endRow = (req.getParameter("_endRow") != null) ? Integer.valueOf(req.getParameter("_endRow")) : 75;
			eQuery.setStartRow(startRow);
			eQuery.setEndRow(endRow);
			eQuery.getFields().add("name");
			EntityResultSet collections = entityService.search(eQuery);
			for(Entity collection : collections.getResults()) {
				Property name = collection.getProperty(SystemModel.NAME);
				buff.append("<node id='"+collection.getId()+"' qname='{openapps.org_repository_1.0}collection' localName='collection' parent='null'><title><![CDATA["+clean(name.toString())+"]]></title></node>");
				buff.append("<node id='"+collection.getId()+"-accessions' qname='{openapps.org_repository_1.0}accessions' localName='accessions' parent='"+collection.getId()+"'><title>Accessions</title></node>");
				buff.append("<node id='"+collection.getId()+"-categories' qname='{openapps.org_repository_1.0}categories' localName='categories' parent='"+collection.getId()+"'><title>Categories</title></node>");
			}
			res.getWriter().append("<response><status>0</status><totalRows>"+collections.getResultSize()+"</totalRows><startRow>"+startRow+"</startRow><endRow>"+endRow+"</endRow><data>"+buff.toString()+"</data></response>");
			return null;
		} else {
			ExportProcessor processor = entityService.getExportProcessor("default");
			List<Association> list = new ArrayList<Association>();
			if(parent.endsWith("-accessions")) {
				Entity collection = entityService.getEntity(Long.valueOf(parent.substring(0, parent.length()-11)));
				for(Association assoc : collection.getSourceAssociations(RepositoryModel.ACCESSIONS)) {
					assoc.setSortField(RepositoryModel.ACCESSION_DATE);
				}
				//entityService.hydrate(collection);
				for(Association assoc : collection.getSourceAssociations(RepositoryModel.ACCESSIONS)) {
					buff.append(processor.export(new FormatInstructions(true), assoc));
				}
				res.getWriter().append("<response><status>0</status><data>"+buff.toString()+"</data></response>");
				return null;
			} else if(parent.endsWith("-categories")) {
				Entity collection = entityService.getEntity(Long.valueOf(parent.substring(0, parent.length()-11)));	
				//entityService.hydrate(collection);
				list = collection.getSourceAssociations(RepositoryModel.ITEMS,RepositoryModel.CATEGORIES);
			} else {
				Entity collection = entityService.getEntity(Long.valueOf(parent));
				//entityService.hydrate(collection);
				list = collection.getSourceAssociations(RepositoryModel.ITEMS,RepositoryModel.CATEGORIES);
				
			}			
			for(Association component : list) {	
				//DataGenerator generator = entityService.getDataGenerator(component.getQName());
				if(component.getQName().equals(RepositoryModel.COLLECTION) || 
						component.getQName().equals(RepositoryModel.ACCESSION) ||
						component.getQName().equals(RepositoryModel.CATEGORY))
					buff.append(processor.export(new FormatInstructions(true), component.getTargetEntity()));
				else buff.append(processor.export(new FormatInstructions(true), component));
			}
			res.getWriter().append("<response><status>0</status><data>"+buff.toString()+"</data></response>");
			return null;			
		}
	}
	@RequestMapping(value="/collection", method = RequestMethod.GET)
	public ModelAndView fetchCollectionTreeData(HttpServletRequest req, HttpServletResponse res) throws Exception {
		StringBuffer buff = new StringBuffer();
		String parent = req.getParameter("parent");
		String collectionId = req.getParameter("collection");
		if(parent == null || parent.equals("null")) parent = collectionId;
		if(parent == null || parent.equals("null")) {	
			String query = req.getParameter("query");
			EntityQuery eQuery = (query != null) ? new BaseEntityQuery(RepositoryModel.COLLECTION, query, "name_e", true) : new BaseEntityQuery(RepositoryModel.COLLECTION, null, "name_e", true);
			int startRow = (req.getParameter("_startRow") != null) ? Integer.valueOf(req.getParameter("_startRow")) : 0;
			int endRow = (req.getParameter("_endRow") != null) ? Integer.valueOf(req.getParameter("_endRow")) : 75;
			eQuery.setStartRow(startRow);
			eQuery.setEndRow(endRow);
			eQuery.getFields().add("name");
			EntityResultSet collections = entityService.search(eQuery);
			for(Entity collection : collections.getResults()) {
				Property name = collection.getProperty(SystemModel.NAME);
				buff.append("<node id='"+collection.getId()+"' qname='{openapps.org_repository_1.0}collection' localName='collection' parent='null'><title><![CDATA["+name.toString()+"]]></title></node>");
			}
			res.getWriter().append("<response><status>0</status><totalRows>"+collections.getResultSize()+"</totalRows><startRow>"+startRow+"</startRow><endRow>"+endRow+"</endRow><data>"+buff.toString()+"</data></response>");
			return null;
		} else {
			Entity collection = entityService.getEntity(Long.valueOf(parent));
			List<Association> list = new ArrayList<Association>();
			for(Association assoc : collection.getSourceAssociations(RepositoryModel.CATEGORIES, RepositoryModel.ITEMS)) {
				if(assoc.getTargetEntity() == null) {
					Entity accession = entityService.getEntity(assoc.getTarget());
					assoc.setTargetEntity(accession);
				}
				list.add(assoc);
			}
			ExportProcessor processor = entityService.getExportProcessor("default");
			for(Association component : list) {	
				buff.append(processor.export(new FormatInstructions(true), component));
			}
			res.getWriter().append("<response><status>0</status><data>"+buff.toString()+"</data></response>");
			return null;			
		}
	}
	
	@RequestMapping(value="/collection/update", method = RequestMethod.POST)
	public ModelAndView moveCollection(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("id") Long id, @RequestParam("parent") Long parent) throws Exception {
		Entity source = entityService.getEntity(id);
		Entity target = entityService.getEntity(parent);
		Association assoc = source.getTargetAssociation(RepositoryModel.CATEGORIES);
		if(assoc == null) assoc = source.getTargetAssociation(RepositoryModel.ITEMS);
		if(assoc == null) assoc = source.getTargetAssociation(RepositoryModel.ACCESSIONS);
		if(assoc != null && source != null && target != null) {
			assoc.setSource(target.getId());
			entityService.updateAssociation(assoc);
		}
		ExportProcessor processor = entityService.getExportProcessor("default");
		return response(0, "", (String)processor.export(new FormatInstructions(true), assoc), response.getWriter());
	}
	@RequestMapping(value="/collection/propagate", method = RequestMethod.POST)
	public ModelAndView propagate(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("id") Long id, @RequestParam("qname") String type, @RequestParam("value") String value) throws Exception {
		if(type.equals("qname")) {
			cascadeQName(id, RepositoryModel.ITEMS, QName.createQualifiedName(value));
			cascadeQName(id, RepositoryModel.CATEGORIES, QName.createQualifiedName(value));
		} else {
			entityService.cascadeProperty(id, RepositoryModel.ITEMS, QName.createQualifiedName(type), (Serializable)value);
		}
		return response(0, "Successfully updated child properties.", null, response.getWriter());		
	}
	@RequestMapping(value="/collection/orphans", method = RequestMethod.POST)
	public ModelAndView orphans(HttpServletRequest req, HttpServletResponse res, @RequestParam("id") long id) throws Exception {
		Entity repository = entityService.getEntity(id);
		EntityQuery eQuery = new BaseEntityQuery(RepositoryModel.COLLECTION, null, "name_e", true);
		eQuery.setEndRow(0);
		EntityResultSet collections = entityService.search(eQuery);
		for(Entity collection : collections.getResults()) {
			Association association = collection.getTargetAssociation(RepositoryModel.COLLECTIONS);
			if(association == null) {
				System.out.println(collection.getPropertyValue(SystemModel.NAME)+" is an orphan");
				Association newAssociation = new Association(RepositoryModel.COLLECTIONS);
				newAssociation.setSource(repository.getId());
				newAssociation.setTarget(collection.getId());
				entityService.addAssociation(newAssociation);
			}
		}
		return response(0, "Successfully gathered all orphan collections.", null, res.getWriter());
	}
	@RequestMapping(value="/collection/remove.xml", method = RequestMethod.POST)
	public ModelAndView removeEntity(HttpServletRequest request, HttpServletResponse response, @RequestParam("id") Long id) throws Exception {
		Entity entity = entityService.getEntity(id);
		String uid = entity.getUid();
		entityService.removeEntity(id);
		return response(0, "", "<node id='"+id+"' uid='"+uid+"' />", response.getWriter());
	}
	@RequestMapping(value="/accession/contact/remove.xml", method = RequestMethod.POST)
	public ModelAndView contactRemove(HttpServletRequest req, HttpServletResponse res, @RequestParam("id") Long id) throws Exception {
		Association assoc = entityService.getAssociation(id);
		Entity accession = entityService.getEntity(assoc.getSource());
		Entity contact = entityService.getEntity(assoc.getTarget());
		for(Association addressAssoc : contact.getSourceAssociations(ContactModel.ADDRESSES)) {
			Entity address = entityService.getEntity(addressAssoc.getTarget());
			for(Association accessionAddressAssoc : accession.getSourceAssociations(ContactModel.ADDRESSES)) {
				if(accessionAddressAssoc.getTarget().equals(address.getId()))
					entityService.removeAssociation(accessionAddressAssoc.getId());
			}
		}
		entityService.removeAssociation(id);
		return response(0, "", "<node id='"+id+"' />", res.getWriter());
	}
	protected void cascadeQName(Long id, QName association, QName qname) throws InvalidAssociationException, InvalidEntityException, InvalidPropertyException, ModelValidationException, InvalidQualifiedNameException {
		Entity entity = entityService.getEntity(id);
		for(Association assoc : entity.getSourceAssociations(association)) {
			Entity targetEntity = entityService.getEntity(assoc.getTarget());
			if(association.equals(RepositoryModel.CATEGORIES)) {
				assoc.setQname(RepositoryModel.ITEMS);
				entityService.updateAssociation(assoc);
				targetEntity.addProperty(RepositoryModel.CATEGORY_LEVEL, "item");
			}
			targetEntity.setQname(qname);
			entityService.updateEntity(targetEntity, true);
			cascadeQName(targetEntity.getId(), association, qname);
		}
	}
	protected ModelAndView response(int status, String message, String data, PrintWriter out) {
		out.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?><response><status>"+status+"</status><message>"+message+"</message><data>"+data+"</data></response>");
		return null;
	}
	protected String clean(String in) {
		return in.replace("��", "");
	}
}
