package org.archivemanager.webservice.web.controller.json;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.heed.openapps.dictionary.ContentModel;
import org.heed.openapps.QName;
import org.heed.openapps.SystemModel;
import org.heed.openapps.cache.CacheService;
import org.heed.openapps.content.FileNode;
import org.heed.openapps.data.RestResponse;
import org.heed.openapps.entity.Association;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.entity.ExportProcessor;
import org.heed.openapps.entity.data.FormatInstructions;
import org.heed.openapps.util.NumberUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

public class JsonDigitalObjectController {
	private Logger log = LoggerFactory.getLogger(JsonDigitalObjectController.class);
	@Autowired protected EntityService entityService;
	@Autowired protected CacheService cacheService;
	
	@ResponseBody
	@RequestMapping(value="/content/file/associate.json", method = RequestMethod.POST)
	public RestResponse addFile(HttpServletRequest request, HttpServletResponse response, @RequestParam("source") String source, @RequestParam("target") String target) throws Exception {
		RestResponse data = new RestResponse();
		Entity sourceEntity = NumberUtility.isLong(source) ? entityService.getEntity(Long.valueOf(source)) : entityService.getEntity(source);
		Entity targetEntity = null;
		try {
			targetEntity = entityService.getEntity(target);
		} catch(Exception e) {
			
		}
		if(targetEntity == null) {
			Object obj = cacheService.get("objectCache", target);
			if(obj != null) {
				FileNode node = (FileNode)obj;
				targetEntity = new Entity(ContentModel.FILE);
				targetEntity.setUid(target);
				if(node.getName() != null) targetEntity.addProperty(SystemModel.NAME, node.getName());
				targetEntity.addProperty(ContentModel.PATH, node.getPath());
				targetEntity.addProperty(ContentModel.CONTENT_SIZE, node.getSize());
				targetEntity.addProperty(ContentModel.DATASOURCE, node.getDatasource());
				entityService.addEntity(targetEntity);
			}
		}
		if(sourceEntity != null && targetEntity != null) {
			Association assoc = entityService.getAssociation(request, QName.createQualifiedName("{openapps.org_content_1.0}files"));
			assoc.setSourceEntity(sourceEntity);
			assoc.setTargetEntity(targetEntity);
			entityService.addAssociation(assoc);
			ExportProcessor processor = entityService.getExportProcessor(sourceEntity.getQName().toString());
			if(processor == null) processor = entityService.getExportProcessor("default");
			FormatInstructions instructions = new FormatInstructions(false, true, false);
			data.getResponse().getData().add(processor.export(instructions, sourceEntity));
		}
		data.getResponse().addMessage("problem adding association between source:"+source+" target:"+target);
		return data;
	}
}
