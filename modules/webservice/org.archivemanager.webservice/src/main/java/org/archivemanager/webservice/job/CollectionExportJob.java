package org.archivemanager.webservice.job;
import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.archivemanager.webservice.util.ContainerSorter;
import org.archivemanager.webservice.util.FindingAidUtil;
import org.heed.openapps.dictionary.RepositoryModel;
import org.heed.openapps.SystemModel;
import org.heed.openapps.entity.Association;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.entity.InvalidEntityException;
import org.heed.openapps.InvalidQualifiedNameException;
import org.heed.openapps.scheduling.ExecutionContext;
import org.heed.openapps.scheduling.JobSupport;
import org.heed.openapps.util.HTMLUtility;
import org.heed.openapps.util.IOUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class CollectionExportJob extends JobSupport {
	private static final long serialVersionUID = 7588413587668803728L;
	private Logger log = LoggerFactory.getLogger(CollectionExportJob.class);
	protected EntityService entityService;
	protected Long id;
	protected String format;
	
	String primarContainer = "";
	String secondaryContainer = "";
	boolean printLevel2 = true;
	boolean printLevel3 = true;
	boolean printLevel4 = true;
	
	public static final int width = 900;
	
	
	public CollectionExportJob(Long id, String format) {
		this.id = id;
		this.format = format;
	}
	
	@Override
	public void execute(ExecutionContext context) {
		super.execute(context);		
		try {
			Entity collection = entityService.getEntity(id);
			String html = doHTML(collection);
			if(format.equals("pdf")) {
				//FileInputStream in = new FileInputStream("data/finding_aids/"+id+".html");
				//FileOutputStream out = new FileOutputStream("data/finding_aids/"+id+".pdf");
				/*
				Document document = new Document();
			    PdfWriter writer = PdfWriter.getInstance(document, out);
			    document.open();
			    XMLWorkerHelper.getInstance().parseXHtml(writer, document, in);
			    document.close();
			    */
				//doPDF(collection, out);
			} else if(format.equals("doc")) {
				//FileInputStream in = new FileInputStream("data/finding_aids/"+id+".html");
				FileOutputStream out = new FileOutputStream("data/finding_aids/"+id+".doc");
				doDOC(collection, out);
			} else {
				FileOutputStream out = new FileOutputStream("data/finding_aids/"+id+".html");
				ByteArrayInputStream in = new ByteArrayInputStream(html.getBytes("UTF-8"));
				IOUtility.pipe(in, out);
			}
			setComplete(true);
		} catch(Exception e) {
			e.printStackTrace();
			setLastMessage(e.getLocalizedMessage());
			setComplete(true);
		}
	}
	
	protected String doHTML(Entity collection) throws IOException, InvalidEntityException, InvalidQualifiedNameException {
		FindingAidUtil util = new FindingAidUtil();
		StringWriter writer = new StringWriter();
		
		writer.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
		writer.write("<html xmlns=\"http://www.w3.org/1999/xhtml\">");
		writer.write("<head>");
		writer.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />");
		writer.write("</head>");
		
		writer.write("<div style=\"text-align:center;\">"+collection.getPropertyValue(SystemModel.NAME)+"</div>");
		writer.write("<div style=\"text-align:center;\">#"+HTMLUtility.removeTags(collection.getPropertyValue(RepositoryModel.COLLECTION_IDENTIFIER))+"</div>");
		writer.write("<div style=\"text-align:center;\">"+HTMLUtility.removeTags(collection.getPropertyValue(RepositoryModel.ACCESSION_DATE))+"</div>");
		writer.write("<div style=\"text-align:center;\">Preliminary Listing</div>");
		writer.write("<div style=\"height:100%;width:"+width+"px;margin:0 auto;\">");
		
		Map<Long, Entity> nodeMap = getEntityMap(collection);
		ContainerSorter containerSorter = new ContainerSorter(entityService);
		int count = 0;
		List<Association> level1Children = collection.getSourceAssociations(RepositoryModel.CATEGORIES, RepositoryModel.ITEMS);
		Collections.sort(level1Children, containerSorter);
		for(int i=0; i < level1Children.size(); i++) {
			count++;
			setLastMessage("processing "+count+" of "+nodeMap.size()+" items");
			Association assoc1 = level1Children.get(i);
			Entity level1 = nodeMap.get(assoc1.getTarget());	
			String level1Summary = level1.getPropertyValue(RepositoryModel.SUMMARY);
			if(level1Summary != null && level1Summary.length() > 0) level1Summary = "["+level1Summary+"]";
			else level1Summary = "";
			String container1 = level1.getPropertyValue(RepositoryModel.CONTAINER);
			String[] containers1 = container1.split(",");
			String c1 = containers1[0] != null && containers1[0].length() > 0 ? containers1[0] : "";
		
			writer.write("<div style=\"float:left;width:100%;border:0px solid black;padding:5px;\">");
			writer.write("<div style=\"margin-left:100px;width:50px;float:left;border:0px solid black;\">"+util.getNextMarker(1)+".</div>");
			writer.write("<div style=\"float:left;width:"+(width-155)+"px;border:0px solid black;\">");
			writer.write(level1.getPropertyValue(SystemModel.NAME));
			//out.write(level1.getPropertyValue(RepositoryModel.DESCRIPTION));
			writer.write(level1Summary);
			writer.write("</div></div>");			
			if(printLevel2) {
				//entityService.hydrate(level1);
				List<Association> level2Children = level1.getSourceAssociations(RepositoryModel.CATEGORIES, RepositoryModel.ITEMS);
				Collections.sort(level2Children, containerSorter);
				for(int j=0; j < level2Children.size(); j++) {
					count++;
					Association assoc2 = level2Children.get(j);
					Entity level2 = nodeMap.get(assoc2.getTarget());	
					String level2Summary = level2.getPropertyValue(RepositoryModel.SUMMARY);
					String level2Name = level2.getPropertyValue(SystemModel.NAME);
					String level2Desc = level2.getPropertyValue(RepositoryModel.DESCRIPTION);
					if(level2Summary != null && level2Summary.length() > 0) level2Summary = "["+level2Summary+"]";
					else level2Summary = "";
					String container = level2.getPropertyValue(RepositoryModel.CONTAINER);
					String[] containers = getContainers(container);
			
					writer.write("<div style=\"float:left;width:100%;border:0px solid black;padding:5px;\">");
					writer.write("<div style=\"width:100px;float:left;\">"+containers[0]+"</div>");
					writer.write("<div style=\"width:50px;margin-left:50px;float:left;border:0px solid black;\">"+util.getNextMarker(2)+".</div>");
					writer.write("<div style=\"float:left;width:"+(width-285)+"px;border:0px solid black;\">");
					if(level2Desc != null && level2Desc.length() > 0) writer.write(level2Desc);
					else writer.write(level2Name);
					writer.write(level2Summary);
					writer.write("</div>");
					writer.write("<div style=\"float:right;\">"+containers[1]+"</div>");
					writer.write("</div>");
					if(printLevel3) {
						//entityService.hydrate(level2);
						List<Association> level3Children = level2.getSourceAssociations(RepositoryModel.CATEGORIES, RepositoryModel.ITEMS);
						Collections.sort(level3Children, containerSorter);
						for(int k=0; k < level3Children.size(); k++) {
							count++;
							Association assoc3 = level3Children.get(k);
							Entity level3 = nodeMap.get(assoc3.getTarget());
							String level3Name = level3.getPropertyValue(SystemModel.NAME);
							String level3Desc = level3.getPropertyValue(RepositoryModel.DESCRIPTION);
							String level3Summary = level3.getPropertyValue(RepositoryModel.SUMMARY);							
							if(level3Summary != null && level3Summary.length() > 0) level3Summary = "["+level3Summary+"]";
							else level3Summary = "";
							String container3 = level3.getPropertyValue(RepositoryModel.CONTAINER);
							String[] containers3 = getContainers(container3);
														
							writer.write("<div style=\"float:left;width:100%;border:0px solid black;padding:5px;\">");
							writer.write("<div style=\"width:100px;float:left;\">"+containers3[0]+"</div>");
							writer.write("<div style=\"width:50px;margin-left:100px;float:left;border:0px solid black;\">"+util.getNextMarker(3)+".</div>");
							writer.write("<div style=\"float:left;width:"+(width-335)+"px;border:0px solid black;\">");
							if(level3Desc != null && level3Desc.length() > 0) writer.write(level3Desc);
							else writer.write(level3Name);
							writer.write(level3Summary);
							writer.write("</div>");
							writer.write("<div style=\"float:right;\">"+containers3[1]+"</div>");
							writer.write("</div>");
					
							if(printLevel4) {
								//entityService.hydrate(level3);
								List<Association> level4Children = level3.getSourceAssociations(RepositoryModel.CATEGORIES, RepositoryModel.ITEMS);
								Collections.sort(level4Children, containerSorter);
								for(int l=0; l < level4Children.size(); l++) {
									count++;
									Association assoc4 = level4Children.get(l);
									Entity level4 = nodeMap.get(assoc4.getTarget());
									String level4Name = level4.getPropertyValue(SystemModel.NAME);
									String level4Desc = level4.getPropertyValue(RepositoryModel.DESCRIPTION);
									String level4Summary = level4.getPropertyValue(RepositoryModel.SUMMARY);
									String level4Date = level4.getPropertyValue(RepositoryModel.DATE_EXPRESSION);
									if(level4Summary != null && level4Summary.length() > 0) level4Summary = "["+level4Summary+"]";
									else level4Summary = "";
									String container4 = level4.getPropertyValue(RepositoryModel.CONTAINER);
									String[] containers4 = getContainers(container4);
						
									writer.write("<div style=\"float:left;width:100%;border:0px solid black;padding:5px;\">");
									writer.write("<div style=\"width:100px;float:left;\">"+containers4[0]+"</div>");
									
									String marker = util.getNextMarker(4);
									if(marker.length() < 5) {
										writer.write("<div style=\"width:50px;margin-left:150px;float:left;border:0px solid black;\">"+marker+".</div>");
										writer.write("<div style=\"float:left;width:"+(width-385)+"px;border:0px solid black;\">");
									} else {
										for(int m=1; m < 25; m++) {
											if(marker.length() == (4 + m)) {
												writer.write("<div style=\"width:"+(50+(m*10))+"px;margin-left:"+(150-(m*10))+"px;float:left;border:0px solid black;\">"+util.getNextMarker(4)+".</div>");
												writer.write("<div style=\"float:left;width:"+(385 - (m*10))+"px;border:0px solid black;\">");
												break;
											}
										}
									}
									if(level4Desc != null && level4Desc.length() > 0) writer.write(level4Desc);
									else if(level4Name != null && level4Name.length() > 0) writer.write(level4Name);
									else writer.write(level4Date);
									writer.write(level4Summary);
									writer.write(containers4[1]);
									writer.write("</div>");
									writer.write("</div>");
						
									//entityService.hydrate(level4);
									List<Association> level5Children = level4.getSourceAssociations(RepositoryModel.CATEGORIES, RepositoryModel.ITEMS);
									Collections.sort(level5Children, containerSorter);
									for(int m=0; m < level5Children.size(); m++) {
										count++;
										Association assoc5 = level5Children.get(m);
										Entity level5 = nodeMap.get(assoc5.getTarget());						
										String level5Summary = level5.getPropertyValue(RepositoryModel.SUMMARY);
										if(level5Summary != null && level5Summary.length() > 0) level5Summary = "["+level5Summary+"]";
										else level5Summary = "";
										String container5 = level5.getPropertyValue(RepositoryModel.CONTAINER);
										String[] containers5 = getContainers(container5);
						
										writer.write("<div style=\"float:left;width:100%;border:0px solid black;padding:5px;\">");
										writer.write("<div style=\"width:100px;float:left;\">"+containers5[0]+"</div>");
										writer.write("<div style=\"width:50px;margin-left:200px;float:left;border:0px solid black;\">"+util.getNextMarker(5)+".</div>");
										writer.write("<div style=\"float:left;width:"+(width-435)+"px;border:0px solid black;\">");
										//writer.write(level5.getPropertyValue(SystemModel.NAME));
										writer.write(level5.getPropertyValue(RepositoryModel.DESCRIPTION));
										writer.write(level5Summary);
										writer.write(containers5[1]);
										writer.write("</div></div>");
										
										List<Association> level6Children = level5.getSourceAssociations(RepositoryModel.CATEGORIES, RepositoryModel.ITEMS);
										Collections.sort(level6Children, containerSorter);
										for(int n=0; n < level6Children.size(); n++) {
											count++;
											setLastMessage("processing "+count+" of "+nodeMap.size()+" items");
											Association assoc6 = level5Children.get(n);
											Entity level6 = nodeMap.get(assoc5.getTarget());						
											String level6Summary = level6.getPropertyValue(RepositoryModel.SUMMARY);
											if(level6Summary != null && level6Summary.length() > 0) level6Summary = "["+level6Summary+"]";
											else level6Summary = "";
											String container6 = level6.getPropertyValue(RepositoryModel.CONTAINER);
											String[] containers6 = getContainers(container5);
							
											writer.write("<div style=\"float:left;width:100%;border:0px solid black;padding:5px;\">");
											writer.write("<div style=\"width:100px;float:left;\">"+containers6[0]+"</div>");
											writer.write("<div style=\"width:50px;margin-left:200px;float:left;border:0px solid black;\">"+util.getNextMarker(6)+".</div>");
											writer.write("<div style=\"float:left;width:"+(width-435)+"px;border:0px solid black;\">");
											//writer.write(level6.getPropertyValue(SystemModel.NAME));
											writer.write(level6.getPropertyValue(RepositoryModel.DESCRIPTION));
											writer.write(level6Summary);
											writer.write(containers6[1]);
											writer.write("</div></div>");				
										}	
									}						
								}
							}
						}
					}
				}
			}
		}
		setLastMessage("finished processing "+nodeMap.size()+" items");
		writer.close();
		return writer.toString();
	}
	protected void doDOC(Entity collection, OutputStream out) throws IOException, InvalidEntityException, InvalidQualifiedNameException {
		
	}
	/*
	protected void doPDF(Entity collection, OutputStream out) throws IOException, DocumentException, InvalidEntityException, InvalidQualifiedNameException {
		Map<Long, Entity> nodeMap = getEntityMap(collection);
		ContainerSorter containerSorter = new ContainerSorter(nodeMap);
		FindingAidUtil util = new FindingAidUtil();
		boolean printLevel2 = false;
		boolean printLevel3 = false;
		boolean printLevel4 = false;
		
		String primarContainer = "";
		String secondaryContainer = "";
		
		int count = 0;
		
		com.itextpdf.text.Document document = new com.itextpdf.text.Document();
		com.itextpdf.text.Document.plainRandomAccess = true;
		PdfWriter.getInstance(document, out);
		document.open();
		
		Paragraph title = new Paragraph(HTMLUtility.removeTags(collection.getPropertyValue(SystemModel.NAME)));
		title.setAlignment(Element.ALIGN_CENTER);
		document.add(title);
		Paragraph identifier = new Paragraph("#"+HTMLUtility.removeTags(collection.getPropertyValue(RepositoryModel.COLLECTION_IDENTIFIER)));
		identifier.setAlignment(Element.ALIGN_CENTER);
		document.add(identifier);
		Paragraph accession_date = new Paragraph(HTMLUtility.removeTags(collection.getPropertyValue(RepositoryModel.ACCESSION_DATE)));
		accession_date.setAlignment(Element.ALIGN_CENTER);
		document.add(accession_date);
		Paragraph label1 = new Paragraph("Preliminary Listing");
		label1.setAlignment(Element.ALIGN_CENTER);
		document.add(label1);
		
		setLastMessage("processing "+count+" of "+nodeMap.size()+" items");
		List<Association> level1Children = collection.getSourceAssociations(RepositoryModel.CATEGORIES, RepositoryModel.ITEMS);
		Collections.sort(level1Children, containerSorter);
		for(int i=0; i < level1Children.size(); i++) {
			count++;
			setLastMessage("processing "+count+" of "+nodeMap.size()+" items");
			Association assoc1 = level1Children.get(i);
			Entity level1 = nodeMap.get(assoc1.getTarget());	
			String level1Summary = level1.getPropertyValue(RepositoryModel.SUMMARY);
			if(level1Summary != null && level1Summary.length() > 0) level1Summary = "["+level1Summary+"]";
			else level1Summary = "";
			String container1 = level1.getPropertyValue(RepositoryModel.CONTAINER);
			String[] containers1 = container1.split(",");
			String c1 = containers1[0] != null && containers1[0].length() > 0 ? containers1[0] : "";

			Paragraph item = new Paragraph(util.getNextMarker(1)+".    ");
			item.setSpacingBefore(5);
			item.setAlignment(Element.ALIGN_LEFT);
			item.add(level1.getPropertyValue(SystemModel.NAME));
			document.add(item);
			
			if(printLevel2) {
			//entityService.hydrate(level1);
			List<Association> level2Children = level1.getSourceAssociations(RepositoryModel.CATEGORIES, RepositoryModel.ITEMS);
			Collections.sort(level2Children, containerSorter);
			for(int j=0; j < level2Children.size(); j++) {
				count++;
				setLastMessage("processing "+count+" of "+nodeMap.size()+" items");
				Association assoc2 = level2Children.get(j);
				Entity level2 = nodeMap.get(assoc2.getTarget());;		
				String level2Summary = level2.getPropertyValue(RepositoryModel.SUMMARY);
				if(level2Summary != null && level2Summary.length() > 0) level2Summary = "["+level2Summary+"]";
				else level2Summary = "";
				String container2 = level2.getPropertyValue(RepositoryModel.CONTAINER);
				String[] containers2 = container2.split(",");
				String c2 = containers2[0] != null && containers2[0].length() > 0 ? containers2[0] : "&nbsp;";
				String c22 = "";
				if(containers2.length == 4) {
					if(containers2[2].equals("folder")) c22 = "[F. "+containers2[3]+"]";
					if(containers2[2].equals("package")) c22 = "[P. "+containers2[3]+"]";
				}
				if(c2.equals(primarContainer)) c2 = "";
				else primarContainer = c2;
				
				Paragraph item2 = new Paragraph(util.getNextMarker(1)+".    ");
				item2.setAlignment(Element.ALIGN_LEFT);
				item2.add(level1.getPropertyValue(SystemModel.NAME));
				item2.add(level1.getPropertyValue(RepositoryModel.DESCRIPTION));
				item2.add(level1Summary);
				document.add(item2);
				
				if(printLevel3) {
				//entityService.hydrate(level2);
				List<Association> level3Children = level2.getSourceAssociations(RepositoryModel.CATEGORIES, RepositoryModel.ITEMS);
				Collections.sort(level3Children, containerSorter);
				for(int k=0; k < level3Children.size(); k++) {
					count++;
					setLastMessage("processing "+count+" of "+nodeMap.size()+" items");
					Association assoc3 = level3Children.get(k);
					Entity level3 = nodeMap.get(assoc3.getTarget());;			
					String level3Summary = level3.getPropertyValue(RepositoryModel.SUMMARY);
					if(level3Summary != null && level3Summary.length() > 0) level3Summary = "["+level3Summary+"]";
					else level3Summary = "";
					String container3 = level3.getPropertyValue(RepositoryModel.CONTAINER);
					String[] containers3 = container3.trim().replace("  ", " ").split(" ");
					String c3 = containers3[0] != null && containers3[0].length() > 0 ? containers3[0] : "&nbsp;";
					String c33 = "";
					if(containers3.length == 4) {
						if(containers3[2].equals("Folder")) c33 = "[F. "+containers3[3]+"]";
						if(containers3[2].equals("Package")) c33 = "[P. "+containers3[3]+"]";
					}
					if(c33.equals(secondaryContainer)) c33 = "";
					else secondaryContainer = c33;
				
					if(printLevel4) {
					//entityService.hydrate(level3);
					List<Association> level4Children = level3.getSourceAssociations(RepositoryModel.CATEGORIES, RepositoryModel.ITEMS);
					Collections.sort(level4Children, containerSorter);
					for(int l=0; l < level4Children.size(); l++) {
						count++;
						setLastMessage("processing "+count+" of "+nodeMap.size()+" items");
						Association assoc4 = level4Children.get(l);
						Entity level4 = nodeMap.get(assoc4.getTarget());;
						
						String level4Summary = level4.getPropertyValue(RepositoryModel.SUMMARY);
						if(level4Summary != null && level4Summary.length() > 0) level4Summary = "["+level4Summary+"]";
						else level4Summary = "";
						String container4 = level4.getPropertyValue(RepositoryModel.CONTAINER);
						String[] containers4 = container4.trim().replace("  ", " ").split(" ");
						String c4 = containers4[0] != null && containers4[0].length() > 0 ? containers4[0] : "&nbsp;";
						String c44 = "";
						if(containers4.length == 4) {
							if(containers4[2].equals("folder")) c44 = "[F. "+containers4[3]+"]";
							if(containers4[2].equals("package")) c44 = "[P. "+containers4[3]+"]";
						}
						if(c44.equals(secondaryContainer)) c44 = "";
						else secondaryContainer = c44;
						
						//entityService.hydrate(level4);
						List<Association> level5Children = level4.getSourceAssociations(RepositoryModel.CATEGORIES, RepositoryModel.ITEMS);
						Collections.sort(level5Children, containerSorter);
						for(int m=0; m < level5Children.size(); m++) {
							count++;
							setLastMessage("processing "+count+" of "+nodeMap.size()+" items");
							Association assoc5 = level5Children.get(m);
							Entity level5 = nodeMap.get(assoc5.getTarget());;
						
							String level5Summary = level5.getPropertyValue(RepositoryModel.SUMMARY);
							if(level5Summary != null && level5Summary.length() > 0) level5Summary = "["+level5Summary+"]";
							else level5Summary = "";
							String container5 = level5.getPropertyValue(RepositoryModel.CONTAINER);
							String[] containers5 = container5.trim().replace("  ", " ").split(" ");
							String c5 = containers5[0] != null && containers5[0].length() > 0 ? containers5[0] : "&nbsp;";
							String c55 = "";
							if(containers5.length == 4) {
								if(containers5[2].equals("folder")) c55 = "[F. "+containers5[3]+"]";
								if(containers5[2].equals("package")) c55 = "[P. "+containers5[3]+"]";
							}
							if(c55.equals(secondaryContainer)) c55 = "";
							else secondaryContainer = c55;
							List<Association> level6Children = level5.getSourceAssociations(RepositoryModel.CATEGORIES, RepositoryModel.ITEMS);
							Collections.sort(level6Children, containerSorter);
							for(int n=0; n < level6Children.size(); n++) {
								count++;
								setLastMessage("processing "+count+" of "+nodeMap.size()+" items");
								Association assoc6 = level5Children.get(m);
								Entity level6 = nodeMap.get(assoc6.getTarget());							
								String level6Summary = level6.getPropertyValue(RepositoryModel.SUMMARY);
								if(level6Summary != null && level6Summary.length() > 0) level6Summary = "["+level6Summary+"]";
								else level6Summary = "";
								String container6 = level6.getPropertyValue(RepositoryModel.CONTAINER);
								String[] containers6 = container6.trim().replace("  ", " ").split(" ");
								String c6 = containers6[0] != null && containers6[0].length() > 0 ? containers6[0] : "&nbsp;";
								String c66 = "";
								if(containers6.length == 4) {
									if(containers6[2].equals("folder")) c66 = "[F. "+containers6[3]+"]";
									if(containers6[2].equals("package")) c66 = "[P. "+containers6[3]+"]";
								}
								if(c66.equals(secondaryContainer)) c66 = "";
								else secondaryContainer = c66;
								
							}
						}
					}}
				}}
			}}			
		}
		document.close();
        out.flush();
        out.close();
	}
	*/
	protected Map<Long, Entity> getEntityMap(Entity collection) throws InvalidQualifiedNameException, InvalidEntityException {
		Map<Long, Entity> nodeMap = new HashMap<Long, Entity>();
		List<Association> level1Children = collection.getSourceAssociations(RepositoryModel.CATEGORIES, RepositoryModel.ITEMS);
		for(int i=0; i < level1Children.size(); i++) {
			Entity e1 = entityService.getEntity(level1Children.get(i).getTarget());
			level1Children.get(i).setSourceEntity(e1);
			nodeMap.put(e1.getId(), e1);
			List<Association> level2Children = e1.getSourceAssociations(RepositoryModel.CATEGORIES, RepositoryModel.ITEMS);
			for(int j=0; j < level2Children.size(); j++) {
				Entity e2 = entityService.getEntity(level2Children.get(j).getTarget());
				level2Children.get(j).setSourceEntity(e2);
				nodeMap.put(e2.getId(), e2);
				List<Association> level3Children = e2.getSourceAssociations(RepositoryModel.CATEGORIES, RepositoryModel.ITEMS);
				for(int k=0; k < level3Children.size(); k++) {
					Entity e3 = entityService.getEntity(level3Children.get(k).getTarget());
					level3Children.get(k).setSourceEntity(e3);
					nodeMap.put(e3.getId(), e3);
					List<Association> level4Children = e3.getSourceAssociations(RepositoryModel.CATEGORIES, RepositoryModel.ITEMS);
					for(int l=0; l < level4Children.size(); l++) {
						Entity e4 = entityService.getEntity(level4Children.get(l).getTarget());
						level4Children.get(l).setSourceEntity(e4);
						nodeMap.put(e4.getId(), e4);
						List<Association> level5Children = e4.getSourceAssociations(RepositoryModel.CATEGORIES, RepositoryModel.ITEMS);
						for(int m=0; m < level5Children.size(); m++) {
							Entity e5 = entityService.getEntity(level5Children.get(m).getTarget());
							level5Children.get(m).setSourceEntity(e5);
							nodeMap.put(e5.getId(), e5);
							List<Association> level6Children = e5.getSourceAssociations(RepositoryModel.CATEGORIES, RepositoryModel.ITEMS);
							for(int n=0; n < level6Children.size(); n++) {
								Entity e6 = entityService.getEntity(level6Children.get(n).getTarget());
								level6Children.get(n).setSourceEntity(e6);
								nodeMap.put(e6.getId(), e6);
							}
						}
					}
				}
			}
		}
		return nodeMap;
	}
	protected String[] getContainers(String container) {
		container = container.replace("  ", " ").trim();
		String[] values = {"",""};
		String[] containers = new String[6];
		if(container.contains(",")) {
			String[] parts = container.split(",");
			if(parts.length > 0) {
				String[] chunks = parts[0].split(" ");
				if(chunks.length == 2) {
					containers[0] = chunks[0];
					containers[1] = chunks[1];
				}
			}
			if(parts.length > 1) {
				String[] chunks = parts[1].trim().split(" ");
				if(chunks.length == 2) {
					containers[2] = chunks[0];
					containers[3] = chunks[1];
				}
			}
		} else {
			String[] parts = container.split(" ");
			if(parts.length > 0) containers[0] = parts[0];
			if(parts.length > 1) containers[1] = parts[1];
			if(parts.length > 2) containers[2] = parts[2];
			if(parts.length > 3) containers[3] = parts[3];
			if(parts.length > 4) containers[4] = parts[4];
			if(parts.length > 5) containers[5] = parts[5];
		}
		if(containers[0] != null && containers[1] != null) {
			if(containers[0].trim().toLowerCase().equals("folder")) values[1] += "[F. "+containers[1].trim()+"]";
			else if(containers[0].trim().toLowerCase().equals("package")) values[1] += "[P. "+containers[1].trim()+"]";
			else values[0] = containers[0].trim()+" "+ containers[1].trim();
		}
		if(containers[2] != null && containers[3] != null) {
			if(containers[2].trim().toLowerCase().equals("folder")) values[1] += "[F. "+containers[3].trim()+"]";
			if(containers[2].trim().toLowerCase().equals("package")) values[1] += "[P. "+containers[3].trim()+"]";
		}
		if(containers[4] != null && containers[5] != null) {
			if(containers[4].trim().toLowerCase().equals("folder")) values[1] += " [F. "+containers[5].trim()+"]";
			if(containers[4].trim().toLowerCase().equals("package")) values[1] += " [P. "+containers[5].trim()+"]";
		}
		if(values[0] != null) {
			if(values[0].length() == 0 || values[0].equals(primarContainer)) values[0] = "&nbsp;&nbsp;&nbsp;";
			else primarContainer = values[0];
		} else values[0] = "&nbsp;&nbsp;&nbsp;";
		if(values[1] != null) {
			if(values[1].equals(secondaryContainer)) values[1] = "";
			else secondaryContainer = values[1];
		} else values[1] = "";
		return values;
	}
	
	public EntityService getEntityService() {
		return entityService;
	}
	public void setEntityService(EntityService entityService) {
		this.entityService = entityService;
	}
		
}
