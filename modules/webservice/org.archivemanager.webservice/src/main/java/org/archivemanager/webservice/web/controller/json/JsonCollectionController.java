package org.archivemanager.webservice.web.controller.json;

import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.TermQuery;
import org.heed.openapps.InvalidQualifiedNameException;
import org.heed.openapps.QName;
import org.heed.openapps.SystemModel;
import org.heed.openapps.cache.TimedCache;
import org.heed.openapps.data.RestResponse;
import org.heed.openapps.data.Sort;
import org.heed.openapps.dictionary.RepositoryModel;
import org.heed.openapps.entity.Association;
import org.heed.openapps.entity.AssociationSorter;
import org.heed.openapps.entity.BaseEntityQuery;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityQuery;
import org.heed.openapps.entity.EntityResultSet;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.entity.EntitySorter;
import org.heed.openapps.entity.ExportProcessor;
import org.heed.openapps.entity.ImportProcessor;
import org.heed.openapps.entity.InvalidAssociationException;
import org.heed.openapps.entity.InvalidEntityException;
import org.heed.openapps.entity.InvalidPropertyException;
import org.heed.openapps.entity.ModelValidationException;
import org.heed.openapps.entity.Property;
import org.heed.openapps.entity.ValidationResult;
import org.heed.openapps.entity.data.FileImportProcessor;
import org.heed.openapps.entity.data.FormatInstructions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class JsonCollectionController {
	private Logger log = LoggerFactory.getLogger(JsonCollectionController.class);
	@Autowired protected EntityService entityService;
		
	private TimedCache<String,Entity> entityCache = new TimedCache<String,Entity>(60);
	private TimedCache<String,FileImportProcessor> parserCache = new TimedCache<String,FileImportProcessor>(60);
	
	@ResponseBody
	@RequestMapping(value="/collection/items.json", method = RequestMethod.GET)
	public RestResponse<Object> fetchCollectionTreeData(HttpServletRequest req, HttpServletResponse res) throws Exception {
		RestResponse<Object> data = new RestResponse<Object>();
		String parent = req.getParameter("parent");
		String collectionId = req.getParameter("collection");
		if(parent == null || parent.equals("null")) parent = collectionId;
		if(parent == null || parent.equals("null")) {	
			String query = req.getParameter("query");
			EntityQuery eQuery = (query != null) ? new BaseEntityQuery(RepositoryModel.COLLECTION, query, "name_e", true) : new BaseEntityQuery(RepositoryModel.COLLECTION, null, "name_e", true);
			int startRow = (req.getParameter("_startRow") != null) ? Integer.valueOf(req.getParameter("_startRow")) : 0;
			int endRow = (req.getParameter("_endRow") != null) ? Integer.valueOf(req.getParameter("_endRow")) : 75;
			eQuery.setStartRow(startRow);
			eQuery.setEndRow(endRow);
			eQuery.getFields().add("name");
			EntityResultSet collections = entityService.search(eQuery);
			FormatInstructions instructions = new FormatInstructions(false, false, false);
			instructions.setFormat(FormatInstructions.FORMAT_JSON);
			List<Entity> entities = collections.getResults();
			
			EntitySorter entitySorter = new EntitySorter(new Sort(Sort.STRING, SystemModel.NAME.getLocalName(), true));
			
			Collections.sort(entities, entitySorter);
			for(Entity collection : entities) {
				//Property name = collection.getProperty(SystemModel.NAME);
				//buff.append("<node id='"+collection.getId()+"' qname='{openapps.org_repository_1.0}collection' localName='collection' parent='null'><title><![CDATA["+name.toString()+"]]></title></node>");
				data.getResponse().getData().add(entityService.export(instructions, collection));
			}
			if(collections.getResultSize() >= collections.getEndRow()) data.getResponse().setEndRow(collections.getEndRow());
			else data.getResponse().setEndRow(collections.getResultSize());
		} else {
			Entity collection = entityService.getEntity(Long.valueOf(parent));
			List<Association> list = new ArrayList<Association>();
			for(Association assoc : collection.getSourceAssociations(RepositoryModel.CATEGORIES, RepositoryModel.ITEMS)) {
				if(assoc.getTargetEntity() == null) {
					Entity accession = entityService.getEntity(assoc.getTarget());
					assoc.setTargetEntity(accession);
				}
				list.add(assoc);
			}
			FormatInstructions instructions = new FormatInstructions(true, false, false);
			instructions.setFormat(FormatInstructions.FORMAT_JSON);
			for(Association component : list) {	
				data.getResponse().getData().add(entityService.export(instructions, component));
			}
			
		}
		return data;
	}
	@ResponseBody
	@RequestMapping(value="/collection/fetch.json", method = RequestMethod.GET)
	public RestResponse<Object> fetchCollectionAccessionTreeData(HttpServletRequest req, HttpServletResponse response) throws Exception {
		RestResponse<Object> data = new RestResponse<Object>();
		//res.setContentType("text/xml; charset=UTF-8");
		//res.setCharacterEncoding("UTF-8");
		String parent = req.getParameter("parent");
		String collectionId = req.getParameter("collection");
		if(parent == null || parent.equals("null")) parent = collectionId;
		int startRow = (req.getParameter("_startRow") != null) ? Integer.valueOf(req.getParameter("_startRow")) : 0;
		int endRow = (req.getParameter("_endRow") != null) ? Integer.valueOf(req.getParameter("_endRow")) : 75;
		if(parent == null || parent.equals("null")) {	
			String query = req.getParameter("query");
			EntityQuery eQuery = (query != null) ? new BaseEntityQuery(RepositoryModel.COLLECTION, query, "name_e", true) : new BaseEntityQuery(RepositoryModel.COLLECTION, null, "name_e", true);
			eQuery.setStartRow(startRow);
			eQuery.setEndRow(endRow);
			eQuery.getFields().add("name");
			EntityResultSet collections = entityService.search(eQuery);
			for(Entity collection : collections.getResults()) {
				Property name = collection.getProperty(SystemModel.NAME);
				data.getResponse().getData().add(getNodeData(String.valueOf(collection.getId()), "null", name.toString(), "{openapps.org_repository_1.0}collection", "collection"));
				data.getResponse().getData().add(getNodeData(collection.getId()+"-accessions", String.valueOf(collection.getId()), "Accessions", "{openapps.org_repository_1.0}accessions", "accessions"));
				data.getResponse().getData().add(getNodeData(collection.getId()+"-categories", String.valueOf(collection.getId()), "Categories", "{openapps.org_repository_1.0}categories", "categories"));
			}
			data.getResponse().setTotalRows(collections.getResultSize());			
		} else {
			List<Association> list = new ArrayList<Association>();
			if(parent.endsWith("-accessions")) {
				Entity collection = entityService.getEntity(Long.valueOf(parent.substring(0, parent.length()-11)));
				for(Association assoc : collection.getSourceAssociations(RepositoryModel.ACCESSIONS)) {
					assoc.setSortField(RepositoryModel.ACCESSION_DATE);
				}
				//entityService.hydrate(collection);
				FormatInstructions instructions = new FormatInstructions(true);
				instructions.setFormat(FormatInstructions.FORMAT_JSON);
				for(Association assoc : collection.getSourceAssociations(RepositoryModel.ACCESSIONS)) {
					data.getResponse().getData().add(entityService.export(instructions, assoc));
				}
			} else if(parent.endsWith("-categories")) {
				Entity collection = entityService.getEntity(Long.valueOf(parent.substring(0, parent.length()-11)));	
				list = collection.getSourceAssociations(RepositoryModel.ITEMS,RepositoryModel.CATEGORIES);
				for(Association assoc : list) {
					assoc.setTargetEntity(entityService.getEntity(assoc.getTarget()));
				}
			} else {
				Entity collection = entityService.getEntity(Long.valueOf(parent));
				list = collection.getSourceAssociations(RepositoryModel.ITEMS,RepositoryModel.CATEGORIES);
					
			}
			
			AssociationSorter associationSorter = new AssociationSorter(new Sort(Sort.STRING, SystemModel.NAME.toString(), false), entityService);
			
			Collections.sort(list, associationSorter);
			FormatInstructions instructions = new FormatInstructions(true);
			instructions.setFormat(FormatInstructions.FORMAT_JSON);
			for(Association component : list) {	
				if(component.getQName().equals(RepositoryModel.COLLECTION) || component.getQName().equals(RepositoryModel.ACCESSION) || component.getQName().equals(RepositoryModel.CATEGORY))
					data.getResponse().getData().add(entityService.export(instructions, component.getTargetEntity()));
				else {
					Entity targetEntity = component.getTargetEntity() != null ? component.getTargetEntity() : entityService.getEntity(component.getTarget());
					if(!targetEntity.getQName().equals(RepositoryModel.CATEGORY)) {
						boolean hasChildren = targetEntity.getSourceAssociations(RepositoryModel.ITEMS,RepositoryModel.CATEGORIES).size() > 0;
						instructions.setHasChildren(hasChildren);
					} else instructions.setHasChildren(true);
					Object obj = entityService.export(instructions, component);
					data.getResponse().getData().add(obj);
				}
			}
		}
		data.getResponse().setStartRow(startRow);
		data.getResponse().setEndRow(endRow);
		
		response.setHeader( "Pragma", "no-cache" );
		response.setHeader( "Cache-Control", "no-cache" );
		response.setDateHeader( "Expires", 0 );
		
		return data;
	}
	@SuppressWarnings("unchecked")
	@ResponseBody
	@RequestMapping(value="/collection/add.json", method = RequestMethod.POST)
	public RestResponse<Object> add(HttpServletRequest request, HttpServletResponse response) throws Exception {
		RestResponse<Object> data = new RestResponse<Object>();
		String collectionId = request.getParameter("source");
		String sessionKey = request.getParameter("sessionKey");
		if(collectionId != null) {
			if(sessionKey != null && sessionKey.length() > 0) {
				Entity collection = entityService.getEntity(Long.valueOf(collectionId));
				Entity root = entityCache.get(sessionKey);
				entityService.addEntity(root);
				ImportProcessor parser = parserCache.get(sessionKey);
				Collection<Entity> entities = parser.getEntities().values();
				entityService.addEntities(entities);
				Association assoc = new Association(RepositoryModel.CATEGORIES, collection.getId(), root.getId());
				entityService.addAssociation(assoc);
				root.addProperty(SystemModel.NAME, root.getPropertyValue(SystemModel.NAME) + "(import)");
				entityService.updateEntity(root, true);
				ExportProcessor processor = entityService.getExportProcessor(root.getQName().toString());
				data.getResponse().getData().add(processor.export(new FormatInstructions(true), assoc));
			} else {
				String assocQname = request.getParameter("assoc_qname");
				String entityQname = request.getParameter("entity_qname");
				String source = request.getParameter("source");
				QName aQname = QName.createQualifiedName(assocQname);
				QName eQname = QName.createQualifiedName(entityQname);
				Entity entity = entityService.getEntity(request, eQname);
				ValidationResult entityResult = entityService.validate(entity);
				if(entityResult.isValid()) {
					FormatInstructions instructions = new FormatInstructions(true);
					instructions.setFormat(FormatInstructions.FORMAT_JSON);
					if(entity.getId() != null) {
						entityService.updateEntity(entity, true);
						data.getResponse().getData().add(entityService.export(instructions, entity));
					} else {
						Long assocId = entityService.addEntity(Long.valueOf(source), null, aQname, null, entity);
						Association assoc = entityService.getAssociation(assocId);	
						data.getResponse().getData().add(entityService.export(instructions, assoc));
					}
				}
			}
		} else {
			String qnameStr = request.getParameter("qname");
			QName qname = QName.createQualifiedName(qnameStr);
			Entity entity = entityService.getEntity(request, qname);
			ValidationResult result = entityService.validate(entity);
			if(result.isValid()) {	
				entityService.addEntity(entity);							
				
				FormatInstructions instructions = new FormatInstructions(false, true, false);
				instructions.setFormat(FormatInstructions.FORMAT_JSON);
				Map<String,Object> map = (Map<String,Object>)entityService.export(instructions, entity);
				if(qname.equals(RepositoryModel.COLLECTION)) {
					map.put("parent", "null");
					data.getResponse().getData().add(getNodeData(entity.getId()+"-accessions", String.valueOf(entity.getId()), "Accessions", "{openapps.org_repository_1.0}accessions", "accessions"));
					data.getResponse().getData().add(getNodeData(entity.getId()+"-categories", String.valueOf(entity.getId()), "Categories", "{openapps.org_repository_1.0}categories", "categories"));
				}
				data.getResponse().addData(map);
								
				response.setHeader( "Pragma", "no-cache" );
				response.setHeader( "Cache-Control", "no-cache" );
				response.setDateHeader( "Expires", 0 );
			} 
		}
		return data;
	}
	@ResponseBody
	@RequestMapping(value="/collection/update.json", method = RequestMethod.POST)
	public RestResponse<Object> moveCollection(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("id") Long id, @RequestParam("parent") Long parent) throws Exception {
		RestResponse<Object> data = new RestResponse<Object>();
		Entity source = entityService.getEntity(id);
		Entity target = entityService.getEntity(parent);
		Association assoc = source.getTargetAssociation(RepositoryModel.CATEGORIES);
		if(assoc == null) assoc = source.getTargetAssociation(RepositoryModel.ITEMS);
		if(assoc == null) assoc = source.getTargetAssociation(RepositoryModel.ACCESSIONS);
		if(assoc != null && source != null && target != null) {
			assoc.setSource(target.getId());
			entityService.updateAssociation(assoc);
		}
		FormatInstructions instructions = new FormatInstructions(true);
		instructions.setFormat(FormatInstructions.FORMAT_JSON);
		data.getResponse().getData().add(entityService.export(instructions, assoc));
		
		response.setHeader( "Pragma", "no-cache" );
		response.setHeader( "Cache-Control", "no-cache" );
		response.setDateHeader( "Expires", 0 );
		
		return data;
	}
	@ResponseBody
	@RequestMapping(value="/collection/remove.json", method = RequestMethod.POST)
	public RestResponse<Object> removeEntity(HttpServletRequest request, HttpServletResponse response, @RequestParam("id") Long id) throws Exception {
		RestResponse<Object> data = new RestResponse<Object>();
		Entity entity = entityService.getEntity(id);
		entityService.removeEntity(id);

		response.setHeader( "Pragma", "no-cache" );
		response.setHeader( "Cache-Control", "no-cache" );
		response.setDateHeader( "Expires", 0 );
		
		Map<String,Object> record = new HashMap<String,Object>();
		record.put("id", entity.getId());
		record.put("uid", entity.getUid());
		data.getResponse().addData(record);
		
		return data;
	}
	@ResponseBody
	@RequestMapping(value="/collection/propagate.json", method = RequestMethod.POST)
	public RestResponse<Object> propagate(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("id") Long id, @RequestParam("qname") String type, @RequestParam("value") String value) throws Exception {
		RestResponse<Object> data = new RestResponse<Object>();
		if(type.equals("qname")) {
			cascadeQName(id, RepositoryModel.ITEMS, QName.createQualifiedName(value));
			cascadeQName(id, RepositoryModel.CATEGORIES, QName.createQualifiedName(value));
		} else {
			entityService.cascadeProperty(id, RepositoryModel.ITEMS, QName.createQualifiedName(type), (Serializable)value);
		}
		data.getResponse().addMessage("Successfully updated child properties.");
		return data;
	}
	@ResponseBody
	@RequestMapping(value="/reindex/{id}", method = RequestMethod.GET)
	public RestResponse<Object> reindex(HttpServletRequest req, HttpServletResponse res, @PathVariable("id") long id) throws Exception {
		RestResponse<Object> data = new RestResponse<Object>();
		
		EntityQuery query = new BaseEntityQuery(RepositoryModel.ITEM);
		query.setNativeQuery(new TermQuery(new Term("path", String.valueOf(id))));
		query.setEndRow(10000);
		EntityResultSet results = entityService.search(query);
		log.info("reindexing "+results.getResults().size()+" entities");
		for(Entity item : results.getResults()) {
			entityService.indexEntity(item.getId());
		}
		data.getResponse().getMessages().add("reindexed "+results.getResults().size()+" entities");
		log.info("reindexed "+results.getResults().size()+" entities");
		return data;
	}
	@RequestMapping(value="/collection/import/upload.json", method = RequestMethod.POST)
	public ModelAndView upload(HttpServletRequest req, HttpServletResponse res) throws Exception {
		String mode = null;
		String sessionKey = "";
		try {
			// Create a new file upload handler
			ServletFileUpload upload = new ServletFileUpload();
			// Parse the request
			FileItemIterator iter = upload.getItemIterator(req);
			while(iter.hasNext()) {
			    FileItemStream item = iter.next();
			    String name = item.getFieldName();
			    InputStream stream = item.openStream();
			    if(item.isFormField()) {
			    	if(name.equals("mode")) mode = Streams.asString(stream);
			    	//System.out.println("Form field " + name + " with value " + Streams.asString(tmpStream) + " detected.");
			    } else {
			    	FileImportProcessor parser = (FileImportProcessor)entityService.getImportProcessors(mode).get(0);
					if(parser != null) {
						parser.process(stream, null);
						sessionKey = java.util.UUID.randomUUID().toString();
						entityCache.put(sessionKey, parser.getRoot());
						parserCache.put(sessionKey, parser);
					}
			    }
			}
		} catch(FileUploadException e) {
			e.printStackTrace();
		}		
		res.setContentType("text/html");
		res.getWriter().print("<script language='javascript' type='text/javascript'>window.top.window.uploadComplete('"+sessionKey+"');</script>");
		return null;
	}
	@ResponseBody
	@RequestMapping(value="/collection/import/fetch.json", method = RequestMethod.GET)
	public RestResponse<Object> fetch(HttpServletRequest req, HttpServletResponse res, @RequestParam("session") String sessionKey) throws Exception {
		RestResponse<Object> data = new RestResponse<Object>();
		String source = req.getParameter("source");
		Entity root = entityCache.get(sessionKey);
		ImportProcessor parser = parserCache.get(sessionKey);
		if(source != null && source.equals("detail")) {
			String id = req.getParameter("id");
			if(id != null) {				
				Entity node = parser.getEntityById(id);
				if(node != null) {
					Map<String,Object> map = new HashMap<String,Object>();
					map.put("id", id);
					map.put("localName", node.getQName().getLocalName());
					//buff.append("<properties>");
					List<Map<String,Object>> properties = new ArrayList<Map<String,Object>>();
					for(Property prop : node.getProperties()) {
						Map<String,Object> property = new HashMap<String,Object>();
						property.put("id", id);
						property.put("name", prop.getQName().getLocalName());
						property.put("value", prop.getValue());
						properties.add(property);
					}
					map.put("properties", properties);
					/*
					buff.append("<containers>");
					for(Association prop : node.getTargetAssociations(SystemModel.CATEGORIES,SystemModel.ITEMS)) {
						buff.append("<node name='"+prop.getSourceNode().getProperty(RepositoryModel.CONTAINER_TYPE)+"'>");
						buff.append("<value><![CDATA["+toXmlCdata(prop.getSourceNode().getProperty(RepositoryModel.CONTAINER_VALUE))+"]]></value>");
						buff.append("</node>");
					}
					buff.append("</containers>");
					
					buff.append("<notes>");
					for(Association propAssoc : node.getSourceAssociations(SystemModel.NOTES)) {
						Entity prop = parser.getEntityById(String.valueOf(propAssoc.getTarget()));
						buff.append("<node name='"+prop.getProperty(SystemModel.NOTE_TYPE)+"'>");
						buff.append("<value><![CDATA["+prop.getProperty(SystemModel.NOTE_CONTENT)+"]]></value>");
						buff.append("</node>");
					}
					buff.append("</notes>");
					buff.append("<names>");
					for(Association propAssoc : node.getSourceAssociations(ClassificationModel.NAMED_ENTITIES)) {
						Entity prop = parser.getEntityById(String.valueOf(propAssoc.getTarget()));
						buff.append("<node name='"+prop.getQName().getLocalName()+"' value='"+prop.getProperty(SystemModel.NAME)+"' />");
					}
					buff.append("</names>");
					buff.append("<subjects>");
					for(Association propAssoc : node.getSourceAssociations(ClassificationModel.SUBJECTS)) {
						Entity prop = parser.getEntityById(String.valueOf(propAssoc.getTarget()));
						buff.append("<node name='"+prop.getQName().getLocalName()+"' value='"+prop.getProperty(SystemModel.NAME)+"' />");
					}
					buff.append("</subjects>");
					buff.append("</node>");
					 */
					data.getResponse().addData(map);
				}
			}
		} else {
			if(root != null) {
				List<Association> associations = root.getSourceAssociations();
				List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();
				Map<String,Object> map = new HashMap<String,Object>();
				if(associations.size() > 0) {					
					map.put("id", root.getUid());
					map.put("title", root.getProperty(SystemModel.NAME).getValue());
					map.put("parent", "null");
				} else {
					map.put("id", "0");
					map.put("title", "Collection");
					map.put("parent", "null");
				}
				list.add(map);
				printNodeTaxonomy(list, root, parser);
				for(Object o : list) {
					data.getResponse().addData(o);
				}
			}
			//System.out.println(buff.toString());
			//return response(buff.toString(), res.getWriter());
		} 
		return data;
	}
	protected void printNodeTaxonomy(List<Map<String,Object>> list, Entity node, ImportProcessor parser) throws InvalidEntityException {
		List<Association> children = node.getChildren();
		for(Association assoc : children) {
			Entity child = parser.getEntityById(assoc.getTargetUid());
			Property titleProp = child.getProperty(SystemModel.NAME);
			String title = titleProp != null ? titleProp.toString() : child.getQName().getLocalName();
			Map<String,Object> property = new HashMap<String,Object>();
			if(child.getChildren().size() > 0) {				
				property.put("id", child.getUid());
				property.put("title", title);
				property.put("parent", node.getUid());
				printNodeTaxonomy(list,child, parser);
			} else {
				property.put("id", child.getUid());
				property.put("title", title);
				property.put("parent", node.getUid());
				property.put("isFolder", false);
			}
			list.add(property);
		}
	}
	protected void cascadeQName(Long id, QName association, QName qname) throws InvalidAssociationException, InvalidEntityException, InvalidPropertyException, ModelValidationException, InvalidQualifiedNameException {
		Entity entity = entityService.getEntity(id);
		for(Association assoc : entity.getSourceAssociations(association)) {
			Entity targetEntity = entityService.getEntity(assoc.getTarget());
			if(association.equals(RepositoryModel.CATEGORIES)) {
				assoc.setQname(RepositoryModel.ITEMS);
				entityService.updateAssociation(assoc);
				targetEntity.addProperty(RepositoryModel.CATEGORY_LEVEL, "item");
			}
			targetEntity.setQname(qname);
			entityService.updateEntity(targetEntity, true);
			cascadeQName(targetEntity.getId(), association, qname);
		}
	}
	protected Map<String,Object> getNodeData(String id, String parent, String name, String qname, String localName) {
		Map<String,Object> nodeData = new HashMap<String,Object>();
		nodeData.put("id", id);
		nodeData.put("parent", parent);
		nodeData.put("name", name);
		nodeData.put("title", name);
		nodeData.put("qname", qname);
		nodeData.put("localName", localName);		
		return nodeData;
	}
}
