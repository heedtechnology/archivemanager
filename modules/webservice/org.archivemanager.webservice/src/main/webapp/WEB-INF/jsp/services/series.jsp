<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>	
<%
response.setContentType("text/xml");
response.setHeader("Cache-Control", "no-cache");
response.setHeader("pragma","no-cache");
%>
<response>
	<status><c:out value="${status}" /></status>
	<message></message>
	<data>
		<c:forEach items="${collections}" var="collection">
			<node id="<c:out value="${collection.id}" />" type="collection" parent="null">
				<name><c:out value="${collection.title}" /></name>
			
				<c:forEach items="${collection.series}" var="series">
					<node id="<c:out value="${series.id}" />" type="series" parent="<c:out value="${collection.id}" />" isFolder="false">
						<name><c:out value="${series.title}" /></name>
						<description><c:out value="${series.description}" /></description>
					</node>
				</c:forEach>
			</node>
		</c:forEach>
	</data>
</response>