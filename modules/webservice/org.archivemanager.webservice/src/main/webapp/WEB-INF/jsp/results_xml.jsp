<%@page contentType="text/html;charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>	
<%
response.setContentType("text/xml");
response.setHeader("Cache-Control", "no-cache");
response.setHeader("pragma","no-cache");
response.setDateHeader ("Expires", -1);
%>
<response>
	<status><c:out value="${status}" /></status>
	<message><c:out value="${message}" /></message>
	<data>
		<c:out value="${data}" escapeXml="false" />
	</data>
</response>