package org.archivemanager.content.endpoint;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

import org.archivemanager.content.util.DirectorySettingsUtility;


public class ContentBrowsingServlet extends HttpServlet {
	private static final long serialVersionUID = -1697073773256413712L;
	private String directory;
	private Cache cache;
	
	@Override
	public void init() throws ServletException {		
		super.init();
		this.directory = getServletConfig().getInitParameter("directory");
		CacheManager manager = CacheManager.getInstance();
		cache = manager.getCache("globalCache");
	}
	
	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {		
		StringWriter buff = new StringWriter();
		String uid = req.getParameter("uid");
		String requestName = req.getRequestURI();
		if(requestName.endsWith("browse.xml")) {
			File parent = null;
			if(uid != null && uid.length() > 0) {
				String path = getPath(uid);
				if(path != null) {
					System.out.println("found match for uid:"+uid+" path:"+path);
					parent = new File(path);
				} else {
					scanForUids(new File(directory));
					path = getPath(uid);
					if(path != null) {
						System.out.println("found match for uid:"+uid+" path:"+path);
						parent = new File(path);
					} else {
						throw new IOException("no match was found for uid:"+uid);
					}
					/*
					DirectorySettings settings = DirectorySettingsUtility.getDirectorySettings(new File(directory), uid);
					if(settings != null) {
						path = settings.getPath();
						if(path != null) parent = new File(path);
					}
					*/
				}
			} else {
				parent = new File(directory);
				scanForUids(parent);
			}
			if(parent != null && parent.exists()) {
				File[] directoryArray = parent.listFiles(DirectorySettingsUtility.directoryFilter);
				for(File child : directoryArray) {
					File settingsFile = DirectorySettingsUtility.getSettingsFile(child);
					if(settingsFile != null) {
						String childUid = DirectorySettingsUtility.getUUID(settingsFile);
						cache.put(new Element(childUid, child.getPath()));
						boolean hasChildren = child.listFiles(DirectorySettingsUtility.directoryFilter).length > 0;
						buff.append("<node uid='"+childUid+"' localName='folder'>");
						buff.append("<path><![CDATA["+child.getPath()+"]]></path>");
						buff.append("<name><![CDATA["+child.getName()+"]]></name>");
						buff.append("<children>"+hasChildren+"</children>");
						buff.append("</node>");
					}
				}
			}
			response(0, "", buff.toString(), res.getWriter());
		} else if(requestName.endsWith("get.xml")) { 
			String path = getPath(uid);
			if(path == null || path.length() == 0) {
				DirectorySettings settings = DirectorySettingsUtility.getDirectorySettings(new File(directory), uid);
				if(settings != null) {
					path = settings.getPath();
				}
			}
			if(path != null && path.length() > 0) {
				File directory = new File(path);
				DirectorySettings settings = DirectorySettingsUtility.getDirectorySettings(directory);
				buff.append("<node uid='"+uid+"' qname='{openapps.org_content_1.0}folder' localName='folder'>");
				buff.append("<name><![CDATA["+directory.getName()+"]]></name>");
				buff.write("<path><![CDATA["+directory.getPath()+"]]></path>");
				if(settings.getFiles().size() > 0) {
					buff.append("<files>");
					for(FileSettings fileSettings : settings.getFiles()) {								
						if(!fileSettings.isDeleted()) {
							buff.write("<file uid='"+fileSettings.getUid()+"' size='"+fileSettings.getSize()+"'>");
							buff.write("<path><![CDATA["+fileSettings.getPath()+"]]></path>");
							buff.write("<name><![CDATA["+fileSettings.getName()+"]]></name>");
							buff.write("</file>");
						}
					}
					buff.append("</files>");
				}
				buff.append("</node>");
			}
			response(0, "", buff.toString(), res.getWriter());
		} else if(requestName.endsWith("stream.xml")) {
			String path = getPath(uid);
			if(path != null && path.length() > 0) {
				File file = new File(path);
				pipe(new FileInputStream(file), res.getOutputStream());
			} else {
				FileSettings settings = DirectorySettingsUtility.getFileSettings(new File(directory), uid);
				if(settings != null) {
					File file = new File(settings.getPath());
					pipe(new FileInputStream(file), res.getOutputStream());
				}
			}
		}
	}
	
	protected String getPath(String uid) {
		Element element = cache.get(uid);
		if(element != null) return (String)element.getObjectValue();
		else {
			File file = new File(directory);
			scanForUids(file);
			element = cache.get(uid);
			if(element != null) return (String)element.getObjectValue();
		}
		return null;
	}
	protected void scanForUids(File parent) {
		File[] directoryArray = parent.listFiles(DirectorySettingsUtility.directoryFilter);
		for(File child : directoryArray) {
			File settingsFile = DirectorySettingsUtility.getSettingsFile(child);
			if(settingsFile != null) {
				String childUid = DirectorySettingsUtility.getUUID(settingsFile);
				cache.put(new Element(childUid, child.getPath()));
				scanForUids(child);
			}
		}
	}
	protected void response(int status, String message, String data, PrintWriter out) {
		out.write("<response><status>"+status+"</status><message><![CDATA["+message+"]]></message><data>"+data+"</data></response>");
	}
	protected static void pipe(InputStream in, OutputStream out) throws IOException {
		byte buffer[] = new byte[8192];
		while(true){
			int len = in.read(buffer,0,8192);
			if(len < 0) break;
			out.write(buffer,0,len);
		}
	}
}
