package org.archivemanager.content.client.file.form;

import org.archivemanager.content.client.data.NativeContentModel;

import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.PasswordItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;

public class DatasourceForm extends DynamicForm {
	private NativeContentModel model = new NativeContentModel();
	
	public DatasourceForm(String width) {
		setWidth(width);
		setHeight(250);
		setCellPadding(5);
		setNumCols(2);
		setBorder("1px solid #A7ABB4;");
		setColWidths(100,"*");
				
		StaticTextItem idItem = new StaticTextItem("id", "ID");
		
		TextItem nameField = new TextItem("name", "Name");
		nameField.setWidth(width);
				
		TextItem urlField = new TextItem("url", "URL");
		urlField.setWidth(width);
				
		SelectItem protocolField = new SelectItem("protocol", "Type");
		protocolField.setWidth(200);
		protocolField.setValueMap(model.getDataSourceTypes());
				
		TextItem domainField = new TextItem("domain", "Domain");
		domainField.setWidth(200);
		
		TextItem usernameField = new TextItem("username", "Username");
		usernameField.setWidth(200);
		
		PasswordItem passwordField = new PasswordItem("password", "Password");
		passwordField.setWidth(200);
				
		TextAreaItem descField = new TextAreaItem("description", "Description");
		descField.setWidth(width);
		descField.setHeight(75);
			
		setFields(idItem,nameField,urlField,protocolField,domainField, usernameField,passwordField,descField);
		
	}
}
