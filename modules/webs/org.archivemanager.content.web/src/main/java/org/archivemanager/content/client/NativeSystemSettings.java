package org.archivemanager.content.client;


public class NativeSystemSettings {
	
	
	public String getApplicationName() { 
		return getNativeApplicationName();
	}	
	private final native String getNativeApplicationName() /*-{
        return $wnd.oa_application_id;
	}-*/;
}
