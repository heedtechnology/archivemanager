package org.archivemanager.content.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.DOM;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.rpc.HandleErrorCallback;
import com.smartgwt.client.rpc.RPCManager;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.Positioning;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.layout.VLayout;

import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.OpenApps;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.OpenAppsEventHandler;


public class ContentManagerEntryPoint implements EntryPoint {
	private OpenApps settings = new OpenApps();	
	private static final int height = 925;
	private static final int width = 1200;
		
	private FileSystemManager filesystemManager;
		
	private String mode = "classification";
	
	public void onModuleLoad() { 
		VLayout mainLayout = new VLayout();
		mainLayout.setHeight(settings.getHeight());  
		mainLayout.setWidth(settings.getWidth());
		mainLayout.setBorder("1px solid #A7ABB4");
		mainLayout.setOverflow(Overflow.HIDDEN);
        		        
        filesystemManager = new FileSystemManager(this);
        mainLayout.addMember(filesystemManager);
        
		EventBus.addHandler(org.heed.openapps.gwt.client.OpenAppsEvent.TYPE, new OpenAppsEventHandler()     {
	        @Override
	        public void onEvent(OpenAppsEvent event) {	            
	        	//if(event.isType(EventTypes.CRAWL_START)) toolbar.showMessage("progress", "starting crawling...");
	        	/*
	        	else if(event.isType(EventTypes.FILE_SELECTED)) toolbar.progressStart("loading file....");
	            else if(event.isType(EventTypes.CRAWL_END) || event.isType(EventTypes.FILE_RECEIVED)) {
	            	toolbar.progressEnd();
	            	if(event.getMessage() != null) toolbar.showMessage(event.getMessage());
	            } else if(event.isType(EventTypes.MESSAGE)) toolbar.setProgressLabel(event.getMessage());
	            else if(event.isType(CrawlingEventTypes.HIDE_PROGRESS)) toolbar.hideProgress();
	            else if(event.isType(CrawlingEventTypes.HIDE_PROGRESS_LABEL)) toolbar.hideProgressLabel();
	            
	        	if(event.isType(EventTypes.FILE_SELECTED)) toolbar.showMessage("progress", "loading file....");
	        	else if(event.isType(EventTypes.FILE_RECEIVED)) {
	        		toolbar.hideMessage();
	        		toolbar.hideProgress();
	        	}
	        	*/
	        }
	    });
		RPCManager.setHandleErrorCallback(new HandleErrorCallback() {
			@Override
			public void handleError(DSResponse response, DSRequest request) {
				int httpCode = response.getHttpResponseCode();
				if(httpCode == 404) SC.warn("Error contacting the server, please check your internet connection and try again.");
			}
		});
        
		filesystemManager.search("", null);
		
		mainLayout.setHtmlElement(DOM.getElementById("gwt"));
        mainLayout.setPosition(Positioning.RELATIVE);
        mainLayout.draw();
	}
	
	private final native void closeLoader() /*-{
    	return $wnd.closeLoader();
	}-*/;
}
