package org.archivemanager.content.client.file;

import org.archivemanager.content.client.data.NativeContentModel;
import org.heed.openapps.gwt.client.event.AddListener;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;

public class AddDatasourceWindow extends Window {
	private NativeContentModel model = new NativeContentModel();
	private SelectItem contentTypeItem;
	private TextItem nameItem;
	private TextItem urlItem;
	private DynamicForm addDataSourceForm;
	
	private AddListener addListener;
	
	public AddDatasourceWindow() {
		setWidth(500);
		setHeight(170);
		setTitle("Add DataSource");
		setAutoCenter(true);
		setIsModal(true);
		addDataSourceForm = new DynamicForm();
		addDataSourceForm.setCellPadding(7);
		addDataSourceForm.setMargin(3);
		addDataSourceForm.setCellPadding(5);
		addDataSourceForm.setColWidths(70, "*");
		nameItem = new TextItem("name", "Name");
		nameItem.setWidth("*");
					
		urlItem = new TextItem("url", "URL");
		urlItem.setWidth("*");
		
		contentTypeItem = new SelectItem("protocol", "Protocol");
		contentTypeItem.setWidth(175);
		contentTypeItem.setValueMap(model.getDataSourceTypes());
		
		ButtonItem submitItem = new ButtonItem("submit", "Add");
		submitItem.setIcon("/theme/images/icons16/add.png");
		submitItem.setStartRow(false);
		submitItem.setEndRow(false);
		submitItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
				Record record = new Record();
				record.setAttribute("qname", "{openapps.org_content_1.0}datasource");
				record.setAttribute("name", nameItem.getValue());
				record.setAttribute("url", urlItem.getValue());
				record.setAttribute("protocol", contentTypeItem.getValue());
				record.setAttribute("format", "tree");
				addListener.add(record);
			}
		});
		
		addDataSourceForm.setFields(nameItem,urlItem,contentTypeItem,submitItem);
		addItem(addDataSourceForm);
	}

	public void setAddListener(AddListener addListener) {
		this.addListener = addListener;
	}
	
}