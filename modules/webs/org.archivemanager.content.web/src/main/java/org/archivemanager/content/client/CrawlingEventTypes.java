package org.archivemanager.content.client;

import org.heed.openapps.gwt.client.EventTypes;

public class CrawlingEventTypes extends EventTypes {

	public static final int HIDE_PROGRESS = 50;
	public static final int HIDE_PROGRESS_LABEL = 51;
	
}
