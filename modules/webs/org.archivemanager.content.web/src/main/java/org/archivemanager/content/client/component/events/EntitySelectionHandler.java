package org.archivemanager.content.client.component.events;

import com.google.gwt.event.shared.EventHandler;

public interface EntitySelectionHandler extends EventHandler {
	
	void onEntitySelection(org.archivemanager.content.client.component.events.EntitySelectionEvent event); 
	
}
