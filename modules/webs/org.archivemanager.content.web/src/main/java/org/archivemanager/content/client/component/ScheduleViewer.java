package org.archivemanager.content.client.component;

import java.util.LinkedHashMap;

import org.heed.openapps.gwt.client.data.RestUtility;

import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class ScheduleViewer extends VLayout {
	private AddNoteWindow addNoteWindow;
	private ListGrid grid;
	
	private Record selection;
	
	public ScheduleViewer(String width) {
		HLayout toolstrip = new HLayout();
		toolstrip.setWidth(width);
		toolstrip.setHeight(20);
		toolstrip.setBackgroundImage("[SKIN]/cssButton/button_stretch.png");
		toolstrip.setBorder("1px solid #A7ABB4");
		addMember(toolstrip);
		
		Label label = new Label("<label style='font-size:11px;font-weight:bold;color:#4C4C4C;font-family:Arial,Verdana,sans-serif;'>Schedule</label>");
		label.setHeight("100%");
		label.setWidth("100%");
		toolstrip.addMember(label);
		
		HLayout buttons = new HLayout();
		buttons.setHeight(16);
		buttons.setMargin(2);
		buttons.setMembersMargin(3);
		toolstrip.addMember(buttons);
		/*
		ImgButton addButton = new ImgButton();
		addButton.setWidth(16);
		addButton.setHeight(16);
		addButton.setSrc("/theme/images/icons16/add.png");
		addButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				addNoteWindow.show();
			}
		});
		buttons.addMember(addButton);
		*/
		ImgButton refreshButton = new ImgButton();
		refreshButton.setWidth(16);
		refreshButton.setHeight(16);
		refreshButton.setSrc("/theme/images/icons16/refresh.png");
		refreshButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				refresh();
			}
		});
		buttons.addMember(refreshButton);
		/*
		ImgButton delButton = new ImgButton();
		delButton.setWidth(16);
		delButton.setHeight(16);
		delButton.setSrc("/theme/images/icons16/delete.png");
		delButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Record rec = new Record();
				String id = grid.getSelectedRecord().getAttribute("target_id");
				if(id == null) id = grid.getSelectedRecord().getAttribute("id");
				rec.setAttribute("id", id);
				RestUtility.post("/service/entity/remove.xml", rec, new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						grid.removeSelectedData();	
					}						
				});	
			}
		});
		buttons.addMember(delButton);
		*/
		grid = new ListGrid();
		grid.setWidth(width);
		grid.setHeight(40);
		grid.setAutoFitData(Autofit.VERTICAL);
		grid.setAutoFitMaxHeight(200);
		grid.setShowHeader(false);
		grid.setWrapCells(true);
		grid.setFixedRecordHeights(false);
		ListGridField contentField = new ListGridField("lastMessage","Message");		
		grid.setFields(contentField);
		addMember(grid);
		
		addNoteWindow = new AddNoteWindow();
	}
	public void refresh() {
		/*
		if(selection != null) {
			Record criteria = new Record();
			criteria.setAttribute("group", "general");
			criteria.setAttribute("id", selection.getAttribute("id"));
			RestUtility.get("/service/schedule/search.xml", criteria, new DSCallback() {
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					grid.setData(response.getData());
				}						
			});
		}
		*/
	}
	public void select(Record record) {
		String id = record.getAttribute("id");
		if(this.selection == null || (id != null && !this.selection.getAttribute("id").equals(id))) {
			this.selection = record;
			/*
			Record criteria = new Record();
			criteria.setAttribute("group", "general");
			criteria.setAttribute("id", record.getAttribute("id"));
			RestUtility.get("/schedule/service/search.xml", criteria, new DSCallback() {
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					grid.setData(response.getData());
				}						
			});
			*/
		}		
	}
	public void setData(Record[] records) {
		grid.setData(records);
	}
	
	public class AddNoteWindow extends Window {
		public AddNoteWindow() {
			setWidth(520);
			setHeight(255);
			setTitle("Add Note");
			setAutoCenter(true);
			setIsModal(true);
			
			final DynamicForm addForm = new DynamicForm();
			addForm.setCellPadding(5);
			final SelectItem typeItem = new SelectItem("type", "Note Type");
			LinkedHashMap<String,String> valueMap = new LinkedHashMap<String,String>();
			valueMap.put("General", "General");
			valueMap.put("Description", "Description");
			valueMap.put("Inventory", "Inventory");
			valueMap.put("Condition", "Condition");
			valueMap.put("Access Restriction", "Access Restriction");
			valueMap.put("Use Restriction", "Use Restriction");
			typeItem.setValueMap(valueMap);
			typeItem.setWidth(300);
			
			TextAreaItem contentItem = new TextAreaItem("content","Content");
			contentItem.setHeight(150);
			contentItem.setWidth(400);
			
			ButtonItem submitItem = new ButtonItem("submit", "Add");
			submitItem.setIcon("/theme/images/icons16/add.png");
			submitItem.setStartRow(false);
			submitItem.setEndRow(false);
			submitItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					Record record = new Record();
					record.setAttribute("assoc_qname", "openapps_org_system_1_0_notes");
					record.setAttribute("entity_qname", "{openapps_org_system_1_0_note");
					record.setAttribute("source", selection.getAttribute("id"));
					record.setAttribute("type", addForm.getValue("type"));
					record.setAttribute("title", addForm.getValue("title"));
					record.setAttribute("content", addForm.getValue("content"));
					RestUtility.post("/service/entity/associate.xml",record,new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							if(response.getData() != null && response.getData().length > 0) {
								grid.addData(response.getData()[0]);
								addNoteWindow.hide();
							}
						}						
					});
				}			
			});
			
			addForm.setFields(typeItem,contentItem,submitItem);
			addItem(addForm);
		}
	}
}
