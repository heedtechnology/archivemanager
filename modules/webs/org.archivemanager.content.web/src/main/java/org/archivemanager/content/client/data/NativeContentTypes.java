package org.archivemanager.content.client.data;

import java.util.LinkedHashMap;

import com.google.gwt.core.client.JavaScriptObject;
import com.smartgwt.client.util.JSOHelper;


public class NativeContentTypes {

	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getContentTypes() { 
		JavaScriptObject obj = getNativeContentTypes();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>();
	}	
	private final native JavaScriptObject getNativeContentTypes() /*-{
        return $wnd.item_types;
	}-*/;
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getCorrespondenceGenre() { 
		JavaScriptObject obj = getNativeCorrespondenceGenre();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>();
	}	
	private final native JavaScriptObject getNativeCorrespondenceGenre() /*-{
        return $wnd.correspondence_genre;
	}-*/;
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getCorrespondenceForms() { 
		JavaScriptObject obj = getNativeCorrespondenceForms();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>(); 
	}
	private final native JavaScriptObject getNativeCorrespondenceForms() /*-{
    	return $wnd.correspondence_forms;
	}-*/;
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getArtworkForms() { 
		JavaScriptObject obj = getNativeArtworkForms();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>(); 
	}
	private final native JavaScriptObject getNativeArtworkForms() /*-{
    	return $wnd.artwork_forms;
	}-*/;
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getArtworkMedium() { 
		JavaScriptObject obj = getNativeArtworkForms();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>(); 
	}
	private final native JavaScriptObject getNativeArtworkMedium() /*-{
    	return $wnd.artwork_medium;
	}-*/;
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getArtworkGenre() { 
		JavaScriptObject obj = getNativeArtworkGenre();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>(); 
	}
	private final native JavaScriptObject getNativeArtworkGenre() /*-{
    	return $wnd.artwork_genre;
	}-*/;
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getAudioMedium() { 
		JavaScriptObject obj = getNativeAudioMedium();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>(); 
	}
	private final native JavaScriptObject getNativeAudioMedium() /*-{
    	return $wnd.audio_medium;
	}-*/;
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getFinancialGenre() { 
		JavaScriptObject obj = getNativeFinancialGenre();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>(); 
	}
	private final native JavaScriptObject getNativeFinancialGenre() /*-{
    	return $wnd.financial_genre;
	}-*/;
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getJournalsGenre() { 
		JavaScriptObject obj = getNativeJournalsGenre();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>(); 
	}
	private final native JavaScriptObject getNativeJournalsGenre() /*-{
    	return $wnd.journals_genre;
	}-*/;
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getJournalsForms() { 
		JavaScriptObject obj = getNativeJournalsForms();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>(); 
	}
	private final native JavaScriptObject getNativeJournalsForms() /*-{
    	return $wnd.journals_forms;
	}-*/;
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getLegalGenre() { 
		JavaScriptObject obj = getNativeLegalGenre();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>(); 
	}
	private final native JavaScriptObject getNativeLegalGenre() /*-{
    	return $wnd.legal_genre;
	}-*/;
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getManuscriptGenre() { 
		JavaScriptObject obj = getNativeManuscriptGenre();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>(); 
	}
	private final native JavaScriptObject getNativeManuscriptGenre() /*-{
    	return $wnd.manuscript_genre;
	}-*/;
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getManuscriptForms() { 
		JavaScriptObject obj = getNativeManuscriptForms();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>(); 
	}
	private final native JavaScriptObject getNativeManuscriptForms() /*-{
    	return $wnd.manuscript_forms;
	}-*/;
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getMedicalGenre() { 
		JavaScriptObject obj = getNativeMedicalGenre();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>(); 
	}
	private final native JavaScriptObject getNativeMedicalGenre() /*-{
    	return $wnd.medical_genre;
	}-*/;
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getMemorabiliaGenre() { 
		JavaScriptObject obj = getNativeMemorabiliaGenre();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>(); 
	}
	private final native JavaScriptObject getNativeMemorabiliaGenre() /*-{
    	return $wnd.memorabilia_genre;
	}-*/;
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getNotebooksGenre() { 
		JavaScriptObject obj = getNativeNotebooksGenre();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>(); 
	}
	private final native JavaScriptObject getNativeNotebooksGenre() /*-{
    	return $wnd.notebooks_genre;
	}-*/;
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getNotebooksForms() { 
		JavaScriptObject obj = getNativeNotebooksForms();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>(); 
	}
	private final native JavaScriptObject getNativeNotebooksForms() /*-{
    	return $wnd.notebooks_forms;
	}-*/;
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getPhotographsForms() { 
		JavaScriptObject obj = getNativePhotographsForms();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>(); 
	}
	private final native JavaScriptObject getNativePhotographsForms() /*-{
    	return $wnd.photographs_forms;
	}-*/;
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getPrintedGenre() { 
		JavaScriptObject obj = getNativePrintedGenre();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>(); 
	}
	private final native JavaScriptObject getNativePrintedGenre() /*-{
    	return $wnd.printed_genre;
	}-*/;
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getProfessionalGenre() { 
		JavaScriptObject obj = getNativeProfessionalGenre();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>(); 
	}
	private final native JavaScriptObject getNativeProfessionalGenre() /*-{
    	return $wnd.professional_genre;
	}-*/;
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getResearchGenre() { 
		JavaScriptObject obj = getNativeResearchGenre();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>(); 
	}
	private final native JavaScriptObject getNativeResearchGenre() /*-{
    	return $wnd.research_genre;
	}-*/;
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getScrapbooksForms() { 
		JavaScriptObject obj = getNativeScrapbooksForms();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>(); 
	}
	private final native JavaScriptObject getNativeScrapbooksForms() /*-{
    	return $wnd.scrapbooks_forms;
	}-*/;
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getVideoMedium() { 
		JavaScriptObject obj = getNativeVideoMedium();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>(); 
	}
	private final native JavaScriptObject getNativeVideoMedium() /*-{
    	return $wnd.video_medium;
	}-*/;
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getVideoForms() { 
		JavaScriptObject obj = getNativeVideoForms();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>(); 
	}
	private final native JavaScriptObject getNativeVideoForms() /*-{
    	return $wnd.video_forms;
	}-*/;
}
