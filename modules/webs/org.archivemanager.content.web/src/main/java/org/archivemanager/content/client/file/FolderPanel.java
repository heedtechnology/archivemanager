package org.archivemanager.content.client.file;

import org.archivemanager.content.client.data.NativeClassificationModel;
import org.archivemanager.content.client.data.NativeContentModel;
import org.archivemanager.content.client.file.form.FolderForm;
import org.archivemanager.gwt.client.component.BasicContentViewer;
import org.archivemanager.gwt.client.component.FileViewer;
import org.heed.openapps.gwt.client.Component;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.EventTypes;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.component.NamedEntityComponent;
import org.heed.openapps.gwt.client.component.NotesComponent;
import org.heed.openapps.gwt.client.component.SubjectComponent;
import org.heed.openapps.gwt.client.component.Toolbar;
import org.heed.openapps.gwt.client.data.RestUtility;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;


public class FolderPanel extends TabSet implements Component {
	private NativeClassificationModel classificationModel = new NativeClassificationModel();
	private NativeContentModel contentModel = new NativeContentModel();
	private FolderForm localForm;
	private BasicContentViewer contentViewer;
	private FileViewer fileViewer;
	private NamedEntityComponent nameComponent;
	private SubjectComponent subjectComponent;
	private NotesComponent noteComponent;
	
	
	public FolderPanel() {
		setWidth100();
		setHeight100();
		
		VLayout mainLayout = new VLayout();
		mainLayout.setMembersMargin(5);
		Tab contentTab = new Tab("Folder");
		contentTab.setPane(mainLayout);
		addTab(contentTab);
		                
        HLayout bottomLayout = new HLayout();
        bottomLayout.setMembersMargin(5);
        mainLayout.addMember(bottomLayout);
        
        VLayout leftColumn = new VLayout();
        leftColumn.setWidth100();
        leftColumn.setMembersMargin(5);
        bottomLayout.addMember(leftColumn);
        
        localForm = new FolderForm("100%");
        leftColumn.addMember(localForm);
        
		VLayout rightColumn = new VLayout();
		rightColumn.setWidth(385);
		rightColumn.setMembersMargin(5);
		bottomLayout.addMember(rightColumn);
        
        //scheduleViewer = new ScheduleViewer("405");
        //leftColumn.addMember(scheduleViewer);
        
        nameComponent = new NamedEntityComponent("385", true, true, true, classificationModel.getNamedEntityFunction(), classificationModel.getNamedEntityRole());
        rightColumn.addMember(nameComponent);
		
        subjectComponent = new SubjectComponent("385", true);
		rightColumn.addMember(subjectComponent);
        
		noteComponent = new NotesComponent("folder", "385", contentModel.getNoteTypes());
		rightColumn.addMember(noteComponent);
		
		Canvas spacer2 = new Canvas();
		spacer2.setWidth100();
		spacer2.setHeight100();
		rightColumn.addMember(spacer2);
		
        VLayout viewerLayout = new VLayout();
        viewerLayout.setWidth100();
        viewerLayout.setHeight100();
		Tab viewerTab = new Tab("Files");
		viewerTab.setPane(viewerLayout);
		addTab(viewerTab);
		
		fileViewer = new FileViewer();
		viewerLayout.addMember(fileViewer);
		
		TabSet tabs = new TabSet();
		tabs.setHeight100();
        viewerLayout.addMember(tabs);
        
        Tab viewTab = new Tab("Viewer");
        viewTab.setPrompt("Viewer");
        contentViewer = new BasicContentViewer();
		//contentViewer.setMargin(5);
        viewTab.setPane(contentViewer);		
		tabs.addTab(viewTab);
				
		Tab textTab = new Tab("Content");
		textTab.setPrompt("Content");
		DynamicForm textContentForm = new DynamicForm();
		textContentForm.setColWidths(90,"*");
		TextAreaItem contentItem = new TextAreaItem("content", "Text Content");
		contentItem.setHeight(180);
		contentItem.setWidth(655);
		textContentForm.setItems(contentItem);
		textTab.setPane(textContentForm);		
		tabs.addTab(textTab);
	}
	
	public void fetchData(Criteria criteria, DSCallback callback) {
		localForm.fetchData(criteria, callback);
	}
	public void select(final Record record) {
		//scheduleViewer.select(record);
		fileViewer.select(record);
		localForm.editRecord(record);
		contentViewer.select(record);
		nameComponent.select(record);
		subjectComponent.select(record);
		noteComponent.select(record);
	}
	public void refresh() {
		//scheduleViewer.refresh();
	}
	public Record getData() {
		Record values = localForm.getValuesAsRecord();
		Record criteria = new Record();
		criteria.setAttribute("qname", "openapps_org_content_1_0_folder");
		for(String key : values.getAttributes()) {
			if(!key.equals("files")) criteria.setAttribute(key, values.getAttribute(key));
		}
		return criteria;
	}
	public void save(DSCallback callback) {
		//localForm.saveData(callback);
		Record values = localForm.getValuesAsRecord();
		Record criteria = new Record();
		for(String key : values.getAttributes()) {
			if(!key.equals("files")) criteria.setAttribute(key, values.getAttribute(key));
		}
				
		RestUtility.post("/content/update.xml", criteria, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				Record rec = new Record();
				rec.setAttribute("status", Toolbar.SUCCESS_ICON);
				rec.setAttribute("message", "Folder saved successfully");
				EventBus.fireEvent(new OpenAppsEvent(EventTypes.MESSAGE, rec));
			}						
		});
	}

}
