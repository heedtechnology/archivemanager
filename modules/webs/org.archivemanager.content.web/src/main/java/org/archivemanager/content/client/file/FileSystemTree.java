package org.archivemanager.content.client.file;

import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.EventTypes;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.data.ContentServiceDS;

import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.tree.TreeGrid;


public class FileSystemTree extends TreeGrid {
		
	
	public FileSystemTree() {
		setHeight100();
		setWidth100();
		setShowHeader(false);
		setBorder("0px");
		setWrapCells(true);
		setFixedRecordHeights(false);
		setCanReorderRecords(false);  
        setCanAcceptDroppedRecords(false);
        setDataSource(ContentServiceDS.getInstance().getFolderBrowseDatasource());
        
        addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				EventBus.fireEvent(new OpenAppsEvent(EventTypes.SELECTION, event.getRecord()));	
			}			
		}); 
        
	}
	
}
