package org.archivemanager.content.client.file;

import org.archivemanager.content.client.file.form.DatasourceForm;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;


public class DataSourcePanel extends TabSet {
	//private ScheduleViewer scheduleViewer;
	private DatasourceForm localForm;
	
		
	public DataSourcePanel() {
		setWidth100();
		setHeight100();
		
		VLayout mainLayout = new VLayout();
		mainLayout.setMembersMargin(5);
		Tab contentTab = new Tab("DataSource");
		contentTab.setPane(mainLayout);
		addTab(contentTab);
		
		HLayout topRightLayout = new HLayout();
        topRightLayout.setMembersMargin(5);
        topRightLayout.setHeight(250);
        mainLayout.addMember(topRightLayout);
        
        final Canvas canvas = new Canvas();
        canvas.setWidth("100%");       
        canvas.setHeight("100%"); 
        topRightLayout.addMember(canvas);
        
        localForm = new DatasourceForm("375");
        canvas.addChild(localForm);
        
        //scheduleViewer = new ScheduleViewer("375");
        //topRightLayout.addMember(scheduleViewer);
        
        HLayout rowTwoLeftLayout = new HLayout();
        rowTwoLeftLayout.setBorder("1px solid #A7ABB4;");
        rowTwoLeftLayout.setMembersMargin(5);
        rowTwoLeftLayout.setWidth("447");
        rowTwoLeftLayout.setHeight(30);
        
        mainLayout.addMember(rowTwoLeftLayout);
	}
	
	public void fetchData(Criteria criteria, DSCallback callback) {
		localForm.fetchData(criteria, callback);
	}
	public void select(Record record) {
		//scheduleViewer.select(record);
		localForm.editRecord(record);
	}
	public void refresh() {
		//scheduleViewer.refresh();
	}
	public Record getData() {
		Record record = localForm.getValuesAsRecord();
		record.setAttribute("qname", "openapps_org_content_1_0_datasource");
		return record;
	}
}
