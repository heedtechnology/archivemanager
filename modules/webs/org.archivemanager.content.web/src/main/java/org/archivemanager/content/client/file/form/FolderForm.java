package org.archivemanager.content.client.file.form;

import org.archivemanager.content.client.data.NativeContentModel;

import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;

public class FolderForm extends DynamicForm {
	private NativeContentModel contentModel = new NativeContentModel();
	
	public FolderForm(String width) {
		setWidth(width);
		setHeight(250);
		setPadding(5);
		setCellPadding(5);
		setNumCols(2);
		setBorder("1px solid #A7ABB4;");
		setColWidths(140,"*");
		
		StaticTextItem idItem = new StaticTextItem("id", "ID");		
		
		SelectItem typeField = new SelectItem("content_type", "Content Type");
		typeField.setWidth(150);
		typeField.setValueMap(contentModel.getContentTypes());
		
		TextItem titleField = new TextItem("title", "Title");
		titleField.setWidth("*");
				
		TextItem collectionField = new TextItem("collection", "Collection");
		collectionField.setWidth("*");
								
		TextItem contextualField = new TextItem("contextual_information", "Contextual Information");
		contextualField.setWidth("*");
		
		TextItem authorField = new TextItem("author", "Author");
		authorField.setWidth("*");
				
		TextItem locationField = new TextItem("location", "Location");
		locationField.setWidth("*");
			
		TextItem staffField = new TextItem("staff", "Staff");
		staffField.setWidth("*");
		
		TextItem dateField = new TextItem("date", "Date");
		dateField.setWidth(150);
		
		TextItem dimensionsField = new TextItem("dimensions", "Dimensions");
		dimensionsField.setWidth(150);
		
		TextAreaItem descriptionField = new TextAreaItem("description", "Description");
		descriptionField.setWidth("*");
		
		StaticTextItem created = new StaticTextItem("created", "Created");
		StaticTextItem modified = new StaticTextItem("modified", "Modified");
		/*
		StaticTextItem mimetypeField = new StaticTextItem("mimetype", "Mime Type");
		
		StaticTextItem modifier = new StaticTextItem("modifier", "Modifier");
		StaticTextItem modified = new StaticTextItem("modified", "Modified");
		*/
		setFields(idItem,typeField,titleField,collectionField,contextualField,authorField,locationField,
				staffField,descriptionField,dateField,dimensionsField,created, modified);
						
	}
}