package org.archivemanager.content.client.component;

import java.util.HashMap;
import java.util.Map;

import org.archivemanager.content.client.component.events.EntitySelectionEvent;
import org.archivemanager.content.client.component.events.EntitySelectionHandler;
import org.archivemanager.content.client.data.NativeContentModel;
import org.heed.openapps.gwt.client.data.ContentServiceDS;
import org.heed.openapps.gwt.client.data.RestUtility;

import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.event.shared.GwtEvent.Type;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.SpinnerItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tree.TreeGrid;

public class FileAssociationComponent extends VLayout {
	private Record accession;
	private NativeContentModel contentModel = new NativeContentModel();
	private ListGrid grid;
	private AddItemWindow addItemWindow;
	
	private Map<HandlerRegistration, EntitySelectionHandler> handlers = new HashMap<HandlerRegistration, EntitySelectionHandler>();
	
	public FileAssociationComponent(String title, String width, boolean upload, boolean associate, boolean delete) {
		setWidth(width);
		setHeight100();
		setMargin(10);
		setBorder("1px solid #C0C3C7");
		
		HLayout toolstrip = new HLayout();
		toolstrip.setWidth("100%");
		toolstrip.setHeight(20);
		toolstrip.setBackgroundImage("[SKIN]/cssButton/button_stretch.png");
		//toolstrip.setBorder("1px solid #A7ABB4");
		addMember(toolstrip);
		
		Label label = new Label("<label style='font-size:11px;font-weight:bold;color:#4C4C4C;font-family:Arial,Verdana,sans-serif;'>"+title+"</label>");
		label.setHeight("100%");
		label.setWidth("100%");
		toolstrip.addMember(label);
		
		HLayout buttons = new HLayout();
		buttons.setHeight(16);
		buttons.setMargin(2);
		buttons.setMembersMargin(3);
		toolstrip.addMember(buttons);
		
		ImgButton addButton = new ImgButton();
		addButton.setWidth(16);
		addButton.setHeight(16);
		addButton.setSrc("/theme/images/icons16/add.png");
		addButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				addItemWindow.show();
			}
		});
		buttons.addMember(addButton);
		ImgButton delButton = new ImgButton();
		delButton.setWidth(16);
		delButton.setHeight(16);
		delButton.setSrc("/theme/images/icons16/delete.png");
		delButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Record rec = new Record();
				String id = grid.getSelectedRecord().getAttribute("id");
				rec.setAttribute("id", id);
				RestUtility.post("/service/association/remove.xml", rec, new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						grid.removeSelectedData();	
					}						
				});	
			}
		});
		buttons.addMember(delButton);
		
		grid = new ListGrid();
		grid.setWidth("100%");
		grid.setHeight("100%");
		grid.setBorder("0px");
		grid.setShowHeader(false);
		ListGridField typeField = new ListGridField("type","Type");
		typeField.setWidth(100);
		ListGridField nameField = new ListGridField("name","Name");
		ListGridField sizeField = new ListGridField("size","Size");
		sizeField.setWidth(50);
		grid.setFields(typeField, nameField,sizeField);
		grid.addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				EntitySelectionEvent evt = new EntitySelectionEvent();
				evt.setRecord(grid.getSelectedRecord());
				for(EntitySelectionHandler handler : handlers.values()) {
					handler.onEntitySelection(evt);
				}
			}
		});
        addMember(grid);
                
        addItemWindow = new AddItemWindow();
	}
	public void setData(Record[] records) {
		grid.setData(records);
	}
	public void select(Record record) {
		this.accession = record;
		try {
			Record source_associations = record.getAttributeAsRecord("files");
			if(source_associations != null) {
				setData(source_associations.getAttributeAsRecordArray("file"));
			} else setData(new Record[0]);
			
		} catch(ClassCastException e) {
			setData(new Record[0]);
		}
	}
	public HandlerRegistration addEntitySelectionHandler(EntitySelectionHandler handler) {
		HandlerRegistration reg = doAddHandler(handler, new Type<EntitySelectionHandler>());
		if(!handlers.containsKey(reg)) handlers.put(reg, handler);
		return reg;
	}
	
	public class AddItemWindow extends Window {
		private TreeGrid tree;
		private DynamicForm typeForm;
		private TextItem searchField;
		
		public AddItemWindow() {
			setWidth(550);
			setHeight(400);
			setTitle("Add Image");
			setAutoCenter(true);
			setIsModal(true);
			
			HLayout topLayout = new HLayout();
			topLayout.setMargin(5);
			topLayout.setHeight(30);
			topLayout.setWidth100();
			
			DynamicForm searchForm = new DynamicForm();
			searchForm.setWidth("100%");
			searchForm.setHeight(25);
			searchForm.setMargin(0);
			searchForm.setNumCols(4);
			searchForm.setCellPadding(2);
			searchField = new TextItem("query");
			searchField.setShowTitle(false);
			searchField.setWidth(475);
			ButtonItem searchButton = new ButtonItem("search", "search");
			searchButton.setWidth(50);
			searchButton.setStartRow(false);
			searchButton.setEndRow(false);
			searchButton.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					Criteria criteria = new Criteria();
					criteria.addCriteria("query", searchField.getValueAsString());
					tree.fetchData(criteria);
				}
			});
			searchForm.setFields(searchField,searchButton);
			topLayout.addMember(searchForm);
			addItem(topLayout);
			
			tree = new TreeGrid();
			tree.setShowHeader(false);
			tree.setDataSource(ContentServiceDS.getInstance().getFolderBrowseDatasource());
			tree.addClickHandler(new ClickHandler() {
				public void onClick(ClickEvent event) {
					if(tree.getSelectedRecord().getAttribute("localName").equals("file"))
						typeForm.show();
					else
						typeForm.hide();
				}
			});
			addItem(tree);
						
			HLayout layout = new HLayout();
			layout.setMargin(5);
			layout.setHeight(30);
			layout.setWidth100();
			layout.setMembersMargin(5);
			layout.setAlign(Alignment.RIGHT);
			
			typeForm = new DynamicForm();
			typeForm.setWidth("100%");
			typeForm.setNumCols(8);
			typeForm.hide();
			
			final SelectItem typeItem = new SelectItem("type","Type");
			typeItem.setWidth(100);
			typeItem.setValueMap(contentModel.getImageRelationships());
			
			final TextItem groupItem = new TextItem("group", "Group");
			groupItem.setWidth(125);
			
			final SpinnerItem orderItem = new SpinnerItem("order", "Order");
			orderItem.setWidth(35);
						
			ButtonItem linkItem = new ButtonItem("Link");
			linkItem.setWidth(65);
			linkItem.setStartRow(false);
			linkItem.setEndRow(false);
			linkItem.setIcon("/theme/images/icons16/link.png");
			linkItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					Record record = new Record();
					record.setAttribute("qname", "openapps_org_content_1_0_files");
					record.setAttribute("source", accession.getAttribute("id"));
					record.setAttribute("target", tree.getSelectedRecord().getAttribute("id"));
					record.setAttribute("type", typeItem.getValue());
					record.setAttribute("group", groupItem.getValue());
					record.setAttribute("order", orderItem.getValue());
					RestUtility.post("/service/association/add.xml", record, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							if(response.getData() != null && response.getData().length > 0) {
								Record accession = response.getData()[0];
								if(accession != null) {
									Record source_associations = accession.getAttributeAsRecord("source_associations");
									if(source_associations != null) {
										Record items = source_associations.getAttributeAsRecord("files");
										if(items != null) {
											setData(items.getAttributeAsRecordArray("node"));
										} else setData(new Record[0]);
									}
								}
							}
						}
					});
					addItemWindow.hide();
				}
			});
			typeForm.setFields(typeItem,groupItem,orderItem,linkItem);
			layout.addMember(typeForm);
			addItem(layout);
		}
		@Override
		public void show() {
			/*
			if(accession != null && !collectionId.equals(accession.getAttribute("parent"))) {
				collectionId = accession.getAttribute("parent");
				Criteria criteria = new Criteria();
				criteria.setAttribute("collection", collectionId);				
				tree.fetchData(criteria);
			}
			*/
			super.show();
		}
	}
	
}
