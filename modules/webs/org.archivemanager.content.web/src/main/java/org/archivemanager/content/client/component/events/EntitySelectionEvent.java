package org.archivemanager.content.client.component.events;

import com.smartgwt.client.data.Record;

public class EntitySelectionEvent {
	private Record record;

	
	public Record getRecord() {
		return record;
	}
	public void setRecord(Record record) {
		this.record = record;
	}
	
}
