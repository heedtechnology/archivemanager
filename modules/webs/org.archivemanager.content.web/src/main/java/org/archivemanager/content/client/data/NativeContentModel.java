package org.archivemanager.content.client.data;

import java.util.LinkedHashMap;

import com.google.gwt.core.client.JavaScriptObject;
import com.smartgwt.client.util.JSOHelper;

public class NativeContentModel {

	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getDataSourceTypes() { 
		JavaScriptObject obj = getNativeDataSourceTypes();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>();
	}	
	private final native JavaScriptObject getNativeDataSourceTypes() /*-{
        return $wnd.datasource_types;
	}-*/;
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getContentTypes() { 
		JavaScriptObject obj = getNativeContentTypes();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>();
	}	
	private final native JavaScriptObject getNativeContentTypes() /*-{
        return $wnd.item_types;
	}-*/;
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getImageRelationships() { 
		JavaScriptObject obj = getNativeImageRelationships();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>();
	}	
	private final native JavaScriptObject getNativeImageRelationships() /*-{
        return $wnd.image_relationships;
	}-*/;
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,String> getNoteTypes() { 
		JavaScriptObject obj = getNativeNoteTypes();
		if(obj != null) return (LinkedHashMap<String,String>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,String>();
	}	
	private final native JavaScriptObject getNativeNoteTypes() /*-{
        return $wnd.content_note_types;
	}-*/;
}
