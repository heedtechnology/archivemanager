package org.archivemanager.content.client;

import org.archivemanager.content.client.file.AddDatasourceWindow;
import org.archivemanager.content.client.file.DataSourcePanel;
import org.archivemanager.content.client.file.FolderPanel;
import org.archivemanager.gwt.client.component.BreadcrumbComponent;
import org.archivemanager.gwt.client.component.FilesystemGrid;
import org.heed.openapps.gwt.client.component.Toolbar;
import org.heed.openapps.gwt.client.data.ContentServiceDS;
import org.heed.openapps.gwt.client.event.AddListener;
import org.heed.openapps.gwt.client.event.BrowseListener;
import org.heed.openapps.gwt.client.event.SelectionListener;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class FileSystemManager extends VLayout {
	private Toolbar toolbar;
	private FilesystemGrid grid;
	private FolderPanel contentPanel;
	private DataSourcePanel datasourcePanel;
	private Canvas canvas;
	private BreadcrumbComponent breadcrumb;
	private AddDatasourceWindow addWindow;
	
	private Record selection;
	
	
	public FileSystemManager(final ContentManagerEntryPoint mgr) {
		//User user = security.getUser();
		
		setWidth100();
		setHeight100();
		setMembersMargin(5);
		
		toolbar = new Toolbar(30);
		toolbar.setMargin(1);
		toolbar.setLayoutLeftMargin(2);
		toolbar.setBorder("1px solid #A7ABB4");
				
		breadcrumb = new BreadcrumbComponent();	    
		toolbar.addToLeftCanvas(breadcrumb);
		
		toolbar.addButton("save", "/theme/images/icons32/disk.png", "Save", new ClickHandler() {  
			public void onClick(ClickEvent event) { 
				save();
	    	}  
	    });
		toolbar.addButton("delete", "/theme/images/icons32/delete.png", "Delete Datasource", new ClickHandler() {  
			public void onClick(ClickEvent event) {
				SC.confirm("Do you really wish to remove this permanently?", new BooleanCallback() {
					public void execute(Boolean value) {
						if(value) {
							grid.removeSelectedData();
						}
					}			
				});
	    	}  
	    });		
	    toolbar.addButton("add", "/theme/images/icons32/add.png", "Add Datasource", new ClickHandler() {  
			public void onClick(ClickEvent event) { 
				addWindow.show();
	    	}  
	    });
	    toolbar.hideButton("delete");
		toolbar.hideButton("save");
	    addMember(toolbar);
	    	    
	    HLayout mainLayout = new HLayout();
	    mainLayout.setWidth100();
	    mainLayout.setHeight100();
	    addMember(mainLayout);
	    
	    grid = new FilesystemGrid();
	    grid.setWidth(300);
	    grid.setMargin(1);
	    grid.setShowResizeBar(true);
	    grid.setSelectionListener(new SelectionListener() {
			@Override
			public void select(Record record) {
				selectFile(record);
			}	    	
	    });
	    grid.setBrowseListener(new BrowseListener() {
			@Override
			public void browse(Record record) {
				grid.fetchData(record, new DSCallback() {
					@Override
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						
					}				
				});
			}	    	
	    });
        mainLayout.addMember(grid);
	    
        canvas = new Canvas();
        canvas.setWidth100();
        canvas.setHeight100();
        canvas.setMargin(1);
        //canvas.setBorder("1px solid #A7ABB4");
        mainLayout.addMember(canvas);
		
        datasourcePanel = new DataSourcePanel();
        datasourcePanel.hide();
        canvas.addChild(datasourcePanel);
        
		contentPanel = new FolderPanel();
		contentPanel.hide();
        canvas.addChild(contentPanel);
        
        addWindow = new AddDatasourceWindow();
        addWindow.setAddListener(new AddListener() {
			@Override
			public void add(Record record) {
				grid.addData(record, new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
				
					}
		        });
			}    	
        });
        /*
        EventBus.addHandler(org.heed.openapps.gwt.client.OpenAppsEvent.TYPE, new OpenAppsEventHandler()     {
	        @Override
	        public void onEvent(OpenAppsEvent event) {
	            if(event.isType(EventTypes.ADD)) {
		        	if(event.getRecord().getAttribute("qname").equals("openapps_org_content_1_0_datasource")) {
		        		RestUtility.post("/service/entity/create.xml", event.getRecord(), new DSCallback() {
		        			public void execute(DSResponse response, Object rawData, DSRequest request) {
		        				addWindow.hide();
		        				grid.addData(response.getData()[0], new DSCallback() {
									public void execute(DSResponse response, Object rawData, DSRequest request) {
										
									}
								});
		        			}						
		        		});	
		        		
		        	}
	            } else if(event.isType(ArchiveManagerEventTypes.CONTENT_BROWSE)) {
	            	uid = event.getRecord().getAttribute("parent");	            	
	            	
	            	Record record = new Record();
	            	record.setAttribute("parent", uid);
	            	grid.fetchData(record, new DSCallback() {
						@Override
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							
						}	            		
	            	});
	            	if(uid == null || uid.equals("null") || uid.equals("")) {
	            		toolbar.hideButton("delete");
	            		toolbar.hideButton("save");
	            	}
	            } else if(event.isType(ArchiveManagerEventTypes.CONTENT_SELECTION)) {
	        		select(event.getRecord());
	            }
	        }
	    });
	    */
	}
	public void selectFile(Record record) {
		selection = record;
		final String type = grid.getSelectedRecord().getAttribute("localName");
		if(type != null) {
			if(type.equals("folder")) {				
				Criteria criteria = new Criteria();
				criteria.setAttribute("uid", record.getAttribute("uid"));
				ContentServiceDS.getInstance().getFolder(criteria, new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						if(response.getData() != null && response.getData().length > 0) {
							Record folder = response.getData()[0];
							if(folder != null) {
								contentPanel.select(folder);
								contentPanel.show(); 
							}
						}
					}						
				});				
				toolbar.hideButton("delete");
				toolbar.hideButton("add");
				toolbar.showButton("save");
			} else if(type.equals("datasource")) {
				contentPanel.hide();
				datasourcePanel.select(record);
				toolbar.showButton("delete");
				toolbar.showButton("add");
				toolbar.showButton("save");
				datasourcePanel.show();
			} else if(type.equals("file")) {
				contentPanel.select(record);
				toolbar.hideButton("delete");
				toolbar.hideButton("add");
				toolbar.hideButton("save");
			}
		}		
	}
	public void save() {
		String type = grid.getSelectedRecord().getAttribute("localName");
		if(type == null || type.equals("folder")) {
			Record criteria = contentPanel.getData();
			ContentServiceDS.getInstance().update(criteria, new DSCallback() {
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					if(response.getData() != null && response.getData().length > 0) {
						contentPanel.select(response.getData()[0]);
						toolbar.showMessage(Toolbar.SUCCESS_ICON, "Folder saved successfully");
					}
				}						
			});
		} else if(type.equals("datasource")) {
			Record criteria = datasourcePanel.getData();					
			ContentServiceDS.getInstance().update(criteria, new DSCallback() {
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					if(response.getData() != null && response.getData().length > 0) {
						datasourcePanel.select(response.getData()[0]);
						toolbar.showMessage(Toolbar.SUCCESS_ICON, "Datasource saved successfully");
					}
				}						
			});			
		}
	}
	public void search(String query, DSCallback callback) {
		Record criteria = new Record();
		//criteria.setAttribute("qname", "{openapps.org_content_1.0}folder");
		criteria.setAttribute("sources", "false");
		criteria.setAttribute("query", query);
		/*
		RestUtility.get("/service/archivemanager/content/search.xml?uid="+fileGrid.getSelectedRecord().getAttribute("uid"), new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				if(response.getData() != null && response.getData().length > 0) {
					grid.setData(response.getData());
				}
			}						
		});
		*/
	}
	public void delete() {
		grid.removeSelectedData();
	}
	public Record getSelection() {
		return selection;
	}
	
}
