package org.archivemanager.content.client.data;

import java.util.LinkedHashMap;

import com.google.gwt.core.client.JavaScriptObject;
import com.smartgwt.client.util.JSOHelper;

public class NativeClassificationModel {

	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getSubjectSource() { 
		JavaScriptObject obj = getNativeSubjectSource();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>();
	}	
	private final native JavaScriptObject getNativeSubjectSource() /*-{
        return $wnd.subject_source;
	}-*/;
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getSubjectType() { 
		JavaScriptObject obj = getNativeSubjectType();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>();
	}	
	private final native JavaScriptObject getNativeSubjectType() /*-{
        return $wnd.subject_type;
	}-*/;
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getNamedEntityRule() { 
		JavaScriptObject obj = getNativeNamedEntityRule();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>();
	}	
	private final native JavaScriptObject getNativeNamedEntityRule() /*-{
        return $wnd.named_entity_rule;
	}-*/;
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getNamedEntityType() { 
		JavaScriptObject obj = getNativeNamedEntityType();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>();
	}	
	private final native JavaScriptObject getNativeNamedEntityType() /*-{
        return $wnd.named_entity_type;
	}-*/;
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getNamedEntityFunction() { 
		JavaScriptObject obj = getNativeNamedEntityFunction();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>();
	}	
	private final native JavaScriptObject getNativeNamedEntityFunction() /*-{
        return $wnd.named_entity_function;
	}-*/;
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getNamedEntityRole() { 
		JavaScriptObject obj = getNativeNamedEntityRole();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>();
	}	
	private final native JavaScriptObject getNativeNamedEntityRole() /*-{
        return $wnd.named_entity_role;
	}-*/;
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getClassificationType() { 
		JavaScriptObject obj = getNativeClassificationType();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>();
	}	
	private final native JavaScriptObject getNativeClassificationType() /*-{
        return $wnd.classification_types;
	}-*/;
	
}
