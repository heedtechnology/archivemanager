package org.archivemanager.content.client.form;

import java.util.LinkedHashMap;

import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.CanvasItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.SpinnerItem;
import com.smartgwt.client.widgets.layout.HLayout;

public class TimeItem extends CanvasItem {
	private HLayout layout;
	private SpinnerItem hoursItem;
	private SelectItem minutesItem;
	
	public TimeItem(String name, String title) {
		super(name,title);
		setWidth("100%");
		setHeight("100%");
		layout = new HLayout();
		layout.setWidth(115);
		layout.setHeight(30);
		
		DynamicForm form = new DynamicForm();
		form.setWidth(150);
		form.setNumCols(5);
		
		hoursItem = new SpinnerItem("hours");
		hoursItem.setWidth(50);
		hoursItem.setShowTitle(false);
		hoursItem.setAlign(Alignment.CENTER);
		hoursItem.setMin(0);
		
		CanvasItem spacer = new CanvasItem();
		spacer.setAlign(Alignment.CENTER);
		spacer.setShowTitle(false);
		Label label = new Label(" : ");
		label.setHeight(30);
		label.setWidth(5);
		spacer.setCanvas(label);
		
		minutesItem = new SelectItem("minutes");
		minutesItem.setWidth(50);
		minutesItem.setShowTitle(false);
		minutesItem.setAlign(Alignment.CENTER);
		LinkedHashMap<String,String> valueMap = new LinkedHashMap<String,String>();		
			
		valueMap.put("55", "55");
		valueMap.put("50", "50");
		valueMap.put("45", "45");
		valueMap.put("40", "40");
		valueMap.put("35", "35");
		valueMap.put("30", "30");
		valueMap.put("25", "25");
		valueMap.put("20", "20");
		valueMap.put("15", "15");
		valueMap.put("10", "10");
		valueMap.put("05", "05");
		valueMap.put("00", "00");
		
		minutesItem.setValueMap(valueMap);
		
		form.setFields(hoursItem,spacer,minutesItem);
		layout.addChild(form);
		
		setCanvas(layout);
	}
	@Override
	public Object getValue() {
		return getHours()+":"+getMinutes();
	}
	@Override
	public void setValue(String value) {
		if(value != null && value.contains(":")) {
			String[] vals = value.split(":");
			hoursItem.setValue(vals[0]);
			minutesItem.setValue(vals[1]);
		}
		super.setValue(value);
	}
	public String getHours() {
		if(hoursItem.getValue() == null) return "0";
		return hoursItem.getValueAsString();
	}
	public String getMinutes() {
		if(minutesItem.getValue() == null) return "0";
		return minutesItem.getValueAsString();
	}
}
