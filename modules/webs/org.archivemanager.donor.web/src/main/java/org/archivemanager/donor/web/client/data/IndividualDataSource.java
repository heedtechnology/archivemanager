package org.archivemanager.donor.web.client.data;

import org.heed.openapps.gwt.client.data.NativeSecurityModel;

import com.google.gwt.core.client.JavaScriptObject;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.util.JSOHelper;


public class IndividualDataSource extends RestDataSource {
	private NativeSecurityModel security = new NativeSecurityModel();
	
	public IndividualDataSource() {
		setID(id);  
        setTitleField("Title");  
        //setRecordXPath("/List/employee");  
        DataSourceTextField nameField = new DataSourceTextField("title", "Title", 128);  
  
        DataSourceTextField idField = new DataSourceTextField("id", "Dictionary ID");  
        idField.setPrimaryKey(true);  
        idField.setRequired(true);  
  
        DataSourceTextField parentField = new DataSourceTextField("parent", "Parent");  
        parentField.setRequired(true);  
        parentField.setForeignKey(id + ".id");  
        parentField.setRootValue("0");  
        
        setFields(nameField,idField,parentField);
        
    	setFetchDataURL("/service/entity/get.xml");  
    	setAddDataURL("/search/manager/definition/add.xml");  
    	setUpdateDataURL("/service/entity/update.xml");  
    	setRemoveDataURL("/service/entity/remove.xml");
	}
	
	@Override
	protected Object transformRequest(DSRequest dsRequest) {
		JavaScriptObject data = dsRequest.getData();
		
		JSOHelper.setAttribute(data, "ticket", security.getTicket());
		
		return data;
	} 
    @Override  
    protected void transformResponse(DSResponse response, DSRequest request, Object data) {  
        super.transformResponse(response, request, data);  
    }	
}
