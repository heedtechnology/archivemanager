package org.archivemanager.donor.web.client;

import org.archivemanager.donor.web.client.component.AddressComponent;
import org.archivemanager.donor.web.client.component.EmailComponent;
import org.archivemanager.donor.web.client.component.PhoneComponent;
import org.archivemanager.donor.web.client.component.WebAddressComponent;
import org.archivemanager.donor.web.client.form.IndividualContactForm;
import org.archivemanager.donor.web.client.form.OrganizationContactForm;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.EventTypes;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.OpenAppsEventHandler;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class ContactPanel extends VLayout {
	private Record selection;
	private IndividualContactForm individualForm;
	private OrganizationContactForm organizationForm;
	
	private AddressComponent addressComponent;
	private PhoneComponent phoneComponent;
	private EmailComponent emailComponent;
	private WebAddressComponent webAddressComponent;
	
	
	public ContactPanel() {
		setWidth100();
		setHeight100();
		
		HLayout topLayout = new HLayout();
		topLayout.setMargin(2);
		topLayout.setMembersMargin(2);
		addMember(topLayout);
		/*
		HLayout botLayout = new HLayout();
		botLayout.setMargin(2);
		botLayout.setBorder("1px solid #C0C3C7");
		addMember(botLayout);
		*/
		VLayout leftColumn = new VLayout();
		leftColumn.setHeight(400);
		leftColumn.setWidth(505);
		//leftColumn.setBorder("1px solid #C0C3C7");
		topLayout.addMember(leftColumn);
		
		VLayout rightColumn = new VLayout();
		rightColumn.setHeight(400);
		//rightColumn.setBorder("1px solid #C0C3C7");
		topLayout.addMember(rightColumn);
		
		Canvas formCanvas = new Canvas();
		formCanvas.setHeight100();
		leftColumn.addMember(formCanvas);
		
		individualForm = new IndividualContactForm();
		individualForm.setHeight100();
		formCanvas.addChild(individualForm);
		
		organizationForm = new OrganizationContactForm();
		organizationForm.setHeight100();
		organizationForm.hide();
		formCanvas.addChild(organizationForm);
		
		addressComponent = new AddressComponent("100%", true);
		addressComponent.setMargin(5);
		rightColumn.addMember(addressComponent);
		
		phoneComponent = new PhoneComponent("100%", true);
		phoneComponent.setMargin(5);
		rightColumn.addMember(phoneComponent);
		
		emailComponent = new EmailComponent("100%", true);
		emailComponent.setMargin(5);
		rightColumn.addMember(emailComponent);
		
		webAddressComponent = new WebAddressComponent("100%", true);
		webAddressComponent.setMargin(5);
		rightColumn.addMember(webAddressComponent);
		
		Canvas topRightSpacer = new Canvas();
		topRightSpacer.setHeight100();
		rightColumn.addMember(topRightSpacer);
						
		Canvas bottomSpacer = new Canvas();
		bottomSpacer.setHeight100();
		rightColumn.addMember(bottomSpacer);
		
		EventBus.addHandler(org.heed.openapps.gwt.client.OpenAppsEvent.TYPE, new OpenAppsEventHandler()     {
	        @Override
	        public void onEvent(OpenAppsEvent event) {
	            if(event.isType(EventTypes.SELECTION)) {
	            	selection = event.getRecord();
	            	addressComponent.select(selection);
	            	phoneComponent.select(selection);
	            	webAddressComponent.select(selection);
	            	emailComponent.select(selection);
	            	String qname = selection.getAttribute("qname");
	            	String parentQName = selection.getAttribute("parentQName");
	            	if((qname != null && qname.equals("{openapps.org_contact_1.0}individual")) || (parentQName != null && parentQName.equals("{openapps.org_contact_1.0}individual"))) {
	            		organizationForm.hide();
	            	   	individualForm.show();
	            	} else if((qname != null && qname.equals("{openapps.org_contact_1.0}organization")) || (parentQName != null && parentQName.equals("{openapps.org_contact_1.0}organization"))) {
	            		individualForm.hide();
            	   		organizationForm.show();
	            	} else selection = null;
	            } 
	        }
	    });
	}
	
	public IndividualContactForm getIndividualForm() {
		return individualForm;
	}
	public OrganizationContactForm getOrganizationForm() {
		return organizationForm;
	}
	
}
