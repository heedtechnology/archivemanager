package org.archivemanager.donor.web.client.component;

import java.util.LinkedHashMap;

import org.heed.openapps.gwt.client.data.RestUtility;

import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class WebAddressComponent extends VLayout {
	private AddNoteWindow addNoteWindow;
	private ListGrid grid;
	private Record selection;
	
	
	public WebAddressComponent(String width, boolean editable) {
		HLayout toolstrip = new HLayout();
		toolstrip.setWidth(width);
		toolstrip.setHeight(20);
		toolstrip.setBackgroundImage("[SKIN]/cssButton/button_stretch.png");
		toolstrip.setBorder("1px solid #A7ABB4");
		addMember(toolstrip);
		
		Label label = new Label("<label style='font-size:11px;font-weight:bold;color:#4C4C4C;font-family:Arial,Verdana,sans-serif;'>Web Addresses</label>");
		label.setHeight("100%");
		label.setWidth("100%");
		toolstrip.addMember(label);
		
		if(editable) {
			HLayout buttons = new HLayout();
			buttons.setHeight(16);
			buttons.setMargin(2);
			buttons.setMembersMargin(3);
			toolstrip.addMember(buttons);
		
			ImgButton addButton = new ImgButton();
			addButton.setWidth(16);
			addButton.setHeight(16);
			addButton.setSrc("/theme/images/icons16/add.png");
			addButton.addClickHandler(new ClickHandler() {
				public void onClick(ClickEvent event) {
					addNoteWindow.show();
				}
			});
			buttons.addMember(addButton);
			ImgButton delButton = new ImgButton();
			delButton.setWidth(16);
			delButton.setHeight(16);
			delButton.setSrc("/theme/images/icons16/delete.png");
			delButton.addClickHandler(new ClickHandler() {
				public void onClick(ClickEvent event) {
					Record rec = new Record();
					String id = grid.getSelectedRecord().getAttribute("target_id");
					if(id == null) id = grid.getSelectedRecord().getAttribute("id");
					rec.setAttribute("id", id);
					RestUtility.post("/service/entity/remove.xml", rec, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							grid.removeSelectedData();	
						}						
					});	
				}
			});
			buttons.addMember(delButton);
		
			addNoteWindow = new AddNoteWindow();
		}
		grid = new ListGrid();
		grid.setWidth(width);
		grid.setHeight(40);
		grid.setAutoFitData(Autofit.VERTICAL);
		grid.setAutoFitMaxHeight(200);
		grid.setShowHeader(false);
		grid.setWrapCells(true);
		grid.setFixedRecordHeights(false);
		ListGridField typeField = new ListGridField("type","Type");
		typeField.setWidth(55);
		ListGridField contentField = new ListGridField("address");
		ListGridField primaryField = new ListGridField("primary");
		grid.setFields(typeField, contentField,primaryField);
		addMember(grid);
				
	}
	public void select(Record record) {
		this.selection = record;
		try {
			Record source_associations = record.getAttributeAsRecord("source_associations");
			if(source_associations != null) {
				Record notes = source_associations.getAttributeAsRecord("webs");
				if(notes != null) {
					setData(notes.getAttributeAsRecordArray("node"));
				} else setData(new Record[0]);
			}
		} catch(ClassCastException e) {
			setData(new Record[0]);
		}
	}
	public void setData(Record[] records) {
		for(Record record : records) {
			String primary = record.getAttribute("primary");
			if(primary != null && primary.equals("true")) record.setAttribute("primary", "primary");
			else record.setAttribute("primary", "");
		}
		grid.setData(records);
	}
	
	public class AddNoteWindow extends Window {
		private static final int FIELD_WIDTH = 300;
		public AddNoteWindow() {
			setWidth(420);
			setHeight(160);
			setTitle("Add A Phone Number");
			setAutoCenter(true);
			setIsModal(true);
			
			final DynamicForm addForm = new DynamicForm();
			addForm.setCellPadding(5);
			final SelectItem typeItem = new SelectItem("type", "Type");
			LinkedHashMap<String,String> valueMap = new LinkedHashMap<String,String>();
			valueMap.put("personal","Personal");
			valueMap.put("work","Work");
			typeItem.setValueMap(valueMap);
			typeItem.setWidth(FIELD_WIDTH/2);
			
			TextItem phoneItem = new TextItem("address","Address");
			phoneItem.setWidth(FIELD_WIDTH);
			
			CheckboxItem primaryItem = new CheckboxItem("primary", "Is Primary");
			
			ButtonItem submitItem = new ButtonItem("submit", "Add");
			submitItem.setIcon("/theme/images/icons16/add.png");
			submitItem.setStartRow(false);
			submitItem.setEndRow(false);
			submitItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					Record record = new Record();
					record.setAttribute("assoc_qname", "{openapps.org_contact_1.0}webs");
					record.setAttribute("entity_qname", "{openapps.org_contact_1.0}web");
					record.setAttribute("source", selection.getAttribute("id"));
					record.setAttribute("type", addForm.getValue("type"));
					record.setAttribute("address", addForm.getValue("address"));
					record.setAttribute("primary", addForm.getValue("primary"));
					RestUtility.post("/service/entity/associate.xml",record,new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							if(response.getData() != null && response.getData().length > 0) {
								grid.addData(response.getData()[0]);
								addForm.clearValues();
								addNoteWindow.hide();
							}
						}						
					});
				}			
			});
			
			addForm.setFields(phoneItem,typeItem,primaryItem,submitItem);
			addItem(addForm);
		}
	}
	
}