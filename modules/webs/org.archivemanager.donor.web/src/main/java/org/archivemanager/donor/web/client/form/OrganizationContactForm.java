package org.archivemanager.donor.web.client.form;

import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.SpacerItem;
import com.smartgwt.client.widgets.form.fields.TextItem;

public class OrganizationContactForm extends DynamicForm {
	private static final int FIELD_WIDTH = 300;
	
	
	public OrganizationContactForm() {
		setWidth100();
		setNumCols(2);
		setCellPadding(7);
		setColWidths(120, "*");
		
		SpacerItem spacer = new SpacerItem();
		spacer.setHeight(10);
		spacer.setColSpan(2);
		/*
		TextItem salutationField = new TextItem("salutation", "Salutation");
		salutationField.setWidth(FIELD_WIDTH);
		
		TextItem greetingField = new TextItem("greeting", "Greeting");
		greetingField.setWidth(FIELD_WIDTH);
				
		TextItem altsalutationField = new TextItem("alt_salutation", "Alternative Salutation");
		altsalutationField.setWidth(FIELD_WIDTH);
		*/
		TextItem faxField = new TextItem("fax1", "Fax Number");
		faxField.setWidth(FIELD_WIDTH/2);
		
		TextItem roleField = new TextItem("role", "Role");
		roleField.setWidth(FIELD_WIDTH/2);
		
		TextItem dateListField = new TextItem("dateList", "Date List");
		dateListField.setWidth(FIELD_WIDTH);
		
		DateItem lastWroteItem = new DateItem("last_wrote", "Last Wrote");
		
		DateItem lastShipItem = new DateItem("last_ship", "Last Shipped");
		
		setFields(spacer,faxField,roleField,dateListField,lastWroteItem,lastShipItem);
		
	}
}
