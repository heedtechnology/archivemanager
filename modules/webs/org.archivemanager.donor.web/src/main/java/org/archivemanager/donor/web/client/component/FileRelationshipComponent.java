package org.archivemanager.donor.web.client.component;

import org.archivemanager.donor.web.client.NativeDonorModel;
import org.heed.openapps.gwt.client.component.UploadListener;
import org.heed.openapps.gwt.client.data.RestUtility;
import org.heed.openapps.gwt.client.util.FileUtility;

import com.google.gwt.user.client.ui.NamedFrame;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.types.Encoding;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.HiddenItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.UploadItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class FileRelationshipComponent extends VLayout {
	private NativeDonorModel model = new NativeDonorModel();
	private ListGrid grid;
	private AddItemWindow addItemWindow;
		
	//private Map<HandlerRegistration, EntitySelectionHandler> handlers = new HashMap<HandlerRegistration, EntitySelectionHandler>();		
	private UploadListener listener;
	private Record selection;
	
	public FileRelationshipComponent(String title, String width, boolean upload, boolean associate, boolean delete) {
		setWidth(width);
		initComplete(this);
		
		HLayout toolstrip = new HLayout();
		toolstrip.setWidth("100%");
		toolstrip.setHeight(20);
		toolstrip.setBackgroundImage("[SKIN]/cssButton/button_stretch.png");
		toolstrip.setBorder("1px solid #A7ABB4");
		addMember(toolstrip);
		
		Label label = new Label("<label style='font-size:11px;font-weight:bold;color:#4C4C4C;font-family:Arial,Verdana,sans-serif;'>"+title+"</label>");
		label.setHeight("100%");
		label.setWidth("100%");
		toolstrip.addMember(label);
		
		HLayout buttons = new HLayout();
		buttons.setHeight(16);
		buttons.setMargin(2);
		buttons.setMembersMargin(3);
		toolstrip.addMember(buttons);
		
		ImgButton addButton = new ImgButton();
		addButton.setWidth(16);
		addButton.setHeight(16);
		addButton.setShowDown(false);
		addButton.setShowRollOver(false);
		addButton.setPrompt("Add");
		addButton.setSrc("/theme/images/icons16/add.png");
		addButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				//EventBus.fireEvent(new OpenAppsEvent(EquityEventTypes.SAVE_DEAL));
				addItemWindow.show();
			}
		});
		buttons.addMember(addButton);
		
		final ImgButton editButton = new ImgButton();
		editButton.setWidth(16);
		editButton.setHeight(16);
		editButton.setShowDown(false);
		editButton.setShowRollOver(false);
		editButton.setVisible(false);
		editButton.setPrompt("View");
		editButton.setSrc("/theme/images/icons16/zoom.png");
		editButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				if(grid.getSelectedRecord() != null) {
					String id = grid.getSelectedRecord().getAttribute("target_id");
					String filename = grid.getSelectedRecord().getAttribute("filename");
					if(filename != null) {
						com.google.gwt.user.client.Window.open("/equity/service/stream/"+id+"."+FileUtility.getExtension(filename), "_blank", null);
					} else 
						com.google.gwt.user.client.Window.open("/equity/service/stream/"+id, "_blank", null);
				} else SC.say("Please select a file to view");
			}
		});
		buttons.addMember(editButton);
		
		final ImgButton delButton = new ImgButton();
		delButton.setWidth(16);
		delButton.setHeight(16);
		delButton.setShowDown(false);
		delButton.setShowRollOver(false);
		delButton.setVisible(false);
		delButton.setPrompt("Delete");
		delButton.setSrc("/theme/images/icons16/delete.png");
		delButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Record rec = new Record();
				String id = grid.getSelectedRecord().getAttribute("id");
				rec.setAttribute("id", id);
				RestUtility.post("/service/association/remove.xml", rec, new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						grid.removeSelectedData();	
					}						
				});	
			}
		});
		buttons.addMember(delButton);
		
		grid = new ListGrid();
		grid.setWidth("100%");
		grid.setHeight(48);
		grid.setAutoFitData(Autofit.VERTICAL);
		grid.setAutoFitMaxHeight(200);
		//grid.setBorder("0px");
		grid.setShowHeader(false);
		ListGridField typeField = new ListGridField("filetype","Type", 150);
		typeField.setValueMap(model.getFileRelationships());
		ListGridField nameField = new ListGridField("filename","Name");
		grid.setFields(typeField,nameField);
		grid.addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				editButton.show();
				delButton.show();
				//EntitySelectionEvent evt = new EntitySelectionEvent();
				//evt.setRecord(grid.getSelectedRecord());
				//for(EntitySelectionHandler handler : handlers.values()) {
					//handler.onEntitySelection(evt);
				//}
			}
		});
        addMember(grid);
        
        addUploadListener(new UploadListener() {
			public void uploadComplete(String key) {				
				Record record = new Record();
				record.setAttribute("id", selection.getAttribute("id"));
				//EventBus.fireEvent(new OpenAppsEvent(EquityEventTypes.EDIT_DEAL, record));
				addItemWindow.hide();
				//SC.say("Collection Imported Successfully");
			}
		});
        
        addItemWindow = new AddItemWindow();
	}
	public void setData(Record[] records) {
		grid.setData(records);
	}
	public void select(Record record) {
		this.selection = record;
		try {
			Record source_associations = record.getAttributeAsRecord("source_associations");
			if(source_associations != null) {
				Record notes = source_associations.getAttributeAsRecord("files");
				if(notes != null) {
					setData(notes.getAttributeAsRecordArray("node"));
				} else setData(new Record[0]);
			}
		} catch(ClassCastException e) {
			setData(new Record[0]);
		}
		try {
			Record target_associations = record.getAttributeAsRecord("target_associations");
			if(target_associations != null) {
				
			}
		} catch(ClassCastException e) {
			
		}
	}
	/*
	public HandlerRegistration addEntitySelectionHandler(EntitySelectionHandler handler) {
		HandlerRegistration reg = doAddHandler(handler, new Type<EntitySelectionHandler>());
		if(!handlers.containsKey(reg)) handlers.put(reg, handler);
		return reg;
	}
	*/
	public class AddItemWindow extends Window {
		
		public static final String TARGET="uploadTarget";
		
		public AddItemWindow() {
			setWidth(375);
			setHeight(140);
			setTitle("File Upload");
			setAutoCenter(true);
			setIsModal(true);
			
			Canvas canvas = new Canvas();
			canvas.setWidth100();
			canvas.setHeight100();
			addItem(canvas);
			
			NamedFrame frame = new NamedFrame(TARGET);
			frame.setWidth("1");
			frame.setHeight("1");
			frame.setVisible(false);
			canvas.addChild(frame);	
						
			final DynamicForm searchForm = new DynamicForm();
			searchForm.setWidth("100%");
			searchForm.setHeight(25);
			searchForm.setMargin(2);
			searchForm.setNumCols(2);
			searchForm.setCellPadding(5);
			searchForm.setColWidths("80", "*");
			searchForm.setEncoding(Encoding.MULTIPART);
			searchForm.setTarget(TARGET);
			//searchForm.setDataSource(FileUploadDS.getInstance());
			searchForm.setAction("/equity/service/upload");
					
			final SelectItem typeItem = new SelectItem("type","Type");
			typeItem.setWidth(200);
			typeItem.setValueMap(model.getFileRelationships());
			
			UploadItem upload = new UploadItem("file", "File");
			upload.setHeight(22);
			upload.setWidth(300);	
						
			final HiddenItem sourceItem = new HiddenItem("source");
			sourceItem.setHeight(1);
			
			ButtonItem searchButton = new ButtonItem("submit", "Submit");
			searchButton.setWidth(50);
			searchButton.setStartRow(false);
			searchButton.setEndRow(false);
			searchButton.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					String id = selection.getAttribute("id");
					sourceItem.setValue(id);
					searchForm.submitForm();
				}
			});
			searchForm.setItems(sourceItem,typeItem,upload,searchButton);
			canvas.addChild(searchForm);
			
		}		
	}
	public void addUploadListener(UploadListener listener) {
		this.listener = listener;
	}
	public void uploadComplete(String fileName) {
		if (listener != null)
			listener.uploadComplete(fileName);
	}
	private native void initComplete(FileRelationshipComponent upload) /*-{
	   $wnd.uploadComplete = function (fileName) {
	       upload.@org.archivemanager.donor.web.client.component.FileRelationshipComponent::uploadComplete(Ljava/lang/String;)(fileName);
	   };
	}-*/;
}
