package org.archivemanager.donor.web.client.component;

import org.archivemanager.donor.web.client.form.AddressForm;
import org.heed.openapps.gwt.client.data.RestUtility;

import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;

import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class AddressComponent extends VLayout {
	private AddNoteWindow addNoteWindow;
	private Record selection;
	private VLayout addressLayout;
	private HLayout labelLayout;
	
	
	public AddressComponent(String width, boolean editable) {
		HLayout toolstrip = new HLayout();
		toolstrip.setWidth(width);
		toolstrip.setHeight(20);
		toolstrip.setBackgroundImage("[SKIN]/cssButton/button_stretch.png");
		toolstrip.setBorder("1px solid #A7ABB4");
		addMember(toolstrip);
		
		Label label = new Label("<label style='font-size:11px;font-weight:bold;color:#4C4C4C;font-family:Arial,Verdana,sans-serif;'>Addresses</label>");
		label.setHeight("100%");
		label.setWidth("100%");
		toolstrip.addMember(label);
		
		if(editable) {
			HLayout buttons = new HLayout();
			buttons.setHeight(16);
			buttons.setMargin(2);
			buttons.setMembersMargin(3);
			toolstrip.addMember(buttons);
		
			ImgButton addButton = new ImgButton();
			addButton.setWidth(16);
			addButton.setHeight(16);
			addButton.setSrc("/theme/images/icons16/add.png");
			addButton.addClickHandler(new ClickHandler() {
				public void onClick(ClickEvent event) {
					addNoteWindow.show();
				}
			});
			buttons.addMember(addButton);
			
			addNoteWindow = new AddNoteWindow();
		}
		addressLayout = new VLayout();
		addressLayout.setWidth100();
		addressLayout.setHeight(40);
		addressLayout.setBorder("1px solid #C0C3C7");
		addressLayout.setMembersMargin(2);
		addMember(addressLayout);
		labelLayout = new HLayout();
		labelLayout.setHeight(40);
		labelLayout.setWidth(width);
		labelLayout.setAlign(Alignment.CENTER);
		Label label2 = new Label("<label style='font-size:11px;'>No items to show.</label>");
		label2.setAlign(Alignment.CENTER);
		labelLayout.addMember(label2);
		addressLayout.addMember(labelLayout);		
	}
	public void select(Record record) {
		this.selection = record;
		try {
			Record source_associations = record.getAttributeAsRecord("source_associations");
			if(source_associations != null) {
				Record notes = source_associations.getAttributeAsRecord("addresses");
				if(notes != null) {
					setData(notes.getAttributeAsRecordArray("node"));
				} else setData(new Record[0]);
			} else setData(new Record[0]);
		} catch(ClassCastException e) {
			setData(new Record[0]);
		}
	}
	public void setData(Record[] records) {
		addressLayout.removeMembers(addressLayout.getMembers());
		if(records.length > 0) {
			labelLayout.hide();
			for(Record record : records) {
				addData(record);
			}			
		} else {
			addressLayout.addMember(labelLayout);
			labelLayout.show();
		}
	}
	public void addData(Record record) {
		final VLayout layout = new VLayout();
		layout.setMargin(2);
		layout.setStyleName("donorAddressPanel");
		final String addressId = record.getAttribute("id");
		final HLayout layout3 = new HLayout();
		layout3.setBorder("1px solid #A7ABB4;");
		layout.addMember(layout3);
		VLayout layout2 = new VLayout();
		layout3.addMember(layout2);
		String primary = record.getAttribute("primary");
		String header = record.getAttribute("address1");
		if(primary != null && primary.equals("true")) header = header + " (primary)";
		Label address1 = new Label("<label style='font-size:11px;font-weight:bold;padding-left:10px;;'>"+header+"</label>");
		address1.setHeight(18);
		layout2.addMember(address1);
		if(record.getAttribute("address2") != null) {
			Label address2 = new Label("<label style='font-size:11px;padding-left:25px;'>"+record.getAttribute("address2")+"</label>");
			address2.setHeight(18);
			layout2.addMember(address2);
		}
		String city = record.getAttribute("city") != null ? record.getAttribute("city") : "";
		String state = record.getAttribute("state") != null ? record.getAttribute("state") : "";
		String zip = record.getAttribute("zip") != null ? record.getAttribute("zip") : "";
		Label cityLabel = new Label("<label style='font-size:11px;padding-left:25px;'>"+city+", "+state+" "+zip+"</label>");
		cityLabel.setHeight(18);
		layout2.addMember(cityLabel);
			
		ImgButton chartImg = new ImgButton();        			
		chartImg.setMargin(2);
        chartImg.setSrc("[SKIN]/headerIcons/close.png");  
        chartImg.setPrompt("Remove Address");  
        chartImg.setHeight(16);  
        chartImg.setWidth(16);  
        chartImg.addClickHandler(new ClickHandler() {  
          	public void onClick(ClickEvent event) {  
           		Record rec = new Record();
        		rec.setAttribute("id", addressId);
                RestUtility.post("/service/entity/association/remove.xml", rec, new DSCallback() {
                	public void execute(DSResponse response, Object rawData, DSRequest request) {
        				//removeAddress(addressId);
        				layout.removeMember(layout3);
        			}						
        		});	   
            }  
        });
        layout3.addMember(chartImg);
        addressLayout.addMember(layout);
	}
	public class AddNoteWindow extends Window {
		public AddNoteWindow() {
			setWidth(420);
			setHeight(260);
			setTitle("Add Address");
			setAutoCenter(true);
			setIsModal(true);
			
			final AddressForm addForm = new AddressForm();
			addForm.setCellPadding(5);
			addForm.setMargin(5);
			addItem(addForm);
			
			HLayout buttonStrip = new HLayout();
			buttonStrip.setWidth100();
			buttonStrip.setHeight(30);
			buttonStrip.setMargin(5);
			addItem(buttonStrip);
			
			IButton submitItem = new IButton("Add");
			submitItem.setWidth(70);
			submitItem.setIcon("/theme/images/icons16/add.png");
			submitItem.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
					Record record = new Record(addForm.getValues());
					record.setAttribute("assoc_qname", "{openapps.org_contact_1.0}addresses");
					record.setAttribute("entity_qname", "{openapps.org_contact_1.0}address");
					record.setAttribute("source", selection.getAttribute("id"));
					RestUtility.post("/service/entity/associate.xml",record,new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							if(response.getData() != null && response.getData().length > 0) {
								labelLayout.hide();
								addData(response.getData()[0]);
								addForm.clearValues();
								addNoteWindow.hide();
							}
						}						
					});
				}			
			});
			buttonStrip.addMember(submitItem);			
		}
	}
	
}