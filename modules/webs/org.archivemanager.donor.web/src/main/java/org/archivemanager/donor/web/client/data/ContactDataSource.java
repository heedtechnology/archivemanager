package org.archivemanager.donor.web.client.data;

import java.util.Date;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceDateField;
import com.smartgwt.client.data.fields.DataSourceTextField;


public class ContactDataSource extends RestDataSource {
	private static ContactDataSource instance = null;  
	private static DateTimeFormat dateParser = DateTimeFormat.getFormat("yyyy-MM-dd");
	private static DateTimeFormat dateFormatter = DateTimeFormat.getFormat("EEEE MMMM d, yyyy");
	
    public static ContactDataSource getInstance() {  
        if (instance == null) {  
            instance = new ContactDataSource("contactDS");  
        }  
        return instance;  
    } 
	
	public ContactDataSource(String id) {
		setID(id);  
        
		DataSourceTextField idField = new DataSourceTextField("id", "ID",50);  
        idField.setPrimaryKey(true);  
        idField.setRequired(true);
        
		DataSourceDateField birthDateField = new DataSourceDateField("birth_date");
		
		DataSourceDateField deathDateField = new DataSourceDateField("death_date");
		
		DataSourceDateField lastWroteDateField = new DataSourceDateField("last_wrote");
		
		DataSourceDateField lastShippedDateField = new DataSourceDateField("last_ship");
		
		setFields(idField,birthDateField,deathDateField,lastWroteDateField,lastShippedDateField);
		
    	setFetchDataURL("/service/entity/get.xml");  
    	setUpdateDataURL("/service/entity/update.xml"); 
    	setRemoveDataURL("/service/entity/remove.xml");
	}
	
	@Override  
    protected Object transformRequest(DSRequest dsRequest) {  
        return super.transformRequest(dsRequest);  
    }  
    @Override  
    protected void transformResponse(DSResponse response, DSRequest request, Object data) {  
        super.transformResponse(response, request, data);  
    }
    
    public static String getShortFormattedDate(Date in) {
		return dateParser.format(in);
	}
    public static String getFormattedDate(Date in) {
		return dateFormatter.format(in);
	}
	public static Date getDate(String in) {
		try {
			return dateParser.parse(in);
		} catch(IllegalArgumentException e) {
			e.printStackTrace();
		}
		return null;
	}
	public static String getFormattedDate(String date) {
		try {
			return dateFormatter.format(dateParser.parse(date));
		} catch(IllegalArgumentException e) {
			e.printStackTrace();
		}
		return "";
	}
}
