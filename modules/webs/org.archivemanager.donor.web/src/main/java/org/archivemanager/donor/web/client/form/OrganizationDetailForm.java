package org.archivemanager.donor.web.client.form;
import java.util.LinkedHashMap;

import org.archivemanager.donor.web.client.data.ContactDataSource;

import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;


public class OrganizationDetailForm extends DynamicForm {
	private static final int FIELD_WIDTH = 300;
	
	
	public OrganizationDetailForm() {
		setWidth100();
		setNumCols(2);
		setCellPadding(7);
		setColWidths(100, "*");
		//setBorder("1px solid #C0C3C7");
		setDataSource(ContactDataSource.getInstance());
		
		StaticTextItem idField = new StaticTextItem("id", "ID");
		
		SelectItem statusField = new SelectItem("status", "Status");
		statusField.setWidth(90);
		LinkedHashMap<String,String> valueMap = new LinkedHashMap<String,String>();
		valueMap.put("active", "Active");
		valueMap.put("inactive", "Inactive");
		valueMap.put("general", "General");
		statusField.setValueMap(valueMap);
		
		SelectItem typeField = new SelectItem("type", "Type");
		typeField.setWidth(120);
		valueMap = new LinkedHashMap<String,String>();
		valueMap.put("collectee", "Active");
		valueMap.put("donor", "Donor");
		valueMap.put("book_collection", "Book Collection");
		typeField.setValueMap(valueMap);
		
		SelectItem qnameField = new SelectItem("qname", "Contact Type");
		qnameField.setWidth(120);
		valueMap = new LinkedHashMap<String,String>();
		valueMap.put("{openapps.org_contact_1.0}individual","Individual");
		valueMap.put("{openapps.org_contact_1.0}organization","Organization");
		qnameField.setValueMap(valueMap);
		
		TextItem firstNameField = new TextItem("name", "Name");
		firstNameField.setWidth(FIELD_WIDTH);
		
		TextItem classificationField = new TextItem("classification", "Classification");
		classificationField.setWidth(FIELD_WIDTH);
		
		TextAreaItem biographyField = new TextAreaItem("biography", "Biography");
		biographyField.setWidth(FIELD_WIDTH+75);
		
		TextAreaItem descField = new TextAreaItem("note", "General Note");
		descField.setWidth(FIELD_WIDTH+75);
				
	    setFields(idField,statusField,typeField,qnameField,firstNameField,classificationField,biographyField,descField);
	}
	
	
}
