package org.archivemanager.donor.web.client.form;

import java.util.LinkedHashMap;

import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.SpacerItem;
import com.smartgwt.client.widgets.form.fields.TextItem;

public class IndividualContactForm extends DynamicForm {
	private DateItem birthDateItem;
	private DateItem deathDateItem;
	
	private static final int FIELD_WIDTH = 300;
	
	
	public IndividualContactForm() {
		setWidth100();
		setNumCols(2);
		setCellPadding(7);
		setColWidths(120, "*");
		
		SpacerItem spacer = new SpacerItem();
		spacer.setHeight(10);
		spacer.setColSpan(2);
		
		SelectItem salutationField = new SelectItem("salutation", "Salutation");
		salutationField.setWidth(60);
		LinkedHashMap<String,String> valueMap = new LinkedHashMap<String,String>();
		valueMap.put("mr", "Mr.");
		valueMap.put("mrs", "Mrs.");
		valueMap.put("ms", "Ms.");
		valueMap.put("dr", "Dr.");
		valueMap.put("prof", "Prof.");
		salutationField.setValueMap(valueMap);
		
		TextItem greetingField = new TextItem("greeting", "Greeting");
		greetingField.setWidth(FIELD_WIDTH-75);
		
		TextItem altsalutationField = new TextItem("alt_salutation", "Alternative Salutation");
		altsalutationField.setWidth(FIELD_WIDTH-75);
		
		TextItem spouseField = new TextItem("spouse", "Spouse");
		spouseField.setWidth(FIELD_WIDTH-75);
		
		TextItem faxField = new TextItem("fax1", "Fax Number");
		faxField.setWidth(FIELD_WIDTH/2);
		
		TextItem roleField = new TextItem("role", "Role");
		roleField.setWidth(FIELD_WIDTH/2);
								
		TextItem dateListField = new TextItem("dateList", "Date List");
		dateListField.setWidth(FIELD_WIDTH+75);
		
		birthDateItem = new DateItem("birth_date", "Birth Date");
		
		deathDateItem = new DateItem("death_date", "Death Date");
		
		DateItem lastWroteItem = new DateItem("last_wrote", "Last Wrote");
		
		DateItem lastShipItem = new DateItem("last_ship", "Last Shipped");
		
		setFields(spacer,salutationField,greetingField,altsalutationField,spouseField,faxField,roleField,dateListField,birthDateItem,deathDateItem,lastWroteItem,lastShipItem);
		/*	    
	   {name:'fax1',width:'*',title:'Fax'},
		*/
	}
}
