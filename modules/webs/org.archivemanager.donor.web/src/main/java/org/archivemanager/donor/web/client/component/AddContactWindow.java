package org.archivemanager.donor.web.client.component;
import java.util.LinkedHashMap;

import org.archivemanager.donor.web.client.DonorEventTypes;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.OpenAppsEvent;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;

public class AddContactWindow extends Window {
	private static final int FIELD_WIDTH = 300;
	private ButtonItem submitItem;
	private SelectItem levelItem;
	private TextItem nameField;
	private TextItem firstNameField;
	private TextItem middleNameField;
	private TextItem lastNameField;
	
	private DynamicForm addForm;
	
	public AddContactWindow() {
		setWidth(430);
		setHeight(100);
		setTitle("Add Contact");
		setAutoCenter(true);
		setIsModal(true);
		addForm = new DynamicForm();
		addForm.setMargin(3);
		addForm.setCellPadding(5);
		addForm.setColWidths(100, "*");
					
		levelItem = new SelectItem("qname", "Contact Type");
		LinkedHashMap<String,String> valueMap = new LinkedHashMap<String,String>();
		valueMap.put("{openapps.org_contact_1.0}individual","Individual");
		valueMap.put("{openapps.org_contact_1.0}organization","Organization");
		levelItem.setValueMap(valueMap);
		levelItem.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				if(event.getItem().getValue().equals("{openapps.org_contact_1.0}individual")) {
					nameField.hide();
					firstNameField.show();
					middleNameField.show();
					lastNameField.show();
					setTitle("Add Individual");
					setHeight(200);
				} else {
					firstNameField.hide();
					middleNameField.hide();
					lastNameField.hide();
					nameField.show();
					setTitle("Add Organization");
					setHeight(130);
				}
			}				
		});			
		
		nameField = new TextItem("name", "Company Name");
		nameField.setVisible(false);
		nameField.setWidth(FIELD_WIDTH);
		
		firstNameField = new TextItem("first_name", "First Name");
		firstNameField.setVisible(false);
		firstNameField.setWidth(FIELD_WIDTH);
		
		middleNameField = new TextItem("middle_name", "Middle Name");
		middleNameField.setVisible(false);
		middleNameField.setWidth(FIELD_WIDTH);
		
		lastNameField = new TextItem("last_name", "Last Name");
		lastNameField.setVisible(false);
		lastNameField.setWidth(FIELD_WIDTH);
		
		submitItem = new ButtonItem("submit", "Add");
		//submitItem.setIcon("/theme/images/icons16/add.png");
		submitItem.setStartRow(false);
		submitItem.setEndRow(false);
		submitItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
				String level = levelItem.getValueAsString();
				Record record = new Record(addForm.getValues());
				EventBus.fireEvent(new OpenAppsEvent(DonorEventTypes.ADD_DONOR, record));
				addForm.clearValues();
				hide();
				
			}
		});
		
		addForm.setFields(nameField,firstNameField,middleNameField,lastNameField,levelItem,submitItem);
		addItem(addForm);
	}
	public void selectRecord(Record record) {
		String type = record.getAttribute("localName");
		
	}
}