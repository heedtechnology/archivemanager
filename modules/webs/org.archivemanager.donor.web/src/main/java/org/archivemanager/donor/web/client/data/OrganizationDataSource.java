package org.archivemanager.donor.web.client.data;

import org.heed.openapps.gwt.client.data.NativeSecurityModel;

import com.google.gwt.core.client.JavaScriptObject;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.util.JSOHelper;

public class OrganizationDataSource extends RestDataSource {
	private static OrganizationDataSource instance = null;  
	private NativeSecurityModel security = new NativeSecurityModel();
	
	
    public static OrganizationDataSource getInstance() {  
        if (instance == null) {  
            instance = new OrganizationDataSource("attributeDS");  
        }  
        return instance;  
    }  
	
	public OrganizationDataSource(String id) {
		setID(id);  
        
        DataSourceTextField itemNameField = new DataSourceTextField("name", "Name", 128, true);  
          
        DataSourceTextField idField = new DataSourceTextField("id", null);  
        idField.setPrimaryKey(true);  
        idField.setRequired(true);  
        //parentField.setRootValue("root");  
        idField.setForeignKey("attributeDS.name");  
    
        setFields(itemNameField, idField);  
        
        setDataURL("");  
	}
	
	@Override
	protected Object transformRequest(DSRequest dsRequest) {
		JavaScriptObject data = dsRequest.getData();
		
		JSOHelper.setAttribute(data, "ticket", security.getTicket());
		
		return data;
	}
}
