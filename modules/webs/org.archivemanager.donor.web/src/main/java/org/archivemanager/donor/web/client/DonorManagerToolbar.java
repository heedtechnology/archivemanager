package org.archivemanager.donor.web.client;

import java.util.LinkedHashMap;

import org.archivemanager.donor.web.client.data.NativeContactModel;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.EventTypes;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.OpenAppsEventHandler;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class DonorManagerToolbar extends HLayout {
	private NativeContactModel model = new NativeContactModel();
	private TextItem searchField;
	private SelectItem classificationSelect;
	private ImgButton addButton;
	private ImgButton deleteButton;
	private ImgButton saveButton;
		
	private Img progressBar;
	private Label progressLabel;
	
	private Img messageImg;
	private Label messageLabel;
	private ImgButton messageImgClose;
	
	public DonorManagerToolbar() {
		setWidth("100%");
		setHeight(25);
		setLayoutLeftMargin(2);
		setMembersMargin(5);
		
		DynamicForm searchForm = new DynamicForm();
		searchForm.setWidth(300);
		searchForm.setHeight(25);
		searchForm.setMargin(0);
		searchForm.setNumCols(4);
		searchForm.setCellPadding(2);
		searchField = new TextItem("query");
		searchField.setShowTitle(false);
		searchField.setWidth(255);
		searchField.addKeyPressHandler(new KeyPressHandler() {
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if(event.getKeyName().equals("Enter")) {
					Record record = new Record();
					record.setAttribute("query", searchField.getValueAsString());
					record.setAttribute("qname", classificationSelect.getValueAsString());
					EventBus.fireEvent(new OpenAppsEvent(EventTypes.SEARCH, record));
				}				
			}
			
		});
		ButtonItem searchButton = new ButtonItem("search", "search");
		searchButton.setWidth(50);
		searchButton.setStartRow(false);
		searchButton.setEndRow(false);
		searchButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Record record = new Record();
				record.setAttribute("query", searchField.getValueAsString());
				record.setAttribute("qname", classificationSelect.getValueAsString());
				EventBus.fireEvent(new OpenAppsEvent(EventTypes.SEARCH, record));
			}
		});
				
		classificationSelect = new SelectItem("classification");
		classificationSelect.setWidth(150);		
		LinkedHashMap<String,String> values = model.getSearchTypes();
		classificationSelect.setValueMap(values);
		classificationSelect.setDefaultValue(model.getDefaultSearchType());
		classificationSelect.setShowTitle(false);
		
		searchForm.setFields(searchField,searchButton,classificationSelect);
		addMember(searchForm);		
		
		Canvas canvas = new Canvas();
		canvas.setWidth100();
		canvas.setHeight(25);
		canvas.setAlign(Alignment.CENTER);
		addMember(canvas);
		
		HLayout progress = new HLayout();
		progress.setHeight(25);
		canvas.addChild(progress);
		
		progressBar = new Img("/theme/images/loader.gif");
		progressBar.setWidth(220);
		progressBar.setHeight(20);
		progressBar.hide();
		progress.addMember(progressBar);
		
		progressLabel = new Label();
		progressLabel.setHeight(20);
		progressLabel.setWrap(false);
		progressLabel.hide();
		progress.addMember(progressLabel);
		
		HLayout message = new HLayout();
		message.setMembersMargin(5);
		//message.setBorder("1px solid black;");
		message.setHeight(25);
		canvas.addChild(message);
		
		VLayout messageImgLayout = new VLayout();
		messageImgLayout.setHeight(25);
		messageImgLayout.setAlign(VerticalAlignment.CENTER);
		messageImg = new Img("/theme/images/icons32/error.png");
		messageImg.setWidth(20);
		messageImg.setHeight(20);
		messageImg.hide();
		messageImgLayout.addMember(messageImg);
		message.addMember(messageImgLayout);
		
		VLayout messageLabelLayout = new VLayout();
		messageLabelLayout.setHeight(25);
		messageLabelLayout.setAlign(VerticalAlignment.CENTER);
		//messageLabelLayout.setBorder("1px solid black;");
		messageLabel = new Label("<label style='font-size:12px;font-weight:bold;color:#4C4C4C;font-family:Arial,Verdana,sans-serif;'>Activities</label>");
		messageLabel.setHeight(15);
		messageLabel.setWrap(false);
		messageLabel.setAutoWidth();
		messageLabelLayout.addMember(messageLabel);
		messageLabel.hide();
		message.addMember(messageLabelLayout);
		
		VLayout messageCloseLayout = new VLayout();
		messageCloseLayout.setHeight(25);
		messageCloseLayout.setWidth(25);
		messageCloseLayout.setAlign(VerticalAlignment.CENTER);
		//messageCloseLayout.setBorder("1px solid black;");
		messageImgClose = new ImgButton();
		messageImgClose.setSrc("/theme/images/icons16/remove.png");
		messageImgClose.setWidth(9);
		messageImgClose.setHeight(9);
		messageImgClose.setPrompt("close message");
		messageImgClose.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				hideMessage();
			}
		});
		messageCloseLayout.addMember(messageImgClose);
		messageImgClose.hide();		
		message.addMember(messageCloseLayout);
		
		HLayout buttons = new HLayout();
		buttons.setWidth(100);
		buttons.setMembersMargin(5);
		buttons.setAlign(Alignment.RIGHT);
		//form.setNumCols(3);
					
		addButton = new ImgButton();
		addButton.setSrc("/theme/images/icons32/add.png");
		addButton.setPrompt("Add");
		addButton.setWidth(24);
		addButton.setHeight(24);
		//addButton.setVisible(false);
		addButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				EventBus.fireEvent(new OpenAppsEvent(EventTypes.ADD));
			}
		});
		buttons.addMember(addButton);
		
		deleteButton = new ImgButton();
		deleteButton.setSrc("/theme/images/icons32/delete.png");
		deleteButton.setPrompt("Delete");
		deleteButton.setWidth(24);
		deleteButton.setHeight(24);
		deleteButton.setVisible(false);
		deleteButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				EventBus.fireEvent(new OpenAppsEvent(EventTypes.DELETE));
			}
		});
		buttons.addMember(deleteButton);
		
		saveButton = new ImgButton();
		saveButton.setSrc("/theme/images/icons32/disk.png");
		saveButton.setPrompt("Save");
		saveButton.setWidth(24);
		saveButton.setHeight(24);
		saveButton.setVisible(false);
		saveButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				EventBus.fireEvent(new OpenAppsEvent(EventTypes.SAVE));
			}
		});
		buttons.addMember(saveButton);
		
		addMember(buttons);
		
		EventBus.addHandler(org.heed.openapps.gwt.client.OpenAppsEvent.TYPE, new OpenAppsEventHandler()     {
	        @Override
	        public void onEvent(OpenAppsEvent event) {
	        	if(event.isType(EventTypes.SELECTION)) {
	        		showButtons(true,true,true);
	        	}
	        }
	    });
	}
	
	public void setQuery(String query) {
		searchField.setValue(query);
	}
	public void showAddButton(boolean show) {
		if(show) addButton.show();
		else addButton.hide();
	}
	public void showDeleteButton(boolean show) {
		if(show) deleteButton.show();
		else deleteButton.hide();
	}
	public void showSaveButton(boolean show) {
		if(show) saveButton.show();
		else saveButton.hide();
	}
	public void showButtons(boolean add, boolean delete, boolean save) {
		showAddButton(add);
		showDeleteButton(delete);
		showSaveButton(save);
	}
	public void progressStart(String message) {
		progressLabel.setContents(message);
		progressBar.show();
		progressLabel.show();
	}
	public void progressEnd() {
		progressBar.hide();
		progressLabel.hide();
	}
	public void showMessage(String message) {
		messageLabel.setContents("<label style='font-size:12px;font-weight:bold;color:#4C4C4C;font-family:Arial,Verdana,sans-serif;'>"+message+"</label>");
		messageImg.show();
		messageLabel.show();
		messageImgClose.show();
	}
	public void hideMessage() {
		messageImg.hide();
		messageLabel.hide();
		messageImgClose.hide();
	}
}
