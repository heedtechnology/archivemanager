package org.archivemanager.donor.web.client;

import org.archivemanager.donor.web.client.data.NativeContactModel;
import org.archivemanager.donor.web.client.form.IndividualDetailForm;
import org.archivemanager.donor.web.client.form.OrganizationDetailForm;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.EventTypes;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.OpenAppsEventHandler;
import org.heed.openapps.gwt.client.component.ActivitiesComponent;
import org.heed.openapps.gwt.client.component.NotesComponent;
import org.heed.openapps.gwt.client.component.TasksComponent;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class DetailPanel extends VLayout {
	private NativeContactModel model = new NativeContactModel();
	private Record selection;
	private IndividualDetailForm individualForm;
	private OrganizationDetailForm organizationForm;
	private NotesComponent notesComponent;
	private TasksComponent tasksComponent;
	private ActivitiesComponent activitiesComponent;
	
	public DetailPanel() {
		setWidth100();
		setHeight100();
		
		HLayout topLayout = new HLayout();
		topLayout.setMargin(2);
		topLayout.setMembersMargin(2);
		addMember(topLayout);
		/*
		HLayout botLayout = new HLayout();
		botLayout.setMargin(2);
		botLayout.setBorder("1px solid #C0C3C7");
		addMember(botLayout);
		*/
		VLayout leftColumn = new VLayout();
		leftColumn.setHeight(490);
		leftColumn.setWidth(505);
		//leftColumn.setBorder("1px solid #C0C3C7");
		topLayout.addMember(leftColumn);
		
		VLayout rightColumn = new VLayout();
		rightColumn.setHeight(490);
		//rightColumn.setBorder("1px solid #C0C3C7");
		topLayout.addMember(rightColumn);
		
		Canvas formCanvas = new Canvas();
		formCanvas.setHeight100();
		leftColumn.addMember(formCanvas);
		
		individualForm = new IndividualDetailForm();
		formCanvas.addChild(individualForm);
		
		organizationForm = new OrganizationDetailForm();
		organizationForm.hide();
		formCanvas.addChild(organizationForm);
		
		formCanvas.addChild(organizationForm);
		
		notesComponent = new NotesComponent("detail", "100%", true, model.getNoteTypes());
		notesComponent.setMargin(5);
		rightColumn.addMember(notesComponent);
		
		activitiesComponent = new ActivitiesComponent("100%", model.getActivityTypes());
		activitiesComponent.setMargin(5);		
		rightColumn.addMember(activitiesComponent);	
		
		tasksComponent = new TasksComponent("100%", model.getTaskTypes());
		tasksComponent.setMargin(5);		
		rightColumn.addMember(tasksComponent);
		
		Canvas canvas = new Canvas();
		canvas.setWidth100();
		canvas.setHeight100();
		rightColumn.addMember(canvas);
		
		EventBus.addHandler(org.heed.openapps.gwt.client.OpenAppsEvent.TYPE, new OpenAppsEventHandler()     {
	        @Override
	        public void onEvent(OpenAppsEvent event) {
	            if(event.isType(EventTypes.SELECTION)) {
	            	selection = event.getRecord();
	            	String qname = selection.getAttribute("qname");
	            	if(qname != null) {
	            		if(qname.equals("{openapps.org_contact_1.0}individual")) {
	            	   		//individualForm.editRecord(selection);	            	   		
	            	   		organizationForm.hide();
	            	   		individualForm.show();
	            		} else if(qname.equals("{openapps.org_contact_1.0}organization")) {
	            			//organizationForm.editRecord(selection);
	            	   		individualForm.hide();
	            	   		organizationForm.show();
	            		} else selection = null;
	            	}
	            }
	        }
	    });
	}
	public IndividualDetailForm getIndividualForm() {
		return individualForm;
	}
	public OrganizationDetailForm getOrganizationForm() {
		return organizationForm;
	}
}
