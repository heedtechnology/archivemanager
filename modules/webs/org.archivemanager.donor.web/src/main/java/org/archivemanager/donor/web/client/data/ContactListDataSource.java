package org.archivemanager.donor.web.client.data;

import org.heed.openapps.gwt.client.data.NativeSecurityModel;

import com.google.gwt.core.client.JavaScriptObject;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.util.JSOHelper;


public class ContactListDataSource extends RestDataSource {
	private static ContactListDataSource instance = null;  
	private NativeSecurityModel security = new NativeSecurityModel();
	
    public static ContactListDataSource getInstance() {  
        if (instance == null) {  
            instance = new ContactListDataSource("contactListDS");  
        }  
        return instance;  
    } 
	
	public ContactListDataSource(String id) {
		setID(id);  
        
		DataSourceTextField idField = new DataSourceTextField("id", "ID",50);  
        idField.setPrimaryKey(true);  
        idField.setRequired(true);
        
		setFields(idField);
		
    	setFetchDataURL("/service/entity/search.xml"); 
    	setAddDataURL("/service/entity/create.xml");
    	setUpdateDataURL("/service/entity/update.xml"); 
    	setRemoveDataURL("/service/entity/remove.xml");
	}
	
	@Override
	protected Object transformRequest(DSRequest dsRequest) {
		JavaScriptObject data = dsRequest.getData();
		
		JSOHelper.setAttribute(data, "ticket", security.getTicket());
		
		return data;
	}
    @Override  
    protected void transformResponse(DSResponse response, DSRequest request, Object data) {  
    	try {
			if(response.getData() != null && response.getData().length > 0) {
				for(int i=0; i < response.getData().length; i++) {
					Record entity = response.getData()[i];
					if(entity != null) {
						//String parent = entity.getAttribute("parent");
						//String type = entity.getAttribute("localName");
						String title = entity.getAttribute("name");
						if(title == null || title.length() == 0) {
							String first_name = entity.getAttribute("first_name");
							String last_name = entity.getAttribute("last_name");
							entity.setAttribute("name", first_name+" "+last_name);
						}
					}
				}
			}
		} catch(ClassCastException e) {
				
		}
		super.transformResponse(response, request, data);  
    }	
}
