package org.archivemanager.donor.web.client.form;

import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.TextItem;

public class AddressForm extends DynamicForm {
	private static final int FIELD_WIDTH = 300;
	
	
	public AddressForm() {
		
		TextItem address1Field = new TextItem("address1", "Address 1");
		address1Field.setWidth(FIELD_WIDTH);

		TextItem address2Field = new TextItem("address2", "Address 2");
		address2Field.setWidth(FIELD_WIDTH);
		
		TextItem cityField = new TextItem("city", "City");
		cityField.setWidth(FIELD_WIDTH/2);
		
		TextItem stateField = new TextItem("state", "State/Province");
		stateField.setWidth(FIELD_WIDTH/3);
		
		TextItem zipField = new TextItem("zip", "Postal Code");
		zipField.setWidth(FIELD_WIDTH/3);
		
		CheckboxItem primaryItem = new CheckboxItem("primary", "Primary");
		
		setFields(address1Field,address2Field,cityField,stateField,zipField,primaryItem);
		
	}
}
