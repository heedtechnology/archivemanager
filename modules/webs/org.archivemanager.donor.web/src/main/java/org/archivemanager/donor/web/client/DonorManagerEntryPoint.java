package org.archivemanager.donor.web.client;
import java.util.Date;

import org.archivemanager.donor.web.client.component.AddContactWindow;
import org.archivemanager.donor.web.client.data.ContactDataSource;
import org.archivemanager.donor.web.client.data.ContactListDataSource;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.EventTypes;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.OpenAppsEventHandler;
import org.heed.openapps.gwt.client.data.RestUtility;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.DOM;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.rpc.HandleErrorCallback;
import com.smartgwt.client.rpc.RPCManager;
import com.smartgwt.client.types.Positioning;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;


public class DonorManagerEntryPoint extends VLayout implements EntryPoint {
	private static final int height = 680;
	private static final int width = 1200;
	
	private Record selection;
	private DonorManagerToolbar toolbar;
	private ListGrid contactList;
	private AddContactWindow addWindow;
	private DetailPanel detailPanel;
	private ContactPanel contactPanel;
	private CorrespondencePanel correspondencePanel;
	private DigitalObjectPanel digitalObjectPanel;
	
	
	public void onModuleLoad() {  
		setHeight(height);  
        setWidth(width);
        
        toolbar = new DonorManagerToolbar();
        addMember(toolbar);
        
        HLayout bodyLayout = new HLayout();
        addMember(bodyLayout);
        
        contactList = new ListGrid();
        contactList.setWidth(310);
        contactList.setHeight100();
        contactList.setDataSource(ContactListDataSource.getInstance());
        ListGridField nameField = new ListGridField("name","Name");
        contactList.setFields(nameField);
        contactList.addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				String id = contactList.getSelectedRecord().getAttribute("id");
				final String qname = event.getRecord().getAttribute("qname");				
            	final String parentQName = event.getRecord().getAttribute("parentQName");
            	RestUtility.get("/service/entity/get/"+id+".xml", new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						Record record = response.getData()[0];
						EventBus.fireEvent(new OpenAppsEvent(EventTypes.SELECTION, record));
						if((qname != null && qname.equals("{openapps.org_contact_1.0}individual")) || (parentQName != null && parentQName.equals("{openapps.org_contact_1.0}individual"))) {
							detailPanel.getIndividualForm().editRecord(record);
							contactPanel.getIndividualForm().editRecord(record);	
						} else if((qname != null && qname.equals("{openapps.org_contact_1.0}organization")) || (parentQName != null && parentQName.equals("{openapps.org_contact_1.0}organization"))) {
							detailPanel.getOrganizationForm().editRecord(record);
							contactPanel.getOrganizationForm().editRecord(record);							
						}
					}						
				});
			}
		});
        bodyLayout.addMember(contactList);
        
        Canvas mainLayout = new Canvas();
        mainLayout.setWidth100();
        mainLayout.setHeight100();
        mainLayout.setBorder("1px solid #C0C3C7");
        bodyLayout.addMember(mainLayout);
        
        TabSet tabs = new TabSet();
        tabs.setWidth100();
        tabs.setHeight100();
        tabs.setMargin(2);
        
        detailPanel = new DetailPanel();
        Tab detailsTab = new Tab("Details");
        detailsTab.setPane(detailPanel);
		tabs.addTab(detailsTab);
        
		contactPanel = new ContactPanel();
        Tab contactTab = new Tab("Contact Information");
        contactTab.setPane(contactPanel);
		tabs.addTab(contactTab);
        mainLayout.addChild(tabs);
        
        correspondencePanel = new CorrespondencePanel();
        Tab correspondenceTab = new Tab("Correspondence");
        correspondenceTab.setPane(correspondencePanel);
		tabs.addTab(correspondenceTab);
        mainLayout.addChild(tabs);
        		
        digitalObjectPanel = new DigitalObjectPanel();
        Tab digitalObjectTab = new Tab("Digital Objects");
        digitalObjectTab.setPane(digitalObjectPanel);
		tabs.addTab(digitalObjectTab);
        mainLayout.addChild(tabs);
        
		addWindow = new AddContactWindow();
		
        EventBus.addHandler(org.heed.openapps.gwt.client.OpenAppsEvent.TYPE, new OpenAppsEventHandler()     {
	        @Override
	        public void onEvent(OpenAppsEvent event) {
	            if(event.isType(EventTypes.SEARCH)) {
	            	Criteria criteria = new Criteria();
	        		criteria.setAttribute("qname", event.getRecord().getAttribute("qname"));
	        		criteria.setAttribute("query", event.getRecord().getAttribute("query"));
	        		criteria.setAttribute("field", "name");
	        		criteria.setAttribute("sort", "name_e");
	        		criteria.setAttribute("sources", "false");
	        		contactList.fetchData(criteria);
	            } else if(event.isType(EventTypes.SELECTION)) {
	            	String name = event.getRecord().getAttribute("name");
	            	if(name == null || name.length() == 0) {
						String first_name = event.getRecord().getAttribute("first_name");
						String last_name = event.getRecord().getAttribute("last_name");
						event.getRecord().setAttribute("name", first_name+" "+last_name);
					}
	            	/*
	            	if(contactList.getSelectedRecord() != null) {
	            		contactList.getSelectedRecord().setAttribute("name", name);
	            		contactList.refreshFields();
	            	}
	            	*/
					selection = event.getRecord();
	            } else if(event.isType(DonorEventTypes.ADD_DONOR)) {
	            	contactList.addData(event.getRecord(), new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							
						}
					});
	            } else if(event.isType(EventTypes.ADD)) {
	            	addWindow.show();
	            } else if(event.isType(EventTypes.DELETE)) {
	            	contactList.removeSelectedData();
	            } else if(event.isType(EventTypes.SAVE)) {
	 	            if(selection != null) {
	 	            	String qname = selection.getAttribute("qname");
	 	            	if(qname != null) {
	 	            		Record record = null;
		            		if(qname.equals("{openapps.org_contact_1.0}individual")) {
		            			record = new Record(detailPanel.getIndividualForm().getValues());
		            			for(FormItem item : detailPanel.getIndividualForm().getFields()) {
		            				if(item.getForm().isVisible() && item.getVisible()) {
		            					if(item instanceof DateItem) {
		            						Date value = ((DateItem)item).getValueAsDate();
		            						record.setAttribute(item.getName(), DateTimeFormat.getShortDateFormat().format(value));
		            					} else {
		            						if(item.getValue() != null) record.setAttribute(item.getName(), item.getValue());
		            					}
		            				}
		            			}
		            		} else if(qname.equals("{openapps.org_contact_1.0}organization")) {
		            			record = new Record(detailPanel.getOrganizationForm().getValues());
		            			for(FormItem item : detailPanel.getOrganizationForm().getFields()) {
		            				if(item.getForm().isVisible() && item.getVisible()) {
		            					if(item instanceof DateItem) {
		            						Date value = ((DateItem)item).getValueAsDate();
		            						record.setAttribute(item.getName(), DateTimeFormat.getShortDateFormat().format(value));
		            					} else {
		            						if(item.getValue() != null) record.setAttribute(item.getName(), item.getValue());
		            					}
		            				}
		            			}
		            		}
		            		if(record != null) {
		            			try {
		            				Date date = record.getAttributeAsDate("birth_date");
		            				String birth_date = ContactDataSource.getShortFormattedDate(date);
		            				record.setAttribute("birth_date", birth_date);
		            			} catch(Exception e) { record.setAttribute("birth_date", ""); }
		            			try {
		            				Date date = record.getAttributeAsDate("death_date");
		            				String death_date = ContactDataSource.getShortFormattedDate(record.getAttributeAsDate("death_date"));
			            			record.setAttribute("death_date", death_date);
		            			} catch(Exception e) { record.setAttribute("death_date", ""); }
		            			try {
		            				Date date = record.getAttributeAsDate("last_wrote");
		            				String last_wrote = ContactDataSource.getShortFormattedDate(date);
			            			record.setAttribute("last_wrote", last_wrote);
		            			} catch(Exception e) { record.setAttribute("last_wrote", ""); }
		            			try {
		            				Date date = record.getAttributeAsDate("last_ship");
		            				String last_shipped = ContactDataSource.getShortFormattedDate(date);
		            				record.setAttribute("last_ship", last_shipped);
		            			} catch(Exception e) { record.setAttribute("last_ship", ""); }
		            			RestUtility.post("/service/entity/update.xml", record, new DSCallback() {
		            				public void execute(DSResponse response, Object rawData, DSRequest request) {
		            					Record record = response.getData()[0];
		            					EventBus.fireEvent(new OpenAppsEvent(EventTypes.SELECTION, record));
		            				}						
		            			});	
		            		}
	 	            	}
	 	            }
	 	        }
	        }
	    });
        
        RPCManager.setHandleErrorCallback(new HandleErrorCallback() {
			@Override
			public void handleError(DSResponse response, DSRequest request) {
				int httpCode = response.getHttpResponseCode();
				if(httpCode == 404) SC.warn("Error contacting the server, please check your internet connection and try again.");
			}
		});
        
        setHtmlElement(DOM.getElementById("gwt"));
        setPosition(Positioning.RELATIVE);
        draw();
    }  
	
}
