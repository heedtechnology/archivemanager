package org.archivemanager.donor.web.server.controller;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.heed.openapps.entity.EntityService;
import org.heed.openapps.security.SecurityService;
import org.heed.openapps.theme.ThemeVariables;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ContactManagerController implements ApplicationContextAware {
	@Autowired protected SecurityService securityService;
	@Autowired protected EntityService entityService;
	
	
	public void setApplicationContext(ApplicationContext ctx) throws BeansException {
		
	}
	
	@RequestMapping(value="/manager", method = RequestMethod.GET)
	public ModelAndView manager(HttpServletRequest req, HttpServletResponse res) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		ThemeVariables vars = new ThemeVariables("smartclient");
		parms.put("theme", vars);
		//User user = securityService.getCurrentUser(req);
		//parms.put("modes", getContactImportProcessors());
		return new ModelAndView("home", parms);
	}
	@RequestMapping(value="/organizations", method = RequestMethod.GET)
	public ModelAndView organizations(HttpServletRequest req, HttpServletResponse res) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		ThemeVariables vars = new ThemeVariables("base");
		parms.put("theme", vars);
		return new ModelAndView("organizations", parms);
	}
	@RequestMapping(value="/correspondence", method = RequestMethod.GET)
	public ModelAndView correspondence(HttpServletRequest req, HttpServletResponse res) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		ThemeVariables vars = new ThemeVariables("base");
		parms.put("theme", vars);
		return new ModelAndView("correspondence", parms);
	}
	@RequestMapping(value="/addresses", method = RequestMethod.GET)
	public ModelAndView addresses(HttpServletRequest req, HttpServletResponse res) throws Exception {
		StringBuffer buff = new StringBuffer();
		
		return response(buff.toString(), res.getWriter());
	}
	/*
	protected String getContactImportProcessors() {
		StringBuffer buff = new StringBuffer("{'':'',");
		for(String key : entityService.getImportProcessors().keySet()) {
			if(key.equals(ContactsModel.CONTACT.toString())) {
				ImportProcessor processor = entityService.getImportProcessors().get(key);
				buff.append("'"+key+"':'"+processor.getName()+"',");
			}
		}
		buff.replace(buff.length()-1, buff.length(), "}");
		return buff.toString();
	}
	*/
	protected ModelAndView response(String data, PrintWriter out) {
		out.write("<response><status>0</status><message></message><data>"+data+"</data></response>");
		return null;
	}
}
