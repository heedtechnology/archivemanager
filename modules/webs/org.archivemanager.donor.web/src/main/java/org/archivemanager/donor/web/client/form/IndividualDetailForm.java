package org.archivemanager.donor.web.client.form;
import java.util.LinkedHashMap;

import org.archivemanager.donor.web.client.data.ContactDataSource;
import org.archivemanager.donor.web.client.data.NativeContactModel;

import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.SpacerItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;


public class IndividualDetailForm extends DynamicForm {
	private static final int FIELD_WIDTH = 300;
	private NativeContactModel model = new NativeContactModel();
		
	
	public IndividualDetailForm() {
		setWidth100();
		setNumCols(2);
		setCellPadding(7);
		setColWidths(100, "*");
		//setBorder("1px solid #C0C3C7");
		setDataSource(ContactDataSource.getInstance());
		
		StaticTextItem idField = new StaticTextItem("id", "ID");
				
		SelectItem statusField = new SelectItem("status", "Status");
		statusField.setWidth(90);
		LinkedHashMap<String,String> valueMap = new LinkedHashMap<String,String>();
		valueMap.put("active", "Active");
		valueMap.put("inactive", "Inactive");
		valueMap.put("general", "General");
		statusField.setValueMap(valueMap);
		
		SelectItem typeField = new SelectItem("type", "Type");
		typeField.setWidth(120);
		valueMap = new LinkedHashMap<String,String>();
		valueMap.put("collectee", "Active");
		valueMap.put("donor", "Donor");
		valueMap.put("book_collection", "Book Collection");
		typeField.setValueMap(valueMap);
		
		SelectItem qnameField = new SelectItem("qname", "Contact Type");
		qnameField.setWidth(120);
		qnameField.setValueMap(model.getSearchTypes());
		
		TextItem firstNameField = new TextItem("first_name", "First Name");
		firstNameField.setWidth(FIELD_WIDTH);
		
		TextItem middleNameField = new TextItem("middle_name", "Middle Name");
		middleNameField.setWidth(FIELD_WIDTH);
		
		TextItem lastNameField = new TextItem("last_name", "Last Name");
		lastNameField.setWidth(FIELD_WIDTH);
		
		TextItem suffixField = new TextItem("suffix", "Suffix");
		suffixField.setWidth(FIELD_WIDTH);
		
		TextItem nickNameField = new TextItem("nick_name", "Nick Name");
		nickNameField.setWidth(FIELD_WIDTH);
		
		TextAreaItem descField = new TextAreaItem("note", "General Note");
		descField.setWidth(FIELD_WIDTH+75);
		
		TextAreaItem biographyField = new TextAreaItem("biography", "Biography");
		biographyField.setWidth(FIELD_WIDTH+75);
		
		TextItem bioField = new TextItem("bio_sources", "Bio Sources");
		bioField.setWidth(FIELD_WIDTH+75);
		
		SpacerItem spacer = new SpacerItem();
		spacer.setHeight(10);
		spacer.setColSpan(2);
				
	    setFields(idField,statusField,typeField,qnameField,firstNameField,middleNameField,lastNameField,
	    		suffixField,nickNameField,descField,biographyField,bioField,spacer);
	}
		
}
