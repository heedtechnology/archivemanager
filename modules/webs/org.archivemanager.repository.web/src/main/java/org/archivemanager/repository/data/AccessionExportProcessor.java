package org.archivemanager.repository.data;

import org.heed.openapps.ContactModel;
import org.heed.openapps.entity.Association;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.DefaultExportProcessor;

public class AccessionExportProcessor extends DefaultExportProcessor {

	
	public String toXml(Association association, boolean printSource, boolean isTree) {
		if(association.getQname().equals(ContactModel.ADDRESSES)) {
			try {
				Entity accession = getEntityService().getEntity(association.getSource());
				if(accession != null && accession.getId() != null) {
					getBuffer().append("<parent>"+accession.getId()+"</parent>");
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		return super.toXml(association, printSource, isTree);
	}
}
