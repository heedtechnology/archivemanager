package org.archivemanager.repository.server;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.heed.openapps.ContactModel;
import org.heed.openapps.QName;
import org.heed.openapps.RepositoryModel;
import org.heed.openapps.SystemModel;
import org.heed.openapps.entity.Association;
import org.heed.openapps.entity.BaseEntityQuery;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityQuery;
import org.heed.openapps.entity.EntityResultSet;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.entity.Property;
import org.heed.openapps.entity.ValidationResult;
import org.heed.openapps.entity.EntitySorter;
import org.heed.openapps.entity.ExportProcessor;
import org.heed.openapps.entity.data.FormatInstructions;
import org.heed.openapps.template.TemplateService;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AccessionController implements ApplicationContextAware {
	@Autowired protected EntityService entityService;
	@Autowired protected TemplateService templateService;
	private EntitySorter accessionSorter = new EntitySorter(RepositoryModel.ACCESSION_DATE);
	private EntitySorter collectionSorter = new EntitySorter(SystemModel.OPENAPPS_NAME);
	
	public void setApplicationContext(ApplicationContext ctx) throws BeansException {
		
	}
	
	@RequestMapping(value="/accessions", method = RequestMethod.GET)
	public ModelAndView accessions(HttpServletRequest req, HttpServletResponse res) throws Exception {
		StringBuffer buff = new StringBuffer();
		String parent = req.getParameter("collection");
		Entity collection = entityService.getEntity(Long.valueOf(parent));
		List<Entity> list = new ArrayList<Entity>();
		for(Association assoc : collection.getSourceAssociations(RepositoryModel.ACCESSIONS)) {
			Entity accession = entityService.getEntity(assoc.getTarget());
			list.add(accession);
		}
		Collections.sort(list, accessionSorter);
		for(Entity accession : list) {	
			//buff.append(generator.toXml(accession, true, true));
		}
		res.getWriter().append("<response><status>0</status><data>"+buff.toString()+"</data></response>");
		return null;
	}
	@RequestMapping(value="/accession", method = RequestMethod.GET)
	public ModelAndView fetchCollectionTreeData(HttpServletRequest req, HttpServletResponse res) throws Exception {
		StringBuffer buff = new StringBuffer();
		String parent = req.getParameter("parent");
		String field = req.getParameter("field");
		String query = req.getParameter("query");
		int total = 0,end=0;
		if(parent == null || parent.equals("null")) {
			if(field.equals("name")) {
				EntityQuery eQuery = (query != null) ? new BaseEntityQuery(RepositoryModel.COLLECTION, query, "name_e", true) : new BaseEntityQuery(RepositoryModel.COLLECTION, null, "name_e", true);
				if(eQuery.getQueryString() != null) eQuery.setEndRow(0);
				eQuery.getFields().add("name");
				EntityResultSet collections = entityService.search(eQuery);
				total = collections.getResultSize();
				end = collections.getEndRow();
				for(Entity collection : collections.getResults()) {
					Property name = collection.getProperty(SystemModel.OPENAPPS_NAME);
					buff.append("<node id='"+collection.getId()+"' localName='collection' parent='0'><title><![CDATA["+name.toString()+"]]></title></node>");
				}
			} else {
				EntityQuery eQuery =new BaseEntityQuery(RepositoryModel.ACCESSION, query, "name_e", true);
				eQuery.getFields().add("date");
				List<Entity> collections = new ArrayList<Entity>();
				EntityResultSet accessions = entityService.search(eQuery);
				for(Entity accession : accessions.getResults()) {
					Association collectionAssoc = accession.getTargetAssociation(RepositoryModel.ACCESSIONS);
					if(collectionAssoc != null) {
						Entity collection = entityService.getEntity(collectionAssoc.getSource());
						if(!collections.contains(collection)) collections.add(collection);
					}
				}
				Collections.sort(collections, collectionSorter);
				for(Entity collection : collections) {
					Property name = collection.getProperty(SystemModel.OPENAPPS_NAME);
					buff.append("<node id='"+collection.getId()+"' localName='collection' parent='0'><title><![CDATA["+name.toString()+"]]></title></node>");
				}
			}
		} else {
			Entity collection = entityService.getEntity(Long.valueOf(parent));
			ExportProcessor processor = entityService.getExportProcessor("default");
			for(Association assoc : collection.getSourceAssociations(RepositoryModel.ACCESSIONS)) {
				assoc.setSortField(RepositoryModel.ACCESSION_DATE);
			}
			//entityService.hydrate(collection);
			for(Association assoc : collection.getSourceAssociations(RepositoryModel.ACCESSIONS)) {
				buff.append(processor.export(new FormatInstructions(true), assoc));
			}
			res.getWriter().append("<response><status>0</status><data>"+buff.toString()+"</data></response>");
			return null;
			
		}
		return response("", total, 0, end, buff.toString(), res.getWriter());
	}
	@RequestMapping(value="/accession/add", method = RequestMethod.POST)
	public ModelAndView entityAssociationAdd(HttpServletRequest request, HttpServletResponse response, 
			@RequestParam("assoc_qname") String assocQname, @RequestParam("entity_qname") String entityQname,
			@RequestParam("source_id") long source) throws Exception {
		QName aQname = QName.createQualifiedName(assocQname);
		QName eQname = QName.createQualifiedName(entityQname);
		Entity entity = entityService.getEntity(request, eQname);
		ValidationResult entityResult = entityService.validate(entity);
		if(entityResult.isValid()) {
			ExportProcessor processor = entityService.getExportProcessor(entity.getQname().toString());
			if(processor == null) processor = entityService.getExportProcessor("default");
			//entity.setParent(entityService.getEntity(source));
			if(entity.getId() != null) {
				entityService.updateEntity(entity, true);
				//DataGenerator generator = entityService.getDataGenerator(eQname);
				response(0, "Accession updated successfully", processor.export(new FormatInstructions(), entity), response.getWriter());
			} else {
				entityService.addEntity(entity);
				Association assoc = new Association(aQname, source, entity.getId());
				ValidationResult assocResult = entityService.validate(assoc);
				if(assocResult.isValid()) {
					entityService.addAssociation(assoc);
					entity.getTargetAssociations().add(assoc);
					//DataGenerator generator = entityService.getDataGenerator(entity.getQname());
					response(0, "", processor.export(new FormatInstructions(), entity), response.getWriter());
				} else response(0, "Accession added successfully", assocResult.toXml(), response.getWriter());
			}
		} else response(0, "", entityResult.toXml(), response.getWriter());
		return null;
	}
	@RequestMapping(value="/accession/update", method = RequestMethod.POST)
	public ModelAndView moveCollection(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("id") Long id, @RequestParam("parent") Long parent) throws Exception {
		Entity source = entityService.getEntity(id);
		Entity target = entityService.getEntity(parent);
		Association assoc = source.getTargetAssociation(RepositoryModel.ACCESSIONS);
		if(assoc != null && source != null && target != null) {
			assoc.setSource(target.getId());
			entityService.updateAssociation(assoc);
		}
		ExportProcessor processor = entityService.getExportProcessor(source.getQname().toString());
		if(processor == null) processor = entityService.getExportProcessor("default");
		//DataGenerator generator = entityService.getDataGenerator(source.getQname());
		return response(0, "", processor.export(new FormatInstructions(), source), response.getWriter());
	}
	@RequestMapping(value="/accession/contact/remove", method = RequestMethod.POST)
	public ModelAndView contactRemove(HttpServletRequest req, HttpServletResponse res, @RequestParam("id") Long id) throws Exception {
		Association assoc = entityService.getAssociation(id);
		Entity accession = entityService.getEntity(assoc.getSource());
		Entity contact = entityService.getEntity(assoc.getTarget());
		for(Association addressAssoc : contact.getSourceAssociations(ContactModel.ADDRESSES)) {
			Entity address = entityService.getEntity(addressAssoc.getTarget());
			for(Association accessionAddressAssoc : accession.getSourceAssociations(ContactModel.ADDRESSES)) {
				if(accessionAddressAssoc.getTarget().equals(address.getId()))
					entityService.removeAssociation(accessionAddressAssoc.getId());
			}
		}
		entityService.removeAssociation(id);
		return response(0, "", "<node id='"+id+"' />", res.getWriter());
	}
	protected ModelAndView response(int status, String message, String data, PrintWriter out) {
		out.write("<response><status>"+status+"</status><message>"+message+"</message><data>"+data+"</data></response>");
		return null;
	}
	protected ModelAndView response(String message, int total, int start, int end, String data, PrintWriter out) {
		out.write("<response><status>0</status><totalRows>"+total+"</totalRows><startRow>"+start+"</startRow><endRow>"+end+"</endRow><message>"+message+"</message><data>"+data+"</data></response>");
		return null;
	}
}
