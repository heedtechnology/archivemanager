package org.archivemanager.repository.client.data;

import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.RestDataSource;


public class ItemDS extends RestDataSource {
	private static ItemDS instance = null;  
	  
    public static ItemDS getInstance() {  
        if (instance == null) {  
            instance = new ItemDS("itemDS");  
        }  
        return instance;  
    }
    
	public ItemDS(String id) {
		setID(id);
		/*
		DataSourceBooleanField paidField = new DataSourceBooleanField("paid");  
		DataSourceBooleanField appraisalItem = new DataSourceBooleanField("appraisal");
		DataSourceBooleanField newCollectionField = new DataSourceBooleanField("new_collection");
		DataSourceBooleanField acknowledgedField = new DataSourceBooleanField("acknowledged");
		DataSourceBooleanField existingCollectionField = new DataSourceBooleanField("existing_collection");
		DataSourceDateField dateField = new DataSourceDateField("date");
		
		setFields(paidField,appraisalItem,newCollectionField,acknowledgedField,existingCollectionField,dateField);
		*/
		
		setAddDataURL("/service/archivemanager/repository/collection/add.xml");
		setFetchDataURL("/service/entity/get.xml");
		setRemoveDataURL("/service/entity/remove.xml");
		setUpdateDataURL("/service/entity/update.xml");
	}
	
	@Override
	protected void transformResponse(DSResponse response, DSRequest request,Object data) {
		// TODO Auto-generated method stub
		super.transformResponse(response, request, data);
	}
}