package org.archivemanager.repository.client;

import java.util.Date;

import org.archivemanager.repository.client.accession.AccessionPanel;
import org.archivemanager.repository.client.accession.AccessionTree;
import org.heed.openapps.gwt.client.Application;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;


public class AccessionApplication extends HLayout implements Application {
	private RepositoryManagerEntryPoint mgr;
	private AccessionTree tree;
	private TabSet tabs;
	private AccessionPanel accessionPanel;
	
	private AddAccessionWindow addWindow;
	
	public AccessionApplication(RepositoryManagerEntryPoint mgr) {
		this.mgr = mgr;
		setWidth100();
		setHeight100();
		setMembersMargin(5);
		
		tree = new AccessionTree(this);
		tree.setWidth(300);
        //tree.setHeight("100%");
        tree.setShowResizeBar(true);
        addMember(tree);
        
        tabs = new TabSet();
		tabs.setWidth100();
		tabs.setHeight100();
		addMember(tabs);
		
		Tab homeTab = new Tab("Home");
		accessionPanel = new AccessionPanel();
		homeTab.setPane(accessionPanel);
        tabs.addTab(homeTab);
        
        tabs.hide();
        
        addWindow = new AddAccessionWindow();
        
	}
	
	@Override
	public void select(Record record) {
		String type = record.getAttribute("localName");
		if(type.equals("accession")) {
			Criteria criteria = new Criteria();
			criteria.setAttribute("id", record.getAttribute("id"));
			accessionPanel.fetchData(criteria, new DSCallback() {
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					if(response.getData() != null && response.getData().length > 0) {
						Record accession = response.getData()[0];
						if(accession != null) {
							String date = accession.getAttribute("title");
							String title = tree.getSelectedRecord().getAttribute("title");
							if(title != null && !title.equals(date)) {
								tree.getSelectedRecord().setAttribute("title", date);
								tree.refreshFields();
							}
							accessionPanel.select(accession);
						}
					}
				}
			});
			accessionPanel.select(record);
			accessionPanel.show();
			mgr.getToolbar().showAddButton(false);
			mgr.getToolbar().showDeleteButton(true);
			mgr.getToolbar().showSaveButton(true);
		} else {
			accessionPanel.hide();
			mgr.getToolbar().showAddButton(true);
			mgr.getToolbar().showDeleteButton(false);
			mgr.getToolbar().showSaveButton(false);
		}
	}
	@Override
	public void save() {
		accessionPanel.save(new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				if(response.getData() != null && response.getData().length > 0) {
					Record accession = response.getData()[0];
					if(accession != null) {
						String date = accession.getAttribute("title");
						String title = tree.getSelectedRecord().getAttribute("title");
						if(title != null && !title.equals(date)) {
							tree.getSelectedRecord().setAttribute("title", date);
							tree.refreshFields();
						}
					}
				}
			}
		});		
	}
	public void importEntity() {
		
	}
	public void exportEntity() {
		
	}
	@Override
	public void add() {
		addWindow.show();
	}
	@Override
	public void delete() {
		tree.removeSelectedData();
	}
	@Override
	public void search(String query) {
		if(!mgr.getToolbar().isDateSearch()) {
			Criteria criteria = new Criteria();
			criteria.setAttribute("qname", "{openapps.org_repository_1.0}collection");
			criteria.setAttribute("query", query);
			criteria.setAttribute("field", "name");
			criteria.setAttribute("sort", "name_e");
			tree.fetchData(criteria);
		} else {
			Criteria criteria = new Criteria();
			criteria.setAttribute("qname", "{openapps.org_repository_1.0}accession");
			criteria.setAttribute("query", query);
			criteria.setAttribute("field", "date");
			criteria.setAttribute("sort", "name_e");
			tree.fetchData(criteria);
		}
		tabs.show();
	}

	public class AddAccessionWindow extends Window {
		@SuppressWarnings("deprecation")
		public AddAccessionWindow() {
			setWidth(400);
			setHeight(100);
			setTitle("Add Accession");
			setAutoCenter(true);
			setIsModal(true);
			DynamicForm addForm = new DynamicForm();
			addForm.setCellPadding(7);
			final DateItem dateItem = new DateItem("date", "Date");
			dateItem.setStartDate(new Date(1990, 1, 1));
						
			ButtonItem submitItem = new ButtonItem("submit", "Add");
			submitItem.setIcon("/theme/images/icons16/add.png");
			submitItem.setStartRow(false);
			submitItem.setEndRow(false);
			submitItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					Record record = new Record();
					record.setAttribute("assoc_qname", "{openapps.org_repository_1.0}accessions");
					record.setAttribute("entity_qname", "{openapps.org_repository_1.0}accession");
					record.setAttribute("source_id", tree.getSelectedRecord().getAttribute("id"));
					record.setAttribute("date", dateItem.getValueAsDate());
					tree.addData(record, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							addWindow.hide();
						}
					});
				}
			});
			
			addForm.setFields(dateItem,submitItem);
			addItem(addForm);
		}
	}

}
