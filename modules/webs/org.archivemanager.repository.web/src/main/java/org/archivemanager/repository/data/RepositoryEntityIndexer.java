package org.archivemanager.repository.data;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.heed.openapps.ClassificationModel;
import org.heed.openapps.QName;
import org.heed.openapps.RepositoryModel;
import org.heed.openapps.SystemModel;
import org.heed.openapps.entity.Association;
import org.heed.openapps.entity.DefaultEntityIndexer;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.entity.InvalidEntityException;
import org.heed.openapps.entity.Property;
import org.heed.openapps.InvalidQualifiedNameException;
import org.heed.openapps.node.Node;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;


public class RepositoryEntityIndexer extends DefaultEntityIndexer implements ApplicationContextAware {
	private EntityService entityService;
	
	private StringBuffer freeText = new StringBuffer();
	
	/*
	public static final RelationshipType nameType = new RelationshipType(ClassificationModel.NAMED_ENTITIES);
	public static final RelationshipType subjectType = new RelationshipType(ClassificationModel.SUBJECTS);
	public static final RelationshipType noteType = new RelationshipType(SystemModel.NOTES);
	public static final RelationshipType collectionType = new RelationshipType(RepositoryModel.COLLECTIONS);
	public static final RelationshipType categories = new RelationshipType(RepositoryModel.CATEGORIES);
	*/
	
	@Override
	public void setApplicationContext(ApplicationContext ctx) throws BeansException {
		entityService = (EntityService)ctx.getBean(EntityService.class);
	}
	@Override
	public void index(QName qname, Entity entity) {
		/*
		if(node.getEntries().size() > 0) {
			for(ACE entry : node.getEntries()) {
				String entryName = entry.getACEType().name();
				Field field = new Field(entryName, String.valueOf(entry.getAuthority()), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS, Field.TermVector.NO);
				field.setBoost(luceneOptions.getBoost());
				document.add(field);
			}
		}
		
		List<Node> names = new ArrayList<Node>();
		List<Node> subjects = new ArrayList<Node>();		
		*/
		Association parent_assoc = entity.getTargetAssociation(RepositoryModel.CATEGORIES);
		if(parent_assoc == null) parent_assoc = entity.getTargetAssociation(RepositoryModel.ITEMS);
		if(parent_assoc != null && parent_assoc.getSource() != null) {
			try {
				Entity parent = entityService.getEntity(parent_assoc.getSource());
				//long parentId = parent.getId();
				String parent_qname = String.valueOf(parent.getProperty("qname"));
				entityService.index(entity.getId(), qname.toString(), "parent_id", parent.getId());
				entityService.index(entity.getId(), qname.toString(), "parent_qname", parent_qname);
				StringBuffer buff = new StringBuffer();
				while(parent != null) {
					/*
					for(Relationship noteAssoc : parent.getRelationships(nameType, Direction.OUTGOING)) {
						Node nameNode = noteAssoc.getEndNode();
						if(!names.contains(nameNode)) names.add(nameNode);
					}
					for(Relationship noteAssoc : parent.getRelationships(subjectType, Direction.OUTGOING)) {
						Node subjNode = noteAssoc.getEndNode();
						if(!subjects.contains(subjNode)) subjects.add(subjNode);
					}
					 */
					buff.insert(0, parent.getId()+" ");
					//Relationship rel = parent.getSingleRelationship(categories, Direction.INCOMING);
					//if(rel == null) {
						//rel = parent.getSingleRelationship(collectionType, Direction.INCOMING);
						/*
						for(Relationship relation : parent.getRelationships(collectionType, Direction.INCOMING)) {
							QName relQname = getQName(relation.getStartNode());
							if(relQname.equals(RepositoryModel.REPOSITORY)) rel = relation;
						}
						 */
					//}
					//parent = rel != null ? rel.getStartNode() : null;
				}
				entityService.index(entity.getId(), qname.toString(), "path", buff.toString().trim());
			} catch(InvalidEntityException e) {
				e.printStackTrace();
			}
		}
		for(Association assoc : entity.getSourceAssociations()) {			
			try {
				Entity node = entityService.getEntity(assoc.getTarget());
				QName nodeQname = QName.createQualifiedName(node.getProperty("qname").toString());
				if(nodeQname.equals(ClassificationModel.SUBJECT) || nodeQname.equals(ClassificationModel.NAMED_ENTITY)) {
					Object nodeName = node.getProperty(SystemModel.OPENAPPS_NAME.toString());
					if(nodeName != null) appendFreeText(nodeName.toString());
				} else if(nodeQname.equals(SystemModel.NOTE)) {
					String type = node.hasProperty(SystemModel.NOTE_TYPE.toString()) ? node.getPropertyValue(SystemModel.NOTE_TYPE) : null;
					if(type != null) {
						if(type.equals("General note") || type.equals("Abstract") || type.equals("General Physical Description note")) {
							String property = node.hasProperty(SystemModel.NOTE_CONTENT.toString()) ? node.getPropertyValue(SystemModel.NOTE_CONTENT) : null;
							if(property != null) {
								String content = property.toString();
								if(content != null && content.length() > 0) {
									//entityService.index(node.getId(), qname.toString(), type, content);
									appendFreeText(content);
								} 
							}
						}
					}
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		for(Property property : entity.getProperties()) {
			if(property.getValue().toString().length() > 0) {
				Serializable propertyValue = property.getValue();
				if(propertyValue != null) {
					if(propertyValue instanceof String) {
						String text = (String)propertyValue;
						entityService.index(entity.getId(), qname.toString(), property.getQname().getLocalName(), text);
						if(property.getQname().equals(RepositoryModel.DESCRIPTION)) 
							appendFreeText(text);
					} 
				}
			}
			/*
			if(dict != null) {
				PropertyDefinition def = dict.getProperty(qname);
				if(def != null && def.isIndexed()) {
					if(document.get(XMLUtility.ISO9075encode(qname.getLocalName())) == null) {
						Field.Store store = !def.isStoredInIndex() ? Field.Store.NO : Field.Store.YES;
						//String attributeName = QualifiedName.createQualifiedName(qname.getNamespace(), XMLUtility.ISO9075encode(qname.getLocalName())).toString();
						Field field = new Field(XMLUtility.ISO9075encode(qname.getLocalName()), prop.toString(), store, def.getIndexTokenisationMode(), Field.TermVector.NO);
						field.setBoost(luceneOptions.getBoost());
						document.add(field);
					} else {
						System.out.println("Duplicate property on node:"+node.getId());
					}
				}
			}
			*/
		}
		/*
		String dateExpression = node.hasProperty(RepositoryModel.DATE_EXPRESSION.toString()) ? (String)node.getProperty(RepositoryModel.DATE_EXPRESSION.toString()) : null;
		if(dateExpression != null) {
			String date_value = parseTextDate(dateExpression);
			if(date_value != null && !date_value.equals("0")) {
				index.remove(node, RepositoryModel.DATE_EXPRESSION.getLocalName()+"_");
				index.add(node, RepositoryModel.DATE_EXPRESSION.getLocalName()+"_", date_value);
			}
		}
		*/
		String textVal = freeText.toString();
		if(textVal != null && textVal.length() > 0) {
			textVal = textVal.replace(",", "");
		}
		entityService.index(entity.getId(), qname.toString(), "freetext", textVal.trim());
		freeText = new StringBuffer();
		super.index(qname, entity);
	}
	@Override
	public void deindex(QName arg0, Entity arg1) {
		// TODO Auto-generated method stub
		
	}
	
	protected String parseTextDate(String in) {
		Date date = null;
		SimpleDateFormat format = new SimpleDateFormat("yyyy MMMM dd");
		try {
			date = format.parse(in);			
		} catch(Exception e) {
			SimpleDateFormat format2 = new SimpleDateFormat("yyyy MMMM");
			try {
				date = format2.parse(in);
			} catch(Exception e2) {
				SimpleDateFormat format3 = new SimpleDateFormat("yyyy");
				try {
					date = format3.parse(in);
				} catch(Exception e3) {
					SimpleDateFormat format4 = new SimpleDateFormat("d MMMM yyyy");
					try {
						date = format4.parse(in);
					} catch(Exception e4) {
						
					}
				}
			}
		}
		if(date != null) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			StringBuffer buff = new StringBuffer();
			if(calendar.get(Calendar.YEAR) > 0) {
				buff.append(calendar.get(Calendar.YEAR));
				if(calendar.get(Calendar.MONTH) > 0) {
					if(calendar.get(Calendar.MONTH) < 9)
						buff.append("0"+(calendar.get(Calendar.MONTH)+1));
					else
						buff.append(calendar.get(Calendar.MONTH)+1);
				} else
					buff.append("00");
				if(calendar.get(Calendar.DAY_OF_MONTH) > 0) 
					if(calendar.get(Calendar.DAY_OF_MONTH) < 10)
						buff.append("0"+calendar.get(Calendar.DAY_OF_MONTH));
					else
						buff.append(calendar.get(Calendar.DAY_OF_MONTH));
				else
					buff.append("00");
				if(buff.length() == 8) return buff.toString();
				//else System.out.println(in+" parsed to "+buff.toString());
			}
		}
		return null;
	}
	protected QName getQName(Node node) {
		QName qname = null;
		try {
			if(node != null) {
				String qnameStr = node.hasProperty("qname") ? (String)node.getProperty("qname") :"";
				qname = QName.createQualifiedName(qnameStr);
			}
		} catch(InvalidQualifiedNameException e) {
			e.printStackTrace();
		}
		return qname;
	}
	protected void appendFreeText(String text) {
		String cleanText = text.replace(",", "").replace("\"", "").replace(";", "").replace(".", "").replace(":", "");
		String[] parts = cleanText.split(" ");
		for(String part : parts) {
			if(freeText.indexOf(part) == -1) freeText.append(part.toLowerCase()+" ");
		}
	}

}
