package org.archivemanager.repository.client.data;

import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;

public class ClassificationListDS extends RestDataSource {
	private static ClassificationListDS instance = null;  
	  
    public static ClassificationListDS getInstance() {  
        if (instance == null) {  
            instance = new ClassificationListDS("classificationListDS");  
        }  
        return instance;  
    }
    
	public ClassificationListDS(String id) {
		setID(id);
		
		DataSourceTextField idField = new DataSourceTextField("id", "ID",50);  
        idField.setPrimaryKey(true);  
        idField.setRequired(true);
        
        setFields(idField);
        
		setAddDataURL("/service/entity/create.xml");
		setFetchDataURL("/service/entity/search.xml");
		setRemoveDataURL("/service/entity/remove.xml");
		setUpdateDataURL("/service/entity/update.xml");
	}
	
	@Override
	protected void transformResponse(DSResponse response, DSRequest request,Object data) {
		// TODO Auto-generated method stub
		super.transformResponse(response, request, data);
	}
}