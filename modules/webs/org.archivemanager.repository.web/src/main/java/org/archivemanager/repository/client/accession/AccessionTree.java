package org.archivemanager.repository.client.accession;

import org.archivemanager.repository.client.data.AccessionTreeDS;
import org.heed.openapps.gwt.client.Application;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.tree.TreeGrid;
import com.smartgwt.client.widgets.tree.TreeNode;
import com.smartgwt.client.widgets.tree.events.FolderDropEvent;
import com.smartgwt.client.widgets.tree.events.FolderDropHandler;

public class AccessionTree extends TreeGrid {
	private Application mgr;
	
	
	public AccessionTree(final Application mgr) {
		this.mgr = mgr;
		setHeight100();
		setWidth100();
		setShowHeader(false);
		setBorder("0px");
		setWrapCells(true);
		setFixedRecordHeights(false);
		setCanReorderRecords(true);  
        setCanAcceptDroppedRecords(true);
		AccessionTreeDS ds = AccessionTreeDS.getInstance();
        setDataSource(ds);
        
        addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				mgr.select(event.getRecord());	
			}			
		});
        addFolderDropHandler(new FolderDropHandler() {
        	public void onFolderDrop(FolderDropEvent event) {        		
        		TreeNode child = event.getNodes()[0];
        		TreeNode parent = event.getFolder();
        		Record record = new Record();
        		record.setAttribute("id", child.getAttribute("id"));
        		record.setAttribute("parent", parent.getAttribute("id"));
        		updateData(record);
			}
        });
	}
	
}
