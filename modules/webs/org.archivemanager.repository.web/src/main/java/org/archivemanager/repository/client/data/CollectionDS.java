package org.archivemanager.repository.client.data;

import com.smartgwt.client.data.RestDataSource;


public class CollectionDS extends RestDataSource {
	private static CollectionDS instance = null;  
	  
    public static CollectionDS getInstance() {  
        if (instance == null) {  
            instance = new CollectionDS("collectionDS");  
        }  
        return instance;  
    }
    
	public CollectionDS(String id) {
		setID(id);
		/*
		DataSourceBooleanField paidField = new DataSourceBooleanField("paid");  
		DataSourceBooleanField appraisalItem = new DataSourceBooleanField("appraisal");
		DataSourceBooleanField newCollectionField = new DataSourceBooleanField("new_collection");
		DataSourceBooleanField acknowledgedField = new DataSourceBooleanField("acknowledged");
		DataSourceBooleanField existingCollectionField = new DataSourceBooleanField("existing_collection");
		DataSourceDateField dateField = new DataSourceDateField("date");
		
		setFields(paidField,appraisalItem,newCollectionField,acknowledgedField,existingCollectionField,dateField);
		*/
		
		setAddDataURL("/service/archivemanager/repository/collection/add.xml");
		setFetchDataURL("/service/entity/get.xml");
		setRemoveDataURL("/service/entity/remove.xml");
		setUpdateDataURL("/service/entity/update.xml");
	}
		
}