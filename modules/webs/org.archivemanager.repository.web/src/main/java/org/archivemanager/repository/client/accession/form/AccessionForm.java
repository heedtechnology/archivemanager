package org.archivemanager.repository.client.accession.form;

import java.util.Date;
import java.util.LinkedHashMap;

import org.archivemanager.repository.client.accession.AccessionContentComponent;
import org.archivemanager.repository.client.data.AccessionDS;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.CanvasItem;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.SpinnerItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;

public class AccessionForm extends DynamicForm {
	private AccessionContentComponent contentComponent;
	
	@SuppressWarnings("deprecation")
	public AccessionForm() {
		setWidth100();
		//setHeight100();
		setNumCols(4);
		setColWidths("175","*","175","*");
		setBorder("1px solid #C0C3C7");
		setDataSource(AccessionDS.getInstance());
				
		DateItem dateItem = new DateItem("date", "Date");
		dateItem.setStartDate(new Date(1900, 1, 1));
		
		TextItem accessionNumberItem = new TextItem("identifier", "Number");
		accessionNumberItem.setWidth(75);
		
		TextItem costItem = new TextItem("cost", "Cost");
		costItem.setWidth(85);
		
		SelectItem aquisitionItem = new SelectItem("aquisition_type", "Aquisition Type");
		aquisitionItem.setWidth(75);
		LinkedHashMap<String,String> valueMap = new LinkedHashMap<String,String>();
		valueMap.put("Deposit","Deposit");
		valueMap.put("Gift","Gift");
		valueMap.put("Purchase","Purchase");
		valueMap.put("Transfer","Transfer");
		aquisitionItem.setValueMap(valueMap);
		
		SpinnerItem quantityItem = new SpinnerItem("extent_number","Quantity");
		quantityItem.setWidth(60);
		quantityItem.setMin(0);
		quantityItem.setMax(1000);
		
		SelectItem quantityTypeItem = new SelectItem("extent_type", "Quantity Type");
		quantityTypeItem.setWidth(75);
		LinkedHashMap<String,String> valueMap2 = new LinkedHashMap<String,String>();
		valueMap2.put("box","Box");
		valueMap2.put("envelope","Envelope");
		valueMap2.put("package","Package");
		valueMap2.put("digital","Digital");
		quantityTypeItem.setValueMap(valueMap2);
		
		SpinnerItem pageBoxuantityItem = new SpinnerItem("pagebox_quantity","Estimated PaigeBox(s) On Shelf");
		pageBoxuantityItem.setWidth(60);
		pageBoxuantityItem.setMin(0);
		pageBoxuantityItem.setMax(1000);
		
		SelectItem priorityItem = new SelectItem("priority", "Priority");
		priorityItem.setWidth(75);
		LinkedHashMap<String,String> valueMap3 = new LinkedHashMap<String,String>();
		valueMap3.put("","");
		valueMap3.put("low","Low");
		valueMap3.put("normal","Normal");
		valueMap3.put("high","High");
		priorityItem.setValueMap(valueMap3);
		
		SpinnerItem packagesItem = new SpinnerItem("estimated_packages","Estimated Package(s) On Shelf");
		packagesItem.setWidth(60);
		packagesItem.setMin(0);
		packagesItem.setMax(1000);
		
		CheckboxItem paidItem = new CheckboxItem("paid", "Previously Paid");
		paidItem.setLabelAsTitle(true);
		
		SpinnerItem linearFeetItem = new SpinnerItem("linear_feet","Linear Feet");
		linearFeetItem.setWidth(60);
		linearFeetItem.setMin(0);
		linearFeetItem.setMax(1000);
		
		CheckboxItem appraisalItem = new CheckboxItem("appraisal", "Donor Wants Appraisal");
		appraisalItem.setLabelAsTitle(true);
		
		CheckboxItem newCollectionItem = new CheckboxItem("new_collection", "New Collection");
		newCollectionItem.setLabelAsTitle(true);
		
		CheckboxItem acknowledgedItem = new CheckboxItem("acknowledged", "Acknowledgement Letter Sent");
		acknowledgedItem.setLabelAsTitle(true);
		
		CheckboxItem existingItem = new CheckboxItem("existing_collection", "Existing Collection");
		existingItem.setLabelAsTitle(true);
				
		TextItem booksItem = new TextItem("books", "Books");
		booksItem.setWidth(400);
		booksItem.setColSpan(2);
				
		TextAreaItem generalNoteItem = new TextAreaItem("general_note", "Contents");
		generalNoteItem.setHeight(100);
		generalNoteItem.setWidth(400);
		generalNoteItem.setColSpan(2);
		
		CanvasItem contentsItem = new CanvasItem();
		contentsItem.setShowTitle(false);
		contentComponent = new AccessionContentComponent("200");
		contentsItem.setCanvas(contentComponent);
		
		setFields(dateItem,accessionNumberItem,costItem,aquisitionItem,quantityItem,quantityTypeItem,
				pageBoxuantityItem,priorityItem,packagesItem,paidItem,linearFeetItem,appraisalItem,
				newCollectionItem,acknowledgedItem,existingItem,booksItem,generalNoteItem,contentsItem);
	}
	
	public void select(Record record) {
		contentComponent.select(record);
	}
}
