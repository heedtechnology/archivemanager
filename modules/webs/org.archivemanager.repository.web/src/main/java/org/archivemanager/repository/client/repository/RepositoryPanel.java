package org.archivemanager.repository.client.repository;

import org.archivemanager.repository.client.RepositoryManagerEntryPoint;
import org.archivemanager.repository.client.data.RepositoryDS;
import org.archivemanager.repository.client.repository.component.CollectionComponent;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.EventTypes;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.data.RestUtility;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;

public class RepositoryPanel extends TabSet {
	private DynamicForm form;
	
	//private CollectionImportWindow importPanel;
	//private CollectionExportPanel exportPanel;
	private CollectionComponent collectionComponent;
	
	private static final int fieldWidth = 300;
	
	private Record selection;
	
	public RepositoryPanel(final RepositoryManagerEntryPoint mgr) {
		setWidth100();
		setHeight100();
		
		VLayout mainLayout = new VLayout();
		Tab collectionTab = new Tab("Repository");
		collectionTab.setPane(mainLayout);
		addTab(collectionTab);
		
		HLayout layout1 = new HLayout();
		layout1.setWidth100();
		layout1.setHeight(270);
		layout1.setBorder("1px solid #C0C3C7");
		mainLayout.setMembersMargin(10);
		mainLayout.addMember(layout1);
		
		VLayout topLeftLayout = new VLayout();
		topLeftLayout.setWidth("50%");
		layout1.addMember(topLeftLayout);
		
		form = new DynamicForm();
		form.setWidth("100%");
		form.setHeight(330);
		form.setMargin(5);
		form.setCellPadding(5);
		form.setNumCols(2);
		form.setColWidths("100","*");
		form.setBorder("1px solid #C0C3C7");
		form.setDataSource(RepositoryDS.getInstance());
		
		TextItem nameItem = new TextItem("name", "Name");
		nameItem.setWidth(fieldWidth);		
		TextItem shortNameItem = new TextItem("short_name", "Short Name");
		shortNameItem.setWidth(fieldWidth);
		TextItem urlItem = new TextItem("url", "URL");
		urlItem.setWidth(fieldWidth);
		TextItem countryItem = new TextItem("country_code", "Country Code");
		countryItem.setWidth(fieldWidth);
		TextItem agencyItem = new TextItem("agency_code", "Agency Code");
		agencyItem.setWidth(fieldWidth);
		TextItem ncesItem = new TextItem("nces", "NCES");
		ncesItem.setWidth(fieldWidth);
		TextItem brandingItem = new TextItem("branding", "Branding");
		brandingItem.setWidth(fieldWidth);
		TextItem languageItem = new TextItem("lang", "Language");
		languageItem.setWidth(fieldWidth);
		CheckboxItem publicItem = new CheckboxItem("public","Public");
				
		form.setFields(nameItem,shortNameItem,urlItem,countryItem,agencyItem,ncesItem,brandingItem,languageItem,publicItem);
		topLeftLayout.addMember(form);
		
		HLayout repoToolbar = new HLayout();
		
		topLeftLayout.addMember(repoToolbar);
		
		ImgButton importButton = new ImgButton();
		importButton.setSrc("/theme/images/icons32/import.png");
		importButton.setPrompt("Import Data");
		importButton.setWidth(32);
		importButton.setHeight(32);
		importButton.setShowDown(false);
		importButton.setShowRollOver(false);
		importButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				mgr.importEntity();
			}
		});
		VLayout iconLayout = new VLayout();
		iconLayout.setHeight(50);
		iconLayout.setWidth(50);
		iconLayout.setMargin(15);
		iconLayout.addMember(importButton);
		Label label = new Label(" Import  Data");
		label.setHeight(15);
		iconLayout.addMember(label);        
		repoToolbar.addMember(iconLayout);
		
		ImgButton orphanButton = new ImgButton();
		orphanButton.setSrc("/theme/images/icons32/link_add.png");
		orphanButton.setPrompt("Import Orphans");
		orphanButton.setWidth(32);
		orphanButton.setHeight(32);
		orphanButton.setShowDown(false);
		orphanButton.setShowRollOver(false);
		orphanButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				if(selection == null) {
					SC.say("Please select a repository to collect orphaned collections");
				} else {
					Record record = new Record();
					record.setAttribute("id", selection.getAttribute("id"));
					RestUtility.post("/repository/collection/orphans.xml", record, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							String msg = response.getAttribute("message") != null ? response.getAttribute("message") : "";
							OpenAppsEvent event = new OpenAppsEvent(EventTypes.MESSAGE);
							event.setMessage(msg);
							EventBus.fireEvent(event);
						}
					});
				}
			}
		});
		VLayout orphanLayout = new VLayout();
		orphanLayout.setHeight(50);
		orphanLayout.setWidth(50);
		orphanLayout.setMargin(15);
		orphanLayout.addMember(orphanButton);
		Label orphanLabel = new Label("Import Orphans");
		orphanLabel.setHeight(15);
		orphanLayout.addMember(orphanLabel);        
		repoToolbar.addMember(orphanLayout);
				
		//top right column
		collectionComponent = new CollectionComponent("100%");
		collectionComponent.setMargin(5);
		layout1.addMember(collectionComponent);
		
		//bottom
		HLayout layout2 = new HLayout();
		layout2.setWidth100();
		layout2.setBorder("1px solid #C0C3C7");
		mainLayout.addMember(layout2);
		
		//bottom left column
		VLayout layout3 = new VLayout();
		layout3.setWidth("50%");
		layout2.addMember(layout3);
		
		//bottom layout
		HLayout subLayout1 = new HLayout();
		subLayout1.setWidth100();
		layout2.addMember(subLayout1);
				
		//right column
		VLayout layout4 = new VLayout();
		layout4.setWidth("50%");
		layout2.addMember(layout4);
			
		
	}
	
	public void fetchData(Criteria criteria, DSCallback callback) {
		form.fetchData(criteria, callback);
	}
	public void select(Record repository) {
		if(repository != null) {
			try {
				form.editRecord(repository);
				collectionComponent.select(repository);
				selection = repository;
			} catch(ClassCastException e) {
					
			}
		}
	}
	public void save(DSCallback callback) {
		form.saveData(callback);
	}
}
