package org.archivemanager.repository.client.repository.component;

import org.archivemanager.repository.client.data.RepositoryListDS;
import org.heed.openapps.gwt.client.data.RestUtility;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class CollectionComponent extends VLayout {
	private Record repository;
	private ListGrid grid;
	private MoveCollectionWindow moveWindow;
	private AddCollectionWindow addWindow;
	private CollectionExportWindow exportWindow;
	
	
	public CollectionComponent(String width) {
		HLayout toolstrip = new HLayout();
		toolstrip.setWidth(width);
		toolstrip.setHeight(20);
		toolstrip.setBackgroundImage("[SKIN]/cssButton/button_stretch.png");
		toolstrip.setBorder("1px solid #A7ABB4");
		addMember(toolstrip);
		
		Label label = new Label("<label style='font-size:11px;font-weight:bold;color:#4C4C4C;font-family:Arial,Verdana,sans-serif;'>Collections</label>");
		label.setHeight("100%");
		label.setWidth("100%");
		toolstrip.addMember(label);
				
		HLayout buttons = new HLayout();
		buttons.setHeight(16);
		buttons.setMargin(2);
		buttons.setMembersMargin(3);
		toolstrip.addMember(buttons);
		
		ImgButton addButton = new ImgButton();
		addButton.setWidth(15);
		addButton.setHeight(15);
		addButton.setSrc("/theme/images/icons16/add.png");
		addButton.setPrompt("Add");
		addButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				addWindow.show();
			}
		});
		buttons.addMember(addButton);
		
		ImgButton delButton = new ImgButton();
		delButton.setWidth(16);
		delButton.setHeight(16);
		delButton.setSrc("/theme/images/icons16/delete.png");
		delButton.setPrompt("Delete");
		delButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				SC.confirm("Do you really wish to remove this permanently?", new BooleanCallback() {
					public void execute(Boolean value) {
						if(value) {
							Record rec = new Record();
							String id = grid.getSelectedRecord().getAttribute("target_id");
							if(id == null) id = grid.getSelectedRecord().getAttribute("id");
							rec.setAttribute("id", id);
							RestUtility.post("/entity/service/remove.xml", rec, new DSCallback() {
								public void execute(DSResponse response, Object rawData, DSRequest request) {
									grid.removeSelectedData();	
								}						
							});	
						}
					}			
				});				
			}
		});
		buttons.addMember(delButton);
		
		ImgButton exportButton = new ImgButton();
		exportButton = new ImgButton();
		exportButton.setSrc("/theme/images/icons16/export.png");
		exportButton.setPrompt("Export");
		exportButton.setWidth(16);
		exportButton.setHeight(16);
		exportButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				exportWindow.show();
			}
		});
		buttons.addMember(exportButton);
		
		ImgButton moveButton = new ImgButton();
		moveButton.setWidth(15);
		moveButton.setHeight(15);
		moveButton.setSrc("/theme/images/icons16/move.png");
		moveButton.setPrompt("Move");
		moveButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				moveWindow.show();
			}
		});
		buttons.addMember(moveButton);
		
		grid = new ListGrid();
		grid.setWidth(width);
		grid.setHeight(40);
		grid.setAutoFitData(Autofit.VERTICAL);
		grid.setAutoFitMaxHeight(300);
		grid.setShowHeader(false);
		grid.setSortField("name");
		grid.addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				exportWindow.select(event.getRecord());
			}
		});
		ListGridField nameField = new ListGridField("name");
		grid.setFields(nameField);
		addMember(grid);
		
		moveWindow = new MoveCollectionWindow();
		addWindow = new AddCollectionWindow();
		exportWindow = new CollectionExportWindow();
	}
	
	public void select(Record record) {
		this.repository = record;
		try {
			Record source_associations = record.getAttributeAsRecord("source_associations");
			if(source_associations != null) {
				Record notes = source_associations.getAttributeAsRecord("collections");
				if(notes != null) {
					setData(notes.getAttributeAsRecordArray("node"));
				} else setData(new Record[0]);
			} else setData(new Record[0]);
		} catch(ClassCastException e) {
			setData(new Record[0]);
		}
	}
	public void setData(Record[] records) {
		grid.setData(records);
	}
	
	public class MoveCollectionWindow extends Window {
		private TextItem searchField;
		private ListGrid moveGrid;
		
		public MoveCollectionWindow() {
			setWidth(400);
			setHeight(400);
			setTitle("Move Collection");
			setAutoCenter(true);
			setIsModal(true);
			
			DynamicForm searchForm = new DynamicForm();
			searchForm.setWidth("100%");
			searchForm.setHeight(25);
			searchForm.setMargin(2);
			searchForm.setNumCols(4);
			searchForm.setCellPadding(2);
			searchField = new TextItem("query");
			searchField.setShowTitle(false);
			searchField.setWidth(330);
			ButtonItem searchButton = new ButtonItem("search", "search");
			searchButton.setWidth(50);
			searchButton.setStartRow(false);
			searchButton.setEndRow(false);
			searchButton.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					Criteria criteria = new Criteria();
					criteria.setAttribute("qname", "{openapps.org_repository_1.0}repository");
					criteria.setAttribute("query", searchField.getValueAsString());
					criteria.setAttribute("field", "name");
					criteria.setAttribute("sort", "name_e");
					moveGrid.fetchData(criteria);
				}
			});
			searchForm.setFields(searchField,searchButton);
			addItem(searchForm);	
			
			moveGrid = new ListGrid();
			moveGrid.setWidth("100%");
			moveGrid.setHeight("100%");
			moveGrid.setMargin(2);
			//moveGrid.setBorder("0px");
			moveGrid.setShowHeader(false);
			ListGridField nameField = new ListGridField("name","Name");
			moveGrid.setDataSource(RepositoryListDS.getInstance());
			moveGrid.setFields(nameField);
	        addItem(moveGrid);
	        
	        HLayout buttons = new HLayout();
			buttons.setHeight(30);
			buttons.setMargin(2);
			addItem(buttons);
	        IButton submitItem = new IButton("Move");
	        //submitItem.setIcon("/theme/images/icons16/add.png");
			submitItem.addClickHandler(new ClickHandler() {
				public void onClick(ClickEvent event) {
					Record target = grid.getSelectedRecord();
					Record source = moveGrid.getSelectedRecord();
					if(target != null && source != null) {
						Record rec = new Record();
						rec.setAttribute("id", target.getAttribute("id"));
						rec.setAttribute("source", source.getAttribute("id"));
						rec.setAttribute("target", target.getAttribute("target_id"));
						RestUtility.post("/service/association/switch.xml", rec, new DSCallback() {
							public void execute(DSResponse response, Object rawData, DSRequest request) {
								grid.removeSelectedData();
								moveWindow.hide();	
							}						
						});					 
					}
				}
			});
			buttons.addMember(submitItem);
		}
	}
	public class AddCollectionWindow extends Window {
		public AddCollectionWindow() {
			setWidth(400);
			setHeight(100);
			setTitle("Add Collection");
			setAutoCenter(true);
			setIsModal(true);
			final DynamicForm addForm = new DynamicForm();
			addForm.setCellPadding(7);
			final TextItem nameItem = new TextItem("name", "Name");
			nameItem.setWidth(250);
						
			ButtonItem submitItem = new ButtonItem("submit", "Add");
			submitItem.setIcon("/theme/images/icons16/add.png");
			submitItem.setStartRow(false);
			submitItem.setEndRow(false);
			submitItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					Record record = new Record();
					record.setAttribute("assoc_qname", "{openapps.org_repository_1.0}collections");
					record.setAttribute("entity_qname", "{openapps.org_repository_1.0}collection");
					record.setAttribute("source", repository.getAttribute("id"));
					record.setAttribute("name", addForm.getValueAsString("name"));
					RestUtility.post("/entity/service/associate.xml", record, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							if(response.getData().length > 0) {
								Record record = response.getData()[0];
								grid.addData(record);
							}
							addWindow.hide();	
						}						
					});	
				}
			});
			
			addForm.setFields(nameItem,submitItem);
			addItem(addForm);
		}
	}
	
	
	
	public static native void openWindow(String url) /*-{   
		window.open(url, "win", "width=600,height=600");
	}-*/;
}