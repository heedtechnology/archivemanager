package org.archivemanager.repository.client.accession;
import java.util.LinkedHashMap;

import org.heed.openapps.gwt.client.data.RestUtility;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.events.ItemChangedEvent;
import com.smartgwt.client.widgets.form.events.ItemChangedHandler;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class AccessionDonorComponent extends VLayout {
	private DonorListGrid grid;
	private ListGrid donorGrid;
	private AddAccessionDonorWindow addWindow;
	private AddContactWindow addContactWindow;
	private AddAddressWindow addAddressWindow;
	
	private AccessionDonorDS accessionDonorDS = new AccessionDonorDS();
	
	LinkedHashMap<String,String> relationshipValueMap = new LinkedHashMap<String,String>();
	
	private Record selection;
	
	public AccessionDonorComponent(String width) {
		relationshipValueMap.put("","");
		relationshipValueMap.put("donor","Donor");
		relationshipValueMap.put("co_donor","Co-Donor");
		
		HLayout toolstrip = new HLayout();
		toolstrip.setWidth(width);
		toolstrip.setHeight(20);
		toolstrip.setBackgroundImage("[SKIN]/cssButton/button_stretch.png");
		toolstrip.setBorder("1px solid #A7ABB4");
		addMember(toolstrip);
		
		Label label = new Label("<label style='font-size:11px;font-weight:bold;color:#4C4C4C;font-family:Arial,Verdana,sans-serif;'>Donors</label>");
		label.setHeight("100%");
		label.setWidth("100%");
		toolstrip.addMember(label);
		
		HLayout buttons = new HLayout();
		buttons.setHeight(16);
		buttons.setMargin(2);
		buttons.setMembersMargin(3);
		toolstrip.addMember(buttons);
		
		ImgButton addButton = new ImgButton();
		addButton.setWidth(15);
		addButton.setHeight(15);
		addButton.setSrc("/theme/images/icons16/add.png");
		addButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				addWindow.show();
			}
		});
		buttons.addMember(addButton);
		ImgButton delButton = new ImgButton();
		delButton.setWidth(15);
		delButton.setHeight(15);
		delButton.setSrc("/theme/images/icons16/delete.png");
		delButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Record record = grid.getSelectedRecord();
				if(record != null) {
					Record rec = new Record();
					rec.setAttribute("id", record.getAttribute("id"));
					RestUtility.post("/repository/accession/contact/remove.xml", rec, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							grid.removeSelectedData();	
						}						
					});					 
				}
			}
		});
		buttons.addMember(delButton);
		
		grid = new DonorListGrid();
		grid.setWidth(width);
		grid.setHeight(40);
		grid.setAutoFitData(Autofit.VERTICAL);
		grid.setAutoFitMaxHeight(200);
		grid.setShowHeader(false);
		grid.setCanExpandRecords(true);
		ListGridField relationship = new ListGridField("relationship", 50);
		relationship.setValueMap(relationshipValueMap);
		ListGridField name = new ListGridField("name");
		name.setWidth("100%");
		name.setAlign(Alignment.LEFT);		
		ListGridField icon = new ListGridField("iconField", 25); 
        grid.setFields(relationship,name,icon);
		addMember(grid);
		
		addWindow = new AddAccessionDonorWindow();
		addContactWindow = new AddContactWindow();
		addAddressWindow = new AddAddressWindow();
	}
	public void setData(Record[] donors, Record[] addresses) {
		grid.setData(donors);
		grid.setAddressData(addresses);
	}
	public void select(Record record) {
		this.selection = record;
		try {
			Record source_associations = record.getAttributeAsRecord("source_associations");
			if(source_associations != null) {
				Record donors = source_associations.getAttributeAsRecord("contacts");
				if(donors != null) {
					Record addresses = source_associations.getAttributeAsRecord("addresses");
					if(addresses != null) {
						setData(donors.getAttributeAsRecordArray("node"), addresses.getAttributeAsRecordArray("node"));
					} else {
						setData(donors.getAttributeAsRecordArray("node"), new Record[0]);
					}
				} else setData(new Record[0], new Record[0]);
			}
		} catch(ClassCastException e) {
			setData(new Record[0], new Record[0]);
		}
		try {
			Record target_associations = record.getAttributeAsRecord("target_associations");
			if(target_associations != null) {
				
			}
		} catch(ClassCastException e) {
			
		}
	}
	
	public class DonorListGrid extends ListGrid {
		private Record[] addressData;
				
		public void setAddressData(Record[] records) {
			//innerGrid.setData(records);
			addressData = records;
		}
		public void removeAddress(String id) {
			Record[] newData = new Record[addressData.length-1];
			for(Record address : addressData) {
				String addressId = address.getAttribute("id");
				if(!addressId.equals(id))
					newData[newData.length] = address;
			}
			addressData = newData;
		}
		@Override  
        protected Canvas getExpansionComponent(final ListGridRecord record) {
			final VLayout layout = new VLayout();
			layout.setStyleName("donorAddressPanel");
			for(Record address : addressData) {
				String id = address.getAttribute("parent");
				String contactId = record.getAttribute("source_id");
				if(id != null && contactId.equals(id)) {
					final String addressId = address.getAttribute("id");
					final HLayout layout3 = new HLayout();
					layout3.setBorder("1px solid #A7ABB4;");
					layout.addMember(layout3);
					VLayout layout2 = new VLayout();
					layout3.addMember(layout2);
					Label address1 = new Label("<label style='font-size:11px;font-weight:bold;'>"+address.getAttribute("address1")+"</label>");
					address1.setHeight(18);
					layout2.addMember(address1);
					if(address.getAttribute("address2") != null) {
						Label address2 = new Label("<label style='font-size:11px;padding-left:10px;'>"+address.getAttribute("address2")+"</label>");
						address2.setHeight(18);
						layout2.addMember(address2);
					}
					Label city = new Label("<label style='font-size:11px;padding-left:10px;'>"+address.getAttribute("city")+", "+address.getAttribute("state")+" "+address.getAttribute("zip")+"</label>");
					city.setHeight(18);
					layout2.addMember(city);
					ImgButton chartImg = new ImgButton();
					chartImg.setMargin(2);
                    chartImg.setSrc("[SKIN]/headerIcons/close.png");  
                    chartImg.setPrompt("Remove Address");  
                    chartImg.setHeight(16);  
                    chartImg.setWidth(16);  
                    chartImg.addClickHandler(new ClickHandler() {  
                        public void onClick(ClickEvent event) {  
                        	Record rec = new Record();
        					rec.setAttribute("id", addressId);
                        	RestUtility.post("/service/association/remove.xml", rec, new DSCallback() {
        						public void execute(DSResponse response, Object rawData, DSRequest request) {
        							removeAddress(addressId);
        							layout.removeMember(layout3);
        						}						
        					});	   
                        }  
                    });
                    layout3.addMember(chartImg);
				}
			}
			
			HLayout innerToolbar = new HLayout();
			innerToolbar.setStyleName("donorAddAddress");
			innerToolbar.setWidth100();				
			innerToolbar.setAlign(Alignment.RIGHT);
			Label label = new Label("add address");
			label.setWidth(65);
			innerToolbar.addMember(label);
			ImgButton addImg = new ImgButton();  
            addImg.setSrc("[SKIN]/headerIcons/plus.png");  
            addImg.setHeight(12);  
            addImg.setWidth(12);
            addImg.setPrompt("Add Address");  
            addImg.addClickHandler(new ClickHandler() {  
                public void onClick(ClickEvent event) {
                	addAddressWindow.setDonor(record);
                    addAddressWindow.show();
                }  
            });
            innerToolbar.addMember(addImg);
			layout.addMember(innerToolbar);
			return layout;
		}
	}
	public class AddAccessionDonorWindow extends Window {
		private TextItem searchField;
				
		public AddAccessionDonorWindow() {
			setWidth(450);
			setHeight(350);
			setTitle("Add Donor");
			setAutoCenter(true);
			setIsModal(true);
			DynamicForm searchForm = new DynamicForm();
			searchForm.setWidth("100%");
			searchForm.setHeight(25);
			searchForm.setMargin(2);
			searchForm.setNumCols(2);
			searchForm.setCellPadding(2);
			searchField = new TextItem("query");
			searchField.setShowTitle(false);
			searchField.setWidth(380);
			ButtonItem searchButton = new ButtonItem("search", "search");
			searchButton.setWidth(50);
			searchButton.setStartRow(false);
			searchButton.setEndRow(false);
			searchButton.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					Criteria criteria = new Criteria();
					criteria.setAttribute("qname", "{openapps.org_contact_1.0}contact");
					criteria.setAttribute("field", "name");
					criteria.setAttribute("sort", "name_e");
					if(searchField.getValue() != null) {
						criteria.setAttribute("query", searchField.getValueAsString());
					} else {
						criteria.setAttribute("_startRow", "0");
						criteria.setAttribute("_endRow", "75");
					}
					donorGrid.fetchData(criteria);
				}
			});
			searchForm.setFields(searchField,searchButton);
			addItem(searchForm);
			
			donorGrid = new ListGrid();
			donorGrid.setWidth("100%");
			donorGrid.setHeight("100%");
			donorGrid.setMargin(2);
			donorGrid.setShowHeader(false);
			donorGrid.setDataSource(accessionDonorDS);
			ListGridField nameField = new ListGridField("name","Name");
			donorGrid.setFields(nameField);
			addItem(donorGrid);
			
			DynamicForm editForm = new DynamicForm();
			editForm.setHeight(25);
			editForm.setNumCols(4);
			editForm.setMargin(2);
			final SelectItem relationshipItem = new SelectItem("relationship","Relationship");
			relationshipItem.setWidth(195);
			relationshipItem.setValueMap(relationshipValueMap);
			
			ButtonItem linkItem = new ButtonItem("link", "Link");
			linkItem.setIcon("/theme/images/icons16/link.png");
			linkItem.setStartRow(false);
			linkItem.setEndRow(false);
			linkItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					if(donorGrid.anySelected()) {
						Record record = new Record();
						record.setAttribute("qname", "{openapps.org_contact_1.0}contacts");
						record.setAttribute("source", selection.getAttribute("id"));
						record.setAttribute("target", donorGrid.getSelectedRecord().getAttribute("id"));
						record.setAttribute("relationship", relationshipItem.getValue());
						RestUtility.post("/service/association/add.xml",record,new DSCallback() {
							public void execute(DSResponse response, Object rawData, DSRequest request) {
								if(response.getData() != null && response.getData().length > 0) {
									Record accession = response.getData()[0];
									if(accession != null) {
										Record source_associations = accession.getAttributeAsRecord("source_associations");
										if(source_associations != null) {
											Record donors = source_associations.getAttributeAsRecord("contacts");
											if(donors != null) {
												grid.setData(donors.getAttributeAsRecordArray("node"));
											}									
										}
									}
								}
								addWindow.hide();
							}						
						});
					}
				}			
			});
			
			ButtonItem newItem = new ButtonItem("new", "New Contact");
			newItem.setIcon("/theme/images/icons16/contact_grey_add.png");
			newItem.setStartRow(false);
			newItem.setEndRow(false);
			newItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					addContactWindow.show();
				}			
			});
			
			editForm.setFields(relationshipItem,linkItem,newItem);
			addItem(editForm);
		}
	}
	
	public class AddContactWindow extends Window {
		
		public AddContactWindow() {
			setWidth(400);
			setHeight(150);
			setTitle("Add Contact");
			setAutoCenter(true);
			setIsModal(true);
			
			final DynamicForm form = new DynamicForm();
			form.setMargin(10);
			form.setColWidths(65,"*");
			final TextItem name = new TextItem("name", "Name");
			name.setWidth("*");
			name.setVisible(false);
			final TextItem first_name = new TextItem("first_name", "First Name");
			first_name.setWidth("*");
			name.setVisible(false);
			final TextItem last_name = new TextItem("last_name", "Last Name");
			last_name.setWidth("*");
			name.setVisible(false);
			form.setDataSource(accessionDonorDS);
			SelectItem qname = new SelectItem("qname", "Type");
			qname.setDefaultValue("{openapps.org_contact_1.0}individual");
			LinkedHashMap<String,String> qnameValueMap = new LinkedHashMap<String,String>();
			qnameValueMap.put("{openapps.org_contact_1.0}individual", "Individual");
			qnameValueMap.put("{openapps.org_contact_1.0}organization", "Organization");
			qname.setValueMap(qnameValueMap);
			ButtonItem linkItem = new ButtonItem("add", "Add");
			linkItem.setIcon("/theme/images/icons16/add.png");
			linkItem.setStartRow(false);
			linkItem.setEndRow(false);
			linkItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					Record record = form.getValuesAsRecord();
					donorGrid.addData(record, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							addContactWindow.hide();
						}
					});
				}
			});
			form.addItemChangedHandler(new ItemChangedHandler() {
				public void onItemChanged(ItemChangedEvent event) {
					if(event.getNewValue().equals("{openapps.org_contact_1.0}individual")) {
						name.hide();
						first_name.show();
						last_name.show();
						addContactWindow.setHeight(150);
					} else if(event.getNewValue().equals("{openapps.org_contact_1.0}organization")) {
						name.show();
						first_name.hide();
						last_name.hide();
						addContactWindow.setHeight(125);
					}
				}
			});
			form.setFields(name,first_name,last_name,qname,linkItem);
			addItem(form);
		}
	}
	
	public class AddAddressWindow extends Window {
		private ListGrid addressGrid;
		
		private Record donor;
		
		public AddAddressWindow() {
			setWidth(500);
			setHeight(150);
			setTitle("Select Address");
			setAutoCenter(true);
			setIsModal(true);
			
			addressGrid = new ListGrid();
			addressGrid.setWidth100();
			addressGrid.setHeight100();
			addressGrid.setShowHeader(false);
			ListGridField address1 = new ListGridField("address1");
			ListGridField city = new ListGridField("city",125);
			ListGridField state = new ListGridField("state",35);
			state.setAlign(Alignment.CENTER);
			ListGridField zip = new ListGridField("zip",50);
			zip.setAlign(Alignment.CENTER);
			addressGrid.setFields(address1,city,state,zip);
			addItem(addressGrid);
			HLayout innerToolbar = new HLayout();
			innerToolbar.setStyleName("donorAddAddress");
			innerToolbar.setWidth100();
			innerToolbar.setMargin(5);
			innerToolbar.setAlign(Alignment.RIGHT);
			Label label = new Label("add address");
			label.setWidth(65);
			innerToolbar.addMember(label);
			ImgButton addImg = new ImgButton();  
            addImg.setSrc("[SKIN]/headerIcons/plus.png");  
            addImg.setHeight(12);  
            addImg.setWidth(12);  
            addImg.addClickHandler(new ClickHandler() {  
                public void onClick(ClickEvent event) {
                	if(addressGrid.anySelected()) {
						Record record = new Record();
						record.setAttribute("qname", "{openapps.org_contact_1.0}addresses");
						record.setAttribute("source", selection.getAttribute("id"));
						record.setAttribute("target", addressGrid.getSelectedRecord().getAttribute("id"));
						RestUtility.post("/service/association/add.xml",record,new DSCallback() {
							public void execute(DSResponse response, Object rawData, DSRequest request) {
								if(response.getData() != null && response.getData().length > 0) {
									Record accession = response.getData()[0];
									if(accession != null) {
										Record source_associations = accession.getAttributeAsRecord("source_associations");
										if(source_associations != null) {
											Record donors = source_associations.getAttributeAsRecord("contacts");
											if(donors != null) {
												Record addresses = source_associations.getAttributeAsRecord("addresses");
												if(addresses != null) {
													setData(donors.getAttributeAsRecordArray("node"), addresses.getAttributeAsRecordArray("node"));
												} else {
													setData(donors.getAttributeAsRecordArray("node"), new Record[0]);
												}
											}									
										}
									}
								}
								addAddressWindow.hide();
							}						
						});
					}
                }  
            });
            innerToolbar.addMember(addImg);
            addItem(innerToolbar);
		}
		@Override
		public void show() {			
			RestUtility.get("/entity/service/get/"+donor.getAttribute("target_id")+"/children.xml?targets=false&sources=false",new DSCallback() {
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					addressGrid.setData(response.getData());
				}
			});
			super.show();
		}
		public void setDonor(Record record) {
			this.donor = record;
		}
	}
	
	public class AccessionDonorDS extends RestDataSource {
		public AccessionDonorDS() {
			setID("accessionDonorDS");
			DataSourceTextField idField = new DataSourceTextField("id", "ID",50);  
	        idField.setPrimaryKey(true);  
	        idField.setRequired(true);
	        setFields(idField);
			setFetchDataURL("/entity/service/search.xml");
			setAddDataURL("/entity/service/create.xml");
		}
	}
}
