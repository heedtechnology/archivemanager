package org.archivemanager.repository.client.data;

import com.smartgwt.client.data.RestDataSource;


public class RepositoryDS extends RestDataSource {
	private static RepositoryDS instance = null;  
	  
    public static RepositoryDS getInstance() {  
        if (instance == null) {  
            instance = new RepositoryDS("repositoryDS");  
        }  
        return instance;  
    }
    
	public RepositoryDS(String id) {
		setID(id);				
		setAddDataURL("/service/archivemanager/repository/collection/add.xml");
		setFetchDataURL("/service/entity/get.xml");
		setRemoveDataURL("/service/entity/remove.xml");
		setUpdateDataURL("/service/entity/update.xml");
	}
	
}