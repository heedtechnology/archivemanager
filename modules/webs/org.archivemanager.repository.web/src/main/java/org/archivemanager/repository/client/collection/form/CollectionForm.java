package org.archivemanager.repository.client.collection.form;

import java.util.LinkedHashMap;

import org.archivemanager.repository.client.data.CollectionDS;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.RichTextItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.SpinnerItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.layout.HLayout;


public class CollectionForm extends HLayout {
	private final ValuesManager vm;
	private DynamicForm form1;
	private DynamicForm form2;
	
	public CollectionForm() {
		super();
		setWidth100();
		setBorder("1px solid #C0C3C7");
		vm = new ValuesManager();
		vm.setDataSource(CollectionDS.getInstance());
		
		form1 = new DynamicForm();
		form1.setWidth(555);
		form1.setNumCols(4);
		form1.setColWidths("100","*","75","*");
		form1.setValuesManager(vm);
				
		RichTextItem nameItem = new RichTextItem("name","Heading");
		nameItem.setHeight(75);
		nameItem.setWidth(425);
		nameItem.setColSpan(3);
		nameItem.setShowTitle(true);
		nameItem.setStartRow(false);
		nameItem.setEndRow(false);
		nameItem.setControlGroups(new String[]{"fontControls", "styleControls"});
		
		RichTextItem scopeItem = new RichTextItem("scope_note","Scope Note");
		scopeItem.setHeight(125);
		scopeItem.setWidth(425);
		scopeItem.setColSpan(3);
		scopeItem.setShowTitle(true);
		scopeItem.setStartRow(false);
		scopeItem.setEndRow(false);
		scopeItem.setControlGroups(new String[]{"fontControls", "styleControls"});
		
		RichTextItem bioItem = new RichTextItem("bio_note","Biographical Note");
		bioItem.setHeight(125);
		bioItem.setWidth(425);
		bioItem.setColSpan(3);
		bioItem.setShowTitle(true);
		bioItem.setStartRow(false);
		bioItem.setEndRow(false);
		bioItem.setControlGroups(new String[]{"fontControls", "styleControls"});
		
		TextAreaItem commentsItem = new TextAreaItem("comments","Coments");
		commentsItem.setHeight(50);
		commentsItem.setWidth(430);
		commentsItem.setColSpan(3);
		commentsItem.setShowTitle(true);
				
		TextItem accessionDateItem = new TextItem("accession_date", "Accession Date");
		accessionDateItem.setColSpan(3);
		accessionDateItem.setWidth(430);
		
		CheckboxItem restrictionItem = new CheckboxItem("restrictions", "Restrictions");
		//restrictionItem.setHeight(50);
		restrictionItem.setLabelAsTitle(true);
		
		CheckboxItem internalItem = new CheckboxItem("internal", "Internal");
		internalItem.setLabelAsTitle(true);	
				
		CheckboxItem publicItem = new CheckboxItem("public", "Public");
		publicItem.setLabelAsTitle(true);
		
		LinkedHashMap<String,String> valueMap2 = new LinkedHashMap<String,String>();
		valueMap2.put("box", "Box");
		valueMap2.put("cubic_foot", "Cubic Foot");
		valueMap2.put("envelope", "Envelope");
		valueMap2.put("file", "File");
		valueMap2.put("folder", "Folder");
		valueMap2.put("item", "Item");
		valueMap2.put("linear_foot", "Linear Foot");
		valueMap2.put("object", "Object");
		valueMap2.put("package", "Package");
		//extentItem = new SpinnerSelectItem("extent_number","extent_units", "Extents", 150, valueMap2);
		SpinnerItem extentValueItem = new SpinnerItem("extent_number", "Extent Value");
		extentValueItem.setWidth(50);
		
		SelectItem extentUnitsItem = new SelectItem("extent_units", "Extent Units");
		extentUnitsItem.setWidth(100);
		extentUnitsItem.setValueMap(valueMap2);
				
		form1.setFields(nameItem,scopeItem,bioItem,commentsItem,accessionDateItem,extentValueItem,extentUnitsItem,restrictionItem,internalItem);
		addMember(form1);
		
		form2 = new DynamicForm();
		form2.setWidth100();
		form2.setCellPadding(5);
		form2.setMargin(7);
		form2.setNumCols(2);
		form2.setColWidths("125","*");
		//form2.setDataSource(CollectionDS.getInstance());
		form2.setValuesManager(vm);
		
		StaticTextItem idItem = new StaticTextItem("id", "ID");
		
		TextItem codeItem = new TextItem("code", "Collection Code");
		codeItem.setWidth(150);
		
		TextItem cidItem = new TextItem("identifier", "Collection ID");
		cidItem.setWidth(150);
		
		TextItem dateItem = new TextItem("date_expression", "Date Expression");
		dateItem.setWidth(150);
		
		TextItem beginItem = new TextItem("begin", "Begin Date");
		
		TextItem endItem = new TextItem("end", "End Date");
				
		TextItem bulkBeginItem = new TextItem("bulk_begin", "Bulk Begin Date");
				
		TextItem bulkEndItem = new TextItem("bulk_end", "Bulk End Date");
		
		SelectItem languageItem = new SelectItem("language", "Language");
		languageItem.setWidth(150);
		LinkedHashMap<String,String> valueMap1 = new LinkedHashMap<String,String>();
		valueMap1.put("ar","Arabic");
		valueMap1.put("zh","Chinese");
		valueMap1.put("cs","Czech");
		valueMap1.put("da","Danish");
		valueMap1.put("nl","Dutch");
		valueMap1.put("en","English");
		valueMap1.put("fi","Finnish");
		valueMap1.put("fr","French");
		valueMap1.put("de","German");
		valueMap1.put("el","Greek");
		valueMap1.put("he","Hebrew");
		valueMap1.put("hu","Hungarian");
		valueMap1.put("is","Icelandic");
		valueMap1.put("it","Italian");
		valueMap1.put("ja","Japanese");
		valueMap1.put("ko","Korean");
		valueMap1.put("no","Norwegian");
		valueMap1.put("pl","Polish");
		valueMap1.put("pt","Portugese");
		valueMap1.put("ru","Russian");
		valueMap1.put("es","Spanish");
		valueMap1.put("sv","Swedish");
		valueMap1.put("th","Thai");
		valueMap1.put("tr","Turkish");
		languageItem.setValueMap(valueMap1);
		
		form2.setFields(idItem,codeItem,cidItem,dateItem,beginItem,endItem,bulkBeginItem,bulkEndItem,languageItem,publicItem);
		addMember(form2);
	}
	
	
	public void editRecord(Record record) {
		if(record.getAttribute("restrictions") != null) 
			record.setAttribute("restrictions", Boolean.valueOf(record.getAttribute("restrictions")));
		if(record.getAttribute("internal") != null) 
			record.setAttribute("internal", Boolean.valueOf(record.getAttribute("internal")));
		if(record.getAttribute("public") != null) 
			record.setAttribute("public", Boolean.valueOf(record.getAttribute("public")));
		vm.editRecord(record);
	}
	public Record getValuesAsRecord() {
		Record record = new Record();
		record.setAttribute("id", vm.getValue("id"));
		for(Object key : vm.getValues().keySet()) {
			Object val = vm.getValues().get(key);
			if(val != null && !val.equals("")) record.setAttribute(String.valueOf(key), val);
			else record.setAttribute(String.valueOf(key), "");
		}
		return record;
	}
	public void fetchData(Criteria criteria, DSCallback callback) {
		vm.fetchData(criteria, callback);
	}
	public void saveData(DSCallback callback) {
		vm.saveData(callback);
	}
}
