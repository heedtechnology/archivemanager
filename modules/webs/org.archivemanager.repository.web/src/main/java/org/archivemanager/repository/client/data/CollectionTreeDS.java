package org.archivemanager.repository.client.data;

import java.util.Date;

import com.google.gwt.i18n.client.DateTimeFormat;

import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;

public class CollectionTreeDS extends RestDataSource {
	private static CollectionTreeDS instance = null;  
	private static DateTimeFormat dateParser = DateTimeFormat.getFormat("yyyy-MM-dd");
	private static DateTimeFormat dateFormatter = DateTimeFormat.getFormat("EEEE MMMM d, yyyy");
	
	
    public static CollectionTreeDS getInstance() {  
        if (instance == null) {  
            instance = new CollectionTreeDS("collectionTreeDS");  
        }  
        return instance;  
    }
    
	public CollectionTreeDS(String id) {
		setID(id);  
        setTitleField("Name");	        
        DataSourceTextField nameField = new DataSourceTextField("title", "Title");     
        
        DataSourceTextField idField = new DataSourceTextField("id", "ID",50);  
        idField.setPrimaryKey(true);  
        idField.setRequired(true);
        
        DataSourceTextField parentField = new DataSourceTextField("parent", "Parent");  
        parentField.setRequired(true);  
        parentField.setForeignKey(id + ".id");  
        parentField.setRootValue("null");  
        
        setFields(idField,nameField,parentField);
        
        setFetchDataURL("/service/archivemanager/repository/collection/accession.xml?sources=true");
        setAddDataURL("/service/entity/associate.xml");
        setUpdateDataURL("/service/archivemanager/repository/collection/update.xml");
        setRemoveDataURL("/service/entity/remove.xml");
	}
	
	@Override
	protected void transformResponse(DSResponse response, DSRequest request,Object data) {
		try {
			if(response.getData() != null && response.getData().length > 0) {
				for(int i=0; i < response.getData().length; i++) {
					Record entity = response.getData()[i];
					if(entity != null) {
						String parent = entity.getAttribute("parent");
						String type = entity.getAttribute("localName");
						if(parent != null && type.equals("accession")) {
							String date = entity.getAttribute("date");
							entity.setAttribute("parent", parent+"-accessions");
							entity.setAttribute("icon", "/theme/images/icons16/drawer.png");
							entity.setAttribute("isFolder", "false");
							entity.setAttribute("title", getFormattedDate(date));
						} else if(parent != null && type.equals("category")) {
							String parentLocalName = entity.getAttribute("parentLocalName");
							if(parentLocalName != null && parentLocalName.equals("collection"))
								entity.setAttribute("parent", parent+"-categories");						
						} else if(type != null && !type.equals("accessions") && !type.equals("categories") && !type.equals("collection")) {
							//entity.setAttribute("isFolder", false);
							entity.setAttribute("icon", "/theme/images/tree_icons/"+type+".png");
						}
					}
				}
			}
		} catch(ClassCastException e) {
				
		}
		super.transformResponse(response, request, data);
	}
	
	public static String getShortFormattedDate(Date in) {
		return dateParser.format(in);
	}
	public static String getFormattedDate(Date in) {
		return dateFormatter.format(in);
	}
	public static Date getDate(String in) {
		try {
			return dateParser.parse(in);
		} catch(IllegalArgumentException e) {
			e.printStackTrace();
		}
		return null;
	}
	public static String getFormattedDate(String date) {
		try {
			return dateFormatter.format(dateParser.parse(date));
		} catch(IllegalArgumentException e) {
			e.printStackTrace();
		}
		return "";
	}
}
