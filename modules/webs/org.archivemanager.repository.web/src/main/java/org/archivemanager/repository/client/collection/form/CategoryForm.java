package org.archivemanager.repository.client.collection.form;

import java.util.LinkedHashMap;

import org.archivemanager.repository.client.data.CategoryDS;
import org.archivemanager.repository.client.data.NativeCollectionModel;
import org.heed.openapps.gwt.client.form.ContainerItem;
import org.heed.openapps.gwt.client.form.SpinnerSelectItem;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.RichTextItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.SpinnerItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;


public class CategoryForm extends DynamicForm {
	private NativeCollectionModel collectionTypes = new NativeCollectionModel();
	private SpinnerSelectItem extentItem;
	
	public CategoryForm() {
		super();
		setWidth100();
		//setHeight(300);
		//setMargin(10);
		setNumCols(6);
		setColWidths("125","*","75","*","125","*");
		setBorder("1px solid #C0C3C7");
		setDataSource(CategoryDS.getInstance());
		
		RichTextItem nameItem = new RichTextItem("name","Heading");
		nameItem.setHeight(75);
		nameItem.setWidth(425);
		nameItem.setColSpan(3);
		nameItem.setShowTitle(true);
		nameItem.setStartRow(false);
		nameItem.setEndRow(false);
		nameItem.setControlGroups(new String[]{"fontControls", "styleControls"});
		
		TextAreaItem scopeItem = new TextAreaItem("description","Description");
		scopeItem.setHeight(50);
		scopeItem.setWidth(430);
		scopeItem.setColSpan(3);
		scopeItem.setShowTitle(true);
		
		TextAreaItem commentsItem = new TextAreaItem("summary","Summary");
		commentsItem.setHeight(50);
		commentsItem.setWidth(430);
		commentsItem.setColSpan(3);
		commentsItem.setShowTitle(true);
						
		TextItem dateItem = new TextItem("date_expression", "Date Expression");
		dateItem.setWidth(150);
				
		CheckboxItem restrictionItem = new CheckboxItem("restrictions", "Restrictions");
		restrictionItem.setHeight(35);
		restrictionItem.setLabelAsTitle(true);
		
		CheckboxItem internalItem = new CheckboxItem("internal", "Internal");
		internalItem.setHeight(35);
		internalItem.setLabelAsTitle(true);
		
		SelectItem languageItem = new SelectItem("language", "Language");
		languageItem.setWidth(150);
		LinkedHashMap<String,String> valueMap1 = new LinkedHashMap<String,String>();
		valueMap1.put("ar","Arabic");
		valueMap1.put("zh","Chinese");
		valueMap1.put("cs","Czech");
		valueMap1.put("da","Danish");
		valueMap1.put("nl","Dutch");
		valueMap1.put("en","English");
		valueMap1.put("fi","Finnish");
		valueMap1.put("fr","French");
		valueMap1.put("de","German");
		valueMap1.put("el","Greek");
		valueMap1.put("he","Hebrew");
		valueMap1.put("hu","Hungarian");
		valueMap1.put("is","Icelandic");
		valueMap1.put("it","Italian");
		valueMap1.put("ja","Japanese");
		valueMap1.put("ko","Korean");
		valueMap1.put("no","Norwegian");
		valueMap1.put("pl","Polish");
		valueMap1.put("pt","Portugese");
		valueMap1.put("ru","Russian");
		valueMap1.put("es","Spanish");
		valueMap1.put("sv","Swedish");
		valueMap1.put("th","Thai");
		valueMap1.put("tr","Turkish");
		languageItem.setValueMap(valueMap1);
		
		TextItem beginItem = new TextItem("begin", "Begin Date");
				
		TextItem endItem = new TextItem("end", "End Date");
				
		TextItem accessionDateItem = new TextItem("accession_date", "Accession Date");
		accessionDateItem.setWidth(430);
		accessionDateItem.setColSpan(3);
						
		LinkedHashMap<String,String> valueMap2 = new LinkedHashMap<String,String>();
		valueMap2.put("box", "Box");
		valueMap2.put("cubic_foot", "Cubic Foot");
		valueMap2.put("envelope", "Envelope");
		valueMap2.put("file", "File");
		valueMap2.put("folder", "Folder");
		valueMap2.put("item", "Item");
		valueMap2.put("linear_foot", "Linear Foot");
		valueMap2.put("object", "Object");
		valueMap2.put("package", "Package");
		//extentItem = new SpinnerSelectItem("extent_number","extent_units", "Extents", 150, valueMap2);
		SpinnerItem extentValueItem = new SpinnerItem("extent_number", "Extent Value");
		extentValueItem.setWidth(75);
		
		SelectItem extentUnitsItem = new SelectItem("extent_units", "Extent Units");
		extentUnitsItem.setWidth(100);
		extentUnitsItem.setValueMap(valueMap2);
				
		ContainerItem containerItem = new ContainerItem("container", "Container", collectionTypes.getContainerTypes());
		containerItem.setColSpan(3);
		
		SelectItem levelItem = new SelectItem("level", "Level");
		LinkedHashMap<String,String> valueMap3 = new LinkedHashMap<String,String>();
		valueMap3.put("series","Series");
		valueMap3.put("subseries","Subseries");
		valueMap3.put("group","Group");
		valueMap3.put("subgroup","Subgroup");
		valueMap3.put("file","File");
		valueMap3.put("item","Item");
		levelItem.setValueMap(valueMap3);
		
		SelectItem contentTypeItem = new SelectItem("qname", "Content Type");
		LinkedHashMap<String,String> valueMap4 = new LinkedHashMap<String,String>();
		valueMap4.put("{openapps.org_repository_1.0}category","Category");
		valueMap4.put("{openapps.org_repository_1.0}artwork","Artwork");
		valueMap4.put("{openapps.org_repository_1.0}audio","Audio");
		valueMap4.put("{openapps.org_repository_1.0}correspondence","Correspondence");
		valueMap4.put("{openapps.org_repository_1.0}financial","Financial");
		valueMap4.put("{openapps.org_repository_1.0}journals","Journals");
		valueMap4.put("{openapps.org_repository_1.0}legal","Legal");
		valueMap4.put("{openapps.org_repository_1.0}manuscript","Manuscript");
		valueMap4.put("{openapps.org_repository_1.0}medical","Medical");
		valueMap4.put("{openapps.org_repository_1.0}memorabilia","Memorabilia");
		valueMap4.put("{openapps.org_repository_1.0}miscellaneous","Miscellaneous");
		valueMap4.put("{openapps.org_repository_1.0}notebooks","Notebooks");
		valueMap4.put("{openapps.org_repository_1.0}photographs","Photographs");
		valueMap4.put("{openapps.org_repository_1.0}printed_material","Printed Material");
		valueMap4.put("{openapps.org_repository_1.0}professional","Professional");
		valueMap4.put("{openapps.org_repository_1.0}research","Research");
		valueMap4.put("{openapps.org_repository_1.0}scrapbooks","Scrapbooks");
		valueMap4.put("{openapps.org_repository_1.0}video","Video");
		contentTypeItem.setValueMap(valueMap4);
		
		SpinnerItem sizeItem = new SpinnerItem("size", "Size %");
		sizeItem.setMax(0);
		sizeItem.setMax(100);
		sizeItem.setWidth(50);
		
		setFields(nameItem,dateItem,
				scopeItem,beginItem,
				commentsItem,endItem,
				accessionDateItem,
				languageItem,extentValueItem, extentUnitsItem,
				internalItem,
				levelItem,contentTypeItem,restrictionItem,sizeItem,
				containerItem);
	}
	@Override
	public void editRecord(Record record) {
		if(record.getAttribute("restrictions") != null) 
			record.setAttribute("restrictions", Boolean.valueOf(record.getAttribute("restrictions")));
		if(record.getAttribute("internal") != null) 
			record.setAttribute("internal", Boolean.valueOf(record.getAttribute("internal")));
		super.editRecord(record);
	}
	@Override
	public Record getValuesAsRecord() {
		Record record = new Record();
		record.setAttribute("id", getValue("id"));
		for(FormItem item : getFields()) {
			if(item.getName().length() > 0 && item.getValue() == null) 
				record.setAttribute(item.getName(), "");
			else
				record.setAttribute(item.getName(), item.getValue());
		}		
		//record.setAttribute("extent_number", extentItem.getSpinnerValue());
		//record.setAttribute("extent_units", extentItem.getSelectValue());
		return record;
	}
}
