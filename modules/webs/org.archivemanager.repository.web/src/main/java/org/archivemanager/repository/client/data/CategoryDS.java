package org.archivemanager.repository.client.data;

import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;

public class CategoryDS extends RestDataSource {
	private static CategoryDS instance = null;  
	  
    public static CategoryDS getInstance() {  
        if (instance == null) {  
            instance = new CategoryDS("categoryDS");  
        }  
        return instance;  
    }
    
	public CategoryDS(String id) {
		setID(id);
		/*
		DataSourceBooleanField paidField = new DataSourceBooleanField("paid");  
		DataSourceBooleanField appraisalItem = new DataSourceBooleanField("appraisal");
		DataSourceBooleanField newCollectionField = new DataSourceBooleanField("new_collection");
		DataSourceBooleanField acknowledgedField = new DataSourceBooleanField("acknowledged");
		DataSourceBooleanField existingCollectionField = new DataSourceBooleanField("existing_collection");
		DataSourceDateField dateField = new DataSourceDateField("date");
		
		setFields(paidField,appraisalItem,newCollectionField,acknowledgedField,existingCollectionField,dateField);
		*/
		
		setAddDataURL("/service/archivemanager/repository/collection/add.xml");
		setFetchDataURL("/service/entity/get.xml");
		setRemoveDataURL("/service/entity/remove.xml");
		setUpdateDataURL("/service/entity/update.xml");
	}
	
	@Override
	protected void transformResponse(DSResponse response, DSRequest request,Object data) {
		// TODO Auto-generated method stub
		super.transformResponse(response, request, data);
	}
}