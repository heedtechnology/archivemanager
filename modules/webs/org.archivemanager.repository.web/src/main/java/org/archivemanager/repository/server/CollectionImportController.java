package org.archivemanager.repository.server;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;
import org.heed.openapps.ClassificationModel;
import org.heed.openapps.RepositoryModel;
import org.heed.openapps.SystemModel;
import org.heed.openapps.entity.Association;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.entity.InvalidEntityException;
import org.heed.openapps.entity.Property;
import org.heed.openapps.entity.data.FileImportProcessor;
import org.heed.openapps.entity.ImportProcessor;
import org.heed.openapps.util.XMLUtility;
import org.heed.openapps.cache.TimedCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class CollectionImportController {
	@Autowired protected EntityService entityService;
	private TimedCache<String,Entity> entityCache = new TimedCache<String,Entity>(60);
	private TimedCache<String,FileImportProcessor> parserCache = new TimedCache<String,FileImportProcessor>(60);
	
	@RequestMapping(value="/collection/import/add", method = RequestMethod.POST)
	public ModelAndView add(HttpServletRequest req, HttpServletResponse res, @RequestParam("session") String sessionKey) throws Exception {
		String repoId = req.getParameter("repository");
		if(repoId != null) {
			Entity repository = entityService.getEntity(Long.valueOf(repoId));
			Entity root = entityCache.get(sessionKey);		
			ImportProcessor parser = parserCache.get(sessionKey);		
			entityService.addEntities(parser.getEntities().values());
			entityService.addAssociation(new Association(RepositoryModel.COLLECTIONS, repository.getId(), root.getId()));
			root.addProperty(SystemModel.OPENAPPS_NAME, root.getPropertyValue(SystemModel.OPENAPPS_NAME));
			entityService.updateEntity(root, true);
		}
		return response("Success!!!!", "", res.getWriter());
	}
	@RequestMapping(value="/collection/import/fetch", method = RequestMethod.GET)
	public ModelAndView fetch(HttpServletRequest req, HttpServletResponse res, @RequestParam("session") String sessionKey) throws Exception {
		String source = req.getParameter("_dataSource");
		StringBuffer buff = new StringBuffer();		
		Entity root = entityCache.get(sessionKey);
		ImportProcessor parser = parserCache.get(sessionKey);
		if(source.equals("importTreeDS")) {
			if(root != null) {
				List<Association> associations = root.getSourceAssociations();
				if(associations.size() > 0) buff.append("<record id='"+root.getUid()+"' title='"+root.getProperty(SystemModel.OPENAPPS_NAME)+"' parent='null'/>");
				else buff.append("<record id='0' title='Collection' parent='null'><children/></record>");
				printNodeTaxonomy(buff, root, parser); 
			}
			//System.out.println(buff.toString());
			return response(buff.toString(), res.getWriter());
		} else if(source.equals("node")) {
			String id = req.getParameter("id");
			if(id != null) {				
				Entity node = parser.getEntityById(id);
				if(node != null) {
					buff.append("<node id='"+id+"' localName='"+node.getQname().getLocalName()+"'>");
					buff.append("<properties>");
					for(Property prop : node.getProperties()) {
						buff.append("<property name='"+prop.getQname().getLocalName()+"'><value><![CDATA["+prop.getValue()+"]]></value></property>");
					}
					buff.append("</properties>");
					/*
					buff.append("<containers>");
					for(Association prop : node.getTargetAssociations(SystemModel.CATEGORIES,SystemModel.ITEMS)) {
						buff.append("<node name='"+prop.getSourceNode().getProperty(RepositoryModel.CONTAINER_TYPE)+"'>");
						buff.append("<value><![CDATA["+toXmlCdata(prop.getSourceNode().getProperty(RepositoryModel.CONTAINER_VALUE))+"]]></value>");
						buff.append("</node>");
					}
					buff.append("</containers>");
					 */
					buff.append("<notes>");
					for(Association propAssoc : node.getSourceAssociations(SystemModel.NOTES)) {
						Entity prop = parser.getEntityById(String.valueOf(propAssoc.getTarget()));
						buff.append("<node name='"+prop.getProperty(SystemModel.NOTE_TYPE)+"'>");
						buff.append("<value><![CDATA["+prop.getProperty(SystemModel.NOTE_CONTENT)+"]]></value>");
						buff.append("</node>");
					}
					buff.append("</notes>");
					buff.append("<names>");
					for(Association propAssoc : node.getSourceAssociations(ClassificationModel.NAMED_ENTITIES)) {
						Entity prop = parser.getEntityById(String.valueOf(propAssoc.getTarget()));
						buff.append("<node name='"+prop.getQname().getLocalName()+"' value='"+prop.getProperty(SystemModel.OPENAPPS_NAME)+"' />");
					}
					buff.append("</names>");
					buff.append("<subjects>");
					for(Association propAssoc : node.getSourceAssociations(ClassificationModel.SUBJECTS)) {
						Entity prop = parser.getEntityById(String.valueOf(propAssoc.getTarget()));
						buff.append("<node name='"+prop.getQname().getLocalName()+"' value='"+prop.getProperty(SystemModel.OPENAPPS_NAME)+"' />");
					}
					buff.append("</subjects>");
					buff.append("</node>");
					return response(buff.toString(), res.getWriter());
				}
			}
		}
		return error("problem fetching resource", res.getWriter());
	}
	@RequestMapping(value="/collection/import/upload", method = RequestMethod.POST)
	public ModelAndView upload(HttpServletRequest req, HttpServletResponse res) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		String mode = null;
		String sessionKey = "";
		try {
			// Create a new file upload handler
			ServletFileUpload upload = new ServletFileUpload();
			// Parse the request
			FileItemIterator iter = upload.getItemIterator(req);
			while(iter.hasNext()) {
			    FileItemStream item = iter.next();
			    String name = item.getFieldName();
			    InputStream stream = item.openStream();
			    if(item.isFormField()) {
			    	if(name.equals("mode")) mode = Streams.asString(stream);
			    	//System.out.println("Form field " + name + " with value " + Streams.asString(tmpStream) + " detected.");
			    } else {
			    	FileImportProcessor parser = (FileImportProcessor)entityService.getImportProcessors(mode).get(0);
					if(parser != null) {
						parser.process(stream);
						sessionKey = java.util.UUID.randomUUID().toString();
						entityCache.put(sessionKey, parser.getRoot());
						parserCache.put(sessionKey, parser);
					}
			    }
			}
		} catch(FileUploadException e) {
			e.printStackTrace();
		}		
		res.setContentType("text/html");
		res.getWriter().print("<script language='javascript' type='text/javascript'>window.top.window.uploadComplete('"+sessionKey+"');</script>");
		return null;
	}
	
	protected void printNodeTaxonomy(StringBuffer buff, Entity node, ImportProcessor parser) throws InvalidEntityException {
		for(Association assoc : node.getChildren()) {
			Entity child = parser.getEntityById(assoc.getTargetUid());
			Property titleProp = child.getProperty(SystemModel.OPENAPPS_NAME);
			String title = titleProp != null ? titleProp.toString() : child.getQname().getLocalName();
			if(child.getChildren().size() > 0) {
				buff.append("<record id='"+child.getUid()+"' title='"+toXmlData(title)+"' parent='"+node.getUid()+"'/>");
				printNodeTaxonomy(buff,child, parser);
			} else buff.append("<record id='"+child.getUid()+"' title='"+toXmlData(title)+"' parent='"+node.getUid()+"' isFolder='false'/>");
		}
	}
	protected ModelAndView response(String data, PrintWriter out) {
		out.write("<response><status>0</status><data>"+data+"</data></response>");
		return null;
	}
	protected ModelAndView response(String message, String data, PrintWriter out) {
		out.write("<response><status>0</status><message>"+message+"</message><data>"+data+"</data></response>");
		return null;
	}
	protected ModelAndView error(String msg, PrintWriter out) {
		out.write("<response><status>-1</status><message>"+msg+"</message></response>");
		return null;
	}
	public static String toXmlData(Object o) {
		if(o == null) return "";
		else return XMLUtility.escape(o.toString());
	}
}
