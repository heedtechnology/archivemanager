package org.archivemanager.repository.client;

import org.archivemanager.repository.client.data.RepositoryListDS;
import org.archivemanager.repository.client.repository.RepositoryPanel;
import org.archivemanager.repository.client.repository.component.CollectionImportWindow;
import org.heed.openapps.gwt.client.Application;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;


public class RepositoryApplication extends HLayout implements Application {
	private RepositoryManagerEntryPoint mgr;
	private ListGrid grid;
	private RepositoryPanel repositoryPanel;
	
	private AddRepositoryWindow addWindow;
	private CollectionImportWindow importWindow;
	
	public RepositoryApplication(final RepositoryManagerEntryPoint mgr) {
		this.mgr = mgr;
		setWidth100();
		setHeight100();
		setMembersMargin(5);
		
		grid = new ListGrid();
		grid.setWidth(300);
		grid.setHeight("100%");
		grid.setBorder("0px");
		grid.setShowHeader(false);
		grid.setShowResizeBar(true);
		ListGridField nameField = new ListGridField("name","Name");
		grid.setDataSource(RepositoryListDS.getInstance());
		grid.setFields(nameField);
		grid.addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				mgr.select(event.getRecord());
			}
		});
        addMember(grid);
        
        repositoryPanel = new RepositoryPanel(mgr);
        repositoryPanel.hide();
        addMember(repositoryPanel);
        
        addWindow = new AddRepositoryWindow();
        importWindow = new CollectionImportWindow();
	}
	
	@Override
	public void select(Record record) {
		String type = record.getAttribute("localName");
		if(type.equals("repository")) {
			Criteria criteria = new Criteria();
			criteria.setAttribute("id", record.getAttribute("id"));
			criteria.setAttribute("sources", "true");
			repositoryPanel.fetchData(criteria, new DSCallback() {
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					if(response.getData() != null && response.getData().length > 0) {
						Record accession = response.getData()[0];
						if(accession != null) {
							String date = accession.getAttribute("title");
							String title = grid.getSelectedRecord().getAttribute("title");
							if(title != null && !title.equals(date)) {
								grid.getSelectedRecord().setAttribute("title", date);
								grid.refreshFields();
							}
							repositoryPanel.select(accession);
							importWindow.select(accession);
							repositoryPanel.show();	
						}
					}
				}
			});
					
			mgr.getToolbar().showButtons(true, true, true);
		}
	}
	@Override
	public void save() {
		repositoryPanel.save(new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				if(response.getData() != null && response.getData().length > 0) {
					Record accession = response.getData()[0];
					if(accession != null) {
						String date = accession.getAttribute("title");
						String title = grid.getSelectedRecord().getAttribute("title");
						if(title != null && !title.equals(date)) {
							grid.getSelectedRecord().setAttribute("title", date);
							grid.refreshFields();
						}
					}
				}
			}
		});		
	}
	public void importEntity() {
		importWindow.show();
	}
	public void exportEntity() {
		//addWindow.show();
	}
	@Override
	public void add() {
		addWindow.show();
	}
	@Override
	public void delete() {
		SC.confirm("Do you really wish to remove this permanently?", new BooleanCallback() {
			public void execute(Boolean value) {
				if(value) grid.removeSelectedData();
			}			
		});		
	}
	@Override
	public void search(String query) {
		Criteria criteria = new Criteria();
		criteria.setAttribute("qname", "{openapps.org_repository_1.0}repository");
		criteria.setAttribute("query", query);
		criteria.setAttribute("field", "freetext");
		criteria.setAttribute("sort", "name_e");
		criteria.setAttribute("sources", "false");
		grid.fetchData(criteria); 
		repositoryPanel.show();
	}

	public class AddRepositoryWindow extends Window {
		public AddRepositoryWindow() {
			setWidth(400);
			setHeight(100);
			setTitle("Add Repository");
			setAutoCenter(true);
			setIsModal(true);
			DynamicForm addForm = new DynamicForm();
			addForm.setHeight100();
			addForm.setColWidths("50","*");
			addForm.setCellPadding(7);
			final TextItem nameItem = new TextItem("name", "Name");
			nameItem.setWidth(320);
						
			ButtonItem submitItem = new ButtonItem("submit", "Add");
			submitItem.setIcon("/theme/images/icons16/add.png");
			submitItem.setStartRow(false);
			submitItem.setEndRow(false);
			submitItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					Record record = new Record();
					record.setAttribute("qname", "{openapps.org_repository_1.0}repository");
					record.setAttribute("name", nameItem.getValue());
					grid.addData(record, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							addWindow.hide();
						}
					});
				}
			});
			
			addForm.setFields(nameItem,submitItem);
			addItem(addForm);
		}
	}

}
