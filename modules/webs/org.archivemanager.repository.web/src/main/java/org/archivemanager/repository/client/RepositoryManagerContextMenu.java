package org.archivemanager.repository.client;

import com.smartgwt.client.widgets.HTMLFlow;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.events.ClickHandler;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;

public class RepositoryManagerContextMenu extends Menu {
	private MenuItem aboutItem;
	
	private AboutWindow aboutWindow;
	
	public RepositoryManagerContextMenu() {
		super();
		aboutItem = new MenuItem("About ArchiveManager");
		aboutItem.addClickHandler(new ClickHandler() {
        	public void onClick(MenuItemClickEvent event) {
				
			}
        });
		setItems(aboutItem);
		
		aboutWindow = new AboutWindow();
	}
	
	public class AboutWindow extends Window {
		public AboutWindow() {
			setWidth(325);
			setHeight(100);
			HTMLFlow flow = new HTMLFlow();
			flow.setContentsURL("/about.html");
		}
	}
}
