package org.archivemanager.repository.server;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;
import org.heed.openapps.RepositoryModel;
import org.heed.openapps.SystemModel;
import org.heed.openapps.entity.Association;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.entity.Property;
import org.heed.openapps.entity.data.FileImportProcessor;
import org.heed.openapps.entity.ImportProcessor;
import org.heed.openapps.util.XMLUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AccessionImportController {
	@Autowired protected EntityService entityService;
	
	
	
	@RequestMapping(value="/accession/import/add", method = RequestMethod.GET)
	public ModelAndView add(HttpServletRequest req, HttpServletResponse res) throws Exception {
		String mode = (String)req.getSession().getAttribute("mode");
		Map<String,Entity> accessions = (Map<String,Entity>)req.getSession().getAttribute("accessions");
		ImportProcessor parser = (mode != null) ? entityService.getImportProcessors(mode).get(0) : null;		
		for(Entity accession : accessions.values()) {
			Association collectionAssoc = accession.getTargetAssociation(RepositoryModel.ACCESSIONS);
			if(collectionAssoc != null && collectionAssoc.getSourceEntity() != null) {
				entityService.addEntity(accession);
				collectionAssoc.setTarget(accession.getId());
				entityService.addAssociation(collectionAssoc);
				List<Association> noteAssocs = accession.getSourceAssociations();
				for(Association noteAssoc : noteAssocs) {
					if(noteAssoc != null && noteAssoc.getTargetEntity() != null) {
						entityService.addEntity(noteAssoc.getTargetEntity());
						noteAssoc.setSource(accession.getId());
						noteAssoc.setTarget(noteAssoc.getTargetEntity().getId());
						entityService.addAssociation(noteAssoc);
					}
				}
			}
		}
		return response("Success!!!!", "", res.getWriter());
	}
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/accession/import/fetch", method = RequestMethod.GET)
	public ModelAndView fetch(HttpServletRequest req, HttpServletResponse res) throws Exception {
		String source = req.getParameter("_dataSource");
		StringBuffer buff = new StringBuffer();
		Map<String,Entity> accessions = (Map<String,Entity>)req.getSession().getAttribute("accessions");
		String mode = (String)req.getSession().getAttribute("mode");
		String op = req.getParameter("op");
		if(op != null && op.equals("clear")) {
			return response("", res.getWriter());
		}
		//Mapping mapping = model.getMappings().get(mode);
		ImportProcessor parser = (mode != null) ? entityService.getImportProcessors(mode).get(0) : null;
		if(source.equals("treeData")) {
			if(accessions != null) {
				for(Entity accession : accessions.values()) {
					String collection = "";
					String restriction = "";
					String donor = "";
					String address = "";
					String collectionTitle = "";
					String donorTitle = "";
					String addressTitle = "";
					Association collectionAssoc = accession.getTargetAssociation(RepositoryModel.ACCESSIONS);
					if(collectionAssoc != null && collectionAssoc.getSourceEntity() != null) {
						collectionTitle = collectionAssoc.getSourceEntity().getPropertyValue(SystemModel.OPENAPPS_TITLE);
						if(collectionAssoc.getSourceEntity().getId() != null)
							collection = collectionAssoc.getSourceEntity().getPropertyValue(SystemModel.OPENAPPS_NAME);
					}
					List<Association> noteAssocs = accession.getSourceAssociations(SystemModel.NOTES);
					for(Association noteAssoc : noteAssocs) {
						if(noteAssoc != null && noteAssoc.getTargetEntity() != null) {
							String type = noteAssoc.getTargetEntity().getPropertyValue(SystemModel.NOTE_TYPE);
							if(type.equals("Access Restriction"))
								restriction = noteAssoc.getTargetEntity().getPropertyValue(SystemModel.NOTE_CONTENT);
							else if(type.equals("Address"))
								addressTitle = noteAssoc.getTargetEntity().getPropertyValue(SystemModel.NOTE_CONTENT);
							else if(type.equals("Donor"))
								donorTitle = noteAssoc.getTargetEntity().getPropertyValue(SystemModel.NOTE_CONTENT);
						}
					}
					String date = accession.getPropertyValue(RepositoryModel.ACCESSION_DATE);
					String cost = accession.getPropertyValue(RepositoryModel.ACCESSION_COST);
					String general_note = accession.getPropertyValue(RepositoryModel.ACCESSION_GENERAL_NOTE);
					String extent_type = accession.getPropertyValue(RepositoryModel.EXTENT_TYPE);
					String extent_value = accession.getPropertyValue(RepositoryModel.EXTENT_VALUE);
					String pagebox_quantity = accession.getPropertyValue(RepositoryModel.PAGEBOX_QUANTITY);
					buff.append("<node uid='"+accession.getUid()+"'>");
					buff.append("<title>"+collectionTitle+" ("+date+")</title>");
					buff.append("<date>"+date+"</date>");
					buff.append("<cost>"+cost+"</cost>");
					buff.append("<restriction><![CDATA["+restriction+"]]></restriction>");
					buff.append("<general_note><![CDATA["+general_note+"]]></general_note>");
					buff.append("<collection>"+collection+"</collection>");
					buff.append("<extent_type>"+extent_type+"</extent_type>");
					buff.append("<extent_number>"+extent_value+"</extent_number>");
					buff.append("<pagebox_quantity>"+pagebox_quantity+"</pagebox_quantity>");
					buff.append("<address>"+address+"</address>");
					buff.append("<collectionTitle>"+collectionTitle+"</collectionTitle>");
					buff.append("<donorTitle>"+donorTitle+"</donorTitle>");
					buff.append("<addressTitle>"+addressTitle+"</addressTitle>");
					buff.append("</node>");
				}
			}
			//System.out.println(buff.toString());
			return response(buff.toString(), res.getWriter());
		} else if(source.equals("node")) {
			String id = req.getParameter("id");
			if(id != null) {				
				Entity node = parser.getEntityById(id);
				buff.append("<node id='"+id+"' localName='"+node.getQname().getLocalName()+"'>");
				buff.append("<properties>");
				for(Property prop : node.getProperties()) {
					buff.append("<property name='"+prop.getQname().getLocalName()+"'><value><![CDATA["+prop.getValue()+"]]></value></property>");
				}
				buff.append("</properties>");
				
				return response(buff.toString(), res.getWriter());
			}
		}
		return error("problem fetching resource", res.getWriter());
	}
	@RequestMapping(value="/accession/import/upload", method = RequestMethod.POST)
	public ModelAndView upload(HttpServletRequest req, HttpServletResponse res) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		String mode = null;
		String coll = null;
		String repo = null;
		try {
			// Create a new file upload handler
			ServletFileUpload upload = new ServletFileUpload();
			// Parse the request
			FileItemIterator iter = upload.getItemIterator(req);
			while(iter.hasNext()) {
			    FileItemStream item = iter.next();
			    String name = item.getFieldName();
			    InputStream stream = item.openStream();
			    if(item.isFormField()) {
			    	if(name.equals("mode")) mode = Streams.asString(stream);
			    	else if(name.equals("collection")) coll = Streams.asString(stream);
			    	else if(name.equals("repository")) repo = Streams.asString(stream);
			    	//System.out.println("Form field " + name + " with value " + Streams.asString(tmpStream) + " detected.");
			    } else {
			        //System.out.println("File field " + name + " with file name " + item.getName() + " detected.");
			    	//Entity repository = entityService.getEntity(Long.valueOf(repo));
			    	FileImportProcessor parser = (FileImportProcessor)entityService.getImportProcessors(mode).get(0);
					if(parser != null) {
						parser.process(stream);
						req.getSession().setAttribute("accessions", parser.getEntities());
						req.getSession().setAttribute("mode", mode);
						//req.getSession().setAttribute("collection", coll);
					}
			    }
			}
		} catch(FileUploadException e) {
			e.printStackTrace();
		}		
		res.setContentType("text/html");
		res.getWriter().print("<script language='javascript' type='text/javascript'>window.top.window.stopUpload(1);</script>");
		return null;
	}
	
	protected ModelAndView response(String data, PrintWriter out) {
		out.write("<response><status>0</status><data>"+data+"</data></response>");
		return null;
	}
	protected ModelAndView response(String message, String data, PrintWriter out) {
		out.write("<response><status>0</status><message>"+message+"</message><data>"+data+"</data></response>");
		return null;
	}
	protected ModelAndView error(String msg, PrintWriter out) {
		out.write("<response><status>-1</status><message>"+msg+"</message></response>");
		return null;
	}
	public static String toXmlData(Object o) {
		if(o == null) return "";
		else return XMLUtility.escape(o.toString());
	}
}
