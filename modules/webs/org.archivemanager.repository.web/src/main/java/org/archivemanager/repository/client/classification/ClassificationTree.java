package org.archivemanager.repository.client.classification;

import org.archivemanager.repository.client.data.AccessionTreeDS;
import org.heed.openapps.gwt.client.Application;

import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.tree.TreeGrid;

public class ClassificationTree extends TreeGrid {
	private Application mgr;
	
	
	public ClassificationTree(final Application mgr) {
		this.mgr = mgr;
		setHeight100();
		setWidth100();
		setShowHeader(false);
		setBorder("0px");
		
		AccessionTreeDS ds = AccessionTreeDS.getInstance();
        setDataSource(ds);
        
        addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				mgr.select(event.getRecord());	
			}			
		});
	}
	
}
