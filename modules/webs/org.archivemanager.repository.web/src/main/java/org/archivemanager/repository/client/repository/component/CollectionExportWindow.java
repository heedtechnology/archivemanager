package org.archivemanager.repository.client.repository.component;

import java.util.LinkedHashMap;

import org.archivemanager.repository.client.RepositoryEvents;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.HeaderControls;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.HTMLPane;
import com.smartgwt.client.widgets.HeaderControl;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.RadioGroupItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class CollectionExportWindow extends Window {
	private Button downloadButton;
	private ExportViewer exportViewer;
	
	private Record selection;
	
	
	public CollectionExportWindow() {
		setWidth(250);
		setHeight(155);
		setTitle("Export Collection");
		setAutoCenter(true);
		setIsModal(true);
		
		VLayout mainLayout = new VLayout();
		mainLayout.setWidth100();
		mainLayout.setHeight100();
		addItem(mainLayout);
		
		final DynamicForm addForm = new DynamicForm();
		addForm.setCellPadding(7);
		addForm.setColWidths("100", "*");

		final RadioGroupItem formatItem = new RadioGroupItem("format");  
		formatItem.setTitle("Export Format");
		LinkedHashMap<String,String> valueMap = new LinkedHashMap<String,String>();
		valueMap.put("html", "HTML");
		valueMap.put("pdf", "PDF");
		valueMap.put("rtf", "RTF (MSWord)");
		formatItem.setDefaultValue("html");
		formatItem.setValueMap(valueMap);
		
		addForm.setFields(formatItem);
		mainLayout.addMember(addForm);
		
		HLayout buttons = new HLayout();
		buttons.setHeight(25);
		buttons.setMargin(5);
		buttons.setMembersMargin(5);
		mainLayout.addMember(buttons);
		
		Button addButton = new Button("Submit");
		addButton.setWidth(100);
		addButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				if(selection == null) return;
				String id = selection.getAttribute("target_id");
				String format = formatItem.getValueAsString();
				Record criteria = new Record();
        		criteria.setAttribute("id", id);
        		criteria.setAttribute("format", format);
        		criteria.setAttribute("title", "finding_aid_"+id);
        		EventBus.fireEvent(new OpenAppsEvent(RepositoryEvents.EXPORT_COLLECTION, criteria)); 
			}
		});
		buttons.addMember(addButton);
		downloadButton = new Button("Download");
		downloadButton.setWidth(100);
		//downloadButton.setVisible(false);
		downloadButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				//exportViewer.setURL("repository/collection/export/"+selection.getAttribute("target_id")+"."+formatItem.getValueAsString());
				//exportViewer.show();
				com.google.gwt.user.client.Window.open("/service/archivemanager/reporting/stream/finding_aid_"+selection.getAttribute("target_id")+"."+formatItem.getValueAsString(), "_blank", "");
			}
		});
		buttons.addMember(downloadButton);
		
		exportViewer = new ExportViewer();
	}
	public void select(Record collection) {
		this.selection = collection;
	}
		
	public class ExportViewer extends Window {
		private HTMLPane pane;
		
		public ExportViewer() {
			setWidth(950);
			setHeight(600);
			setTitle("Export Viewer");
			setAutoCenter(true);
			setIsModal(true);
			
			HeaderControl print = new HeaderControl(HeaderControl.PRINT, new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					Window window = new Window();
					window.setWidth(850);
					window.setHeight(500);
					window.setAutoCenter(true);
					Canvas.showPrintPreview(new Object[]{pane}, null, "Print Preview", null, window, "Print");
				}				
			});		
			setHeaderControls(HeaderControls.HEADER_LABEL, print, HeaderControls.CLOSE_BUTTON);  
			
			pane = new HTMLPane();
			pane.setWidth100();
			pane.setHeight100();
			addItem(pane);
		}
		public void setURL(String url) {
			pane.setContentsURL(url);
		}
	}
}