package org.archivemanager.repository.client;
import java.util.Date;
import java.util.LinkedHashMap;

import org.archivemanager.repository.client.accession.AccessionPanel;
import org.archivemanager.repository.client.collection.CategoryPanel;
import org.archivemanager.repository.client.collection.CollectionPanel;
import org.archivemanager.repository.client.collection.CollectionTree;
import org.archivemanager.repository.client.collection.ItemPanel;
import org.archivemanager.repository.client.component.HTMLLoadingPanel;
import org.archivemanager.repository.client.data.CollectionTreeDS;
import org.heed.openapps.gwt.client.Application;
import org.heed.openapps.gwt.client.data.NativeContentTypes;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.tree.TreeNode;

public class CollectionApplication extends HLayout implements Application {
	private RepositoryManagerEntryPoint mgr;
	private CollectionTree tree;
	private CollectionPanel collectionPanel;
	private CategoryPanel categoryPanel;
	private ItemPanel itemPanel;
	private AccessionPanel accessionPanel;
	private NativeContentTypes model = new NativeContentTypes();
	private Canvas canvas;
	private HTMLLoadingPanel htmlPanel;
	private Record selection;
	
	private AddCollectionWindow addWindow;
	private AddAccessionWindow addAccessionWindow;
	
	public CollectionApplication(RepositoryManagerEntryPoint mgr) {
		this.mgr = mgr;
		setWidth100();
		setHeight100();
		setMembersMargin(5);
		
		tree = new CollectionTree(this);
		tree.setWidth(300);
        tree.setHeight("100%");
        tree.setShowResizeBar(true);
        addMember(tree);
        	
        canvas = new Canvas();
        canvas.setWidth100();
        canvas.setHeight100();
		addMember(canvas);
		
		collectionPanel = new CollectionPanel();
		collectionPanel.hide();
		canvas.addChild(collectionPanel);
		
		categoryPanel = new CategoryPanel();
		categoryPanel.hide();
		canvas.addChild(categoryPanel);
		
		accessionPanel = new AccessionPanel();
		accessionPanel.hide();
		canvas.addChild(accessionPanel);
		
		itemPanel = new ItemPanel();
		itemPanel.hide();
		canvas.addChild(itemPanel);
		
		htmlPanel = new HTMLLoadingPanel();
		htmlPanel.hide();
		canvas.addChild(htmlPanel);
		
		addWindow = new AddCollectionWindow();
		addAccessionWindow = new AddAccessionWindow();
		
	}
	
	@Override
	public void select(Record record) {
		this.selection = record;
		String type = record.getAttribute("localName");
		collectionPanel.hide();
		categoryPanel.hide();
		accessionPanel.hide();
		itemPanel.hide();
		htmlPanel.hide();
		tree.select(record);
		if(type.equals("categories") || type.equals("accessions")) {
			mgr.getToolbar().showButtons(true, false, false);
			htmlPanel.select(record);
			htmlPanel.show();
		} else if(type.equals("collection")) {
			Criteria criteria = new Criteria();
			criteria.setAttribute("id", record.getAttribute("id"));
			collectionPanel.fetchData(criteria, new DSCallback() {
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					if(response.getData() != null && response.getData().length > 0) {
						Record accession = response.getData()[0];
						if(accession != null) {
							String date = accession.getAttribute("name");
							String title = tree.getSelectedRecord().getAttribute("name");
							String localName = accession.getAttribute("localName");
							if(title != null && !title.equals(date)) tree.getSelectedRecord().setAttribute("name", date);
							if(localName != null) tree.getSelectedRecord().setAttribute("localName", localName);
							tree.refreshFields();							
							collectionPanel.select(accession);
						}
					}
				}
			});
			collectionPanel.select(record);
			collectionPanel.show();
			mgr.getToolbar().showButtons(true, true, true);
		} else if(type.equals("category")) {
			Criteria criteria = new Criteria();
			criteria.setAttribute("id", record.getAttribute("id"));
			categoryPanel.fetchData(criteria, new DSCallback() {
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					if(response.getData() != null && response.getData().length > 0) {
						Record accession = response.getData()[0];
						if(accession != null) {
							String date = accession.getAttribute("name");
							String title = tree.getSelectedRecord().getAttribute("title");
							String localName = accession.getAttribute("localName");
							if(title != null && !title.equals(date)) tree.getSelectedRecord().setAttribute("title", date);
							if(localName != null) tree.getSelectedRecord().setAttribute("localName", localName);
							tree.refreshFields();
							categoryPanel.select(accession);
						}
					}
				}
			});
			categoryPanel.select(record);
			categoryPanel.show();
			Boolean isFolder = record.getAttributeAsBoolean("isFolder");
			mgr.getToolbar().showButtons(true, false, true);
		} else if(type.equals("accession")) {
			Criteria criteria = new Criteria();
			criteria.setAttribute("id", record.getAttribute("id"));
			accessionPanel.fetchData(criteria, new DSCallback() {
				@SuppressWarnings("deprecation")
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					if(response.getData() != null && response.getData().length > 0) {
						Record accession = response.getData()[0];
						if(accession != null) {
							String accessionsId = tree.getSelectedRecord().getAttribute("parent");
							if(accessionsId != null) {
								TreeNode accessionsFolder = tree.getData().findById(accessionsId);
								if(accessionsFolder != null) {
									String collectionId = accessionsFolder.getAttribute("parent");
									if(collectionId != null) accession.setAttribute("collection", collectionId);
								}
							}
							accessionPanel.select(accession);
						}
					}
				}
			});
			accessionPanel.select(record);
			accessionPanel.show();
			mgr.getToolbar().showButtons(false, true, true);
		} else {
			Criteria criteria = new Criteria();
			criteria.setAttribute("id", record.getAttribute("id"));
			itemPanel.fetchData(criteria, new DSCallback() {
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					if(response.getData() != null && response.getData().length > 0) {
						Record accession = response.getData()[0];
						if(accession != null) {
							String date = accession.getAttribute("name");
							String title = tree.getSelectedRecord().getAttribute("name");
							String localName = accession.getAttribute("localName");
							if(title != null && !title.equals(date)) tree.getSelectedRecord().setAttribute("name", date);
							if(localName != null) tree.getSelectedRecord().setAttribute("localName", localName);
							tree.refreshFields();
							itemPanel.select(accession);
						}
					}
				}
			});
			itemPanel.select(record);
			itemPanel.show();
			Boolean isFolder = record.getAttributeAsBoolean("isFolder");
			mgr.getToolbar().showButtons(true, false, true);
		} 
	}
	@Override
	public void save() {
		String type = tree.getSelectedRecord().getAttribute("localName");
		if(type != null) {
			if(type.equals("collection")) {
				collectionPanel.saveData(new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						if(response.getData() != null && response.getData().length > 0) {
							Record accession = response.getData()[0];
							if(accession != null) {
								String date = accession.getAttribute("title");
								String title = tree.getSelectedRecord().getAttribute("title");
								String localName = accession.getAttribute("localName");
								if(title != null && !title.equals(date)) tree.getSelectedRecord().setAttribute("title", date);
								if(localName != null) {
									tree.getSelectedRecord().setAttribute("localName", localName);
									tree.getSelectedRecord().setAttribute("icon", "/theme/images/tree_icons/"+localName+".png");
								}
								tree.refreshFields();
							}
						}
					}
				});
			} else if(type.equals("category")) {
				categoryPanel.save(new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						if(response.getData() != null && response.getData().length > 0) {
							Record accession = response.getData()[0];
							if(accession != null) {
								String date = accession.getAttribute("name");
								String localName = accession.getAttribute("localName");
								String title = tree.getSelectedRecord().getAttribute("title");
								if(title != null && !title.equals(date)) tree.getSelectedRecord().setAttribute("title", date);
								if(localName != null) {
									tree.getSelectedRecord().setAttribute("localName", localName);
									tree.getSelectedRecord().setAttribute("icon", "/theme/images/tree_icons/"+localName+".png");
								}
								tree.refreshFields();
							}
						}
					}
				});
			} else if(type.equals("accession")) {
				accessionPanel.save(new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						if(response.getData() != null && response.getData().length > 0) {
							Record accession = response.getData()[0];
							if(accession != null) {
								Date date = accession.getAttributeAsDate("date");
								String localName = accession.getAttribute("localName");
								Date title = CollectionTreeDS.getDate(tree.getSelectedRecord().getAttribute("date"));
								if(date.getYear() != title.getYear() || date.getMonth() != title.getMonth() || date.getDate() != title.getDate())
									tree.getSelectedRecord().setAttribute("title", CollectionTreeDS.getFormattedDate(date));									
								if(localName != null) {
									tree.getSelectedRecord().setAttribute("localName", localName);
									tree.getSelectedRecord().setAttribute("icon", "/theme/images/tree_icons/"+localName+".png");
								}
								tree.refreshFields();
							}
						}
					}
				});
			} else {
				itemPanel.saveData(new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						if(response.getData() != null && response.getData().length > 0) {
							Record accession = response.getData()[0];
							if(accession != null) {
								String date = accession.getAttribute("name");
								String title = tree.getSelectedRecord().getAttribute("title");
								String localName = accession.getAttribute("localName");
								if(title != null && !title.equals(date)) tree.getSelectedRecord().setAttribute("title", date);									
								if(localName != null) {
									tree.getSelectedRecord().setAttribute("localName", localName);
									tree.getSelectedRecord().setAttribute("icon", "/theme/images/tree_icons/"+localName+".png");
								}
								tree.refreshFields();
							}
						}
					}
				});
			}
		}
	}
	public void importEntity() {
		
	}
	public void exportEntity() {
		
	}
	@Override
	public void add() {
		String type = selection.getAttribute("localName");
		if(type != null) {
			if(type.equals("accessions")) addAccessionWindow.show();
			else addWindow.show();
		}
	}
	@Override
	public void delete() {
		SC.confirm("Do you really wish to remove this permanently?", new BooleanCallback() {
			public void execute(Boolean value) {
				if(value) tree.removeSelectedData();
			}			
		});		
	}
	@Override
	public void search(String query) {
		Criteria criteria = new Criteria();
		criteria.setAttribute("qname", "{openapps.org_repository_1.0}collection");
		criteria.setAttribute("query", query);
		criteria.setAttribute("field", "freetext");
		criteria.setAttribute("sort", "name_e");
		tree.fetchData(criteria);
	}

	public class AddCollectionWindow extends Window {
		private ButtonItem submitItem;
		private SelectItem levelItem;
		private SelectItem contentTypeItem;
		private DynamicForm addForm;
		
		public AddCollectionWindow() {
			setWidth(370);
			setHeight(130);
			setTitle("Add Category/Item");
			setAutoCenter(true);
			setIsModal(true);
			addForm = new DynamicForm();
			addForm.setMargin(3);
			addForm.setCellPadding(5);
			addForm.setColWidths(70, "*");
			final TextItem nameItem = new TextItem("name", "Name");
			nameItem.setWidth("*");
			
			levelItem = new SelectItem("level", "Level");
			LinkedHashMap<String,String> valueMap = new LinkedHashMap<String,String>();
			valueMap.put("series","Series");
			valueMap.put("subseries","SubSeries");
			valueMap.put("group","Group");
			valueMap.put("subgroup","SubGroup");
			valueMap.put("file","File");
			valueMap.put("item","Item");
			levelItem.setValueMap(valueMap);
			levelItem.addChangedHandler(new ChangedHandler() {
				public void onChanged(ChangedEvent event) {
					if(event.getItem().getValue().equals("item")) {
						contentTypeItem.show();
						addWindow.setHeight(165);
					} else {
						contentTypeItem.hide();
						addWindow.setHeight(130);
					}
				}				
			});
			
			contentTypeItem = new SelectItem("type", "Type");
			contentTypeItem.setValueMap(model.getContentTypes());
			contentTypeItem.setVisible(false);
			
			submitItem = new ButtonItem("submit", "Add");
			//submitItem.setIcon("/theme/images/icons16/add.png");
			submitItem.setStartRow(false);
			submitItem.setEndRow(false);
			submitItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					String level = levelItem.getValueAsString();
					Record record = new Record();
					String id = tree.getSelectedRecord().getAttribute("id");
					if(id.endsWith("-categories")) id = id.substring(0, id.length()-11);
					record.setAttribute("source", id);
					if(level.equals("item")) {
						record.setAttribute("assoc_qname", "{openapps.org_repository_1.0}items");
						record.setAttribute("entity_qname", contentTypeItem.getValueAsString());
					} else {
						record.setAttribute("assoc_qname", "{openapps.org_repository_1.0}categories");
						record.setAttribute("entity_qname", "{openapps.org_repository_1.0}category");
					}					
					record.setAttribute("name", nameItem.getValueAsString());
					record.setAttribute("format", "tree");
					tree.addData(record, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							addWindow.hide();
						}
					});
				}
			});
			
			addForm.setFields(nameItem,levelItem,contentTypeItem,submitItem);
			addItem(addForm);
		}
		public void selectRecord(Record record) {
			String type = record.getAttribute("localName");
			
		}
	}
	public class AddAccessionWindow extends Window {
		@SuppressWarnings("deprecation")
		public AddAccessionWindow() {
			setWidth(400);
			setHeight(100);
			setTitle("Add Accession");
			setAutoCenter(true);
			setIsModal(true);
			DynamicForm addForm = new DynamicForm();
			addForm.setCellPadding(7);
			final DateItem dateItem = new DateItem("date", "Date");
			dateItem.setStartDate(new Date(1990, 1, 1));
						
			ButtonItem submitItem = new ButtonItem("submit", "Add");
			submitItem.setIcon("/theme/images/icons16/add.png");
			submitItem.setStartRow(false);
			submitItem.setEndRow(false);
			submitItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					Record record = new Record();
					String id = tree.getSelectedRecord().getAttribute("id");
					if(id.endsWith("-accessions")) id = id.substring(0, id.length()-11);
					record.setAttribute("assoc_qname", "{openapps.org_repository_1.0}accessions");
					record.setAttribute("entity_qname", "{openapps.org_repository_1.0}accession");
					record.setAttribute("source", id);
					record.setAttribute("date", CollectionTreeDS.getShortFormattedDate(dateItem.getValueAsDate()));
					record.setAttribute("format", "tree");
					tree.addData(record, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							addAccessionWindow.hide();
						}
					});
				}
			});
			
			addForm.setFields(dateItem,submitItem);
			addItem(addForm);
		}
	}

}