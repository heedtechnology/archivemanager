package org.archivemanager.repository.client;
import java.util.LinkedHashMap;

import org.heed.openapps.gwt.client.component.Toolbar;

import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.widgets.form.fields.events.ChangeEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangeHandler;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;


public class RepositoryManagerToolbar extends Toolbar {
	private TextItem searchField;
	private DateItem dateItem;
	private CheckboxItem searchDate;
	private SelectItem classificationSelect;
	
	
	public RepositoryManagerToolbar(final RepositoryManagerEntryPoint mgr) {
		super(25);
		setWidth("100%");
		//setHeight(25);
		setLayoutLeftMargin(2);
		setMembersMargin(5);
				
		DynamicForm searchForm = new DynamicForm();
		searchForm.setWidth(300);
		searchForm.setHeight(25);
		searchForm.setMargin(0);
		searchForm.setNumCols(4);
		searchForm.setCellPadding(2);
		searchField = new TextItem("query");
		searchField.setShowTitle(false);
		searchField.setWidth(255);
		ButtonItem searchButton = new ButtonItem("search", "search");
		searchButton.setWidth(50);
		searchButton.setStartRow(false);
		searchButton.setEndRow(false);
		searchButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				mgr.search(searchField.getValueAsString());
			}
		});
		
		dateItem = new DateItem();
		dateItem.setWidth(100);
		dateItem.setDisplayFormat(DateDisplayFormat.TOUSSHORTDATE);
		dateItem.setShowTitle(false);
		dateItem.setDisabled(true);
		
		searchDate = new CheckboxItem();
		searchDate.setWidth(25);
		searchDate.setShowTitle(false);
		searchDate.setShowLabel(false);
		searchDate.addChangeHandler(new ChangeHandler() {
			public void onChange(ChangeEvent event) {
				Object value = event.getValue();
				if(value.equals(true)) {
					dateItem.setDisabled(false);
					searchField.setDisabled(true);
        		} else {
					dateItem.setDisabled(true);
        			searchField.setDisabled(false);
        		}
			}
		});		
		
		classificationSelect = new SelectItem("classification");
		classificationSelect.setWidth(100);
		LinkedHashMap<String,String> valueMap1 = new LinkedHashMap<String,String>();
		valueMap1.put("{openapps.org_classification_1.0}person","Person");
		valueMap1.put("{openapps.org_classification_1.0}corporation","Corporate");
		valueMap1.put("{openapps.org_classification_1.0}subject","Subject");
		classificationSelect.setValueMap(valueMap1);
		classificationSelect.setDefaultValue("{openapps.org_classification_1.0}person");
		classificationSelect.setShowTitle(false);
		
		searchForm.setFields(searchField,searchButton,dateItem,searchDate,classificationSelect);
		setFormCanvas(searchForm);	
		
		this.addButton("add", "/theme/images/icons32/add.png", "Add", new com.smartgwt.client.widgets.events.ClickHandler() {  
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				mgr.add();
			}  
		});
		this.addButton("delete", "/theme/images/icons32/delete.png", "Delete", new com.smartgwt.client.widgets.events.ClickHandler() {  
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				mgr.delete();
			}  
		});
		this.addButton("save", "/theme/images/icons32/disk.png", "Save", new com.smartgwt.client.widgets.events.ClickHandler() {  
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				mgr.save();
			}  
		});
		
	}
	
	public void showSearchSupport(String entity) {
		dateItem.hide();
		searchDate.hide();
		classificationSelect.hide();
		if(entity == "accession") {
			dateItem.show();
			searchDate.show();
		} else if(entity == "classification"){
			classificationSelect.show();
		}
	}
	public void setQuery(String query) {
		searchField.setValue(query);
	}
	public boolean isDateSearch() {
		return searchDate.getValueAsBoolean();
	}
	public String getClassification() {
		return classificationSelect.getValueAsString();
	}
	public void showAddButton(boolean show) {		
		if(show) this.showButton("add");
		else this.hideButton("add");
	}
	public void showDeleteButton(boolean show) {
		if(show) this.showButton("delete");
		else this.hideButton("delete");
	}
	public void showSaveButton(boolean show) {
		if(show) this.showButton("save");
		else this.hideButton("save");
	}
	public void showButtons(boolean add, boolean delete, boolean save) {
		showAddButton(add);
		showDeleteButton(delete);
		showSaveButton(save);
	}
	public void progressStart(final String message) {
					
	}	
	/*
	public void progressEnd() {
		progressState = false;
		progressBar.hide();
		progressLabel.hide();
	}
	public void showMessage(String message) {
		messageLabel.setContents("<label style='font-size:12px;font-weight:bold;color:#4C4C4C;font-family:Arial,Verdana,sans-serif;'>"+message+"</label>");
		messageImg.show();
		messageLabel.show();
		messageImgClose.show();
	}
	public void hideMessage() {
		messageImg.hide();
		messageLabel.hide();
		messageImgClose.hide();
	}
	*/
}
