package org.archivemanager.repository.client;

import org.archivemanager.gwt.client.component.ContentAssociationPanel;
import org.archivemanager.repository.client.classification.NamedEntityPanel;
import org.archivemanager.repository.client.classification.SubjectPanel;
import org.archivemanager.repository.client.data.ClassificationListDS;
import org.heed.openapps.gwt.client.Application;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.data.NativeClassificationModel;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;

public class ClassificationApplication extends HLayout implements Application {
	private NativeClassificationModel model = new NativeClassificationModel();
	private RepositoryManagerEntryPoint mgr;
	private ListGrid grid;
	private TabSet tabs;
	private NamedEntityPanel classificationPanel;
	private SubjectPanel subjectPanel;
	private ContentAssociationPanel contentPanel;
	
	private AddClassificationWindow addWindow;
	
	public ClassificationApplication(RepositoryManagerEntryPoint mgr) {
		this.mgr = mgr;
		setWidth100();
		setHeight100();
		setMembersMargin(5);
		
		grid = new ListGrid();
		grid.setWidth(300);
		grid.setHeight("100%");
		grid.setBorder("0px");
		grid.setShowHeader(false);
		grid.setShowResizeBar(true);
		ListGridField nameField = new ListGridField("name","Name");
		grid.setDataSource(ClassificationListDS.getInstance());
		grid.setFields(nameField);
		grid.addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				select(event.getRecord());
			}
		});
        addMember(grid);
        
        tabs = new TabSet();
		tabs.setWidth100();
		tabs.setHeight100();
		addMember(tabs);
		        
        Tab homeTab = new Tab("Home");
        Canvas canvas = new Canvas();
        canvas.setWidth100();
        canvas.setHeight100();        
        classificationPanel = new NamedEntityPanel();
        canvas.addChild(classificationPanel);
        subjectPanel = new SubjectPanel();
        canvas.addChild(subjectPanel);
        homeTab.setPane(canvas);
        tabs.addTab(homeTab);
        
        Tab contentTab = new Tab("Content");
        contentPanel = new ContentAssociationPanel();
        contentTab.setPane(contentPanel);
        tabs.addTab(contentTab);
        
        tabs.hide();
        
        addWindow = new AddClassificationWindow();
	}
	
	@Override
	public void select(Record record) {		
		Criteria criteria = new Criteria();
		criteria.setAttribute("id", record.getAttribute("id"));
		String type = record.getAttribute("localName");
		if(type.equals("person") || type.equals("corporation")) {
			classificationPanel.select(record);
			classificationPanel.fetchData(criteria, new DSCallback() {
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					if(response.getData() != null && response.getData().length > 0) {
						Record accession = response.getData()[0];
						if(accession != null) {
							String date = accession.getAttribute("name");
							String title = grid.getSelectedRecord().getAttribute("name");
							if(title != null && !title.equals(date)) {
								grid.getSelectedRecord().setAttribute("name", date);
								grid.refreshFields();
							}
							subjectPanel.hide();
							classificationPanel.show();							
						}
						contentPanel.select(accession);
					}
				}
			});	
		} else if(type.equals("subject")) {
			subjectPanel.select(record);
			subjectPanel.fetchData(criteria, new DSCallback() {
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					if(response.getData() != null && response.getData().length > 0) {
						Record accession = response.getData()[0];
						if(accession != null) {
							String date = accession.getAttribute("name");
							String title = grid.getSelectedRecord().getAttribute("name");
							if(title != null && !title.equals(date)) {
								grid.getSelectedRecord().setAttribute("name", date);
								grid.refreshFields();
							}
							classificationPanel.hide();
							subjectPanel.show();							
						}
						contentPanel.select(accession);
					}
				}
			});	
		}
		tabs.show();
		mgr.getToolbar().showAddButton(true);
		mgr.getToolbar().showDeleteButton(true);
		mgr.getToolbar().showSaveButton(true);
	}
	@Override
	public void save() {
		classificationPanel.save(new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				if(response.getData() != null && response.getData().length > 0) {
					Record accession = response.getData()[0];
					if(accession != null) {
						String date = accession.getAttribute("name");
						String title = grid.getSelectedRecord().getAttribute("name");
						if(title != null && !title.equals(date)) {
							grid.getSelectedRecord().setAttribute("name", date);
							grid.refreshFields();
						}
					}
				}
			}
		});		
	}
	public void importEntity() {
		
	}
	public void exportEntity() {
		
	}
	@Override
	public void add() {
		addWindow.show();
	}
	@Override
	public void delete() {
		SC.confirm("Do you really wish to remove this permanently?", new BooleanCallback() {
			public void execute(Boolean value) {
				if(value) grid.removeSelectedData();
			}			
		});		
	}
	@Override
	public void search(String query) {
		Criteria criteria = new Criteria();
		criteria.setAttribute("qname", mgr.getToolbar().getClassification());
		criteria.setAttribute("query", query);
		criteria.setAttribute("field", "freetext");
		criteria.setAttribute("sort", "name_e");
		grid.fetchData(criteria);
	}

	public class AddClassificationWindow extends Window {
		public AddClassificationWindow() {
			setWidth(400);
			setHeight(140);
			setTitle("Add Classification");
			setAutoCenter(true);
			setIsModal(true);
			
			/*
			 * isc.DynamicForm.create({ID:'contactsListAddWindowForm',width:'100%',datasource:'contacts',margin:10,numCols:2,cellPadding:7,
	    	fields:[
	    	    {name:'name',width:'*',required:true,title:'Name'},
	    	    {name:'type',width:'*',required:true,title:'Type',type:'select',valueMap:{'person':'Person','corporation':'Corporation','subject':'Subject'}},
	    	    {name:'validateBtn',title:'Save',type:'button',
	    	    	click:function() {
	    	    		var parms = contactsListAddWindowForm.getValues();
	    	    		parms['qname'] = '{openapps.org_classification_1.0}'+contactsListAddWindowForm.getValue('type');
	    	    		contactsListList.addData(parms);
	    	    		contactsListAddWindow.hide();
	    	    	}
	    	    }
	    	]
	    })
			 */
			final DynamicForm addForm = new DynamicForm();
			addForm.setCellPadding(7);
			final TextItem nameItem = new TextItem("name", "Name");
			nameItem.setWidth("*");
			
			SelectItem typeItem = new SelectItem("type");
			typeItem.setValueMap(model.getClassificationType());
			
			ButtonItem submitItem = new ButtonItem("submit", "Add");
			submitItem.setIcon("/theme/images/icons16/add.png");
			submitItem.setStartRow(false);
			submitItem.setEndRow(false);
			submitItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					Record record = new Record();
					record.setAttribute("qname", "{openapps.org_classification_1.0}"+addForm.getValueAsString("type"));
					record.setAttribute("name", nameItem.getValueAsString());
					grid.addData(record, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							addWindow.hide();
						}
					});
				}
			});
			
			addForm.setFields(nameItem,typeItem,submitItem);
			addItem(addForm);
		}
	}

}