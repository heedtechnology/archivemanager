package org.archivemanager.repository.client.classification;

import org.archivemanager.repository.client.classification.form.SubjectForm;
import org.heed.openapps.gwt.client.component.SearchTermComponent;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.layout.VLayout;

public class SubjectPanel extends VLayout {
	private SubjectForm subjectForm;
	private SearchTermComponent searchTerms;
	
	private Record selection;
	
	public SubjectPanel() {
		setWidth100();
		Canvas formCanvas = new Canvas();
		subjectForm = new SubjectForm();
		formCanvas.addChild(subjectForm);
		subjectForm.hide();
		
		addMember(formCanvas);
		
		VLayout layout = new VLayout();
		addMember(layout);
		layout.setLayoutLeftMargin(105);
		searchTerms = new SearchTermComponent("500");
		searchTerms.setMargin(5);
		layout.addMember(searchTerms);
		Canvas spacer = new Canvas();
		spacer.setWidth(500);
		spacer.setHeight100();
		layout.addMember(spacer);		
	}
	
	public void fetchData(Criteria criteria, DSCallback callback) {
		if(selection != null) {
			String type = selection.getAttribute("localName");
			if(type != null && type.equals("subject")) {
				subjectForm.show();
				subjectForm.fetchData(criteria, callback);
			}
		}
	}
	public void select(Record selection) {
		this.selection = selection;
		searchTerms.select(selection);
	}
	public void save(DSCallback callback) {
		if(selection != null) {
			subjectForm.saveData(callback);
		}
	}
}
