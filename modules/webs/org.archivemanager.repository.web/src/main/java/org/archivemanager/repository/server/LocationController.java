package org.archivemanager.repository.server;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.heed.openapps.QName;
import org.heed.openapps.RepositoryModel;
import org.heed.openapps.entity.Association;
import org.heed.openapps.entity.BaseEntityQuery;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityQuery;
import org.heed.openapps.entity.EntityResultSet;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.entity.ValidationResult;
import org.heed.openapps.entity.ExportProcessor;
import org.heed.openapps.entity.data.FormatInstructions;
import org.heed.openapps.template.TemplateService;
import org.heed.openapps.util.NumberUtility;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class LocationController implements ApplicationContextAware {
	@Autowired protected EntityService entityService;
	@Autowired protected TemplateService templateService;
	
	public void setApplicationContext(ApplicationContext ctx) throws BeansException {
		//LocationDataGenerator generator = new LocationDataGenerator();
		//entityService.registerDataGenerator(RepositoryModel.LOCATION.toString(), generator);
	}
	
	@RequestMapping(value="/locations", method = RequestMethod.GET)
	public ModelAndView fetchLocationTreeData(HttpServletRequest request, HttpServletResponse response) throws Exception {
		QName qname = RepositoryModel.LOCATION;
		StringBuffer buff = new StringBuffer();
		String parent = request.getParameter("parent");
		if(parent == null || parent.equals("null") || parent.equals("0")) {
			String query = request.getParameter("query");
			EntityQuery eQuery = (query != null && !query.equals("null")) ? new BaseEntityQuery(qname, query, null, true) : new BaseEntityQuery(qname, null, null, true);
			int startRow = (request.getParameter("_startRow") != null) ? Integer.valueOf(request.getParameter("_startRow")) : 0;
			int endRow = (request.getParameter("_endRow") != null) ? Integer.valueOf(request.getParameter("_endRow")) : 0;
			eQuery.setStartRow(startRow);
			eQuery.setEndRow(endRow);
			//eQuery.setDefaultOperator("AND");
			EntityResultSet results = entityService.search(eQuery);
			ExportProcessor processor = entityService.getExportProcessor("default");
			for(int i=0; i < results.getResults().size(); i++) {
				buff.append(processor.export(new FormatInstructions(), results.getResults().get(i)));
			}
			return response(results.getResultSize()+" "+qname.getLocalName()+" fetched", results.getResultSize(), results.getStartRow(), results.getEndRow(), buff.toString(), response.getWriter());
		} else {
			Entity collection = entityService.getEntity(Long.valueOf(parent));
			List<Entity> list = new ArrayList<Entity>();
			for(Association assoc : collection.getSourceAssociations(RepositoryModel.LOCATIONS)) {
				Entity accession = entityService.getEntity(assoc.getTarget());
				//accession.setParent(collection);
				list.add(accession);
			}
			//Collections.sort(list, sorter);
			//Collections.reverse(list);
			ExportProcessor processor = entityService.getExportProcessor("default");
			for(Entity component : list) {	
				buff.append(processor.export(new FormatInstructions(true), component));
			}
			return response("", buff.toString(), response.getWriter());
		}
	}
	@RequestMapping(value="/location/add", method = RequestMethod.POST)
	public ModelAndView entityAssociationAdd(HttpServletRequest request, HttpServletResponse response) throws Exception {
		QName aQname = RepositoryModel.LOCATIONS;
		QName eQname = RepositoryModel.LOCATION;
		String sourceStr = request.getParameter("source");
		Entity entity = entityService.getEntity(request, eQname);
		ValidationResult entityResult = entityService.validate(entity);
		if(entityResult.isValid()) {
			entityService.addEntity(entity);
			if(sourceStr != null && NumberUtility.isLong(sourceStr)) {
				Long source = Long.valueOf(sourceStr);
				//entity.setParent(entityService.getEntity(source));
				Association assoc = new Association(aQname, source, entity.getId());
				ValidationResult assocResult = entityService.validate(assoc);
				if(assocResult.isValid()) {
					entityService.addAssociation(assoc);
					entity.getTargetAssociations().add(assoc);
				}
			}
			ExportProcessor processor = entityService.getExportProcessor("default");
			response("", processor.export(new FormatInstructions(), entity), response.getWriter());
			
		} else response("", entityResult.toXml(), response.getWriter());
		return null;
	}
	
	protected ModelAndView response(String message, String data, PrintWriter out) {
		out.write("<response><status>0</status><message>"+message+"</message><data>"+data+"</data></response>");
		return null;
	}
	protected ModelAndView response(String message, int total, int start, int end, String data, PrintWriter out) {
		out.write("<response><status>0</status><totalRows>"+total+"</totalRows><startRow>"+start+"</startRow><endRow>"+end+"</endRow><message>"+message+"</message><data>"+data+"</data></response>");
		return null;
	}
}
