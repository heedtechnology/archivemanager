package org.archivemanager.repository.client.collection;

import java.util.LinkedHashMap;

import org.archivemanager.repository.client.data.CollectionTreeDS;
import org.archivemanager.repository.client.data.NativeCollectionModel;
import org.heed.openapps.gwt.client.Application;
import org.heed.openapps.gwt.client.data.NativeContentTypes;
import org.heed.openapps.gwt.client.data.RestUtility;
import org.heed.openapps.gwt.client.form.ContainerItem;

import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.events.ClickHandler;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;
import com.smartgwt.client.widgets.tree.TreeGrid;
import com.smartgwt.client.widgets.tree.TreeNode;
import com.smartgwt.client.widgets.tree.events.FolderDropEvent;
import com.smartgwt.client.widgets.tree.events.FolderDropHandler;

public class CollectionTree extends TreeGrid {
	private NativeContentTypes model = new NativeContentTypes();
	private NativeCollectionModel collectionTypes = new NativeCollectionModel();
	//private Application mgr;
	
	private Menu emptyContextMenu;
	private Menu addContextMenu;
	private Menu fullContextMenu;
	
	private AddPropertyWindow addWindow;
	
	public CollectionTree(final Application mgr) {
		//this.mgr = mgr;
		setHeight100();
		setWidth100();
		setShowHeader(false);
		setBorder("0px");
		setWrapCells(true);
		setFixedRecordHeights(false);
		setCanReorderRecords(true);  
        setCanAcceptDroppedRecords(true);
        setSelectionType(SelectionStyle.SINGLE);
		CollectionTreeDS ds = CollectionTreeDS.getInstance();
        setDataSource(ds);
        
        addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				mgr.select(event.getRecord());	
			}			
		});
        addFolderDropHandler(new FolderDropHandler() {
        	public void onFolderDrop(FolderDropEvent event) {        		
        		TreeNode child = event.getNodes()[0];
        		TreeNode parent = event.getFolder();
        		String parentType = parent.getAttribute("localName");
        		String childType = child.getAttribute("localName"); 
        		if((parentType.equals("accessions") && childType.equals("accession")) ||
        				(parentType.equals("categories") && childType.equals("category")) ||
        				(parentType.equals("categories") && isItem(childType)) ||
        				(parentType.equals("category") && childType.equals("category")) ||
        				(parentType.equals("category") && isItem(childType))) {
        			//System.out.println("drop accepted");
        			Record record = new Record();
        			String parentId = parent.getAttribute("id");
        			if(parentType.equals("accessions") || parentType.equals("categories")) 
        				parentId = parentId.substring(0,  parentId.length()-11);
            		record.setAttribute("id", child.getAttribute("id"));
            		record.setAttribute("parent", parentId);
            		updateData(record);
        		} else {
        			SC.say("sorry, you cannot relocate your item to that folder");    			
        		}
        		event.cancel();
			}
        });     
        
        MenuItem addItem = new MenuItem("Add New Item");
        addItem.addClickHandler(new ClickHandler() {
        	public void onClick(MenuItemClickEvent event) {
				mgr.add();
			}
        });
        MenuItem delItem = new MenuItem("Delete Item");
        delItem.addClickHandler(new ClickHandler() {
        	public void onClick(MenuItemClickEvent event) {
        		mgr.delete();
			}
        });
        MenuItem propItem = new MenuItem("Set Child Property");
        propItem.addClickHandler(new ClickHandler() {
        	public void onClick(MenuItemClickEvent event) {
				addWindow.show();
			}
        });        
        addContextMenu = new Menu(); 
        addContextMenu.setItems(addItem);
        
        emptyContextMenu = new Menu();
        fullContextMenu = new Menu();
        fullContextMenu.setItems(addItem,delItem,propItem);
        
        addWindow = new AddPropertyWindow();
	}
	
	public void select(Record record) {
		if(record != null) {
			String type = record.getAttribute("localName");
			if(type != null) {
				if(type.equals("accessions") || type.equals("categories") || type.equals("collection")) 
					setContextMenu(addContextMenu);
				else if(type.equals("category") || isItem(type)) setContextMenu(fullContextMenu);
				else setContextMenu(emptyContextMenu);
			}
		}
	}
	protected boolean isItem(String type) {
		return model.getContentTypes().containsKey("{openapps.org_repository_1.0}"+type);
	}
	public class AddPropertyWindow extends Window {
		private ButtonItem submitItem;
		private SelectItem levelItem;
		private SelectItem contentTypeItem;
		private ContainerItem containerItem;
		private DynamicForm addForm;
		
		public AddPropertyWindow() {
			setWidth(325);
			setHeight(100);
			setTitle("Set Child Property");
			setAutoCenter(true);
			setIsModal(true);
			addForm = new DynamicForm();
			addForm.setMargin(3);
			addForm.setCellPadding(5);
			addForm.setColWidths(100, "*");
			
			levelItem = new SelectItem("level", "Level");
			LinkedHashMap<String,String> valueMap = new LinkedHashMap<String,String>();
			valueMap.put("content_type","Content Type");
			valueMap.put("container","Container");
			levelItem.setValueMap(valueMap);
			levelItem.addChangedHandler(new ChangedHandler() {
				public void onChanged(ChangedEvent event) {
					if(event.getItem().getValue().equals("content_type")) {
						contentTypeItem.show();
						containerItem.hide();
						addWindow.setHeight(130);
					} else {
						containerItem.show();
						contentTypeItem.hide();
						addWindow.setHeight(140);
					}
				}				
			});			
			contentTypeItem = new SelectItem("type", "Content Type");
			contentTypeItem.setValueMap(model.getContentTypes());
			contentTypeItem.setVisible(false);
			
			containerItem = new ContainerItem("container", "Container", collectionTypes.getContainerTypes());
			containerItem.setVisible(false);
			
			submitItem = new ButtonItem("submit", "Add");
			//submitItem.setIcon("/theme/images/icons16/add.png");
			submitItem.setStartRow(false);
			submitItem.setEndRow(false);
			submitItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					/*
					 * var id = repositoryTree.getSelectedRecord().id;
           	    		if(childPropertyForm.getValue('type') == 'level2') {           	    			
               	    		var value = childPropertyForm.getValue("level2");  
               	    		var qname = 'qname';
           	    		} else if(childPropertyForm.getValue('type') == 'component_container') {           	    			
               	    		var value = childPropertyForm.getValue("container");
               	    		var qname = '{openapps.org_repository_1.0}container';
           	    		}
           	    		isc.XMLTools.loadXML("/repository/collection/propagate.xml?id="+id+"&qname="+qname+"&value="+value, function(xmlDoc, xmlText) {
           	    			childPropertyWindow.hide();
           	    		});         	
					 */
					String level = levelItem.getValueAsString();
					Record record = new Record();
					record.setAttribute("id", getSelectedRecord().getAttribute("id"));
					if(level.equals("content_type")) {
						record.setAttribute("qname", "qname");
						record.setAttribute("value", contentTypeItem.getValue());
					} else {
						record.setAttribute("qname", "{openapps.org_repository_1.0}"+levelItem.getValue());
						record.setAttribute("value", containerItem.getValue());
					}					
					RestUtility.post("/repository/collection/propagate.xml", record, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							addWindow.hide();
							SC.say("Property successfully set on children");
						}
					});
				}
			});
			
			addForm.setFields(levelItem,contentTypeItem,containerItem,submitItem);
			addItem(addForm);
		}
	}
	
}
