package org.archivemanager.repository.client.data;

import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;


public class AccessionTreeDS extends RestDataSource {
	private static AccessionTreeDS instance = null;  
	  
    public static AccessionTreeDS getInstance() {  
        if (instance == null) {  
            instance = new AccessionTreeDS("accessionTreeDS");  
        }  
        return instance;  
    }
    
	public AccessionTreeDS(String id) {
		setID(id);  
        setTitleField("Name");	        
        DataSourceTextField nameField = new DataSourceTextField("title", "Title");     
        
        DataSourceTextField idField = new DataSourceTextField("id", "ID",50);  
        idField.setPrimaryKey(true);  
        idField.setRequired(true);
        
        DataSourceTextField parentField = new DataSourceTextField("parent", "Parent");  
        parentField.setRequired(true);  
        parentField.setForeignKey(id + ".id");  
        parentField.setRootValue("null");  
        
        setFields(idField,nameField,parentField);
        
        setFetchDataURL("/service/archivemanager/repository/accession.xml");
        setAddDataURL("/service/archivemanager/repository/accession/add.xml"); 
        setRemoveDataURL("/service/entity/remove.xml");
        setUpdateDataURL("/service/archivemanager/repository/accession/update.xml");
	}
}
