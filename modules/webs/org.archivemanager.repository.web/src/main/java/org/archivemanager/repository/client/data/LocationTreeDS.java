package org.archivemanager.repository.client.data;

import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;


public class LocationTreeDS extends RestDataSource {
	private static LocationTreeDS instance = null;  
	  
    public static LocationTreeDS getInstance() {  
        if (instance == null) {  
            instance = new LocationTreeDS("locationListDS");  
        }  
        return instance;  
    }
    
	public LocationTreeDS(String id) {
		setID(id);  
        setTitleField("Name");	        
        DataSourceTextField nameField = new DataSourceTextField("title", "Title");     
        
        DataSourceTextField idField = new DataSourceTextField("id", "ID",50);  
        idField.setPrimaryKey(true);  
        idField.setRequired(true);
        
        DataSourceTextField parentField = new DataSourceTextField("parent", "Parent");  
        parentField.setRequired(true);  
        parentField.setForeignKey(id + ".id");  
        parentField.setRootValue("null");  
        
        setFields(idField,nameField,parentField);
        
		setAddDataURL("/service/entity/create.xml");
		setFetchDataURL("/service/archivemanager/repository/locations.xml");
		setRemoveDataURL("/service/entity/remove.xml");
		setUpdateDataURL("/service/entity/update.xml");	
	}
}
