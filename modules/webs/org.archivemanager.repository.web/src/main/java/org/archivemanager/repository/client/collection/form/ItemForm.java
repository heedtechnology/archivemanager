package org.archivemanager.repository.client.collection.form;
import java.util.LinkedHashMap;

import org.archivemanager.repository.client.data.ItemDS;
import org.archivemanager.repository.client.data.NativeCollectionModel;
import org.heed.openapps.gwt.client.data.NativeContentTypes;
import org.heed.openapps.gwt.client.form.ContainerItem;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.ValuesManager;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.RichTextItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.SpinnerItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class ItemForm extends VLayout {
	private final ValuesManager vm;
	private NativeContentTypes contentTypes = new NativeContentTypes();
	private NativeCollectionModel collectionTypes = new NativeCollectionModel();
	
	private DynamicForm form1;
	private DynamicForm form2;
	private DynamicForm form3;
	
	private SelectItem mediumItem;
	private SelectItem genreItem;
	private SelectItem formItem;
	private TextItem authorsItem;
	
	public ItemForm() {
		super();
		setWidth100();
		setMembersMargin(5);
		
		vm = new ValuesManager();
		vm.setDataSource(ItemDS.getInstance());
		
		form1 = new DynamicForm();
		form1.setNumCols(4);
		form1.setColWidths("125","*","75","*");
		form1.setValuesManager(vm);
				
		RichTextItem nameItem = new RichTextItem("name","Heading");
		nameItem.setHeight(75);
		nameItem.setWidth(425);
		nameItem.setColSpan(3);
		nameItem.setShowTitle(true);
		nameItem.setStartRow(false);
		nameItem.setEndRow(false);
		nameItem.setControlGroups(new String[]{"fontControls", "styleControls"});
		
		TextAreaItem scopeItem = new TextAreaItem("description","Description");
		scopeItem.setHeight(50);
		scopeItem.setWidth(430);
		scopeItem.setColSpan(3);
		scopeItem.setShowTitle(true);
		
		TextAreaItem commentsItem = new TextAreaItem("summary","Summary");
		commentsItem.setHeight(50);
		commentsItem.setWidth(430);
		commentsItem.setColSpan(3);
		commentsItem.setShowTitle(true);
						
		TextItem accessionDateItem = new TextItem("accession_date", "Accession Date");
		accessionDateItem.setWidth(430);
		accessionDateItem.setColSpan(3);
		
		LinkedHashMap<String,String> valueMap2 = new LinkedHashMap<String,String>();
		valueMap2.put("box", "Box");
		valueMap2.put("cubic_foot", "Cubic Foot");
		valueMap2.put("envelope", "Envelope");
		valueMap2.put("file", "File");
		valueMap2.put("folder", "Folder");
		valueMap2.put("item", "Item");
		valueMap2.put("linear_foot", "Linear Foot");
		valueMap2.put("object", "Object");
		valueMap2.put("package", "Package");
		//extentItem = new SpinnerSelectItem("extent_number","extent_units", "Extents", 150, valueMap2);
		SpinnerItem extentValueItem = new SpinnerItem("extent_number", "Extent Value");
		extentValueItem.setWidth(75);
		
		SelectItem extentUnitsItem = new SelectItem("extent_units", "Extent Units");
		extentUnitsItem.setWidth(100);
		extentUnitsItem.setValueMap(valueMap2);
				
		ContainerItem containerItem = new ContainerItem("container", "Container", collectionTypes.getContainerTypes());
		containerItem.setColSpan(4);
		//containerItem.setWidth(150);
		
		SelectItem levelItem = new SelectItem("level", "Level");
		LinkedHashMap<String,String> valueMap3 = new LinkedHashMap<String,String>();
		valueMap3.put("series","Series");
		valueMap3.put("subseries","Subseries");
		valueMap3.put("group","Group");
		valueMap3.put("subgroup","Subgroup");
		valueMap3.put("file","File");
		valueMap3.put("item","Item");
		levelItem.setValueMap(valueMap3);
		
		SelectItem contentTypeItem = new SelectItem("qname", "Content Type");
		LinkedHashMap<String,String> valueMap4 = new LinkedHashMap<String,String>();
		valueMap4.put("{openapps.org_repository_1.0}category","Category");
		valueMap4.put("{openapps.org_repository_1.0}artwork","Artwork");
		valueMap4.put("{openapps.org_repository_1.0}audio","Audio");
		valueMap4.put("{openapps.org_repository_1.0}correspondence","Correspondence");
		valueMap4.put("{openapps.org_repository_1.0}financial","Financial");
		valueMap4.put("{openapps.org_repository_1.0}journals","Journals");
		valueMap4.put("{openapps.org_repository_1.0}legal","Legal");
		valueMap4.put("{openapps.org_repository_1.0}manuscript","Manuscript");
		valueMap4.put("{openapps.org_repository_1.0}medical","Medical");
		valueMap4.put("{openapps.org_repository_1.0}memorabilia","Memorabilia");
		valueMap4.put("{openapps.org_repository_1.0}miscellaneous","Miscellaneous");
		valueMap4.put("{openapps.org_repository_1.0}notebooks","Notebooks");
		valueMap4.put("{openapps.org_repository_1.0}photographs","Photographs");
		valueMap4.put("{openapps.org_repository_1.0}printed_material","Printed Material");
		valueMap4.put("{openapps.org_repository_1.0}professional","Professional");
		valueMap4.put("{openapps.org_repository_1.0}research","Research");
		valueMap4.put("{openapps.org_repository_1.0}scrapbooks","Scrapbooks");
		valueMap4.put("{openapps.org_repository_1.0}video","Video");
		contentTypeItem.setValueMap(valueMap4);
						
		form1.setFields(nameItem,scopeItem,commentsItem,accessionDateItem,extentValueItem, extentUnitsItem,
				levelItem,contentTypeItem,containerItem);
		
		HLayout topFormLayout = new HLayout();
		topFormLayout.setBorder("1px solid #C0C3C7");
		addMember(topFormLayout);
		topFormLayout.addMember(form1);
		
		form2 = new DynamicForm();
		form2.setBorder("1px solid #C0C3C7");
		form2.setNumCols(6);
		form2.setCellPadding(5);
		form2.setColWidths("85","*","85","*","85","*");
		form2.setValuesManager(vm);
		
		genreItem = new SelectItem("genre","Genre");
		genreItem.setValueMap(new LinkedHashMap<String,Object>(0));
		
		formItem = new SelectItem("form","Form");
		formItem.setValueMap(contentTypes.getCorrespondenceForms());
		
		mediumItem = new SelectItem("medium","Medium");
		mediumItem.setValueMap(contentTypes.getCorrespondenceForms());
		
		authorsItem = new TextItem("authors", "Authors");
		authorsItem.setWidth(200);
		authorsItem.setColSpan(3);
		
		form2.setFields(genreItem,mediumItem,formItem,authorsItem);
		addMember(form2);
		
		form3 = new DynamicForm();
		form3.setMargin(5);
		form3.setCellPadding(3);
		form3.setValuesManager(vm);
		
		StaticTextItem idItem = new StaticTextItem("id", "ID");
		
		TextItem dateItem = new TextItem("date_expression", "Date Expression");
		dateItem.setWidth(150);
				
		CheckboxItem restrictionItem = new CheckboxItem("restrictions", "Restrictions");
		restrictionItem.setHeight(35);
		restrictionItem.setLabelAsTitle(true);
		
		CheckboxItem internalItem = new CheckboxItem("internal", "Internal");
		internalItem.setHeight(35);
		internalItem.setLabelAsTitle(true);
		
		SelectItem languageItem = new SelectItem("language", "Language");
		languageItem.setWidth(150);
		LinkedHashMap<String,String> valueMap1 = new LinkedHashMap<String,String>();
		valueMap1.put("ar","Arabic");
		valueMap1.put("zh","Chinese");
		valueMap1.put("cs","Czech");
		valueMap1.put("da","Danish");
		valueMap1.put("nl","Dutch");
		valueMap1.put("en","English");
		valueMap1.put("fi","Finnish");
		valueMap1.put("fr","French");
		valueMap1.put("de","German");
		valueMap1.put("el","Greek");
		valueMap1.put("he","Hebrew");
		valueMap1.put("hu","Hungarian");
		valueMap1.put("is","Icelandic");
		valueMap1.put("it","Italian");
		valueMap1.put("ja","Japanese");
		valueMap1.put("ko","Korean");
		valueMap1.put("no","Norwegian");
		valueMap1.put("pl","Polish");
		valueMap1.put("pt","Portugese");
		valueMap1.put("ru","Russian");
		valueMap1.put("es","Spanish");
		valueMap1.put("sv","Swedish");
		valueMap1.put("th","Thai");
		valueMap1.put("tr","Turkish");
		languageItem.setValueMap(valueMap1);
		
		SpinnerItem sizeItem = new SpinnerItem("size", "Size %");
		sizeItem.setMax(0);
		sizeItem.setMax(100);
		sizeItem.setWidth(50);
		
		TextItem beginItem = new TextItem("begin", "Begin Date");
				
		TextItem endItem = new TextItem("end", "End Date");
		
		form3.setItems(idItem,dateItem,beginItem,endItem,languageItem,sizeItem,internalItem,restrictionItem);
		topFormLayout.addMember(form3);
	}
	
	public void editRecord(Record record) {
		genreItem.show();
		formItem.show();
		mediumItem.show();
		authorsItem.show();
		String type = record.getAttribute("localName");
		if(record.getAttribute("restrictions") != null) 
			record.setAttribute("restrictions", Boolean.valueOf(record.getAttribute("restrictions")));
		if(record.getAttribute("internal") != null) 
			record.setAttribute("internal", Boolean.valueOf(record.getAttribute("internal")));
		if(type != null) {
			if(type.equals("artwork")) {
				//aspectArray['artwork']=new Array('artwork_form','artwork_genre','artwork_media','artwork_size','authors');
				genreItem.setValueMap(contentTypes.getArtworkGenre());
				mediumItem.setValueMap(contentTypes.getArtworkMedium());
				formItem.setValueMap(contentTypes.getArtworkForms());
			} else if(type.equals("audio")) {
				//aspectArray['audio']=new Array('audio_medium','authors');
				genreItem.hide();
				mediumItem.setValueMap(contentTypes.getAudioMedium());
				formItem.hide();
			} else if(type.equals("correspondence")) {
				//aspectArray['correspondence']=new Array('correspondence_genre','correspondence_form','authors');
				genreItem.setValueMap(contentTypes.getCorrespondenceGenre());
				formItem.setValueMap(contentTypes.getCorrespondenceForms());
				mediumItem.hide();
			} else if(type.equals("financial")) {
				//aspectArray['financial']=new Array('financial_genre','authors');
				genreItem.setValueMap(contentTypes.getFinancialGenre());
				formItem.hide();
				mediumItem.hide();
			} else if(type.equals("journals")) {
				//aspectArray['journals']=new Array('journals_genre','journals_form','authors');
				genreItem.setValueMap(contentTypes.getJournalsGenre());
				formItem.setValueMap(contentTypes.getJournalsForms());
				mediumItem.hide();
			} else if(type.equals("legal")) {
				//aspectArray['legal']=new Array('legal_genre','authors');
				genreItem.setValueMap(contentTypes.getLegalGenre());
				formItem.hide();
				mediumItem.hide();
			} else if(type.equals("manuscript")) {
				//aspectArray['manuscript']=new Array('manuscript_genre','manuscript_form','authors');
				genreItem.setValueMap(contentTypes.getManuscriptGenre());
				formItem.setValueMap(contentTypes.getManuscriptForms());
				mediumItem.hide();
			} else if(type.equals("medical")) {
				//aspectArray['medical']=new Array('medical_genre','authors');
				genreItem.setValueMap(contentTypes.getMedicalGenre());
				formItem.hide();
				mediumItem.hide();
			} else if(type.equals("memorabilia")) {
				//aspectArray['memorabilia']=new Array('memorabilia_genre','authors');
				genreItem.setValueMap(contentTypes.getMemorabiliaGenre());
				formItem.hide();
				mediumItem.hide();
			} else if(type.equals("miscellaneous")) {
				//aspectArray['miscellaneous']=new Array();
				genreItem.hide();
				formItem.hide();
				mediumItem.hide();
			} else if(type.equals("notebooks")) {
				//aspectArray['notebooks']=new Array('notebooks_form','notebooks_genre','authors');
				genreItem.setValueMap(contentTypes.getNotebooksGenre());
				formItem.setValueMap(contentTypes.getNotebooksForms());
				mediumItem.hide();
			} else if(type.equals("photographs")) {
				//aspectArray['photographs']=new Array('photograph_form','photograph_size','authors');
				genreItem.hide();
				formItem.setValueMap(contentTypes.getPhotographsForms());
				mediumItem.hide();
			} else if(type.equals("printed_material")) {
				//aspectArray['printed_material']=new Array('printed_genre','authors');
				genreItem.setValueMap(contentTypes.getPrintedGenre());
				formItem.hide();
				mediumItem.hide();
			} else if(type.equals("professional")) {
				//aspectArray['professional']=new Array('professional_genre','authors');
				genreItem.setValueMap(contentTypes.getProfessionalGenre());
				formItem.hide();
				mediumItem.hide();
			} else if(type.equals("research")) {
				//aspectArray['research']=new Array('research_genre','authors');
				genreItem.setValueMap(contentTypes.getResearchGenre());
				formItem.hide();
				mediumItem.hide();
			} else if(type.equals("scrapbooks")) {
				//aspectArray['scrapbooks']=new Array('scrapbooks_form','authors');
				genreItem.hide();
				formItem.setValueMap(contentTypes.getScrapbooksForms());
				mediumItem.hide();
			} else if(type.equals("video")) {
				//aspectArray['video']=new Array('video_medium','video_form','authors');
				genreItem.hide();
				formItem.setValueMap(contentTypes.getVideoForms());
				mediumItem.setValueMap(contentTypes.getVideoMedium());
			}
		}
		vm.editRecord(record);
	}
	public Record getValuesAsRecord() {
		Record record = new Record();
		record.setAttribute("id", vm.getValue("id"));
		for(Object key : vm.getValues().keySet()) {
			Object val = vm.getValues().get(key);
			if(val != null && !val.equals("")) record.setAttribute(String.valueOf(key), val);
			else record.setAttribute(String.valueOf(key), "");
		}
		return record;
	}
	public void fetchData(Criteria criteria, DSCallback callback) {
		vm.fetchData(criteria, callback);
	}
	public void saveData(DSCallback callback) {
		vm.saveData(callback);
	}
}
