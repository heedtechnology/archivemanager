package org.archivemanager.repository.client.data;

import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.RestDataSource;

public class LocationDS extends RestDataSource {
	private static LocationDS instance = null;  
	  
    public static LocationDS getInstance() {  
        if (instance == null) {  
            instance = new LocationDS("locationDS");  
        }  
        return instance;  
    }
    
	public LocationDS(String id) {
		setID(id);
		
		
		//setAddDataURL("/repository/accession/add.xml");
		setFetchDataURL("/service/entity/get.xml");
		setRemoveDataURL("/service/entity/remove.xml");
		setUpdateDataURL("/service/entity/update.xml");
	}
	
	@Override
	protected void transformResponse(DSResponse response, DSRequest request,Object data) {
		// TODO Auto-generated method stub
		super.transformResponse(response, request, data);
	}
}
