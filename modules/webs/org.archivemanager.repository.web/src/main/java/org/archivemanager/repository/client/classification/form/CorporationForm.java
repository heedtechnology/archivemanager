package org.archivemanager.repository.client.classification.form;

import org.archivemanager.repository.client.data.ClassificationDS;
import org.heed.openapps.gwt.client.data.NativeClassificationModel;

import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;

public class CorporationForm extends DynamicForm {
	private NativeClassificationModel model = new NativeClassificationModel();
	
	private static final int fieldWidth = 300;
	
	public CorporationForm() {
		setWidth100();
		setMargin(5);
		setCellPadding(5);
		setNumCols(4);
		setColWidths("100","*","100","*");
		//setBorder("1px solid #C0C3C7");
		setDataSource(ClassificationDS.getInstance());
		
		TextItem nameItem = new TextItem("name", "Name");
		nameItem.setWidth(fieldWidth);		
		
		TextItem primaryNameItem = new TextItem("primary_name", "Primary Name");
		primaryNameItem.setWidth(fieldWidth);
		
		TextItem urlItem = new TextItem("secondary_name", "Secondary Name");
		urlItem.setWidth(fieldWidth);
		
		TextItem fullerItem = new TextItem("fuller_form_name", "Fuller Form");
		fullerItem.setWidth(fieldWidth);
		
		TextItem prefixItem = new TextItem("prefix", "Prefix");
		prefixItem.setWidth(fieldWidth);
		
		TextItem suffixItem = new TextItem("suffix", "Suffix");
		suffixItem.setWidth(fieldWidth);
		
		TextItem datesItem = new TextItem("dates", "Dates");
		datesItem.setWidth(fieldWidth);
		
		SelectItem sourceItem = new SelectItem("source", "Source");
		sourceItem.setValueMap(model.getSubjectSource());
		sourceItem.setWidth(fieldWidth);
		
		SelectItem typeItem = new SelectItem("note_type", "Type");
		typeItem.setValueMap(model.getNamedEntityType());
		typeItem.setWidth(fieldWidth/2);
		
		SelectItem ruleItem = new SelectItem("rule", "Rule");
		ruleItem.setValueMap(model.getNamedEntityRule());
		ruleItem.setWidth(fieldWidth-50);
		
		TextAreaItem descItem = new TextAreaItem("description", "Description");
		descItem.setColSpan(4);
		descItem.setWidth(500);
		
		TextAreaItem citationItem = new TextAreaItem("citation", "Citation");
		citationItem.setColSpan(4);
		citationItem.setWidth(500);
		
		TextAreaItem noteItem = new TextAreaItem("note", "Note");
		noteItem.setColSpan(4);
		noteItem.setWidth(500);
		
		setFields(nameItem,primaryNameItem,urlItem,fullerItem,prefixItem,sourceItem,
				suffixItem,ruleItem,datesItem,typeItem,descItem,citationItem,noteItem);
	}
}
