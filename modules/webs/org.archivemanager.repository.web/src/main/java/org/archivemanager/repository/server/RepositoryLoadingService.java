package org.archivemanager.repository.server;

import org.archivemanager.repository.data.AccessionExportProcessor;
import org.archivemanager.repository.data.DefaultTextCollectionImportProcessor;
import org.archivemanager.repository.data.FilemakerXMLAccessionImportProcessor;
import org.archivemanager.repository.data.ExcelLocationImportProcessor;
import org.archivemanager.repository.data.LocationPersistenceListener;
import org.archivemanager.repository.data.RepositoryEntityIndexer;
import org.heed.openapps.RepositoryModel;
import org.heed.openapps.entity.EntityService;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;


public class RepositoryLoadingService implements ApplicationContextAware {
	private EntityService entityService;
	
	
	@Override
	public void setApplicationContext(ApplicationContext ctx) throws BeansException {
		entityService = (EntityService)ctx.getBean(EntityService.class);
		RepositoryEntityIndexer indexer = new RepositoryEntityIndexer();
		indexer.setApplicationContext(ctx);
		entityService.registerEntityIndexer("{openapps.org_repository_1.0}item", indexer);
		AccessionExportProcessor exporter = new AccessionExportProcessor();
		exporter.setApplicationContext(ctx);
		entityService.registerExportProcessor(RepositoryModel.ACCESSION.toString(), exporter);
		
		FilemakerXMLAccessionImportProcessor accessionImporter2 = new FilemakerXMLAccessionImportProcessor("Accession Filemaker Importer", entityService);
		entityService.registerImportProcessor(RepositoryModel.ACCESSION.toString(), accessionImporter2);
		entityService.registerImportProcessor(RepositoryModel.LOCATION.toString(), new ExcelLocationImportProcessor("Excel Location Importer", entityService));
		entityService.registerImportProcessor(RepositoryModel.COLLECTION.toString(), new DefaultTextCollectionImportProcessor("Default Text"));
	
		entityService.registerEntityPersistenceListener(RepositoryModel.LOCATION.toString(), new LocationPersistenceListener());
	}
	
}
