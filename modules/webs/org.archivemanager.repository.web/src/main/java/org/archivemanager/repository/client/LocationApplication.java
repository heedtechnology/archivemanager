package org.archivemanager.repository.client;

import org.archivemanager.repository.client.data.LocationTreeDS;
import org.archivemanager.repository.client.location.LocationPanel;
import org.heed.openapps.gwt.client.Application;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;

public class LocationApplication extends HLayout implements Application {
	private RepositoryManagerEntryPoint mgr;
	private ListGrid grid;
	private LocationPanel locationPanel;
	private Canvas canvas;
	
	private AddLocationWindow addWindow;
	
	public LocationApplication(RepositoryManagerEntryPoint mgr) {
		this.mgr = mgr;
		setWidth100();
		setHeight100();
		setMembersMargin(5);
		
		grid = new ListGrid();
		grid.setWidth(300);
		grid.setHeight("100%");
		grid.setBorder("0px");
		grid.setShowHeader(false);
		grid.setShowResizeBar(true);
		grid.setSortField("name");
		ListGridField nameField = new ListGridField("name","Name");
		grid.setDataSource(LocationTreeDS.getInstance());
		grid.setFields(nameField);
		grid.addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				select(event.getRecord());
			}
		});
        addMember(grid);
        /*
		grid = new TreeGrid();
		grid.setDataSource(LocationTreeDS.getInstance());
		grid.setWidth(300);
		grid.setHeight("100%");
		grid.setBorder("0px");
		grid.setShowHeader(false);
		grid.setShowResizeBar(true);
		grid.addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				select(event.getRecord());
			}
		});
        addMember(grid);
		*/
		canvas = new Canvas();
        canvas.setWidth100();
        canvas.setHeight100();
		addMember(canvas);
        
        addWindow = new AddLocationWindow();
        
	}
	
	@Override
	public void select(Record record) {
		String type = record.getAttribute("localName");
		if(type.equals("location")) {
			if(locationPanel == null) {
				locationPanel = new LocationPanel();
				canvas.addChild(locationPanel);
			}
			Criteria criteria = new Criteria();
			criteria.setAttribute("id", record.getAttribute("id"));
			locationPanel.fetchData(criteria, new DSCallback() {
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					if(response.getData() != null && response.getData().length > 0) {
						Record accession = response.getData()[0];
						if(accession != null) {
							String date = accession.getAttribute("name");
							String title = grid.getSelectedRecord().getAttribute("name");
							if(title != null && !title.equals(date)) {
								grid.getSelectedRecord().setAttribute("name", date);
								grid.refreshFields();
							}
							locationPanel.select(accession);
						}
					}
				}
			});
			locationPanel.select(record);
			locationPanel.show();
			mgr.getToolbar().showAddButton(true);
			mgr.getToolbar().showDeleteButton(true);
			mgr.getToolbar().showSaveButton(true);
		} else {
			if(locationPanel != null) locationPanel.hide();
			mgr.getToolbar().showAddButton(true);
			mgr.getToolbar().showDeleteButton(false);
			mgr.getToolbar().showSaveButton(false);
		}
	}
	@Override
	public void save() {
		locationPanel.save(new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				if(response.getData() != null && response.getData().length > 0) {
					Record accession = response.getData()[0];
					if(accession != null) {
						String date = accession.getAttribute("name");
						String title = grid.getSelectedRecord().getAttribute("name");
						if(title != null && !title.equals(date)) {
							grid.getSelectedRecord().setAttribute("name", date);
							grid.refreshFields();
						}
					}
				}
			}
		});		
	}
	public void importEntity() {
		
	}
	public void exportEntity() {
		
	}
	@Override
	public void add() {
		addWindow.show();
	}
	@Override
	public void delete() {
		SC.confirm("Do you really wish to remove this permanently?", new BooleanCallback() {
			public void execute(Boolean value) {
				if(value) grid.removeSelectedData();
			}			
		});	
	}
	@Override
	public void search(String query) {
		Criteria criteria = new Criteria();
		criteria.setAttribute("qname", "{openapps.org_repository_1.0}location");
		criteria.setAttribute("query", query);
		criteria.setAttribute("field", "name");
		criteria.setAttribute("sort", "name_e");
		grid.fetchData(criteria);
	}

	public class AddLocationWindow extends Window {
		public AddLocationWindow() {
			setWidth(300);
			setHeight(180);
			setTitle("Add Location");
			setAutoCenter(true);
			setIsModal(true);
			DynamicForm addForm = new DynamicForm();
			addForm.setMargin(5);
			addForm.setCellPadding(3);
			addForm.setNumCols(2);
			final TextItem buildingItem = new TextItem("building", "Building");
			buildingItem.setWidth("*");
			
			final TextItem floorItem = new TextItem("floor", "Floor");
			floorItem.setWidth("*");
			
			final TextItem aisleItem = new TextItem("aisle", "Aisle");
			aisleItem.setWidth("*");
			
			final TextItem bayItem = new TextItem("bay", "Bay");
			bayItem.setWidth("*");
			
			ButtonItem submitItem = new ButtonItem("submit", "Add");
			submitItem.setIcon("/theme/images/icons16/add.png");
			submitItem.setStartRow(false);
			submitItem.setEndRow(false);
			submitItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					Record record = new Record();
					record.setAttribute("qname", "{openapps.org_repository_1.0}location");
					record.setAttribute("building", buildingItem.getValue());
					record.setAttribute("floor", floorItem.getValue());
					record.setAttribute("aisle", aisleItem.getValue());
					record.setAttribute("bay", bayItem.getValue());
					grid.addData(record, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							addWindow.hide();
						}
					});
				}
			});
			
			addForm.setFields(buildingItem,floorItem, aisleItem, bayItem, submitItem);
			addItem(addForm);
		}
	}

}