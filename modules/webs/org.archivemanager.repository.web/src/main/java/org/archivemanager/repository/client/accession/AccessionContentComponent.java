package org.archivemanager.repository.client.accession;

import java.util.LinkedHashMap;

import org.heed.openapps.gwt.client.data.RestUtility;

import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;

import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.SpinnerItem;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;


public class AccessionContentComponent extends VLayout {
	private ListGrid grid;
	private AddAccessionContentWindow addWindow;
	
	private Record accession;
	private LinkedHashMap<String,String> extentTypeValueMap;
	
	public AccessionContentComponent(String width) {
		setWidth(width);
		extentTypeValueMap = new LinkedHashMap<String,String>();
		extentTypeValueMap.put("","");
		extentTypeValueMap.put("paige_box","Paige Box");
		extentTypeValueMap.put("manuscript_box","Manuscript Box");
		extentTypeValueMap.put("oversized_box","Oversized Box");
		extentTypeValueMap.put("package","Package");
		extentTypeValueMap.put("film_canister","Film Canister");
				
		HLayout toolstrip = new HLayout();
		toolstrip.setWidth(width);
		toolstrip.setHeight(20);
		toolstrip.setBackgroundImage("[SKIN]/cssButton/button_stretch.png");
		toolstrip.setBorder("1px solid #A7ABB4");
		addMember(toolstrip);
		
		Label label = new Label("<label style='font-size:11px;font-weight:bold;color:#4C4C4C;font-family:Arial,Verdana,sans-serif;'>Contents</label>");
		label.setHeight("100%");
		label.setWidth("100%");
		toolstrip.addMember(label);
		
		HLayout buttons = new HLayout();
		buttons.setHeight(16);
		buttons.setMargin(2);
		buttons.setMembersMargin(3);
		toolstrip.addMember(buttons);
		
		ImgButton addButton = new ImgButton();
		addButton.setWidth(15);
		addButton.setHeight(15);
		addButton.setSrc("/theme/images/icons16/add.png");
		addButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				addWindow.show();
			}
		});
		buttons.addMember(addButton);
		ImgButton delButton = new ImgButton();
		delButton.setWidth(15);
		delButton.setHeight(15);
		delButton.setSrc("/theme/images/icons16/delete.png");
		delButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Record record = grid.getSelectedRecord();
				if(record != null) {
					Record rec = new Record();
					if(record.getAttribute("target_id") != null) rec.setAttribute("id", record.getAttribute("target_id"));
					else rec.setAttribute("id", record.getAttribute("id"));
					RestUtility.post("/entity/service/remove.xml", rec, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							grid.removeSelectedData();	
						}						
					});					 
				}
			}
		});
		buttons.addMember(delButton);
		
		grid = new ListGrid();
		grid.setWidth(width);
		grid.setHeight(40);
		grid.setAutoFitData(Autofit.VERTICAL);
		grid.setAutoFitMaxHeight(200);
		grid.setShowHeader(false);
		ListGridField value = new ListGridField("value", "Value", 30);
		value.setAlign(Alignment.CENTER);
		ListGridField type = new ListGridField("type", "Type");
		type.setValueMap(extentTypeValueMap);
		grid.setFields(value,type);
		//grid.setDataSource(new AccessionContentDS());
		
		addWindow = new AddAccessionContentWindow();
		//addMember(addWindow);
		
		addMember(grid);
	}
	public void setData(Record[] records) {
		grid.setData(records);
	}
	public void select(Record record) {
		this.accession = record;
		try {
			Record source_associations = record.getAttributeAsRecord("source_associations");
			if(source_associations != null) {
				Record notes = source_associations.getAttributeAsRecord("extents");
				if(notes != null) {
					setData(notes.getAttributeAsRecordArray("node"));
				} else setData(new Record[0]);
			}
		} catch(ClassCastException e) {
			setData(new Record[0]);
		}
		try {
			Record target_associations = record.getAttributeAsRecord("target_associations");
			if(target_associations != null) {
				
			}
		} catch(ClassCastException e) {
			
		}
	}
	public class AddAccessionContentWindow extends Window {
		public AddAccessionContentWindow() {
			setWidth(250);
			setHeight(125);
			setTitle("Add Contents");
			setAutoCenter(true);
			setIsModal(true);
			DynamicForm addForm = new DynamicForm();
			addForm.setCellPadding(5);
			final SpinnerItem valueItem = new SpinnerItem("value", "Value");
			valueItem.setWidth(75);
			valueItem.setMin(0);
			valueItem.setMax(1000);
			valueItem.setStep(1);
			final SelectItem typeItem = new SelectItem("type", "Unit of Measure");
			typeItem.setValueMap(extentTypeValueMap);
			typeItem.setWidth("*");
			
			ButtonItem submitItem = new ButtonItem("submit", "Add");
			submitItem.setIcon("/theme/images/icons16/add.png");
			submitItem.setStartRow(false);
			submitItem.setEndRow(false);
			submitItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					Record record = new Record();
					record.setAttribute("assoc_qname", "{openapps.org_repository_1.0}extents");
					record.setAttribute("entity_qname", "{openapps.org_repository_1.0}extent");
					record.setAttribute("source", accession.getAttribute("id"));
					record.setAttribute("value", valueItem.getValueAsString());
					record.setAttribute("type", typeItem.getValue());
					RestUtility.post("/entity/service/associate.xml",record,new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							if(response.getData() != null && response.getData().length > 0) {
								grid.addData(response.getData()[0]);
								addWindow.hide();
							}
						}						
					});
				}			
			});
			
			addForm.setFields(valueItem,typeItem,submitItem);
			addItem(addForm);
		}
	}
	
}
