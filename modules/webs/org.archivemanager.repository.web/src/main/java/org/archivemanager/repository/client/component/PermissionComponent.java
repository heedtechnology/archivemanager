package org.archivemanager.repository.client.component;

import org.heed.openapps.gwt.client.data.RestUtility;

import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class PermissionComponent extends VLayout {
	private ListGrid grid;
	
	public PermissionComponent(String width) {
		setWidth(width);
		
		HLayout toolstrip = new HLayout();
		toolstrip.setWidth(width);
		toolstrip.setHeight(20);
		toolstrip.setBackgroundImage("[SKIN]/cssButton/button_stretch.png");
		toolstrip.setBorder("1px solid #A7ABB4");
		addMember(toolstrip);
		
		Label label = new Label("<label style='font-size:11px;font-weight:bold;color:#4C4C4C;font-family:Arial,Verdana,sans-serif;'>Permissions</label>");
		label.setHeight("100%");
		label.setWidth("100%");
		toolstrip.addMember(label);
		
		HLayout buttons = new HLayout();
		buttons.setHeight(16);
		buttons.setMargin(2);
		buttons.setMembersMargin(3);
		toolstrip.addMember(buttons);
		
		ImgButton addButton = new ImgButton();
		addButton.setWidth(15);
		addButton.setHeight(15);
		addButton.setSrc("/theme/images/icons16/add.png");
		addButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				//addWindow.show();
			}
		});
		buttons.addMember(addButton);
		ImgButton delButton = new ImgButton();
		delButton.setWidth(15);
		delButton.setHeight(15);
		delButton.setSrc("/theme/images/icons16/delete.png");
		delButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Record record = grid.getSelectedRecord();
				if(record != null) {
					Record rec = new Record();
					if(record.getAttribute("target_id") != null) rec.setAttribute("id", record.getAttribute("target_id"));
					else rec.setAttribute("id", record.getAttribute("id"));
					RestUtility.post("/service/entity/remove.xml", rec, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							grid.removeSelectedData();	
						}						
					});					 
				}
			}
		});
		buttons.addMember(delButton);
		
		grid = new ListGrid();
		grid.setWidth(width);
		grid.setHeight(40);
		grid.setAutoFitData(Autofit.VERTICAL);
		grid.setAutoFitMaxHeight(200);
		grid.setShowHeader(false);
		
		//addWindow = new AddAccessionContentWindow();
		//addMember(addWindow);
		
		addMember(grid);
	}
}
