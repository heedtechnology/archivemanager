package org.archivemanager.repository.client.data;

import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;

public class ImportTreeDS extends RestDataSource {
	private static ImportTreeDS instance = null;  
	  
    public static ImportTreeDS getInstance() {  
        if (instance == null) {  
            instance = new ImportTreeDS("importTreeDS");  
        }  
        return instance;  
    }
    
	public ImportTreeDS(String id) {
		setID(id);  
        setTitleField("Name");	        
        DataSourceTextField nameField = new DataSourceTextField("title", "Title");     
        
        DataSourceTextField idField = new DataSourceTextField("id", "ID",50);  
        idField.setPrimaryKey(true);  
        idField.setRequired(true);
        
        DataSourceTextField parentField = new DataSourceTextField("parent", "Parent");  
        parentField.setRequired(true);  
        parentField.setForeignKey(id + ".id");  
        parentField.setRootValue("null");  
        
        setFields(idField,nameField,parentField);
        
        setFetchDataURL("/service/archivemanager/repository/collection/import/fetch.xml");
        setAddDataURL("/service/archivemanager/repository/collection/import/add.xml"); 
        //setRemoveDataURL("/core/entity/remove.xml");
        //setUpdateDataURL("/repository/accession/update.xml");
	}
}