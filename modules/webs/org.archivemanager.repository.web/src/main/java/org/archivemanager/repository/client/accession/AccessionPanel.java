package org.archivemanager.repository.client.accession;
import java.util.LinkedHashMap;

import org.archivemanager.gwt.client.component.CollectionItemsComponent;
import org.archivemanager.repository.client.accession.form.AccessionForm;
import org.heed.openapps.gwt.client.component.ActivitiesComponent;
import org.heed.openapps.gwt.client.component.NotesComponent;
import org.heed.openapps.gwt.client.component.TasksComponent;
import org.heed.openapps.gwt.client.data.NativeRepositoryModel;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;


public class AccessionPanel extends TabSet {
	private NativeRepositoryModel repositoryModel = new NativeRepositoryModel();
	private AccessionForm form;
	private AccessionDonorComponent donorComponent;
	private CollectionItemsComponent collectionItemsComponent;
	//private RolesComponent rolesComponent;
	private NotesComponent notesComponent;
	private ActivitiesComponent activitiesComponent;
	private TasksComponent tasksComponent;
	
	public AccessionPanel() {
		setWidth100();
		setHeight100();
		
		VLayout mainLayout = new VLayout();
		mainLayout.setMembersMargin(5);
		Tab collectionTab = new Tab("Accession");
		collectionTab.setPane(mainLayout);
		addTab(collectionTab);
		
		//HLayout topCanvas = new HLayout();
		//topCanvas.setBorder("1px solid #C0C3C7");
		form = new AccessionForm();
		mainLayout.addMember(form);
		
		HLayout subLayout1 = new HLayout();
		subLayout1.setWidth100();
		mainLayout.addMember(subLayout1);
				
		notesComponent = new NotesComponent("100%", true, repositoryModel.getCollectionNoteTypes());
		notesComponent.setMargin(5);
		subLayout1.addMember(notesComponent);
		
		HLayout layout1 = new HLayout();
		layout1.setWidth100();
		//layout1.setBorder("1px solid #C0C3C7");
		//setMembersMargin(10);
		mainLayout.addMember(layout1);
		
		//left column
		VLayout layout2 = new VLayout();
		layout2.setWidth("50%");
		layout1.addMember(layout2);
				
		collectionItemsComponent = new CollectionItemsComponent("100%");
		collectionItemsComponent.setMargin(5);
		layout2.addMember(collectionItemsComponent);
		
		LinkedHashMap<String,String> valueMap4 = new LinkedHashMap<String,String>();
		valueMap4.put("acknowledgement_sent","Acknowledgement Sent");
		valueMap4.put("arranged_sorted","Arranged and Sorted");
		valueMap4.put("listed","Listed");
		valueMap4.put("processing_beginning","Processing Beginning");
		valueMap4.put("processing_ending","Processing Ending");
		valueMap4.put("rights_transferred","Rights Transferred");
		valueMap4.put("shelved","Shelved");
		activitiesComponent = new ActivitiesComponent("100%", valueMap4);
		activitiesComponent.setMargin(5);		
		layout2.addMember(activitiesComponent);
		
		//right column
		VLayout layout3 = new VLayout();
		layout3.setWidth("50%");
		layout1.addMember(layout3);
				
		donorComponent = new AccessionDonorComponent("100%");
		donorComponent.setMargin(5);
		layout3.addMember(donorComponent);
							
		tasksComponent = new TasksComponent("100%", repositoryModel.getCollectionTaskTypes());
		tasksComponent.setMargin(5);
		layout3.addMember(tasksComponent);
		
		Canvas canvas = new Canvas();
		canvas.setHeight100();
		mainLayout.addMember(canvas);
	}
	public void fetchData(Criteria criteria, DSCallback callback) {
		form.fetchData(criteria, callback);
	}
	public void select(Record accession) {
		donorComponent.select(accession);
		collectionItemsComponent.select(accession);
		notesComponent.select(accession);
		tasksComponent.select(accession);
		activitiesComponent.select(accession);
		form.select(accession);
		if(accession != null) {
			try {
				Record target_associations = accession.getAttributeAsRecord("target_associations");
				if(target_associations != null) {
					Record accessions = target_associations.getAttributeAsRecord("accessions");
					if(accessions != null) {
						Record collection = accessions.getAttributeAsRecord("node");
						if(collection != null) {
							String title = collection.getAttribute("title");
							form.setValue("collection", title);
						}
					}
				}
			} catch(ClassCastException e) {
				//contentComponent.setData(new Record[0]);
			}
		}
	}
	public void save(DSCallback callback) {
		form.saveData(callback);
	}
}
