package org.archivemanager.repository.client.data;

import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;

public class ClassificationDS extends RestDataSource {
	private static ClassificationDS instance = null;  
	  
    public static ClassificationDS getInstance() {  
        if (instance == null) {  
            instance = new ClassificationDS("classificationDS");  
        }  
        return instance;  
    }
    
	public ClassificationDS(String id) {
		setID(id);
				
		setAddDataURL("/service/archivemanager/repository/collection/add.xml");
		setFetchDataURL("/service/entity/get.xml");
		setRemoveDataURL("/service/entity/remove.xml");
		setUpdateDataURL("/service/entity/update.xml");
	}
	
	@Override
	protected void transformResponse(DSResponse response, DSRequest request,Object data) {
		// TODO Auto-generated method stub
		super.transformResponse(response, request, data);
	}
}