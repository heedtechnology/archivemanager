package org.archivemanager.repository.client.collection;

import org.archivemanager.gwt.client.component.ContentAssociationPanel;
import org.archivemanager.repository.client.collection.form.ItemForm;
import org.heed.openapps.gwt.client.component.ActivitiesComponent;
import org.heed.openapps.gwt.client.component.NamedEntityComponent;
import org.heed.openapps.gwt.client.component.NotesComponent;
import org.heed.openapps.gwt.client.component.SearchTermComponent;
import org.heed.openapps.gwt.client.component.SubjectComponent;
import org.heed.openapps.gwt.client.component.TasksComponent;
import org.heed.openapps.gwt.client.data.NativeClassificationModel;
import org.heed.openapps.gwt.client.data.NativeRepositoryModel;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;

public class ItemPanel extends TabSet {
	private NativeClassificationModel model = new NativeClassificationModel();
	private NativeRepositoryModel repositoryModel = new NativeRepositoryModel();
	
	private ItemForm form;
	
	private NotesComponent notesComponent;
	private ActivitiesComponent activitiesComponent;
	private NamedEntityComponent nameComponent;
	private SubjectComponent subjectComponent;
	private ContentAssociationPanel contentPanel;
	private TasksComponent tasksComponent;
	private SearchTermComponent searchTerms;
	
	public ItemPanel() {
		setWidth100();
		setHeight100();
		
		VLayout mainLayout = new VLayout();
		Tab collectionTab = new Tab("Item");
		collectionTab.setPane(mainLayout);
		addTab(collectionTab);
				
		form = new ItemForm();
		mainLayout.addMember(form);
		
		notesComponent = new NotesComponent("100%", true, repositoryModel.getCollectionNoteTypes());
		notesComponent.setMargin(5);
		mainLayout.addMember(notesComponent);
				
		HLayout layout1 = new HLayout();
		layout1.setWidth100();
		//layout1.setBorder("1px solid #C0C3C7");
		mainLayout.setMembersMargin(10);
		mainLayout.addMember(layout1);
		
		//left column
		VLayout layout2 = new VLayout();
		layout2.setWidth("50%");
		layout1.addMember(layout2);
			
		subjectComponent = new SubjectComponent("100%", true);
		subjectComponent.setMargin(5);
		layout2.addMember(subjectComponent);
		
		activitiesComponent = new ActivitiesComponent("100%", repositoryModel.getCollectionActivityTypes());
		activitiesComponent.setMargin(5);		
		layout2.addMember(activitiesComponent);	
		
		searchTerms = new SearchTermComponent("100%");
		searchTerms.setMargin(5);
		layout2.addMember(searchTerms);
		
		//right column
		VLayout layout3 = new VLayout();
		layout3.setWidth("50%");
		layout1.addMember(layout3);
		
		nameComponent = new NamedEntityComponent("100%", true, true, true, model.getNamedEntityFunction(), model.getNamedEntityRole());
		nameComponent.setMargin(5);
		layout3.addMember(nameComponent);
		
		tasksComponent = new TasksComponent("100%", repositoryModel.getCollectionTaskTypes());
		tasksComponent.setMargin(5);		
		layout3.addMember(tasksComponent);
		
		contentPanel = new ContentAssociationPanel();
		Tab contentTab = new Tab("Content");
		contentTab.setPane(contentPanel);
		addTab(contentTab);
		
		Canvas canvas = new Canvas();
		canvas.setHeight100();
		mainLayout.addMember(canvas);
	}
	public void fetchData(Criteria criteria, DSCallback callback) {
		form.fetchData(criteria, callback);
	}
	public void select(Record collection) {
		if(collection != null) {
			try {
				form.editRecord(collection);
				notesComponent.select(collection);
				activitiesComponent.select(collection);
				tasksComponent.select(collection);
				nameComponent.select(collection);
				subjectComponent.select(collection);
				contentPanel.select(collection);
				searchTerms.select(collection);				
			} catch(ClassCastException e) {
				
			}
		}
	}
	public void saveData(DSCallback callback) {
		form.saveData(callback);
	}
}
