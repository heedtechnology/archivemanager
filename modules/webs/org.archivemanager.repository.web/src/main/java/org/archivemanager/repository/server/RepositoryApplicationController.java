package org.archivemanager.repository.server;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.heed.openapps.QName;
import org.heed.openapps.RepositoryModel;
import org.heed.openapps.SystemModel;
import org.heed.openapps.dictionary.DataDictionaryService;
import org.heed.openapps.entity.BaseEntityQuery;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityQuery;
import org.heed.openapps.entity.EntityResultSet;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.entity.Property;
import org.heed.openapps.entity.ImportProcessor;
import org.heed.openapps.security.SecurityService;
import org.heed.openapps.theme.ThemeVariables;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class RepositoryApplicationController implements ApplicationContextAware {
	@Autowired private EntityService entityService;
	@Autowired private SecurityService securityService;
	@Autowired private DataDictionaryService dictionaryService;
	//private NodeIndexer indexer;
	
	
	public void setApplicationContext(ApplicationContext ctx) throws BeansException {
		entityService = (EntityService)ctx.getBean(EntityService.class);
		
	}
	
	@RequestMapping(value="/manager", method = RequestMethod.GET)
	public ModelAndView home(HttpServletRequest req, HttpServletResponse res) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		ThemeVariables vars = new ThemeVariables("smartclient");
		parms.put("theme", vars);
		//User user = securityService.currentUser();
		
		return new ModelAndView("home", parms);
	}
	/*
	@RequestMapping(value="/manager", method = RequestMethod.GET)
	public ModelAndView repositories(HttpServletRequest req, HttpServletResponse res) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		ThemeVariables vars = new ThemeVariables("default");
		parms.put("theme", vars);
		
		return new ModelAndView("repositories", parms);
	}
	*/
	@RequestMapping(value="/splash/{name}", method = RequestMethod.GET)
	public ModelAndView home(HttpServletRequest req, HttpServletResponse res, @PathVariable("name") String name) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		//ThemeVariables vars = new ThemeVariables("gwt-default");
		//parms.put("theme", vars);
		//User user = serviceManager.getSecurityService().currentUser();
		
		return new ModelAndView("splash/"+name, parms);
	}
	@RequestMapping(value="/accession/manager", method = RequestMethod.GET)
	public ModelAndView accessions(HttpServletRequest req, HttpServletResponse res) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		ThemeVariables vars = new ThemeVariables("default");
		parms.put("theme", vars);
		parms.put("repositories", getRepositories());
		parms.put("modes", getAccessionImportProcessors());
		return new ModelAndView("accessions", parms);
	}
	@RequestMapping(value="/collection/manager", method = RequestMethod.GET)
	public ModelAndView collections(HttpServletRequest req, HttpServletResponse res) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		ThemeVariables vars = new ThemeVariables("default");
		parms.put("theme", vars);
		//parms.put("", value)
		return new ModelAndView("home", parms);
	}
	@RequestMapping(value="/collection/import", method = RequestMethod.GET)
	public ModelAndView importCollection(HttpServletRequest req, HttpServletResponse res) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		ThemeVariables vars = new ThemeVariables("default");
		parms.put("theme", vars);
		parms.put("modes", getCollectionImportProcessors());
		parms.put("repositories", getRepositories());
		return new ModelAndView("import", parms);
	}
	@RequestMapping(value="/location/manager", method = RequestMethod.GET)
	public ModelAndView locations(HttpServletRequest req, HttpServletResponse res) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		ThemeVariables vars = new ThemeVariables("default");
		parms.put("theme", vars);
		parms.put("modes", getLocationImportProcessors());
		return new ModelAndView("locations", parms);
	}
			
	protected String getLocationImportProcessors() {
		//Map<String, Object> parms = new HashMap<String, Object>();
		StringBuffer buff = new StringBuffer("{'':'',");
		String key = RepositoryModel.LOCATION.toString();
		ImportProcessor processor = entityService.getImportProcessors(key).get(0);
		buff.append("'"+key+"':'"+processor.getName()+"',");		
		if(buff.length() > 2) return buff.substring(0, buff.length()-1).toString()+"}";
		else return "{}";
	}
	protected String getAccessionImportProcessors() {
		//Map<String, Object> parms = new HashMap<String, Object>();
		StringBuffer buff = new StringBuffer("{'':'',");
		String key = RepositoryModel.ACCESSION.toString();
		ImportProcessor processor = entityService.getImportProcessors(key).get(0);
		buff.append("'"+key+"':'"+processor.getName()+"',");
		
		if(buff.length() > 2) return buff.substring(0, buff.length()-1).toString()+"}";
		else return "{}";
	}
	protected String getCollectionImportProcessors() {
		//Map<String, Object> parms = new HashMap<String, Object>();
		StringBuffer buff = new StringBuffer("{'':'',");
		String key = RepositoryModel.COLLECTION.toString();
		ImportProcessor processor = entityService.getImportProcessors(key).get(0);
		buff.append("'"+key+"':'"+processor.getName()+"',");
		
		if(buff.length() > 2) return buff.substring(0, buff.length()-1).toString()+"}";
		else return "{}";
	}
	protected String getRepositories() throws Exception {
		StringBuffer buff = new StringBuffer("{");
		EntityQuery eQuery = new BaseEntityQuery(new QName("openapps.org_repository_1.0","repository"), null, "name", true);
		EntityResultSet results = entityService.search(eQuery);
		for(Entity e : results.getResults()) {
			Property repoName = e.getProperty(SystemModel.OPENAPPS_NAME);
			if(repoName != null) buff.append("'"+e.getId()+"':'"+repoName.toString()+"',");
		}
		if(buff.length() > 2) return buff.substring(0, buff.length()-1).toString()+"}";
		else return "{}";
	}
}
