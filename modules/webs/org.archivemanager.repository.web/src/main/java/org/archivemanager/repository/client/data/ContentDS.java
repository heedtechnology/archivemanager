package org.archivemanager.repository.client.data;

import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RestDataSource;

public class ContentDS extends RestDataSource {
	private static ContentDS instance = null;  
	  
    public static ContentDS getInstance() {  
        if (instance == null) {  
            instance = new ContentDS("contentDS");  
        }  
        return instance;  
    }
    
	public ContentDS(String id) {
		setID(id);
		
		setAddDataURL("/service/archivemanager/repository/accession/add.xml");
		setFetchDataURL("/service/entity/get.xml");
		setRemoveDataURL("/service/entity/remove.xml");
		setUpdateDataURL("/service/entity/update.xml");
	}
	
}
