package org.archivemanager.repository.client;

import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.OpenAppsEventHandler;
import org.heed.openapps.gwt.client.data.RestUtility;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.DOM;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.rpc.HandleErrorCallback;
import com.smartgwt.client.rpc.RPCManager;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.Positioning;
import com.smartgwt.client.types.Side;
import com.smartgwt.client.util.Page;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;


public class RepositoryManagerEntryPoint implements EntryPoint {
	private static final int height = Page.getHeight()-85;
	private static final int width = 1200;
	
	private NativeSystemSettings settings = new NativeSystemSettings();
	private RepositoryManagerToolbar toolbar;
	private TabSet tabs;
	
	private CollectionApplication collectionApplication;
	private ClassificationApplication classificationApplication;
	private LocationApplication locationApplication;
	private RepositoryApplication repositoryApplication;
	
	private int tab = -1;
	private Record selection;
	
	private String mode = "classification";
	
	public void onModuleLoad() {
		VLayout mainLayout = new VLayout();
		if(Page.getHeight() < 700) mainLayout.setHeight(700);
		else mainLayout.setHeight(height);  
		mainLayout.setWidth(width);
		mainLayout.setMaxHeight(560);
		mainLayout.setMargin(1);
		mainLayout.setOverflow(Overflow.HIDDEN);
        
        //LoadingScreen loader = new LoadingScreen("Loading....");
        //addMember(loader);
        
        toolbar = new RepositoryManagerToolbar(this);
        toolbar.setLayoutLeftMargin(32);
        toolbar.showSearchSupport("");
        mainLayout.addMember(toolbar);
        
        tabs = new TabSet();
        tabs.setTabBarThickness(35);
        tabs.setTabBarPosition(Side.LEFT);
        tabs.addClickHandler(new ClickHandler() {
        	public void onClick(ClickEvent event) {
        		if(tabs.getSelectedTabNumber() != tab) {
        			if(tabs.getSelectedTabNumber() == 1) toolbar.showSearchSupport("accession");
        			else if(tabs.getSelectedTabNumber() == 2) toolbar.showSearchSupport("classification");
        			else toolbar.showSearchSupport("clear");
        			if(tabs.getSelectedTabNumber() != 1) {
        				toolbar.showButtons(true, false, false);
        			} else toolbar.showButtons(false, false, false);
        			tab = tabs.getSelectedTabNumber();
        		}
			}
        });
        mainLayout.addMember(tabs);
        
        repositoryApplication = new RepositoryApplication(this);
        Tab repositoryTab = new Tab();
        repositoryTab.setPrompt("Repositories");
        repositoryTab.setIcon("/theme/images/icons32/database.png", 24);
        repositoryTab.setTitleStyle("sideTab");
        repositoryTab.setPane(repositoryApplication);
        tabs.addTab(repositoryTab);
        
        collectionApplication = new CollectionApplication(this);
        Tab collectionTab = new Tab();
        collectionTab.setPrompt("Collections");
        collectionTab.setIcon("/theme/images/icons/project-icon-emailarchive.gif", 24);
        collectionTab.setTitleStyle("sideTab");
        collectionTab.setPane(collectionApplication);
        tabs.addTab(collectionTab);
        
        classificationApplication = new ClassificationApplication(this);
        Tab classificationTab = new Tab();
        classificationTab.setPrompt("Classification");
        classificationTab.setIcon("/theme/images/icons32/large_tiles.png", 24);
        classificationTab.setPane(classificationApplication);
        tabs.addTab(classificationTab);
        
        locationApplication = new LocationApplication(this);
        Tab locationTab = new Tab();
        locationTab.setPrompt("Locations");
        locationTab.setIcon("/theme/images/icons32/drawer.png", 24);
        locationTab.setPane(locationApplication);
        tabs.addTab(locationTab);       
        
        //startup();
        
        EventBus.addHandler(org.heed.openapps.gwt.client.OpenAppsEvent.TYPE, new OpenAppsEventHandler()     {
	        @Override
	        public void onEvent(OpenAppsEvent event) {
	        	if(event.isType(RepositoryEvents.EXPORT_COLLECTION)) {
	        		RestUtility.get("/service/archivemanager/reporting/finding_aid/generate.xml", event.getRecord(), new DSCallback() {
	        			public void execute(DSResponse response, Object rawData, DSRequest request) {
	        				String message = response.getAttribute("message");
	        				String uid = response.getData().length > 0 ? response.getData()[0].getAttribute("uid") : null;
	        				toolbar.trackJobStatus(uid, message);
	        			}						
	        		});
	        	}
	        }
        });
        
        RPCManager.setHandleErrorCallback(new HandleErrorCallback() {
			@Override
			public void handleError(DSResponse response, DSRequest request) {
				int httpCode = response.getHttpResponseCode();
				if(httpCode == 404) SC.warn("Error contacting the server, please check your internet connection and try again.");
			}
		});
        
        mainLayout.setHtmlElement(DOM.getElementById("gwt"));
        mainLayout.setPosition(Positioning.RELATIVE);
        mainLayout.draw();
        //loader.draw();
        //closeLoader();
        //DOM.removeChild(RootPanel.getBodyElement(), DOM.getElementById("loader"));
        //DOM.setStyleAttribute(RootPanel.get("gwt").getElement(), "display", "block");
	}
	
	protected void startup() {
		if(settings.getApplicationName().equals("collection")) tabs.selectTab(1);
		else if(settings.getApplicationName().equals("classification")) tabs.selectTab(2);
		else if(settings.getApplicationName().equals("content")) tabs.selectTab(4);
		if(mode != null) {
			String query = "";;
			if(mode.equals("classification")) {
				query = "daly";
			}
			toolbar.setQuery(query);
			search(query);
		}
	}
	public void search(String query) {
		if(tabs.getSelectedTabNumber() == 0) repositoryApplication.search(query);
		else if(tabs.getSelectedTabNumber() == 1) collectionApplication.search(query);
		else if(tabs.getSelectedTabNumber() == 2) classificationApplication.search(query);
		else if(tabs.getSelectedTabNumber() == 3) locationApplication.search(query);
	}
	public void add() {
		if(tabs.getSelectedTabNumber() == 0) repositoryApplication.add();
		else if(tabs.getSelectedTabNumber() == 1) collectionApplication.add();
		else if(tabs.getSelectedTabNumber() == 2) classificationApplication.add();
		else if(tabs.getSelectedTabNumber() == 3) locationApplication.add();
	}
	public void delete() {
		if(tabs.getSelectedTabNumber() == 0) repositoryApplication.delete();
		else if(tabs.getSelectedTabNumber() == 1) collectionApplication.delete();
		else if(tabs.getSelectedTabNumber() == 2) classificationApplication.delete();
		else if(tabs.getSelectedTabNumber() == 3) locationApplication.delete();
	}
	public void save() {
		int tab = tabs.getSelectedTabNumber();
		if(tab == 0) repositoryApplication.save();
		else if(tab == 1) collectionApplication.save();
		else if(tab == 2) classificationApplication.save();
		else if(tab == 3) locationApplication.save();
	}
	public void importEntity() {
		int tab = tabs.getSelectedTabNumber();
		if(tab == 0) repositoryApplication.importEntity();
	}
	public void exportEntity() {
		int tab = tabs.getSelectedTabNumber();
		
	}
	public void crawl() {
		
	}
	public Record getSelection() {
		return selection;
	}
	public void select(Record record) {
		selection = record;
		String type = selection.getAttribute("localName");
		if(type.equals("accession") || type.equals("collection") || 
				type.equals("category") || type.equals("item") ||
				type.equals("categories") || type.equals("accessions")) 
			collectionApplication.select(record);
		else if(type.equals("repository")) repositoryApplication.select(record);
		else if(type.equals("location")) locationApplication.select(record);
		else if(type.equals("person") || type.equals("corporation")) classificationApplication.select(record);
		
	}
	public RepositoryManagerToolbar getToolbar() {
		return toolbar;
	}
	
	private final native void closeLoader() /*-{
    	return $wnd.closeLoader();
	}-*/;
}
