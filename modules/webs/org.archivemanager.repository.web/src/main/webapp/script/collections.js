var typeNames = {"series":"series","subseries":"subseries","group":"group","subgroup":"subgroup","file":"file","item":"item"};
var propertyNames = {"":"","level2":"Content Type","component_container":"Container"};
//var level2Values = {"correspondence":"Correspondence","manuscript":"Manuscript","printed_material":"Printed Material","professional":"Professional","research":"Research","legal":"Legal","financial":"Financial","audio":"Audio","video":"Video","photographs":"Photographs","artwork":"Artwork","journals":"Journal","notebooks":"Notebook","scrapbooks":"Scrapbook","medical":"Medical","memorabilia":"Memorabilia","miscellaneous":"Miscellaneous"};
var containerTypes = ["","Bin","Box","Package","Box/Folder","Carton","Cassette","Disk","Drawer","Envelope","Folder",
       					"Frame","Map/Case","Object","Oversize","Page","Reel","Reel/Frame","Volume"];
var aspectArray = new Array(18);
aspectArray['financial']=new Array('financial_genre','authors');
aspectArray['photographs']=new Array('photograph_form','photograph_size','authors');
aspectArray['miscellaneous']=new Array();
aspectArray['legal']=new Array('legal_genre','authors');
aspectArray['correspondence']=new Array('correspondence_genre','correspondence_form','authors');
aspectArray['research']=new Array('research_genre','authors');
aspectArray['type']=new Array('authors');
aspectArray['audio']=new Array('audio_medium','authors');
aspectArray['journals']=new Array('journals_genre','journals_form','authors');
aspectArray['professional']=new Array('professional_genre','authors');
aspectArray['scrapbooks']=new Array('scrapbooks_form','authors');
aspectArray['notebooks']=new Array('notebooks_form','notebooks_genre','authors');
aspectArray['printed_material']=new Array('printed_genre','authors');
aspectArray['artwork']=new Array('artwork_form','artwork_genre','artwork_media','artwork_size','authors');
aspectArray['manuscript']=new Array('manuscript_genre','manuscript_form','authors');
aspectArray['memorabilia']=new Array('memorabilia_genre','authors');
aspectArray['medical']=new Array('medical_genre','authors');
aspectArray['video']=new Array('video_medium','video_form','authors');
var itemTypes = {'{openapps.org_system_1.0}item':'','{openapps.org_repository_1.0}artwork':'Artwork','{openapps.org_repository_1.0}audio':'Audio','{openapps.org_repository_1.0}correspondence':'Correspondence','{openapps.org_repository_1.0}financial':'Financial','{openapps.org_repository_1.0}journals':'Journals','{openapps.org_repository_1.0}legal':'Legal','{openapps.org_repository_1.0}manuscript':'Manuscript','{openapps.org_repository_1.0}medical':'Medical','{openapps.org_repository_1.0}memorabilia':'Memorabilia','{openapps.org_repository_1.0}miscellaneous':'Miscellaneous','{openapps.org_repository_1.0}notebooks':'Notebooks','{openapps.org_repository_1.0}photographs':'Photographs','{openapps.org_repository_1.0}printed_material':'Printed Material','{openapps.org_repository_1.0}professional':'Professional','{openapps.org_repository_1.0}research':'Research','{openapps.org_repository_1.0}scrapbooks':'Scrapbooks','{openapps.org_repository_1.0}video':'Video'};
var namedEntityRoles = {'Actor':'Actor','Adapter':'Adapter','Animator':'Animator','Annotator':'Annotator','Applicant':'Applicant','Architect':'Architect','Arranger':'Arranger','Artist':'Artist','Assignee':'Assignee','Associated name':'Associated name','Attributed name':'Attributed name','Author':'Author','Author in quotations or text extracts':'Author in quotations or text extracts','Author of afterword, colophon, etc.':'Author of afterword, colophon, etc.','Author of dialog':'Author of dialog','Author of introduction':'Author of introduction','Author of screenplay, etc.':'Author of screenplay, etc.','Bibliographic antecedent':'Bibliographic antecedent','Binder':'Binder','Binding designer':'Binding designer','Book designer':'Book designer','Book producer':'Book producer','Bookjacket designer':'Bookjacket designer','Bookplate designer':'Bookplate designer','Calligrapher':'Calligrapher','Cartographer':'Cartographer','Censor':'Censor','Choreographer':'Choreographer','Cinematographer':'Cinematographer','Client':'Client','Collaborator':'Collaborator','Collotyper':'Collotyper','Commentator':'Commentator','Commentator for written text':'Commentator for written text','Compiler':'Compiler','Complainant':'Complainant','Complainant-appellant':'Complainant-appellant','Complainant-appellee':'Complainant-appellee','Composer':'Composer','Compositor':'Compositor','Conceptor':'Conceptor','Conductor':'Conductor','Consultant':'Consultant','Consultant to a project':'Consultant to a project','Contestant':'Contestant','Contestant-appellant':'Contestant-appellant','Contestant-appellee':'Contestant-appellee','Contestee':'Contestee','Contestee-appellant':'Contestee-appellant','Contestee-appellee':'Contestee-appellee','Contractor':'Contractor','Contributor':'Contributor','Copyright claimant':'Copyright claimant','Copyright holder':'Copyright holder','Corrector':'Corrector','Correspondent':'Correspondent','Costume designer':'Costume designer','Cover designer':'Cover designer','Creator':'Creator','Curator of an exhibition':'Curator of an exhibition','Dancer':'Dancer','Dedicatee':'Dedicatee','Dedicator':'Dedicator','Defendant':'Defendant','Defendant-appellant':'Defendant-appellant','Defendant-appellee':'Defendant-appellee','Degree grantor':'Degree grantor','Delineator':'Delineator','Depicted':'Depicted','Designer':'Designer','Director':'Director','Dissertant':'Dissertant','Distributor':'Distributor','Draftsman':'Draftsman','Dubious author':'Dubious author','Editor':'Editor','Electrotyper':'Electrotyper','Engineer':'Engineer','Engraver':'Engraver','Etcher':'Etcher','Expert':'Expert','Facsimilist':'Facsimilist','Film editor':'Film editor','First party':'First party','Forger':'Forger','Honoree':'Honoree','Host':'Host','Illuminator':'Illuminator','Illustrator':'Illustrator','Inscriber':'Inscriber','Instrumentalist':'Instrumentalist','Interviewee':'Interviewee','Interviewer':'Interviewer','Inventor':'Inventor','Landscape architect':'Landscape architect','Lender':'Lender','Libelant':'Libelant','Libelant-appellant':'Libelant-appellant','Libelant-appellee':'Libelant-appellee','Libelee':'Libelee','Libelee-appellant':'Libelee-appellant','Libelee-appellee':'Libelee-appellee','Librettist':'Librettist','Licensee':'Licensee','Licensor':'Licensor','Lighting designer':'Lighting designer','Lithographer':'Lithographer','Lyricist':'Lyricist','Manufacturer':'Manufacturer','Markup editor':'Markup editor','Metadata contact':'Metadata contact','Metal-engraver':'Metal-engraver','Moderator':'Moderator','Monitor':'Monitor','Musician':'Musician','Narrator':'Narrator','Opponent':'Opponent','Organizer of meeting':'Organizer of meeting','Originator':'Originator','Other':'Other','Papermaker':'Papermaker','Patent applicant':'Patent applicant','Patent holder':'Patent holder','Patron':'Patron','Performer':'Performer','Photographer':'Photographer','Plaintiff':'Plaintiff','Plaintiff-appellant':'Plaintiff-appellant','Plaintiff-appellee':'Plaintiff-appellee','Platemaker':'Platemaker','Printer':'Printer','Printer of plates':'Printer of plates','Printmaker':'Printmaker','Process contact':'Process contact','Producer':'Producer','Production personnel':'Production personnel','Programmer':'Programmer','Proofreader':'Proofreader','Publisher':'Publisher','Publishing director':'Publishing director','Puppeteer':'Puppeteer','Recipient':'Recipient','Recording engineer':'Recording engineer','Redactor':'Redactor','Renderer':'Renderer','Reporter':'Reporter','Research team head':'Research team head','Research team member':'Research team member','Researcher':'Researcher','Respondent':'Respondent','Respondent-appellant':'Respondent-appellant','Respondent-appellee':'Respondent-appellee','Responsible party':'Responsible party','Restager':'Restager','Reviewer':'Reviewer','Rubicator':'Rubicator','Scenarist':'Scenarist','Scientific advisor':'Scientific advisor','Scribe':'Scribe','Sculptor':'Sculptor','Second party':'Second party','Secretary':'Secretary','Set designer':'Set designer','Signer':'Signer','Singer':'Singer','Speaker':'Speaker','Sponsor':'Sponsor','Standards body':'Standards body','Stereotyper':'Stereotyper','Storyteller':'Storyteller','Surveyor':'Surveyor','Teacher':'Teacher','Thesis advisor':'Thesis advisor','Transcriber':'Transcriber','Translator':'Translator','Type designer':'Type designer','Typographer':'Typographer','Videographer':'Videographer','Vocalist':'Vocalist','Witness':'Witness','Wood-engraver':'Wood-engraver','Woodcutter':'Woodcutter','Writer of accompanying material':'Writer of accompanying material'};
var namedEntityFunctions = {'Creator':'Creator','Source':'Source','Subject':'Subject'};
var activityValueMap = {'Processing Beginning':'Processing Beginning','Processing Ending':'Processing Ending','Rights Transferred':'Rights Transferred'};
var noteValueMap = {'Abstract':'Abstract','Accruals note':'Accruals note','Appraisal note':'Appraisal note','Arrangement note':'Arrangement note','Bibliography':'Bibliography','Biographical/Historical note':'Biographical/Historical note','Chronology':'Chronology','Conditions Governing Access note':'Conditions Governing Access note','Conditions Governing Use note':'Conditions Governing Use note','Custodial History note':'Custodial History note','Dimensions note':'Dimensions note','Existence and Location of Copies note':'Existence and Location of Copies note','Existence and Location of Originals note':'Existence and Location of Originals note','File Plan note':'File Plan note','General note':'General note','General Physical Description note':'General Physical Description note','Immediate Source of Acquisition note':'Immediate Source of Acquisition note','Language of Materials note':'Language of Materials note','Legal Status note':'Legal Status note','Location note':'Location note','Material Specific Details note':'Material Specific Details note','Other Finding Aids note':'Other Finding Aids note','Physical Characteristics and Technical Requirements note':'Physical Characteristics and Technical Requirements note','Physical Facet note':'Physical Facet note','Preferred Citation note':'Preferred Citation note','Processing Information note':'Processing Information note','Related Archival Materials note':'Related Archival Materials note','Scope and Contents note':'Scope and Contents note','Separated Materials note':'Separated Materials note','Text':'Text'}
var taskValueMap = {'Processing Beginning':'Processing Beginning','Processing Ending':'Processing Ending'};
var extentType = {'':'','box':'Box','cubic_foot':'Cubic Foot','envelope':'Envelope','file':'File','folder':'Folder','item':'Item','object':'Object','package':'Package','linear_foot':'Linear Foot'};
var languages = {'ar':'Arabic','zh':'Chinese','cs':'Czech','da':'Danish','nl':'Dutch','en':'English','fi':'Finnish','fr':'French','de':'German','el':'Greek','he':'Hebrew','hu':'Hungarian','is':'Icelandic','it':'Italian','ja':'Japanese','ko':'Korean','no':'Norwegian','pl':'Polish','pt':'Portugese','ru':'Russian','es':'Spanish','sv':'Swedish','th':'Thai','tr':'Turkish'};
var currCompNode = 0;
var persistContainer = true;
function updateEntityData(xmlDoc, xmlText) {
	var compXml = component.recordsFromXML(isc.XMLTools.selectNodes(xmlDoc, "//node"));
	isc.ResultSet.create({ID:"componentRS",dataSource:"component",initialData:compXml});
	for(var i=0; i < aspectNames.length; i++) {
		for(var j=0; j < aspectArray[aspectNames[i]].length; j++) componentForm.getField(aspectArray[aspectNames[i]][j]).hide();
	}
	repositoryTree.getSelectedRecord().title = compXml[0]['title'];
	repositoryTree.getSelectedRecord().icon = compXml[0]['icon'];
	repositoryTree.refreshFields();
	if(currCompNode.localName == 'collection') {
		collectionForm.setValues(compXml);
	} else if(currCompNode.localName == 'category') {
		componentForm.getField("qname").hide();
		componentForm.setValues(compXml);
		componentForm.getField("header").setValue(componentForm.getValue("level"));
	} else {
		//document.getElementById('finding_aid_button').style.visibility = 'hidden';
		componentForm.getField("qname").show();
		//if(rightTabSet.getTab("finding_aid_tab")) rightTabSet.removeTab("finding_aid_tab");
		componentForm.setValues(compXml);
		if(componentForm.getValue("localName") != 'item') {
			componentForm.getField("level").hide();
			componentForm.getField("header").setValue(componentForm.getValue("localName"));
			for(var i=0; i < aspectArray[componentForm.getValue("localName")].length; i++) {
				componentForm.getField(aspectArray[componentForm.getValue("localName")][i]).show();
			}
		} else {
			componentForm.getField("header").setValue(componentForm.getValue("level"));
		}
	}
	noteXml = ds.recordsFromXML(isc.XMLTools.selectNodes(xmlDoc, "//node/source_associations/notes/node"));
	noteList.setData(noteXml);
	activityXml = ds.recordsFromXML(isc.XMLTools.selectNodes(xmlDoc, "//node/source_associations/activities/node"));
	activityList.setData(activityXml);
	taskXml = ds.recordsFromXML(isc.XMLTools.selectNodes(xmlDoc, "//node/source_associations/tasks/node"));
	taskList.setData(taskXml);
	nameXml = names.recordsFromXML(isc.XMLTools.selectNodes(xmlDoc, "//node/source_associations/named_entities/node"));
	isc.ResultSet.create({ID:"nameRS",dataSource:"names",initialData:nameXml});
	namePanelList.setData(nameXml);
	subjXml = subjects.recordsFromXML(isc.XMLTools.selectNodes(xmlDoc, "//node/source_associations/subjects/node"));
	isc.ResultSet.create({ID:"subjRS",dataSource:"subjects",initialData:subjXml});
	subjectPanelList.setData(subjXml);
	accessXml = aclDS.recordsFromXML(isc.XMLTools.selectNodes(xmlDoc, "//node/source_associations/access/entry"));
	isc.ResultSet.create({ID:"accessRS",dataSource:"aclDS",initialData:accessXml});
	aceList.setData(accessXml);
	tagXml = aclDS.recordsFromXML(isc.XMLTools.selectNodes(xmlDoc, "//node/source_associations/tags/node"));
	isc.ResultSet.create({ID:"tadRS",dataSource:"tags",initialData:tagXml});
	tagPanelList.setData(tagXml);
	objectXml = aclDS.recordsFromXML(isc.XMLTools.selectNodes(xmlDoc, "//node/source_associations/digital_objects/node"));
	isc.ResultSet.create({ID:"tagRS",dataSource:"objects",initialData:objectXml});
	digitalObjectPanelList.setData(objectXml);
	
}
function save(reload) {
	if(collectionForm.valuesHaveChanged()) {
		var parms = collectionForm.getValues();
		parms['qname'] = '{openapps.org_repository_1.0}collection'
		if(reload) isc.XMLTools.loadXML("/core/entity/update", "updateEntityData(xmlDoc, xmlText)",{httpMethod:"POST",params:parms});
		else isc.XMLTools.loadXML("/core/entity/update", function(dsResponse, data, dsRequest) {},{httpMethod:"POST",params:parms});
	}
	if(componentForm.valuesHaveChanged()) {
		var parms = componentForm.getValues();
  		if(componentForm.getValue("localName") == 'category') {
  			parms['qname'] = '{openapps.org_system_1.0}category';
  		} 
		if(reload) isc.XMLTools.loadXML("/core/entity/update", "updateEntityData(xmlDoc, xmlText)",{httpMethod:"POST",params:parms});
		else isc.XMLTools.loadXML("/core/entity/update", function(dsResponse, data, dsRequest) {},{httpMethod:"POST",params:parms});
	}
}
function stopUpload(id) {
	importTree.fetchData(Math.random(),function(dsResponse, data, dsRequest) {
		var obj = data.get(0).title;
		uploadForm.setValue('title', obj);
	});
}
isc.MessagingDataSource.create({ID:'names',fetchDataURL:'fetch.xml',addDataURL:'add.xml',updateDataURL:'update.xml',removeDataURL:'remove.xml',fields:[{name:'id',title:'ID',type:'text',primaryKey:true,required:true}]});
isc.MessagingDataSource.create({ID:'groups',fetchDataURL:'fetch.xml',addDataURL:'add.xml',updateDataURL:'update.xml',removeDataURL:'remove.xml',fields:[{name:'id',title:'ID',type:'text',primaryKey:true,required:true}]});
isc.MessagingDataSource.create({ID:'subjects',fetchDataURL:'fetch.xml',addDataURL:'add.xml',updateDataURL:'update.xml',removeDataURL:'remove.xml',fields:[{name:'id',title:'ID',type:'text',primaryKey:true,required:true}]});
isc.MessagingDataSource.create({ID:'notes',fetchDataURL:'fetch.xml',addDataURL:'add.xml',updateDataURL:'update.xml',removeDataURL:'remove.xml',fields:[{name:'id',title:'ID',type:'text',primaryKey:true,required:true}]});
isc.MessagingDataSource.create({ID:'tags',fetchDataURL:'fetch.xml',addDataURL:'add.xml',updateDataURL:'update.xml',removeDataURL:'remove.xml',fields:[{name:'id',title:'ID',type:'text',primaryKey:true,required:true}]});
isc.MessagingDataSource.create({ID:'component',fetchDataURL:'fetch.xml',addDataURL:'add.xml',updateDataURL:'update.xml',removeDataURL:'remove.xml',fields:[{name:'restrictions', type:'boolean'},{name:'internal', type:'boolean'},{name:'id',title:'ID',type:'text',primaryKey:true,required:true}]});
isc.MessagingDataSource.create({ID:'objects',fetchDataURL:'fetch.xml',addDataURL:'add.xml',updateDataURL:'update.xml',removeDataURL:'remove.xml',fields:[{name:'id',title:'ID',type:'text',primaryKey:true,required:true}]});
isc.MessagingDataSource.create({ID:"repositoryDS",fetchDataURL:"/repository/collection",addDataURL:"/core/entity/associate.xml",updateDataURL:"/repository/collection/update.xml",removeDataURL:"/core/entity/remove.xml",
	fields:[
		{name:"id", title:"ID", type:"text", primaryKey:true, required:true},
    	{name:"title", title:"Title", type:"text", length:"128", required:true},
    	{name:"idx", title:"Index", type:"integer"},
    	{name:"parent", title:"ParentID", type:"text", required:true, foreignKey:"repositories.id", rootValue:"null"}
	]
});
isc.MessagingDataSource.create({ID:"objectDS",fetchDataURL:"fetch.xml",addDataURL:"add.xml",updateDataURL:"update.xml",removeDataURL:"remove.xml",
	fields:[
		{name:"id", title:"ID", type:"text", primaryKey:true, required:true},
    	{name:"title", title:"Title", type:"text", length:"128", required:true},
    	{name:"type", type:"text"},
    	{name:"parent", title:"ParentID", type:"text", required:true, foreignKey:"physical.id", rootValue:"null"}
	]
});
isc.MessagingDataSource.create({ID:"aclDS",fetchDataURL:"fetch.xml",addDataURL:"add.xml",updateDataURL:"update.xml",removeDataURL:"remove.xml",
	fields:[
	    {name:"id", title:"ID", type:"text", primaryKey:true, required:true}
	]
});
isc.MessagingDataSource.create({ID:"authorityDS",fetchDataURL:"fetch.xml",addDataURL:"add.xml",updateDataURL:"update.xml",removeDataURL:"remove.xml",
	fields:[
	    {name:"id", title:"ID", type:"text", primaryKey:true, required:true}
	]
});
isc.HLayout.create({ID:'buttonPanel',height:'25',width:'100%',autoDraw:false,layoutLeftMargin:2,membersMargin:5,
	members:[
	    isc.IButton.create({autoDraw:false,title:'import',width:70,icon:"/theme/images/icons16/database.png",click:'importWindow.show();'}),
	    isc.IButton.create({autoDraw:false,title:'export',width:70,icon:"/theme/images/icons16/table.png",click:'exportWindow.show();'}),
	    isc.IButton.create({autoDraw:false,title:'add',width:60,icon:"/theme/images/icons16/add.png",click:'addItemWindow.show();'}),
	    isc.IButton.create({autoDraw:false,title:'delete',width:70,icon:"/theme/images/icons16/remove.png",click:'repositoryTree.removeSelectedData();'}),
	    isc.IButton.create({autoDraw:false,title:'save',width:60,icon:"/theme/images/icons16/save.png",click:'save(true);'})
	]
});
isc.TreeGrid.create({ID:'repositoryTree',dataSource:'repositoryDS',height:'100%',width:'100%',showHeader:false,animateFolders:true,
	canAcceptDroppedRecords:true,canReparentNodes:true,canReorderRecords:true,canDropOnLeaves:true,selectionType:'single',autoDraw:false,
	dragDataAction:'move',canDragRecordsOut:true,nodeContextClick:'repositoryTreeMenu.showContextMenu()',
	folderDrop:function(dragRecords,dropFolder,index){repositoryDrop(dragRecords, dropFolder, index);},
	recordClick:'load_repository(record)'
});
//isc.VLayout.create({ID:'repository',autoDraw:false,height:'100%',width:'100%',members:[repositoryTree]});
isc.VLayout.create({ID:'leftCanvas',visibility:'visible',position:'absolute',width:'350',height:'100%',
	members:[
	    buttonPanel,
	    isc.DynamicForm.create({ID:'repositoryListSearch',width:'100%',margin:0,numCols:2,cellPadding:2,
		    fields:[
		        {name:'query',width:'300',type:'text',showTitle:false},
		        {name:'validateBtn',title:'search',type:'button',width:50,startRow:false,endRow:false,
		        	click:function() {
		        		query = repositoryListSearch.getValue('query');
		        		repositoryTree.fetchData({qname:'{openapps.org_repository_1.0}collection',query:query,field:'name_e',sort:'name_e'});
		        	}
		        }
		    ]
		}),
	    repositoryTree	    
	]
});
isc.DynamicForm.create({ID:'collectionForm',width:'98%',numCols:'4',colWidths:[130,'*'],visibility:'hidden',autoDraw:false,autoFocus:false,cellPadding:5,
	fields:[
	    {defaultValue:'collection',type:'header',align:'center'},
	    {name:'name',width:'*',type:'richText',height:100,controlGroups:["fontControls","styleControls"],showTitle:true,colSpan:'4',title:'Heading'},
	    {name:'scope_note',width:'*',type:'richText',height:100,controlGroups:["fontControls","styleControls"],showTitle:true,colSpan:'4',title:'Scope Note'},
	    {name:'comments',width:'*',type:'richText',height:100,controlGroups:["fontControls","styleControls"],showTitle:true,colSpan:'4',title:'Comments'},
	    {name:'bio_note',width:'*',type:'richText',height:100,controlGroups:["fontControls","styleControls"],showTitle:true,colSpan:'4',title:'Biographical Note'},
	    {name:'date_expression',width:'*',title:'Date Expression'},
	    {type:'spacer'},
	    //{name:'okra_num',width:'200',title:'OKRA ID'},
	    {name:'begin',width:'150',title:'Begin Date'},
	    {name:'end',width:'150',title:'End Date'},
	    {name:'bulk_begin',width:'150',title:'Bulk Begin Date'},
	    {name:'bulk_end',width:'150',title:'Bulk End Date'},     
	    {name:'extent_number',width:'100',title:'Extent Number'},	    
	    {name:'extent_units',width:'100',type:'select',valueMap:extentType,width:'100',title:'Extent Units'},
	    {name:'language',width:'*',type:'select',valueMap:languages,width:'100',title:'Language'},
	    {name:'restrictions',width:'*',type:'select',valueMap:{'true':'yes','false':'no'},width:75,title:'Restrictions'},
	    {name:'internal',width:'*',type:'select',valueMap:{'true':'yes','false':'no'},width:75,title:'Internal'}
	]
});
isc.DynamicForm.create({ID:'componentForm',width:'98%',numCols:'4',colWidths:[120,'*'],autoDraw:false,visibility:'hidden',autoFocus:false,cellPadding:5,
	fields:[
	    {name:'header',defaultValue:'Component',type:'header',align:'center'},
	    {name:'name',width:'*',autoDraw:false,type:'richText',height:100,controlGroups:["fontControls","styleControls"],showTitle:true,colSpan:'4',title:'Heading'},
	    {name:'description',width:'*',autoDraw:false,type:'richText',height:100,controlGroups:["fontControls","styleControls"],showTitle:true,colSpan:'4',title:'Description'},
	    {name:'summary',width:'*',autoDraw:false,type:'richText',height:100,controlGroups:["fontControls","styleControls"],showTitle:true,colSpan:'4',title:'Summary'},
	    {name:'authors',width:'300',title:'Authors',autoDraw:false},
	    {name:'qname',width:'125',title:'Content Type',type:'select',valueMap:itemTypes},
	    {name:'date_expression',width:'*',autoDraw:false,title:'Date Expression'},
	    {name:'accession_date',width:'150',autoDraw:false,title:'Accession Date'},
	    {name:'container',width:'*',autoDraw:false,icons:[{src:'/theme/images/icons/edit_form.gif',click:'component_containerCustomControl()'}],title:'Container'},
	    {type:'spacer'},
	    //{name:'okra_num',width:'200',autoDraw:false,title:'OKRA ID'},
	    {name:'begin',width:'150',autoDraw:false,title:'Begin Date'},
	    {name:'end',width:'150',autoDraw:false,title:'End Date'},
	    {name:'extent_number',width:'100',autoDraw:false,title:'Extent Number'},
	    {name:'extent_units',width:'100',autoDraw:false,type:'select',valueMap:extentType,title:'Extent Units'},
	    {name:'level',type:'select',autoDraw:false,valueMap:{'series':'series','subseries':'subseries','file':'file','item':'item','subgrp':'subgrp'},width:'100',title:'Level'},
	    {name:'language',width:'*',autoDraw:false,type:'select',valueMap:languages,width:'100',title:'Language'},
	    {name:'restrictions',width:'*',autoDraw:false,type:'select',valueMap:{'true':'yes','false':'no'},width:75,title:'Restrictions'},
	    {name:'internal',width:'*',autoDraw:false,type:'select',valueMap:{'true':'yes','false':'no'},width:75,title:'Internal'},
	    {name:'financial_genre',width:'*',autoDraw:false,type:'select',valueMap:{'':'','Receipt':'Receipt','Royalty stamement':'Royalty stamement','Tax return':'Tax return','Check stub':'Check stub','Bank stamement':'Bank stamement','Other':'Other'},width:'200',title:'Genre'},
	    {name:'photograph_form',width:'*', visible:false,autoDraw:false,type:'select',valueMap:{'':'','Print':'Print','Contact sheet':'Contact sheet','Negative':'Negative','Slide':'Slide','Polaroid':'Polaroid','Daguerrotype':'Daguerrotype','Glass plame negamive':'Glass plame negamive','Tintype':'Tintype','Carte de visite':'Carte de visite','Albumen print':'Albumen print','Cyanotype':'Cyanotype','Unknown':'Unknown','Other':'Other'},width:'200',title:'Form'},
	    {name:'photograph_size',width:'*', visible:false,autoDraw:false,title:'Size'},
	    {name:'legal_genre',width:'*', visible:false,autoDraw:false,type:'select',valueMap:{'':'','Contract':'Contract','Deed':'Deed','Passport':'Passport','Birth certificame':'Birth certificame','Marriage certificame':'Marriage certificame','Deamh certificame':'Deamh certificame','Court transcript':'Court transcript','Other':'Other'},width:'200',title:'Genre'},
	    {name:'correspondence_genre',width:'*', visible:false,autoDraw:false,type:'select',valueMap:{'':'','Letter':'Letter','Postcard':'Postcard','Card':'Card','Telegram':'Telegram','Invitamion':'Invitamion','Fax':'Fax','E-mail':'E-mail','Memo':'Memo','Note':'Note','Other':'Other'},width:'100',title:'Genre'},
	    {name:'correspondence_form',width:'*', visible:false,autoDraw:false,type:'select',valueMap:{'':'','ALS':'ALS','TLS':'TLS','CTLS':'CTLS','ANS':'ANS','TNS':'TNS','AL':'AL','TL':'TL','Other':'Other'},width:'100',title:'Form'},
	    {name:'research_genre',width:'*', visible:false,autoDraw:false,type:'select',valueMap:{'':'','Interview':'Interview','Notes':'Notes','Other':'Other'},width:'100',title:'Genre'},
	    {name:'audio_medium',width:'*', visible:false,autoDraw:false,type:'select',valueMap:{'':'','Phonograph record, 33 1/3 rpm':'Phonograph record, 33 1/3 rpm','Phonograph record, 45 rpm':'Phonograph record, 45 rpm','Phonograph record, 78 rpm':'Phonograph record, 78 rpm','Cassette':'Cassette','8-Track tape':'8-Track tape','Reel-to-reel tape, 1/4�':'Reel-to-reel tape, 1/4�','Reel-to-reel tape, 2�':'Reel-to-reel tape, 2�','Digital audio tape':'Digital audio tape','Compact disc':'Compact disc','Acetame disc':'Acetame disc','Unknown':'Unknown','Other':'Other'},width:'200',title:'Medium'},
	    {name:'journals_genre',width:'*', visible:false,autoDraw:false,type:'select',valueMap:{'':'','Diary':'Diary','Journal':'Journal','Daily planner':'Daily planner','Commonplace book':'Commonplace book','Other':'Other'},width:'200',title:'Genre'},
	    {name:'journals_form',width:'*', visible:false,autoDraw:false,type:'select',valueMap:{'':'','Holograph':'Holograph','TS':'TS','Other':'Other'},width:'100',title:'Form'},
	    {name:'professional_genre',width:'*', visible:false,autoDraw:false,type:'select',valueMap:{'':'','Agenda':'Agenda','Schedule':'Schedule','Meeting minutes':'Meeting minutes','Appointment book':'Appointment book','Report':'Report','Itinerary':'Itinerary','Press kit':'Press kit','Other':'Other'},width:'200',title:'Genre'},
	    {name:'scrapbooks_form',width:'*', visible:false,autoDraw:false,type:'select',valueMap:{'':'','Printed mamerial':'Printed mamerial','Photographs':'Photographs','Correspondence':'Correspondence','Manuscripts':'Manuscripts','Other':'Other'},width:'200',title:'Form'},
	    {name:'notebooks_form',width:'*', visible:false,autoDraw:false,type:'select',valueMap:{'':'','Holograph':'Holograph','TS':'TS','Other':'Other'},width:'100',title:'Form'},
	    {name:'notebooks_genre',width:'*', visible:false,autoDraw:false,type:'select',valueMap:{'':'','Reporter�s notebook':'Reporter�s notebook','General notebook':'General notebook','Other':'Other'},width:'200',title:'Genre'},
	    {name:'printed_genre',width:'*', visible:false,autoDraw:false,type:'select',valueMap:{'':'','Newspaper':'Newspaper','Magazine':'Magazine','Program':'Program','Pamphlet':'Pamphlet','Booklet':'Booklet','Poster':'Poster','Flyer':'Flyer','Proof':'Proof','Map':'Map','Other':'Other'},width:'100',title:'Genre'},
	    {name:'artwork_form',width:'*', visible:false,autoDraw:false,type:'select',valueMap:{'':'','Painting':'Painting','Sculpture':'Sculpture','Drawing, ink':'Drawing, ink','Print':'Print','Ceramic':'Ceramic','Textile':'Textile','Unknown':'Unknown','Other':'Other'},width:'100',title:'Form'},
	    {name:'artwork_genre',width:'*', visibility:'hidden',autoDraw:false,type:'select',valueMap:{'':'','Portrait':'Portrait','Bust':'Bust','Landscape':'Landscape','Caricamure':'Caricamure','Cartoon':'Cartoon','Comic strip':'Comic strip','Illustramion':'Illustramion','Bas-relief':'Bas-relief','Pastel':'Pastel','Wamercolor':'Wamercolor','Unknown':'Unknown','Other':'Other'},width:'100',title:'Genre'},
	    {name:'artwork_media',width:'*', visible:false,autoDraw:false,type:'select',valueMap:{'':'','Oil':'Oil','Acrylic':'Acrylic','Wamercolor':'Wamercolor','Pastel':'Pastel','Pencil':'Pencil','Ink':'Ink','Metal':'Metal','Stone':'Stone','Porcelain':'Porcelain','Wood':'Wood','Mixed media':'Mixed media','Unknown':'Unknown','Other':'Other'},width:'100',title:'Media'},
	    {name:'artwork_size',width:'125', visible:false,autoDraw:false,title:'Size'},
	    {name:'manuscript_genre',width:'*',visible:false,autoDraw:false,type:'select',valueMap:{'':'','Novel':'Novel','Short story':'Short story','Stage play':'Stage play','Screenplay':'Screenplay','Teleplay':'Teleplay','Radio play':'Radio play','Poem':'Poem','Essay':'Essay','Article':'Article','Speech':'Speech','Memoir':'Memoir','Notes':'Notes','Other':'Other'},width:'100',title:'Genre'},
	    {name:'manuscript_form',width:'*', visible:false,autoDraw:false,type:'select',valueMap:{'':'','Holograph':'Holograph','TS':'TS','CTS':'CTS','Other':'Other'},width:'100',title:'Form'},{name:'memorabilia_genre',width:'*',type:'select',valueMap:{'':'','Diploma':'Diploma','Certificame':'Certificame','Object':'Object','Textile':'Textile','Award':'Award','Unknown':'Unknown','Other':'Other'},width:'100',title:'Genre'},
	    {name:'medical_genre',width:'*', visible:false,autoDraw:false,type:'select',valueMap:{'':'','Chart':'Chart','X-ray film':'X-ray film','Ultrasound':'Ultrasound','Prescription':'Prescription','Personal health report':'Personal health report','Other':'Other'},width:'200',title:'Genre'},
	    {name:'video_medium',width:'200',visible:false,autoDraw:false,type:'select',valueMap:{'':'','Film reel':'Film reel','Analog video cassette or reel':'Analog video cassette or reel','Digital video cassette':'Digital video cassette','Optical disc':'Optical disc','Unknown':'Unknown','Other':'Other'},title:'Medium'},
	    {name:'video_form',width:'200', visible:false,autoDraw:false,type:'select',valueMap:{'':'','8mm or Super 8mm (film)':'8mm or Super 8mm (film)','16mm (film)':'16mm (film)','35mm (film)':'35mm (film)','VHS, NTSC (analog video)':'VHS, NTSC (analog video)','VHS, PAL (analog video)':'VHS, PAL (analog video)','U-Mamic (analog video)':'U-Mamic (analog video)','Betamax (analog video)':'Betamax (analog video)','Reel-to-reel video (analog video)':'Reel-to-reel video (analog video)','Betacam (digital video)':'Betacam (digital video)','DV or MiniDV (digital video)':'DV or MiniDV (digital video)','Laserdisc (optical disc)':'Laserdisc (optical disc)','DVD (optical disc)':'DVD (optical disc)','Unknown':'Unknown','Other':'Other'},title:'Form'}
	]
});
isc.Canvas.create({ID:'mainCanvas',width:'100%',height:'100%',visibility:'hidden',children:[collectionForm,componentForm],width:'100%',height:'100%'});

isc.VLayout.create({ID:"notesAndActivitiesPanel",autoDraw:false,height:'100%',width:'100%',margin:5,membersMargin:5,
	members:[
	    getNotesPanel(noteValueMap),
	    getActivitiesPanel(activityValueMap),
	    getTasksPanel(taskValueMap)
	]
});
isc.VLayout.create({ID:"classificationPanel",autoDraw:false,height:'100%',width:'100%',margin:5,membersMargin:5,
	members:[
	    getNamedEntityPanel('100%','30%'),
	    getSubjectPanel('100%','30%'),
	    getTagPanel('100%','30%')
	]
});
isc.TabSet.create({ID:'rightTabSet',tabBarPosition:'top',height:'100%',width:'100%',autoDraw:false,
	tabs:[
	    {title:'Basic Information',pane:mainCanvas},
	    {title:'Notes, Activities and Tasks',pane:notesAndActivitiesPanel},
	    {title:'Names, Subjects and Tags',pane:classificationPanel},
	    {title:'Digital Objects',pane:getDigitalObjectPanel()}
	]
});
isc.HLayout.create({ID:'pageLayout',position:'relative',visibility:'visible',members:[leftCanvas,rightTabSet],width:smartWidth,height:smartHeight});

function loadDigitalObjectPanel(record) {
	buff = "<img src='/theme/images/filetypes64/" + record.mimetype + ".png' />";
	
	digitalObjectIcon.setContents(buff);
}
function findingAid() {
	var comp = repositoryTree.getSelectedRecord();
	isc.XMLTools.loadXML("../finding_aid/export.xhtml?collection=" + comp.id, function(dsResponse, data, dsRequest) {});
}
function filterCollections(form) {
	repositoryTree.fetchData({query:form.getValue('repoQuery')});
	
}
function save_notesPanel() {
	save(false);
	var parms = notesPanelForm.getValues();
	parms['component'] = repositoryTree.getSelectedRecord().id;
	if(!parms['id']) {
		if(repositoryTree.anySelected()) {
			isc.XMLTools.loadXML("add.xml?_dataSource=note", "updateEntityData(xmlDoc, xmlText)",{httpMethod:"GET",params:parms});
		} else isc.warn('Please select a collection or component to attach this note to.')
	} else isc.XMLTools.loadXML("update.xml?_dataSource=note", "updateEntityData(xmlDoc, xmlText)",{httpMethod:"GET",params:parms});
}
function openTreeNode(key,value,tree) {
	var thisIndex = tree.data.findIndex(key, value);
    if(thisIndex > -1) {
    	var thisRecord = tree.data.get(thisIndex);
    	if(thisRecord) {
    		var parentIndex = tree.data.findIndex("id", thisRecord.parentId);
    		var parentRecord= tree.data.get(parentIndex);
    		tree.openFolder(parentRecord);
    		tree.openFolder(thisRecord);
    		tree.deselectAllRecords();
    		tree.selection.select(thisRecord);
    	}
    }
}
function load_repository(node) {
	//save(false);
	entityId = node.id;
	if(currCompNode !== null && node.id !== currCompNode.id) {
		isc.XMLTools.loadXML("/core/entity/get/"+node.id, "updateEntityData(xmlDoc, xmlText)");
		rightTabSet.enableTab(1);
		rightTabSet.enableTab(2);
		rightTabSet.enableTab(3);
		rightTabSet.enableTab(4);
		rightTabSet.enableTab(5);
		if(node.localName == 'collection') {
			componentForm.hide();
			collectionForm.show();
		} else {
			collectionForm.hide();
			componentForm.show();
		}
		currCompNode = node;
	}
}
var objectId;

function remove_notesPanel() {
	save(false);
	var note_id = notesPanelList.getSelectedRecord().target_id;
	var component_id = repositoryTree.getSelectedRecord().id;
	isc.XMLTools.loadXML("remove.xml?_dataSource=notes&note="+note_id+"&component="+component_id, "updateEntityData(xmlDoc, xmlText)");
}
function remove_digitalObjectPanel() {
	save(false);
	var note_id = digitalObjectPanelList.getSelectedRecord().id;
	var component_id = repositoryTree.getSelectedRecord().id;
	isc.XMLTools.loadXML("remove.xml?_dataSource=digital_objects&id="+note_id+"&component="+component_id, "updateEntityData(xmlDoc, xmlText)");
}
function remove_ace() {
	save(false);
	var id = aceList.getSelectedRecord().id;
	var component_id = repositoryTree.getSelectedRecord().id;
	isc.XMLTools.loadXML("remove.xml?_dataSource=aclDS&id="+id+"&component="+component_id, "updateEntityData(xmlDoc, xmlText)");
}
function repositoryDrop(dragRecords,dropFolder,index) {
	var child = dragRecords.get(0);
	//if(child.localName != 'collection') {
		repositoryTree.updateData({id:dragRecords.get(0).id,parent:dropFolder.id},function(dsResponse, data, dsRequest) {
		
		});
	//}
}
function getRepositoryIcon(node) {
	return false;
}
function component_containerCustomControl() {
	containerControlForm.clearValues();
	var container = componentForm.getValue("component_container");
	if(container != null) {
		var containers = container.split(",");
		for(i=0; i<containers.length;i++) {
			var entry = containers[i].split(" ");
			if(entry.length == 3) {
				containerControlForm.setValue('type'+i, entry[1]);
				containerControlForm.setValue('value'+i, entry[2]);
			} else {
				containerControlForm.setValue('type'+i, entry[0]);
				containerControlForm.setValue('value'+i, entry[1]);
			}
		}
	}
	containerControlWindow.show(true);
}
isc.Menu.create({ID:"repositoryTreeMenu",cellHeight:22,
	showContextMenu: function() {
		repositoryTreeMenu.setItemEnabled(0, true);
		repositoryTreeMenu.setItemEnabled(1, true);
		repositoryTreeMenu.setItemEnabled(2, true);
		repositoryTreeMenu.setItemEnabled(3, true);
		return this.Super('showContextMenu', arguments);
	},
    data:[
        {title:"Add New Item",click:"addItemWindow.show();"},
        {title:"Delete",
        	click:function() {
        		repositoryTree.removeSelectedData();
        	}
        },
        //{title:"Access Control",click:"accessControlWindow.show();"},
        {title:"Set Child Property",click:"childPropertyWindow.show();"}
    ]
});
isc.Window.create({ID:"containerControlWindow",title:"Edit Container",width:600,height:210,autoCenter:true,isModal:true,showModalMask:true,autoDraw:false,
	show : function (persist) { 
		persistContainer = persist;
		this.Super("show", arguments);
	},
	items: [
        isc.DynamicForm.create({ID:"containerControlForm",colWidths:[100,"*"],numCols:"4",cellPadding:7,autoDraw:false,
          	fields: [
          	    {name:"type0", title:"Type 1", width:200,type:"select",valueMap:containerTypes},
           	    {name:"value0", title:"Value 1", width:200, type:"text"},
           	    {name:"type1", title:"Type 2", width:200,type:"select",valueMap:containerTypes},
        	    {name:"value1", title:"Value 2", width:200, type:"text"},
        	    {name:"type2", title:"Type 3", width:200,type:"select",valueMap:containerTypes},
           	    {name:"value2", title:"Value 3", width:200, type:"text"},
           	    {name:"type3", title:"Type 4", width:200,type:"select",valueMap:containerTypes},
        	    {name:"value3", title:"Value 4", width:200, type:"text"},
           	    {name: "validateBtn", title: "Save", type: "button", 
           	     	 click: function() {
           	    		var containerStr = "";
           	    		var type0 = containerControlForm.getValue("type0");
           	    		var value0 = containerControlForm.getValue("value0");
           	    		if(type0 != undefined && type0 != "" && value0 != undefined && value0 != "") {
           	    			containerStr += type0 + " " + value0;
           	    			var type1 = containerControlForm.getValue("type1");
               	    		var value1 = containerControlForm.getValue("value1");
               	    		if(type1 != undefined && type1 != "" && value1 != undefined && value1 != "") {
               	    			containerStr += ", " + type1 + " " + value1;
               	    			var type2 = containerControlForm.getValue("type2");
                   	    		var value2 = containerControlForm.getValue("value2");
                   	    		if(type2 != undefined && type2 != "" && value2 != undefined && value2 != "") {
                   	    			containerStr += ", " + type2 + " " + value2;
                   	    			var type3 = containerControlForm.getValue("type3");
                       	    		var value3 = containerControlForm.getValue("value3");
                       	    		if(type3 != undefined && type3 != "" && value3 != undefined && value3 != "") {
                       	    			containerStr += ", " + type3 + " " + value3;
                   	    			}
               	    			}
           	    			}
           	    		}
           	    		if(persistContainer) componentForm.setValue("container", containerStr);
           	    		else childPropertyForm.setValue("container", containerStr);
           	    		containerControlWindow.hide();           	    		
           	    	 }
            	}
            ]
        })
    ]
});
isc.Window.create({ID:"childPropertyWindow",title:"Set Child Property",width:320,height:100,autoCenter:true,isModal:true,showModalMask:true,autoDraw:false,
    items: [
        isc.DynamicForm.create({ID:"childPropertyForm",width:100,numCols:"2",cellPadding:7,autoDraw:false,
          	fields: [
          	    {name:"type", title:"Type", width:250,type:"select",redrawOnChange:true,valueMap:propertyNames,
          	    	changed:function(form,item,value) {
          	    		childPropertyWindow.setHeight(140);
        	    	}
          	    },
           	    {name:"level2", title:"Value", width:250, type:"select",showIf:"childPropertyForm.getValue('type') == 'level2'",valueMap:itemTypes},
           	    {name:"container", title:"Value", width:250, type:"staticText",showIf:"childPropertyForm.getValue('type') == 'component_container'",
           	    	icons:[{src:'/theme/images/icons/edit_form.gif',click:'containerControlWindow.show(false);'}]
           	    },
           	    {name: "validateBtn", title: "Save", type: "button", 
           	     	 click: function() {
           	    		var id = repositoryTree.getSelectedRecord().id;
           	    		if(childPropertyForm.getValue('type') == 'level2') {           	    			
               	    		var value = childPropertyForm.getValue("level2");  
               	    		var qname = 'qname';
           	    		} else if(childPropertyForm.getValue('type') == 'component_container') {           	    			
               	    		var value = childPropertyForm.getValue("container");
               	    		var qname = '{openapps.org_repository_1.0}container';
           	    		}
           	    		isc.XMLTools.loadXML("/repository/collection/propagate.xml?id="+id+"&qname="+qname+"&value="+value, function(xmlDoc, xmlText) {
           	    			childPropertyWindow.hide();
           	    		});         	    		
           	    	 }
            	}
            ]
        })
    ]
});
isc.Window.create({ID:"accessControlWindow",title:"Access Control",width:500,height:190,autoCenter:true,isModal:true,showModalMask:true,autoDraw:false,
    items: [
        isc.ListGrid.create({ID:"aceList",dataSource:"aclDS",autoSaveEdits:false,width:"100%",height:"100%",showHeader:true,animateFolders:true,selectionType:"single",autoDraw:false,
        	fields:[
        	    {name:"authority", title:"Group/User Name", type:"static"},
        	    {name:"permission", title:"Permission", type:"select",width:100,valueMap:{"1":"Read","2":"Write","0":"No Control"}}
        	]
        }),
        isc.HLayout.create({width:"100%",height:23,
        	members:[
        	    isc.Button.create({ID:"addUserButton",title:"Add",click:"authorityList.fetchData();userSelectionWindow.show()"}),
        	    isc.Button.create({ID:"removeUserButton",title:"Remove",click:"remove_ace()"})
            ]
        })
    ]
});
isc.Window.create({ID:"userSelectionWindow",title:"User/Group Permission",width:500,height:190,autoCenter:true,isModal:true,showModalMask:false,autoDraw:false,
    items: [
        isc.ListGrid.create({ID:"authorityList",dataSource:"authorityDS",width:"100%",height:"100%",showHeader:true,animateFolders:true,selectionType:"single",autoDraw:false,canEdit:true,editEvent:"click",modelEditing:true,
        	fields:[
        	    {name:"authority", title:"Username",canEdit:false}        	    
        	]
        }),
        isc.HLayout.create({width:"100%",height:23,
        	members:[
        	    isc.DynamicForm.create({ID:"permissionForm",fields:[{name:"permission", title:"Permission", width:100,type:"select",valueMap:{"1":"Read","2":"Write","0":"No Control"}}]}),
        	    isc.Button.create({ID:"selectUserButton",title:"Add",
        	    	click:function() {
        	    		var id = repositoryTree.getSelectedRecord().id;
        	    		var authority = authorityList.getSelectedRecord().authority;
        	    		var permission = permissionForm.getValue("permission");
        	    		isc.XMLTools.loadXML("add.xml?_dataSource=aclDS&id="+id+"&authority="+authority+"&permission="+permission, "updateEntityData(xmlDoc, xmlText)");
        	    		userSelectionWindow.hide();
        	    	}
        	    })
            ]
        })
    ]
});
isc.Window.create({ID:"addItemWindow",title:"New Component",width:500,height:210,autoCenter:true,isModal:true,showModalMask:true,autoDraw:false,
    items: [
        isc.DynamicForm.create({ID:"addItemWindowForm",width:"100%",numCols:2,colWidths:[25,"*"],cellPadding:5,autoFocus:false,autoDraw:false,
        	fields:[
        	    {name:"title", title:"Title", type:"textArea", required:true, width:420},
        	    {name:"level", title: "Level", width:150, required:true, type: "select",valueMap:typeNames,
        	    	changed:function(form,item,value){
        	    		if(value == 'item') {
        	    			addItemWindow.setHeight(235);
        	    			addItemWindowForm.getField('type').show();
        	    		} else {
        	    			addItemWindow.setHeight(210);
        	    			addItemWindowForm.getField('type').hide();
        	    		}
        	    	}
        	    },
        	    {name:"type", title:"Type", type:"select",visible:false,valueMap:aspectNames},
        	    {name: "validateBtn", title: "Save", type: "button", 
        	    	click: function() {
            			if(this.form.validate()) {
            				parms = {};
            				if(addItemWindowForm.getValue('level') == 'item') {
            					parms['assoc_qname'] = '{openapps.org_repository_1.0}items';
            					parms['entity_qname'] = '{openapps.org_repository_1.0}'+addItemWindowForm.getValue('type');
            				} else {
            					parms['assoc_qname'] = '{openapps.org_repository_1.0}categories';
            					parms['entity_qname'] = '{openapps.org_repository_1.0}category';
            					parms['level'] = addItemWindowForm.getValue('level');
            				}
	         	    		parms['source'] = repositoryTree.getSelectedRecord().id;
	         	    		parms['name'] = addItemWindowForm.getValue('title');
	         	    		parms['format'] = 'tree';
	         	    		repositoryTree.addData(parms);
            				addItemWindow.hide();
            			}
            		}
        	    }
        	]        	
        })
    ]
});
isc.Window.create({ID:"addCollectionWin",title:"Add Collection",width:500,height:190,autoCenter:true,isModal:true,showModalMask:true,autoDraw:false,
    items: [
        isc.DynamicForm.create({ID:"addResourceWin2Form",width:100,numCols:"2",cellPadding:7,autoDraw:false,
         	fields: [
         	    {name:"title",title:"Title",required:true,type:"textArea",width:420},
         	    {name:"submit",title:"Save",type:"button",
         	    	click:function () {
         	    		if(addResourceWin2Form.validate()) {
         	    			var repo = repositoryTree.getSelectedRecord().id;
         	    			var title = addResourceWin2Form.getValue('title');
         	    			repositoryTree.addData({repo:repo,title:title},function(dsResponse, data, dsRequest) {
         	    				//isc.XMLTools.loadXML("fetch.xml?_dataSource=resources&repo="+repo, "updateEntityData(xmlDoc, xmlText)");
         	    				addCollectionWin.hide();
         	    			});
         	    		}
         	    	}
         	    }
           	]
        })
    ]
});
isc.Window.create({ID:"importWindow",title:"Import Collection",width:1020,height:630,autoCenter:true,isModal:true,showModalMask:true,autoDraw:false,
    items: [
        getImportApplication()
    ]
});
