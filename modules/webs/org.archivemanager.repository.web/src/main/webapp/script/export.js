isc.Window.create({ID:"exportWindow",title:"Export Collection",width:1020,height:630,autoCenter:true,isModal:true,showModalMask:true,autoDraw:false,
	show : function() { 
		if(repositoryTree.anySelected()) {
			var record = repositoryTree.getSelectedRecord();
			if(record.qname == '{openapps.org_repository_1.0}collection') {
				var id = repositoryTree.getSelectedRecord().id;
				exportPanel.setContentsURL('/repository/collection/export/'+id+".pdf");
			}
		}
		this.Super("show", arguments);
	},
	items: [
        isc.VLayout.create({ID:"exportPageLayout",width:1000,height:600,margin:5,membersMargin:5,
		    members:[
		        isc.HTMLFlow.create({ID:"exportPanel",contentsType:"page"})		
		    ]
        })
    ]
});