var aspectNames = ['financial','photographs','miscellaneous','legal','correspondence','research','type','audio','journals','professional','scrapbooks','notebooks','printed_material','artwork','manuscript','memorabilia','medical','video'];
function alphaSearchCollection(query) {
	editCollectionRelationWindowList.fetchData({qname:'{openapps.org_system_1.0}collection',query:query+'*',field:'username',sort:'username'});
}
function getCollectionItemRelationPanel(width,height,search,add,qname) {
	isc.MessagingDataSource.create({ID:"collectionItemRelationshipsDS",fetchDataURL:"/repository/collection.xml",addDataURL:"/core/entity/associate.xml",updateDataURL:"/core/entity/update",removeDataURL:"/core/entity/remove",
		fields:[
			{name:"id", title:"ID", type:"text", primaryKey:true, required:true},
	    	{name:"title", title:"Title", type:"text", length:"128", required:true},
	    	{name:"idx", title:"Index", type:"integer"},
	    	{name:"parent", title:"ParentID", type:"text", required:true, foreignKey:"repositories.id", rootValue:"null"}
		]
	});
	isc.Window.create({ID:"addItemWindow",title:"New Component",width:500,height:205,autoCenter:true,isModal:true,showModalMask:true,autoDraw:false,
	    items: [
	        isc.DynamicForm.create({ID:"addItemWindowForm",width:"100%",numCols:2,colWidths:[25,"*"],cellPadding:5,autoFocus:false,autoDraw:false,
	        	fields:[
	        	    {name:"title", title:"Title", type:"textArea", width:"430",required:true},
	        	    {name:"type", title:"Type", type:"select",valueMap:aspectNames},
	        	    {name: "validateBtn", title: "Save", type: "button", 
	        	    	click: function() {
	            			if(this.form.validate()) {
	            				var rec = repositoryTree.getSelectedRecord();
	            				var cid = rec.id;
	            				var title = addItemWindowForm.getValue('title');
	            				var level = addItemWindowForm.getValue('level');
	            				var type = addItemWindowForm.getValue('type');
	            				collectionItemRelationshipTree.addData({op:'component',pid:cid, level2:type,title:title, level:level});
	            				addItemWindow.hide();
	            			}
	            		}
	        	    }
	        	]        	
	        })
	    ]
	});
	isc.Window.create({ID:"collectionItemDetailWindow",title:"View Collection Item",width:525,height:350,autoCenter:true,isModal:true,showModalMask:true,autoDraw:false,
		items: [
		    isc.HTMLFlow.create({ID:"collectionItemDetailWindowPane",width:"100%",height:"100%"})
	    ]
	});
	isc.Window.create({ID:"editCollectionItemRelationWindow",title:"Add/Update Collection Items",width:480,height:500,autoCenter:true,isModal:true,showModalMask:true,autoDraw:false,
		show:function() {
			if(search) collectionListSearch.show();
    		else collectionListSearch.hide();
			this.Super("show", arguments);
		},
		items: [
		    isc.DynamicForm.create({ID:'collectionListSearch',width:'100%',margin:0,numCols:2,cellPadding:2,visibility:'hidden',
		    	fields:[
		    	    {name:'query',width:'415',type:'text',showTitle:false},
		    	    {name:'validateBtn',title:'search',type:'button',width:50,startRow:false,endRow:false,
		    	    	click:function() {
		    	    		query = collectionListSearch.getValue('query');
		    	    		collectionItemRelationshipTree.fetchData({qname:'{openapps.org_repository_1.0}collection',query:query,field:'name',sort:'name_e'});
		    	    	}
		    	    }
		    	]
		    }),
		    isc.TreeGrid.create({ID:'collectionItemRelationshipTree',dataSource:'collectionItemRelationshipsDS',height:'100%',width:'100%',showHeader:false,animateFolders:true,
		    	canAcceptDroppedRecords:true,canReparentNodes:true,canReorderRecords:true,canDropOnLeaves:true,selectionType:'single',autoDraw:false,
		    	dragDataAction:'move',canDragRecordsOut:true,nodeContextClick:'repositoryTreeMenu.showContextMenu()',
		    	folderDrop:function(dragRecords,dropFolder,index){repositoryDrop(dragRecords, dropFolder, index);},
		    	recordClick:function(viewer,record) {
		    		if(record.localName == 'category' && add) editCollectionItemRelationWindowAdd.show();
		    		else editCollectionItemRelationWindowAdd.hide();
		    		if(record.localName != 'category' && record.localName != 'collection') editCollectionItemRelationWindowSave.show();
		    		else editCollectionItemRelationWindowSave.hide();
		    	}
		    }),
		    isc.HLayout.create({height:'25',width:"100%",align:"right",margin:5,membersMargin:5,
		    	members: [
		    	    isc.Button.create({ID:'editCollectionItemRelationWindowSave',width:60,icon:"/theme/images/icons16/link.png",visibility:"hidden",title:"Link",
		    	    	click:function () {
	 	         	   		if(collectionItemRelationshipTree.anySelected()) {
	 	         	   			var entity_qname = '{openapps.org_repository_1.0}'+collectionItemRelationshipTree.getSelectedRecord().localName;
	 	         	   			var target = collectionItemRelationshipTree.getSelectedRecord().id;
	 	         	   			if(qname) {
	 	         	   				associate(qname, target, entityId, {}, function(xmlDoc, xmlText) {
	 	         	   					updateEntityData(xmlDoc, xmlText);
	 	         	   					editCollectionItemRelationWindow.hide();		         	    				
	 	         	   				});
	 	         	   			} else {
	 	         	   				associate('{openapps.org_repository_1.0}items', entityId, target, {}, function(xmlDoc, xmlText) {
	 	         	   					updateEntityData(xmlDoc, xmlText);
	 	         	   					editCollectionItemRelationWindow.hide();		         	    				
	 	         	   				});
	 	         	   			}
	 	         	   		}
	 	         	   	}
		    	    }),
		    	    isc.Button.create({ID:'editCollectionItemRelationWindowAdd',width:60,icon:"/theme/images/icons16/add.png",visibility:"hidden",title:"add",
		    	    	click:function () {
		    	    		addItemWindow.show();
		    	    	}
		    	    })
		    	]
		    })
	    ]
	});
	return isc.VLayout.create({ID:"collectionItemRelationPanel",autoDraw:false,width:width,
   	 	members:[	        	          
   	 	    isc.ToolStrip.create({width: "100%", height:24,membersMargin:5,
   	    	 members: [
   	    	     isc.HTMLFlow.create({width:"100%",contents:"<span style='font-weight:bold;font-size:14px;'>Collection Items</span>"}),
   	    	     isc.Button.create({width:60,height:20,icon:"/theme/images/icons16/add.png",title:"add",
   	    	    	 click:function() {
   	    	    		 editCollectionItemRelationWindow.show();
   	    	    	 }
   	 	    	 }),
   	 	    	isc.Button.create({ID:"collectionItemRelationPanelAddButton",width:60,height:20,icon:"/theme/images/icons16/edit.png",title:"view",visibility:"hidden",
  	    	    	click:function() {
  	    	    		collectionItemDetailWindowPane.setContentsURL('/repository/item/'+collectionItemRelationList.getSelectedRecord().target_id+".html");
  	    	    		collectionItemDetailWindow.show();
  	    	    	}
  	 	    	}),
   	    	    isc.Button.create({width:75,height:20,icon:"/theme/images/icons16/remove.png",title:"delete",click:function() {
   	    	    	parms = {};
   	    	    	parms['id'] = collectionItemRelationList.getSelectedRecord().id;
   	    	    	parms['target'] = collectionItemRelationList.getSelectedRecord().target_id;
   	    	    	isc.XMLTools.loadXML("/core/association/remove", function(xmlDoc, xmlText) {
   	    	    		collectionItemRelationList.removeSelectedData();
   	    	    	},{httpMethod:"POST",params:parms});	    		        	        	
   	    	    }})
   	    	]
   	 	    }),
   	 	    isc.ListGrid.create({ID:'collectionItemRelationList',autoFitData:"vertical",autoFitMaxHeight:200,width:'100%',height:60,width:'100%',alternateRecordStyles:true,autoDraw:false,autoFetchData:false,
   	 	    	dateFormatter:'toLocaleString',
   	 	    	fields:[
   	 	    	    {name:'title',title:'Title',width:'*',align:'left'},
   	 	    	    {name:'container',title:'Container',width:'150',align:'left'}
	            ],
	            recordClick:function(viewer,record) {
	            	collectionItemRelationPanelAddButton.show();
	            }
   	 	    })
   	 	]
    });
}
function getContainerRelationPanel(relationshipMap,width,height) {
	isc.MessagingDataSource.create({ID:'containerRelationshipsDS',fetchDataURL:'/core/entity/search',updateDataURL:'/core/entity/update',fields:[{name:'id',title:'ID',type:'text',primaryKey:true,required:true}]});
	isc.Window.create({ID:"editContainerRelationWindow",title:"Add/Update User",width:480,height:350,autoCenter:true,isModal:true,showModalMask:true,autoDraw:false,
		items: [
		    isc.ListGrid.create({ID:'editContainerRelationWindowList',dataSource:'userRelationshipsDS',height:'100%',width:'100%',margin:5,alternateRecordStyles:true,autoDraw:false,autoFetchData:false,
		    	fields:[
		    	    {name:'username',title:'Username',width:'*',align:'left'}
		    	]
		    }),
		    getAlphaSearchStrip('User',2),
	        isc.DynamicForm.create({ID:"editContainerRelationWindowForm",height:'30',width:"100%",numCols:"3",cellPadding:5,autoDraw:false,colWidths:[100,'*'],
	        	fields: [
	        	    {name:'role',title:'Role',type:'select',width:'200',align:'left',valueMap:relationshipMap},
		        	{name:"submit",title:"Save",type:"button",startRow:false,endRow:false,
	 	         	   	click:function () {
	 	         	   		if(editUserRelationWindowList.anySelected()) {
	 	         	   			var parms = {};
	 	         	   			parms['entity_qname'] = '{openapps.org_system_1.0}user';
	 	         	   			parms['assoc_qname'] = '{openapps.org_system_1.0}users';
	 	         	   			parms['source'] = entityId;
	 	         	   			parms['target'] = editUserRelationWindowList.getSelectedRecord().id;
	 	         	   			parms['role'] = editUserRelationWindowForm.getValue('role'); 
	 	         	   			isc.XMLTools.loadXML("/core/association/add", function(xmlDoc, xmlText) {
	 	         	   				updateEntityData(xmlDoc, xmlText);
	 	         	   				editUserRelationWindowForm.clearValues();
	 	         	   				editUserRelationWindow.hide();
	 	         	   			},{httpMethod:"POST",params:parms});
	 	         	   		}
	 	         	   	}
		        	}
	        	]
	        })
	    ]
	});
	return isc.VLayout.create({ID:"userRelationPanel",autoDraw:false,width:width,height:height,
   	 	members:[	        	          
   	 	    isc.ToolStrip.create({width: "100%", height:24,membersMargin:5,
   	    	 members: [
   	    	     isc.HTMLFlow.create({width:"100%",contents:"<span style='font-weight:bold;font-size:14px;'>Users</span>"}),
   	    	     isc.Button.create({width:60,height:20,icon:"/theme/images/icons16/add.png",title:"add",click:"editUserRelationWindow.show()"}),
   	    	     isc.Button.create({width:75,height:20,icon:"/theme/images/icons16/remove.png",title:"delete",click:function() {
   	    	    	 parms = {};
   	    	    	 parms['id'] = userRelationList.getSelectedRecord().id;
   	    	    	 parms['target'] = userRelationList.getSelectedRecord().target_id;
   	    	    	 isc.XMLTools.loadXML("/core/association/remove/"+userRelationList.getSelectedRecord().id, function(xmlDoc, xmlText) {
   	    	    		userRelationList.removeSelectedData();
   	    	    	 },{httpMethod:"POST",params:parms});	    		        	        	
   	    	     }})
   	    	 ]
   	 	    }),
   	 	    isc.ListGrid.create({ID:'userRelationList',height:'100%',width:'100%',alternateRecordStyles:true,autoDraw:false,autoFetchData:false,
   	 	    	dateFormatter:'toLocaleString',
   	 	    	fields:[
   	 	    	    {name:'username',title:'Username',width:'*',align:'left'},
   	    	        {name:'role',title:'Role',type:'select',width:'150',align:'left',valueMap:{}},
	            ]
   	 	    })
   	 	]
    });
}
function getCollectionRelationPanel(qname, width,height) {
	isc.MessagingDataSource.create({ID:'collectionRelationshipsDS',fetchDataURL:'/core/entity/search',updateDataURL:'/core/entity/update',fields:[{name:'id',title:'ID',type:'text',primaryKey:true,required:true}]});
	isc.Window.create({ID:"editCollectionRelationWindow",title:"Add/Update Collection",width:480,height:350,autoCenter:true,isModal:true,showModalMask:true,autoDraw:false,
		items: [
		    isc.DynamicForm.create({ID:'collectionRelationListSearch',width:'100%',margin:2,numCols:2,cellPadding:2,
		    	fields:[
		    	    {name:'query',width:'410',type:'text',showTitle:false},
		    	    {name:'validateBtn',title:'search',type:'button',width:50,startRow:false,endRow:false,
		    	    	click:function() {
		    	    		editCollectionRelationWindowList.fetchData({qname:'{openapps.org_repository_1.0}collection',query:collectionRelationListSearch.getValue('query'),field:'name',sort:'name_e'});
		    	    	}
		    	    }
		    	]
		    }),
		    isc.ListGrid.create({ID:'editCollectionRelationWindowList',dataSource:'collectionRelationshipsDS',height:'100%',width:'100%',margin:5,alternateRecordStyles:true,autoDraw:false,autoFetchData:false,
		    	fields:[
		    	    {name:'title',title:'Name',width:'*',align:'left'}
		    	]
		    }),
	        isc.DynamicForm.create({ID:"editCollectionRelationWindowForm",height:'30',width:"100%",numCols:"3",cellPadding:5,autoDraw:false,colWidths:[100,'*'],
	        	fields: [
	        	    {name:"submit",title:"Link",icon:"/theme/images/icons16/link.png",type:"button",startRow:false,endRow:false,
	 	         	   	click:function () {
	 	         	   		if(editCollectionRelationWindowList.anySelected()) {
	 	         	   			associate(qname, entityId, editCollectionRelationWindowList.getSelectedRecord().id, {}, function(xmlDoc, xmlText) {
	 	         	   				updateEntityData(xmlDoc, xmlText);
	 	         	   				editCollectionRelationWindowForm.clearValues();
	 	         	   				editCollectionRelationWindow.hide();
	 	         	   			});
	 	         	   		}
	 	         	   	}
		        	}
	        	]
	        })
	    ]
	});
	return isc.VLayout.create({ID:"collectionRelationPanel",autoDraw:false,width:width,height:height,
   	 	members:[	        	          
   	 	    isc.ToolStrip.create({width: "100%", height:24,membersMargin:5,
   	    	 members: [
   	    	     isc.HTMLFlow.create({width:"100%",contents:"<span style='font-weight:bold;font-size:14px;'>Collections</span>"}),
   	    	     isc.Button.create({width:60,height:20,icon:"/theme/images/icons16/add.png",title:"add",click:"editCollectionRelationWindow.show()"}),
   	    	     isc.Button.create({width:75,height:20,icon:"/theme/images/icons16/remove.png",title:"delete",click:function() {
   	    	    	 disassociate(collectionRelationList.getSelectedRecord().id, false, function(xmlDoc, xmlText) {
   	    	    		collectionRelationList.removeSelectedData();
  	    	    	 });
   	    	     }})
   	    	 ]
   	 	    }),
   	 	    isc.ListGrid.create({ID:'collectionRelationList',height:'100%',width:'100%',alternateRecordStyles:true,autoDraw:false,autoFetchData:false,
   	 	    	dateFormatter:'toLocaleString',
   	 	    	fields:[
   	 	    	    {name:'name',title:'Name',width:'*',align:'left'}
	            ]
   	 	    })
   	 	]
    });
}
function getAccessionSelectionPanel(qname, width,height) {
	isc.MessagingDataSource.create({ID:"accessionDS",fetchDataURL:"/repository/accession",addDataURL:"/repository/accession/add.xml",updateDataURL:"/core/entity/update.xml",removeDataURL:"/core/entity/remove.xml",
		fields:[
			{name:"id", title:"ID", type:"text", primaryKey:true, required:true},
	    	{name:"title", title:"Title", type:"text", length:"128", required:true},
	    	{name:"idx", title:"Index", type:"integer"},
	    	{name:"parent", title:"ParentID", type:"text", required:true, foreignKey:"repositories.id", rootValue:"0"}
		]
	});
	isc.Window.create({ID:"editAccessionWindow",title:"Add/Update Accessions",width:400,height:170,autoCenter:true,isModal:true,showModalMask:true,autoDraw:false,
		items: [
		    isc.TreeGrid.create({ID:'accessionTree',dataSource:'accessionDS',height:'100%',width:'100%',showHeader:false,animateFolders:true,
		    	selectionType:'single',autoDraw:false,recordClick:'load_accession(record)'
		    }),
		    isc.Button.create({width:60,height:20,icon:"/theme/images/icons16/link.png",title:"link",
		    	click:function () {
	         		if(accessionTree.anySelected()) {
	         			associate(qname, accessionTree.getSelectedRecord().id, entityId, {}, function(xmlDoc, xmlText) {
	         				updateEntityData(xmlDoc, xmlText);
	         				editCollectionRelationWindowForm.clearValues();
	         				editCollectionRelationWindow.hide();
	         			});
	         		}
	         	}
		    })
	    ]
	});
	return isc.VLayout.create({ID:"accessionPanel",autoDraw:false,width:width,height:height,
   	 	members:[	        	          
   	 	    isc.ToolStrip.create({width: "100%", height:24,membersMargin:5,
   	    	 members: [
   	    	     isc.HTMLFlow.create({width:"100%",contents:"<span style='font-weight:bold;font-size:14px;'>Accessions</span>"}),
   	    	     isc.Button.create({width:60,height:20,icon:"/theme/images/icons16/add.png",title:"add",click:"editCollectionWindow.show()"}),
   	    	     isc.Button.create({width:75,height:20,icon:"/theme/images/icons16/remove.png",title:"delete",click:function() {
   	    	    	 parms = {};
   	    	    	 parms['id'] = collectionList.getSelectedRecord().id;
   	    	    	 parms['target'] = collectionList.getSelectedRecord().target_id;
   	    	    	 isc.XMLTools.loadXML("/contacts/remove.xml", function(xmlDoc, xmlText) {
   	    	    		collectionList.removeSelectedData();
   	    	    	 },{httpMethod:"POST",params:parms});	    		        	        	
   	    	     }})
   	    	 ]
   	 	    }),
   	 	    isc.ListGrid.create({ID:'accessionRelationList',height:'100%',width:'100%',alternateRecordStyles:true,autoDraw:false,autoFetchData:false,
	 	    	dateFormatter:'toLocaleString',
	 	    	fields:[
	 	    	    {name:'name',title:'Name',width:'*',align:'left'}
	 	    	]
	 	    })
   	 	]
    });
}