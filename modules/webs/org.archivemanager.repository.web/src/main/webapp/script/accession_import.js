function populateForm(record) {
	//if(record['setCollection'] == 'false') accessionImportForm1.getField('collectionSetter').setDisabled(true);
	//else accessionImportForm1.getField('collectionSetter').setDisabled(false);
	//if(record['setDonor'] == 'false') accessionImportForm1.getField('donorSetter').setDisabled(true);
	//else accessionImportForm1.getField('donorSetter').setDisabled(false);
	//if(record['setAddress'] == 'false') accessionImportForm1.getField('addressSetter').setDisabled(true);
	//else accessionImportForm1.getField('addressSetter').setDisabled(false);
	accessionImportForm1.editRecord(record);
}
function saveImport() {
	isc.XMLTools.loadXML("import/add", function(xmlDoc, xmlText) {
  		 
  	});	  
}
function upload() {
	uploadForm.submit();
}
function getImportApplication() {
	isc.RestDataSource.create({ID:"treeData",fetchDataURL:"import/fetch.xml",addDataURL:"import/add.xml",
	fields:[
		{name:"id", title:"ID", type:"text", primaryKey:true, required:true},
    	{name:"title", title:"Title", type:"text", length:"128", required:true},
    	{name:"parent", title:"ParentID", type:"text", required:true, foreignKey:"treeData.id", rootValue:"null"}
	]	
	});
	isc.DataSource.create({ID:"node"});
	isc.DataSource.create({ID:"properties"});
	isc.DataSource.create({ID:"notes"});
	isc.DataSource.create({ID:"names"});
	isc.DataSource.create({ID:"subjects"});
	isc.DataSource.create({ID:"containers"});
	isc.RestDataSource.create({ID:"repositories",fetchDataURL:"fetch.xml"});

	isc.ListGrid.create({ID:"importTree",dataSource:"treeData",width:350,height:"100%",autoFetchData:false,showHeader:true,
		showResizeBar:true,leaveScrollbarGap:false,animateFolders:true,selectionType:"single",autoDraw:false,
		recordClick:"populateForm(record)",
		fields:[
		    {name:"title", title:"Title"}
		]
	});
	//Main Body
	isc.TabSet.create({ID:"tabSet",tabBarPosition:"top",width:"60%",height:"100%",autoDraw:false,
    tabs: [
        {title: "Properties", pane:
        	isc.HLayout.create({ID:"importLayout",width:"100%",height:"100%",
        		members:[
        	       	isc.DynamicForm.create({ID:'accessionImportForm1',width:'100%',height:'50%',margin:'10',cellPadding:5,numCols:'4',colWidths:[115,'*'],visibility:'visible',autoFocus:false,
        	       		fields:[
        	       		    {name:'date',width:'*',colSpan:'1',type:'staticText',title:'Date'},
        	       		    {type:'spacer'},	
        	       		    {name:'cost',width:'125',type:'staticText',title:'Cost'},
        	       		    {name:'aquisition_type',width:'*',type:'staticText',width:'100',title:'Acquisition Type'},
        	       		    {name:'extent_number',width:'*',type:'staticText',title:'Extent Number'},
        	       		    {name:'extent_type',width:'*',type:'staticText',width:'100',title:'Quantity Type'},
        	       		    {name:'pagebox_quantity',width:'*',type:'staticText',width:75,title:'PageBox Quantity'},
        	       		    {name:'estimated_packages',width:'*',type:'staticText',width:75,title:'Estimated Packages'},
        	       		    {name:'linear_feet',width:'*',colSpan:'1',type:'staticText',width:75,title:'Linear Feet'},
        	       		    {type:'rowSpacer'},
        	       		    {name:'general_note',width:'500',type:'staticText',showTitle:true,controlGroups:['styleControls'],colSpan:'4',height:'75',title:'General Note'},
        	       		    {name:'restriction',type:'staticText',width:'*',colSpan:'4',title:'Restriction'},
        	       		    {name:'donorTitle',type:'staticText',width:'*',colSpan:'2',title:'Donor'},
        	       		    {name:'addressTitle',type:'staticText',width:'*',colSpan:'2',title:'Address'},
        	       		    {type:'rowSpacer'},
        	       		    {name:'collectionTitle',type:'staticText',width:'*',colSpan:'2',title:'Collection Title'},
        	       		    {name:'collection',type:'staticText',width:'*',colSpan:'2',title:'Collection'}//,
        	       		    //{name:'collectionSetter',type:'button',title:'set collection',startRow:false,endRow:false,click:'findCollectionWin.show()'}        	       		    
        	       		]
        	       	})
        	    ]
        	})
        }
    ]
	});	
	isc.HLayout.create({ID:"menuLayout",width:"100%",height:35,border:'1px solid #C0C3C7',defaultLayoutAlign:"center",membersMargin:5,
		members:[
		    UploadForm.create({ID:"uploadForm",action:"import/upload.xml",numCols:4,cellPadding:5,width:"350",
		    	fields: [
		    	    {name: "mode",title:"Processor",width:"250",type:"select",valueMap:modesData},
		    	    {name: "file",title:"File",type:"UploadItem"}
		    	]
		    }),
		    isc.IButton.create({ID:"submitButton",title:"Process",click:"upload();"}),
		    isc.IButton.create({ID:"saveButton",title:"Save",click:"saveImport();"})
		]
	});
	isc.HLayout.create({ID:"pageLayout1",width:"100%",height:"100%",members:[importTree,tabSet]});
	return isc.VLayout.create({ID:"importPageLayout",width:1000,height:600,margin:5,membersMargin:5,members:[menuLayout,pageLayout1]});
}
isc.Window.create({ID:"findCollectionWin",title:"Find Collection",width:400,height:100,autoCenter:true,isModal:false,showModalMask:false,autoDraw:false,
    items: [
        isc.DynamicForm.create({ID:"findCollectionWin2Form",numCols:"2",cellPadding:5,autoDraw:false,colWidths:[100,"*"],
         	fields: [
         	    {name:"date", title:"Date", type:"date",displayFormat:"toUSShortDate",required:true},
         	    //{name:"collectee", title:"Collectee", type:"text",width:"*",required:true},
         	    {name:"submit",title:"Save",type:"button",
         	    	click:function () {
         	    		
         	    	}
         	    }
           	]
        })
    ]
});
