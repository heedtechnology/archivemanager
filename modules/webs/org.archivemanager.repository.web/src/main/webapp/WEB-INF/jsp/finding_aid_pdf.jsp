<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Collections" %>
<%@ page import="com.itextpdf.text.Document" %>
<%@ page import="com.itextpdf.text.Paragraph" %>
<%@ page import="com.itextpdf.text.Element" %>
<%@ page import="org.heed.openapps.entity.EntityService" %>
<%@ page import="org.archivemanager.repository.data.ContainerSorter" %>
<%@ page import="org.heed.openapps.RepositoryModel" %>
<%@ page import="org.heed.openapps.SystemModel" %>
<%@ page import="org.heed.openapps.util.HTMLUtility" %>
<%@ page import="org.heed.openapps.util.NumberUtility" %>
<%@ page import="org.heed.openapps.entity.Entity" %>
<%@ page import="org.heed.openapps.entity.Association" %>
<%@ page import="org.archivemanager.repository.server.FindingAidUtil" %>
<%
int width = 1200;

EntityService entityService = (EntityService)request.getAttribute("entityService");
ContainerSorter containerSorter = (ContainerSorter)request.getAttribute("containerSorter");
Entity collection = (Entity)request.getAttribute("collection");
FindingAidUtil util = new FindingAidUtil();
Document document = (Document)request.getAttribute("document");

String primarContainer = "";
String secondaryContainer = "";

boolean printLevel2 = false;
boolean printLevel3 = false;
boolean printLevel4 = false;

document.open();

Paragraph title = new Paragraph(HTMLUtility.removeTags(collection.getPropertyValue(SystemModel.OPENAPPS_NAME)));
title.setAlignment(Element.ALIGN_CENTER);
document.add(title);
Paragraph identifier = new Paragraph("#"+HTMLUtility.removeTags(collection.getPropertyValue(RepositoryModel.COLLECTION_IDENTIFIER)));
identifier.setAlignment(Element.ALIGN_CENTER);
document.add(identifier);
Paragraph accession_date = new Paragraph(HTMLUtility.removeTags(collection.getPropertyValue(RepositoryModel.ACCESSION_DATE)));
accession_date.setAlignment(Element.ALIGN_CENTER);
document.add(accession_date);
Paragraph label1 = new Paragraph("Preliminary Listing");
label1.setAlignment(Element.ALIGN_CENTER);
document.add(label1);

entityService.hydrate(collection);
List<Association> level1Children = collection.getSourceAssociations(RepositoryModel.CATEGORIES, RepositoryModel.ITEMS);
Collections.sort(level1Children, containerSorter);
for(int i=0; i < level1Children.size(); i++) {
	Association assoc1 = level1Children.get(i);
	Entity level1 = assoc1.getTargetEntity();	
	String level1Summary = level1.getPropertyValue(RepositoryModel.SUMMARY);
	if(level1Summary != null && level1Summary.length() > 0) level1Summary = "["+level1Summary+"]";
	else level1Summary = "";
	String container1 = level1.getPropertyValue(RepositoryModel.CONTAINER);
	String[] containers1 = container1.split(",");
	String c1 = containers1[0] != null && containers1[0].length() > 0 ? containers1[0] : "";

	//

	if(printLevel2) {
	entityService.hydrate(level1);
	List<Association> level2Children = level1.getSourceAssociations(RepositoryModel.CATEGORIES, RepositoryModel.ITEMS);
	Collections.sort(level2Children, containerSorter);
	for(int j=0; j < level2Children.size(); j++) {
		Association assoc2 = level2Children.get(j);
		Entity level2 = assoc2.getTargetEntity();		
		String level2Summary = level2.getPropertyValue(RepositoryModel.SUMMARY);
		if(level2Summary != null && level2Summary.length() > 0) level2Summary = "["+level2Summary+"]";
		else level2Summary = "";
		String container2 = level2.getPropertyValue(RepositoryModel.CONTAINER);
		String[] containers2 = container2.split(",");
		String c2 = containers2[0] != null && containers2[0].length() > 0 ? containers2[0] : "&nbsp;";
		String c22 = "";
		if(containers2.length == 4) {
			if(containers2[2].equals("folder")) c22 = "[F. "+containers2[3]+"]";
			if(containers2[2].equals("package")) c22 = "[P. "+containers2[3]+"]";
		}
		if(c2.equals(primarContainer)) c2 = "";
		else primarContainer = c2;
	
		//
		
		if(printLevel3) {
		entityService.hydrate(level2);
		List<Association> level3Children = level2.getSourceAssociations(RepositoryModel.CATEGORIES, RepositoryModel.ITEMS);
		Collections.sort(level3Children, containerSorter);
		for(int k=0; k < level3Children.size(); k++) {
			Association assoc3 = level3Children.get(k);
			Entity level3 = assoc3.getTargetEntity();			
			String level3Summary = level3.getPropertyValue(RepositoryModel.SUMMARY);
			if(level3Summary != null && level3Summary.length() > 0) level3Summary = "["+level3Summary+"]";
			else level3Summary = "";
			String container3 = level3.getPropertyValue(RepositoryModel.CONTAINER);
			String[] containers3 = container3.trim().replace("  ", " ").split(" ");
			String c3 = containers3[0] != null && containers3[0].length() > 0 ? containers3[0] : "&nbsp;";
			String c33 = "";
			if(containers3.length == 4) {
				if(containers3[2].equals("Folder")) c33 = "[F. "+containers3[3]+"]";
				if(containers3[2].equals("Package")) c33 = "[P. "+containers3[3]+"]";
			}
			if(c33.equals(secondaryContainer)) c33 = "";
			else secondaryContainer = c33;
		
			//
			
			if(printLevel4) {
			entityService.hydrate(level3);
			List<Association> level4Children = level3.getSourceAssociations(RepositoryModel.CATEGORIES, RepositoryModel.ITEMS);
			Collections.sort(level4Children, containerSorter);
			for(int l=0; l < level4Children.size(); l++) {
				Association assoc4 = level4Children.get(l);
				Entity level4 = assoc4.getTargetEntity();
				
				String level4Summary = level4.getPropertyValue(RepositoryModel.SUMMARY);
				if(level4Summary != null && level4Summary.length() > 0) level4Summary = "["+level4Summary+"]";
				else level4Summary = "";
				String container4 = level4.getPropertyValue(RepositoryModel.CONTAINER);
				String[] containers4 = container4.trim().replace("  ", " ").split(" ");
				String c4 = containers4[0] != null && containers4[0].length() > 0 ? containers4[0] : "&nbsp;";
				String c44 = "";
				if(containers4.length == 4) {
					if(containers4[2].equals("folder")) c44 = "[F. "+containers4[3]+"]";
					if(containers4[2].equals("package")) c44 = "[P. "+containers4[3]+"]";
				}
				if(c44.equals(secondaryContainer)) c44 = "";
				else secondaryContainer = c44;
				
				//
					
				entityService.hydrate(level4);
				List<Association> level5Children = level4.getSourceAssociations(RepositoryModel.CATEGORIES, RepositoryModel.ITEMS);
				Collections.sort(level5Children, containerSorter);
				for(int m=0; m < level5Children.size(); m++) {
					Association assoc5 = level5Children.get(m);
					Entity level5 = assoc5.getTargetEntity();
				
					String level5Summary = level5.getPropertyValue(RepositoryModel.SUMMARY);
					if(level5Summary != null && level5Summary.length() > 0) level5Summary = "["+level5Summary+"]";
					else level5Summary = "";
					String container5 = level5.getPropertyValue(RepositoryModel.CONTAINER);
					String[] containers5 = container5.trim().replace("  ", " ").split(" ");
					String c5 = containers5[0] != null && containers5[0].length() > 0 ? containers5[0] : "&nbsp;";
					String c55 = "";
					if(containers5.length == 4) {
						if(containers5[2].equals("folder")) c55 = "[F. "+containers5[3]+"]";
						if(containers5[2].equals("package")) c55 = "[P. "+containers5[3]+"]";
					}
					if(c55.equals(secondaryContainer)) c55 = "";
					else secondaryContainer = c55;
					
					//
					
				}
			}}
		}}
	}}
}
document.close();
%>