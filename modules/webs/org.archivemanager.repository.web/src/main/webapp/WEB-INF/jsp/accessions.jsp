<script src="/contacts/script/contact_library.js"></script>
<script src="/repository/script/repository_library.js"></script>
<script src="/repository/script/accession_import.js"></script>
<script type="text/javascript">
	var modesData = ${modes};
</script>
<div class="body_container">
	<table cellspacing="0" cellpadding="0" style="width:100%;">
		<tr><td class="top-left"></td><td class="top-mid"></td><td class="top-right"></td></tr>
		<tr>
			<td class="mid-left"></td>
			<td class="mid-mid">
				<div style="width:100%">
					<div><script src="../script/accessions.js"></script></div>
        		</div> 
			</td>
			<td class="mid-right"></td>
		</tr>
		<tr><td class="bot-left"></td><td class="bot-mid"></td><td class="bot-right"></td></tr>
	</table>
</div>