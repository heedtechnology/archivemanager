/*
 * Copyright (C) 2011 Heed Technology Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package org.archivemanager.core.web.controller;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.lucene.index.Term;
import org.apache.lucene.search.PrefixQuery;
import org.archivemanager.Individual;
import org.archivemanager.Organization;
import org.archivemanager.Collection;
import org.heed.openapps.dictionary.ClassificationModel;
import org.heed.openapps.dictionary.ContactModel;
import org.heed.openapps.dictionary.ContentModel;
import org.heed.openapps.QName;
import org.heed.openapps.dictionary.RepositoryModel;
import org.heed.openapps.SystemModel;
import org.heed.opensearch.search.SearchRequest;
import org.heed.opensearch.search.SearchService;
import org.heed.openapps.data.Sort;
import org.heed.openapps.security.SecurityService;
import org.heed.openapps.security.User;
import org.heed.openapps.theme.ThemeVariables;
import org.heed.openapps.util.IDTypeName;
import org.heed.openapps.entity.Association;
import org.heed.openapps.entity.BaseEntityQuery;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityQuery;
import org.heed.openapps.entity.EntityResultSet;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.entity.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;


@Controller
public class DashboardController {
	@Autowired protected EntityService entityService;
	@Autowired protected SecurityService securityService;
	@Autowired protected SearchService searchService;
	
	
	@RequestMapping(value="", method = RequestMethod.GET)
	public ModelAndView dashboard(HttpServletRequest req, HttpServletResponse res) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		ThemeVariables vars = new ThemeVariables("dashboard");
		parms.put("theme", vars);
		User user = securityService.currentUser();
		
		//EntityQuery query = new EntityQuery(RepositoryModel.ITEM);
		//EntityResultSet items = entityService.search(query);
		//parms.put("itemCount", String.format("%, d", items.getResultSize()));
		if(!user.isGuest()) return new ModelAndView("dashboard", parms);
		return new ModelAndView(new RedirectView("/login"));//new ModelAndView("dashboard_guest", parms);
	}
	@ResponseBody
	@RequestMapping(value="/dashboard/search", method = RequestMethod.GET)
	public Map<String, Object> search(HttpServletRequest req, HttpServletResponse res,
			@RequestParam("query") String query) throws Exception {		
		List<IDTypeName> entries = new ArrayList<IDTypeName>();
		PrefixQuery luceneQuery = new PrefixQuery(new Term("name", query));
		/*
		EntityQuery collectionQuery = new EntityQuery(RepositoryModel.COLLECTION);
		collectionQuery.setLuceneQuery(luceneQuery);
		collectionQuery.setEndRow(1000);
		EntityResultSet collectionResult = entityService.search(collectionQuery);
		
		for(Entity result : collectionResult.getResults()) {
			String name = result.getPropertyValue(SystemModel.NAME);
			if(name != null && name.toLowerCase().startsWith(query.toLowerCase())) {
				entries.add(new IDName(String.valueOf(result.getId()), name));
			}
		}
		*/
		EntityQuery donorQuery = new BaseEntityQuery(ContactModel.INDIVIDUAL);
		donorQuery.setNativeQuery(luceneQuery);
		donorQuery.setEndRow(1000);
		EntityResultSet donorResult = entityService.search(donorQuery);		
		
		for(Entity result : donorResult.getResults()) {
			String name = result.getPropertyValue(SystemModel.NAME);
			if(name != null && name.toLowerCase().startsWith(query.toLowerCase())) {
				entries.add(new IDTypeName(String.valueOf(result.getId()), ContactModel.INDIVIDUAL.getLocalName(), name));
			}
		}
		Collections.sort(entries);
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("query", query);
		List<Map<String, String>> suggestions = new ArrayList<Map<String, String>>();
		for(IDTypeName entry : entries) {
			Map<String,String> entryMap = new HashMap<String, String>();
			entryMap.put("data", entry.getId());
			entryMap.put("value", entry.getName());
			entryMap.put("type", entry.getType());
			suggestions.add(entryMap);
		}
		map.put("suggestions", suggestions);
		return map;
	}
	
	@RequestMapping(value="/dashboard/detail/{id}", method = RequestMethod.GET)
	public ModelAndView detail(HttpServletRequest req, HttpServletResponse res, @PathVariable("id") Long id) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		User user = securityService.currentUser();
		//if(!user.isGuest()) return new ModelAndView(new RedirectView("/login"));
		
		Entity entity = entityService.getEntity(id);
		parms.put("entity", entity);
		if(entity.getQName().equals(ContactModel.INDIVIDUAL)) {
			Individual individual = new Individual(entity);
			parms.put("individual", individual);
		} else if(entity.getQName().equals(ContactModel.ORGANIZATION)) {
			Organization organization = new Organization();
			parms.put("organization", organization);
		} else if(entity.getQName().equals(RepositoryModel.COLLECTION)) {
			Collection collection = new Collection(entity);
			parms.put("collection", collection);
		}
		return new ModelAndView("detail", parms);
	}
	
	/*
	@RequestMapping(value="/dashboard/{query}", method = RequestMethod.GET)
	public ModelAndView dashboard(HttpServletRequest req, HttpServletResponse res,
			@PathVariable("query") String query) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		ThemeVariables vars = new ThemeVariables("default");
		parms.put("theme", vars);
		User user = securityService.currentUser();
		parms.put("query", query);
		
		SearchRequest searchRequest = getSearchRequest(RepositoryModel.COLLECTION, 0L, query, 0, 20, "name_e", false);
		SearchResponse result = searchService.search(searchRequest);
		System.out.println(result.getQueryExplanation());
		parms.put("collections", result.getResults());
		
		searchRequest = getSearchRequest(ClassificationModel.PERSON, 0L, query, 0, 20, "name_e", false);
		result = searchService.search(searchRequest);
		parms.put("people", result.getResults());
		
		searchRequest = getSearchRequest(ClassificationModel.CORPORATION, 0L, query, 0, 20, "name_e", false);
		result = searchService.search(searchRequest);
		parms.put("corporations", result.getResults());
		
		//if(user != null && user.hasRole("Kiosk")) return new ModelAndView("kiosk", parms);
		return new ModelAndView("results", parms);
	}
	*/
	@RequestMapping(value="/browse", method = RequestMethod.GET)
	public ModelAndView browse(HttpServletRequest req, HttpServletResponse res) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		ThemeVariables vars = new ThemeVariables("default");
		parms.put("theme", vars);
		User user = securityService.currentUser();
		EntityQuery query = new BaseEntityQuery(RepositoryModel.COLLECTION, "public", "true", "name_e", true);
		EntityResultSet results = entityService.search(query);
		parms.put("entities", results.getResults());
		
		return new ModelAndView("browse", parms);
	}
	@RequestMapping(value="/browse/{id}", method = RequestMethod.GET)
	public ModelAndView browse(HttpServletRequest req, HttpServletResponse res, @PathVariable("id") Long id) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		ThemeVariables vars = new ThemeVariables("default");
		parms.put("theme", vars);
		User user = securityService.currentUser();
		Entity entity = entityService.getEntity(id);
		parms.put("entity", entity);
		List<Association> parents = entity.getTargetAssociations(RepositoryModel.COLLECTIONS, RepositoryModel.CATEGORIES, RepositoryModel.ITEMS);
		List<Entity> parentEntities = new ArrayList<Entity>();
		while(parents.size() > 0) {
			List<Association> list = new ArrayList<Association>();
			for(Association child : parents) {
				Entity e = entityService.getEntity(child.getSource());
				parentEntities.add(e);
				list.addAll(e.getTargetAssociations(RepositoryModel.COLLECTIONS, RepositoryModel.CATEGORIES, RepositoryModel.ITEMS));
			}
			parents = list;
		}
		Collections.reverse(parentEntities);
		parms.put("parents", parentEntities);
		List<Association> children = entity.getSourceAssociations(RepositoryModel.COLLECTIONS, RepositoryModel.CATEGORIES, RepositoryModel.ITEMS);
		List<Entity> entities = new ArrayList<Entity>();
		for(Association child : children) {
			entities.add(entityService.getEntity(child.getTarget()));
		}
		parms.put("entities", entities);
		List<Association> note_assocs = entity.getSourceAssociations(SystemModel.NOTES);
		List<Entity> notes = new ArrayList<Entity>();
		for(Association assoc : note_assocs) {
			notes.add(entityService.getEntity(assoc.getTarget()));
		}
		parms.put("notes", notes);
		
		List<Association> name_assocs = entity.getSourceAssociations(ClassificationModel.NAMED_ENTITIES);
		List<Entity> names = new ArrayList<Entity>();
		for(Association assoc : name_assocs) {
			names.add(entityService.getEntity(assoc.getTarget()));
		}
		parms.put("names", names);
		
		List<Association> subj_assocs = entity.getSourceAssociations(ClassificationModel.SUBJECTS);
		List<Entity> subjects = new ArrayList<Entity>();
		for(Association assoc : subj_assocs) {
			subjects.add(entityService.getEntity(assoc.getTarget()));
		}
		parms.put("subjects", subjects);
				
		return new ModelAndView("browse", parms);
	}
	@RequestMapping(value="/names", method = RequestMethod.GET)
	public ModelAndView names(HttpServletRequest req, HttpServletResponse res) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		ThemeVariables vars = new ThemeVariables("default");
		parms.put("theme", vars);
		User user = securityService.currentUser();
		
		
		return new ModelAndView("names", parms);
	}
	@RequestMapping(value="/names/{category}", method = RequestMethod.GET)
	public ModelAndView namesByCategory(HttpServletRequest req, HttpServletResponse res, @PathVariable("category") String category) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		ThemeVariables vars = new ThemeVariables("default");
		parms.put("theme", vars);
		User user = securityService.currentUser();
		
		EntityQuery personQuery = new BaseEntityQuery(ClassificationModel.NAMED_ENTITY, "name", category, "name_e", true);
		//personQuery.setDefaultOperator("AND");
		personQuery.setEndRow(5000);
		EntityResultSet people = entityService.search(personQuery);
		List<Entity> list = new ArrayList<Entity>();
		for(Entity e : people.getResults()) {
			String name = e.getPropertyValue(SystemModel.NAME);
			if(name != null && name.startsWith(category))
				list.add(e);
		}
		parms.put("names", list);
		
		return new ModelAndView("names", parms);
	}
	@RequestMapping(value="/subjects", method = RequestMethod.GET)
	public ModelAndView subjects(HttpServletRequest req, HttpServletResponse res) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		ThemeVariables vars = new ThemeVariables("default");
		parms.put("theme", vars);
		User user = securityService.currentUser();
		
		
		return new ModelAndView("subjects", parms);
	}
	@RequestMapping(value="/subjects/{category}", method = RequestMethod.GET)
	public ModelAndView subjectsByCategory(HttpServletRequest req, HttpServletResponse res, @PathVariable("category") String category) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		ThemeVariables vars = new ThemeVariables("default");
		parms.put("theme", vars);
		User user = securityService.currentUser();
		
		EntityQuery personQuery = new BaseEntityQuery(ClassificationModel.SUBJECT, "name", category, "name_e", true);
		//personQuery.setDefaultOperator("AND");
		personQuery.setEndRow(5000);
		EntityResultSet people = entityService.search(personQuery);
		List<Entity> list = new ArrayList<Entity>();
		for(Entity e : people.getResults()) {
			String name = e.getPropertyValue(SystemModel.NAME);
			if(name != null && name.startsWith(category))
				list.add(e);
		}
		parms.put("subjects", list);
		
		return new ModelAndView("subjects", parms);
	}
	@RequestMapping(value="/av", method = RequestMethod.GET)
	public ModelAndView audioVideo(HttpServletRequest req, HttpServletResponse res) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		ThemeVariables vars = new ThemeVariables("default");
		parms.put("theme", vars);
		User user = securityService.currentUser();
		
		//if(user != null && user.hasRole("Kiosk")) return new ModelAndView("kiosk", parms);
		return new ModelAndView("audio_video", parms);
	}
	@RequestMapping(value="/dashboard/view/{id}", method = RequestMethod.GET)
	public ModelAndView view(HttpServletRequest req, HttpServletResponse res,
			@PathVariable("id") Long id) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		ThemeVariables vars = new ThemeVariables("default");
		parms.put("theme", vars);
		User user = securityService.currentUser();
		Entity entity = entityService.getEntity(id);
		for(Association assoc : entity.getSourceAssociations()) {
			assoc.setSortField(SystemModel.NAME);
			if(assoc.hasProperty(ContentModel.RELATIONSHIP_TYPE)) {
				Property type = assoc.getProperty(ContentModel.RELATIONSHIP_TYPE);
				if(type.getValue().equals("avatar")) {
					//Transformation transformation = new Transformation(String.valueOf(1), Mimetypes.MIMETYPE_IMAGE_PNG, 400, 400);
					//ByteArrayOutputStream sos = new ByteArrayOutputStream();
					//contentService.stream(String.valueOf(assoc.getTarget()), transformation, sos);
					//parms.put("avatar", sos.toString());
				}
			}
		}
		//entityService.hydrate(entity);
		parms.put("entity", entity);
		if(entity.getQName().equals(RepositoryModel.COLLECTION)) return new ModelAndView("view_collection", parms);
		else return new ModelAndView("view", parms);
	}
	protected SearchRequest getSearchRequest(QName qname, long start, String query, int page, int size, String sort, boolean attributes) {
		SearchRequest sQuery = new SearchRequest(qname, query);
		sQuery.setAttributes(attributes);
		sQuery.setPage(page);
		sQuery.setPageSize(size);
		if(sort != null) {
			Sort lSort = null;
			String[] s = sort.split("_");
			if(s.length == 2) {
				boolean reverse = s[1].equals("a") ? true : false;
				if(s[0].equals("date")) lSort = new Sort(Sort.DATE, "date_expression_numeric", reverse);
				else lSort = new Sort(Sort.STRING, s[0], reverse);						
			} else if(s.length == 1) {
				if(s[0].equals("date")) lSort = new Sort(Sort.DATE, "date_expression_numeric", false);
				else lSort = new Sort(Sort.STRING, sort, false);
			}
			sQuery.addSort(lSort);
		}		
		if(start != 0) {
			sQuery.addParameter("path", String.valueOf(start));
		}		
		return sQuery;
	}
	/*
	protected List<TagNode> findTagNodes(QName qname, int count) {
		List<TagNode> nodes = new ArrayList<TagNode>();
		try {
			EntityResultSet names = entityService.search(new EntityQuery(qname));
			for(Entity entity : names.getResults()) {
				Property nameProperty = entity.getProperty(SystemModel.NAME);
				if(nameProperty != null) {
					TagNode tag = new TagNode(entity.getId(), nameProperty.toString(), entity.getTargetAssociations().size());
					nodes.add(tag);
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		Collections.sort(nodes);
		if(nodes.size() > count) return nodes.subList(0, count);
		return nodes;
	}
	*/
}
