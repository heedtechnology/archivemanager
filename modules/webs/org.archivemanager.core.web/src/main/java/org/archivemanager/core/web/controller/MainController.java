package org.archivemanager.core.web.controller;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.heed.openapps.SystemModel;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.messaging.MessagingService;
import org.heed.openapps.security.SecurityService;
import org.heed.openapps.security.User;
import org.heed.openapps.template.TemplateService;
import org.heed.openapps.theme.ThemeVariables;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class MainController extends ControllerSupport implements ApplicationContextAware {
	@Autowired SecurityService securityService;
	@Autowired EntityService entityService;
	@Autowired MessagingService messagingService;
	@Autowired TemplateService templateService;
	
	@Override
	public void setApplicationContext(ApplicationContext ctx) throws BeansException {
		
	}
	/*
	@RequestMapping(value="/{domain}", method = RequestMethod.GET)
	public ModelAndView root(HttpServletRequest request, HttpServletResponse response, @PathVariable("domain") String domain) throws Exception {
		
		return new ModelAndView("redirect:dashboard");
	}
	*/
	@RequestMapping(value="/login")
	public ModelAndView login(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String,Object> parms = new HashMap<String,Object>();
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String remember = request.getParameter("remember");
		ThemeVariables vars = new ThemeVariables("default");
		parms.put("theme", vars);
		if(username != null && password != null) {
			int result = 0;
			if(remember != null && remember.equals("on")) result = securityService.login(username, password, true);
			else result = securityService.login(username, password, false);
			if(result == -1) parms.put("authentication_message", "Unknown Account Exception");
			else if(result == -2) parms.put("authentication_message", "Incorrect Credentials Exception");
			else if(result == -3) parms.put("authentication_message", "Locked Account Exception");
			else if(result == -4) parms.put("authentication_message", "Excessive Attempts Exception");
			else if(result == -5) parms.put("authentication_message", "Authentication Exception");
		} else {
			return new ModelAndView("login", parms);
		}
		return new ModelAndView("redirect:", parms);
	}
	@RequestMapping(value="/logout")
	public ModelAndView logout(HttpServletRequest request, HttpServletResponse response) throws Exception {
		securityService.logout();
		return new ModelAndView("redirect:");
	}
	@RequestMapping(value="/register")
	public ModelAndView register(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String,Object> parms = new HashMap<String,Object>();
		String submitted = request.getParameter("submitted");
		if(submitted != null && submitted.length() > 0) {
			String username = request.getParameter("username");
			String email = request.getParameter("email");
			String password = request.getParameter("password");
			String first_name = request.getParameter("first_name");
			String last_name = request.getParameter("last_name");
			String organization = request.getParameter("organization");
			List<String> errors = new ArrayList<String>();
			if(username != null && username.length() > 0) {
				Entity user = entityService.getEntity(SystemModel.USER, SystemModel.USERNAME, username);
				if(user != null) errors.add("username already taken");
			} else  errors.add("username required");
			if(email == null || email.length() == 0) errors.add("email required");
			if(password == null || password.length() == 0) errors.add("password required");
			if(errors.size() > 0) {
				String msg = "your registration failed ";
				for(String error : errors)
					msg = msg += error+", ";
				parms.put("registration_message", msg.substring(0, msg.length()-2));
			}
			if(username != null && email != null && password != null && username.length() > 0 && email.length() > 0 && password.length() > 0) {
				Entity entity = entityService.getEntity(request, SystemModel.USER);
				entity.addProperty(SystemModel.NAME, email);
				entityService.addEntity(entity);
				parms.put("registration_message", "your registration is complete");
				return new ModelAndView("dashboard_guest", parms);
			}
		}
		ThemeVariables vars = new ThemeVariables("default");
		parms.put("theme", vars);
		return new ModelAndView("registration", parms);
	}
	@RequestMapping(value="/reminder")
	public ModelAndView reminder(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String,Object> parms = new HashMap<String,Object>();
		ThemeVariables vars = new ThemeVariables("default");
		parms.put("theme", vars);
		String submitted = request.getParameter("submitted");
		if(submitted != null && submitted.length() > 0) {
			String email = request.getParameter("email");
			Entity user = entityService.getEntity(SystemModel.USER, SystemModel.EMAIL, email);
			if(user != null) {
				Map<String,Object> templparms = new HashMap<String,Object>();
				String newPassword = securityService.generatePassword();
				user.addProperty(SystemModel.USER_PASSWORD, newPassword);
				entityService.updateEntity(user, false);
				templparms.put("email", email);
				templparms.put("password", newPassword);
				String text = templateService.process("messaging/password_reminder.ftl", templparms);
				messagingService.sendEmail(email, text, null, "ArchiveManager", "Password Reminder");
				parms.put("reminder_message", email);
			} else {
				parms.put("not_found_message", email);
			}
		}
		return new ModelAndView("reminder", parms);
	}
	@RequestMapping(value="/profile")
	public ModelAndView profile(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		ThemeVariables vars = new ThemeVariables("default");
		parms.put("theme", vars);
		User user = securityService.currentUser();
		parms.put("user", user);
		String submitted = request.getParameter("submitted");
		if(submitted != null && submitted.length() > 0) {
			String email = request.getParameter("email");
			String first_name = request.getParameter("first_name");
			String last_name = request.getParameter("last_name");
			String organization = request.getParameter("organization");
			List<String> errors = new ArrayList<String>();
			if(email == null || email.length() == 0) errors.add("email required");
			if(errors.size() > 0) {
				String msg = "your registration failed ";
				for(String error : errors)
					msg = msg += error+", ";
				parms.put("registration_message", msg.substring(0, msg.length()-2));
			}
			if(errors.isEmpty()) {
				/*
				user.getEntity().addProperty(SystemModel.EMAIL, email);
				user.getEntity().addProperty(SystemModel.FIRSTNAME, first_name);
				user.getEntity().addProperty(SystemModel.LASTNAME, last_name);
				user.getEntity().addProperty(SystemModel.ORGANIZATION, organization);
				entityService.updateEntity(user.getEntity(), true);
				*/
				parms.put("registration_message", "your profile was update successfully");				
			}						
		}
		return new ModelAndView("profile", parms);
	}
	
}
