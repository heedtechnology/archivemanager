package org.archivemanager.core.web.controller;

import org.heed.openapps.security.Role;
import org.heed.openapps.security.User;



public abstract class ControllerSupport {

	
	protected String getUser(User user) {
		StringBuffer out = new StringBuffer();
		out.append("{");
		out.append("'username':'"+user.getUsername()+"',");
		if(user.getFullName() != null) out.append("'fullname':'"+user.getFullName()+"',");
		out.replace(out.length()-1, out.length(), "}");
		if(out.length() > 2) return out.toString();
		else return "{}";
	}
	protected String getUserRoles(User user) {
		StringBuffer out = new StringBuffer();
		out.append("[");
		for(Role role : user.getRoles()) {
			String role_name = role.getName();
			out.append("'"+role_name+"',");
		}
		out.replace(out.length()-1, out.length(), "]");
		if(out.length() > 2) return out.toString();
		else return "[]";
	}
}
