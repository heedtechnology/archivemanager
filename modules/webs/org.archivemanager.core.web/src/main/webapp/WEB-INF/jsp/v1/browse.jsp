<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://shiro.apache.org/tags" prefix="shiro"%>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="org.heed.openapps.entity.Entity" %>
<%@ page import="org.heed.openapps.entity.Association" %>
<%@ page import="org.heed.openapps.SystemModel" %>
<%@ page import="org.heed.openapps.RepositoryModel" %>
<style>
.webtop-full-section {
    background: none repeat scroll 0 0 #FFFFFF;
    border-radius: 4px 4px 4px 4px;
    box-shadow: 0 0 6px rgba(0, 0, 0, 0.25);
    color: #333333;
    float: left;
    padding: 5px 5px 0;
    margin:10px;
    position: relative;
    width: 820px;
}
.webtop-menu-item {
	margin:10px;
	font-size:14px;
}
</style>

<div class="section">
	<div style="width:100%;position:relative;float:left;">
		<h4>Browse The Collections</h4>
		<div class="webtop-full-section" style="width:96%;">    		
        	<div style="float:left;width:100%;margin:10px;">
        		<%
        		int margin = 0;
        		List<Entity> parents = (List<Entity>)request.getAttribute("parents");
        		if(parents != null && parents.size() > 0) {
        			for(Entity entity : parents) {
        				String name = entity.getPropertyValue(SystemModel.OPENAPPS_NAME);
        				if(name == null || name.length() == 0) name = entity.getPropertyValue(RepositoryModel.DATE_EXPRESSION);
        				String localName = entity.getQname().getLocalName();
        				if(name == null || name.length() == 0) name = entity.getPropertyValue(RepositoryModel.CATEGORY_LEVEL);
        		%>
        				<div class="webtop-menu-item" style="margin-left:<%=margin%>px;">
        					<a href="/browse/<%=entity.getId() %>"><%=name %></a>
        				</div>
        		<%
        				margin += 25;
        			}
        		}
        		Entity entity = (Entity)request.getAttribute("entity");
        		if(entity != null) {
        			String name = entity.getPropertyValue(SystemModel.OPENAPPS_NAME);
    				if(name == null || name.length() == 0) name = entity.getPropertyValue(RepositoryModel.DATE_EXPRESSION);
        		%>
        			<div class="webtop-menu-item" style="margin-left:<%=margin%>px;">
        				<%=name %>
        			</div>
        		<%
        		}
        		%>
        	</div>
        	<div class="webtop-video-categories" style="float:left;width:45%;min-height:500px;margin:10px;">
        		        		
        		<%
        		List<Entity> entities = (List<Entity>)request.getAttribute("entities");
        		for(Entity e : entities) {
        			String name = e.getPropertyValue(SystemModel.OPENAPPS_NAME);
        			if(name == null || name.length() == 0) name = e.getPropertyValue(RepositoryModel.DATE_EXPRESSION);
        			String localName = e.getQname().getLocalName();
        			if(name == null || name.length() == 0) name = entity.getPropertyValue(RepositoryModel.CATEGORY_LEVEL);
        		%>
        			<div class="webtop-menu-item">
        				<img src="/theme/images/tree_icons/<%=localName %>.png" />
        				<a href="/browse/<%=e.getId() %>"><%=name %></a>
        			</div>
        		<%
        		}
        		%>
        	</div>
        	<div class="webtop-video-results" style="float:right;width:51%;min-height:500px;margin:10px;">
        		<%
        		if(entity != null) {
        			String date_expression = entity.getPropertyValue(RepositoryModel.DATE_EXPRESSION);
        			if(date_expression != null && date_expression.length() > 0) {
        		%>
        				<div class="webtop-menu-item">
        					Date Expression : <%=date_expression %>
        				</div>
        		<%
        			}
        		}
        		List<Entity> notes = (List<Entity>)request.getAttribute("notes");
        		if(notes != null && notes.size() > 0) {
        			for(Entity note : notes) {
        				String type = note.getPropertyValue(SystemModel.NOTE_TYPE);
        				String content = note.getPropertyValue(SystemModel.NOTE_CONTENT);
        		%>
        				<div class="webtop-menu-item" style="text-decoration:underline;"><%=type %></div>
        				<div class="webtop-menu-item" style="text-align:justify;"><%=content %></div>
        		<%
        			}
        		} 		
        		List<Entity> names = (List<Entity>)request.getAttribute("names");
        		if(names != null && names.size() > 0) {
        		%>	
        			<div class="webtop-menu-item" style="text-decoration:underline;">Associated Names</div>
        		<%	
        			for(Entity assoc_name : names) {
        				String n = assoc_name.getPropertyValue(SystemModel.OPENAPPS_NAME);
        		%>
        				<div class="webtop-menu-item"><%=n %></div>
        		<%
        			}
        		}
        		        		
        		List<Entity> subjects = (List<Entity>)request.getAttribute("subjects");
        		if(subjects != null && subjects.size() > 0) {
        		%>	
        			<div class="webtop-menu-item" style="text-decoration:underline;">Associated Subjects</div>
        		<%	
        			for(Entity assoc_subj : subjects) {
        				String n = assoc_subj.getPropertyValue(SystemModel.OPENAPPS_NAME);
        		%>
        				<div class="webtop-menu-item"><%=n %></div>
        		<%
        			}
        		}
        		%>
        	</div>
    	</div> 
    
    </div>
</div>