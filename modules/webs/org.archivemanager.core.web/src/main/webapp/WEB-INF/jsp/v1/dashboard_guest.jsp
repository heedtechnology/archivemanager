<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://shiro.apache.org/tags" prefix="shiro"%>
<link rel="stylesheet" href="/theme/styles/dashboard.css" type="text/css" />
<script>
function searchSubmit(id) {
	var query = document.getElementById(id).value;
	document.location = '/dashboard/'+query;
}
function submitenter(myfield,e) {
	var keycode;
	if(window.event) keycode = window.event.keyCode;
	else if(e) keycode = e.which;
	else return true;
	if(keycode == 13) {
		searchSubmit();
   		return false;
   	} else return true;
}
</script>
<div class="section">
	<div class="webtop-section">
    	<h4>Check out all these features you're going to love</h4>
        <ul style="height:240px;">
        	<li>
        		<span class="icon webtop-icons-collection"></span>
                <p>Create and classify collections, include your online and offline content.</p>
           	</li>
            <li>
            	<span class="icon webtop-icons-classification"></span>
            	<p>Capture profiles of people, companies and subjects and connect them to your content.</p>
            </li>
         	<li>
                <span class="icon webtop-icons-content"></span>
                <p>Create content stores for your images, videos and office documents.</p>
            </li>
            <li>
               <span class="icon webtop-icons-contacts"></span>
               <p>Create profiles of your donors, capture notes and activities.</p>
            </li>
            <li>
               <span class="icon webtop-icons-server"></span>
               <p>Our enterprise architecture is easy to customize and extend.</p>
            </li>
            <li>
            	<span class="icon webtop-icons-search"></span>
                <p>Create intuitive search and navigation that helps users discover the value your content.</p>
            </li>
            
        </ul>
        <h3>
        	It's easier than ever to organize and share your online content across all of your sources.  
        	Build and categorize content of any type, connect that content to famous people, places and subjects.
        	Build intuitive search and navigation around your collections, serve digital media and content on a secure enterprise platform across all mobile and web devices.
        </h3>        
   	</div>
   	<div style="float:left;width:350px;margin-top:25px;">
   		<div class="webtop-signin">
   			<div style="margin:5px 5px 10px 10px;font-weight:bold;">
   				Returning users please login below
   			</div>
   			<c:if test="${authentication_message != null}">
   				<div style="margin-left:75px;font-weight:bold;font-size:10px;"><c:out value="${authentication_message}" /></div>
   			</c:if>
   			<form action="/login" method="post">
   				<table>
   					<tr>
   						<td>Username</td>
   						<td><input style="width:175px;" name="username" type="text"/></td>
   					</tr>
   					<tr>
   						<td>Password</td>
   						<td><input style="width:175px;" name="password" type="password" /></td>
   					</tr>
   					<tr>
   						<td><input type="submit" value="submit"/></td>
   						<!-- 
   						<td>
   							<input type="checkbox" name="remember" style="margin-left:25px;"/>
   							<span style="font-size:15px;">remember me</span>
   						</td>
   						-->
   					</tr>
   				</table>
   			</form>
   		</div>
   		<div style="float:right;width:100%;margin:15px 150px 15px;vertical-align:middle;text-align:right;font-size:22px;font-weight:bold;"></div>
   		<div class="webtop-links">
   			<div class="webtop-link">
   				<img src="/theme/images/icons32/bullet_blue.png" />
   				<a style="position:relative;top:-11px;" href="/register">Register for a new account</a>
   			</div>
   			<div class="webtop-link">
   				<img src="/theme/images/icons32/bullet_red.png" />
   				<a style="position:relative;top:-11px;" href="/reminder">Password reminder</a>
   			</div>
   		</div>
   	</div> 
   	<div class="bottom-row">
   		<ul>
   			<li>
            	<jsp:include page="fragments/browser.jsp" />
            </li>
        	<li>
        		<jsp:include page="fragments/news_notes.jsp" />
           	</li>
            <li style="">
           		<jsp:include page="fragments/public_menu.jsp" />
            </li>
    	</ul>
   	</div> 		
</div>
		