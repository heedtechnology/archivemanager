<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://shiro.apache.org/tags" prefix="shiro"%>
<style>
.webtop-registration {
	width:800px;
	height:400px;
	margin:0 auto;
	padding: 14px 16px 30px;
	background: none repeat scroll 0 0 #FFFFFF;
    border-radius: 4px 4px 4px 4px;
    box-shadow: 0 0 6px rgba(0, 0, 0, 0.25);
    color: #333333;
    display: block;
    font-size: 13px;
}
.webtop-row {
	height:35px;
	width:100%;
	margin-left:50px;
}
.webtop-cell {
	float:left;
	height:100%;
	width:100%;
}
.webtop-label {
	float:left;
	width:65px;
	font-size:15px;
}
</style>
<div class="section">
	<div style="width:100%;height:65px;"></div>
	<div class="webtop-registration">
   		<div style="width:150px;float:left;">
   			<img src="/theme/images/icons128/password_reminder.png" />
   		</div>
   		<div style="width:640px;height:100%;float:left;">
   			<div class="webtop-row" style="height:50px;"></div>
   			<div class="webtop-row">
   				<div class="webtop-cell" style="margin-top:65px;">
   					<span style="font-weight:bold;">Enter your email address and we will send your password shortly...</span>
   				</div>
   				<c:if test="${reminder_message != null}">
   					<div class="webtop-cell" style="">A reminder was sent to : <c:out value="${reminder_message}" /></div>
   				</c:if>
   				<c:if test="${not_found_message != null}">
   					<div class="webtop-cell" style="">No user was found with the email address : <c:out value="${not_found_message}" /></div>
   				</c:if>
   			</div>
   			<form action="/reminder" method="POST">
   			<div class="webtop-row">
   				<div class="webtop-cell">
   					<div class="webtop-label" style="">EMail</div>
   					<input name="email" size="45" value="<c:out value="${user.email}" />" />
   				</div>
   			</div>
   			<div class="webtop-row">
   				<input type="submit" name="submitted" value="enter"/>
   				<div style="height:15px;"></div>
   			</div>
   			</form>
   		</div>
   	</div>
   	<div style="width:100%;height:65px;"></div>
</div>