<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://shiro.apache.org/tags" prefix="shiro"%>
<%@ page import="org.archivemanager.Collection" %>
<%@ page import="org.archivemanager.Individual" %>
<%@ page import="org.archivemanager.Organization" %>
<%
Collection collection = (Collection)request.getAttribute("collection");
Individual individual = (Individual)request.getAttribute("individual");
Organization organization = (Organization)request.getAttribute("organization");
%>
<script>
$(document).ready(function() {
	$("#tabs").tabs();
});
</script>
<c:if test="${not empty individual}">
	<div id="tabs">
  		<ul>
    		<li><a href="#tabs-1">Contact</a></li>
    		<li><a href="#tabs-2">Archival</a></li>
    		<li><a href="#tabs-3">Correspondence</a></li>
  		</ul>
  		<div id="tabs-1">
  			<p><b>Contact Information For : </b> <%=individual.getTitle() %></p>
  			<p></p>
  			<p><b>Biography</b></p>
  			<p><b>Status of Relationship</b></p>
  			
  			<p><b>Biographical Digital Sources</b></p>
  			<p><b>Correspondence and Activities</b></p>
  			<p><b>Restrictions</b></p>
  			<p><b>Reference</b></p>
  			
  		</div>
  		<div id="tabs-2">
    
  		</div>
  		<div id="tabs-3">
    
  		</div>
  	</div>
</c:if>

<c:if test="${not empty organization}">
	<div id="tabs">
  		<ul>
    		<li><a href="#tabs-1">Contact</a></li>
    		<li><a href="#tabs-2">Archival</a></li>
    		<li><a href="#tabs-3">Correspondence</a></li>
  		</ul>
  		<div id="tabs-1">
  			<p></p>
  		</div>
  		<div id="tabs-2">
    
  		</div>
  		<div id="tabs-3">
    
  		</div>
  	</div>
</c:if>

<c:if test="${not empty collection}">
	<div id="tabs">
  		<ul>
    		<li><a href="#tabs-1">Archival</a></li>
    		<li><a href="#tabs-2">Donor</a></li>
  		</ul>
  		<div id="tabs-1">
  			<p></p>
  		</div>
  		<div id="tabs-2">
    
  		</div>
  		<div id="tabs-3">
    
  		</div>
  	</div>
</c:if>