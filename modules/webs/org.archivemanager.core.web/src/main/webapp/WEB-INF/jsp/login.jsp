<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div style="float:center;width:350px;margin:0 auto;">
	<div style="float:right;width:100%;height:75px;vertical-align:middle;text-align:right;font-size:22px;font-weight:bold;"></div>
   	<div class="webtop-signin">
   		<div style="margin:5px 5px 10px 50px;font-weight:bold;">
   			Returning user login below
   		</div>
   		<c:if test="${authentication_message != null}">
   			<div><c:out value="${authentication_message}" /></div>
   		</c:if>
   		<form action="/login" method="post">
   			<table>
   				<tr>
   					<td>Username</td>
   					<td><input style="width:175px;" name="username" type="text"/></td>
   				</tr>
   				<tr>
   					<td>Password</td>
   					<td><input style="width:175px;" name="password" type="password" /></td>
   				</tr>
   				<tr>
   					<td><input type="submit" value="submit"/></td>
   					<td>
   						<!--
   						<input type="checkbox" name="remember" style="margin-left:25px;"/>
   						<span style="font-size:15px;">remember me</span>
   						-->
   					</td>
   				</tr>
   				<tr><td><div style="height:15px;"></div></td></tr>
   			</table>
   		</form>
   	</div>
   	<div style="float:right;width:100%;height:75px;vertical-align:middle;text-align:right;font-size:22px;font-weight:bold;"></div>
</div> 