<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://shiro.apache.org/tags" prefix="shiro"%>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="org.heed.openapps.entity.Entity" %>
<%@ page import="org.heed.openapps.entity.Association" %>
<%@ page import="org.heed.openapps.ContentModel" %>
<%@ page import="org.heed.openapps.SystemModel" %>
<%@ page import="org.heed.openapps.RepositoryModel" %>
<%@ page import="org.heed.openapps.ClassificationModel" %>
<link rel="stylesheet" href="/theme/styles/dashboard.css" type="text/css" />

<%
List<String[]> properties = new ArrayList<String[]>();
Entity collection = (Entity)request.getAttribute("entity");
String description = "";
if(collection.hasProperty(ClassificationModel.DATES)) {
	String value = collection.getPropertyValue(ClassificationModel.DATES);
	if(value.length() > 0) properties.add(new String[]{"Date", value});
}
if(collection.hasProperty(ClassificationModel.SOURCE)) {
	String value = collection.getPropertyValue(ClassificationModel.SOURCE);
	if(value.length() > 0) properties.add(new String[]{"Source", value});
}
if(collection.hasProperty(SystemModel.OPENAPPS_DESCRIPTION))
	description = collection.getPropertyValue(SystemModel.OPENAPPS_DESCRIPTION);
String avatar = (String)request.getAttribute("avatar");
List<Association> entities = collection.getAssociations(ClassificationModel.NAMED_ENTITIES);
%>
<div class="section">
	<div style="width:830px;position:relative;float:left;">
		<div class="webtop-section">
    		<h4><c:out value="${entity.label}" /></h4>
        	<div class="webtop-avatar" style="margin:15px 0 10px 10px;">
        		<%if(avatar != null) { %>
        		<img src="data:image/png;base64,<%=avatar %>" style="width:360px;height:360px;" />
        		<%} else { %>
        		<img src="/theme/images/image_not_available.png" />
        		<%} %>
        	</div>
        	<div class="webtop-details" style="margin:15px 0 10px 10px;">
        		<%for(int i=0; i < properties.size(); i++) { %>
        		<div class="webtop-detail-row">
        			<div class="webtop-detail-label"><%=properties.get(i)[0] %></div>
        			<div class="webtop-detail-value"><%=properties.get(i)[1] %></div>
        		</div>
        		<%} %>
        		<div class="webtop-detail-row" style="text-align:justify;">
        			<div class="webtop-detail-value" style="padding-right:25px;"><%=description %></div>
        		</div>
        	</div>
    	</div> 
    
    </div>
    <div style="float:left;width:350px;margin-top:25px;">
   		<div class="webtop-links">
   			<%for(Association targetAssoc : entities) {
   				Entity target = targetAssoc.getTargetEntity();
   				if(target != null) {
   					String name = target != null ? target.getPropertyValue(SystemModel.OPENAPPS_NAME) : "undefined";
   			%>
   					<div class="webtop-link">
   						<a style="position:relative;top:-11px;" href="/dashboard/view/<%=target.getId() %>">
   						<%=name %>
   					</a>
   				</div> 
   			<%	} 
   			}
   			%>		
   		</div>
   	</div>  
</div>