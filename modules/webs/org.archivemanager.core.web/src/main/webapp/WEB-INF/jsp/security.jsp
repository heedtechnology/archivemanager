<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script>var isomorphicDir = "/theme/smartclient/"</script>
<script src="/theme/smartclient/system/modules/ISC_Core.js"></script>
<script src="/theme/smartclient/system/modules/ISC_Foundation.js"></script>
<script src="/theme/smartclient/system/modules/ISC_Containers.js"></script>
<script src="/theme/smartclient/system/modules/ISC_Grids.js"></script>
<script src="/theme/smartclient/system/modules/ISC_Forms.js"></script>
<script src="/theme/smartclient/system/modules/ISC_DataBinding.js"></script>
<script src="/theme/smartclient/system/modules/ISC_RichTextEditor.js"></script>
<script SRC="/script/core.js"></script>
<script SRC="/theme/smartclient/skins/Enterprise/load_skin.js"></script>
<script SRC="/theme/script/core.js"></script>
<script src="/script/library/contact_library.js"></script>
<script src="/script/library/repository_library.js"></script>

<div class="body_container">
	<table cellspacing="0" cellpadding="0" style="width:100%;">
		<tr><td class="top-left"></td><td class="top-mid"></td><td class="top-right"></td></tr>
		<tr>
			<td class="mid-left"></td>
			<td class="mid-mid">
				<div style="width:100%">
					<div><script src="/script/user.js"></script></div>
        		</div> 
			</td>
			<td class="mid-right"></td>
		</tr>
		<tr><td class="bot-left"></td><td class="bot-mid"></td><td class="bot-right"></td></tr>
	</table>
</div>