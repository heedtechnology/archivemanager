<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://shiro.apache.org/tags" prefix="shiro"%>
<link rel="stylesheet" href="/theme/styles/dashboard.css" type="text/css" />
<script>
function searchSubmit(id) {
	var query = document.getElementById(id).value;
	if(id == 'searchInput1') document.location = '/search/client/'+query;
	else document.location = '/dashboard/'+query;
}
function submitenter(myfield,e) {
	var keycode;
	if(window.event) keycode = window.event.keyCode;
	else if(e) keycode = e.which;
	else return true;
	if(keycode == 13) {
		searchSubmit();
   		return false;
   	} else return true;
}
</script>
<div class="section">
	<div class="webtop-section">
    	<h4>Check out all these features you're going to love</h4>
        <ul style="height:240px;">
        	<li>
        		<span class="icon webtop-icons-collection"></span>
                <p>Create and classify collections, include your online and offline content.</p>
           	</li>
            <li>
            	<span class="icon webtop-icons-classification"></span>
            	<p>Capture profiles of people, companies and subjects and connect them to your content.</p>
            </li>
         	<li>
                <span class="icon webtop-icons-content"></span>
                <p>Create content stores for your images, videos and office documents.</p>
            </li>
            <li>
               <span class="icon webtop-icons-contacts"></span>
               <p>Create profiles of your donors, capture notes and activities.</p>
            </li>
            <li>
               <span class="icon webtop-icons-server"></span>
               <p>Our enterprise architecture is easy to customize and extend.</p>
            </li>
            <li>
            	<span class="icon webtop-icons-search"></span>
                <p>Create intuitive search and navigation that helps users discover the value your content.</p>
            </li>
            
        </ul>
        <h3>
        	It's easier than ever to organize and share your online content across all of your sources.  
        	Build and categorize content of any type, connect that content to famous people, places and subjects.
        	Build intuitive search and navigation around your collections, serve digital media and content on a secure enterprise platform across all mobile and web devices.
        </h3>        
   	</div>
   	<div style="float:left;width:310px;margin:25px 10px;">
   		<jsp:include page="fragments/user_menu.jsp" />
   	</div> 
   	<div class="bottom-row">
   		<ul>
   			<li>
            	<jsp:include page="fragments/browser.jsp" />
            </li>            
        	<li>
        		<jsp:include page="fragments/news_notes.jsp" />
           	</li>
           	<li>
           		<jsp:include page="fragments/public_menu.jsp" />
           	</li>
    	</ul>
   	</div> 		
</div>
		