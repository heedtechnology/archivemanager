<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script src="script/tagcloud.js"></script>
<style>
	.cloudHeading{width:500px;font-weight:bold;font-size:14px;text-align:center;}
</style>
<div class="section">
	<table cellspacing='0' cellpadding='0' style='width:100%'>
		<tr>
			<td valign="top" width="185">
				<div style="height:20px;width:185px;"></div>
			</td>
			<td width="100%" align="center" valign="top">
				<table width="100%" border="0">
					<tr>
						<td valign="top" align="center" height="100" colspan="2">
							<img src='/theme/images/logo/ArchiveManager500.png' width='500' height='91' alt=''/>
						</td>
					</tr>
					<tr><td valign="top" colspan="2"><center><c:out value="${itemCount}" /> items and counting...</center></td></tr>
					<tr>
						<td width="50%" style="text-align:center;">
							<span class="cloudHeading">Most Popular People</span>
							<div id="tagcloud1" style="border:0px solid black;position:relative;width:100%;height:250px;"></div>
						</td>
						<td width="50%" style="text-align:center;">
							<span class="cloudHeading">Most Popular Organizations</span>
							<div id="tagcloud2" style="border:0px solid black;position:relative;width:100%;height:250px;"></div>
						</td>
						<script>
								var tagcloud1 = new TagCloud(document.getElementById('tagcloud1'),'descending');
								<c:forEach items="${people}" var="tag">
									tagcloud1.addNode(new Node(<c:out value="${tag.id}"/>,'<c:out value="${tag.name}" />',<c:out value="${tag.size}" />));
								</c:forEach>
								tagcloud1.draw();
								var tagcloud2 = new TagCloud(document.getElementById('tagcloud2'),'descending');
								<c:forEach items="${subjects}" var="tag">
									tagcloud2.addNode(new Node(<c:out value="${tag.id}"/>,'<c:out value="${tag.name}" />',<c:out value="${tag.size}" />));
								</c:forEach>
								tagcloud2.draw();
						</script> 
					</tr>
					<tr>
						<td width="100%" align="center" valign="top" colspan="2">
							<jsp:include page="fragments/sites.jsp"/>
						</td>
					</tr>
				</table>		
			</td>
			<td valign="top" width="185">					
				<div style="height:20px;;width:185px;"></div>					
			</td>
		</tr>
	</table>
</div>