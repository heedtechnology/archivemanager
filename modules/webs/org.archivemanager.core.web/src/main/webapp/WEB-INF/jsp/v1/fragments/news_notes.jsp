<span class="webtop-cell-label">Features And Functionality</span>

<p>
	<div class="webtop-news-title">Managing Content</div>
    <div class="webtop-news-desc">Crawl remote filesystems and web content, store metadata and generate dynamic rendions.</div>
</p>
<p>
  	<div class="webtop-news-title">Custom Search Engine</div>
   	<div class="webtop-news-desc">Configure the search engine to your specifications, manage keyword definitions and navigation attributes.</div>
</p>
<p>
  	<div class="webtop-news-title">Donor Relationship Management</div>
   	<div class="webtop-news-desc">Capture donor contact information and track interactions through custom actions and activities.</div>
</p>