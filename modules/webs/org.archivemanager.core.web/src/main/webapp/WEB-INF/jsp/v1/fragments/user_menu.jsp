<%@ taglib uri="http://shiro.apache.org/tags" prefix="shiro"%>		
		<shiro:user>
    		<div class="webtop-links">
   				<div class="webtop-link">
   					<img src="/theme/images/icons32/drawer.png" />
   					<a style="position:relative;top:-11px;" href="/repository">Collections</a>
   				</div>
   				<div class="webtop-link">
   					<img src="/theme/images/icons32/bricks.png" />
   					<a style="position:relative;top:-11px;" href="/content/manager">Content Manager</a>
   				</div>
   				<div class="webtop-link">
   					<img src="/theme/images/icons32/users_large.gif" />
   					<a style="position:relative;top:-11px;" href="/donors">Donor Management</a>
   				</div>
   				<div class="webtop-link">
   					<img src="/theme/images/icons32/chart_organisation.png" />
   					<a style="position:relative;top:-11px;" href="/search/client">Search & Navigation</a>
   				</div>
   				<!--
   				<div class="webtop-link">
   					<img src="/theme/images/icons32/help.png" />
    				<a style="position:relative;top:-11px;" href="/help">Help & Documentation</a>
    			</div>
   				<shiro:hasRole name="administrator">
   				
   				<div class="webtop-link">
   					<img src="/theme/images/icons32/chart_organisation.png" />
   					<a style="position:relative;top:-11px;" href="/search/manager">Search Management</a>
   				</div>
   				 
   				<div class="webtop-link">
   					<img src="/theme/images/icons32/chart_bar.png" />
   					<a style="position:relative;top:-11px;" href="/analytics_web">Web Analytics</a>
   				</div>
   				-->
   				<div class="webtop-link">
   					<img src="/theme/images/icons32/application_view_icons.png" />
   					<a style="position:relative;top:-11px;" href="/administration/home">Administration</a>
   				</div>
   				</shiro:hasRole>
   			</div>
		</shiro:user>