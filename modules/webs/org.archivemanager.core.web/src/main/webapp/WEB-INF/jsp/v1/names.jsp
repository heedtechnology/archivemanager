<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://shiro.apache.org/tags" prefix="shiro"%>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="org.heed.openapps.entity.Entity" %>
<%@ page import="org.heed.openapps.entity.Association" %>
<%@ page import="org.heed.openapps.SystemModel" %>
<%@ page import="org.heed.openapps.RepositoryModel" %>
<style>
	.webtop-alpha-nav {
		float:left;
		width:25px;
		font-weight:bold;
	}
</style>
<div class="section">
	<div style="width:100%;position:relative;float:left;">
		<h4>Browse Names</h4>
		<div style="float:left;width:100%;margin:10px;text-align:center;">
			<div>Below is an alphabetical list of individuals and corporations associated with our collections:</div>
			<div style="height:25px;margin:15px;text-align:center;">
				<div class="webtop-alpha-nav" style="width:260px;">&nbsp;</div>
				<div class="webtop-alpha-nav"><a href="/names/A">A</a></div>
				<div class="webtop-alpha-nav"><a href="/names/B">B</a></div>
				<div class="webtop-alpha-nav"><a href="/names/C">C</a></div>
				<div class="webtop-alpha-nav"><a href="/names/D">D</a></div>
				<div class="webtop-alpha-nav"><a href="/names/E">E</a></div>
				<div class="webtop-alpha-nav"><a href="/names/F">F</a></div>
				<div class="webtop-alpha-nav"><a href="/names/G">G</a></div>
				<div class="webtop-alpha-nav"><a href="/names/H">H</a></div>
				<div class="webtop-alpha-nav"><a href="/names/I">I</a></div>
				<div class="webtop-alpha-nav"><a href="/names/J">J</a></div>
				<div class="webtop-alpha-nav"><a href="/names/K">K</a></div>
				<div class="webtop-alpha-nav"><a href="/names/L">L</a></div>
				<div class="webtop-alpha-nav"><a href="/names/M">M</a></div>
				<div class="webtop-alpha-nav"><a href="/names/N">N</a></div>
				<div class="webtop-alpha-nav"><a href="/names/O">O</a></div>
				<div class="webtop-alpha-nav"><a href="/names/P">P</a></div>
				<div class="webtop-alpha-nav"><a href="/names/Q">Q</a></div>
				<div class="webtop-alpha-nav"><a href="/names/R">R</a></div>
				<div class="webtop-alpha-nav"><a href="/names/S">S</a></div>
				<div class="webtop-alpha-nav"><a href="/names/T">T</a></div>
				<div class="webtop-alpha-nav"><a href="/names/U">U</a></div>
				<div class="webtop-alpha-nav"><a href="/names/V">V</a></div>
				<div class="webtop-alpha-nav"><a href="/names/W">W</a></div>
				<div class="webtop-alpha-nav"><a href="/names/X">X</a></div>
				<div class="webtop-alpha-nav"><a href="/names/Y">Y</a></div>
				<div class="webtop-alpha-nav"><a href="/names/Z">Z</a></div>
			</div>
		</div>
		<div class="webtop-video-categories" style="float:left;width:100%;min-height:500px;margin:10px;">
			<%
			List<Entity> names = (List<Entity>)request.getAttribute("names");
			if(names != null && names.size() > 0) {
				for(Entity entity : names) {
					String name = entity.getPropertyValue(SystemModel.OPENAPPS_NAME);
			%>		
					<div class="webtop-menu-item" style="float:left;width:48%;">
        				<a href="/dashboard/view/<%=entity.getId() %>"><%=name %></a>
        			</div>
			<%		
				}
			}
			%>
		</div>
	</div>
</div>