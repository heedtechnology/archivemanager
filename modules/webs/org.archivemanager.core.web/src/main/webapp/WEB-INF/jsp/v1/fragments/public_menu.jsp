<%@ taglib uri="http://shiro.apache.org/tags" prefix="shiro"%>	
	
	<div class="webtop-tombstone" style="margin:15px 0 0 5px;">
	<a href="/browse">
 		<span class="icon webtop-icons-repositories"></span>
   	</a>
	<div style="margin:3px;">Browse The Collections</div>           			
</div> 
<div class="webtop-tombstone" style="margin:15px 0 0 5px;">
	<a href="/names">
 		<span class="icon webtop-icons-names"></span>
 	</a>
	<div style="margin:3px;">Browse The Names</div>           			
</div>
<div class="webtop-tombstone" style="margin:15px 0 0 5px;">
	<a href="/subjects">
		<span class="icon webtop-icons-subjects"></span>
    </a>
	<div style="margin:3px;">Browse The Subjects</div>           			
</div>
	<!-- 
	<div class="webtop-tombstone" style="margin:15px 0 0 5px;">
		<a href="/av">
    		<span class="icon webtop-icons-av"></span>
    	</a>
    	<div style="margin:3px;">Media Center</div>
	</div>
	-->