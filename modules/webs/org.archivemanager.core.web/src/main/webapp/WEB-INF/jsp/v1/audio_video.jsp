<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://shiro.apache.org/tags" prefix="shiro"%>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="org.heed.openapps.entity.Entity" %>
<%@ page import="org.heed.openapps.SystemModel" %>
<%@ page import="org.heed.openapps.RepositoryModel" %>
<link rel="stylesheet" href="/dashboard/styles/main.css" type="text/css" />
<style>
.webtop-full-section {
    background: none repeat scroll 0 0 #101010;
    border-radius: 4px 4px 4px 4px;
    box-shadow: 0 0 6px rgba(0, 0, 0, 0.25);
    color: #333333;
    float: left;
    padding: 5px 5px 0;
    margin:10px;
    position: relative;
    width: 820px;
}
.webtop-video-selected {
    background: none repeat scroll 0 0 #FAFBFB;
    border-radius: 4px 4px 4px 4px;
    box-shadow: 0 0 6px rgba(0, 0, 0, 0.25);
    color: #333333;
    float: left;
    padding: 5px 5px 0;
    margin:10px;
    position: relative;
    width: 97%;
    height:100px;
}
</style>

<div class="section">
	<div style="width:100%;position:relative;float:left;">
		<h4>Audio Video Center</h4>
		<div class="webtop-full-section" style="width:96%;">    		
        	<div class="webtop-video-categories" style="float:left;width:20%;min-height:335px;margin:10px;border:1px solid black;">
        		
        	</div>
        	<div class="webtop-video-results" style="float:right;width:76%;margin:10px;border:1px solid black;">
        		<div>Most Popular Videos</div>
        		<div>Most Popular Audio</div>
        	</div>
        	<div class="webtop-video-selected">
        		
        	</div>
    	</div> 
    
    </div>
</div>