<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://shiro.apache.org/tags" prefix="shiro"%>

<script>
function searchSubmit(id) {
	var query = document.getElementById(id).value;
	document.location = '/dashboard/'+query;
}
function submitenter(myfield,e) {
	var keycode;
	if(window.event) keycode = window.event.keyCode;
	else if(e) keycode = e.which;
	else return true;
	if(keycode == 13) {
		searchSubmit();
   		return false;
   	} else return true;
}
$(document).ready(function() {
	$('#autocomplete').autocomplete({
    	serviceUrl: '/dashboard/search.json',
    	formatResult : function (suggestion, currentValue) {
    		var reEscape = new RegExp('(\\' + ['/', '.', '*', '+', '?', '|', '(', ')', '[', ']', '{', '}', '\\'].join('|\\') + ')', 'g'),
            	pattern = '(' + currentValue.replace(reEscape, '\\$1') + ')';
        	var label = suggestion.value.replace(new RegExp(pattern, 'gi'), '<strong>$1<\/strong>');
        	return label + ' (' + suggestion.type + ')';
    	},
    	onSelect: function (suggestion) {
    		$('#webtop-body').load('/dashboard/detail/'+suggestion.data+'.xml');
    	}
	});
});
</script>
<div class="section">
	<div class="webtop-column">
		<div class="webtop-cell">
			<b>Archival Tools</b>
		
			<div class="webtop-link">
   				<img src="/theme/images/icons32/drawer.png" />
   				<a style="position:relative;top:-11px;" href="/collections">Collections</a>
   			</div>
   			<div class="webtop-link">
   				<img src="/theme/images/icons32/CD.png" />
   				<a style="position:relative;top:-11px;" href="/content">Digital Objects</a>
   			</div>
   			<div class="webtop-link">
   				<img src="/theme/images/icons32/users_large.gif" />
   				<a style="position:relative;top:-11px;" href="/donors">Donors</a>
   			</div>
   			<div class="webtop-link">
   				<img src="/theme/images/icons32/entity.png" />
   				<a style="position:relative;top:-11px;" href="/entities">Names & Subjects</a>
   			</div>
   			<div class="webtop-link">
   				<img src="/theme/images/icons32/location_pin.png" />
   				<a style="position:relative;top:-11px;" href="/locations">Locations</a>
   			</div>
   			<div class="webtop-link">
   				<img src="/theme/images/icons32/chart_organisation.png" />
   				<a style="position:relative;top:-11px;" href="/search">Search & Navigation</a>
   			</div>
		</div>
		
		<div class="webtop-cell">
			<b>Management Tools</b>
			
			<div class="webtop-link">
   				<img src="/theme/images/icons32/chart_bar.png" />
   				<a style="position:relative;top:-11px;" href="/reporting">Reporting</a>
   			</div>
		</div>
		
		<shiro:hasRole name="administrator">
		<div class="webtop-cell">
			<b>Administration Tools</b>
			<div class="webtop-link">
   				<img src="/theme/images/icons32/bricks.png" />
   				<a style="position:relative;top:-11px;" href="/entity/manager">Entity Manager</a>
   			</div>
   			<div class="webtop-link">
   				<img src="/theme/images/icons32/set_security_question.png" />
   				<a style="position:relative;top:-11px;" href="/security/manager">Security</a>
   			</div>
		</div>
		</shiro:hasRole>
	</div>
	
	<div class="webtop-section">
    	<h4>Already Know What You Are Looking For?<br/>Search While You Type</h4>
        <div style="margin:10px 0 20px;">
        	<input type="text" name="country" id="autocomplete" style="width:550px;margin:0 auto;"/>        
        </div>
        <div id="webtop-body" style="">
        	<h3>
        		It's easier than ever to organize and share your online content across all of your sources.  
        		Build and categorize content of any type, connect that content to famous people, places and subjects.
        		Build intuitive search and navigation around your collections, serve digital media and content on a secure enterprise platform across all mobile and web devices.
        	</h3>
        </div>        
   	</div>
   	<div class="bottom-row">
   		<ul>
   			
    	</ul>
   	</div> 		
</div>
		