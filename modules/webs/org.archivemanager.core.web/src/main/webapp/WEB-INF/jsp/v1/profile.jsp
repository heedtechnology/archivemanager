<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://shiro.apache.org/tags" prefix="shiro"%>
<style>
.webtop-registration {
	width:800px;
	height:400px;
	margin:0 auto;
	padding: 14px 16px 30px;
	background: none repeat scroll 0 0 #FFFFFF;
    border-radius: 4px 4px 4px 4px;
    box-shadow: 0 0 6px rgba(0, 0, 0, 0.25);
    color: #333333;
    display: block;
    font-size: 13px;
}
.webtop-row {
	height:35px;
	width:100%;
	margin-left:50px;
}
.webtop-cell {
	float:left;
	height:100%;
	width:100%;
}
.webtop-label {
	float:left;
	width:90px;
	font-size:15px;
}
</style>
<div class="section">
	<div style="width:100%;height:65px;"></div>
	<div class="webtop-registration">
   		<div style="width:150px;float:left;">
   			<img src="/theme/images/icons128/profile.png" />
   		</div>
   		<div style="width:640px;height:100%;float:left;">
   			<div class="webtop-row">
   				<div class="webtop-cell" style="margin-top:75px;">
   					<span style="font-weight:bold;">Update your profile</span>
   				</div>
   			</div>
   			<form action="/profile" method="POST">
   			<div class="webtop-row">
   				<div class="webtop-cell">
   					<div class="webtop-label" style="">Username</div>
   					<div class="webtop-label" style=""><c:out value="${user.username}" /></div>
   				</div>
   				<div class="webtop-cell">
   					<div class="webtop-label" style="">EMail</div>
   					<input name="email" size="55" value="<c:out value="${user.email}" />" />
   					<img src="/theme/images/icons16/asterisk_yellow.png" />
   				</div>
   			</div>
   			<div class="webtop-row">
   				<div class="webtop-cell">
   					<div class="webtop-label" style="">First Name</div>
   					<input name="first_name" size="55" value="<c:out value="${user.firstName}" />" />
   				</div>
   				<div class="webtop-cell">
   					<div class="webtop-label" style="">Last Name</div>
   					<input name="last_name" size="55" value="<c:out value="${user.lastName}" />" />
   				</div>
   			</div>
   			<div class="webtop-row">
   				<div class="webtop-cell">
   					<div class="webtop-label" style="">Institution</div>
   					<input name="organization" size="55" value="<c:out value="${user.organization}" />" />
   				</div>
   			</div>
   			<div class="webtop-row">
   				<input type="submit" name="submitted" value="update"/>
   				<div style="height:15px;"></div>
   				<c:if test="${registration_message != null}">
   					<div class="webtop-cell"><c:out value="${registration_message}" /></div>
   				</c:if>
   			</div>
   			</form>
   		</div>
   	</div>
   	<div style="width:100%;height:65px;"></div>
</div>