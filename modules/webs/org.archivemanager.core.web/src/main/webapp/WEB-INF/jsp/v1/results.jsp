<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://shiro.apache.org/tags" prefix="shiro"%>
<%@ page import="java.util.List" %>
<%@ page import="org.heed.openapps.entity.Entity" %>
<%@ page import="org.heed.openapps.SystemModel" %>
<%@ page import="org.heed.openapps.RepositoryModel" %>
<%@ page import="org.heed.openapps.search.SearchResult" %>

<div class="section">
	<div style="float:left;">
	<c:if test="${not empty collections}">
	<div class="webtop-section" style="width:375px;">
    	<h3>Were you looking for one of these Collections</h3>
        
        	<%
        	List<SearchResult> collections = (List)request.getAttribute("collections");
        	for(SearchResult collection : collections) {
        		Long id = collection.getId();
        		String label = (String)collection.getData().get(SystemModel.OPENAPPS_NAME.toString());
        		String description = ""; //corporations.get(i).getPropertyValue(RepositoryModel.BIOGRAPHICAL_NOTE);        	
        	%>
        		<div class="webtop-link">
   					<img style="margin-right:10px;" src="/theme/images/icons32/drawer.png" />
   					<a style="position:relative;top:-6px;" href="/dashboard/view/<%=id %>">
   						<%=label %>
   					</a>
   					<div class="webtop-desc"><%=description %></div>
   				</div> 
   			<%} %>
        
    </div> 
    </c:if>   
	<c:if test="${not empty people}">
    <div class="webtop-section" style="width:375px;">
    	<h3>Were you looking for one of these People</h3>
        
        	<%
        	List<SearchResult> collections = (List)request.getAttribute("people");
        	for(SearchResult collection : collections) {
        		Long id = collection.getId();
        		String label = (String)collection.getData().get(SystemModel.OPENAPPS_NAME.toString());
        		String description = ""; //corporations.get(i).getPropertyValue(RepositoryModel.BIOGRAPHICAL_NOTE);        	
        	%>
        		<div class="webtop-link">
   					<img style="margin-right:10px;" src="/theme/images/icons32/user.png" />
   					<a style="position:relative;top:-6px;" href="/dashboard/view/<%=id %>">
   						<%=label %>
   					</a>
   					<div class="webtop-desc"><%=description %></div>
   				</div> 
   			<%} %>
        
    </div> 
    </c:if> 
    <c:if test="${not empty corporations}">
    <div class="webtop-section" style="width:375px;">
    	<h3>Were you looking for one of these Corporations</h3>
        
        	<%
        	List<SearchResult> collections = (List)request.getAttribute("corporations");
        	for(SearchResult collection : collections) {
        		Long id = collection.getId();
        		String label = (String)collection.getData().get(SystemModel.OPENAPPS_NAME.toString());
        		String description = ""; //corporations.get(i).getPropertyValue(RepositoryModel.BIOGRAPHICAL_NOTE);        	
        	%>
        		<div class="webtop-link">
   					<img style="margin-right:10px;" src="/theme/images/icons32/entity.png" />
   					<a style="position:relative;top:-6px;" href="/dashboard/view/<%=id %>">
   						<%=label %>
   					</a>
   					<div class="webtop-desc"><%=description %></div>
   				</div> 
   			<%} %>
        
    </div> 
    </c:if>
    </div>
    
</div>