<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://shiro.apache.org/tags" prefix="shiro"%>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="org.heed.openapps.entity.Entity" %>
<%@ page import="org.heed.openapps.entity.Association" %>
<%@ page import="org.heed.openapps.SystemModel" %>
<%@ page import="org.heed.openapps.RepositoryModel" %>
<style>
	.webtop-alpha-nav {
		float:left;
		width:25px;
		font-weight:bold;
	}
</style>
<div class="section">
	<div style="width:100%;position:relative;float:left;">
		<h4>Browse Subjects</h4>
		<div style="float:left;width:100%;margin:10px;text-align:center;">
			<div>Below is an alphabetical list of subjects associated with our collections:</div>
			<div style="height:25px;margin:15px;text-align:center;">
				<div class="webtop-alpha-nav" style="width:260px;">&nbsp;</div>
				<div class="webtop-alpha-nav"><a href="/subjects/A">A</a></div>
				<div class="webtop-alpha-nav"><a href="/subjects/B">B</a></div>
				<div class="webtop-alpha-nav"><a href="/subjects/C">C</a></div>
				<div class="webtop-alpha-nav"><a href="/subjects/D">D</a></div>
				<div class="webtop-alpha-nav"><a href="/subjects/E">E</a></div>
				<div class="webtop-alpha-nav"><a href="/subjects/F">F</a></div>
				<div class="webtop-alpha-nav"><a href="/subjects/G">G</a></div>
				<div class="webtop-alpha-nav"><a href="/subjects/H">H</a></div>
				<div class="webtop-alpha-nav"><a href="/subjects/I">I</a></div>
				<div class="webtop-alpha-nav"><a href="/subjects/J">J</a></div>
				<div class="webtop-alpha-nav"><a href="/subjects/K">K</a></div>
				<div class="webtop-alpha-nav"><a href="/subjects/L">L</a></div>
				<div class="webtop-alpha-nav"><a href="/subjects/M">M</a></div>
				<div class="webtop-alpha-nav"><a href="/subjects/N">N</a></div>
				<div class="webtop-alpha-nav"><a href="/subjects/O">O</a></div>
				<div class="webtop-alpha-nav"><a href="/subjects/P">P</a></div>
				<div class="webtop-alpha-nav"><a href="/subjects/Q">Q</a></div>
				<div class="webtop-alpha-nav"><a href="/subjects/R">R</a></div>
				<div class="webtop-alpha-nav"><a href="/subjects/S">S</a></div>
				<div class="webtop-alpha-nav"><a href="/subjects/T">T</a></div>
				<div class="webtop-alpha-nav"><a href="/subjects/U">U</a></div>
				<div class="webtop-alpha-nav"><a href="/subjects/V">V</a></div>
				<div class="webtop-alpha-nav"><a href="/subjects/W">W</a></div>
				<div class="webtop-alpha-nav"><a href="/subjects/X">X</a></div>
				<div class="webtop-alpha-nav"><a href="/subjects/Y">Y</a></div>
				<div class="webtop-alpha-nav"><a href="/subjects/Z">Z</a></div>
			</div>
		</div>
		<div class="webtop-video-categories" style="float:left;width:100%;min-height:500px;margin:10px;">
			<%
			List<Entity> subjects = (List<Entity>)request.getAttribute("subjects");
			if(subjects != null && subjects.size() > 0) {
				for(Entity entity : subjects) {
					String name = entity.getPropertyValue(SystemModel.OPENAPPS_NAME);
			%>		
					<div class="webtop-menu-item" style="float:left;width:48%;">
        				<a href="/dashboard/view/<%=entity.getId() %>"><%=name %></a>
        			</div>
			<%		
				}
			}
			%>
		</div>
	</div>
</div>