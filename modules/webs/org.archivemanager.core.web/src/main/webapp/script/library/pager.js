function getListPager(list) {
	return isc.HLayout.create({ID:list.getID()+"_pager",autoDraw:false,height:'24',width:'100%',align:'center',margin:'2',
		members:[
		     isc.Img.create({ID:list.getID()+"_FirstPage",width:20, height:20, imageType: "stretch",src: "/theme/images/icons/FirstPage.gif"}),
		     isc.Img.create({ID:list.getID()+"_FirstPage_unavailable",width:20, height:20, imageType: "stretch",src: "/theme/images/icons/FirstPage_unavailable.gif"}),
		     isc.Img.create({ID:list.getID()+"_pager",width:20, height:20, imageType: "stretch",src: "/theme/images/icons/PreviousPage.gif"}),
		     isc.Img.create({ID:list.getID()+"_pager",width:20, height:20, imageType: "stretch",src: "/theme/images/icons/PreviousPage_unavailable.gif"}),
		     isc.HTMLFlow.create({ID:list.getID()+"_pagerMsg",width:"100%",contents:"<span style='font-weight:bold;font-size:12px;'></span>"}),
		     isc.Img.create({ID:list.getID()+"_pager",width:20, height:20, imageType: "stretch",src: "/theme/images/icons/NextPage.gif"}),
		     isc.Img.create({ID:list.getID()+"_pager",width:20, height:20, imageType: "stretch",src: "/theme/images/icons/NextPage_unavailable.gif"}),
		     isc.Img.create({ID:list.getID()+"_pager",width:20, height:20, imageType: "stretch",src: "/theme/images/icons/LastPage.gif"}),
		     isc.Img.create({ID:list.getID()+"_pager",width:20, height:20, imageType: "stretch",src: "/theme/images/icons/LastPage_unavailable.gif"})
		],
		search:function(qname, query, field, sort) {
			list.fetchData({qname:qname,query:query,field:field,sort:sort});
		}
	});
}