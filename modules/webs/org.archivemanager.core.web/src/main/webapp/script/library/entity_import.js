function populateForm(record) {
	//if(record['setCollection'] == 'false') accessionImportForm1.getField('collectionSetter').setDisabled(true);
	//else accessionImportForm1.getField('collectionSetter').setDisabled(false);
	//if(record['setDonor'] == 'false') accessionImportForm1.getField('donorSetter').setDisabled(true);
	//else accessionImportForm1.getField('donorSetter').setDisabled(false);
	//if(record['setAddress'] == 'false') accessionImportForm1.getField('addressSetter').setDisabled(true);
	//else accessionImportForm1.getField('addressSetter').setDisabled(false);
	accessionImportForm1.editRecord(record);
}
function saveImport() {
	treeData.addData({repository:uploadForm.getValue('repository'),title:uploadForm.getValue('title')});
}
function upload() {
	uploadForm.submit({qname:});
}
function getImportApplication() {
	isc.RestDataSource.create({ID:"treeData",fetchDataURL:"import/fetch.xml",addDataURL:"import/add.xml",
	fields:[
		{name:"id", title:"ID", type:"text", primaryKey:true, required:true},
    	{name:"title", title:"Title", type:"text", length:"128", required:true},
    	{name:"parent", title:"ParentID", type:"text", required:true, foreignKey:"treeData.id", rootValue:"null"}
	]	
	});
	isc.DataSource.create({ID:"node"});
	isc.DataSource.create({ID:"properties"});
	isc.DataSource.create({ID:"notes"});
	isc.DataSource.create({ID:"names"});
	isc.DataSource.create({ID:"subjects"});
	isc.DataSource.create({ID:"containers"});
	isc.RestDataSource.create({ID:"repositories",fetchDataURL:"fetch.xml"});

	isc.ListGrid.create({ID:"importTree",dataSource:"treeData",width:350,height:"100%",autoFetchData:false,showHeader:true,
		showResizeBar:true,leaveScrollbarGap:false,animateFolders:true,selectionType:"single",autoDraw:false,
		recordClick:"populateForm(record)",
		fields:[
		    {name:"name", title:"Name"}
		]
	});
	//Main Body
	isc.TabSet.create({ID:"tabSet",tabBarPosition:"top",width:"60%",height:"100%",autoDraw:false,
    tabs: [
        {title: "Properties", pane:
        	isc.HLayout.create({ID:"importLayout",width:"100%",height:"100%",
        		members:[
        	       	isc.DynamicForm.create({ID:'accessionImportForm1',width:'100%',height:'50%',margin:'10',cellPadding:5,numCols:'4',colWidths:[115,'*'],visibility:'visible',autoFocus:false,
        	       		fields:[
        	       		    {name:'salutation',width:'100',type:'staticText',title:'Salutation'},
        	       		    {name:'status',width:'100',type:'staticText',title:'Status'},
        	       		    {name:'first_name',width:'*',type:'staticText',title:'First Name'},
        	       		    {name:'last_name',width:'*',type:'staticText',title:'Last Name'},
        	       		    {name:'type',width:'100',type:'staticText',title:'Type'},
        	       		    {name:'greeting',width:'*',type:'staticText',title:'Greeting'},
        	       		    
        	       		    {name:'address1',width:'*',type:'staticText',title:'Address 1'},
        	       		    {name:'address2',width:'*',type:'staticText',title:'Address 2'},
        	       		    {name:'city',width:'*',type:'staticText',title:'City'},
        	       		    {name:'state',width:'*',type:'staticText',title:'State'},
        	       		    {name:'country',width:'*',type:'staticText',title:'Country'},
        	       		    {name:'zip',width:'*',type:'staticText',title:'Zip'},
        	       		    
        	       		    {name:'phone',width:'*',type:'staticText',title:'Phone'},
        	       		 
        	       		    {name:'birth_date',width:'*',type:'staticText',title:'Birth Date'},        	       		    
        	       		    {name:'death_date',width:'*',type:'staticText',title:'Death Date'},        	       		    
        		    	    {name:'last_wrote',width:'*',type:'staticText',title:'Last Wrote'},	    	    
        		    	    {name:'last_ship',width:'*',type:'staticText',title:'Last Ship'},
        		    	    {name:'spouse',width:'*',type:'staticText',title:'Spouse'},
        		    	    {name:'datelist',width:'*',type:'staticText',title:'Date List'},
        	       		    {name:'bio_sources',width:'*',colSpan:'4',type:'staticText',title:'Bio Sources'},
        	       		    {name:'note',width:'500',height:'100',type:'staticText',colSpan:'4',title:'General Note'}       		    
        	       		]
        	       	})
        	    ]
        	})
        }
    ]
	});	
	isc.HLayout.create({ID:"menuLayout",width:"100%",height:35,border:'1px solid #C0C3C7',defaultLayoutAlign:"center",membersMargin:5,
		members:[
		    UploadForm.create({ID:"uploadForm",action:"/entity/import/upload.xml",numCols:4,cellPadding:5,width:"350",
		    	fields: [
		    	    {name: "file",title:"File",type:"UploadItem"}
		    	]
		    }),
		    isc.IButton.create({ID:"submitButton",title:"Process",click:"upload();"}),
		    isc.IButton.create({ID:"saveButton",title:"Save",click:"saveImport();"})
		]
	});
	isc.HLayout.create({ID:"pageLayout1",width:"100%",height:"100%",members:[importTree,tabSet]});
	return isc.VLayout.create({ID:"importPageLayout",width:1000,height:600,margin:5,membersMargin:5,members:[menuLayout,pageLayout1]});
}
isc.Window.create({ID:"findCollectionWin",title:"Add Address",width:400,height:100,autoCenter:true,isModal:true,showModalMask:true,autoDraw:false,
    items: [
        isc.DynamicForm.create({ID:"findCollectionWin2Form",numCols:"2",cellPadding:5,autoDraw:false,colWidths:[100,"*"],
         	fields: [
         	    {name:"date", title:"Date", type:"date",displayFormat:"toUSShortDate",required:true},
         	    //{name:"collectee", title:"Collectee", type:"text",width:"*",required:true},
         	    {name:"submit",title:"Save",type:"button",
         	    	click:function () {
         	    		if(addAccessionWin2Form.validate()) {
         	    			var parms = addAccessionWin2Form.getValues();
	         	    		parms['assoc_qname'] = '{openapps.org_repository_1.0}accessions';
	         	    		parms['entity_qname'] = '{openapps.org_repository_1.0}accession';
	         	    		parms['source_id'] = entityId;
	         	    		repositoryTree.addData(parms, function(xmlDoc, xmlText) {
	         	    			addAccessionWin2Form.clearValues();
	         	    			addAccessionWin.hide();
	         	    		});        	    			
         	    		}
         	    	}
         	    }
           	]
        })
    ]
});