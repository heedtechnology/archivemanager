function getContactRelationPanel(title,relationshipMap,width,height) {
	isc.MessagingDataSource.create({ID:'contacts',fetchDataURL:'/service/entity/search.xml',addDataURL:'/service/entity/create.xml',updateDataURL:'/service/entity/update.xml',removeDataURL:'/service/entity/remove.xml',fields:[{name:'id',title:'ID',type:'text',primaryKey:true,required:true}]});
	isc.Window.create({ID:"editContactRelationWindow",title:"Add/Update Contact",width:450,height:350,autoCenter:true,isModal:true,showModalMask:true,autoDraw:false,
		items: [
		    isc.DynamicForm.create({ID:'contactRelationListSearch',width:'100%',margin:2,numCols:2,cellPadding:2,
		    	fields:[
		    	    {name:'query',width:'380',type:'text',showTitle:false},
		    	    {name:'validateBtn',title:'search',type:'button',width:50,startRow:false,endRow:false,
		    	    	click:function() {
		    	    		var parms = {qname:'{openapps.org_contact_1.0}contact',query:contactRelationListSearch.getValue('query'),field:'name',sort:'name_e'};
		    	    		if(contactRelationListSearch.getValue('query')) {
		    	    			parms['query'] = contactRelationListSearch.getValue('query');
		    	    		} else {
		    	    			parms['_startRow'] = 0;
		    	    			parms['_endRow'] = 75;
		    	    		}
		    	    		//editContactRelationWindowList.fetchData({qname:'{openapps.org_contact_1.0}contact',query:query,field:'name',sort:'name_e'});
		    	    		editContactRelationWindowList.fetchData(parms);
		    	    		//isc.XMLTools.loadXML("/service/entity/search", function(xmlDoc, xmlText) {
		    	    			//var data = ds.recordsFromXML(isc.XMLTools.selectNodes(xmlDoc, "//data/node"));
		    	    			//editContactRelationWindowList.setData(data);
		    	    		//},{httpMethod:"GET",params:parms});
		    	    	}
		    	    }
		    	]
		    }),
		    isc.ListGrid.create({ID:'editContactRelationWindowList',dataSource:'contacts',height:'100%',width:'100%',margin:5,alternateRecordStyles:true,autoDraw:false,autoFetchData:false,
		    	fields:[
		    	    {name:'name',title:'Name',width:'*',align:'left'}
		    	]
		    }),
	        isc.DynamicForm.create({ID:"editContactRelationWindowForm",height:'30',width:"100%",numCols:"4",cellPadding:5,autoDraw:false,colWidths:[100,'*'],
	        	fields: [
	        	    {name:'relation',title:'Relationship',type:'select',width:'195',align:'left',valueMap:relationshipMap},
		        	{name:"submit",title:"Link",type:"button",icon:"/theme/images/icons16/link.png",startRow:false,endRow:false,
	 	         	   	click:function () {
	 	         	   		if(entityId && editContactRelationWindowList.anySelected()) {
	 	         	   			save(false);
	 	         	   			var target = editContactRelationWindowList.getSelectedRecord().id;
	 	         	   			parms = {};
	 	         	   			parms['relationship'] = editContactRelationWindowForm.getValue('relation');
	 	         	   			associate('{openapps.org_contact_1.0}contacts', entityId, target, parms, function(xmlDoc, xmlText) {
	 	         	   				updateEntityData(xmlDoc, xmlText);
	 	         	   				editContactRelationWindowForm.clearValues();
	 	         	   				editContactRelationWindow.hide();
	 	         	   			});
	 	         	   		}
	 	         	   	}
		        	},
		        	{name:"submit",title:"New Contact",type:"button",icon:'/theme/images/icons16/contact_grey_add.png',startRow:false,endRow:false,
	 	         	   	click:function () {
	 	         	   		addContactWindow.show();
	 	         	   	}
		        	}
	        	]
	        })
	    ]
	});
	createAddContactWindow(editContactRelationWindowList);
	return isc.VLayout.create({ID:"contactRelationPanel",autoDraw:false,width:width,
   	 	members:[	        	          
   	 	    isc.ToolStrip.create({width: "100%", height:24,membersMargin:5,
   	 	    	members: [
   	 	    	    isc.HTMLFlow.create({width:"100%",contents:"<span style='font-weight:bold;font-size:14px;'>"+title+"</span>"}),
   	 	    	    isc.Button.create({width:60,height:20,icon:"/theme/images/icons16/add.png",title:"add",click:"editContactRelationWindow.show()"}),
   	 	    	    isc.Button.create({width:75,height:20,icon:"/theme/images/icons16/remove.png",title:"delete",
   	    	    	 click:function() {
   	    	    		 parms = {};
   	    	    		 parms['id'] = contactRelationList.getSelectedRecord().id;
   	    	    		 parms['target'] = contactRelationList.getSelectedRecord().target_id;
   	    	    		 isc.XMLTools.loadXML("/service/association/remove.xml", function(xmlDoc, xmlText) {
   	    	    			 contactRelationList.removeSelectedData();
   	    	    		 },{httpMethod:"POST",params:parms});	    		        	        	
   	    	    	 }
   	 	    	    })
   	 	    	]
   	 	    }),
   	 	    isc.ListGrid.create({ID:'contactRelationList',autoFitData:"vertical",autoFitMaxHeight:height,width:'100%',height:60,alternateRecordStyles:true,autoDraw:false,autoFetchData:false,
   	 	    	fields:[
   	 	    	    {name:'name',title:'Name',width:'*',align:'left'},
   	    	        {name:'relationship',title:'Relationship',type:'select',width:'100',align:'center',valueMap:{'':''}}
	            ]
   	 	    })
   	 	]
    });
}
function createAddContactWindow(list) {
	return isc.Window.create({ID:'addContactWindow',title:'Add Contact',width:400,height:185,autoCenter:true,isModal:true,showModalMask:true,autoDraw:false,
		items:[
		    isc.DynamicForm.create({ID:'contactsListAddWindowForm',width:'100%',datasource:'contacts',margin:10,numCols:2,cellPadding:7,
		    	itemChanged:function(item,value) {
		    		if(value == '{openapps.org_contact_1.0}individual') {
		    			contactsListAddWindowForm.getField('name').hide();
		    			contactsListAddWindowForm.getField('first_name').show();
		    			contactsListAddWindowForm.getField('last_name').show();
		    			addContactWindow.setHeight(185);
		    		} else if(value == '{openapps.org_contact_1.0}organization') {
		    			contactsListAddWindowForm.getField('name').show();
		    			contactsListAddWindowForm.getField('first_name').hide();
		    			contactsListAddWindowForm.getField('last_name').hide();
		    			addContactWindow.setHeight(145);
		    		} 
		    	},
		    	fields:[
		    	    {name:'name',width:'*',required:true,title:'Name',visible:false},
		    	    {name:'first_name',width:'*',required:true,title:'First Name'},
		    	    {name:'last_name',width:'*',required:true,title:'Last Name'},
		    	    {name:'qname',width:'160',required:true,type:'select',title:'Type',defaultValue:'{openapps.org_contact_1.0}individual',
		    	    	valueMap:{'{openapps.org_contact_1.0}individual':'Individual','{openapps.org_contact_1.0}organization':'Organization'}
		    	    },	    	    
		    	    {name:'validateBtn',title:'Save',type:'button',
		    	    	click: function() {
		    	    		var parms = contactsListAddWindowForm.getValues();
		    	    		if(parms['qname'] == '{openapps.org_contact_1.0}individual')
		    	    			parms['name'] = parms['last_name']+', '+parms['first_name'];
		    	    		parms['format'] = 'tree';
		    	    		list.addData(parms);		    	    				    	    		
		    	    		//addEntity(contactsListAddWindowForm.getValue('qname'), parms, function(xmlDoc, xmlText) {
      	    					//data = ds.recordsFromXML(isc.XMLTools.selectNodes(xmlDoc, "//node"));
      	    					//var updatedRecord = isc.addProperties(taskList.getSelectedRecord(),data[0]); 
      	    					//list.addData(data);		         	    				
      	    				//});
		    	    		contactsListAddWindowForm.clearValues();
		    	    		addContactWindow.hide();
		    	    	}
		    	    }
		    	]
		    })
        ]
	});
}