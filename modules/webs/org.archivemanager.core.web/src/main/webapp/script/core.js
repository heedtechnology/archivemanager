var isIE = navigator.userAgent.indexOf("MSIE") != -1;
if(isIE) {
	var smartWidth = 1200;
	var smartHeight = screen.height-210;	
} else {
	var smartHeight = window.innerHeight-105;
	var smartWidth = 1200;
}
isc.defineClass('UploadForm', 'DynamicForm');
UploadForm.addClassProperties({
  create: function(data) {
    // We are creating IFRAME that will work as a target for upload.
    var iframeCanvas = Canvas.getById('uploadFormIframe');
    if(!iframeCanvas) {
      Canvas.create({
        ID: "uploadFormIframe",
        contents: "<iframe name=\"uploadFormIframe\"></iframe>",
        autoDraw: true,
        visibility: "hidden"
      });
    }
    // parameters needed to submit a form  
    isc.addProperties(data, {
      encoding: "multipart",
      canSubmit: "true",
      target: "uploadFormIframe"
    });
    // special field that will hold form's ID
    data.fields.push({name: "uploadFormID", type: "hidden"});
    // We are creating a form.
    var f = this.Super('create', data);
    // We are setting special field to an ID of newly created form.
    f.setValue('uploadFormID', f.getID());
    return f;
  }
});
isc.defineClass("MessagingDataSource", "RestDataSource");
isc.MessagingDataSource.addProperties({
	transformResponse : function(dsResponse, dsRequest, data) {
		//message(data);
		this.Super('transformResponse', arguments); 
	}
});