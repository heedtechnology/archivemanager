package org.archivemanager.collections.client.collection.form;
import java.util.ArrayList;
import java.util.List;

import org.archivemanager.collections.client.ArchiveManager;
import org.archivemanager.collections.client.collection.component.PermissionComponent;
import org.archivemanager.collections.client.data.NativeCollectionModel;
import org.heed.openapps.gwt.client.form.WYSIWYGFormItem;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.SpinnerItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class CollectionForm extends HLayout {
	private ArchiveManager settings = new ArchiveManager();
	private NativeCollectionModel collectionModel = new NativeCollectionModel();
	private DynamicForm form1;
	private DynamicForm form2;
	private PermissionComponent permissionComponent;
	private CollectionOwnerItem ownerItem;
	
	List<FormItem> formFields = new ArrayList<FormItem>();
	
	public CollectionForm() {
		setHeight100();
		setWidth100();
		setMembersMargin(5);
		
		form1 = new DynamicForm();
		form1.setWidth100();
		form1.setNumCols(2);
		form1.setCellPadding(5);
		form1.setColWidths("80","*");
		
		List<FormItem> fields = new ArrayList<FormItem>();
		
		WYSIWYGFormItem nameItem = new WYSIWYGFormItem("name","Heading");
		nameItem.setHeight(75);
		nameItem.setWidth(420);
		nameItem.setColSpan(3);
		nameItem.setShowTitle(true);
		fields.add(nameItem);
		
		WYSIWYGFormItem scopeItem = new WYSIWYGFormItem("scope_note","Scope Note");
		scopeItem.setHeight(250);
		scopeItem.setWidth(420);
		scopeItem.setColSpan(3);
		scopeItem.setShowTitle(true);
		fields.add(scopeItem);
		
		WYSIWYGFormItem bioItem = new WYSIWYGFormItem("bio_note","Biographical Note");
		bioItem.setHeight(250);
		bioItem.setWidth(420);
		bioItem.setColSpan(3);
		bioItem.setShowTitle(true);
		fields.add(bioItem);
		
		TextAreaItem commentsItem = new TextAreaItem("comments","Coments");
		commentsItem.setHeight(125);
		commentsItem.setWidth(430);
		commentsItem.setColSpan(3);
		commentsItem.setShowTitle(true);
		fields.add(commentsItem);
		
		TextItem urlItem = new TextItem("url", "URL");
		urlItem.setColSpan(3);
		urlItem.setWidth(430);
		fields.add(urlItem);
		
		formFields.addAll(fields);
		FormItem[] itemArray = fields.toArray(new FormItem[fields.size()]);		
		form1.setFields(itemArray);
		addMember(form1);
		
		form2 = new DynamicForm();
		form2.setWidth100();
		form2.setCellPadding(5);
		form2.setMargin(7);
		form2.setNumCols(2);
		form2.setColWidths("125","*");
		form2.setDisabled(true);
		
		VLayout rightLayout = new VLayout();
		rightLayout.setWidth100();
		rightLayout.setHeight100();
		addMember(rightLayout);
		
		fields = new ArrayList<FormItem>();
		
		StaticTextItem idItem = new StaticTextItem("id", "ID");
		fields.add(idItem);
		
		ownerItem = new CollectionOwnerItem("username", "Owner");
		ownerItem.setWidth(150);
		fields.add(ownerItem);
				
		TextItem codeItem = new TextItem("code", "Collection Code");
		codeItem.setWidth(150);
		fields.add(codeItem);
		
		TextItem cidItem = new TextItem("identifier", "Collection ID");
		cidItem.setWidth(150);
		fields.add(cidItem);
		
		TextItem accessionDateItem = new TextItem("accession_date", "Accession Date");
		accessionDateItem.setWidth(150);
		fields.add(accessionDateItem);
		
		TextItem dateItem = new TextItem("date_expression", "Date Expression");
		dateItem.setWidth(150);
		fields.add(dateItem);
		
		TextItem beginItem = new TextItem("begin", "Begin Date");
		fields.add(beginItem);
		
		TextItem endItem = new TextItem("end", "End Date");
		fields.add(endItem);
		
		TextItem bulkBeginItem = new TextItem("bulk_begin", "Bulk Begin Date");
		fields.add(bulkBeginItem);
		
		TextItem bulkEndItem = new TextItem("bulk_end", "Bulk End Date");
		fields.add(bulkEndItem);
		
		SelectItem languageItem = new SelectItem("language", "Language");
		languageItem.setWidth(150);
		languageItem.setValueMap(collectionModel.getLanguageCodes());
		fields.add(languageItem);
		
		SelectItem extentUnitsItem = new SelectItem("extent_units", "Extent Units");
		extentUnitsItem.setWidth(100);
		extentUnitsItem.setValueMap(collectionModel.getExtentUnits());
		fields.add(extentUnitsItem);
		
		SpinnerItem extentValueItem = new SpinnerItem("extent_number", "Extent Value");
		extentValueItem.setWidth(50);
		fields.add(extentValueItem);
		
		CheckboxItem restrictionItem = new CheckboxItem("restrictions", "Restrictions");
		//restrictionItem.setHeight(50);
		restrictionItem.setLabelAsTitle(true);
		fields.add(restrictionItem);
		
		CheckboxItem internalItem = new CheckboxItem("internal", "Internal");
		internalItem.setLabelAsTitle(true);	
		fields.add(internalItem);
		
		CheckboxItem publicItem = new CheckboxItem("isPublic", "Public");
		publicItem.setLabelAsTitle(true);
		fields.add(publicItem);
		
		formFields.addAll(fields);
		itemArray = fields.toArray(new FormItem[fields.size()]);		
		form2.setFields(itemArray);
		rightLayout.addMember(form2);
		
		permissionComponent = new PermissionComponent("Shares");
		permissionComponent.hide();
		rightLayout.addMember(permissionComponent);		
	}
	
	
	public void editRecord(Record record) {		
		if(record.getAttribute("restrictions") != null) 
			record.setAttribute("restrictions", Boolean.valueOf(record.getAttribute("restrictions")));
		if(record.getAttribute("internal") != null) 
			record.setAttribute("internal", Boolean.valueOf(record.getAttribute("internal")));
		if(record.getAttribute("isPublic") != null) 
			record.setAttribute("isPublic", Boolean.valueOf(record.getAttribute("isPublic")));
		
		permissionComponent.select(record);		
		form1.editRecord(record);
		form2.editRecord(record);
		
		String username = record.getAttribute("username");
		if(settings.getUser().isAdmin() || settings.getUser().getUsername().equals(username)) {
			ownerItem.show();
			form1.setDisabled(false);
			form2.setDisabled(false);
			permissionComponent.show();
		} else if(settings.getUser().isArchiveManager()) {
			ownerItem.hide();
			form1.setDisabled(false);
			form2.setDisabled(false);
			permissionComponent.hide();
		} else {
			ownerItem.hide();
			form1.setDisabled(true);
			form2.setDisabled(true);
			permissionComponent.hide();
		}				
	}
	public Record getValuesAsRecord() {
		Record record = new Record();
		for(FormItem item : formFields) {
			String name = item.getName();
			Object value = item.getValue();
			record.setAttribute(name, value);
		}
		if(ownerItem != null && ownerItem.getUserId() > 0) {
			record.setAttribute("user", ownerItem.getUserId());
		}
		return record;
	}
}
