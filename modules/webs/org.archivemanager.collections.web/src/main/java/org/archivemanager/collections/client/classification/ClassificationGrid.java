package org.archivemanager.collections.client.classification;

import org.archivemanager.collections.client.ArchiveManager;
import org.archivemanager.collections.client.Searchable;
import org.archivemanager.collections.client.components.PagingDisplay;
import org.heed.openapps.gwt.client.data.EntityServiceDS;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class ClassificationGrid extends VLayout implements Searchable {
	private ArchiveManager settings = new ArchiveManager();
	private ListGrid grid;
	private PagingDisplay paging;
	private AddClassificationWindow addWindow;
	private HLayout toolbar;
	
	int startRow;
	int total;
	String action;
	String query;
	String qname;
	
	private CellClickHandler callback;
	
	public ClassificationGrid() {
		setWidth100();
		setHeight100();
		
		/** Add/Delete Toolbar **/
		toolbar = new HLayout();
		toolbar.setWidth100();
		toolbar.setHeight(24);
		if(!settings.getUser().isArchiveManager()) toolbar.hide();
		toolbar.setBorder("1px solid #C0C3C7");
		addMember(toolbar);
		
		ImgButton addButton = new ImgButton();
		addButton.setWidth(22);
		addButton.setHeight(22);
		addButton.setMargin(2);
		addButton.setSrc("/theme/images/icons16/add.png");
		addButton.setPrompt("Add Classification");
		addButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				addWindow.show();
			}
		});
		toolbar.addMember(addButton);
		
		Label addLabel = new Label("<span style='color:#C0C3C7;cursor:pointer;'>add</span>");
		addLabel.setWidth(50);
		addLabel.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				addWindow.show();
			}
		});
		toolbar.addMember(addLabel);
		
		final ImgButton delButton = new ImgButton();
		delButton.setWidth(22);
		delButton.setHeight(22);
		delButton.setMargin(2);
		delButton.setSrc("/theme/images/icons16/delete.png");
		delButton.hide();
		delButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {				
				SC.confirm("Do you really wish to remove this permanently?", new BooleanCallback() {
					public void execute(Boolean value) {
						if(value) {
							Record rec = new Record();
							String id = grid.getSelectedRecord().getAttribute("target_id");
							if(id == null) id = grid.getSelectedRecord().getAttribute("id");
							rec.setAttribute("id", id);	
							
							EntityServiceDS.getInstance().removeEntity(rec, new DSCallback() {
								public void execute(DSResponse response, Object rawData, DSRequest request) {
									grid.removeSelectedData();	
								}
							});
						}
					}			
				});					
			}
		});
		toolbar.addMember(delButton);
		final Label removeLabel = new Label("<span style='color:#C0C3C7;cursor:pointer;'>remove</span>");
		removeLabel.setWidth(50);
		removeLabel.hide();
		removeLabel.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				SC.confirm("Do you really wish to remove this permanently?", new BooleanCallback() {
					public void execute(Boolean value) {
						if(value) {
							Record rec = new Record();
							String id = grid.getSelectedRecord().getAttribute("target_id");
							if(id == null) id = grid.getSelectedRecord().getAttribute("id");
							rec.setAttribute("id", id);	
							
							EntityServiceDS.getInstance().removeEntity(rec, new DSCallback() {
								public void execute(DSResponse response, Object rawData, DSRequest request) {
									grid.removeSelectedData();	
								}
							});
						}
					}			
				});		
			}
		});
		removeLabel.setWidth(150);
		toolbar.addMember(removeLabel);
		
		/** Grid  **/
		grid = new ListGrid();
		grid.setWidth100();
		grid.setHeight100();
		grid.setBorder("1px solid #C0C3C7");
		grid.setShowHeader(false);
		ListGridField nameField = new ListGridField("name","Name");
		grid.setFields(nameField);
		grid.addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				if(settings.getUser().isArchiveManager()) {
					delButton.show();
					removeLabel.show();
				}
				callback.onCellClick(event);
			}
		});
		addMember(grid);
		
		/** Paging **/
		paging = new PagingDisplay(this);
		paging.hide();
		addMember(paging);
		
		addWindow = new AddClassificationWindow();
		addWindow.setAddCallback(new DSCallback() {
			@Override
			public void execute(DSResponse dsResponse, Object data, DSRequest dsRequest) {
				if(dsResponse.getData() != null && dsResponse.getData().length > 0) {
					grid.addData(dsResponse.getData()[0]);
				}
			}
		});
	}
	public void search(Criteria criteria) {
		grid.setEmptyMessage("${loadingImage}&nbsp;Loading data...");
		grid.setData(new ListGridRecord[0]);
		action = criteria.getAttribute("action");
		if(action != null) {
			if(criteria.getAttribute("query") != null) query = criteria.getAttribute("query");
			if(criteria.getAttribute("qname") != null) qname = criteria.getAttribute("qname");
			if(action.equals("next")) {
				startRow = startRow + 20;
			} else if(action.equals("last")) {
				startRow = total - 20;
			} else if(action.equals("prev")) {
				startRow = startRow - 20;
			} else if(action.equals("first") || action == null) {
				startRow = 0;
			}
		} else {
			query = criteria.getAttribute("query");
			qname = criteria.getAttribute("qname");
			startRow = 0;
		}
		criteria.setAttribute("_startRow", startRow);
		criteria.setAttribute("_endRow", startRow + 25);
		criteria.setAttribute("sources", true);
		if(query != null) criteria.setAttribute("query", query);
		if(qname != null) criteria.setAttribute("qname", qname);
		grid.setData(new Record[0]);
		EntityServiceDS.getInstance().search(criteria, new DSCallback() {
			@Override
			public void execute(DSResponse dsResponse, Object data,	DSRequest dsRequest) {
				startRow = dsResponse.getStartRow();
				total = dsResponse.getTotalRows();
				grid.setData(dsResponse.getData());
				paging.setPagingData(dsResponse.getStartRow(), dsResponse.getEndRow(), dsResponse.getTotalRows());
				paging.show();
				grid.setEmptyMessage("No items to show.");
			}						
		});
	}
	public void addCellClickHandler(CellClickHandler handler) {
		callback = handler;
	}
	public Record getSelectedRecord() {
		return grid.getSelectedRecord();
	}
	public void removeSelectedData() {
		grid.removeSelectedData();
	}
	public void refreshFields() {
		grid.refreshFields();
	}
	public void setData(int start, int end, int total, Record[] data) {
		grid.setData(data);
		paging.setPagingData(start, end, total);
	}
	public void updateData(Record record) {
		grid.updateData(record);
	}
}
