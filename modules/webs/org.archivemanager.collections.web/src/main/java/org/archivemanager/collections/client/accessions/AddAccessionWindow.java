package org.archivemanager.collections.client.accessions;
import java.util.Date;
import java.util.LinkedHashMap;

import org.archivemanager.collections.client.CollectionEventTypes;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.OpenAppsEvent;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;


public class AddAccessionWindow extends Window {
	private static DateTimeFormat dateParser = DateTimeFormat.getFormat("yyyy-MM-dd");
	
	@SuppressWarnings("deprecation")
	public AddAccessionWindow() {
		setWidth(350);
		setHeight(200);
		setTitle("Add Accession");
		setAutoCenter(true);
		setIsModal(true);
		
		final DynamicForm addForm = new DynamicForm();
		addForm.setCellPadding(7);
		
		SelectItem yearSelector = new SelectItem();
		int year = new Date().getYear() + 1900;
		LinkedHashMap<Integer, Integer> years = new LinkedHashMap<Integer, Integer>();
		for(int i=0; i < 136; i++) {
			years.put(year, year);
			year--;
		}
		yearSelector.setValueMap(years);
		
		final DateItem dateItem = new DateItem("date", "Date");
		dateItem.setDateFormatter(DateDisplayFormat.TOSERIALIZEABLEDATE);
		dateItem.setStartDate(new Date(1885, 1, 1));
		dateItem.setYearSelectorProperties(yearSelector);
					
		final TextAreaItem generalNoteItem = new TextAreaItem("general_note", "Contents");
		generalNoteItem.setHeight(90);
		generalNoteItem.setWidth(400);
		generalNoteItem.setColSpan(2);
		
		ButtonItem submitItem = new ButtonItem("submit", "Add");
		submitItem.setIcon("/theme/images/icons16/add.png");
		submitItem.setStartRow(false);
		submitItem.setEndRow(false);
		submitItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
				Record record = addForm.getValuesAsRecord();				
				record.setAttribute("assoc_qname", "openapps_org_repository_1_0_accessions");
				record.setAttribute("entity_qname", "openapps_org_repository_1_0_accession");
				record.setAttribute("date", getShortFormattedDate(dateItem.getValueAsDate()));
				record.setAttribute("general_note", generalNoteItem.getValue());
				record.setAttribute("format", "tree");
				EventBus.fireEvent(new OpenAppsEvent(CollectionEventTypes.ADD_ACCESSION, record));
			}
		});
		
		addForm.setFields(dateItem,generalNoteItem,submitItem);
		addItem(addForm);
	}
	
	public static String getShortFormattedDate(Date in) {
		return dateParser.format(in);
	}
}