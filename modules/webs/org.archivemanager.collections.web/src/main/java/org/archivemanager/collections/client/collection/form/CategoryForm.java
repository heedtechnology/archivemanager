package org.archivemanager.collections.client.collection.form;
import java.util.ArrayList;
import java.util.List;

import org.archivemanager.collections.client.ArchiveManager;
import org.archivemanager.collections.client.data.NativeCollectionModel;
import org.heed.openapps.gwt.client.data.NativeContentTypes;
import org.heed.openapps.gwt.client.form.ContainerItem;
import org.heed.openapps.gwt.client.form.WYSIWYGFormItem;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.SpinnerItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class CategoryForm extends HLayout {
	private ArchiveManager settings = new ArchiveManager();
	private NativeCollectionModel collectionModel = new NativeCollectionModel();
	private NativeContentTypes contentTypes = new NativeContentTypes();
	
	private DynamicForm form1;
	private DynamicForm form2;
	
	private ContainerItem containerItem;
	
	List<FormItem> formFields = new ArrayList<FormItem>();
	
	public CategoryForm() {
		setHeight100();
		setWidth100();
		setMembersMargin(5);
		
		form1 = new DynamicForm();
		form1.setWidth100();
		form1.setNumCols(2);
		form1.setCellPadding(5);
		form1.setColWidths("80","*");
		
		List<FormItem> fields = new ArrayList<FormItem>();
		
		WYSIWYGFormItem nameItem = new WYSIWYGFormItem("name","Heading");
		nameItem.setHeight(75);
		nameItem.setWidth(420);
		nameItem.setColSpan(3);
		nameItem.setShowTitle(true);
		fields.add(nameItem);
		
		WYSIWYGFormItem descriptionItem = new WYSIWYGFormItem("description","Description");
		descriptionItem.setHeight(190);
		descriptionItem.setWidth(420);
		descriptionItem.setColSpan(3);
		descriptionItem.setShowTitle(true);
		fields.add(descriptionItem);
		
		WYSIWYGFormItem summaryItem = new WYSIWYGFormItem("summary","Summary");
		summaryItem.setHeight(190);
		summaryItem.setWidth(420);
		summaryItem.setColSpan(3);
		summaryItem.setShowTitle(true);
		fields.add(summaryItem);
		
		TextAreaItem commentsItem = new TextAreaItem("comments","Coments");
		commentsItem.setHeight(75);
		commentsItem.setWidth(430);
		commentsItem.setColSpan(3);
		commentsItem.setShowTitle(true);
		fields.add(commentsItem);
		
		TextItem urlItem = new TextItem("url", "URL");
		urlItem.setColSpan(3);
		urlItem.setWidth(430);
		fields.add(urlItem);
		
		containerItem = new ContainerItem("container", "Container", collectionModel.getContainerTypes());
		containerItem.setWidth(430);
		fields.add(containerItem);
		
		formFields.addAll(fields);
		FormItem[] itemArray = fields.toArray(new FormItem[fields.size()]);		
		form1.setFields(itemArray);
		addMember(form1);
		
		VLayout rightLayout = new VLayout();
		rightLayout.setWidth100();
		rightLayout.setHeight100();
		addMember(rightLayout);
		
		fields = new ArrayList<FormItem>();
		
		form2 = new DynamicForm();
		form2.setWidth100();
		form2.setCellPadding(5);
		form2.setMargin(7);
		form2.setNumCols(2);
		form2.setColWidths("100","*");
		
		StaticTextItem idItem = new StaticTextItem("id", "ID");
		fields.add(idItem);
		
		SelectItem contentTypeItem = new SelectItem("qname", "Content Type");
		contentTypeItem.setValueMap(contentTypes.getContentTypes());
		fields.add(contentTypeItem);
		
		SelectItem levelItem = new SelectItem("level", "Level");
		levelItem.setValueMap(collectionModel.getItemLevels());
		fields.add(levelItem);
		
		SelectItem languageItem = new SelectItem("language", "Language");
		languageItem.setWidth(150);
		languageItem.setValueMap(collectionModel.getLanguageCodes());
		fields.add(languageItem);
		
		TextItem dateExpressionItem = new TextItem("date_expression", "Date Expression");
		fields.add(dateExpressionItem);
		
		TextItem accessionDateItem = new TextItem("accession_date", "Accession Date");
		fields.add(accessionDateItem);
		
		TextItem beginItem = new TextItem("begin", "Begin Date");
		fields.add(beginItem);
		
		TextItem endItem = new TextItem("end", "End Date");
		fields.add(endItem);
		
		SelectItem extentUnitsItem = new SelectItem("extent_units", "Extent Units");
		extentUnitsItem.setWidth(100);
		extentUnitsItem.setValueMap(collectionModel.getExtentUnits());
		fields.add(extentUnitsItem);
		
		SpinnerItem extentValueItem = new SpinnerItem("extent_number", "Extent Value");
		extentValueItem.setWidth(75);
		fields.add(extentValueItem);
		
		SpinnerItem sizeItem = new SpinnerItem("size", "Size %");
		sizeItem.setMax(0);
		sizeItem.setMax(100);
		sizeItem.setWidth(50);
		fields.add(sizeItem);
		
		CheckboxItem restrictionItem = new CheckboxItem("restrictions", "Restrictions");
		restrictionItem.setHeight(35);
		restrictionItem.setLabelAsTitle(true);
		fields.add(restrictionItem);
		
		CheckboxItem internalItem = new CheckboxItem("internal", "Internal");
		internalItem.setHeight(35);
		internalItem.setLabelAsTitle(true);
		fields.add(internalItem);
		
		formFields.addAll(fields);
		itemArray = fields.toArray(new FormItem[fields.size()]);		
		form2.setFields(itemArray);
		rightLayout.addMember(form2);
		
		Canvas spacer = new Canvas();
		spacer.setHeight(20);
		rightLayout.addMember(spacer);
		
	}
	
	public void editRecord(Record record) {	
		form1.clearValues();
		form2.clearValues();
		
		form1.editRecord(record);
		form2.editRecord(record);
		
		if(record.getAttribute("restrictions") != null) 
			record.setAttribute("restrictions", Boolean.valueOf(record.getAttribute("restrictions")));
		if(record.getAttribute("internal") != null) 
			record.setAttribute("internal", Boolean.valueOf(record.getAttribute("internal")));
		containerItem.setValue(record.getAttribute("container"));		
		
		if(settings.getUser().isAdmin() || settings.getUser().isArchiveManager()) {
			form1.setDisabled(false);
			form2.setDisabled(false);
		} else {
			form1.setDisabled(true);
			form2.setDisabled(true);			
		}
	}
	
	public Record getValuesAsRecord() {
		Record record = new Record();
		for(FormItem item : formFields) {
			String name = item.getName();
			Object value = item.getValue();
			record.setAttribute(name, value);
		}		
		return record;
	}
	
}
