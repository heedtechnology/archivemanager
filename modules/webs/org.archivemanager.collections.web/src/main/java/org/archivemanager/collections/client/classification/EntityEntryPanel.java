package org.archivemanager.collections.client.classification;

import org.archivemanager.collections.client.ArchiveManager;
import org.heed.openapps.gwt.client.component.Toolbar;
import org.heed.openapps.gwt.client.data.EntityServiceDS;

import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.ExpansionMode;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class EntityEntryPanel extends VLayout {
	private ArchiveManager settings = new ArchiveManager();
	private Toolbar toolbar;
	private ListGrid grid;	
	private DynamicForm addForm;
	private Label message;
	private IButton saveButton;
	private ListGridField iconField;
	private AddEntryWindow addEntryWindow;
	
	private Record selection;
	private DSCallback callback;
	
	
	public EntityEntryPanel() {
		setWidth100();
		setHeight100();
		//setMargin(10);
				
		toolbar = new Toolbar(30);
		toolbar.hide();
		toolbar.addButton("add", "/theme/images/icons32/add.png", "Add", new com.smartgwt.client.widgets.events.ClickHandler() {  
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				addForm.editNewRecord();
				addEntryWindow.show();
			}  
		});
		
		addMember(toolbar);
		
		grid = new ListGrid() {
			@Override  
            protected Canvas createRecordComponent(final ListGridRecord record, Integer colNum) {  
				String fieldName = this.getFieldName(colNum);  
                if(fieldName.equals("iconField")) {  
                    HLayout recordCanvas = new HLayout(8);  
                    recordCanvas.setHeight100();
                    recordCanvas.setWidth100();
                    recordCanvas.setAlign(Alignment.CENTER);
                    
                    ImgButton editImg = new ImgButton();  
                    editImg.setShowDown(false);  
                    editImg.setShowRollOver(false);  
                    editImg.setLayoutAlign(Alignment.CENTER);  
                    editImg.setSrc("/theme/images/icons16/pencil.png");  
                    editImg.setPrompt("Edit Entry");  
                    editImg.setHeight(16);  
                    editImg.setWidth(16);
                    if(settings.getUser().isAdmin() || settings.getUser().isArchiveManager()) {
                    	editImg.show();
                    } else {
                    	editImg.hide();
                    }
                    editImg.addClickHandler(new ClickHandler() {  
                        public void onClick(ClickEvent event) {  
                        	addForm.editRecord(record);						
                        	addEntryWindow.show();
                        }  
                    });
                    recordCanvas.addMember(editImg);
                    
                    ImgButton delImg = new ImgButton();  
                    delImg.setShowDown(false);  
                    delImg.setShowRollOver(false);  
                    delImg.setLayoutAlign(Alignment.CENTER);  
                    delImg.setSrc("/theme/images/icons16/delete.png");  
                    delImg.setPrompt("Delete Entry");  
                    delImg.setHeight(16);  
                    delImg.setWidth(16);
                    if(settings.getUser().isAdmin() || settings.getUser().isArchiveManager()) {
                    	delImg.show();
                    } else {
                    	delImg.hide();
                    }
                    delImg.addClickHandler(new ClickHandler() {  
                        public void onClick(ClickEvent event) {  
                        	SC.confirm("Do you really wish to remove this permanently?", new BooleanCallback() {
            					public void execute(Boolean value) {
            						if(value) {
            							Record rec = new Record();
            							String id = record.getAttribute("target_id");
            							if(id == null) id = record.getAttribute("id");
            							rec.setAttribute("id", id);
            							EntityServiceDS.getInstance().removeEntity(rec, new DSCallback() {
            								public void execute(DSResponse response, Object rawData, DSRequest request) {
            									grid.removeData(record);
            									addForm.clearValues();
            								}						
            							});
            						}
            					}			
            				});
                        }  
                    });
                    recordCanvas.addMember(delImg);                    
                    return recordCanvas;
                }
                return null;
			};
			@Override  
            protected Canvas getExpansionComponent(ListGridRecord record) {  
                Canvas canvas = super.getExpansionComponent(record);  
                canvas.setMargin(5);  
                return canvas;  
            }
		};
		grid.setWidth100();
		grid.setHeight(300);
		grid.setShowRecordComponents(true);          
        grid.setShowRecordComponentsByCell(true);
		grid.setWrapCells(true);
		grid.setFixedRecordHeights(false);
		grid.setCanExpandRecords(true);
		grid.setShowHeader(false);
        grid.setExpansionMode(ExpansionMode.DETAILS);  
        //grid.setDetailField("items");
		
		ListGridField dateField = new ListGridField("date", "Date", 100);		
		ListGridField collectionField = new ListGridField("collection_name", "Collection");		
				
		iconField = new ListGridField("iconField", " ", 50);
		iconField.setCanSort(false);
		iconField.setCanFreeze(false);
		iconField.setCanGroupBy(false);
		iconField.setCanFilter(false);
		
		grid.setFields(dateField, collectionField, iconField);
		grid.addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				addForm.editRecord(event.getRecord());
				if(settings.getUser().isAdmin() || settings.getUser().isArchiveManager()) {
					saveButton.show();
					toolbar.show();
					iconField.setHidden(false);
				}
			}
		});
        addMember(grid);
        
        addEntryWindow = new AddEntryWindow();
	}
	
	public void select(Record record) {
		this.selection = record;
		try {
			Record[] notes = new Record[0];
			Record[] source_associations = record.getAttributeAsRecordArray("source_associations");
			if(source_associations != null) {				
				for(Record source_assoc : source_associations) {					
					Record notesRecord = source_assoc.getAttributeAsRecord("entries");
					if(notesRecord != null) {
						notes = source_assoc.getAttributeAsRecordArray("entries");
					}
				}
				grid.setData(notes);
			}
			if(settings.getUser().isAdmin() || settings.getUser().isArchiveManager()) {
				toolbar.show();
			}
		} catch(ClassCastException e) {
			grid.setData(new Record[0]);
		}
		addForm.clearValues();
	}
	public void setAddCallback(DSCallback callback) {
		this.callback = callback;
	}
	
	public class AddEntryWindow extends Window {
		
		public AddEntryWindow() {
			setWidth(525);
			setHeight(275);
			setTitle("Add Entry");
			setAutoCenter(true);
			setIsModal(true);
			
			VLayout mainLayout = new VLayout();
			mainLayout.setWidth100();
			mainLayout.setHeight100();
			addItem(mainLayout);
			
			addForm = new DynamicForm();
			addForm.setNumCols(3);
			addForm.setCellPadding(5);
			addForm.setMargin(5);
			addForm.setColWidths("75", "50", "*");
			addForm.setHeight100();
			
			final TextItem dateItem = new TextItem("date", "Date");
			dateItem.setWidth(200);
			dateItem.setColSpan(3);
			
			final TextItem collectionItem = new TextItem("collection_name", "Collection");
			collectionItem.setWidth(400);
			collectionItem.setColSpan(3);
			
			final TextItem nameItem = new TextItem("name", "Name");
			nameItem.setWidth(400);
			nameItem.setColSpan(3);
			
			TextAreaItem contentItem = new TextAreaItem("items","Items");
			contentItem.setColSpan(3);
			contentItem.setHeight(85);
			contentItem.setWidth(400);
			
			addForm.setFields(dateItem, collectionItem,nameItem,contentItem);
			mainLayout.addMember(addForm);
			
			HLayout buttonLayout = new HLayout();
			buttonLayout.setWidth100();
			buttonLayout.setHeight(25);
			buttonLayout.setMargin(10);
			buttonLayout.setMembersMargin(17);
			mainLayout.addMember(buttonLayout);
			
			saveButton = new IButton("Save");
			saveButton.setHeight(25);
			saveButton.setWidth(75);
			saveButton.setIcon("/theme/images/icons16/save.png");
			saveButton.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					final Record record = addForm.getValuesAsRecord();								
					if(addForm.isNewRecord()) {
						String id = selection.getAttribute("id");
						record.setAttribute("assoc_qname", "openapps_org_classification_1_0_entries");
						record.setAttribute("entity_qname", "openapps_org_classification_1_0_entry");	
						record.setAttribute("source", id);
						EntityServiceDS.getInstance().addAssociation(record, new DSCallback() {
							@Override
							public void execute(DSResponse response, Object data, DSRequest request) {
								if(response.getData() != null && response.getData().length > 0) {
									if(callback != null) callback.execute(response, data, request);
									//message.setContents("entry added successfully");
									addEntryWindow.close();
								}
							}							
						});
					} else {
						String id = grid.getSelectedRecord().getAttribute("target_id");
						if(id == null) id = grid.getSelectedRecord().getAttribute("id");
						record.setAttribute("id", id);
						record.setAttribute("sources", "false");
						record.setAttribute("targets", "false");
						record.setAttribute("qname", "openapps_org_classification_1_0_entry");
						EntityServiceDS.getInstance().updateEntity(record,new DSCallback() {
							public void execute(DSResponse response, Object rawData, DSRequest request) {
								if(response.getData() != null && response.getData().length > 0) {
									Record resp = response.getData()[0];
									grid.getSelectedRecord().setAttribute("date", resp.getAttribute("date"));
									grid.getSelectedRecord().setAttribute("items", resp.getAttribute("items"));
									grid.getSelectedRecord().setAttribute("name", resp.getAttribute("name"));
									grid.getSelectedRecord().setAttribute("collection_name", resp.getAttribute("collection_name"));
									grid.redraw();
									//message.setContents("entry saved successfully");
									addEntryWindow.close();
								}
							}						
						});					
					}
				}
			});
			buttonLayout.addMember(saveButton);
			
			message = new Label();
			message.setHeight(25);
			message.setWidth100();
			buttonLayout.addMember(message);
		}
	}
}
