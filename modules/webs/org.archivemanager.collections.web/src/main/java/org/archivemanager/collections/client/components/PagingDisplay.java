package org.archivemanager.collections.client.components;

import org.archivemanager.collections.client.Searchable;

import com.google.gwt.i18n.client.NumberFormat;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;


public class PagingDisplay extends HLayout {
	private NumberFormat fmt = NumberFormat.getDecimalFormat();
	private Label results;
	
	
	public PagingDisplay(final Searchable grid) {
		setWidth100();
		setHeight(30);
		setBorder("1px solid #C0C3C7");
        
        HLayout buttons = new HLayout();
        buttons.setWidth100();
        buttons.setMargin(3);
        buttons.setAlign(Alignment.CENTER);
        buttons.setAlign(VerticalAlignment.CENTER);
        buttons.setMembersMargin(3);
        
        IButton first = new IButton();
        first.setIcon("/theme/images/icons16/resultset_first.png");
        first.setWidth(24);
        first.addClickHandler(new ClickHandler() {
        	public void onClick(ClickEvent event) {
        		Criteria rec = new Criteria();
        		rec.setAttribute("action", "first");
        		grid.search(rec);
			}        	
        });
        buttons.addMember(first);
        
        IButton previous = new IButton();
        previous.setIcon("/theme/images/icons16/resultset_previous.png");
        previous.setWidth(24);
        previous.addClickHandler(new ClickHandler() {
        	public void onClick(ClickEvent event) {
        		Criteria rec = new Criteria();
        		rec.setAttribute("action", "prev");
        		grid.search(rec);
			}        	
        });                
        buttons.addMember(previous);
        
        results = new Label();
        results.setWidth100();
        results.setHeight(24);
        buttons.addMember(results);
        
        IButton next = new IButton();
        next.setIcon("/theme/images/icons16/resultset_next.png");
        next.setWidth(24);
        next.addClickHandler(new ClickHandler() {
        	public void onClick(ClickEvent event) {
        		Criteria rec = new Criteria();
        		rec.setAttribute("action", "next");
        		grid.search(rec);
			}        	
        });                
        buttons.addMember(next);
        
        IButton last = new IButton();
        last.setIcon("/theme/images/icons16/resultset_last.png");
        last.setWidth(24);        
        last.addClickHandler(new ClickHandler() {
        	public void onClick(ClickEvent event) {
        		Criteria rec = new Criteria();
        		rec.setAttribute("action", "last");
        		grid.search(rec);
			}        	
        });
        buttons.addMember(last);
        
        addMember(buttons);        
	}
	
	public void setPagingData(int start, int end, int resultCount) {
		int startPage = start+1;
		if(resultCount > 0) results.setContents("<div style='color:#A6ABB4;font-weight:bold;font-size:12px;text-align:center;'>"+ startPage +" - "+ end +" of "+fmt.format(resultCount)+" results</div>");	
		else results.setContents("<div style='color:#A6ABB4;font-weight:bold;font-size:12px;text-align:center;'>no results available</div>");
	}
	
	public class PageClickHandler implements ClickHandler {
		private int pageNumber;
		
		public PageClickHandler(int pageNumber) {
			this.pageNumber = pageNumber;
		}
		@Override
		public void onClick(ClickEvent event) {
			Record rec = new Record();
			rec.setAttribute("pageNumber", pageNumber);
			//EventBus.fireEvent(new OpenAppsEvent(SearchEventTypes.PAGING, rec));
		}
		
	}
	
}
