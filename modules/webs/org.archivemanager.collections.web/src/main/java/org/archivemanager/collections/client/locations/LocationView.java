package org.archivemanager.collections.client.locations;

import org.archivemanager.collections.client.ArchiveManager;
import org.archivemanager.collections.client.Searchable;
import org.archivemanager.collections.client.components.PagingDisplay;
import org.heed.openapps.gwt.client.data.EntityServiceDS;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class LocationView extends VLayout implements Searchable {
	private ArchiveManager settings = new ArchiveManager();
	private ListGrid locationGrid;
	private ListGrid searchGrid;
	private PagingDisplay paging;
	private LocationForm form;
	//private HLayout spacerViewer;
	//private Label spacerLabel;
	private AddLocationWindow addWindow;
	private VLayout vStack;
	private LocationToolbar toolbar;
	private Label message;
	private HLayout buttonLayout;
	private ImgButton rightArrowImg;
	private ImgButton leftArrowImg;
	
	private Record source;
	int startRow;
	int total;
	String action;
	String query;
	String qname = "openapps_org_repository_1_0_location";
	
	
	public LocationView(boolean edit) {
		setWidth100();
		setHeight100();
		
		HLayout topLayout = new HLayout();
		topLayout.setWidth100();
		topLayout.setHeight("290px");
		addMember(topLayout);
		
		VLayout locationViewer = new VLayout();
		locationViewer.setHeight100();
		locationViewer.setWidth100();
		locationViewer.setMargin(5);
		topLayout.addMember(locationViewer);
		
		vStack = new VLayout(5);  
        vStack.setHeight100();
        vStack.setWidth("40px");
        vStack.setMembersMargin(5);
        topLayout.addMember(vStack);
        
		VLayout locationSearch = new VLayout();
		locationSearch.setHeight100();
		locationSearch.setWidth100();
		locationSearch.setMargin(5);
		topLayout.addMember(locationSearch);
				
		HLayout toolstrip = new HLayout();
		toolstrip.setWidth100();
		toolstrip.setHeight(25);
		locationViewer.addMember(toolstrip);
		
		Label label = new Label("<label style='font-size:13px;font-weight:bold;color:#4C4C4C;font-family:Arial,Verdana,sans-serif;'>Current Locations</label>");
		label.setHeight("100%");
		label.setWidth("100%");
		toolstrip.addMember(label);
		
		locationGrid = new ListGrid();
		locationGrid.setWidth(345);
		locationGrid.setHeight100();
		locationGrid.setShowHeader(false);
		locationGrid.setAutoFitData(Autofit.VERTICAL);
		locationGrid.setWrapCells(true);
		locationGrid.setFixedRecordHeights(false);
		locationGrid.addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				Record record = locationGrid.getSelectedRecord();
				Criteria criteria = new Criteria();
				String target_id = record.getAttribute("target_id");
				if(target_id == null) target_id = record.getAttribute("id");
            	criteria.setAttribute("id", target_id);
            	criteria.setAttribute("sources", true);
            	criteria.setAttribute("targets", true);
            	EntityServiceDS.getInstance().getEntity(criteria, new DSCallback() {
            		public void execute(DSResponse dsResponse, Object data, DSRequest dsRequest) {
            			if(dsResponse.getData() != null && dsResponse.getData().length > 0) {
            				Record record = dsResponse.getData()[0];
            				selectLocation(record);
            				searchGrid.deselectAllRecords();
            			}
            		}			
            	});	
			}
		});
		
		ListGridField nameField = new ListGridField("name","Name");
				
		locationGrid.setFields(nameField);
		locationViewer.addMember(locationGrid);
					
		Canvas spacerCanvas = new Canvas();
        spacerCanvas.setHeight("125px");
        vStack.addMember(spacerCanvas);
        
        leftArrowImg = new ImgButton();
        leftArrowImg.setWidth(36);
        leftArrowImg.setHeight(36);
        leftArrowImg.setMargin(5);
        leftArrowImg.setSrc("[SKIN]/TransferIcons/left.png");
        leftArrowImg.hide();
        leftArrowImg.addClickHandler(new ClickHandler() {  
            public void onClick(ClickEvent event) {  
            	final Record record = searchGrid.getSelectedRecord();
            	if(record != null && source != null) {
	            	Record rec = new Record();
	            	String target_id = record.getAttribute("target_id");
					if(target_id == null) target_id = record.getAttribute("id");
	            	rec.setAttribute("source", source.getAttribute("id"));
	            	rec.setAttribute("target", target_id);
	            	rec.setAttribute("assoc_qname", "openapps_org_repository_1_0_locations");
					rec.setAttribute("entity_qname", "openapps_org_repository_1_0_location");				            	
					EntityServiceDS.getInstance().addAssociation(rec, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {						
							Record[] resultData = response.getData();
							if(resultData != null && resultData.length > 0 && resultData[0].getAttribute("id") != null) {
								searchGrid.removeSelectedData();
								select(resultData[0]);
								//message.setContents("donor saved successfully");
							} else {
								//message.setContents("there was a problem saving the donor");
							}
						}
					});
            	} else {
            		if(source == null) SC.warn("Error adding donor, missing valid collection");
            		else SC.warn("Error adding donor, missing valid donor");
            	}
            }  
        }); 
        vStack.addMember(leftArrowImg);
        
		rightArrowImg = new ImgButton();
		rightArrowImg.setWidth(36);
		rightArrowImg.setHeight(36);
		rightArrowImg.setMargin(5);
		rightArrowImg.setSrc("[SKIN]/TransferIcons/right.png");
		rightArrowImg.hide();
		rightArrowImg.addClickHandler(new ClickHandler() {  
            public void onClick(ClickEvent event) { 
            	final Record record = locationGrid.getSelectedRecord();
            	Record rec = new Record();
            	rec.setAttribute("id", record.getAttribute("id"));
            	EntityServiceDS.getInstance().removeAssociation(rec, new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						locationGrid.removeSelectedData();
						//searchGrid.addData(record);
					}
				}); 
            }  
        });  
        vStack.addMember(rightArrowImg);
        
		
		toolbar = new LocationToolbar(this);
		toolbar.setDeleteCallback(new DSCallback() {
			@Override
			public void execute(DSResponse dsResponse, Object data, DSRequest dsRequest) {
				SC.confirm("Do you really wish to remove this permanently?", new BooleanCallback() {
					public void execute(Boolean value) {
						if(value) {
							Record rec = new Record();
							String id = searchGrid.getSelectedRecord().getAttribute("target_id");
							if(id == null) id = searchGrid.getSelectedRecord().getAttribute("id");
							rec.setAttribute("id", id);					
							EntityServiceDS.getInstance().removeEntity(rec, new DSCallback() {
								public void execute(DSResponse response, Object rawData, DSRequest request) {
									searchGrid.removeSelectedData();	
								}
							});
						}
					}
				});
			}			
		});
		toolbar.setAddCallback(new DSCallback() {
			@Override
			public void execute(DSResponse dsResponse, Object data, DSRequest dsRequest) {
				addWindow.show();
			}			
		});
		locationSearch.addMember(toolbar);
				
		searchGrid = new ListGrid();
		searchGrid.setWidth100();
		searchGrid.setHeight100();
		searchGrid.setAutoFitData(Autofit.VERTICAL);
		searchGrid.setWrapCells(true);
		searchGrid.setFixedRecordHeights(false);
		searchGrid.setShowHeader(false);
		searchGrid.setSortField("name");
		nameField = new ListGridField("name","Name");
		searchGrid.setFields(nameField);
		searchGrid.addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				Record record = searchGrid.getSelectedRecord();
				Criteria criteria = new Criteria();
				String target_id = record.getAttribute("target_id");
				if(target_id == null) target_id = record.getAttribute("id");
            	criteria.setAttribute("id", target_id);
            	criteria.setAttribute("sources", true);
            	criteria.setAttribute("targets", true);
            	EntityServiceDS.getInstance().getEntity(criteria, new DSCallback() {
            		public void execute(DSResponse dsResponse, Object data, DSRequest dsRequest) {
            			if(dsResponse.getData() != null && dsResponse.getData().length > 0) {
            				Record record = dsResponse.getData()[0];
            				selectLocation(record);
            				locationGrid.deselectAllRecords();
            			}
            		}			
            	});
			}
		});
		locationSearch.addMember(searchGrid);
		
		/** Paging **/
		paging = new PagingDisplay(this);
		paging.hide();
		locationSearch.addMember(paging);
		
        HLayout bottomLayout = new HLayout();
        bottomLayout.setWidth100();
        bottomLayout.setHeight100();
        bottomLayout.setMembersMargin(5);
        addMember(bottomLayout);
		
        VLayout formLayout = new VLayout();
        formLayout.setWidth100();
        formLayout.setHeight100();
        formLayout.setMembersMargin(5);
        bottomLayout.addMember(formLayout);
        
		form = new LocationForm();
		formLayout.addMember(form);
        
		buttonLayout = new HLayout();
		buttonLayout.setWidth100();
		buttonLayout.setHeight(25);
		buttonLayout.setMargin(10);
		buttonLayout.setMembersMargin(17);
		buttonLayout.hide();
		formLayout.addMember(buttonLayout);
		
		IButton saveButton = new IButton("Save");
		saveButton.setHeight(25);
		saveButton.setWidth(75);
		saveButton.setIcon("/theme/images/icons16/save.png");
		saveButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Record record = form.getValuesAsRecord();
				record.setAttribute("qname", "openapps_org_repository_1_0_location");
				String name = "";
				if(form.getValue("building") != null) name += form.getValue("building");
				if(form.getValue("floor") != null) name += " "+form.getValue("floor");
				if(form.getValue("aisle") != null) name += " "+form.getValue("aisle");
				if(form.getValue("bay") != null) name += " "+form.getValue("bay");
				record.setAttribute("name", name.trim());
				EntityServiceDS.getInstance().updateEntity(record, new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						if(response.getData() != null && response.getData().length > 0) {
							form.editRecord(response.getData()[0]);
							locationGrid.updateData(response.getData()[0]);
							searchGrid.updateData(response.getData()[0]);
							message.setContents("location saved successfully");
							if(searchGrid.getSelectedRecord() != null) {
								searchGrid.getSelectedRecord().setAttribute("name", response.getData()[0].getAttribute("name"));
								searchGrid.refreshFields();
							}
							if(locationGrid.getSelectedRecord() != null) 
								locationGrid.getSelectedRecord().setAttribute("name", response.getData()[0].getAttribute("name"));
								locationGrid.refreshFields();
						}
					}
				});
			}
		});
		buttonLayout.addMember(saveButton);
		
		message = new Label();
		message.setHeight(25);
		message.setWidth100();
		buttonLayout.addMember(message);
		
		
		/** Space Display **/
		VLayout spacerLayout = new VLayout();
		spacerLayout.setWidth100();
		spacerLayout.setHeight100();
		spacerLayout.setBorder("1px solid #C0C3C7");
		bottomLayout.addMember(spacerLayout);
		/*
		spacerLabel = new Label();
		spacerLabel.setWidth(250);
		spacerLabel.setHeight(50);
		spacerLabel.setAlign(Alignment.CENTER);
		spacerLayout.addMember(spacerLabel);
		
		spacerViewer = new HLayout();
		spacerViewer.setWidth100();
		spacerViewer.setHeight(50);		
		spacerViewer.setLayoutTopMargin(10);
		spacerViewer.setLayoutLeftMargin(10);
		spacerViewer.setMembersMargin(1);
		spacerLayout.addMember(spacerViewer);
		*/
		addWindow = new AddLocationWindow();
		addWindow.setCallback(new DSCallback() {	
			@Override
			public void execute(DSResponse dsResponse, Object data,	DSRequest dsRequest) {
				if(dsResponse.getData() != null && dsResponse.getData().length > 0) {
					searchGrid.addData(dsResponse.getData()[0]);
				}
			}			
		});
	}
	public void selectLocation(Record record) {
		form.editRecord(record);
		toolbar.select(record);
		if(settings.getUser().isAdmin() || settings.getUser().isArchiveManager()) {
			buttonLayout.show();
			message.setContents("");
		}
		/*
		int space = 0;
		spacerViewer.removeMembers(spacerViewer.getMembers());
		spacerLabel.setContents("<b>Spaces</b><br/>"+space+" % filled");
		for(int i=0; i < 20; i++) {
			Canvas c = new Canvas();
			c.setWidth(20);
			c.setHeight(50);
			c.setBorder("1px solid black");
			if(i*5 >= space)	c.setBackgroundColor("red");
			else c.setBackgroundColor("green");
			spacerViewer.addMember(c);
		}
		*/
	}
	public void select(Record record) {	
		this.source = record;
		try {
			Record[] notes = new Record[0];
			Record[] source_associations = record.getAttributeAsRecordArray("source_associations");
			if(source_associations != null) {				
				for(Record source_assoc : source_associations) {					
					Record notesRecord = source_assoc.getAttributeAsRecord("locations");
					if(notesRecord != null) {
						notes = source_assoc.getAttributeAsRecordArray("locations");
					}
				}
				locationGrid.setData(notes);
			}
			if(settings.getUser().isArchiveManager()) {
				rightArrowImg.show();
				leftArrowImg.show();
			}
		} catch(ClassCastException e) {
			locationGrid.setData(new Record[0]);
		}
	}
	public void search(Criteria criteria) {
		action = criteria.getAttribute("action");
		if(action != null) {
			if(criteria.getAttribute("query") != null) query = criteria.getAttribute("query");
			else criteria.setAttribute("query", query);
			if(action.equals("next")) {
				startRow = startRow + 10;
			} else if(action.equals("last")) {
				startRow = total - 10;
			} else if(action.equals("prev")) {
				startRow = startRow - 10;
			} else if(action.equals("first") || action == null) {
				startRow = 0;
			}
		} else {
			query = criteria.getAttribute("query");
			startRow = 0;
		}
		criteria.setAttribute("_startRow", startRow);
		criteria.setAttribute("_endRow", startRow + 10);
		criteria.setAttribute("qname", qname);
		searchGrid.setData(new Record[0]);
		EntityServiceDS.getInstance().search(criteria, new DSCallback() {
			@Override
			public void execute(DSResponse dsResponse, Object data,	DSRequest dsRequest) {
				searchGrid.setData(dsResponse.getData());
				startRow = dsResponse.getStartRow();
				total = dsResponse.getTotalRows();				
				paging.setPagingData(dsResponse.getStartRow(), dsResponse.getEndRow(), dsResponse.getTotalRows());
				paging.show();
			}						
		});
	}
}
