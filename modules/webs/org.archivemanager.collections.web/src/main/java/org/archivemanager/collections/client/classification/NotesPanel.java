package org.archivemanager.collections.client.classification;

import org.archivemanager.collections.client.ArchiveManager;
import org.archivemanager.collections.client.classification.form.NoteForm;
import org.heed.openapps.gwt.client.data.EntityServiceDS;

import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class NotesPanel extends VLayout {
	private ArchiveManager settings = new ArchiveManager();
	
	private NoteForm form;
	
	private HLayout buttonLayout;
	private Label message;
	
	public NotesPanel() {
		setWidth100();
		
		form = new NoteForm();	
		addMember(form);
		
		buttonLayout = new HLayout();
		buttonLayout.setWidth100();
		buttonLayout.setHeight(25);
		buttonLayout.setMargin(10);		
		buttonLayout.setMembersMargin(17);
		buttonLayout.hide();		
		addMember(buttonLayout);
		
		IButton saveButton = new IButton("Save");
		saveButton.setHeight(25);
		saveButton.setWidth(75);
		saveButton.setIcon("/theme/images/icons16/save.png");
		saveButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				EntityServiceDS.getInstance().updateEntity(form.getValuesAsRecord(), new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						if(response.getData() != null && response.getData().length > 0) {
							form.editRecord(response.getData()[0]);
							message.setContents("note saved successfully");
						}
					}
				});
			}
		});
		buttonLayout.addMember(saveButton);
		
		message = new Label();
		message.setHeight(25);
		message.setWidth100();
		buttonLayout.addMember(message);
	}
	
	public void select(Record record) {
		form.editRecord(record);
		if(settings.getUser().isArchiveManager()) {
			buttonLayout.show();
		}
		message.setContents("");
	}
}
