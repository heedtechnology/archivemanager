package org.archivemanager.collections.client.classification;
import java.util.LinkedHashMap;

import org.archivemanager.collections.client.ArchiveManager;
import org.archivemanager.collections.client.components.NamedEntityComponent;
import org.archivemanager.collections.client.components.SubjectComponent;
import org.archivemanager.gwt.client.NativeClassificationModel;
import org.heed.openapps.gwt.client.data.EntityServiceDS;
import org.heed.openapps.gwt.client.event.SaveListener;
import org.heed.openapps.gwt.client.event.SelectionListener;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;


public class ClassificationView extends VLayout {
	private ArchiveManager settings = new ArchiveManager();	
	private NativeClassificationModel model = new NativeClassificationModel();
	private NamedEntityComponent nameComponent;
	private SubjectComponent subjectComponent;
	private HLayout toolbar;
	private DynamicForm searchForm;
	private ClassificationGrid grid;
	private TabSet tabs;
	private DetailPanel detailPanel;
	//private SubjectPanel subjectPanel;
	//private ContentAssociationPanel contentPanel;
	private EntityEntryPanel entriesPanel;
	private NotesPanel notesPanel;
	
		
	public ClassificationView() {
		setHeight100();  
		setWidth100();
		
		HLayout topLayout = new HLayout();
		topLayout.setWidth100();
		topLayout.setHeight(200);
		addMember(topLayout);
		
		VLayout leftLayout = new VLayout();
		leftLayout.setWidth100();
		leftLayout.setMembersMargin(5);
		topLayout.addMember(leftLayout);
		
		nameComponent = new NamedEntityComponent("100%", true, true, true, model.getNamedEntityFunction(), model.getNamedEntityRole());
		nameComponent.setMargin(5);
		nameComponent.setSelectionListener(new SelectionListener() {
			@Override
			public void select(Record record) {
				String target_id = record.getAttribute("target_id");
				if(target_id != null) record.setAttribute("id", target_id);
            	selectClassification(record);
			}			
		});
		leftLayout.addMember(nameComponent);
				
		VLayout rightLayout = new VLayout();
		rightLayout.setWidth("325px");
		rightLayout.setMembersMargin(5);
		topLayout.addMember(rightLayout);
		
		subjectComponent = new SubjectComponent("100%", true);
		subjectComponent.setMargin(5);
		subjectComponent.setSelectionListener(new SelectionListener() {
			@Override
			public void select(Record record) {
				String target_id = record.getAttribute("target_id");
				if(target_id != null) record.setAttribute("id", target_id);
            	selectClassification(record);
			}			
		});
		rightLayout.addMember(subjectComponent);
		
		
		/** Classification Manager **/
		toolbar = new HLayout();
		toolbar.setWidth100();
		toolbar.setHeight(25);
		//toolbar.setBorder("1px solid #C0C3C7;");
        //toolbar.setLayoutLeftMargin(32);
        
        searchForm = new DynamicForm();
		searchForm.setWidth(300);
		searchForm.setHeight(25);
		searchForm.setMargin(0);
		searchForm.setNumCols(4);
		searchForm.setCellPadding(2);
		
		TextItem searchField = new TextItem("query");
		searchField.setShowTitle(false);
		searchField.setWidth(255);
		searchField.addKeyPressHandler(new KeyPressHandler() {
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if(event.getKeyName().equals("Enter")) {
					Criteria record = new Criteria();					
					search(record);
				}				
			}
			
		});
		ButtonItem searchButton = new ButtonItem("search", "search");
		searchButton.setWidth(50);
		searchButton.setStartRow(false);
		searchButton.setEndRow(false);
		searchButton.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
				Criteria record = new Criteria();
				search(record);
			}
		});
		toolbar.addMember(searchForm);
		toolbar.setLayoutAlign(Alignment.RIGHT);
		
		SelectItem classificationSelect = new SelectItem("classification");
		classificationSelect.setWidth(100);
		LinkedHashMap<String,String> valueMap1 = new LinkedHashMap<String,String>();
		valueMap1.put("openapps_org_classification_1_0_person","Person");
		valueMap1.put("openapps_org_classification_1_0_corporation","Corporate");
		valueMap1.put("openapps_org_classification_1_0_subject","Subject");
		classificationSelect.setValueMap(valueMap1);
		classificationSelect.setDefaultValue("openapps_org_classification_1_0_person");
		classificationSelect.setShowTitle(false);
		
		searchForm.setFields(searchField,searchButton,classificationSelect);
				               
        HLayout mainLayout = new HLayout();
        mainLayout.setWidth100();
        mainLayout.setHeight100();
        mainLayout.setMembersMargin(2);
        mainLayout.setMargin(2);
        addMember(mainLayout);
        
		grid = new ClassificationGrid();
		grid.setWidth(300);
		grid.addCellClickHandler(new CellClickHandler() {
			@Override
			public void onCellClick(CellClickEvent event) {
				Record record = grid.getSelectedRecord();
				selectClassification(record);
			}			
		});
		mainLayout.addMember(grid);
        
		VLayout tabLayout = new VLayout();
		tabLayout.setWidth100();
		tabLayout.setHeight100();
		mainLayout.addMember(tabLayout);
		
		tabLayout.addMember(toolbar);
		
        tabs = new TabSet();
		tabs.setWidth100();
		tabs.setHeight100();
		tabLayout.addMember(tabs);
		        
        Tab detailTab = new Tab("Detail");
        detailPanel = new DetailPanel();
        detailPanel.setSaveListener(new SaveListener() {
			@Override
			public void save(Record record) {
				String date = record.getAttribute("name");
				String title = grid.getSelectedRecord().getAttribute("name");
				if(title != null && !title.equals(date)) {
					grid.getSelectedRecord().setAttribute("name", date);
					grid.refreshFields();
				}
			}
        });
        detailTab.setPane(detailPanel);
        tabs.addTab(detailTab);
        
        Tab noteTab = new Tab("Notes");
        notesPanel = new NotesPanel();
        notesPanel.setTitle("Notes");
        noteTab.setPane(notesPanel);
        tabs.addTab(noteTab);
        
        Tab entryTab = new Tab("Entries");
        entriesPanel = new EntityEntryPanel();
        entriesPanel.setAddCallback(new DSCallback() {
        	@Override
			public void execute(DSResponse dsResponse, Object data,	DSRequest dsRequest) {
        		if(dsResponse.getData() != null && dsResponse.getData().length > 0) {
        			selectClassification(grid.getSelectedRecord());
        		}
			}        	
        });
        entriesPanel.setTitle("Entries");
        entryTab.setPane(entriesPanel);
        tabs.addTab(entryTab);
        
	}
	public void select(Record record) {
		nameComponent.select(record);
		subjectComponent.select(record);
		detailPanel.select(record);
		notesPanel.select(record);
		entriesPanel.select(record);
		if(settings.getUser().isAdmin() || settings.getUser().isArchiveManager()) {
			nameComponent.canEdit(true);
			subjectComponent.canEdit(true);
		}
	}
	public void selectClassification(Record record) {
		Criteria criteria = new Criteria();
		criteria.setAttribute("id", record.getAttribute("id"));
		criteria.setAttribute("sources", true);
		criteria.setAttribute("targets", true);
		EntityServiceDS.getInstance().getEntity(criteria, new DSCallback() {
			public void execute(DSResponse dsResponse, Object data, DSRequest dsRequest) {
				if(dsResponse.getData() != null && dsResponse.getData().length > 0) {
					Record record = dsResponse.getData()[0];
					String qname = record.getAttribute("qname");	
					if(qname != null) {
						if(qname.equals("openapps_org_classification_1_0_subject")) {
							hideTab(entriesPanel);
						} else {
							showTab(entriesPanel);
						}
						detailPanel.select(record);
						notesPanel.select(record);
						entriesPanel.select(record);
					}
					String date = record.getAttribute("name");
					String title = grid.getSelectedRecord().getAttribute("name");
					if(title != null && !title.equals(date)) {
						grid.getSelectedRecord().setAttribute("name", date);
						grid.refreshFields();
					}
				}
			}			
		});
			
	}
	public void canEdit(boolean editor) {
		nameComponent.canEdit(editor);
		subjectComponent.canEdit(editor);
	}
	public void search(Criteria criteria) {
		criteria.setAttribute("qname", searchForm.getValue("classification"));
		criteria.setAttribute("query", searchForm.getValue("query"));
		grid.search(criteria);
	}
	
	protected void hideTab(Canvas canvas) {
		for(Tab tab : tabs.getTabs()) {
			if(tab.getPane().equals(canvas)) {
				tabs.updateTab(tab, null);
				tabs.removeTab(tab);
			}
		}
	}
	protected void showTab(Canvas canvas) {
		boolean exists = false;
		for(Tab tab : tabs.getTabs()) {
			if(tab.getPane().equals(canvas))
				exists = true;
		}
		if(!exists) {
			Tab t = new Tab(canvas.getTitle());
			t.setPane(canvas);
			tabs.addTab(t);
		}
	}	
}