package org.archivemanager.collections.client.locations;

import java.util.LinkedHashMap;

import com.google.gwt.core.client.JavaScriptObject;
import com.smartgwt.client.util.JSOHelper;

public class NativeLocationModel {

	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getLocationTypes() { 
		JavaScriptObject obj = getNativeLocationTypes();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>();
	}	
	private final native JavaScriptObject getNativeLocationTypes() /*-{
        return $wnd.location_types;
	}-*/;
}
