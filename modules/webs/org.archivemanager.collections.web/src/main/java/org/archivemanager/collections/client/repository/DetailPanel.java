package org.archivemanager.collections.client.repository;

import org.archivemanager.collections.client.ArchiveManager;
import org.heed.openapps.gwt.client.data.EntityServiceDS;

import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class DetailPanel extends HLayout {
	private ArchiveManager settings = new ArchiveManager();
	private RepositoryForm repositoryForm;
	
	private Label message;
	private CollectionComponent collectionComponent;
	private HLayout buttonLayout;
	
	
	public DetailPanel() {
		setWidth100();
		setHeight100();
		
		VLayout leftLayout = new VLayout();
		leftLayout.setWidth("50%");
		leftLayout.setHeight100();
		addMember(leftLayout);
		
		VLayout rightLayout = new VLayout();
		rightLayout.setWidth("50%");
		rightLayout.setHeight100();
		addMember(rightLayout);
		
		
		/** Left Side **/
		repositoryForm = new RepositoryForm();
		leftLayout.addMember(repositoryForm);
		
		buttonLayout = new HLayout();
		buttonLayout.setWidth100();
		buttonLayout.setHeight(25);
		buttonLayout.setMargin(5);
		buttonLayout.setMembersMargin(17);
		buttonLayout.hide();
		leftLayout.addMember(buttonLayout);
		
		IButton saveButton = new IButton("Save");
		saveButton.setHeight(25);
		saveButton.setWidth(75);
		saveButton.setIcon("/theme/images/icons16/save.png");
		saveButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Record record = repositoryForm.getValuesAsRecord();				
				EntityServiceDS.getInstance().updateEntity(record, new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						Record[] resultData = response.getData();
						if(resultData != null && resultData.length > 0 && resultData[0].getAttribute("id") != null) {
							message.setContents("repository saved successfully");
						} else {
							message.setContents("there was a problem saving the repository");
						}
					}
				});
			}
		});
		buttonLayout.addMember(saveButton);
		
		message = new Label();
		message.setHeight(25);
		message.setWidth100();
		buttonLayout.addMember(message);
		
		
		/** Right Side **/
		collectionComponent = new CollectionComponent(true);
		collectionComponent.setWidth100();
		collectionComponent.setMargin(5);
		rightLayout.addMember(collectionComponent);
	}
	public void select(Record record) {
		repositoryForm.editRecord(record);
		collectionComponent.select(record);
		if(settings.getUser().isArchiveManager()) {
			buttonLayout.show();
		}
	}
	
}
