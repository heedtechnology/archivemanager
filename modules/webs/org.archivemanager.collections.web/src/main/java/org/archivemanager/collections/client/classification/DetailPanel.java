package org.archivemanager.collections.client.classification;

import org.archivemanager.collections.client.ArchiveManager;
import org.archivemanager.collections.client.classification.form.CorporationForm;
import org.archivemanager.collections.client.classification.form.PersonForm;
import org.archivemanager.collections.client.classification.form.SubjectForm;
import org.heed.openapps.gwt.client.data.EntityServiceDS;
import org.heed.openapps.gwt.client.event.SaveListener;

import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class DetailPanel extends VLayout {
	private ArchiveManager settings = new ArchiveManager();
	private PersonForm personForm;
	private CorporationForm corpForm;
	private SubjectForm subjForm;
	private HLayout buttonLayout;
	
	private Label message;
	private SaveListener saveListener;
	
	public DetailPanel() {
		setWidth100();
		setHeight100();
		//setMargin(10);
		
		personForm = new PersonForm();
		corpForm = new CorporationForm();		
		subjForm = new SubjectForm();
		
		corpForm.hide();
		subjForm.hide();
		
		addMember(personForm);
		addMember(corpForm);
		addMember(subjForm);
		
		buttonLayout = new HLayout();
		buttonLayout.setWidth100();
		buttonLayout.setHeight(25);
		buttonLayout.setMargin(10);
		buttonLayout.setMembersMargin(17);
		buttonLayout.hide();
		addMember(buttonLayout);
		
		IButton saveButton = new IButton("Save");
		saveButton.setHeight(25);
		saveButton.setWidth(75);
		saveButton.setIcon("/theme/images/icons16/save.png");
		saveButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Record record = null;
				if(personForm.isVisible()) record = personForm.getValuesAsRecord();
				else if(corpForm.isVisible()) record = corpForm.getValuesAsRecord();
				else if(subjForm.isVisible()) record = subjForm.getValuesAsRecord();
				if(record != null) {
					EntityServiceDS.getInstance().updateEntity(record, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							if(response.getData() != null && response.getData().length > 0) {
								if(personForm.isVisible()) {
									personForm.editRecord(response.getData()[0]);
									message.setContents("person saved successfully");
								} else if(corpForm.isVisible()) {
									corpForm.editRecord(response.getData()[0]);
									message.setContents("corporation saved successfully");
								} else if(subjForm.isVisible()) {
									subjForm.editRecord(response.getData()[0]);
									message.setContents("subject saved successfully");
								}
								saveListener.save(response.getData()[0]);								
							}
						}
					});
				}
			}
		});
		buttonLayout.addMember(saveButton);
		
		message = new Label();
		message.setHeight(25);
		message.setWidth100();
		buttonLayout.addMember(message);
	}
	
	public void select(Record selection) {
		String type = selection.getAttribute("localName");		
		if(type != null) {				
			subjForm.hide();
			corpForm.hide();
			personForm.hide();
			if(type.equals("person")) {
				personForm.editRecord(selection);
				personForm.show();
			} else if(type.equals("corporation")) {
				corpForm.editRecord(selection);
				corpForm.show();
			} else if(type.equals("subject")) {
				subjForm.editRecord(selection);
				subjForm.show();
			} else {
				personForm.show();
			}
			if(settings.getUser().isAdmin() || settings.getUser().isArchiveManager()) {
				buttonLayout.show();
			}
		}
		message.setContents("");
	}

	public void setSaveListener(SaveListener saveListener) {
		this.saveListener = saveListener;
	}
	
}
