package org.archivemanager.collections.client.components;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.HTMLPane;

public class HTMLLoadingPanel extends Canvas {
	private Record selection;
	private HTMLPane flow;
	
	public HTMLLoadingPanel() {
		setWidth100();
		setHeight100();
		flow = new HTMLPane();
		flow.setWidth100();
		flow.setHeight100();
		addChild(flow);
	}
	
	public void select(Record record) {		
		if(this.selection == null || !this.selection.getAttribute("id").equals(record.getAttribute("id"))) {
			String type = record.getAttribute("localName");
			flow.setContentsURL("/collections/splash/"+type);			
		}
		this.selection = record;
	}
}
