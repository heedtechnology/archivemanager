package org.archivemanager.collections.client.donors.form;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.archivemanager.collections.client.donors.data.NativeContactModel;

import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.SpacerItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextItem;


public class OrganizationForm extends DynamicForm {
	private NativeContactModel model = new NativeContactModel();
	
	private static final int FIELD_WIDTH = 225;
	
	
	public OrganizationForm() {
		setWidth100();
		setNumCols(4);
		setCellPadding(5);
		setColWidths(100, "*", 125, "*");
		
		List<FormItem> fields = new ArrayList<FormItem>();
		
		StaticTextItem idField = new StaticTextItem("id", "ID");
		fields.add(idField);
		
		SpacerItem spacer = new SpacerItem();
		fields.add(spacer);
		
		SelectItem statusField = new SelectItem("status", "Status");
		statusField.setWidth(90);
		LinkedHashMap<String,String> valueMap = new LinkedHashMap<String,String>();
		valueMap.put("active", "Active");
		valueMap.put("inactive", "Inactive");
		valueMap.put("general", "General");
		statusField.setValueMap(valueMap);
		fields.add(statusField);
		
		DateItem lastWroteItem = new DateItem("last_wrote", "Last Wrote");
		lastWroteItem.setDateFormatter(DateDisplayFormat.TOSERIALIZEABLEDATE);
		fields.add(lastWroteItem);
		
		SelectItem typeField = new SelectItem("type", "Type");
		typeField.setWidth(120);
		valueMap = new LinkedHashMap<String,String>();
		valueMap.put("collectee", "Active");
		valueMap.put("donor", "Donor");
		valueMap.put("book_collection", "Book Collection");
		typeField.setValueMap(valueMap);
		fields.add(typeField);
		
		DateItem lastShipItem = new DateItem("last_ship", "Last Shipped");
		lastShipItem.setDateFormatter(DateDisplayFormat.TOSERIALIZEABLEDATE);
		fields.add(lastShipItem);
		
		SelectItem qnameField = new SelectItem("qname", "Contact Type");
		qnameField.setWidth(120);
		qnameField.setValueMap(model.getSearchTypes());
		fields.add(qnameField);
		
		SpacerItem spacer2 = new SpacerItem();
		fields.add(spacer2);
		
		TextItem firstNameField = new TextItem("name", "Name");
		firstNameField.setWidth(FIELD_WIDTH);
		fields.add(firstNameField);
		
		SpacerItem spacer3 = new SpacerItem();
		fields.add(spacer3);
		
		TextItem classificationField = new TextItem("classification", "Classification");
		classificationField.setWidth(FIELD_WIDTH);
		fields.add(classificationField);
		
		SpacerItem spacer4 = new SpacerItem();
		fields.add(spacer4);
		
		TextItem faxField = new TextItem("fax1", "Fax Number");
		faxField.setWidth(FIELD_WIDTH);
		fields.add(faxField);
		
		SpacerItem spacer5 = new SpacerItem();
		fields.add(spacer5);
		
		TextItem dateListField = new TextItem("dateList", "Date List");
		dateListField.setWidth(FIELD_WIDTH);
		fields.add(dateListField);
		
		FormItem[] itemArray = fields.toArray(new FormItem[fields.size()]);		
		setFields(itemArray);
	}
	
	
}
