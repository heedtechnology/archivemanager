package org.archivemanager.collections.client.donors;

import org.archivemanager.collections.client.ArchiveManager;
import org.archivemanager.collections.client.Searchable;
import org.archivemanager.collections.client.components.PagingDisplay;
import org.archivemanager.collections.client.donors.component.AddContactWindow;
import org.archivemanager.collections.client.donors.component.DonorManagerToolbar;
import org.heed.openapps.gwt.client.data.EntityServiceDS;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;


public class DonorView extends VLayout implements Searchable {
	private ArchiveManager settings = new ArchiveManager();
	private DonorManagerToolbar toolbar;
	private ListGrid contactList;
	private AddContactWindow addWindow;
	private DetailPanel detailPanel;
	private ContactPanel contactPanel;
	private NotesPanel notesPanel;
	private ListGrid donorGrid;
	private PagingDisplay paging;
	private VLayout vStack;
	private ImgButton rightArrowImg;
	private ImgButton leftArrowImg;
	
	private Record source;
	int startRow;
	int total;
	String action;
	String query;
	String qname;
	
	
	public DonorView() {		
		HLayout topLayout = new HLayout();
		topLayout.setWidth100();
		topLayout.setHeight("290px");
		addMember(topLayout);
		
		VLayout donorViewer = new VLayout();
		donorViewer.setHeight100();
		donorViewer.setWidth100();
		donorViewer.setMargin(5);
		topLayout.addMember(donorViewer);
		
		vStack = new VLayout(5);  
        vStack.setHeight100();
        vStack.setWidth("40px");
        vStack.setMembersMargin(5);
        topLayout.addMember(vStack);
        
		VLayout donorSearch = new VLayout();
		donorSearch.setHeight100();
		donorSearch.setWidth100();
		donorSearch.setMargin(5);
		topLayout.addMember(donorSearch);
			
		HLayout toolstrip = new HLayout();
		toolstrip.setWidth100();
		toolstrip.setHeight(25);
		donorViewer.addMember(toolstrip);
		
		Label label = new Label("<label style='font-size:13px;font-weight:bold;color:#4C4C4C;font-family:Arial,Verdana,sans-serif;'>Current Donors</label>");
		label.setHeight("100%");
		label.setWidth("100%");
		toolstrip.addMember(label);
		
		donorGrid = new ListGrid();
		donorGrid.setWidth(345);
		donorGrid.setHeight100();
		donorGrid.setShowHeader(false);
		donorGrid.setAutoFitData(Autofit.VERTICAL);
		donorGrid.setWrapCells(true);
		donorGrid.setFixedRecordHeights(false);
		donorGrid.addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				Record record = donorGrid.getSelectedRecord();
				Criteria criteria = new Criteria();
				String target_id = record.getAttribute("target_id");
				if(target_id == null) target_id = record.getAttribute("id");
            	criteria.setAttribute("id", target_id);
            	criteria.setAttribute("sources", true);
            	criteria.setAttribute("targets", true);
            	EntityServiceDS.getInstance().getEntity(criteria, new DSCallback() {
            		public void execute(DSResponse dsResponse, Object data, DSRequest dsRequest) {
            			if(dsResponse.getData() != null && dsResponse.getData().length > 0) {
            				Record record = dsResponse.getData()[0];
            				selectContact(record);
            				contactList.deselectAllRecords();
            			}
            		}			
            	});
			}
		});
		
		ListGridField dateField = new ListGridField("name","Name");
		
		donorGrid.setFields(dateField);
		donorViewer.addMember(donorGrid);
		
		Canvas spacerCanvas = new Canvas();
        spacerCanvas.setHeight("125px");
        vStack.addMember(spacerCanvas);
        
        leftArrowImg = new ImgButton();
        leftArrowImg.setWidth(36);
        leftArrowImg.setHeight(36);
        leftArrowImg.setMargin(5);
        leftArrowImg.setSrc("[SKIN]/TransferIcons/left.png");
        leftArrowImg.hide();
        leftArrowImg.addClickHandler(new ClickHandler() {  
            public void onClick(ClickEvent event) {  
            	final Record record = contactList.getSelectedRecord();
            	if(record != null && source != null) {
	            	Record rec = new Record();
	            	String target_id = record.getAttribute("target_id");
					if(target_id == null) target_id = record.getAttribute("id");
	            	rec.setAttribute("source", source.getAttribute("id"));
	            	rec.setAttribute("target", target_id);
	            	rec.setAttribute("assoc_qname", "openapps_org_contact_1_0_contacts");
					rec.setAttribute("entity_qname", "openapps_org_contact_1_0_contact");				            	
					EntityServiceDS.getInstance().addAssociation(rec, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {						
							Record[] resultData = response.getData();
							if(resultData != null && resultData.length > 0 && resultData[0].getAttribute("id") != null) {
								contactList.removeSelectedData();
								select(resultData[0]);
								//message.setContents("donor saved successfully");
							} else {
								//message.setContents("there was a problem saving the donor");
							}
						}
					});
            	} else {
            		if(source == null) SC.warn("Error adding donor, missing valid collection");
            		else SC.warn("Error adding donor, missing valid donor");
            	}
            }  
        }); 
        vStack.addMember(leftArrowImg);
        
		rightArrowImg = new ImgButton();
		rightArrowImg.setWidth(36);
		rightArrowImg.setHeight(36);
		rightArrowImg.setMargin(5);
		rightArrowImg.setSrc("[SKIN]/TransferIcons/right.png");
		rightArrowImg.hide();
		rightArrowImg.addClickHandler(new ClickHandler() {  
            public void onClick(ClickEvent event) { 
            	final Record record = donorGrid.getSelectedRecord();
            	Record rec = new Record();
            	rec.setAttribute("id", record.getAttribute("id"));
            	EntityServiceDS.getInstance().removeAssociation(rec, new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						donorGrid.removeSelectedData();
						contactList.addData(record);
					}
				}); 
            }  
        });  
        vStack.addMember(rightArrowImg);
		
		toolbar = new DonorManagerToolbar(this);
		toolbar.setDeleteCallback(new DSCallback() {
			@Override
			public void execute(DSResponse dsResponse, Object data, DSRequest dsRequest) {
				Record rec = new Record();
				String id = contactList.getSelectedRecord().getAttribute("target_id");
				if(id == null) id = contactList.getSelectedRecord().getAttribute("id");
				rec.setAttribute("id", id);					
				EntityServiceDS.getInstance().removeEntity(rec, new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						contactList.removeSelectedData();	
					}
				});
			}			
		});
		toolbar.setAddCallback(new DSCallback() {
			@Override
			public void execute(DSResponse dsResponse, Object data, DSRequest dsRequest) {
				addWindow.show();
			}			
		});
		donorSearch.addMember(toolbar);
                
        contactList = new ListGrid();
        contactList.setWidth100();
        contactList.setHeight100();
        contactList.setAutoFitData(Autofit.VERTICAL);
        contactList.setWrapCells(true);
        contactList.setFixedRecordHeights(false);
        contactList.setShowHeader(false);
        ListGridField nameField = new ListGridField("name","Name");
        contactList.setFields(nameField);
        contactList.addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				Record record = contactList.getSelectedRecord();
				Criteria criteria = new Criteria();
				String target_id = record.getAttribute("target_id");
				if(target_id == null) target_id = record.getAttribute("id");
            	criteria.setAttribute("id", target_id);
            	criteria.setAttribute("sources", true);
            	criteria.setAttribute("targets", true);
            	EntityServiceDS.getInstance().getEntity(criteria, new DSCallback() {
            		public void execute(DSResponse dsResponse, Object data, DSRequest dsRequest) {
            			if(dsResponse.getData() != null && dsResponse.getData().length > 0) {
            				Record record = dsResponse.getData()[0];
            				selectContact(record);
            				donorGrid.deselectAllRecords();
            			}
            		}			
            	});
			}
		});
        donorSearch.addMember(contactList);
        
        /** Paging **/
		paging = new PagingDisplay(this);
		paging.hide();
		donorSearch.addMember(paging);
        
        Canvas mainLayout = new Canvas();
        mainLayout.setWidth100();
        mainLayout.setHeight100();
        addMember(mainLayout);
        
        TabSet tabs = new TabSet();
        tabs.setWidth100();
        tabs.setHeight100();
        
        detailPanel = new DetailPanel();
        Tab detailsTab = new Tab("Details");
        detailsTab.setPane(detailPanel);
		tabs.addTab(detailsTab);
        
		contactPanel = new ContactPanel();
        Tab contactTab = new Tab("Contact Information");
        contactTab.setPane(contactPanel);
		tabs.addTab(contactTab);
        
        notesPanel = new NotesPanel();        
        Tab notesTab = new Tab("Notes");
        notesTab.setPane(notesPanel);
		tabs.addTab(notesTab);
        
		/*
        correspondencePanel = new CorrespondencePanel();
        Tab correspondenceTab = new Tab("Correspondence");
        correspondenceTab.setPane(correspondencePanel);
		tabs.addTab(correspondenceTab);
        mainLayout.addChild(tabs);
                		
        digitalObjectPanel = new DigitalObjectPanel();
        Tab digitalObjectTab = new Tab("Digital Objects");
        digitalObjectTab.setPane(digitalObjectPanel);
		tabs.addTab(digitalObjectTab);
        mainLayout.addChild(tabs);
        */
		mainLayout.addChild(tabs);
		
		addWindow = new AddContactWindow();
		addWindow.setCallback(new DSCallback() {	
			@Override
			public void execute(DSResponse dsResponse, Object data,	DSRequest dsRequest) {
				if(dsResponse.getData() != null && dsResponse.getData().length > 0) {
					contactList.addData(dsResponse.getData()[0]);
				}
			}			
		});
		/*
        EventBus.addHandler(org.heed.openapps.gwt.client.OpenAppsEvent.TYPE, new OpenAppsEventHandler()     {
	        @Override
	        public void onEvent(OpenAppsEvent event) {
	            if(event.isType(EventTypes.SEARCH)) {
	            	Criteria criteria = new Criteria();
	        		criteria.setAttribute("qname", event.getRecord().getAttribute("qname"));
	        		criteria.setAttribute("query", event.getRecord().getAttribute("query"));
	        		criteria.setAttribute("field", "name");
	        		criteria.setAttribute("sort", "name_e");
	        		criteria.setAttribute("sources", "false");
	        		contactList.fetchData(criteria);
	            } else if(event.isType(EventTypes.SELECTION)) {
	            	String name = event.getRecord().getAttribute("name");
	            	if(name == null || name.length() == 0) {
						String first_name = event.getRecord().getAttribute("first_name");
						String last_name = event.getRecord().getAttribute("last_name");
						event.getRecord().setAttribute("name", first_name+" "+last_name);
					}
	            	selection = event.getRecord();
	            } else if(event.isType(DonorEventTypes.ADD_DONOR)) {
	            	contactList.addData(event.getRecord(), new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							
						}
					});
	            } else if(event.isType(EventTypes.ADD)) {
	            	addWindow.show();
	            } else if(event.isType(EventTypes.DELETE)) {
	            	contactList.removeSelectedData();
	            } else if(event.isType(EventTypes.SAVE)) {
	 	            if(selection != null) {
	 	            	String qname = selection.getAttribute("qname");
	 	            	if(qname != null) {
	 	            		Record record = null;
		            		if(qname.equals("openapps_org_contact_1_0_individual")) {
		            			record = new Record(detailPanel.getIndividualForm().getValues());
		            			for(FormItem item : detailPanel.getIndividualForm().getFields()) {
		            				if(item.getForm().isVisible() && item.getVisible()) {
		            					if(item instanceof DateItem) {
		            						Date value = ((DateItem)item).getValueAsDate();
		            						record.setAttribute(item.getName(), DateTimeFormat.getShortDateFormat().format(value));
		            					} else {
		            						if(item.getValue() != null) record.setAttribute(item.getName(), item.getValue());
		            					}
		            				}
		            			}
		            		} else if(qname.equals("openapps_org_contact_1_0_organization")) {
		            			record = new Record(detailPanel.getOrganizationForm().getValues());
		            			for(FormItem item : detailPanel.getOrganizationForm().getFields()) {
		            				if(item.getForm().isVisible() && item.getVisible()) {
		            					if(item instanceof DateItem) {
		            						Date value = ((DateItem)item).getValueAsDate();
		            						record.setAttribute(item.getName(), DateTimeFormat.getShortDateFormat().format(value));
		            					} else {
		            						if(item.getValue() != null) record.setAttribute(item.getName(), item.getValue());
		            					}
		            				}
		            			}
		            		}
		            		if(record != null) {
		            			try {
		            				Date date = record.getAttributeAsDate("birth_date");
		            				String birth_date = ContactDataSource.getShortFormattedDate(date);
		            				record.setAttribute("birth_date", birth_date);
		            			} catch(Exception e) { record.setAttribute("birth_date", ""); }
		            			try {
		            				Date date = record.getAttributeAsDate("death_date");
		            				String death_date = ContactDataSource.getShortFormattedDate(record.getAttributeAsDate("death_date"));
			            			record.setAttribute("death_date", death_date);
		            			} catch(Exception e) { record.setAttribute("death_date", ""); }
		            			try {
		            				Date date = record.getAttributeAsDate("last_wrote");
		            				String last_wrote = ContactDataSource.getShortFormattedDate(date);
			            			record.setAttribute("last_wrote", last_wrote);
		            			} catch(Exception e) { record.setAttribute("last_wrote", ""); }
		            			try {
		            				Date date = record.getAttributeAsDate("last_ship");
		            				String last_shipped = ContactDataSource.getShortFormattedDate(date);
		            				record.setAttribute("last_ship", last_shipped);
		            			} catch(Exception e) { record.setAttribute("last_ship", ""); }
		            			EntityServiceDS.getInstance().updateEntity(record, new DSCallback() {
		            				public void execute(DSResponse response, Object rawData, DSRequest request) {
		            					Record record = response.getData()[0];
		            					EventBus.fireEvent(new OpenAppsEvent(EventTypes.SELECTION, record));
		            				}						
		            			});	
		            		}
	 	            	}
	 	            }
	 	        }
	        }
	    });
	    */
	}
	public void search(Criteria criteria) {
		action = criteria.getAttribute("action");
		if(action != null) {
			if(criteria.getAttribute("query") != null) query = criteria.getAttribute("query");
			if(criteria.getAttribute("qname") != null) qname = criteria.getAttribute("qname");
			if(action.equals("next")) {
				startRow = startRow + 10;
			} else if(action.equals("last")) {
				startRow = total - 10;
			} else if(action.equals("prev")) {
				startRow = startRow - 10;
			} else if(action.equals("first") || action == null) {
				startRow = 0;
			}
		} else {
			query = criteria.getAttribute("query");
			qname = criteria.getAttribute("qname");
			startRow = 0;
		}
		criteria.setAttribute("_startRow", startRow);
		criteria.setAttribute("_endRow", startRow + 10);
		criteria.setAttribute("qname", qname);
		criteria.setAttribute("query", query);
		contactList.setData(new Record[0]);
		EntityServiceDS.getInstance().search(criteria, new DSCallback() {
			@Override
			public void execute(DSResponse dsResponse, Object data,	DSRequest dsRequest) {
				contactList.setData(dsResponse.getData());
				startRow = dsResponse.getStartRow();
				total = dsResponse.getTotalRows();
				paging.setPagingData(dsResponse.getStartRow(), dsResponse.getEndRow(), dsResponse.getTotalRows());
				paging.show();
			}						
		});
	}
	public void selectContact(Record record) {
		detailPanel.select(record);
		contactPanel.select(record);
		notesPanel.select(record);
		toolbar.select(record);		
	}
	public void select(Record record) {
		this.source = record;
		try {
			Record[] notes = new Record[0];
			Record[] source_associations = record.getAttributeAsRecordArray("source_associations");
			if(source_associations != null) {				
				for(Record source_assoc : source_associations) {					
					Record notesRecord = source_assoc.getAttributeAsRecord("contacts");
					if(notesRecord != null) {
						notes = source_assoc.getAttributeAsRecordArray("contacts");
					}
				}
				donorGrid.setData(notes);
			}
			if(settings.getUser().isAdmin() || settings.getUser().isArchiveManager()) {
				rightArrowImg.show();
				leftArrowImg.show();
			}
		} catch(ClassCastException e) {
			donorGrid.setData(new Record[0]);
		}
	}
}
