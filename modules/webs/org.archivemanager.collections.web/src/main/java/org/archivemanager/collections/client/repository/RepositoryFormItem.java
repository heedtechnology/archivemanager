package org.archivemanager.collections.client.repository;

import org.heed.openapps.gwt.client.OpenApps;
import org.heed.openapps.gwt.client.component.Toolbar;
import org.heed.openapps.gwt.client.data.EntityServiceDS;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.FormItemIcon;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.IconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.IconClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class RepositoryFormItem extends StaticTextItem {
	private static OpenApps oa = new OpenApps();
	private RepositoryWindow window;
	private String value_id;
	
	private boolean initialized;
	
	
	public RepositoryFormItem(String name, String title) {
		super(name, title);
		setHeight(16);
		setWrap(false);
		FormItemIcon formItemIcon = new FormItemIcon();
        setIcons(formItemIcon);
       
        addIconClickHandler(new IconClickHandler() {
            @Override
            public void onIconClick(IconClickEvent event) {
                window.show();
            }
        });
        window = new RepositoryWindow();
	}
	public String getValueId() {		
		return value_id;
	}
	public void setValueId(String id) {
		this.value_id = id;
	}
	public void select(Record record) {
		if(!initialized) {
			window.search();
			initialized = true;
		}		
		if(record != null) {			
			try {
				Record source_associations = record.getAttributeAsRecord("target_associations");
				if(source_associations != null) {
					Record intermediary = source_associations.getAttributeAsRecord("collections");
					if(intermediary != null) {
						Record node = intermediary.getAttributeAsRecord("node");
						value_id = node.getAttribute("target_id");
						String name = node.getAttribute("name");
						setValueId(value_id);
						setValue(name);
					} //else setValue("");
				}
			} catch(ClassCastException e) {
				//setValue("");
			}
		} //else setValue("");
	}
		
	public class RepositoryWindow extends Window {
		private Toolbar toolbar;
		private ListGrid grid;
		private TextItem searchField;
		private RepositoryForm repoForm;
		private AddRepositoryWindow addWindow;

		public RepositoryWindow() {
			VLayout mainLayout = new VLayout();
			mainLayout.setWidth100();
			mainLayout.setHeight100();
			addItem(mainLayout);
			
			setWidth(700);
			setHeight(400);
			setTitle("Add Repository");
			setAutoCenter(true);
			setIsModal(true);
			
			toolbar = new Toolbar(28);
			toolbar.setLayoutLeftMargin(5);
			toolbar.setBorder("1px solid #a8c298;");
			
			DynamicForm searchForm = new DynamicForm();
			searchForm.setHeight(25);
			searchForm.setNumCols(4);
			searchForm.setCellPadding(1);
			searchField = new TextItem("query");
			searchField.setShowTitle(false);
			searchField.setWidth(240);
			ButtonItem searchButton = new ButtonItem("search", "search");
			searchButton.setWidth(50);
			searchButton.setStartRow(false);
			searchButton.setEndRow(false);
			searchButton.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					Criteria criteria = new Criteria();
					criteria.setAttribute("query", searchField.getValueAsString());
					criteria.setAttribute("qname", "openapps_org_repository_1_0_collection");
	        		//criteria.setAttribute("field", "freetext");
	        		//criteria.setAttribute("sort", "name_e");
					EntityServiceDS.getInstance().search(criteria, new DSCallback() {
						@Override
						public void execute(DSResponse dsResponse, Object data, DSRequest dsRequest) {
							grid.setData(dsResponse.getData());
						}						
					});
				}
			});
			searchForm.setFields(searchField,searchButton);
	        toolbar.addToLeftCanvas(searchForm);
					
	        toolbar.addButton("link", "/theme/images/icons32/link.png", "Link Repository", new ClickHandler() {  
				public void onClick(ClickEvent event) { 
					value_id = grid.getSelectedRecord().getAttribute("id");
					setValue(grid.getSelectedRecord().getAttribute("name"));
					fireChangedEvent();					
					window.hide();
				}  
		    });
	        toolbar.addButton("add", "/theme/images/icons32/add.png", "Add Repository", new ClickHandler() {  
				public void onClick(ClickEvent event) { 
					addWindow.show();
		    	}  
		    });
			toolbar.addButton("delete", "/theme/images/icons32/delete.png", "Delete Repository", new ClickHandler() {  
				public void onClick(ClickEvent event) { 
					SC.confirm("Do you really wish to remove this deal permanently?", new BooleanCallback() {
						public void execute(Boolean value) {
							if(value) grid.removeSelectedData();
						}			
					});					
				}  
			});
			toolbar.addButton("save", "/theme/images/icons32/disk.png", "Save Repository", new ClickHandler() {  
				public void onClick(ClickEvent event) { 
					EntityServiceDS.getInstance().updateEntity(repoForm.getValuesAsRecord(), new DSCallback() {
						@Override
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							
						}						
					});
		    	}  
		    });			    
			mainLayout.addMember(toolbar);
			
			HLayout bottomLayout = new HLayout();
			bottomLayout.setHeight100();
			bottomLayout.setWidth100();
			bottomLayout.setMargin(2);
			mainLayout.addMember(bottomLayout);
			
			grid = new ListGrid();
			grid.setWidth(300);
			grid.setHeight100();
			grid.setMargin(1);
			grid.setShowHeader(false);
			grid.setSortField("name");
			grid.addClickHandler(new ClickHandler() {
				public void onClick(ClickEvent event) {
					if(grid.getSelectedRecord() != null) {
						Criteria criteria = new Criteria();
						criteria.setAttribute("id", grid.getSelectedRecord().getAttribute("id"));
						EntityServiceDS.getInstance().getEntity(criteria, new DSCallback() {
							public void execute(DSResponse response, Object rawData, DSRequest request) {
								if(response.getData() != null && response.getData().length > 0) {
									repoForm.editRecord(response.getData()[0]);
									toolbar.showButton("link");
								}
							}
						});
					} else {
						toolbar.hideButton("link");
					}
				}
			});
			ListGridField nameField = new ListGridField("name", "Name");
			nameField.setWidth("*");
			grid.setFields(nameField);
			bottomLayout.addMember(grid);
			
			repoForm = new RepositoryForm();
			repoForm.setWidth100();
			repoForm.setHeight100();
			bottomLayout.addMember(repoForm);
			
			addWindow = new AddRepositoryWindow();
		}
		public void search() {
			Criteria criteria = new Criteria();
			criteria.setAttribute("qname", "openapps_org_repository_1_0_repository");
			criteria.setAttribute("query", "");
			criteria.setAttribute("sources", "false");
			grid.fetchData(criteria);
		}
		@Override
		public void show() {
			/*
			if(accession != null && !collectionId.equals(accession.getAttribute("parent"))) {
				collectionId = accession.getAttribute("parent");
				Criteria criteria = new Criteria();
				criteria.setAttribute("collection", collectionId);				
				tree.fetchData(criteria);
			}
			*/
			super.show();
		}
	}	

	private native void fireChangedEvent() /*-{
		var obj = null;
		obj = this.@com.smartgwt.client.core.DataClass::getJsObj()();
		var selfJ = this;
		if(obj.getValue === undefined){
			return;
		}
		var param = {"form" : obj.form, "item" : obj, "value" : obj.getValue()};
		var event = @com.smartgwt.client.widgets.form.fields.events.ChangedEvent::new(Lcom/google/gwt/core/client/JavaScriptObject;)(param);
		selfJ.@com.smartgwt.client.core.DataClass::fireEvent(Lcom/google/gwt/event/shared/GwtEvent;)(event);
	}-*/;
}