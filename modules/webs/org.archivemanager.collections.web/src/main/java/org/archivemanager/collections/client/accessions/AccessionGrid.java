package org.archivemanager.collections.client.accessions;
import java.util.Date;

import org.archivemanager.collections.client.ArchiveManager;
import org.heed.openapps.gwt.client.data.EntityServiceDS;

import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class AccessionGrid extends VLayout {
	private ArchiveManager settings = new ArchiveManager();
	private ListGrid grid;
	private ImgButton delButton;
	private ImgButton addButton;
	private AddAccessionWindow addAccessionWindow;
	
	private Record collection;
	
	public AccessionGrid() {
		
		HLayout toolstrip = new HLayout();
		toolstrip.setWidth100();
		toolstrip.setHeight(20);
		addMember(toolstrip);
		
		Label label = new Label("<label style='font-size:13px;font-weight:bold;color:#4C4C4C;font-family:Arial,Verdana,sans-serif;'></label>");
		label.setHeight("100%");
		label.setWidth("100%");
		toolstrip.addMember(label);
		
		HLayout buttons = new HLayout();
		buttons.setHeight(16);
		buttons.setMargin(2);
		buttons.setMembersMargin(3);
		toolstrip.addMember(buttons);
			
		addButton = new ImgButton();
		addButton.setWidth(24);
		addButton.setHeight(24);
		addButton.setShowDown(false);
		addButton.setShowRollOver(false);
		addButton.setSrc("/theme/images/icons32/add.png");
		addButton.setPrompt("Add Accession");
		addButton.hide();
		addButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				addAccessionWindow.show();
			}
		});
		buttons.addMember(addButton);
				
		delButton = new ImgButton();
		delButton.setWidth(24);
		delButton.setHeight(24);
		delButton.setShowDown(false);
		delButton.setShowRollOver(false);
		delButton.setVisible(false);
		delButton.setPrompt("Delete");
		delButton.setSrc("/theme/images/icons32/delete.png");
		delButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Record rec = new Record();
				String id = grid.getSelectedRecord().getAttribute("target_id");
				if(id == null) id = grid.getSelectedRecord().getAttribute("id");
				rec.setAttribute("id", id);					
				EntityServiceDS.getInstance().removeEntity(rec, new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						grid.removeSelectedData();	
					}
				});
			}
		});
		buttons.addMember(delButton);
		
		addAccessionWindow = new AddAccessionWindow();
		
		grid = new ListGrid();
		grid.setWidth100();
		grid.setHeight100();
		grid.setAutoFitData(Autofit.VERTICAL);
		grid.setShowHeader(false);
		grid.setWrapCells(true);
		grid.setFixedRecordHeights(false);
		grid.addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				if(settings.getUser().isArchiveManager()) {
					delButton.show();
				}				
			}
		});
		
		ListGridField dateField = new ListGridField("name","Date");
		dateField.setCellAlign(Alignment.CENTER);
		dateField.setWidth("100px");
		ListGridField contentsField = new ListGridField("general_note","Contents");
				
		grid.setFields(dateField, contentsField);
		addMember(grid);
	}
	public void select(Record collection) {
		this.collection = collection;
		try {
			Record[] notes = new Record[0];
			Record[] source_associations = collection.getAttributeAsRecordArray("source_associations");
			if(source_associations != null) {				
				for(Record source_assoc : source_associations) {					
					Record notesRecord = source_assoc.getAttributeAsRecord("accessions");
					if(notesRecord != null) {
						notes = source_assoc.getAttributeAsRecordArray("accessions");
					}
				}
				for(Record note : notes) {
					String date = note.getAttributeAsString("date");
					String name = note.getAttributeAsString("name");
					if(date != null && (name == null || name.length() == 0))
						note.setAttribute("name", date.substring(0, 10));
				}
				grid.setData(notes);
			}
			if(settings.getUser().isArchiveManager()) {
				addButton.show();
				delButton.hide();
			}
		} catch(ClassCastException e) {
			grid.setData(new Record[0]);
		}
	}
	
	public class AddAccessionWindow extends Window {
		@SuppressWarnings("deprecation")
		public AddAccessionWindow() {
			setWidth(440);
			setHeight(220);
			setTitle("Add Accession");
			setAutoCenter(true);
			setIsModal(true);
			
			DynamicForm addForm = new DynamicForm();
			addForm.setColWidths("50","*");
			addForm.setCellPadding(7);
			final DateItem dateItem = new DateItem("date", "Date");
			dateItem.setStartDate(new Date(1990, 1, 1));
						
			final TextAreaItem generalNoteItem = new TextAreaItem("general_note", "Contents");
			generalNoteItem.setHeight(100);
			generalNoteItem.setWidth(350);
			
			ButtonItem submitItem = new ButtonItem("submit", "Add");
			submitItem.setIcon("/theme/images/icons16/add.png");
			submitItem.setStartRow(false);
			submitItem.setEndRow(false);
			submitItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					Record record = new Record();
					record.setAttribute("assoc_qname", "openapps_org_repository_1_0_accessions");
					record.setAttribute("entity_qname", "openapps_org_repository_1_0_accession");
					record.setAttribute("source", collection.getAttribute("id"));
					record.setAttribute("date", dateItem.getValueAsDate());
					record.setAttribute("name", DateTimeFormat.getShortDateFormat().format(dateItem.getValueAsDate()));
					record.setAttribute("general_note", generalNoteItem.getValue());
					EntityServiceDS.getInstance().addAssociation(record, new DSCallback() {
						@Override
						public void execute(DSResponse response, Object data, DSRequest request) {
							if(response.getData() != null && response.getData().length > 0) {
								Record record = response.getData()[0];
								select(record);									
								addAccessionWindow.hide();
							}
						}
					});	
				}
			});
			
			addForm.setFields(dateItem,generalNoteItem,submitItem);
			addItem(addForm);
		}
	}
	public void addData(Record record) {
		grid.addData(record);
	}
	public void setData(Record[] record) {
		grid.setData(record);
	}
	public void removeData(Record record) {
		grid.removeData(record);
	}
	public Record[] getRecords() {
		return grid.getRecords();
	}
	public void removeSelectedData() {
		grid.removeSelectedData();
	}
	public Record getSelectedRecord() {
		return grid.getSelectedRecord();
	}
	@Override
	public HandlerRegistration addClickHandler(ClickHandler handler) {
		return grid.addClickHandler(handler);
	}
}
