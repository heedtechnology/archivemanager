package org.archivemanager.collections.client.locations;
import java.util.ArrayList;
import java.util.List;

import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.SpacerItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;


public class LocationForm extends DynamicForm {
		
	
	public LocationForm() {
		setWidth100();
		setHeight100();
		setMargin(10);
		setNumCols(4);
		setColWidths("75","*", "75","*");
		setCellPadding(3);
		
		List<FormItem> fields = new ArrayList<FormItem>();
		
		StaticTextItem idItem = new StaticTextItem("id", "ID");
		fields.add(idItem);
		
		SpacerItem spacerItem = new SpacerItem();
		fields.add(spacerItem);
		
		final TextItem buildingItem = new TextItem("building", "Building");
		buildingItem.setWidth("*");
		fields.add(buildingItem);
		
		final TextItem floorItem = new TextItem("floor", "Floor");
		floorItem.setWidth("*");
		fields.add(floorItem);
		
		final TextItem aisleItem = new TextItem("aisle", "Aisle");
		aisleItem.setWidth("*");
		fields.add(aisleItem);
		
		final TextItem bayItem = new TextItem("bay", "Bay");
		bayItem.setWidth("*");
		fields.add(bayItem);
		
		TextAreaItem descriptionItem = new TextAreaItem("description", "Description");
		descriptionItem.setWidth("*");
		descriptionItem.setColSpan(4);
		fields.add(descriptionItem);
		
		TextItem collectionItem = new TextItem("collection", "Collection");
		collectionItem.setColSpan(4);
		collectionItem.setWidth("*");
		fields.add(collectionItem);
				
		TextItem containerItem = new TextItem("container", "Container");
		containerItem.setColSpan(4);
		containerItem.setWidth("*");
		fields.add(containerItem);
		
		TextItem accessionItem = new TextItem("accession", "Accession #");
		accessionItem.setWidth("*");
		accessionItem.setColSpan(4);
		fields.add(accessionItem);
		
		TextItem codeItem = new TextItem("code", "Code");
		codeItem.setWidth("*");
		codeItem.setColSpan(4);
		fields.add(codeItem);
		
		FormItem[] itemArray = fields.toArray(new FormItem[fields.size()]);		
		setFields(itemArray);		
	}
	
}
