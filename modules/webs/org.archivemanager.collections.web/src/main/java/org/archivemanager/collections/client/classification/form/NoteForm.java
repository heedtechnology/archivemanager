package org.archivemanager.collections.client.classification.form;

import java.util.ArrayList;
import java.util.List;

import org.archivemanager.gwt.client.NativeClassificationModel;

import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;


public class NoteForm extends DynamicForm {
	private NativeClassificationModel model = new NativeClassificationModel();
	
	public NoteForm() {
		setWidth100();
		setHeight100();
		setMargin(5);
		setNumCols(2);
		setCellPadding(5);
		setColWidths("75","*");
				
		List<FormItem> fields = new ArrayList<FormItem>();
						
		TextAreaItem citationItem = new TextAreaItem("citation", "Citation");
		citationItem.setWidth(330);
		citationItem.setHeight(175);
		fields.add(citationItem);
		
		TextAreaItem noteItem = new TextAreaItem("note", "Note");
		noteItem.setWidth(330);
		noteItem.setHeight(175);
		fields.add(noteItem);
		
		SelectItem typeItem = new SelectItem("note_type", "Type");
		typeItem.setValueMap(model.getNamedEntityType());
		typeItem.setWidth(200);
		fields.add(typeItem);
		
		FormItem[] itemArray = fields.toArray(new FormItem[fields.size()]);		
		setFields(itemArray);
	}
		
}
