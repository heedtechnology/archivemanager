package org.archivemanager.collections.client.collection;

import org.archivemanager.collections.client.data.CollectionTreeJsonDS;
import org.archivemanager.gwt.client.data.AMServiceDS;
import org.heed.openapps.gwt.client.data.NativeContentTypes;
import org.heed.openapps.gwt.client.event.AddListener;
import org.heed.openapps.gwt.client.event.RemoveListener;
import org.heed.openapps.gwt.client.event.SaveListener;

import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.events.ClickHandler;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;
import com.smartgwt.client.widgets.tree.TreeGrid;
import com.smartgwt.client.widgets.tree.TreeNode;
import com.smartgwt.client.widgets.tree.events.FolderDropEvent;
import com.smartgwt.client.widgets.tree.events.FolderDropHandler;

public class CollectionTree extends TreeGrid {
	private NativeContentTypes model = new NativeContentTypes();
			
	private Menu emptyContextMenu;
	private Menu addContextMenu;
	private Menu fullContextMenu;
	
	private AddPropertyWindow addWindow;
	
	private AddListener addListener;
	private RemoveListener removeListener;
	
	
	public CollectionTree() {
		setHeight100();
		setWidth100();
		setShowHeader(false);
		setWrapCells(true);
		setFixedRecordHeights(false);
		setCanReorderRecords(true);  
        setCanAcceptDroppedRecords(true);
        setSelectionType(SelectionStyle.SINGLE);
		CollectionTreeJsonDS ds = CollectionTreeJsonDS.getInstance();
        setDataSource(ds);
        
        addFolderDropHandler(new FolderDropHandler() {
        	public void onFolderDrop(FolderDropEvent event) {        		
        		TreeNode child = event.getNodes()[0];
        		TreeNode parent = event.getFolder();
        		if(parent != null && child != null) {
	        		String parentType = parent.getAttribute("localName");
	        		String childType = child.getAttribute("localName"); 
	        		if(parentType.equals("collection") && childType.equals("collection")) {
	        			SC.say("sorry, you cannot relocate your item to that folder");
	        		} else {
	        			Record record = new Record();
	        			String parentId = parent.getAttribute("id");
	        			String childId = child.getAttribute("id");
	        			if(parentId != null && !parentId.equals("null") && childId != null && !childId.equals("null") && !childId.equals(parentId)) {
		        			record.setAttribute("id", childId);
		            		record.setAttribute("parent", parentId);
		            		updateData(record);
	        			}
	        		}
        		}
        		event.cancel();
			}
        });     
                
        MenuItem addItem = new MenuItem("Add New Item");
        addItem.addClickHandler(new ClickHandler() {
        	public void onClick(MenuItemClickEvent event) {
        		addListener.add(getSelectedRecord());
			}
        });
        MenuItem delItem = new MenuItem("Delete Item");
        delItem.addClickHandler(new ClickHandler() {
        	public void onClick(MenuItemClickEvent event) {
        		removeListener.remove(getSelectedRecord());
			}
        });
        MenuItem propItem = new MenuItem("Set Child Property");
        propItem.addClickHandler(new ClickHandler() {
        	public void onClick(MenuItemClickEvent event) {
				addWindow.show();
			}
        });        
        addContextMenu = new Menu(); 
        addContextMenu.setItems(addItem);
        
        emptyContextMenu = new Menu();
        fullContextMenu = new Menu();
        fullContextMenu.setItems(addItem,delItem,propItem);
        
        addWindow = new AddPropertyWindow();
        addWindow.setSaveListener(new SaveListener() {
			@Override
			public void save(Record record) {
				record.setAttribute("id", getSelectedRecord().getAttribute("id"));
       		 	AMServiceDS.getInstance().propagate(record,  new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						addWindow.setMessage("Property successfully set on children");
					}
				});
			}        	
        });
	}
	
	public void select(Record record) {
		if(record != null) {
			String type = record.getAttribute("localName");
			if(type != null) {
				if(type.equals("collection")) 
					setContextMenu(addContextMenu);
				else if(type.equals("category") || isItem(type)) setContextMenu(fullContextMenu);
				else setContextMenu(emptyContextMenu);
			}
			String date = record.getAttribute("name");
			String title = getSelectedRecord().getAttribute("name");
			String localName = record.getAttribute("localName");
			if(title != null && !title.equals(date)) getSelectedRecord().setAttribute("name", date);
			if(localName != null) getSelectedRecord().setAttribute("localName", localName);
			refreshFields();
		}
	}
	protected boolean isItem(String type) {
		return model.getContentTypes().containsKey("openapps_org_repository_1_0_"+type);
	}
	public void setAddListener(AddListener addListener) {
		this.addListener = addListener;
	}
	public void setRemoveListener(RemoveListener removeListener) {
		this.removeListener = removeListener;
	}	
}
