package org.archivemanager.collections.client.data;

import java.util.LinkedHashMap;

import com.google.gwt.core.client.JavaScriptObject;
import com.smartgwt.client.util.JSOHelper;

public class NativeImportModel {

	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getImportProcessors() { 
		JavaScriptObject obj = getNativeImportProcessors();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>();
	}	
	private final native JavaScriptObject getNativeImportProcessors() /*-{
        return $wnd.import_processors;
	}-*/;
	
}
