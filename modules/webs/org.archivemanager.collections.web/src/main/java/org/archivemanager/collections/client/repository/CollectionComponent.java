package org.archivemanager.collections.client.repository;

import org.heed.openapps.gwt.client.OpenApps;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class CollectionComponent extends VLayout {
	private OpenApps security = new OpenApps();
	
	private ListGrid grid;
		
	
	public CollectionComponent(boolean printControls) {
		HLayout toolstrip = new HLayout();
		toolstrip.setWidth100();
		toolstrip.setHeight(20);
		//toolstrip.setBackgroundImage("[SKIN]/cssButton/button_stretch.png");
		//toolstrip.setBorder("1px solid #A7ABB4");
		addMember(toolstrip);
		
		Label label = new Label("<label style='font-size:13px;font-weight:bold;color:#4C4C4C;font-family:Arial,Verdana,sans-serif;'>Collections</label>");
		label.setHeight("100%");
		label.setWidth("100%");
		toolstrip.addMember(label);
		
		grid = new ListGrid();
		grid.setWidth100();
		grid.setHeight(40);
		grid.setAutoFitData(Autofit.VERTICAL);
		grid.setAutoFitMaxHeight(350);
		grid.setShowHeader(false);
		grid.setSortField("name");
		ListGridField nameField = new ListGridField("name");
		grid.setFields(nameField);
		addMember(grid);
	}
	
	public void select(Record record) {
		try {
			Record[] notes = new Record[0];
			Record[] source_associations = record.getAttributeAsRecordArray("source_associations");
			if(source_associations != null) {				
				for(Record source_assoc : source_associations) {					
					Record notesRecord = source_assoc.getAttributeAsRecord("collections");
					if(notesRecord != null) {
						notes = source_assoc.getAttributeAsRecordArray("collections");
					}
				}
				grid.setData(notes);
			}
		} catch(ClassCastException e) {
			grid.setData(new Record[0]);
		}
	}
	public void setData(Record[] records) {
		grid.setData(records);
	}
		
	public static native void openWindow(String url) /*-{   
		window.open(url, "win", "width=600,height=600");
	}-*/;
}