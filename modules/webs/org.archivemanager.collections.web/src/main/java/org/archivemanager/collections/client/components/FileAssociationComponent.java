package org.archivemanager.collections.client.components;
import java.util.HashMap;
import java.util.Map;

import org.archivemanager.collections.client.content.BasicContentViewer;
import org.archivemanager.gwt.client.ArchiveManagerEventTypes;
import org.archivemanager.gwt.client.component.BreadcrumbComponent;
import org.archivemanager.gwt.client.component.FileViewer;
import org.archivemanager.gwt.client.component.FilesystemGrid;
import org.archivemanager.gwt.client.component.events.EntitySelectionEvent;
import org.archivemanager.gwt.client.component.events.EntitySelectionHandler;
import org.archivemanager.gwt.client.data.ArchiveManagerContentDS;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.EventTypes;
import org.heed.openapps.gwt.client.OpenApps;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.OpenAppsEventHandler;
import org.heed.openapps.gwt.client.component.Toolbar;
import org.heed.openapps.gwt.client.data.NativeContentModel;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.event.shared.GwtEvent.Type;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DSDataFormat;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.SpinnerItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class FileAssociationComponent extends VLayout {
	private Record accession;
	private NativeContentModel contentModel = new NativeContentModel();
	private ListGrid grid;
	private AddItemPanel addItemPanel;
	private BasicContentViewer viewer;
	private NoteDataSource fileDS;
	private boolean initialized;
	private Map<HandlerRegistration, EntitySelectionHandler> handlers = new HashMap<HandlerRegistration, EntitySelectionHandler>();
	
	
	public FileAssociationComponent(String title, boolean upload, boolean associate, boolean delete) {
		setWidth100();
		setHeight100();
		this.fileDS = new NoteDataSource("noteDataSource");
		//setMargin(10);
		setBorder("1px solid #C0C3C7");
		
		HLayout toolstrip = new HLayout();
		toolstrip.setWidth("100%");
		toolstrip.setHeight(20);
		toolstrip.setBackgroundImage("[SKIN]/cssButton/button_stretch.png");
		//toolstrip.setBorder("1px solid #A7ABB4");
		addMember(toolstrip);
		
		Label label = new Label("<label style='font-size:11px;font-weight:bold;color:#4C4C4C;font-family:Arial,Verdana,sans-serif;'>"+title+"</label>");
		label.setHeight("100%");
		label.setWidth("100%");
		toolstrip.addMember(label);
		
		HLayout buttons = new HLayout();
		buttons.setHeight(16);
		buttons.setMargin(2);
		buttons.setMembersMargin(3);
		toolstrip.addMember(buttons);
		
		ImgButton addButton = new ImgButton();
		addButton.setWidth(16);
		addButton.setHeight(16);
		addButton.setSrc("/theme/images/icons16/add.png");
		addButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				grid.hide();
				addItemPanel.show();
			}
		});
		buttons.addMember(addButton);
		ImgButton delButton = new ImgButton();
		delButton.setWidth(16);
		delButton.setHeight(16);
		delButton.setSrc("/theme/images/icons16/delete.png");
		delButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Record rec = new Record();
				String id = grid.getSelectedRecord().getAttribute("id");
				rec.setAttribute("id", id);
				fileDS.removeData(rec, new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						grid.removeSelectedData();	
					}
				});
			}
		});
		buttons.addMember(delButton);
		
		Canvas mainCanvas = new Canvas();
		mainCanvas.setWidth100();
		mainCanvas.setHeight100();
		addMember(mainCanvas);
		
		grid = new ListGrid();
		grid.setWidth("100%");
		grid.setHeight("100%");
		grid.setBorder("0px");
		//grid.setShowHeader(false);
		ListGridField typeField = new ListGridField("type","Type");
		typeField.setValueMap(contentModel.getImageRelationships());
		typeField.setWidth(80);
		ListGridField nameField = new ListGridField("name","Name");
		ListGridField groupField = new ListGridField("group","Group");
		groupField.setWidth(80);
		ListGridField orderField = new ListGridField("order","Order");
		orderField.setAlign(Alignment.CENTER);
		orderField.setWidth(40);
		//ListGridField sizeField = new ListGridField("size","Size");
		//sizeField.setWidth(50);
		grid.setFields(typeField,groupField,orderField,nameField);
		grid.addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				EntitySelectionEvent evt = new EntitySelectionEvent();
				evt.setRecord(grid.getSelectedRecord());
				for(EntitySelectionHandler handler : handlers.values()) {
					handler.onEntitySelection(evt);
				}
			}
		});
		mainCanvas.addChild(grid);
                
        addItemPanel = new AddItemPanel();
        addItemPanel.setWidth100();
        addItemPanel.setHeight100();
        addItemPanel.hide();
        mainCanvas.addChild(addItemPanel);
	}
	public void setData(Record[] records) {
		grid.setData(records);
	}
	public void select(Record record) {
		this.accession = record;
		try {
			Record source_associations = record.getAttributeAsRecord("source_associations");
			if(source_associations != null) {
				Record files = source_associations.getAttributeAsRecord("files");
				if(files != null) {
					Record[] notes = files.getAttributeAsRecordArray("node");
					for(Record n : notes) {
						String path = n.getAttribute("path");
						String name = path.substring(path.lastIndexOf("/")+1);
						n.setAttribute("name", name);
					}
					setData(notes);
				} else setData(new Record[0]);
			}
		} catch(ClassCastException e) {
			setData(new Record[0]);
		}
		try {
			Record target_associations = record.getAttributeAsRecord("target_associations");
			if(target_associations != null) {
				
			}
		} catch(ClassCastException e) {
			
		}
	}
	public HandlerRegistration addEntitySelectionHandler(EntitySelectionHandler handler) {
		HandlerRegistration reg = doAddHandler(handler, new Type<EntitySelectionHandler>());
		if(!handlers.containsKey(reg)) handlers.put(reg, handler);
		return reg;
	}
	
	public class AddItemPanel extends VLayout {
		private FileViewer fileViewer;
		private FilesystemGrid fileGrid;
		private DynamicForm typeForm;
				
		public AddItemPanel() {
			setWidth100();
			setHeight100();
			//setTitle("Add File");
			
			Toolbar toolbar = new Toolbar(25);
	        toolbar.setBorder("1px solid #A7ABB4");
	        toolbar.setMargin(1);
	        addMember(toolbar);
	        
			BreadcrumbComponent breadcrumb = new BreadcrumbComponent();
			toolbar.addToLeftCanvas(breadcrumb);
			
			HLayout formLayout = new HLayout();
			formLayout.setWidth100();
			addMember(formLayout);
			
			Canvas leftSideSpacer = new Canvas();
			leftSideSpacer.setWidth100();
			formLayout.addMember(leftSideSpacer);
			
			typeForm = new DynamicForm();
			typeForm.setNumCols(8);
			typeForm.setCellPadding(3);
			typeForm.setAlign(Alignment.RIGHT);
			final SelectItem typeItem = new SelectItem("type","Type");
			typeItem.setWidth(80);
			typeItem.setAlign(Alignment.RIGHT);
			typeItem.setValueMap(contentModel.getImageRelationships());
			
			final TextItem groupItem = new TextItem("group", "Group");
			groupItem.setAlign(Alignment.RIGHT);
			groupItem.setWidth(125);
			
			final SpinnerItem orderItem = new SpinnerItem("order", "Order");
			orderItem.setAlign(Alignment.RIGHT);
			orderItem.setDefaultValue(0);
			orderItem.setWidth(35);
						
			ButtonItem linkItem = new ButtonItem("Link");
			linkItem.setWidth(35);
			linkItem.setStartRow(false);
			linkItem.setEndRow(false);
			linkItem.setAlign(Alignment.RIGHT);
			linkItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					if(fileViewer.getSelectedRecord() != null) {
						Record record = new Record();
						record.setAttribute("qname", "openapps_org_content_1_0_files");
						record.setAttribute("source", accession.getAttribute("id"));
						record.setAttribute("target", fileViewer.getSelectedRecord().getAttribute("uid"));
						record.setAttribute("type", typeItem.getValue());
						record.setAttribute("group", groupItem.getValue());
						record.setAttribute("order", orderItem.getValue());
						
						fileDS.updateData(record, new DSCallback() {
							public void execute(DSResponse response, Object rawData, DSRequest request) {
								if(response.getData() != null && response.getData().length > 0) {
									Record record = response.getData()[0];
									String path = record.getAttribute("path");
									String name = path.substring(path.lastIndexOf("/")+1);
									record.setAttribute("name", name);
									grid.addData(record);
								}
							}
						});						
						addItemPanel.hide();
						grid.show();
					} else SC.say("please select a file to associate.");
				} 
			});
			ButtonItem cancelItem = new ButtonItem("Cancel");
			cancelItem.setWidth(50);
			cancelItem.setStartRow(false);
			cancelItem.setEndRow(false);
			cancelItem.setAlign(Alignment.RIGHT);
			cancelItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					refresh();
				}
			});
			typeForm.setFields(typeItem,groupItem,orderItem,linkItem,cancelItem);
			formLayout.addMember(typeForm);
			
			HLayout bodyLayout = new HLayout();
			bodyLayout.setHeight100();
			bodyLayout.setWidth100();
			bodyLayout.setMembersMargin(1);
			addMember(bodyLayout);
			
			fileGrid = new FilesystemGrid();
			fileGrid.setWidth100();
			fileGrid.setHeight100();
			fileGrid.setMargin(1);
			fileGrid.setShowResizeBar(true);
			fileGrid.addClickHandler(new ClickHandler() {
				public void onClick(ClickEvent event) {
					if(fileGrid.getSelectedRecord() != null) {
						final String type = fileGrid.getSelectedRecord().getAttribute("localName");
						if(type.equals("folder")) {				
							Criteria criteria = new Criteria();
							criteria.setAttribute("uid", fileGrid.getSelectedRecord().getAttribute("uid"));
							ArchiveManagerContentDS.getInstance().filterData(criteria, new DSCallback() {
								public void execute(DSResponse response, Object rawData, DSRequest request) {
									if(response.getData() != null && response.getData().length > 0) {
										Record folder = response.getData()[0];
										if(folder != null) {
											fileViewer.select(folder);
										}
									}
								}						
							});
						} else if(type.equals("datasource")) {
							
						} else if(type.equals("file")) {
							
						}
					}
				}
			});
			bodyLayout.addMember(fileGrid);
						
			fileViewer = new FileViewer();
			fileViewer.setWidth100();
			fileViewer.setHeight100();
			bodyLayout.addMember(fileViewer);
						
			EventBus.addHandler(org.heed.openapps.gwt.client.OpenAppsEvent.TYPE, new OpenAppsEventHandler()     {
		        @Override
		        public void onEvent(OpenAppsEvent event) {
		        	if(event.isType(ArchiveManagerEventTypes.CONTENT_BROWSE)) {
		        		Criteria criteria = new Criteria();
		        		criteria.setAttribute("parent", event.getRecord().getAttribute("parent"));
		        		fileDS.fetchData(criteria, new DSCallback() {
		    				@Override
		    				public void execute(DSResponse response, Object rawData, DSRequest request) {
		    					fileGrid.setData(response.getData());
		    				}				
		    			});	
		        		
		        	} else if(event.isType(EventTypes.FILE_SELECTED)) {
		        		typeForm.show();
		        	}
		        }
		    });
			
		}
		public void refresh() {
			grid.show();
			addItemPanel.hide();
		}
		public void select(Record record) {
			viewer.select(record);
		}
		@Override
		public void show() {
			if(!initialized) search("", null);
			super.show();
		}
		public void search(String query, DSCallback callback) {
			Criteria criteria = new Criteria();
			//criteria.setAttribute("qname", "{openapps.org_content_1.0}folder");
			criteria.setAttribute("sources", "false");
			criteria.setAttribute("query", query);			
			fileDS.fetchData(criteria, new DSCallback() {
				@Override
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					fileGrid.setData(response.getData());
				}				
			});
		}
	}
	public class NoteDataSource extends RestDataSource {
		private OpenApps security = new OpenApps();
		
		public NoteDataSource(String id) {
			setDataFormat(DSDataFormat.JSON);
			setID(id);  
	        setTitleField("name");	        
	        DataSourceTextField nameField = new DataSourceTextField("name", "Name");     
	                
	        DataSourceTextField idField = new DataSourceTextField("id", "ID", 100);  
	        idField.setPrimaryKey(true);  
	        idField.setRequired(true);
	        
	        setFields(idField,nameField);
	        
			setAddDataURL(security.getServiceUrl() + "service/archivemanager/content/file/associate.json");
			setFetchDataURL(security.getServiceUrl() + "service/archivemanager/datasource/folders/browse.json");
			setRemoveDataURL(security.getServiceUrl() + "service/entity/remove.json");
			setUpdateDataURL(security.getServiceUrl() + "service/entity/update.json");
		}

		@Override
		protected Object transformRequest(DSRequest dsRequest) {
			JavaScriptObject data = dsRequest.getData();
			
			//JSOHelper.setAttribute(data, "ticket", security.getTicket());
			
			return data;
		}
	}
}
