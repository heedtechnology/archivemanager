package org.archivemanager.collections.client.classification.form;

import java.util.ArrayList;
import java.util.List;

import org.archivemanager.gwt.client.NativeClassificationModel;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.layout.VLayout;


public class PersonForm extends VLayout {
	private NativeClassificationModel model = new NativeClassificationModel();
	
	private DynamicForm form;
	
	private static final String fieldWidth1 = "300";
	
	public PersonForm() {
		setWidth100();
		setHeight100();
		
		form = new DynamicForm();
		form.setHeight100();
		form.setNumCols(2);
		form.setCellPadding(5);
		form.setColWidths("110","*");
		addMember(form);
		
		List<FormItem> fields = new ArrayList<FormItem>();
		
		StaticTextItem idItem = new StaticTextItem("id", "ID");
		fields.add(idItem);
		
		TextItem nameItem = new TextItem("name", "Name");
		nameItem.setWidth(fieldWidth1);
		fields.add(nameItem);
		
		TextAreaItem descItem = new TextAreaItem("description", "Description");
		descItem.setWidth(fieldWidth1);
		descItem.setHeight(75);
		fields.add(descItem);
				
		TextItem primaryNameItem = new TextItem("primary_name", "Primary Name");
		primaryNameItem.setWidth(fieldWidth1);
		fields.add(primaryNameItem);
		
		TextItem secondaryNameItem = new TextItem("secondary_name", "Secondary Name");
		secondaryNameItem.setWidth(fieldWidth1);
		fields.add(secondaryNameItem);
		
		TextItem prefixItem = new TextItem("prefix", "Prefix");
		prefixItem.setWidth(fieldWidth1);
		fields.add(prefixItem);
				
		TextItem suffixItem = new TextItem("suffix", "Suffix");
		suffixItem.setWidth(fieldWidth1);
		fields.add(suffixItem);
		
		TextItem fullerItem = new TextItem("fuller_form_name", "Fuller Form");
		fullerItem.setWidth(fieldWidth1);
		fields.add(fullerItem);
		
		TextItem urlItem = new TextItem("url", "URL");
		urlItem.setWidth(fieldWidth1);
		urlItem.setColSpan(4);
		fields.add(urlItem);
		
		TextItem datesItem = new TextItem("dates", "Dates");
		datesItem.setWidth(fieldWidth1);
		fields.add(datesItem);
		
		SelectItem sourceItem = new SelectItem("source", "Source");
		sourceItem.setValueMap(model.getSubjectSource());
		sourceItem.setWidth(fieldWidth1);
		fields.add(sourceItem);
		
		SelectItem ruleItem = new SelectItem("rule", "Rule");
		ruleItem.setValueMap(model.getNamedEntityRule());
		ruleItem.setWidth(250);
		fields.add(ruleItem);
		
		FormItem[] itemArray = fields.toArray(new FormItem[fields.size()]);		
		form.setFields(itemArray);
	}
	public void editRecord(Record record) {
		form.editRecord(record);
	}
	public Record getValuesAsRecord() {
		return form.getValuesAsRecord();
	}
}
