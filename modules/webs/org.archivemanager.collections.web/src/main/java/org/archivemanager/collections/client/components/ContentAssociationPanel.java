package org.archivemanager.collections.client.components;

import org.archivemanager.collections.client.content.BasicContentViewer;
import org.archivemanager.gwt.client.component.WebContentAssociationComponent;
import org.archivemanager.gwt.client.component.WebLinkAssociationComponent;
import org.archivemanager.gwt.client.component.events.EntitySelectionEvent;
import org.archivemanager.gwt.client.component.events.EntitySelectionHandler;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.OpenAppsEventHandler;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;


public class ContentAssociationPanel extends VLayout {
	private FileAssociationComponent fileComponent;
	private BasicContentViewer viewer;
	//private Record selection;
	private WebLinkAssociationComponent linkComponent;
	private WebContentAssociationComponent contentComponent;
		
	
	public ContentAssociationPanel() {
		setWidth100();
		setHeight100();
		setMembersMargin(10);
		
		TabSet tabs = new TabSet();
		tabs.setWidth100();
		tabs.setHeight(400);
		tabs.setShowResizeBar(true);
		addMember(tabs);
		
		linkComponent = new WebLinkAssociationComponent("100%", "100%");
		linkComponent.addEntitySelectionHandler(new EntitySelectionHandler() {
			public void onEntitySelection(EntitySelectionEvent event) {
				viewer.select(event.getRecord());
			}
		});
		Tab linkTab = new Tab("Web Links");
		linkTab.setPane(linkComponent);
		tabs.addTab(linkTab);
		
		fileComponent = new FileAssociationComponent("Files", false, true, true);
		fileComponent.addEntitySelectionHandler(new EntitySelectionHandler() {
			public void onEntitySelection(EntitySelectionEvent event) {
				viewer.select(event.getRecord());
			}
		});
		Tab fileTab = new Tab("Files");
		fileTab.setPane(fileComponent);
		tabs.addTab(fileTab);
		
		contentComponent = new WebContentAssociationComponent("100%", "100%");
		contentComponent.addEntitySelectionHandler(new EntitySelectionHandler() {
			public void onEntitySelection(EntitySelectionEvent event) {
				viewer.select(event.getRecord());
			}
		});
		Tab contentTab = new Tab("Web Content");
		contentTab.setPane(contentComponent);
		tabs.addTab(contentTab);
				
		viewer = new BasicContentViewer();
		addMember(viewer);
		
		EventBus.addHandler(org.heed.openapps.gwt.client.OpenAppsEvent.TYPE, new OpenAppsEventHandler() {
	        @Override
	        public void onEvent(OpenAppsEvent event) {
	        	//if(event.isType(ArchiveManagerEventTypes.CONTENT_SELECTION)) {
	        		
	        	//}
	        }
		});
	}
	public void select(Record selection) {
		//this.selection = selection;
		fileComponent.select(selection);
		linkComponent.select(selection);
		contentComponent.select(selection);
	}
	
}
