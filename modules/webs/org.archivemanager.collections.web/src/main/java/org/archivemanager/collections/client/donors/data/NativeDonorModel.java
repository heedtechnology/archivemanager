package org.archivemanager.collections.client.donors.data;

import java.util.LinkedHashMap;

import com.google.gwt.core.client.JavaScriptObject;
import com.smartgwt.client.util.JSOHelper;

public class NativeDonorModel {

	
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,String> getFileRelationships() { 
		JavaScriptObject obj = getNativeFileRelationships();
		if(obj != null) return (LinkedHashMap<String,String>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,String>();
	}	
	private final native JavaScriptObject getNativeFileRelationships() /*-{
        return $wnd.deal_file_relationships;
	}-*/;
}
