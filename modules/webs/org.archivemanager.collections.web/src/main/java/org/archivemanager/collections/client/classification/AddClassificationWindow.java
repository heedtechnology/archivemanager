package org.archivemanager.collections.client.classification;

import org.archivemanager.gwt.client.NativeClassificationModel;
import org.heed.openapps.gwt.client.data.EntityServiceDS;

import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;

public class AddClassificationWindow extends Window {
	private NativeClassificationModel model = new NativeClassificationModel();
	
	private DynamicForm addForm;
	private DSCallback callback;
	
	public AddClassificationWindow() {
		setWidth(400);
		setHeight(140);
		setTitle("Add Classification");
		setAutoCenter(true);
		setIsModal(true);
		
		addForm = new DynamicForm();
		addForm.setCellPadding(7);
		final TextItem nameItem = new TextItem("name", "Name");
		nameItem.setWidth("*");
		
		SelectItem typeItem = new SelectItem("type");
		typeItem.setValueMap(model.getClassificationType());
		
		ButtonItem submitItem = new ButtonItem("submit", "Add");
		submitItem.setIcon("/theme/images/icons16/add.png");
		submitItem.setStartRow(false);
		submitItem.setEndRow(false);
		submitItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
				Record record = new Record();
				record.setAttribute("qname", "openapps_org_classification_1_0_"+addForm.getValueAsString("type"));
				record.setAttribute("name", nameItem.getValueAsString());
				EntityServiceDS.getInstance().addEntity(record, new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						if(callback != null) callback.execute(response, rawData, request);
						hide();
					}
				});
			}
		});
		
		addForm.setFields(nameItem,typeItem,submitItem);
		addItem(addForm);
	}
	
	public void setAddCallback(DSCallback callback) {
		this.callback = callback;
	}
	@Override
	public void show() {
		addForm.editNewRecord();
		super.show();
	}
}