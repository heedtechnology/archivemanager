package org.archivemanager.collections.client;

import java.util.LinkedHashMap;

import org.archivemanager.collections.client.User;

import com.google.gwt.core.client.JavaScriptObject;
import com.smartgwt.client.util.JSOHelper;

public class ArchiveManager {

	
	@SuppressWarnings("unchecked")
	public User getUser() {
		JavaScriptObject userObj = getNativeUser();
		JavaScriptObject rolesObj = getNativeUserRoles();
		if(userObj != null) {
			LinkedHashMap<String,String> userMap = (LinkedHashMap<String,String>)JSOHelper.convertToMap(userObj);
			User user = new User(userMap.get("username"));
			if(rolesObj != null) {
				Object[] roleArray = JSOHelper.convertToArray(rolesObj);
				for(Object role : roleArray) {
					user.getRoles().add(String.valueOf(role));
				}
			}
			return user;
		}
		return new User("guest");
	}	
	private final native JavaScriptObject getNativeUserRoles() /*-{
        return $wnd.user_roles;
	}-*/;
	private final native JavaScriptObject getNativeUser() /*-{
    	return $wnd.user;
	}-*/;
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getPermissionTypes() { 
		JavaScriptObject obj = getNativePermissionTypes();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>();
	}	
	private final native JavaScriptObject getNativePermissionTypes() /*-{
        return $wnd.permission_types;
	}-*/;
	
	public String getTicket() { 
		String obj = getNativeTicket();
		if(obj != null) return obj;
		return "";
	}	
	private final native String getNativeTicket() /*-{
        return $wnd.ticket;
	}-*/;
	
	public String getHeight() { 
		return getNativeHeight();
	}	
	private final native String getNativeHeight() /*-{
        return $wnd.height;
	}-*/;
	
	public String getWidth() { 
		return getNativeWidth();
	}	
	private final native String getNativeWidth() /*-{
        return $wnd.width;
	}-*/;
	
	public String getServiceUrl() { 
		return getNativeServiceUrl();
	}	
	private final native String getNativeServiceUrl() /*-{
        return $wnd.service_path;
	}-*/;
	
	public String getPathThemeJavaScript() { 
		return getNativePathThemeJavaScript();
	}	
	private final native String getNativePathThemeJavaScript() /*-{
        return $wnd.pathThemeJavaScript;
	}-*/;
	public String getPathThemeImages()  { 
		return getNativePathThemeImages() ;
	}	
	private final native String getNativePathThemeImages() /*-{
        return $wnd.pathThemeImages;
	}-*/;
	public String getApplicationName() { 
		return getNativeApplicationName();
	}	
	private final native String getNativeApplicationName() /*-{
        return $wnd.oa_application_id;
	}-*/;
	
	public String getPathWebservice() { 
		return getNativePathWebservice();
	}	
	private final native String getNativePathWebservice() /*-{
        return $wnd.pathWebservice;
	}-*/;
	
}
