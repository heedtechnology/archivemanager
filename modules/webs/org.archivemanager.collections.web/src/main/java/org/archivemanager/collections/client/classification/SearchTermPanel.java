package org.archivemanager.collections.client.classification;

import org.archivemanager.collections.client.ArchiveManager;
import org.heed.openapps.gwt.client.component.Toolbar;
import org.heed.openapps.gwt.client.data.EntityServiceDS;

import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class SearchTermPanel extends VLayout {
	private ArchiveManager settings = new ArchiveManager();
	private Toolbar toolbar;
	private ListGrid grid;	
	private DynamicForm addForm;
	private Label message;
	private IButton saveButton;
	private ListGridField iconField;
	
	private Record selection;
	private DSCallback callback;
	
	
	public SearchTermPanel() {
		setWidth100();
		setHeight100();
		setMargin(10);
				
		toolbar = new Toolbar(30);
		toolbar.hide();
		toolbar.addButton("add", "/theme/images/icons32/add.png", "Add", new com.smartgwt.client.widgets.events.ClickHandler() {  
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				addForm.editNewRecord();
				addForm.show();
			}  
		});
		
		addMember(toolbar);
		
		grid = new ListGrid() {
			@Override  
            protected Canvas createRecordComponent(final ListGridRecord record, Integer colNum) {  
				String fieldName = this.getFieldName(colNum);  
                if(fieldName.equals("iconField")) {  
                    HLayout recordCanvas = new HLayout(8);  
                    recordCanvas.setHeight100();
                    recordCanvas.setWidth100();
                    recordCanvas.setAlign(Alignment.CENTER);
                    
                    ImgButton editImg = new ImgButton();  
                    editImg.setShowDown(false);  
                    editImg.setShowRollOver(false);  
                    editImg.setLayoutAlign(Alignment.CENTER);  
                    editImg.setSrc("/theme/images/icons16/delete.png");  
                    editImg.setPrompt("Delete Entry");  
                    editImg.setHeight(16);  
                    editImg.setWidth(16);  
                    editImg.addClickHandler(new ClickHandler() {  
                        public void onClick(ClickEvent event) {  
                        	SC.confirm("Do you really wish to remove this permanently?", new BooleanCallback() {
            					public void execute(Boolean value) {
            						if(value) {
            							Record rec = new Record();
            							String id = record.getAttribute("target_id");
            							if(id == null) id = record.getAttribute("id");
            							rec.setAttribute("id", id);
            							EntityServiceDS.getInstance().removeEntity(rec, new DSCallback() {
            								public void execute(DSResponse response, Object rawData, DSRequest request) {
            									grid.removeData(record);
            									addForm.clearValues();
            									addForm.hide();
            								}						
            							});
            						}
            					}			
            				});
                        }  
                    });
                    recordCanvas.addMember(editImg);
                    
                    return recordCanvas;
                }
                return null;
			};
		};
		grid.setWidth100();
		grid.setHeight(150);
		grid.setShowRecordComponents(true);          
        grid.setShowRecordComponentsByCell(true);
		//grid.setBorder("1px");
		grid.setWrapCells(true);
		grid.setFixedRecordHeights(false);
		//grid.setShowHeader(false);
		
		ListGridField nameField = new ListGridField("name", "Name");
		
		iconField = new ListGridField("iconField", " ", 35);
		iconField.setCanSort(false);
		iconField.setCanFreeze(false);
		iconField.setCanGroupBy(false);
		iconField.setCanFilter(false);
		iconField.setHidden(true);
		
		grid.setFields(nameField, iconField);
		grid.addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				addForm.editRecord(event.getRecord());
				addForm.show();
				if(settings.getUser().isArchiveManager()) {
					saveButton.show();
					toolbar.show();
					iconField.setHidden(false);;
				}
			}
		});
        addMember(grid);
        
        addForm = new DynamicForm();
		addForm.setNumCols(3);
		addForm.setCellPadding(5);
		addForm.setMargin(5);
		addForm.setColWidths("75", "50", "*");
		addForm.setHeight100();
		
		final TextItem nameItem = new TextItem("name", "Name");
		nameItem.setWidth(400);
		nameItem.setColSpan(3);
		
		addForm.setFields(nameItem);
		addMember(addForm);
		addForm.hide();
		
		HLayout buttonLayout = new HLayout();
		buttonLayout.setWidth100();
		buttonLayout.setHeight(25);
		buttonLayout.setMargin(10);
		buttonLayout.setMembersMargin(17);
		addMember(buttonLayout);
		
		saveButton = new IButton("Save");
		saveButton.setHeight(25);
		saveButton.setWidth(75);
		saveButton.setIcon("/theme/images/icons16/save.png");
		saveButton.hide();
		saveButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				final Record record = addForm.getValuesAsRecord();								
				if(addForm.isNewRecord()) {
					String id = selection.getAttribute("id");
					record.setAttribute("assoc_qname", "openapps_org_classification_1_0_search_terms");
					record.setAttribute("entity_qname", "openapps_org_classification_1_0_search_term");	
					record.setAttribute("source", id);
					EntityServiceDS.getInstance().addAssociation(record, new DSCallback() {
						@Override
						public void execute(DSResponse response, Object data, DSRequest request) {
							if(response.getData() != null && response.getData().length > 0) {
								if(callback != null) callback.execute(response, data, request);
								message.setContents("entry added successfully");
							}
						}							
					});
				} else {
					String id = grid.getSelectedRecord().getAttribute("target_id");
					if(id == null) id = grid.getSelectedRecord().getAttribute("id");
					record.setAttribute("id", id);
					record.setAttribute("sources", "false");
					record.setAttribute("targets", "false");
					record.setAttribute("qname", "openapps_org_classification_1_0_search_term");
					EntityServiceDS.getInstance().updateEntity(record,new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							if(response.getData() != null && response.getData().length > 0) {
								Record resp = response.getData()[0];
								grid.getSelectedRecord().setAttribute("name", resp.getAttribute("name"));
								grid.redraw();
								message.setContents("entry saved successfully");
							}
						}						
					});					
				}
			}
		});
		buttonLayout.addMember(saveButton);
		
		message = new Label();
		message.setHeight(25);
		message.setWidth100();
		buttonLayout.addMember(message);
	}
	
	public void select(Record record) {
		this.selection = record;
		try {
			Record[] notes = new Record[0];
			Record[] source_associations = record.getAttributeAsRecordArray("source_associations");
			if(source_associations != null) {				
				for(Record source_assoc : source_associations) {					
					Record notesRecord = source_assoc.getAttributeAsRecord("search_terms");
					if(notesRecord != null) {
						notes = source_assoc.getAttributeAsRecordArray("search_terms");
					}
				}
				grid.setData(notes);
			}
			if(settings.getUser().isArchiveManager()) {
				toolbar.show();
			}
		} catch(ClassCastException e) {
			grid.setData(new Record[0]);
		}
		addForm.clearValues();
		addForm.hide();
		saveButton.hide();
	}
	public void setAddCallback(DSCallback callback) {
		this.callback = callback;
	}
}
