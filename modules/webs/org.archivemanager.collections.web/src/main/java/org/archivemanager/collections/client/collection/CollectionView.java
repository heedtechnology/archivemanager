package org.archivemanager.collections.client.collection;

import org.archivemanager.collections.client.ArchiveManager;
import org.archivemanager.collections.client.accessions.AccessionView;
import org.archivemanager.collections.client.classification.ClassificationView;
import org.archivemanager.collections.client.content.ContentView;
import org.archivemanager.collections.client.donors.DonorView;
import org.archivemanager.collections.client.locations.LocationView;
import org.archivemanager.collections.client.repository.RepositoryView;
import org.archivemanager.gwt.client.NativeRepositoryModel;
import org.heed.openapps.gwt.client.component.ActivitiesComponent;
import org.heed.openapps.gwt.client.component.NotesComponent;
import org.heed.openapps.gwt.client.component.TasksComponent;
import org.heed.openapps.gwt.client.event.ReloadListener;
import org.heed.openapps.gwt.client.event.SaveListener;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;


public class CollectionView extends TabSet {
	private ArchiveManager settings = new ArchiveManager();	
	private NativeRepositoryModel repositoryModel = new NativeRepositoryModel();
	
	private Tab formTab;
			
	private ClassificationView classificationView;
	private ContentView contentPanel;
	
	private TabSet ioPanel;
	private Tab importTab;
	private ImportPanel importPanel;
	private ExportPanel exportPanel;
	
	private LocationView locationView;
	private DonorView donorPanel;
	private AccessionView accessionPanel;
	private RepositoryView repositoryPanel;
	private DetailPanel detailPanel;
	
	private NotesComponent notesComponent;
	private ActivitiesComponent activitiesComponent;
	private TasksComponent tasksComponent;	
	
	private SaveListener saveListener;
	private ReloadListener reloadListener;
	
	
	public CollectionView() {		
		setWidth100();
		setHeight100();
				
		/** Collection Form **/
		detailPanel = new DetailPanel();
		detailPanel.setSaveListener(new SaveListener() {
			@Override
			public void save(Record record) {
				saveListener.save(record);
			}			
		});
		formTab = new Tab("Detail");
		formTab.setPane(detailPanel);
		addTab(formTab);
				
		/** Notes **/
		VLayout notesLayout = new VLayout();
		notesLayout.setMargin(25);
		notesLayout.setMembersMargin(15);
		Tab namesNotesTab = new Tab("Notes");
		namesNotesTab.setPane(notesLayout);
		addTab(namesNotesTab);
		
		notesComponent = new NotesComponent("collection", "100%", repositoryModel.getCollectionNoteTypes(), true);
		notesComponent.setMargin(5);
		notesLayout.addMember(notesComponent);
		
		HLayout lowerLayout = new HLayout();
		notesLayout.addMember(lowerLayout);
		
		tasksComponent = new TasksComponent("collectionTask", "100%", repositoryModel.getCollectionTaskTypes());
		tasksComponent.setMargin(5);		
		lowerLayout.addMember(tasksComponent);		
		
		activitiesComponent = new ActivitiesComponent("collectionActivity", "100%", repositoryModel.getCollectionActivityTypes());
		activitiesComponent.setMargin(5);		
		lowerLayout.addMember(activitiesComponent);
		
		Canvas canvas = new Canvas();
		canvas.setHeight100();
		notesLayout.addMember(canvas);
		
		
		/** Names & Subjects **/
		classificationView = new ClassificationView();
		Tab namesTab = new Tab("Names & Subjects");
		namesTab.setPane(classificationView);
		addTab(namesTab);
				
		
		/** Content **/
		Tab contentTab = new Tab("Content");
		contentPanel = new ContentView();
		contentPanel.setReloadListener(new ReloadListener() {
			public void reload() {
				reloadListener.reload();
			}			
		});
		contentTab.setPane(contentPanel);
		addTab(contentTab);
		
		
		/** Donors **/
		Tab donorTab = new Tab("Donors");
		donorPanel = new DonorView();
		donorTab.setPane(donorPanel);
		addTab(donorTab);
		
		
		/** Locations **/
		Tab locationTab = new Tab("Locations");
		locationView = new LocationView(true);
		locationTab.setPane(locationView);
		addTab(locationTab);
		
		
		/** Accessions **/
		accessionPanel = new AccessionView(true);
		accessionPanel.setTitle("Accessions");
		showTab(accessionPanel);
		
		
		/** Repositories **/
		repositoryPanel = new RepositoryView(true);
		repositoryPanel.setTitle("Repositories");
		showTab(repositoryPanel);
		
		
		/** Import/Export **/
		ioPanel = new TabSet();
		ioPanel.setWidth100();
		ioPanel.setHeight100();
		ioPanel.setTitle("Import/Export");
        showTab(ioPanel);
        
        exportPanel = new ExportPanel();
		Tab exportTab = new Tab("Export");
		exportTab.setPane(exportPanel);
		ioPanel.addTab(exportTab);
		
        importPanel = new ImportPanel();
		
	}
	
	public void reload(Record entity) {
		if(entity != null) {
			detailPanel.select(entity);
			notesComponent.select(entity);
			tasksComponent.select(entity);
			activitiesComponent.select(entity);
			classificationView.select(entity);
			contentPanel.select(entity);
			importPanel.select(entity);
			exportPanel.select(entity);
			accessionPanel.select(entity);
			locationView.select(entity);
			donorPanel.select(entity);
			repositoryPanel.select(entity);
		}
	}
	public void select(Record entity) {
		if(entity != null) {
			this.selectTab(formTab);
			detailPanel.select(entity);
			notesComponent.select(entity);
			tasksComponent.select(entity);
			activitiesComponent.select(entity);
			classificationView.select(entity);
			contentPanel.select(entity);
			importPanel.select(entity);
			exportPanel.select(entity);
			accessionPanel.select(entity);
			locationView.select(entity);
			donorPanel.select(entity);
			repositoryPanel.select(entity);
			
			String qname = entity.getAttribute("qname");
			if(qname.equals("openapps_org_repository_1_0_collection")) {
				showTab(accessionPanel);
				showTab(repositoryPanel);				
				if(importTab == null && settings.getUser().isArchiveManager()) {
					importTab = new Tab("Import");
					importTab.setPane(importPanel);
					ioPanel.addTab(importTab);
				}
				showTab(ioPanel);
			} else if(qname.equals("openapps_org_repository_1_0_category")) {
				hideTab(ioPanel);
				hideTab(repositoryPanel);
				hideTab(accessionPanel);
			} else {
				hideTab(ioPanel);
				hideTab(repositoryPanel);
				hideTab(accessionPanel);
			}
			if(settings.getUser().isArchiveManager()) {
				notesComponent.canEdit(true);
				tasksComponent.canEdit(true);
				activitiesComponent.canEdit(true);
			}
		}
	}
	
	public void setSaveListener(SaveListener saveListener) {
		this.saveListener = saveListener;
	}
	public void setReloadListener(ReloadListener reloadListener) {
		this.reloadListener = reloadListener;
	}
	
	protected void hideTab(Canvas canvas) {
		for(Tab tab : getTabs()) {
			if(tab.getPane().equals(canvas)) {
				this.updateTab(tab, null);
				this.removeTab(tab);
			}
		}
	}
	protected void showTab(Canvas canvas) {
		boolean exists = false;
		for(Tab tab : getTabs()) {
			if(tab.getPane().equals(canvas))
				exists = true;
		}
		if(!exists) {
			Tab t = new Tab(canvas.getTitle());
			t.setPane(canvas);
			this.addTab(t);
		}
	}	
}
