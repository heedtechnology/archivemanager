package org.archivemanager.collections.client.repository;

import org.archivemanager.collections.client.ArchiveManager;
import org.archivemanager.collections.client.Searchable;
import org.archivemanager.collections.client.components.PagingDisplay;
import org.heed.openapps.gwt.client.data.EntityServiceDS;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;


public class RepositoryView extends VLayout implements Searchable {
	private ArchiveManager settings = new ArchiveManager();
	private ListGrid grid;
	private DetailPanel detailPanel;
	
	private ListGrid repsitoryGrid;
	private ListGrid searchGrid;
	private VLayout vStack;
	private PagingDisplay paging;
	private AddRepositoryWindow addWindow;
	private RepositoryToolbar toolbar;
	private ImgButton rightArrowImg;
	private ImgButton leftArrowImg;
	
	private Record target;
	int startRow;
	int total;
	String action;
	String query;
	String qname = "openapps_org_repository_1_0_repository";
	
	
	public RepositoryView(boolean edit) {
		setWidth100();
		setHeight100();
		
		HLayout topLayout = new HLayout();
		topLayout.setWidth100();
		topLayout.setHeight("290px");
		addMember(topLayout);
		
		VLayout locationViewer = new VLayout();
		locationViewer.setHeight100();
		locationViewer.setWidth100();
		locationViewer.setMargin(5);
		topLayout.addMember(locationViewer);
		
		vStack = new VLayout(5);  
        vStack.setHeight100();
        vStack.setWidth("40px");
        vStack.setMembersMargin(5);
        topLayout.addMember(vStack);
        
		VLayout locationSearch = new VLayout();
		locationSearch.setHeight100();
		locationSearch.setWidth100();
		locationSearch.setMargin(5);
		topLayout.addMember(locationSearch);
				
		HLayout toolstrip = new HLayout();
		toolstrip.setWidth100();
		toolstrip.setHeight(25);
		//toolstrip.setBackgroundImage("[SKIN]/cssButton/button_stretch.png");
		//toolstrip.setBorder("1px solid #A7ABB4");
		locationViewer.addMember(toolstrip);
		
		Label label = new Label("<label style='font-size:13px;font-weight:bold;color:#4C4C4C;font-family:Arial,Verdana,sans-serif;'>Current Repositories</label>");
		label.setHeight("100%");
		label.setWidth("100%");
		toolstrip.addMember(label);
		
		repsitoryGrid = new ListGrid();
		repsitoryGrid.setWidth(345);
		repsitoryGrid.setHeight100();
		repsitoryGrid.setShowHeader(false);
		repsitoryGrid.setAutoFitData(Autofit.VERTICAL);
		repsitoryGrid.setWrapCells(true);
		repsitoryGrid.setFixedRecordHeights(false);
		repsitoryGrid.addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				Record record = repsitoryGrid.getSelectedRecord();
				Criteria criteria = new Criteria();
				String source_id = record.getAttribute("source_id");
				if(source_id == null) source_id = record.getAttribute("id");
            	criteria.setAttribute("id", source_id);
            	criteria.setAttribute("sources", true);
            	criteria.setAttribute("targets", true);
            	EntityServiceDS.getInstance().getEntity(criteria, new DSCallback() {
            		public void execute(DSResponse dsResponse, Object data, DSRequest dsRequest) {
            			if(dsResponse.getData() != null && dsResponse.getData().length > 0) {
            				Record record = dsResponse.getData()[0];
            				selectRepository(record);
            				searchGrid.deselectAllRecords();
            			}
            		}			
            	});	
			}
		});
		
		ListGridField nameField = new ListGridField("name","Name");
				
		repsitoryGrid.setFields(nameField);
		locationViewer.addMember(repsitoryGrid);
					
		Canvas spacerCanvas = new Canvas();
        spacerCanvas.setHeight("125px");
        vStack.addMember(spacerCanvas);
        
        leftArrowImg = new ImgButton();
        leftArrowImg.setWidth(36);
        leftArrowImg.setHeight(36);
        leftArrowImg.setMargin(5);
        leftArrowImg.setSrc("[SKIN]/TransferIcons/left.png");
        leftArrowImg.hide();
        leftArrowImg.addClickHandler(new ClickHandler() {  
            public void onClick(ClickEvent event) {  
            	final Record record = searchGrid.getSelectedRecord();
            	if(record != null && target != null) {
	            	Record rec = new Record();
	            	String source_id = record.getAttribute("source_id");
					if(source_id == null) source_id = record.getAttribute("id");
	            	rec.setAttribute("source", source_id);
	            	rec.setAttribute("target", target.getAttribute("id"));
	            	rec.setAttribute("assoc_qname", "openapps_org_repository_1_0_collections");
					rec.setAttribute("entity_qname", "openapps_org_repository_1_0_collection");	
					rec.setAttribute("targets", true);
					EntityServiceDS.getInstance().addAssociation(rec, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {						
							Record[] resultData = response.getData();
							if(resultData != null && resultData.length > 0 && resultData[0].getAttribute("id") != null) {
								searchGrid.removeSelectedData();
								select(resultData[0]);
								//message.setContents("donor saved successfully");
							} else {
								//message.setContents("there was a problem saving the donor");
							}
						}
					});
            	} else {
            		if(target == null) SC.warn("Error adding donor, missing valid collection");
            		else SC.warn("Error adding donor, missing valid donor");
            	}
            }  
        }); 
        vStack.addMember(leftArrowImg);
        
		rightArrowImg = new ImgButton();
		rightArrowImg.setWidth(36);
		rightArrowImg.setHeight(36);
		rightArrowImg.setMargin(5);
		rightArrowImg.setSrc("[SKIN]/TransferIcons/right.png");
		rightArrowImg.hide();
		rightArrowImg.addClickHandler(new ClickHandler() {  
            public void onClick(ClickEvent event) { 
            	final Record record = repsitoryGrid.getSelectedRecord();
            	Record rec = new Record();
            	rec.setAttribute("id", record.getAttribute("id"));
            	EntityServiceDS.getInstance().removeAssociation(rec, new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						repsitoryGrid.removeSelectedData();
						searchGrid.addData(record);
					}
				}); 
            }  
        });  
        vStack.addMember(rightArrowImg);
        
		
		toolbar = new RepositoryToolbar(this);
		toolbar.setDeleteCallback(new DSCallback() {
			@Override
			public void execute(DSResponse dsResponse, Object data, DSRequest dsRequest) {
				SC.confirm("Do you really wish to remove this permanently?", new BooleanCallback() {
					public void execute(Boolean value) {
						if(value) {
							Record rec = new Record();
							String id = searchGrid.getSelectedRecord().getAttribute("target_id");
							if(id == null) id = searchGrid.getSelectedRecord().getAttribute("id");
							rec.setAttribute("id", id);					
							EntityServiceDS.getInstance().removeEntity(rec, new DSCallback() {
								public void execute(DSResponse response, Object rawData, DSRequest request) {
									searchGrid.removeSelectedData();	
								}
							});
						}
					}
				});
			}			
		});
		toolbar.setAddCallback(new DSCallback() {
			@Override
			public void execute(DSResponse dsResponse, Object data, DSRequest dsRequest) {
				addWindow.show();
			}			
		});
		locationSearch.addMember(toolbar);
				
		searchGrid = new ListGrid();
		searchGrid.setWidth100();
		searchGrid.setHeight100();
		searchGrid.setAutoFitData(Autofit.VERTICAL);
		searchGrid.setWrapCells(true);
		searchGrid.setFixedRecordHeights(false);
		searchGrid.setShowHeader(false);
		searchGrid.setSortField("name");
		nameField = new ListGridField("name","Name");
		searchGrid.setFields(nameField);
		searchGrid.addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				Record record = searchGrid.getSelectedRecord();
				Criteria criteria = new Criteria();
				String source_id = record.getAttribute("source_id");
				if(source_id == null) source_id = record.getAttribute("id");
            	criteria.setAttribute("id", source_id);
            	criteria.setAttribute("sources", true);
            	criteria.setAttribute("targets", true);
            	EntityServiceDS.getInstance().getEntity(criteria, new DSCallback() {
            		public void execute(DSResponse dsResponse, Object data, DSRequest dsRequest) {
            			if(dsResponse.getData() != null && dsResponse.getData().length > 0) {
            				Record record = dsResponse.getData()[0];
            				selectRepository(record);
            				repsitoryGrid.deselectAllRecords();
            			}
            		}			
            	});
			}
		});
		locationSearch.addMember(searchGrid);
		
		/** Paging **/
		paging = new PagingDisplay(this);
		paging.hide();
		locationSearch.addMember(paging);
		
		TabSet tabs = new TabSet();
        tabs.setWidth100();
        tabs.setHeight100();
        addMember(tabs);
        
        detailPanel = new DetailPanel();
        Tab detailsTab = new Tab("Details");
        detailsTab.setPane(detailPanel);
		tabs.addTab(detailsTab);
		
        addWindow = new AddRepositoryWindow();
        addWindow.setCallback(new DSCallback() {	
			@Override
			public void execute(DSResponse dsResponse, Object data,	DSRequest dsRequest) {
				if(dsResponse.getData() != null && dsResponse.getData().length > 0) {
					searchGrid.addData(dsResponse.getData()[0]);
				}
			}			
		});
	}
	public void selectRepository(Record record) {
		detailPanel.select(record);
		toolbar.select(record);	
	}
	public void select(Record record) {
		this.target = record;
		try {
			Record[] notes = new Record[0];
			Record[] source_associations = record.getAttributeAsRecordArray("target_associations");
			if(source_associations != null) {				
				for(Record source_assoc : source_associations) {					
					Record notesRecord = source_assoc.getAttributeAsRecord("collections");
					if(notesRecord != null) {
						notes = source_assoc.getAttributeAsRecordArray("collections");
					}
				}
				repsitoryGrid.setData(notes);
			}
			if(settings.getUser().isAdmin() || settings.getUser().isArchiveManager()) {
				rightArrowImg.show();
				leftArrowImg.show();
			}
		} catch(ClassCastException e) {
			repsitoryGrid.setData(new Record[0]);
		}
	}
	public void canEdit(boolean editor) {
		
	}
	public void add() {
		addWindow.show();
	}
	public void delete() {
		SC.confirm("Do you really wish to remove this permanently?", new BooleanCallback() {
			public void execute(Boolean value) {
				if(value) grid.removeSelectedData();
			}			
		});		
	}
	public void search(Criteria criteria) {
		action = criteria.getAttribute("action");
		if(action != null) {
			if(criteria.getAttribute("query") != null) query = criteria.getAttribute("query");
			if(action.equals("next")) {
				startRow = startRow + 10;
			} else if(action.equals("last")) {
				startRow = total - 10;
			} else if(action.equals("prev")) {
				startRow = startRow - 10;
			} else if(action.equals("first") || action == null) {
				startRow = 0;
			}
		} else {
			query = criteria.getAttribute("query");
			startRow = 0;
		}
		criteria.setAttribute("_startRow", startRow);
		criteria.setAttribute("_endRow", startRow + 10);
		criteria.setAttribute("qname", qname);
		searchGrid.setEmptyMessage("searching repositories...");
		searchGrid.setData(new Record[0]);
		EntityServiceDS.getInstance().search(criteria, new DSCallback() {
			@Override
			public void execute(DSResponse dsResponse, Object data,	DSRequest dsRequest) {
				searchGrid.setData(dsResponse.getData());
				startRow = dsResponse.getStartRow();
				total = dsResponse.getTotalRows();
				paging.setPagingData(dsResponse.getStartRow(), dsResponse.getEndRow(), dsResponse.getTotalRows());
				paging.show();
			}						
		});
		searchGrid.setEmptyMessage("No items to show.");
	}
}
