package org.archivemanager.collections.client.collection.form;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.archivemanager.collections.client.ArchiveManager;
import org.archivemanager.collections.client.data.NativeCollectionModel;
import org.heed.openapps.gwt.client.data.NativeContentTypes;
import org.heed.openapps.gwt.client.form.ContainerItem;
import org.heed.openapps.gwt.client.form.WYSIWYGFormItem;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.SpinnerItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class ItemForm extends HLayout {
	private ArchiveManager settings = new ArchiveManager();
	private NativeCollectionModel collectionModel = new NativeCollectionModel();
	private NativeContentTypes contentTypes = new NativeContentTypes();
	
	private DynamicForm form1;
	private DynamicForm form2;
	
	private SelectItem mediumItem;
	private SelectItem genreItem;
	private SelectItem formItem;
	private TextItem authorsItem;
	private ContainerItem containerItem;
	
	public ItemForm() {
		setHeight100();
		setWidth100();
		setMembersMargin(5);
		
		form1 = new DynamicForm();
		form1.setWidth100();
		form1.setNumCols(2);
		form1.setCellPadding(5);
		form1.setColWidths("80","*");
				
		WYSIWYGFormItem nameItem = new WYSIWYGFormItem("name","Heading");
		nameItem.setHeight(75);
		nameItem.setWidth(420);
		nameItem.setShowTitle(true);
						
		WYSIWYGFormItem scopeItem = new WYSIWYGFormItem("description","Description");
		scopeItem.setHeight(190);
		scopeItem.setWidth(420);
		scopeItem.setShowTitle(true);
				
		WYSIWYGFormItem summaryItem = new WYSIWYGFormItem("summary","Summary");
		summaryItem.setHeight(190);
		summaryItem.setWidth(420);
		summaryItem.setShowTitle(true);
		
		TextAreaItem commentsItem = new TextAreaItem("comments","Coments");
		commentsItem.setHeight(75);
		commentsItem.setWidth(430);
				
		authorsItem = new TextItem("authors", "Authors");
		authorsItem.setWidth(430);
		
		containerItem = new ContainerItem("container", "Container", collectionModel.getContainerTypes());
		containerItem.setWidth(430);
		
		form1.setFields(nameItem,scopeItem,summaryItem,commentsItem,authorsItem,containerItem);
		addMember(form1);
		
		VLayout rightLayout = new VLayout();
		rightLayout.setWidth100();
		rightLayout.setHeight100();
		addMember(rightLayout);
		
		form2 = new DynamicForm();
		form2.setWidth100();
		form2.setNumCols(2);
		form2.setCellPadding(5);
		form2.setMargin(7);
		form2.setColWidths("100","*");		
		
		List<FormItem> fields = new ArrayList<FormItem>();
		
		StaticTextItem idItem = new StaticTextItem("id", "ID");
		fields.add(idItem);
		
		SelectItem languageItem = new SelectItem("language", "Language");
		languageItem.setWidth(150);
		languageItem.setValueMap(collectionModel.getLanguageCodes());
		fields.add(languageItem);
									
		SelectItem levelItem = new SelectItem("level", "Level");
		levelItem.setValueMap(collectionModel.getItemLevels());
		fields.add(levelItem);
		
		SelectItem contentTypeItem = new SelectItem("qname", "Content Type");
		contentTypeItem.setValueMap(contentTypes.getContentTypes());
		fields.add(contentTypeItem);
		
		genreItem = new SelectItem("genre","Genre");
		genreItem.setValueMap(new LinkedHashMap<String,Object>(0));
		fields.add(genreItem);
		
		formItem = new SelectItem("form","Form");
		formItem.setValueMap(contentTypes.getCorrespondenceForms());
		fields.add(formItem);
		
		mediumItem = new SelectItem("medium","Medium");
		mediumItem.setValueMap(contentTypes.getCorrespondenceForms());
		fields.add(mediumItem);
		
		TextItem accessionDateItem = new TextItem("accession_date", "Accession Date");
		fields.add(accessionDateItem);
		
		TextItem dateItem = new TextItem("date_expression", "Date Expression");
		fields.add(dateItem);
		
		TextItem beginItem = new TextItem("begin", "Begin Date");
		fields.add(beginItem);
		
		TextItem endItem = new TextItem("end", "End Date");
		fields.add(endItem);
		
		SelectItem extentUnitsItem = new SelectItem("extent_units", "Extent Units");
		extentUnitsItem.setWidth(100);
		extentUnitsItem.setValueMap(collectionModel.getExtentUnits());
		fields.add(extentUnitsItem);
		
		SpinnerItem extentValueItem = new SpinnerItem("extent_number", "Extent Value");
		extentValueItem.setWidth(60);
		fields.add(extentValueItem);
		
		SpinnerItem sizeItem = new SpinnerItem("size", "Size %");
		sizeItem.setMax(0);
		sizeItem.setMax(100);
		sizeItem.setWidth(60);
		fields.add(sizeItem);
		
		CheckboxItem restrictionItem = new CheckboxItem("restrictions", "Restrictions");
		restrictionItem.setHeight(35);
		restrictionItem.setLabelAsTitle(true);
		fields.add(restrictionItem);
		
		CheckboxItem internalItem = new CheckboxItem("internal", "Internal");
		internalItem.setHeight(35);
		internalItem.setLabelAsTitle(true);
		fields.add(internalItem);
		
		FormItem[] itemArray = fields.toArray(new FormItem[fields.size()]);		
		form2.setFields(itemArray);
		rightLayout.addMember(form2);
		
		Canvas spacer = new Canvas();
		spacer.setHeight(20);
		rightLayout.addMember(spacer);
		
	}
	
	public void editRecord(Record record) {
		form1.clearValues();
		form2.clearValues();		
		/*
		if(items.size() == 0) {
			for(FormItem item : form1.getFields()) items.add(item);
			for(FormItem item : form2.getFields()) items.add(item);
		}		
		for(FormItem item : items) {
			String val = record.getAttributeAsString(item.getName());
			if(val != null && !val.equals("")) {
				if(item instanceof SpinnerItem) item.setValue(Integer.parseInt(val));
				if(item instanceof CheckboxItem) item.setValue(Boolean.parseBoolean(val));
				else item.setValue(val);
			} else {
				if(item instanceof SpinnerItem) item.setValue(0);
				else item.setValue("");
			}
		}
		*/
		form1.editRecord(record);
		form2.editRecord(record);
		
		genreItem.show();
		formItem.show();
		mediumItem.show();
		authorsItem.show();
		String type = record.getAttribute("localName");
		if(record.getAttribute("restrictions") != null) 
			record.setAttribute("restrictions", Boolean.valueOf(record.getAttribute("restrictions")));
		if(record.getAttribute("internal") != null) 
			record.setAttribute("internal", Boolean.valueOf(record.getAttribute("internal")));
		containerItem.setValue(record.getAttribute("container"));
		
		if(type != null) {
			if(type.equals("artwork")) {
				//aspectArray['artwork']=new Array('artwork_form','artwork_genre','artwork_media','artwork_size','authors');
				genreItem.setValueMap(contentTypes.getArtworkGenre());
				mediumItem.setValueMap(contentTypes.getArtworkMedium());
				formItem.setValueMap(contentTypes.getArtworkForms());
			} else if(type.equals("audio")) {
				//aspectArray['audio']=new Array('audio_medium','authors');
				genreItem.hide();
				mediumItem.setValueMap(contentTypes.getAudioMedium());
				formItem.hide();
			} else if(type.equals("correspondence")) {
				//aspectArray['correspondence']=new Array('correspondence_genre','correspondence_form','authors');
				genreItem.setValueMap(contentTypes.getCorrespondenceGenre());
				formItem.setValueMap(contentTypes.getCorrespondenceForms());
				mediumItem.hide();
			} else if(type.equals("financial")) {
				//aspectArray['financial']=new Array('financial_genre','authors');
				genreItem.setValueMap(contentTypes.getFinancialGenre());
				formItem.hide();
				mediumItem.hide();
			} else if(type.equals("journals")) {
				//aspectArray['journals']=new Array('journals_genre','journals_form','authors');
				genreItem.setValueMap(contentTypes.getJournalsGenre());
				formItem.setValueMap(contentTypes.getJournalsForms());
				mediumItem.hide();
			} else if(type.equals("legal")) {
				//aspectArray['legal']=new Array('legal_genre','authors');
				genreItem.setValueMap(contentTypes.getLegalGenre());
				formItem.hide();
				mediumItem.hide();
			} else if(type.equals("manuscript")) {
				//aspectArray['manuscript']=new Array('manuscript_genre','manuscript_form','authors');
				genreItem.setValueMap(contentTypes.getManuscriptGenre());
				formItem.setValueMap(contentTypes.getManuscriptForms());
				mediumItem.hide();
			} else if(type.equals("medical")) {
				//aspectArray['medical']=new Array('medical_genre','authors');
				genreItem.setValueMap(contentTypes.getMedicalGenre());
				formItem.hide();
				mediumItem.hide();
			} else if(type.equals("memorabilia")) {
				//aspectArray['memorabilia']=new Array('memorabilia_genre','authors');
				genreItem.setValueMap(contentTypes.getMemorabiliaGenre());
				formItem.hide();
				mediumItem.hide();
			} else if(type.equals("miscellaneous")) {
				//aspectArray['miscellaneous']=new Array();
				genreItem.hide();
				formItem.hide();
				mediumItem.hide();
			} else if(type.equals("notebooks")) {
				//aspectArray['notebooks']=new Array('notebooks_form','notebooks_genre','authors');
				genreItem.setValueMap(contentTypes.getNotebooksGenre());
				formItem.setValueMap(contentTypes.getNotebooksForms());
				mediumItem.hide();
			} else if(type.equals("photographs")) {
				//aspectArray['photographs']=new Array('photograph_form','photograph_size','authors');
				genreItem.hide();
				formItem.setValueMap(contentTypes.getPhotographsForms());
				mediumItem.hide();
			} else if(type.equals("printed_material")) {
				//aspectArray['printed_material']=new Array('printed_genre','authors');
				genreItem.setValueMap(contentTypes.getPrintedGenre());
				formItem.hide();
				mediumItem.hide();
			} else if(type.equals("professional")) {
				//aspectArray['professional']=new Array('professional_genre','authors');
				genreItem.setValueMap(contentTypes.getProfessionalGenre());
				formItem.hide();
				mediumItem.hide();
			} else if(type.equals("research")) {
				//aspectArray['research']=new Array('research_genre','authors');
				genreItem.setValueMap(contentTypes.getResearchGenre());
				formItem.hide();
				mediumItem.hide();
			} else if(type.equals("scrapbooks")) {
				//aspectArray['scrapbooks']=new Array('scrapbooks_form','authors');
				genreItem.hide();
				formItem.setValueMap(contentTypes.getScrapbooksForms());
				mediumItem.hide();
			} else if(type.equals("video")) {
				//aspectArray['video']=new Array('video_medium','video_form','authors');
				genreItem.hide();
				formItem.setValueMap(contentTypes.getVideoForms());
				mediumItem.setValueMap(contentTypes.getVideoMedium());				
			}
		}
		if(settings.getUser().isAdmin() || settings.getUser().isArchiveManager()) {
			form1.setDisabled(false);
			form2.setDisabled(false);
		} else {
			form1.setDisabled(true);
			form2.setDisabled(true);			
		}
	}
	public Record getValuesAsRecord() {
		Record record = form1.getValuesAsRecord();		
		for(FormItem item : form2.getFields()) {
			Object val = item.getValue();
			if(val != null && !val.equals("")) record.setAttribute(item.getName(), val);
			else record.setAttribute(item.getName(), "");
		}				
		return record;
	}
}
