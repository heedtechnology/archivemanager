package org.archivemanager.collections.client.collection;
import java.util.LinkedHashMap;

import org.heed.openapps.gwt.client.data.NativeContentTypes;
import org.heed.openapps.gwt.client.event.AddListener;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;


public class AddCollectionObjectWindow extends Window {
	private NativeContentTypes model = new NativeContentTypes();
	private ButtonItem submitItem;
	private SelectItem levelItem;
	private SelectItem contentTypeItem;
	private DynamicForm addForm;
	
	private AddListener addListener;
	
	
	public AddCollectionObjectWindow() {
		setWidth(370);
		setHeight(130);
		setTitle("Add Category/Item");
		setAutoCenter(true);
		setIsModal(true);
		
		addForm = new DynamicForm();
		addForm.setMargin(3);
		addForm.setCellPadding(5);
		addForm.setColWidths(70, "*");
		final TextItem nameItem = new TextItem("name", "Name");
		nameItem.setWidth("*");
		
		levelItem = new SelectItem("level", "Level");
		LinkedHashMap<String,String> valueMap = new LinkedHashMap<String,String>();
		valueMap.put("series","Series");
		valueMap.put("subseries","SubSeries");
		valueMap.put("group","Group");
		valueMap.put("subgroup","SubGroup");
		valueMap.put("file","File");
		valueMap.put("item","Item");
		levelItem.setValueMap(valueMap);
		levelItem.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				if(event.getItem().getValue().equals("item")) {
					contentTypeItem.show();
					setHeight(165);
				} else {
					contentTypeItem.hide();
					setHeight(130);
				}
			}				
		});
		
		contentTypeItem = new SelectItem("type", "Type");
		contentTypeItem.setValueMap(model.getContentTypes());
		contentTypeItem.setVisible(false);
		
		submitItem = new ButtonItem("submit", "Add");
		//submitItem.setIcon("/theme/images/icons16/add.png");
		submitItem.setStartRow(false);
		submitItem.setEndRow(false);
		submitItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
				String level = levelItem.getValueAsString();
				Record record = new Record();
				record.setAttribute("level", level);
				if(level.equals("item")) {
					record.setAttribute("assoc_qname", "openapps_org_repository_1_0_items");
					record.setAttribute("entity_qname", contentTypeItem.getValueAsString());
				} else {
					record.setAttribute("assoc_qname", "openapps_org_repository_1_0_categories");
					record.setAttribute("entity_qname", "openapps_org_repository_1_0_category");
				}					
				record.setAttribute("name", nameItem.getValueAsString());
				record.setAttribute("format", "tree");
				addForm.clearValues();
				addListener.add(record);
				hide();
			}
		});
		
		addForm.setFields(nameItem,levelItem,contentTypeItem,submitItem);
		addItem(addForm);
	}
	public void setAddListener(AddListener addListener) {
		this.addListener = addListener;
	}
}