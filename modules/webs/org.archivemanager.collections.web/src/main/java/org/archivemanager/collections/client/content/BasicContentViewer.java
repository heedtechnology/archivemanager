package org.archivemanager.collections.client.content;

import org.archivemanager.gwt.client.data.ArchiveManagerContentStreamingDS;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.EventTypes;
import org.heed.openapps.gwt.client.Mimetypes;
import org.heed.openapps.gwt.client.OpenAppsEvent;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.ContentsType;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.HTMLPane;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.menu.Menu;


public class BasicContentViewer extends Canvas {
	private VLayout	videoCanvas;
	private HTMLPane iframeCanvas;
	private Menu contextMenu;
	
	private Record selection;
		
	public BasicContentViewer() {
		setWidth100();
		setHeight100();
		setBorder("1px solid #C0C3C7");
		
        videoCanvas = new VLayout();
        videoCanvas.setWidth100();
        videoCanvas.setHeight(300);
        videoCanvas.setBorder("1px solid #C0C3C7");
        videoCanvas.hide();
        addChild(videoCanvas);
        
        Canvas videoPlayer = new Canvas();
        videoPlayer.setWidth100();
        videoPlayer.setHeight(300);        
        videoPlayer.setContents("<div id='vplayer' style='width:448px;height:300px;'>video player</div>");
        videoCanvas.addMember(videoPlayer);
               
        iframeCanvas = new HTMLPane();
        iframeCanvas.setWidth100();
        iframeCanvas.setHeight100();
        iframeCanvas.setContentsType(ContentsType.PAGE);
        iframeCanvas.setShowEdges(false);
        iframeCanvas.setContentsURL("/theme/images/logo/ArchiveManager-viewer.png");
        addChild(iframeCanvas);
        /*
        EventBus.addHandler(org.heed.openapps.gwt.client.OpenAppsEvent.TYPE, new OpenAppsEventHandler()     {
	        @Override
	        public void onEvent(OpenAppsEvent event) {
	            if(event.isType(EventTypes.FILE_SELECTED)) {
	            	String name = event.getRecord().getAttribute("name").toLowerCase();
	            	String uid = event.getRecord().getAttribute("uid");
	            	String mimetype = getMimetype(name);
	            	if(isVideo(mimetype)) {
						iframeCanvas.hide();
						videoCanvas.show();
						playVideo(id);
					} else {
						videoCanvas.hide();
						iframeCanvas.show();
						iframeCanvas.setContentsURL("/service/archivemanager/content/stream/"+uid+"."+getExtension(name));
					} 
	            }
	            else if(event.isType(EventTypes.WEBLINK_SELECTED)) select(event.getRecord());
	            else if(event.isType(EventTypes.WEBCONTENT_SELECTED)) select(event.getRecord());
	        }
	    });
	    */
	}
	public void refresh() {
		videoCanvas.hide();
		iframeCanvas.show();
		iframeCanvas.setContentsURL("/theme/images/logo/ArchiveManager-viewer.png");
	}
	public void select(Record record) {
		//String id = record.getAttribute("uid");
		//if(this.selection == null || (id != null && !this.selection.getAttribute("uid").equals(id))) {
			this.selection = record;
			String type = record.getAttribute("localName");
			if(type != null) {
				if(type.equals("file") || type.equals("web_link")) {
					String url = record.getAttribute("url");
					videoCanvas.hide();
					iframeCanvas.show();
					iframeCanvas.setContentsURL(url);
				} else if(type.equals("web_content")) {
					String content = record.getAttribute("content");
					videoCanvas.hide();
					iframeCanvas.show();
					iframeCanvas.setContents(content);
				}				
			}
		//}
	}
	
	public void setContextMenu(Menu contextMenu) {
		this.contextMenu = contextMenu;
	}

	protected void fetch(String id, int page) {
		Criteria criteria = new Criteria();
		criteria.setAttribute("id", id);
		criteria.setAttribute("page", page);
		ArchiveManagerContentStreamingDS.getInstance().filterData(criteria, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				if(response.getData().length > 0) {
					Record fileRec = response.getData()[0];
					if(fileRec != null) {
						try {
							String id = fileRec.getAttribute("id");
							String mimetype = fileRec.getAttribute("mimetype");
							if(isVideo(mimetype)) {
								iframeCanvas.hide();
								videoCanvas.show();
								playVideo(id);
							} 
						} catch(Exception e) {
							//image.setSrc(rendering_image);
						}
						EventBus.fireEvent(new OpenAppsEvent(EventTypes.FILE_RECEIVED, fileRec));
					}
				}
				if(response != null) {
					int status = response.getStatus();
					if(status < 0)
						EventBus.fireEvent(new OpenAppsEvent(EventTypes.FILE_ERROR));
				}
			}					
		});
	}
	protected boolean isVideo(String mimetype) {		
		return mimetype.equals(Mimetypes.MIMETYPE_AVI) || 
				mimetype.equals(Mimetypes.MIMETYPE_AVI) || 
				mimetype.equals(Mimetypes.MIMETYPE_MOV) ||
				//mimetype.equals(Mimetypes.MIMETYPE_MP4) ||
				mimetype.equals(Mimetypes.MIMETYPE_MP3) ||
				mimetype.equals(Mimetypes.MIMETYPE_MPEG) ||
				mimetype.equals(Mimetypes.MIMETYPE_WAV) ||
				mimetype.equals(Mimetypes.MIMETYPE_FLV);
	}
	protected String getMimetype(String name) {
		if(name != null) {
			String fileName = name.toLowerCase();
			if(fileName.endsWith(".gif")) return Mimetypes.MIMETYPE_IMAGE_GIF;
			if(fileName.endsWith(".jpg")) return Mimetypes.MIMETYPE_IMAGE_JPEG;
			if(fileName.endsWith(".pdf")) return Mimetypes.MIMETYPE_PDF;
			if(fileName.endsWith(".tif") || fileName.endsWith(".tiff")) return Mimetypes.MIMETYPE_IMAGE_TIF;
			if(fileName.endsWith(".mp3")) return Mimetypes.MIMETYPE_MP3;
			if(fileName.endsWith(".mp4")) return Mimetypes.MIMETYPE_MP4;
			if(fileName.endsWith(".flv")) return Mimetypes.MIMETYPE_FLV;
			if(fileName.endsWith(".xml")) return Mimetypes.MIMETYPE_XML;
			if(fileName.endsWith(".xls")) return Mimetypes.MIMETYPE_EXCEL;
			if(fileName.endsWith(".ppt")) return Mimetypes.MIMETYPE_PPT;
			if(fileName.endsWith(".doc")) return Mimetypes.MIMETYPE_WORD;
		}
		return "";
	}
	protected String getExtension(String filename) {
		if (filename == null) {
			return null;
		}
		// get extension
		int pos = filename.lastIndexOf('.');
		if (pos == -1 || pos == filename.length() - 1)
			return null;
		String ext = filename.substring(pos + 1, filename.length());
		ext = ext.toLowerCase();
		return ext;
	}
	
	native void playVideo(String id) /*-{
		$wnd.playVideo(id);
	}-*/;
}