package org.archivemanager.collections.client.content;
import java.util.ArrayList;
import java.util.List;

import org.archivemanager.collections.client.ArchiveManager;
import org.heed.openapps.gwt.client.data.ContentServiceDS;
import org.heed.openapps.gwt.client.data.EntityServiceDS;
import org.heed.openapps.gwt.client.data.NativeContentModel;
import org.heed.openapps.gwt.client.event.ReloadListener;
import org.heed.openapps.gwt.client.event.SaveListener;
import org.heed.openapps.gwt.client.event.SelectionListener;
import org.heed.openapps.gwt.client.event.UploadListener;

import com.google.gwt.user.client.ui.Grid;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class ContentView extends HLayout {
	private ArchiveManager settings = new ArchiveManager();	
	private BasicContentViewer viewer;
	private AddContentWindow addContentWindow;
	private UploadContentWindow uploadContentWindow;
	private AddWeblinkWindow addWebLinkWindow;
	private NativeContentModel contentModel = new NativeContentModel();
	
	private ImgButton delButton;
	private ImgButton editButton;
	private ImgButton addButton;
	private ListGrid grid;
	private HLayout buttons;
	
	private Label urlLabel;
	private Label urlValue;
	private Label descriptionLabel;
	private Label descriptionValue;
	private Label widthLabel;
	private Label widthValue;
	private Label heightLabel;
	private Label heightValue;
	private Label typeLabel;
	private Label typeValue;
	private Label groupLabel;
	private Label groupValue;
	private Label orderLabel;
	private Label orderValue;
	
	private ReloadListener reloadListener;
	
	
	public ContentView() {
		setWidth100();
		setHeight100();
		setMembersMargin(5);
		
		VLayout leftLayout = new VLayout();
		leftLayout.setWidth(250);
		leftLayout.setHeight100();
		//leftLayout.setMembersMargin(5);
		addMember(leftLayout);
		
		VLayout rightLayout = new VLayout();
		rightLayout.setWidth100();
		rightLayout.setHeight100();		
		addMember(rightLayout);
		
		HLayout spacer = new HLayout();
		spacer.setHeight(22);
		leftLayout.addMember(spacer);
		
		HLayout toolstrip = new HLayout();
		toolstrip.setWidth100();
		toolstrip.setHeight(24);
		leftLayout.addMember(toolstrip);
		
		Label label = new Label("<label style='font-size:13px;font-weight:bold;color:#4C4C4C;font-family:Arial,Verdana,sans-serif;'>Web Content</label>");
		label.setHeight("100%");
		label.setWidth("100%");
		toolstrip.addMember(label);
		
		buttons = new HLayout();
		buttons.setHeight(24);
		buttons.setMembersMargin(3);
		buttons.setMargin(2);
		toolstrip.addMember(buttons);
		
		addButton = new ImgButton();
		addButton.setWidth(16);
		addButton.setHeight(16);
		addButton.setShowDown(false);
		addButton.setShowRollOver(false);
		addButton.setVisible(false);
		addButton.setSrc("/theme/images/icons32/add.png");
		addButton.setPrompt("Add");
		addButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				addContentWindow.show();
			}
		});
		buttons.addMember(addButton);
		/*
		editButton = new ImgButton();
		editButton.setWidth(16);
		editButton.setHeight(16);
		editButton.setShowDown(false);
		editButton.setShowRollOver(false);
		editButton.setVisible(false);
		editButton.setPrompt("View");
		editButton.setSrc("/theme/images/icons32/zoom.png");
		editButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				if(grid.getSelectedRecord() != null) {
					addWebLinkWindow.select(grid.getSelectedRecord());
					addWebLinkWindow.show();
				} else SC.say("Please select a file to view");
			}
		});
		buttons.addMember(editButton);
		*/			
		delButton = new ImgButton();
		delButton.setWidth(16);
		delButton.setHeight(16);
		delButton.setVisible(false);
		delButton.setShowDown(false);
		delButton.setShowRollOver(false);
		delButton.setPrompt("Delete");
		delButton.setSrc("/theme/images/icons32/delete.png");
		delButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Record rec = new Record();
				String id = grid.getSelectedRecord().getAttribute("target_id");
				String localName = grid.getSelectedRecord().getAttribute("localName");
				if(id == null) id = grid.getSelectedRecord().getAttribute("id");
				rec.setAttribute("id", id);				
				if(localName != null && localName.equals("web_link")) {
					EntityServiceDS.getInstance().removeEntity(rec, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							int status = response.getStatus();
			       			if(status >= 0) {
			       				grid.removeSelectedData();
			       				clearValues();
			       				delButton.hide();
			       				editButton.hide();
			       			} else {
			       				SC.warn("Exception removing content", "Exception removing content, please notify your administrator.");
			       			}
						}
					});
				} else if(localName != null && localName.equals("file")) {					
					ContentServiceDS.getInstance().removeFile(rec, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							int status = response.getStatus();
			       			if(status >= 0) {
			       				grid.removeSelectedData();
			       				clearValues();
			       				delButton.hide();
			       				editButton.hide();
			       			} else {
			       				SC.warn("Exception removing content", "Exception removing content, please notify your administrator.");
			       			}
						}
					});
				} else SC.warn("Problem deleting "+localName);
			}
		});
		buttons.addMember(delButton);
			    		
		grid = new ListGrid();
		grid.setWidth100();
		grid.setHeight(48);
		grid.setSortField("label");
		grid.setAutoFitData(Autofit.VERTICAL);
		grid.setAutoFitMaxHeight(500);
		grid.setShowHeader(false);
		grid.setWrapCells(true);
		grid.setFixedRecordHeights(false);
		grid.addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				String localName = grid.getSelectedRecord().getAttribute("localName");
				String url = grid.getSelectedRecord().getAttribute("url");
				String type = grid.getSelectedRecord().getAttribute("type");
				String description = grid.getSelectedRecord().getAttribute("description");
				String width = grid.getSelectedRecord().getAttribute("width");
				String height = grid.getSelectedRecord().getAttribute("height");
				String group = grid.getSelectedRecord().getAttribute("group");
				String order = grid.getSelectedRecord().getAttribute("order");
				
				viewer.select(grid.getSelectedRecord());
				
				if(url != null) urlValue.setContents(url);
				else urlValue.setContents("");
				
				if(type != null) {
					String typeVal = (String)contentModel.getWebLinkTypes().get(type);
					if(typeVal != null)	typeValue.setContents("<span style='font-size:12px;'>"+typeVal+"</span>");
					else typeValue.setContents("<span style='font-size:12px;'>"+type+"</span>");
				} else typeValue.setContents("");
				
				if(localName.equals("file")) {
					if(height != null) heightValue.setContents("<span style='font-size:12px;'>"+height+"px</span>");
					else heightValue.setContents("");
					
					if(width != null) widthValue.setContents("<span style='font-size:12px;'>"+width+"px</span>");
					else widthValue.setContents("");
										
					widthLabel.setVisible(true);
					widthValue.setVisible(true);
					heightLabel.setVisible(true);
					heightValue.setVisible(true);
				} else {
					widthLabel.setVisible(false);
					widthValue.setVisible(false);
					heightLabel.setVisible(false);
					heightValue.setVisible(false);
				}
				urlLabel.setVisible(true);
				urlValue.setVisible(true);
				typeLabel.setVisible(true);
				typeValue.setVisible(true);
				
				if(description != null) descriptionValue.setContents("<span style='font-size:12px;'>"+description+"</span>");
				else descriptionValue.setContents("");
				descriptionLabel.setVisible(true);
				descriptionValue.setVisible(true);
				
				groupLabel.setVisible(true);
				groupValue.setVisible(true);
				orderLabel.setVisible(true);
				orderValue.setVisible(true);
				if(group != null) groupValue.setContents("<span style='font-size:12px;'>"+group+"</span>");
				else groupValue.setContents("");
				if(order != null) orderValue.setContents("<span style='font-size:12px;'>"+order+"</span>");
				else orderValue.setContents("");
				
				if(settings.getUser().isArchiveManager()) {
					delButton.show();
					if(localName.equals("web_link"))
						editButton.show();
					else
						editButton.hide();
				}
			}
		});
		ListGridField typeField = new ListGridField("localName");
		typeField.setAlign(Alignment.CENTER);  
		typeField.setType(ListGridFieldType.IMAGE);  
        typeField.setImageURLPrefix("/theme/images/icons16/");  
        typeField.setImageURLSuffix(".png");
        typeField.setWidth(30);
        
		ListGridField labelField = new ListGridField("label");		
				
		grid.setFields(typeField,labelField);
		leftLayout.addMember(grid);
				
		HLayout spacer2 = new HLayout();
		spacer2.setHeight(22);
		rightLayout.addMember(spacer2);
		
		viewer = new BasicContentViewer();
		rightLayout.addMember(viewer);
		
		
		/** Panel 1 **/
		Grid detailPanel1 = new Grid(2, 2);
		detailPanel1.setWidth("400px");
		detailPanel1.setCellPadding(5);
		rightLayout.addMember(detailPanel1);
		
		urlLabel = new Label("<span style='font-size:12px;font-weight:bold;'>URL :</span>");
		urlLabel.setHeight(20);
		urlLabel.setWidth(35);
		detailPanel1.setWidget(0, 0, urlLabel);
		
		urlValue = new Label("");
		urlValue.setWidth(400);
		urlValue.setHeight(35);
		detailPanel1.setWidget(0, 1, urlValue);
		
		descriptionLabel = new Label("<span style='font-size:12px;font-weight:bold;'>Description :</span>");
		descriptionLabel.setHeight(20);
		descriptionLabel.setWidth(75);
		detailPanel1.setWidget(1, 0, descriptionLabel);
		
		descriptionValue = new Label("");
		descriptionValue.setHeight(35);
		descriptionValue.setWidth(400);
		detailPanel1.setWidget(1, 1, descriptionValue);
		
		
		/** Panel 2 **/
		Grid detailPanel2 = new Grid(1, 10);
		detailPanel2.setHeight("35px");
		detailPanel2.setWidth("400px");
		detailPanel2.setCellPadding(5);
		rightLayout.addMember(detailPanel2);
	    
		typeLabel = new Label("<span style='font-size:12px;font-weight:bold;'>Type :</span>");
		typeLabel.setHeight(20);
		typeLabel.setWidth(35);
		detailPanel2.setWidget(0, 0, typeLabel);
		
		typeValue = new Label("");
		typeValue.setHeight(20);
		detailPanel2.setWidget(0, 1, typeValue);
		
		widthLabel = new Label("<span style='font-size:12px;font-weight:bold;'>Width :</span>");
		widthLabel.setHeight(20);
		widthLabel.setWidth(40);
		detailPanel2.setWidget(0, 2, widthLabel);
		
		widthValue = new Label("");
		widthValue.setHeight(20);
		widthValue.setWidth(45);
		detailPanel2.setWidget(0, 3, widthValue);
		
		heightLabel = new Label("<span style='font-size:12px;font-weight:bold;'>Height :</span>");
		heightLabel.setHeight(20);
		heightLabel.setWidth(45);
		detailPanel2.setWidget(0, 4, heightLabel);
		
		heightValue = new Label("");
		heightValue.setHeight(20);
		heightValue.setWidth(45);
		detailPanel2.setWidget(0, 5, heightValue);	
		
		
		/** Panel 3 **/
		Grid detailPanel3 = new Grid(1, 4);
		detailPanel3.setHeight("35px");
		detailPanel3.setWidth("400px");
		detailPanel3.setCellPadding(5);
		rightLayout.addMember(detailPanel3);
		
		groupLabel = new Label("<span style='font-size:12px;font-weight:bold;'>Group :</span>");
		groupLabel.setHeight(20);
		groupLabel.setWidth(50);
		detailPanel3.setWidget(0, 0, groupLabel);
		
		groupValue = new Label("");
		groupValue.setHeight(20);
		groupValue.setWidth(35);
		detailPanel3.setWidget(0, 1, groupValue);
		
		orderLabel = new Label("<span style='font-size:12px;font-weight:bold;'>Order :</span>");
		orderLabel.setHeight(20);
		orderLabel.setWidth(50);
		detailPanel3.setWidget(0, 2, orderLabel);
		
		orderValue = new Label("");
		orderValue.setHeight(20);
		orderValue.setWidth(45);
		detailPanel3.setWidget(0, 3, orderValue);
		
		addContentWindow = new AddContentWindow();
	    addContentWindow.setSelectionListener(new SelectionListener() {
			public void select(Record record) {
				if(record.getAttribute("contentType").equals("web_link")) {
					addWebLinkWindow.show();
				} else {
					uploadContentWindow.show();
				}
			}	    	
	    });
	    uploadContentWindow = new UploadContentWindow();
	    uploadContentWindow.setUploadListener(new UploadListener() {
			public void uploadComplete(String sessionKey) {
				reloadListener.reload();
			}
	    });
	    addWebLinkWindow = new AddWeblinkWindow();
	    addWebLinkWindow.setSaveListener(new SaveListener() {
			@Override
			public void save(Record record) {
				EntityServiceDS.getInstance().addAssociation(record, new DSCallback() {
					@Override
					public void execute(DSResponse dsResponse, Object data,	DSRequest dsRequest) {
						reloadListener.reload();
						addWebLinkWindow.close();
					}					
				});
			}	    	
	    });
	}
	public void canEdit(boolean editor) {
		
	}
	public void select(Record record) {
		addWebLinkWindow.select(record);
		uploadContentWindow.select(record);		
		clearValues();
		try {
			List<Record> items = new ArrayList<Record>();
			Record[] source_associations = record.getAttributeAsRecordArray("source_associations");
			if(source_associations != null) {				
				for(Record source_assoc : source_associations) {					
					Record notesRecord = source_assoc.getAttributeAsRecord("web_links");
					if(notesRecord != null) {
						Record[] links = source_assoc.getAttributeAsRecordArray("web_links");
						for(Record link : links) {
							items.add(link);
						}
					}
					Record filesRecord = source_assoc.getAttributeAsRecord("files");
					if(filesRecord != null) {
						Record[] files = source_assoc.getAttributeAsRecordArray("files");
						for(Record file : files) {
							items.add(file);
						}
					}					
				}				
			}
			setData(items.toArray(new Record[items.size()]));
			if(settings.getUser().isArchiveManager()) {
				addButton.show();
			}
		} catch(ClassCastException e) {
			setData(new Record[0]);
		}
	}
	public void setData(Record[] records) {
		if(records.length > 0) {
			for(Record record : records) {
				String label = "<label style='font-size:11px;font-weight:bold;color:#4C4C4C;font-family:Arial,Verdana,sans-serif;'>"+record.getAttribute("name")+"</label>";
				record.setAttribute("label", label);
			}
			grid.setData(records);
		} else {			
			grid.setData(new Record[0]);
		}
	}
	public void setReloadListener(ReloadListener reloadListener) {
		this.reloadListener = reloadListener;
	}
	
	protected void clearValues() {
		urlValue.setContents("");
		heightValue.setContents("");
		widthValue.setContents("");
		typeValue.setContents("");
		descriptionValue.setContents("");
		
		urlLabel.setVisible(false);
		urlValue.setVisible(false);
		descriptionLabel.setVisible(false);
		descriptionValue.setVisible(false);
		typeLabel.setVisible(false);
		typeValue.setVisible(false);
		widthLabel.setVisible(false);
		widthValue.setVisible(false);
		heightLabel.setVisible(false);
		heightValue.setVisible(false);
		
		groupLabel.setVisible(false);
		groupValue.setVisible(false);
		orderLabel.setVisible(false);
		orderValue.setVisible(false);
		groupValue.setContents("");
		orderValue.setContents("");
		
		viewer.refresh();
		
	}
}
