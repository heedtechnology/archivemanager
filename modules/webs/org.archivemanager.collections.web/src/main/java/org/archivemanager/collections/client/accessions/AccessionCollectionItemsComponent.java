package org.archivemanager.collections.client.accessions;

import org.archivemanager.collections.client.ArchiveManager;
import org.heed.openapps.gwt.client.data.EntityServiceDS;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.types.DSDataFormat;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tree.TreeGrid;


public class AccessionCollectionItemsComponent extends VLayout {
	private ArchiveManager settings = new ArchiveManager();
	private Record accession;
	
	private HLayout buttons;
	private ImgButton delButton;
	private ListGrid grid;
	private AddItemWindow addItemWindow;
	private CollectionTreeDS collectionTreeDS = new CollectionTreeDS();
	
	private String collectionId = "0";
	
	public AccessionCollectionItemsComponent(String width) {
		HLayout toolstrip = new HLayout();
		toolstrip.setWidth(width);
		toolstrip.setHeight(20);
		addMember(toolstrip);
		
		Label label = new Label("<label style='font-size:13px;font-weight:bold;color:#4C4C4C;font-family:Arial,Verdana,sans-serif;'>Collection Items</label>");
		label.setHeight("100%");
		label.setWidth("100%");
		toolstrip.addMember(label);
		
		buttons = new HLayout();
		buttons.setHeight(16);
		buttons.setMargin(2);
		buttons.setMembersMargin(3);
		buttons.hide();
		toolstrip.addMember(buttons);
		
		ImgButton addButton = new ImgButton();
		addButton.setWidth(15);
		addButton.setHeight(15);
		addButton.setPrompt("Add");
		addButton.setSrc("/theme/images/icons16/add.png");
		addButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				addItemWindow.show();
			}
		});
		buttons.addMember(addButton);
				
		delButton = new ImgButton();
		delButton.setWidth(15);
		delButton.setHeight(15);
		delButton.setPrompt("Remove");
		delButton.setSrc("/theme/images/icons16/delete.png");
		delButton.hide();
		delButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Record rec = new Record();
				rec.setAttribute("id", grid.getSelectedRecord().getAttribute("id"));
				collectionTreeDS.removeData(rec, new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						grid.removeSelectedData();
					}						
				});
			}
		});
		buttons.addMember(delButton);
				
		grid = new ListGrid();
		grid.setWidth(width);
		grid.setHeight(40);
		grid.setAutoFitData(Autofit.VERTICAL);
		grid.setAutoFitMaxHeight(200);
		grid.setShowHeader(false);
		grid.setWrapCells(true);
		grid.setFixedRecordHeights(false);
		ListGridField nameField = new ListGridField("name", "Name");
		nameField.setWrap(true);
		grid.setFields(nameField);
		grid.addCellClickHandler(new CellClickHandler() {
			@Override
			public void onCellClick(CellClickEvent event) {
				if(settings.getUser().isAdmin() || settings.getUser().isArchiveManager()) 
					delButton.show();
			}			
		});
		addMember(grid);
		
		addItemWindow = new AddItemWindow();
	}
	public void canEdit(boolean editor) {
		if(editor) buttons.show();
		else buttons.hide();
	}
	public void setData(Record[] records) {
		grid.setData(records);
	}
	public void select(Record record) {
		this.accession = record;
		try {
			Record[] notes = new Record[0];
			Record[] source_associations = record.getAttributeAsRecordArray("source_associations");
			if(source_associations != null) {				
				for(Record source_assoc : source_associations) {					
					Record notesRecord = source_assoc.getAttributeAsRecord("items");
					if(notesRecord != null) {
						notes = source_assoc.getAttributeAsRecordArray("items");
					}
				}
				setData(notes);
			}
		} catch(ClassCastException e) {
			setData(new Record[0]);
		}
	}
	public class AddItemWindow extends Window {
		private TreeGrid tree;
		private IButton linkItem;
		
		public AddItemWindow() {
			setWidth(480);
			setHeight(400);
			setTitle("Add Collection Item");
			setAutoCenter(true);
			setIsModal(true);
			/*
			DynamicForm searchForm = new DynamicForm();
			searchForm.setWidth("100%");
			searchForm.setHeight(25);
			searchForm.setMargin(2);
			searchForm.setNumCols(3);
			searchForm.setCellPadding(2);
			final TextItem searchField = new TextItem("query");
			searchField.setShowTitle(false);
			searchField.setWidth(410);
						
			ButtonItem searchButton = new ButtonItem("search", "search");
			searchButton.setWidth(50);
			searchButton.setStartRow(false);
			searchButton.setEndRow(false);
			searchButton.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					Criteria criteria = new Criteria();
					criteria.setAttribute("qname", "openapps_org_repository_1_0_collection");
					criteria.setAttribute("field", "openapps_org_system_1_0_name");
					criteria.setAttribute("sort", "openapps_org_system_1_0_name");
					if(searchField.getValue() != null) {
						criteria.setAttribute("query", searchField.getValueAsString());
					} else {
						criteria.setAttribute("_startRow", "0");
						criteria.setAttribute("_endRow", "75");
					}
					tree.fetchData(criteria);
				}
			});
			searchForm.setFields(searchField,searchButton);
			addItem(searchForm);
			*/
			tree = new TreeGrid();
			tree.setMargin(2);
			tree.setDataSource(collectionTreeDS);
			tree.setShowHeader(false);
			tree.addClickHandler(new ClickHandler() {
				public void onClick(ClickEvent event) {
					String localName = tree.getSelectedRecord().getAttribute("localName");
					if(localName.equals("collection") || localName.equals("category"))
						linkItem.hide();
					else
						linkItem.show();
				}
			});
			addItem(tree);
						
			HLayout layout = new HLayout();
			layout.setMargin(5);
			layout.setHeight(30);
			layout.setWidth100();
			layout.setAlign(Alignment.RIGHT);
			linkItem = new IButton("Link");
			linkItem.setWidth(65);
			linkItem.hide();
			linkItem.setIcon("/theme/images/icons16/link.png");
			linkItem.addClickHandler(new ClickHandler() {
				public void onClick(ClickEvent event) {
					Record record = new Record();
					record.setAttribute("assoc_qname", "openapps_org_repository_1_0_items");
					record.setAttribute("source", accession.getAttribute("id"));
					record.setAttribute("target", tree.getSelectedRecord().getAttribute("id"));
					EntityServiceDS.getInstance().addAssociation(record, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							if(response.getData() != null && response.getData().length > 0) {
								select(response.getData()[0]);
							}
						}
					});
					addItemWindow.hide();
				}
			});
			layout.addMember(linkItem);
			addItem(layout);
		}
		@Override
		public void show() {
			if(accession != null) {
				if(collectionId == null) collectionId = accession.getAttribute("collection");
				if(collectionId != null) {
					Criteria criteria = new Criteria();
					criteria.setAttribute("collection", collectionId);				
					tree.fetchData(criteria);
				}
			}
			super.show();
		}
	}
	
	public class CollectionTreeDS extends RestDataSource {
		public CollectionTreeDS() {
			setDataFormat(DSDataFormat.JSON);
			setID(id);  
	        setTitleField("name");	        
	        DataSourceTextField nameField = new DataSourceTextField("name", "Name");     
	        
	        DataSourceTextField idField = new DataSourceTextField("id", "ID",50);  
	        idField.setPrimaryKey(true);  
	        idField.setRequired(true);
	        
	        DataSourceTextField parentField = new DataSourceTextField("parent", "Parent");  
	        parentField.setRequired(true);  
	        parentField.setForeignKey(id + ".id");  
	        parentField.setRootValue("null");  
	        
	        setFields(idField,nameField,parentField);
			
	        String ticket = settings.getTicket();
			if(ticket != null) {
		        setFetchDataURL(settings.getServiceUrl() + "service/archivemanager/collection/items.json?api_key="+ticket);
				//setAddDataURL(security.getServiceUrl() + "service/entity/create.json");
				setAddDataURL(settings.getServiceUrl() + "service/entity/association/add.json?api_key="+ticket);
				setRemoveDataURL(settings.getServiceUrl() + "service/entity/association/remove.json?api_key="+ticket);
			}
		}
		@Override
		protected void transformResponse(DSResponse response, DSRequest request,Object data) {
			try {
				if(response.getData() != null && response.getData().length > 0) {
					for(int i=0; i < response.getData().length; i++) {
						Record entity = response.getData()[i];
						if(entity != null) {
							String parent = entity.getAttribute("parent");
							String type = entity.getAttribute("localName");
							if(parent != null && type.equals("category")) {
								String parentLocalName = entity.getAttribute("parentLocalName");
								if(parentLocalName != null && parentLocalName.equals("collection"))
									entity.setAttribute("parent", parent+"-categories");						
							} else if(type != null && !type.equals("accessions") && !type.equals("categories") && !type.equals("collection")) {
								entity.setAttribute("isFolder", false);
								entity.setAttribute("icon", "/theme/images/tree_icons/"+type+".png");
							}
						}
					}
				}
			} catch(ClassCastException e) {
					
			}
			super.transformResponse(response, request, data);
		}
	}

	public void setCollectionId(String collectionId) {
		this.collectionId = collectionId;
	}
	
}
