package org.archivemanager.collections.client.accessions;

import org.archivemanager.collections.client.ArchiveManager;
import org.archivemanager.collections.client.accessions.DetailPanel;
import org.archivemanager.gwt.client.NativeRepositoryModel;
import org.heed.openapps.gwt.client.component.NotesComponent;
import org.heed.openapps.gwt.client.data.EntityServiceDS;
import org.heed.openapps.gwt.client.event.SaveListener;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;


public class AccessionView extends VLayout {
	private ArchiveManager settings = new ArchiveManager();
	private NativeRepositoryModel repositoryModel = new NativeRepositoryModel();
	
	private DetailPanel detailPanel;
	private AccessionGrid grid;
	private AccessionCollectionItemsComponent collectionItemsComponent;
	private NotesComponent notesComponent;
	private AccessionContentComponent contentComponent;
	private TabSet tabs;
	
	
	public AccessionView(boolean edit) {
		setWidth100();
		setHeight100();
		
		grid = new AccessionGrid();
		grid.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Record record = grid.getSelectedRecord();
				if(record != null) {
					Criteria criteria = new Criteria();
					String target_id = record.getAttribute("target_id");
					if(target_id == null) target_id = record.getAttribute("id");
	            	criteria.setAttribute("id", target_id);
	            	criteria.setAttribute("sources", true);
	            	criteria.setAttribute("targets", true);
	            	EntityServiceDS.getInstance().getEntity(criteria, new DSCallback() {
	            		public void execute(DSResponse dsResponse, Object data, DSRequest dsRequest) {
	            			if(dsResponse.getData() != null && dsResponse.getData().length > 0) {
	            				Record record = dsResponse.getData()[0];
	            				tabs.show();
	        					detailPanel.select(record);
	        					notesComponent.select(record);
	        					collectionItemsComponent.select(record);
	        					contentComponent.select(record);
	        					if(settings.getUser().isArchiveManager()) {
	        						notesComponent.canEdit(true);
	        						collectionItemsComponent.canEdit(true);
		        					contentComponent.canEdit(true);
	        					}
	            			}
	            		}			
	            	});
				}
			}			
		});
		grid.setHeight("250px");
		addMember(grid);
		
		tabs = new TabSet();
		tabs.setHeight("100%");
		addMember(tabs);
		
		detailPanel = new DetailPanel();
        Tab detailsTab = new Tab("Details");
        detailsTab.setPane(detailPanel);
		tabs.addTab(detailsTab);
		detailPanel.setSaveListener(new SaveListener() {
			@Override
			public void save(Record record) {
				EntityServiceDS.getInstance().updateEntity(record, new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						Record[] resultData = response.getData();
						if(resultData != null && resultData.length > 0 && resultData[0].getAttribute("id") != null) {
							grid.getSelectedRecord().setAttribute("name", resultData[0].getAttribute("name"));
							grid.getSelectedRecord().setAttribute("general_note", resultData[0].getAttribute("general_note"));
							grid.markForRedraw();
							detailPanel.setMessage("accession saved successfully");
						} else {
							detailPanel.setMessage("there was a problem saving the accession");
						}
					}
				});
			}			
		});
								
		HLayout namesNotesLayout = new HLayout();
		Tab namesNotesTab = new Tab("Notes & Contents");
		namesNotesTab.setPane(namesNotesLayout);
		tabs.addTab(namesNotesTab);
				
		VLayout leftLayout = new VLayout();
		leftLayout.setWidth100();
		leftLayout.setMembersMargin(10);
		namesNotesLayout.addMember(leftLayout);
		
		notesComponent = new NotesComponent("accessionPanelNotes", "100%", repositoryModel.getCollectionNoteTypes(), true);
		notesComponent.setMargin(5);
		leftLayout.addMember(notesComponent);
		
		collectionItemsComponent = new AccessionCollectionItemsComponent("100%");
		collectionItemsComponent.setMargin(5);
		leftLayout.addMember(collectionItemsComponent);
		
		Canvas canvas = new Canvas();
		canvas.setHeight100();
		leftLayout.addMember(canvas);
		
		VLayout rightLayout = new VLayout();
		rightLayout.setWidth("300px");
		rightLayout.setMembersMargin(10);
		namesNotesLayout.addMember(rightLayout);
		
		contentComponent = new AccessionContentComponent();
		contentComponent.setMargin(5);
		rightLayout.addMember(contentComponent);
		
		canvas = new Canvas();
		canvas.setHeight100();
		rightLayout.addMember(canvas);
		
		/*
		LinkedHashMap<String,String> valueMap4 = new LinkedHashMap<String,String>();
		valueMap4.put("acknowledgement_sent","Acknowledgement Sent");
		valueMap4.put("arranged_sorted","Arranged and Sorted");
		valueMap4.put("listed","Listed");
		valueMap4.put("processing_beginning","Processing Beginning");
		valueMap4.put("processing_ending","Processing Ending");
		valueMap4.put("rights_transferred","Rights Transferred");
		valueMap4.put("shelved","Shelved");
		activitiesComponent = new ActivitiesComponent("accessionActivity", "100%", valueMap4);
		activitiesComponent.setMargin(5);		
		layout2.addMember(activitiesComponent);
		*/
		
	}
	public void select(Record collection) {
		grid.select(collection);	
		collectionItemsComponent.setCollectionId(collection.getAttribute("id"));
	}
}
