package org.archivemanager.collections.client.donors;

import org.archivemanager.collections.client.ArchiveManager;
import org.archivemanager.collections.client.donors.form.IndividualForm;
import org.archivemanager.collections.client.donors.form.OrganizationForm;
import org.heed.openapps.gwt.client.data.EntityServiceDS;

import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class DetailPanel extends VLayout {
	private ArchiveManager settings = new ArchiveManager();
	private IndividualForm individualForm;
	private OrganizationForm organizationForm;
	private HLayout buttonLayout;
	
	private Label message;
	
	
	public DetailPanel() {
		setWidth100();
		setHeight100();
		
		Canvas formCanvas = new Canvas();
		formCanvas.setWidth100();
		formCanvas.setHeight100();
		addMember(formCanvas);
		
		individualForm = new IndividualForm();
		formCanvas.addChild(individualForm);
		
		organizationForm = new OrganizationForm();
		organizationForm.hide();
		formCanvas.addChild(organizationForm);
		
		formCanvas.addChild(organizationForm);
		
		buttonLayout = new HLayout();
		buttonLayout.setWidth100();
		buttonLayout.setHeight(25);
		buttonLayout.setMargin(5);
		buttonLayout.setMembersMargin(17);
		buttonLayout.hide();
		addMember(buttonLayout);
		
		IButton saveButton = new IButton("Save");
		saveButton.setHeight(25);
		saveButton.setWidth(75);
		saveButton.setIcon("/theme/images/icons16/save.png");
		saveButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Record record = new Record();
				if(individualForm.isVisible()) {
					for(Object key : individualForm.getValues().keySet()) {
						record.setAttribute((String)key, individualForm.getValues().get(key));
					}					
				} else if(organizationForm.isVisible()) {
					for(Object key : organizationForm.getValues().keySet()) {
						record.setAttribute((String)key, organizationForm.getValues().get(key));
					}
				}
				EntityServiceDS.getInstance().updateEntity(record, new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						Record[] resultData = response.getData();
						if(resultData != null && resultData.length > 0 && resultData[0].getAttribute("id") != null) {
							message.setContents("donor saved successfully");
						} else {
							message.setContents("there was a problem saving the donor");
						}
					}
				});
			}
		});
		buttonLayout.addMember(saveButton);
		
		message = new Label();
		message.setHeight(25);
		message.setWidth100();
		buttonLayout.addMember(message);
	}
	public void select(Record record) {
		String qname = record.getAttribute("qname");
    	if(qname != null) {    		
    		if(qname.equals("openapps_org_contact_1_0_individual")) {
    	   		individualForm.editRecord(record);
    	   		organizationForm.hide();
    	   		individualForm.show();
    		} else if(qname.equals("openapps_org_contact_1_0_organization")) {
    			organizationForm.editRecord(record);
    			individualForm.hide();
    	   		organizationForm.show();
    		}
    		if(settings.getUser().isArchiveManager()) {
    			buttonLayout.show();
    		}
    	}
	}
	public IndividualForm getIndividualForm() {
		return individualForm;
	}
	public OrganizationForm getOrganizationForm() {
		return organizationForm;
	}
}
