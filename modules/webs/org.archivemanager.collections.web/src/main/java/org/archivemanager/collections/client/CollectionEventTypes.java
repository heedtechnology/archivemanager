package org.archivemanager.collections.client;

import org.heed.openapps.gwt.client.EventTypes;

public class CollectionEventTypes extends EventTypes {

	public static final int ADD_COLLECTION = 100;
	public static final int ADD_ACCESSION = 101;
	public static final int ADD_COLLECTION_OBJECT = 102;
	
	public static final int PROPOGATE_PROPERTY = 150;
}
