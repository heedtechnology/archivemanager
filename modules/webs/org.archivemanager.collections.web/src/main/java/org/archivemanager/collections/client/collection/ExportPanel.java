package org.archivemanager.collections.client.collection;

import org.archivemanager.gwt.client.data.AMServiceDS;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.ContentsType;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.HTMLPane;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class ExportPanel extends HLayout {
	private ReportMenu menu;
	private HTMLPane iframeCanvas;
	private ProgressWindow progress;
	
	private String report;
	private Record collection;
	
	public ExportPanel() {
		setWidth100();
		setHeight100();
		setMembersMargin(5);
		
		VLayout leftLayout = new VLayout();
		leftLayout.setWidth(250);
		leftLayout.setHeight100();
		leftLayout.setMembersMargin(5);
		addMember(leftLayout);
			
		HLayout buttonLayout = new HLayout();
		buttonLayout.setWidth100();
		buttonLayout.setHeight(30);
		//buttonLayout.setMargin(5);
		buttonLayout.setMembersMargin(5);
		buttonLayout.setPadding(2);
		buttonLayout.setBorder("1px solid #BFBFBF;");
		leftLayout.addMember(buttonLayout);
				
		Button generateButton = new Button("Generate");
		generateButton.setPrompt("Generate");
		generateButton.setWidth(60);
		generateButton.setHeight(24);
		generateButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if(report.equals("finding_aid")) {
					String format = "pdf";
					final String target_id = collection.getAttribute("id");
					Criteria criteria = new Criteria();
					criteria.setAttribute("id", target_id);
		    		criteria.setAttribute("format", format);
		    		criteria.setAttribute("title", "finding_aid_"+target_id);
		    		criteria.setAttribute("schedule", "false");		    		
		    		progress.show();		    		
		    		AMServiceDS.getInstance().generateFindingAid(criteria, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							progress.hide();	
						}
		    		});
				}
			}			
		});
		buttonLayout.addMember(generateButton);
		
		Button viewButton = new Button("View");
		viewButton.setPrompt("View");
		viewButton.setWidth(60);
		viewButton.setHeight(24);
		viewButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				final String target_id = collection.getAttribute("id");
				iframeCanvas.setContentsURL("/reports/finding_aid_"+target_id+".pdf");
			}			
		});
		buttonLayout.addMember(viewButton);
		
		menu = new ReportMenu();
		leftLayout.addMember(menu);
		
		progress = new ProgressWindow();		
		
		iframeCanvas = new HTMLPane();
        iframeCanvas.setWidth100();
        iframeCanvas.setHeight(500);
        //iframeCanvas.setMargin(5);
        iframeCanvas.setContentsType(ContentsType.PAGE);
        iframeCanvas.setShowEdges(false);
        iframeCanvas.setContentsURL("/theme/images/logo/ArchiveManager450_495.png");
        addMember(iframeCanvas);
        
	}
	public void select(Record collection) {
		this.collection = collection;
		iframeCanvas.setContentsURL("/theme/images/logo/ArchiveManager450_495.png");
		menu.refresh();
	}
	
	public class ReportMenu extends VLayout {		
		public ReportMenu() {
			setWidth100();
			setHeight100();
			setMembersMargin(2);
			setBorder("1px solid #BFBFBF;");
			
			Label label = new Label("<label style='font-weight:bold;font-size:16px;padding-left:5px;'>Collection Reports</label>");
			label.setWidth100();
			label.setHeight(30);			
			addMember(label);
			
			addButton("finding_aid", "Finding Aid");
		}

		protected void addButton(final String id, String title) {
			final Label label = new Label("<label style='font-size:12px;padding-left:25px;'>"+title+"</label>");
			label.setWidth100();
			label.setHeight(25);
			label.setPadding(1);
			label.setMargin(2);
			label.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					report = id;
					for(Canvas member : getMembers()) {
						member.setBorder("0px");
					}
					label.setBorder("1px solid #BFBFBF;");
				}				
			});
			addMember(label);
		}
		public void refresh() {
			for(Canvas member : getMembers()) {
				member.setBorder("0px");
			}			
		}
	}
	
	public class ProgressWindow extends Window {
		
		public ProgressWindow() {
			setWidth(225);
			setHeight(80);
			setTitle("");
			setAutoCenter(true);
			setIsModal(true);
			setShowCloseButton(false);
			setShowMinimizeButton(false);
			
			Label label = new Label("<label style='font-size:16px;font-weight:bold;padding:15px;'>Generating Report...</label>");
			label.setHeight(40);
			label.setWidth100();
			label.setMargin(5);
			addItem(label);
		}
	}
}
