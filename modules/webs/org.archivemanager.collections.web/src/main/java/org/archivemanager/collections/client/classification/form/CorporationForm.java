package org.archivemanager.collections.client.classification.form;

import java.util.ArrayList;
import java.util.List;

import org.archivemanager.gwt.client.NativeClassificationModel;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.layout.VLayout;

public class CorporationForm extends VLayout {
	private NativeClassificationModel model = new NativeClassificationModel();
	
	private static final int fieldWidth = 300;
	
	private DynamicForm form;
	
	public CorporationForm() {
		setWidth100();
		
		form = new DynamicForm();
		form.setHeight100();
		form.setNumCols(2);
		form.setCellPadding(5);
		form.setColWidths("110","*");
		addMember(form);
		
		List<FormItem> fields = new ArrayList<FormItem>();
		
		StaticTextItem idItem = new StaticTextItem("id", "ID");
		idItem.setColSpan(2);
		idItem.setWidth(fieldWidth);
		fields.add(idItem);
		
		TextItem nameItem = new TextItem("name", "Name");
		nameItem.setWidth(fieldWidth);		
		fields.add(nameItem);
		
		TextItem primaryNameItem = new TextItem("primary_name", "Primary Name");
		primaryNameItem.setWidth(fieldWidth);
		fields.add(primaryNameItem);
		
		TextItem secondaryNameItem = new TextItem("secondary_name", "Secondary Name");
		secondaryNameItem.setWidth(fieldWidth);
		fields.add(secondaryNameItem);
		
		TextItem fullerItem = new TextItem("fuller_form_name", "Fuller Form");
		fullerItem.setWidth(fieldWidth);
		fields.add(fullerItem);
		
		TextItem datesItem = new TextItem("dates", "Dates");
		datesItem.setWidth(fieldWidth);
		fields.add(datesItem);
		
		TextItem urlItem = new TextItem("url", "URL");
		urlItem.setWidth(fieldWidth);
		fields.add(urlItem);
		
		SelectItem sourceItem = new SelectItem("source", "Source");
		sourceItem.setValueMap(model.getSubjectSource());
		sourceItem.setWidth(fieldWidth);
		fields.add(sourceItem);
		
		SelectItem ruleItem = new SelectItem("rule", "Rule");
		ruleItem.setValueMap(model.getNamedEntityRule());
		ruleItem.setWidth(fieldWidth-50);
		fields.add(ruleItem);
		
		FormItem[] itemArray = fields.toArray(new FormItem[fields.size()]);		
		form.setFields(itemArray);
	}
	public void editRecord(Record record) {
		form.editRecord(record);
	}
	public Record getValuesAsRecord() {
		return form.getValuesAsRecord();
	}
}
