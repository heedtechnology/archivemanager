package org.archivemanager.collections.client;

import com.smartgwt.client.data.Criteria;


public interface Searchable {

	void search(Criteria criteria);
	
}
