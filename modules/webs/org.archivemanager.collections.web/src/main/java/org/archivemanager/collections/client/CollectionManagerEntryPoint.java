package org.archivemanager.collections.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.DOM;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.rpc.HandleErrorCallback;
import com.smartgwt.client.rpc.HandleTransportErrorCallback;
import com.smartgwt.client.rpc.RPCManager;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.Positioning;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;


public class CollectionManagerEntryPoint implements EntryPoint {
	private ArchiveManager settings = new ArchiveManager();
		
	private CollectionApplication collectionApplication;
		
	private int tab = -1;
	private Record selection;
	
	
	public void onModuleLoad() {
		Canvas mainLayout = new Canvas();
		mainLayout.setHeight(settings.getHeight());  
		mainLayout.setWidth(settings.getWidth());
		//mainLayout.setMargin(1);
		mainLayout.setOverflow(Overflow.HIDDEN);
		//mainLayout.setBorder("1px solid #C0C3C7");
		
        //LoadingScreen loader = new LoadingScreen("Loading....");
        //addMember(loader);
                
        String entityId = getEntityId();
        if(entityId != null) collectionApplication = new CollectionApplication(entityId);
        else collectionApplication = new CollectionApplication();
        mainLayout.addChild(collectionApplication);
        
        RPCManager.setHandleErrorCallback(new HandleErrorCallback() {
			@Override
			public void handleError(DSResponse response, DSRequest request) {
				int httpCode = response.getHttpResponseCode();
				if(httpCode == 404) SC.warn("Error contacting the server, please check your internet connection and try again.");									
				else SC.warn("Server Side Error, please contact your administrator.");
			}
		});
        RPCManager.setHandleTransportErrorCallback(new HandleTransportErrorCallback() {
			@Override
			public void handleTransportError(int transactionNum, int status, int httpResponseCode, String httpResponseText) {
				if(httpResponseCode == 404) SC.warn("Error contacting the server, please check your internet connection and try again.");
				SC.warn("Server Side Error, please contact your administrator.");
			}        	
        });
        mainLayout.setHtmlElement(DOM.getElementById("gwt"));
        mainLayout.setPosition(Positioning.RELATIVE);
        mainLayout.draw();
        
        final Element elem = Document.get().getElementById("loadingWrapper");
        if(elem != null) {
            elem.removeFromParent();            
        }
	}
	
	public String getEntityId() { 
		return getNativeEntityId();
	}	
	private final native String getNativeEntityId() /*-{
        return $wnd.entityId;
	}-*/;
	
}
