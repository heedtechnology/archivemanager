package org.archivemanager.collections.client.data;

import java.util.LinkedHashMap;

import com.google.gwt.core.client.JavaScriptObject;
import com.smartgwt.client.util.JSOHelper;

public class NativeCollectionModel {

	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getItemLevels() { 
		JavaScriptObject obj = getNativeItemLevels();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>();
	}	
	private final native JavaScriptObject getNativeItemLevels() /*-{
        return $wnd.item_levels;
	}-*/;
	
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getExtentUnits() { 
		JavaScriptObject obj = getNativeExtentUnits();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>();
	}	
	private final native JavaScriptObject getNativeExtentUnits() /*-{
        return $wnd.extent_units;
	}-*/;
	
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getLanguageCodes() { 
		JavaScriptObject obj = getNativeLanguageCodes();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>();
	}	
	private final native JavaScriptObject getNativeLanguageCodes() /*-{
        return $wnd.language_codes;
	}-*/;
	
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getContainerTypes() { 
		JavaScriptObject obj = getNativeContainerTypes();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>();
	}	
	private final native JavaScriptObject getNativeContainerTypes() /*-{
        return $wnd.container_types;
	}-*/;
	
}
