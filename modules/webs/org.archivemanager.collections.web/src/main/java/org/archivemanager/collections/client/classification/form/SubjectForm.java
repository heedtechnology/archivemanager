package org.archivemanager.collections.client.classification.form;

import java.util.ArrayList;
import java.util.List;

import org.archivemanager.gwt.client.NativeClassificationModel;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.layout.VLayout;

public class SubjectForm extends VLayout {
	private NativeClassificationModel model = new NativeClassificationModel();
	
	private static final int fieldWidth = 300;
	
	private DynamicForm form;
	
	
	public SubjectForm() {
		setWidth100();
		setHeight100();
		
		form = new DynamicForm();
		form.setNumCols(2);
		form.setCellPadding(5);
		form.setColWidths("110","*");
		form.setHeight100();
		addMember(form);
		
		List<FormItem> fields = new ArrayList<FormItem>();
		
		StaticTextItem idItem = new StaticTextItem("id", "ID");
		idItem.setColSpan(2);
		idItem.setWidth(fieldWidth);
		fields.add(idItem);
		
		TextItem nameItem = new TextItem("name", "Term");
		nameItem.setWidth(fieldWidth);		
		fields.add(nameItem);
		
		TextAreaItem descItem = new TextAreaItem("description", "Description");
		descItem.setWidth(fieldWidth);
		fields.add(descItem);
		
		TextItem urlItem = new TextItem("url", "URL");
		urlItem.setWidth(fieldWidth);
		fields.add(urlItem);
		
		SelectItem sourceItem = new SelectItem("source", "Source");
		sourceItem.setValueMap(model.getSubjectSource());
		sourceItem.setWidth(fieldWidth);
		fields.add(sourceItem);
		
		SelectItem typeItem = new SelectItem("type", "Type");
		typeItem.setValueMap(model.getSubjectType());
		typeItem.setWidth(fieldWidth/2);
		fields.add(typeItem);
		
		FormItem[] itemArray = fields.toArray(new FormItem[fields.size()]);		
		form.setFields(itemArray);
	}
	public void editRecord(Record record) {
		form.editRecord(record);
	}
	public Record getValuesAsRecord() {
		return form.getValuesAsRecord();
	}
}
