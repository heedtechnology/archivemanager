package org.archivemanager.collections.client.donors.form;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;

public class NotesForm extends DynamicForm {
	private static final int FIELD_WIDTH = 325;
	
	private TextAreaItem biographyField;
	private TextItem bioField;
	
	public NotesForm() {
		setWidth100();
		setNumCols(2);
		setCellPadding(3);
				
		TextAreaItem descField = new TextAreaItem("description", "Description");
		descField.setHeight(85);
		descField.setWidth(FIELD_WIDTH);
		
		TextAreaItem noteField = new TextAreaItem("note", "General Note");
		noteField.setHeight(85);
		noteField.setWidth(FIELD_WIDTH);
		
		biographyField = new TextAreaItem("biography", "Biography");
		biographyField.setHeight(85);
		biographyField.setWidth(FIELD_WIDTH);
		
		bioField = new TextItem("bio_sources", "Bio Sources");
		bioField.setWidth(FIELD_WIDTH);
		
		setFields(descField, noteField, biographyField, bioField);
	}
	
	@Override
	public void editRecord(Record record) {
		String qname = record.getAttribute("qname");
		if(qname != null) {
			if(qname.equals("openapps_org_contact_1_0_organization")) {
				biographyField.hide();
				bioField.hide();
			} else {
				biographyField.show();
				bioField.show();
			}
		}
		super.editRecord(record);
	}
}
