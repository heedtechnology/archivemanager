package org.archivemanager.collections.client.components;

import org.heed.openapps.gwt.client.component.AddEntityAndAssociateWindow;
import org.heed.openapps.gwt.client.data.EntityServiceDS;

import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class SearchDefinitionComponent extends VLayout {
	private ListGrid grid;
	private AddEntityAndAssociateWindow addWindow;
	
	
	public SearchDefinitionComponent(String width) {
		setWidth(width);
		//setHeight100();
		//setMargin(10);
		//setBorder("1px solid #A7ABB4");
		
		HLayout toolstrip = new HLayout();
		toolstrip.setWidth("100%");
		toolstrip.setHeight(20);
		toolstrip.setBackgroundImage("[SKIN]/cssButton/button_stretch.png");
		toolstrip.setBorder("1px solid #A7ABB4");
		addMember(toolstrip);
		
		Label label = new Label("<label style='font-size:11px;font-weight:bold;color:#4C4C4C;font-family:Arial,Verdana,sans-serif;'>Search Definitions</label>");
		label.setHeight("100%");
		label.setWidth("100%");
		toolstrip.addMember(label);
		
		HLayout buttons = new HLayout();
		buttons.setHeight(16);
		buttons.setMargin(2);
		buttons.setMembersMargin(3);
		toolstrip.addMember(buttons);
		
		ImgButton addButton = new ImgButton();
		addButton.setWidth(16);
		addButton.setHeight(16);
		addButton.setSrc("/images/icons16/add.png");
		addButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				addWindow.show();
			}
		});
		buttons.addMember(addButton);
		ImgButton delButton = new ImgButton();
		delButton.setWidth(16);
		delButton.setHeight(16);
		delButton.setSrc("/images/icons16/delete.png");
		delButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Record rec = new Record();
				String id = grid.getSelectedRecord().getAttribute("target_id");
				if(id == null) id = grid.getSelectedRecord().getAttribute("id");
				rec.setAttribute("id", id);
				EntityServiceDS.getInstance().removeEntity(rec, new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						grid.removeSelectedData();	
					}						
				});	
			}
		});
		buttons.addMember(delButton);
		
		grid = new ListGrid();
		grid.setWidth("100%");
		grid.setHeight(40);
		grid.setAutoFitData(Autofit.VERTICAL);
		grid.setAutoFitMaxHeight(200);
		//grid.setBorder("0px");
		grid.setWrapCells(true);
		grid.setFixedRecordHeights(false);
		grid.setShowHeader(false);
		//ListGridField typeField = new ListGridField("type");
		ListGridField nameField = new ListGridField("name");
		ListGridField valueField = new ListGridField("value");
		grid.setFields(nameField, valueField);
		grid.addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				//select(event.getRecord());
			}
		});
        addMember(grid);
        
        final DynamicForm addForm = new DynamicForm();
        addForm.setWidth(450);
        addForm.setHeight(150);
        addForm.setMargin(10);
        /*
        SelectItem typeFormField = new SelectItem("type", "Type");
        typeFormField.setWidth(150);
		LinkedHashMap<String,String> valueMap1 = new LinkedHashMap<String,String>();
		valueMap1.put("term","Formula");
		typeFormField.setValueMap(valueMap1);
        */
        TextItem termField = new TextItem("name", "Search Term");
        termField.setWidth(300);
        TextAreaItem descrField = new TextAreaItem("value", "Formula");
        descrField.setHeight(90);
        descrField.setWidth(300);
        addForm.setFields(termField, descrField);
        
        addWindow = new AddEntityAndAssociateWindow("Add Search Definition", "openapps_org_search_1_0_definitions", "openapps_org_search_1_0_definition", addForm, new DSCallback() {
        	public void execute(DSResponse response, Object rawData, DSRequest request) {
        		if(response.getData() != null && response.getData().length > 0) {
					grid.addData(response.getData()[0]);
					addWindow.hide();
				}
			}        	
        });
	}
	public void setData(Record[] records) {
		grid.setData(records);
	}
	public void select(Record record) {
		addWindow.select(record);
		try {
			Record source_associations = record.getAttributeAsRecord("source_associations");
			if(source_associations != null) {
				Record notes = source_associations.getAttributeAsRecord("definitions");
				if(notes != null) {
					setData(notes.getAttributeAsRecordArray("node"));
				} else setData(new Record[0]);
			}
		} catch(ClassCastException e) {
			setData(new Record[0]);
		}
		try {
			Record target_associations = record.getAttributeAsRecord("target_associations");
			if(target_associations != null) {
				
			}
		} catch(ClassCastException e) {
			
		}
	}
}
