package org.archivemanager.collections.client;

import org.archivemanager.collections.client.collection.AddCollectionObjectWindow;
import org.archivemanager.collections.client.collection.AddCollectionWindow;
import org.archivemanager.collections.client.collection.CollectionView;
import org.archivemanager.collections.client.collection.CollectionTree;
import org.heed.openapps.gwt.client.data.EntityServiceDS;
import org.heed.openapps.gwt.client.event.AddListener;
import org.heed.openapps.gwt.client.event.ReloadListener;
import org.heed.openapps.gwt.client.event.RemoveListener;
import org.heed.openapps.gwt.client.event.SaveListener;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class CollectionApplication extends HLayout {
	private ArchiveManager settings = new ArchiveManager();	
	private HLayout toolbar;
	private CollectionTree tree;
	private CollectionView collectionView;
	private Canvas canvas;
	private Record selection;
	
	private ImgButton addObjButton;
	private Label addObjLabel;
	private ImgButton delButton;
	private Label removeLabel;
	
	private AddCollectionWindow addCollectionWindow;
	private AddCollectionObjectWindow addCollectionObjectWindow;
	
	
	public CollectionApplication() {
		this(null);
	}
	public CollectionApplication(String entityId) {
		setWidth100();
		setHeight100();
		setMembersMargin(1);
		
		VLayout leftLayout = new VLayout();
		leftLayout.setWidth(280);
		leftLayout.setHeight100();
		leftLayout.setMembersMargin(1);
		addMember(leftLayout);
		
		/** Search **/
		DynamicForm searchForm = new DynamicForm();
		searchForm.setHeight(26);
		searchForm.setMargin(0);
		searchForm.setNumCols(2);
		searchForm.setCellPadding(2);
		
		final TextItem searchField = new TextItem("query");
		searchField.setShowTitle(false);
		searchField.setWidth(225);
		searchField.addKeyPressHandler(new KeyPressHandler() {
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if(event.getKeyName().equals("Enter")) {
					Criteria record = new Criteria();
					if(searchField.getValueAsString() != null)
						record.setAttribute("query", searchField.getValueAsString());
					tree.fetchData(record);
				}				
			}
		});
		ButtonItem searchButton = new ButtonItem("search", "search");
		searchButton.setWidth(50);
		searchButton.setStartRow(false);
		searchButton.setEndRow(false);
		searchButton.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
				Criteria record = new Criteria();				
				if(searchField.getValueAsString() != null)
					record.setAttribute("query", searchField.getValueAsString());
				tree.fetchData(record);
			}
		});
		
		searchForm.setFields(searchField,searchButton);
		leftLayout.addMember(searchForm);
		
		/** Add/Delete Toolbar **/
		toolbar = new HLayout();
		toolbar.setWidth100();
		toolbar.setHeight(24);
		toolbar.setBorder("1px solid #C0C3C7");
		leftLayout.addMember(toolbar);
		
		ImgButton addButton = new ImgButton();
		addButton.setWidth(22);
		addButton.setHeight(22);
		addButton.setMargin(2);
		addButton.setSrc("/theme/images/icons16/add.png");
		addButton.setPrompt("Add Collection");
		if(!settings.getUser().isArchiveManager()) addButton.hide();
		addButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				addCollectionWindow.show();
			}
		});
		toolbar.addMember(addButton);
		
		Label addLabel = new Label("<span style='color:#C0C3C7;cursor:pointer;'>add collection</span>");
		addLabel.setWidth(90);
		if(!settings.getUser().isArchiveManager()) addLabel.hide();
		addLabel.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				addCollectionWindow.show();
			}

		});
		toolbar.addMember(addLabel);
		
		addObjButton = new ImgButton();
		addObjButton.setWidth(22);
		addObjButton.setHeight(22);
		addObjButton.setMargin(2);
		addObjButton.setSrc("/theme/images/icons16/add.png");
		addObjButton.setPrompt("Add Object");
		addObjButton.hide();
		addObjButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				addCollectionObjectWindow.show();
			}
		});
		toolbar.addMember(addObjButton);
		
		addObjLabel = new Label("<span style='color:#C0C3C7;cursor:pointer;'>add object</span>");
		addObjLabel.setWidth(70);
		addObjLabel.hide();
		addObjLabel.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if(selection != null) {
					addCollectionObjectWindow.show();
				}
			}

		});
		toolbar.addMember(addObjLabel);
		
		delButton = new ImgButton();
		delButton.setWidth(22);
		delButton.setHeight(22);
		delButton.setMargin(2);
		delButton.setPrompt("Delete");
		delButton.setSrc("/theme/images/icons16/delete.png");
		delButton.hide();
		delButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				SC.confirm("Do you really wish to remove this permanently?", new BooleanCallback() {
					public void execute(Boolean value) {
						if(value) tree.removeSelectedData();
					}			
				});
			}
		});
		toolbar.addMember(delButton);
		removeLabel = new Label("<span style='color:#C0C3C7;cursor:pointer;'>remove</span>");
		removeLabel.setWidth(50);
		removeLabel.hide();
		removeLabel.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				SC.confirm("Do you really wish to remove this permanently?", new BooleanCallback() {
					public void execute(Boolean value) {
						if(value) tree.removeSelectedData();
					}			
				});	
			}
		});
		toolbar.addMember(removeLabel);
		
		/** Collection Tree **/
		tree = new CollectionTree();
		tree.setWidth(280);
        tree.setHeight("100%");
        tree.addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				select(event.getRecord());
			}			
		});
        tree.setAddListener(new AddListener() {
        	@Override
			public void add(Record record) {
        		addCollectionObjectWindow.show();
			}        	
        });
        tree.setRemoveListener(new RemoveListener() {
			@Override
			public void remove(Record record) {
				SC.confirm("Do you really wish to remove this permanently?", new BooleanCallback() {
					public void execute(Boolean value) {
						if(value) tree.removeSelectedData();
					}			
				});
			}        	
        });
        leftLayout.addMember(tree);
        	
        canvas = new Canvas();
        canvas.setWidth100();
        canvas.setHeight100();
		addMember(canvas);
		
		collectionView = new CollectionView();
		collectionView.setSaveListener(new SaveListener() {
			@Override
			public void save(Record record) {
				String localName = record.getAttribute("localName");
				if(localName.equals("collection")) {
					String date = record.getAttribute("name");
					String title = tree.getSelectedRecord().getAttribute("name");
					if(title != null && !title.equals(date)) tree.getSelectedRecord().setAttribute("name", date);
					if(localName != null) tree.getSelectedRecord().setAttribute("localName", localName);
					tree.refreshFields();
				} else if(localName.equals("category")) {
					String date = record.getAttribute("name");
					String title = tree.getSelectedRecord().getAttribute("name");
					if(title != null && !title.equals(date)) tree.getSelectedRecord().setAttribute("name", date);
					tree.getSelectedRecord().setAttribute("localName", localName);
					tree.refreshFields();
				} else {
					String date = record.getAttribute("name");
					String title = tree.getSelectedRecord().getAttribute("name");
					//Boolean isFolder = record.getAttributeAsBoolean("isFolder");
					if(title != null && !title.equals(date)) tree.getSelectedRecord().setAttribute("name", date);
					if(localName != null) tree.getSelectedRecord().setAttribute("localName", localName);
					tree.refreshFields();
				}
			}			
		});
		collectionView.setReloadListener(new ReloadListener() {
			public void reload() {
				Criteria criteria = new Criteria();
				criteria.setAttribute("id", tree.getSelectedRecord().getAttribute("id"));
		    	criteria.setAttribute("targets", true);
		    	criteria.setAttribute("sources", true);
		    	EntityServiceDS.getInstance().getEntity(criteria, new DSCallback() {
					@Override
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						if(response.getData() != null && response.getData().length > 0) {					
							collectionView.reload(response.getData()[0]);
						}
					}
		    	});
			}			
		});
		canvas.addChild(collectionView);
		
		addCollectionWindow = new AddCollectionWindow();
		addCollectionWindow.setAddListener(new AddListener() {
			@Override
			public void add(Record record) {
				tree.addData(record, new DSCallback() {
					@Override
					public void execute(DSResponse dsResponse, Object data,	DSRequest dsRequest) {
						if(dsResponse.getData() != null && dsResponse.getData().length > 0) {
							if(searchField.getValueAsString() == null) {
								Criteria record = new Criteria();
								record.setAttribute("query", dsResponse.getData()[0].getAttribute("name"));
								search(record);
							}
						} 
					}						
				});
			}
		});
		addCollectionObjectWindow = new AddCollectionObjectWindow();
		addCollectionObjectWindow.setAddListener(new AddListener() {
			@Override
			public void add(Record record) {
				record.setAttribute("source", tree.getSelectedRecord().getAttribute("id"));
				tree.addData(record, new DSCallback() {
					@Override
					public void execute(DSResponse dsResponse, Object data,	DSRequest dsRequest) {
						if(dsResponse.getData() != null && dsResponse.getData().length > 0) {
							if(searchField.getValueAsString() == null) {
								Criteria record = new Criteria();
								record.setAttribute("query", dsResponse.getData()[0].getAttribute("name"));
								search(record);
							}
						}
					}						
				});	
			}			
		});		
		if(entityId != null) {
			Criteria criteria = new Criteria();
			criteria.setAttribute("entityId", entityId);
			search(criteria);
		}
	}
	
	public void select(Record record) {
		this.selection = record;			
		Criteria criteria = new Criteria();
    	criteria.setAttribute("id", record.getAttribute("id"));
    	criteria.setAttribute("targets", true);
    	criteria.setAttribute("sources", true);
    	EntityServiceDS.getInstance().getEntity(criteria, new DSCallback() {
			@Override
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				if(response.getData() != null && response.getData().length > 0) {					
					tree.select(response.getData()[0]);					
					collectionView.select(response.getData()[0]);
					String username = response.getData()[0].getAttribute("username");
					if(settings.getUser().isAdmin() || settings.getUser().isArchiveManager() || settings.getUser().getUsername().equals(username)) {
						addObjButton.show();
						addObjLabel.show();
						delButton.show();
						removeLabel.show();
					}
				}
			}
    	}); 
	}
	
	public void search(Criteria criteria) {
		criteria.setAttribute("qname", "openapps_org_repository_1_0_collection");
		tree.fetchData(criteria);
	}
	
}