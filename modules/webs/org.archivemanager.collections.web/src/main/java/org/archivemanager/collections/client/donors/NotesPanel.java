package org.archivemanager.collections.client.donors;

import org.archivemanager.collections.client.ArchiveManager;
import org.archivemanager.collections.client.donors.data.NativeContactModel;
import org.archivemanager.collections.client.donors.form.NotesForm;
import org.heed.openapps.gwt.client.component.NotesComponent;
import org.heed.openapps.gwt.client.data.EntityServiceDS;

import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class NotesPanel extends VLayout {
	private ArchiveManager settings = new ArchiveManager();
	private NativeContactModel model = new NativeContactModel();
	private NotesComponent notesComponent;
	private NotesForm notesForm;
	private Label message;
	private HLayout buttonLayout;
	
	public NotesPanel() {
		setWidth100();
		setHeight100();
		
		HLayout mainLayout = new HLayout();
		mainLayout.setWidth100();
		mainLayout.setHeight100();
		addMember(mainLayout);
		
		VLayout leftColumn = new VLayout();
		mainLayout.addMember(leftColumn);
		
		notesForm = new NotesForm();
		leftColumn.addMember(notesForm);
		
		buttonLayout = new HLayout();
		buttonLayout.setWidth100();
		buttonLayout.setHeight(25);
		buttonLayout.setMargin(5);
		buttonLayout.hide();
		buttonLayout.setMembersMargin(17);
		addMember(buttonLayout);
		
		IButton saveButton = new IButton("Save");
		saveButton.setHeight(25);
		saveButton.setWidth(75);
		saveButton.setIcon("/theme/images/icons16/save.png");
		saveButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Record record = notesForm.getValuesAsRecord();
				EntityServiceDS.getInstance().updateEntity(record, new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						Record[] resultData = response.getData();
						if(resultData != null && resultData.length > 0 && resultData[0].getAttribute("id") != null) {
							message.setContents("donor saved successfully");
						} else {
							message.setContents("there was a problem saving the donor");
						}
					}
				});
			}
		});
		buttonLayout.addMember(saveButton);
		
		message = new Label();
		message.setHeight(25);
		message.setWidth100();
		buttonLayout.addMember(message);
		
		VLayout rightColumn = new VLayout();
		mainLayout.addMember(rightColumn);
		
		notesComponent = new NotesComponent("donorNotes", "100%", model.getNoteTypes());
		notesComponent.setMargin(5);
		rightColumn.addMember(notesComponent);
	}
	
	public void select(Record record) {
		if(settings.getUser().isArchiveManager()) {
			buttonLayout.show();
			notesComponent.canEdit(true);
		}
		notesForm.editRecord(record);
		notesComponent.select(record);
	}
}
