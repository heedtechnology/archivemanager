package org.archivemanager.collections.client.donors;

import org.archivemanager.collections.client.donors.component.AddressComponent;
import org.archivemanager.collections.client.donors.component.EmailComponent;
import org.archivemanager.collections.client.donors.component.PhoneComponent;
import org.archivemanager.collections.client.donors.component.WebAddressComponent;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class ContactPanel extends VLayout {
	
	private AddressComponent addressComponent;
	private PhoneComponent phoneComponent;
	private EmailComponent emailComponent;
	private WebAddressComponent webAddressComponent;
	
	
	public ContactPanel() {
		setWidth100();
		setHeight100();
		
		HLayout topLayout = new HLayout();
		topLayout.setMargin(2);
		topLayout.setMembersMargin(2);
		addMember(topLayout);
		/*
		HLayout botLayout = new HLayout();
		botLayout.setMargin(2);
		botLayout.setBorder("1px solid #C0C3C7");
		addMember(botLayout);
		*/
		VLayout leftColumn = new VLayout();
		leftColumn.setHeight100();
		leftColumn.setWidth("100%");
		leftColumn.setMembersMargin(10);
		//leftColumn.setBorder("1px solid #C0C3C7");
		topLayout.addMember(leftColumn);
		
		VLayout midColumn = new VLayout();
		midColumn.setHeight100();
		midColumn.setWidth("100%");
		midColumn.setMembersMargin(10);
		//leftColumn.setBorder("1px solid #C0C3C7");
		topLayout.addMember(midColumn);
		
		VLayout rightColumn = new VLayout();
		rightColumn.setHeight100();
		rightColumn.setWidth("100%");
		rightColumn.setMembersMargin(10);
		//rightColumn.setBorder("1px solid #C0C3C7");
		topLayout.addMember(rightColumn);
				
		addressComponent = new AddressComponent("100%");
		addressComponent.setMargin(5);
		leftColumn.addMember(addressComponent);
		
		phoneComponent = new PhoneComponent("100%");
		phoneComponent.setMargin(5);
		midColumn.addMember(phoneComponent);
				
		emailComponent = new EmailComponent("100%");
		emailComponent.setMargin(5);
		rightColumn.addMember(emailComponent);
				
		webAddressComponent = new WebAddressComponent("100%");
		webAddressComponent.setMargin(5);
		midColumn.addMember(webAddressComponent);
		
		Canvas leftSpacer = new Canvas();
		leftSpacer.setHeight100();
		leftColumn.addMember(leftSpacer);
		
		Canvas midSpacer = new Canvas();
		midSpacer.setHeight100();
		midColumn.addMember(midSpacer);
		
		Canvas rightSpacer = new Canvas();
		rightSpacer.setHeight100();
		rightColumn.addMember(rightSpacer);
		
	}
	public void select(Record record) {
		addressComponent.select(record);
    	phoneComponent.select(record);
    	webAddressComponent.select(record);
    	emailComponent.select(record);
	}
}
