package org.archivemanager.collections.client.donors;

import org.archivemanager.gwt.client.MultiRelationshipGrid;
import org.archivemanager.gwt.client.component.FileRelationshipComponent;
import org.archivemanager.gwt.client.component.WebContentAssociationComponent;
import org.archivemanager.gwt.client.component.WebLinkAssociationComponent;
import org.archivemanager.gwt.client.component.events.EntitySelectionEvent;
import org.archivemanager.gwt.client.component.events.EntitySelectionHandler;
import org.heed.openapps.gwt.client.component.AdvancedContentViewer;
import org.heed.openapps.gwt.client.event.SelectionListener;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class DigitalObjectPanel extends VLayout {
	private FileRelationshipComponent fileComponent;
	private AdvancedContentViewer viewer;
	//private Record selection;
	private WebLinkAssociationComponent linkComponent;
	private WebContentAssociationComponent contentComponent;
	
	
	public DigitalObjectPanel() {
		setWidth100();
		setHeight100();
		setMembersMargin(10);
		
		HLayout topLayout = new HLayout();
		topLayout.setWidth100();
		topLayout.setMembersMargin(10);
		addMember(topLayout);
		
		MultiRelationshipGrid grid = new MultiRelationshipGrid();
		topLayout.addMember(grid);
		
		fileComponent = new FileRelationshipComponent("child", "Files", "280px", "150px", false, true, true);
		fileComponent.setSelectionListener(new SelectionListener() {
			@Override
			public void select(Record record) {
				viewer.select(record);
			}
		});
		grid.addMember("Files", fileComponent);
		
		contentComponent = new WebContentAssociationComponent("280px", "150px");
		contentComponent.addEntitySelectionHandler(new EntitySelectionHandler() {
			public void onEntitySelection(EntitySelectionEvent event) {
				viewer.select(event.getRecord());
			}
		});
		grid.addMember("Web Content", contentComponent);
		
		linkComponent = new WebLinkAssociationComponent("280px", "150px");
		linkComponent.addEntitySelectionHandler(new EntitySelectionHandler() {
			public void onEntitySelection(EntitySelectionEvent event) {
				viewer.select(event.getRecord());
			}
		});
		grid.addMember("Web Links", linkComponent);
		
		VLayout rightLayout = new VLayout();
		addMember(rightLayout);
		
		viewer = new AdvancedContentViewer("100%", "400px");
		rightLayout.addMember(viewer);
		
	}
	
	public void select(Record selection) {
		//this.selection = selection;
		fileComponent.select(selection);
		linkComponent.select(selection);
		contentComponent.select(selection);
		viewer.clearImage();
	}
}