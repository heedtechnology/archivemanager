package org.archivemanager.collections.client.locations;

import org.archivemanager.collections.client.ArchiveManager;
import org.archivemanager.collections.client.Searchable;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.layout.HLayout;

public class LocationToolbar extends HLayout {
	private ArchiveManager settings = new ArchiveManager();
	private TextItem searchField;
	private ImgButton addButton;
	private ImgButton deleteButton;
	private HLayout buttons;
	private Img progressBar;
	private Label progressLabel;
	
	private Img messageImg;
	private Label messageLabel;
	private ImgButton messageImgClose;
	
	private DSCallback deleteCallback;
	private DSCallback addCallback;
	
	public LocationToolbar(final Searchable searchable) {
		setWidth("100%");
		setHeight(25);
		setMembersMargin(5);
		
		DynamicForm searchForm = new DynamicForm();
		searchForm.setHeight(25);
		searchForm.setMargin(0);
		searchForm.setNumCols(4);
		searchForm.setCellPadding(2);
		
		searchField = new TextItem("query");
		searchField.setShowTitle(false);
		searchField.setWidth(235);
		searchField.addKeyPressHandler(new KeyPressHandler() {
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if(event.getKeyName().equals("Enter")) {
					Criteria record = new Criteria();
					if(searchField.getValueAsString() != null) {
						record.setAttribute("query", searchField.getValueAsString());
						record.setAttribute("tokenize", "false");
					}
					searchable.search(record);
				}				
			}
			
		});
		ButtonItem searchButton = new ButtonItem("search", "search");
		searchButton.setWidth(50);
		searchButton.setStartRow(false);
		searchButton.setEndRow(false);
		searchButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Criteria record = new Criteria();				
				if(searchField.getValueAsString() != null) {
					record.setAttribute("query", searchField.getValueAsString());
					record.setAttribute("tokenize", "false");
				}
				searchable.search(record);
			}
		});
		
		searchForm.setFields(searchField,searchButton);
		addMember(searchForm);		
				
		buttons = new HLayout();
		buttons.setWidth(30);
		buttons.setMembersMargin(5);
		buttons.setAlign(Alignment.RIGHT);
							
		addButton = new ImgButton();
		addButton.setSrc("/theme/images/icons32/add.png");
		addButton.setPrompt("Add");
		addButton.setWidth(24);
		addButton.setHeight(24);
		addButton.setShowDown(false);
		addButton.setShowRollOver(false);
		if(!settings.getUser().isArchiveManager()) addButton.setVisible(false);
		addButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				if(addCallback != null) addCallback.execute(new DSResponse(), null, new DSRequest());
			}
		});
		buttons.addMember(addButton);
		
		deleteButton = new ImgButton();
		deleteButton.setSrc("/theme/images/icons32/delete.png");
		deleteButton.setPrompt("Delete");
		deleteButton.setWidth(24);
		deleteButton.setHeight(24);
		deleteButton.setShowDown(false);
		deleteButton.setShowRollOver(false);
		deleteButton.setVisible(false);
		deleteButton.addClickHandler(new com.smartgwt.client.widgets.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				if(deleteCallback != null) deleteCallback.execute(new DSResponse(), null, new DSRequest());
			}
		});
		buttons.addMember(deleteButton);
		
		addMember(buttons);
		/*
		EventBus.addHandler(org.heed.openapps.gwt.client.OpenAppsEvent.TYPE, new OpenAppsEventHandler()     {
	        @Override
	        public void onEvent(OpenAppsEvent event) {
	        	if(event.isType(EventTypes.SELECTION)) {
	        		showButtons(true,true);
	        	}
	        }
	    });
		*/
	}
	public void select(Record record) {
		if(settings.getUser().isArchiveManager()) {
			deleteButton.show();
		}
	}
	public void setDeleteCallback(DSCallback callback) {
		this.deleteCallback = callback;
	}
	public void setAddCallback(DSCallback callback) {
		this.addCallback = callback;
	}
	public void setQuery(String query) {
		searchField.setValue(query);
	}
	public void progressStart(String message) {
		progressLabel.setContents(message);
		progressBar.show();
		progressLabel.show();
	}
	public void progressEnd() {
		progressBar.hide();
		progressLabel.hide();
	}
	public void showMessage(String message) {
		messageLabel.setContents("<label style='font-size:12px;font-weight:bold;color:#4C4C4C;font-family:Arial,Verdana,sans-serif;'>"+message+"</label>");
		messageImg.show();
		messageLabel.show();
		messageImgClose.show();
	}
	public void hideMessage() {
		messageImg.hide();
		messageLabel.hide();
		messageImgClose.hide();
	}
}
