package org.archivemanager.collections.client.accessions;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.SpinnerItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;


public class AccessionForm extends DynamicForm {
	private DateItem dateItem;
	
	
	@SuppressWarnings("deprecation")
	public AccessionForm() {
		setWidth100();
		//setHeight100();
		setNumCols(4);
		setCellPadding(3);
		setColWidths("175","*","175","*");
		//setBorder("1px solid #C0C3C7");
		//setDataSource(AccessionDS.getInstance());
		
		List<FormItem> fields = new ArrayList<FormItem>();
		
		StaticTextItem idItem = new StaticTextItem("id", "ID");
		fields.add(idItem);
		
		TextItem accessionNumberItem = new TextItem("identifier", "Number");
		accessionNumberItem.setWidth(85);
		fields.add(accessionNumberItem);
		
		SelectItem yearSelector = new SelectItem();
		int year = new Date().getYear() + 1900;
		LinkedHashMap<Integer, Integer> years = new LinkedHashMap<Integer, Integer>();
		for(int i=0; i < 136; i++) {
			years.put(year, year);
			year--;
		}
		yearSelector.setValueMap(years);
		
		dateItem = new DateItem("date", "Date");
		dateItem.setDateFormatter(DateDisplayFormat.TOSERIALIZEABLEDATE);
		dateItem.setStartDate(new Date(1885, 1, 1));
		dateItem.setYearSelectorProperties(yearSelector);
		fields.add(dateItem);
				
		TextItem costItem = new TextItem("cost", "Cost");
		costItem.setWidth(85);
		fields.add(costItem);
		
		SelectItem aquisitionItem = new SelectItem("aquisition_type", "Aquisition Type");
		aquisitionItem.setWidth(100);
		LinkedHashMap<String,String> valueMap = new LinkedHashMap<String,String>();
		valueMap.put("Deposit","Deposit");
		valueMap.put("Gift","Gift");
		valueMap.put("Purchase","Purchase");
		valueMap.put("Transfer","Transfer");
		aquisitionItem.setValueMap(valueMap);
		fields.add(aquisitionItem);
		
		SpinnerItem quantityItem = new SpinnerItem("extent_number","Quantity");
		quantityItem.setWidth(60);
		quantityItem.setMin(0);
		quantityItem.setMax(1000);
		fields.add(quantityItem);
		
		SelectItem quantityTypeItem = new SelectItem("extent_type", "Quantity Type");
		quantityTypeItem.setWidth(100);
		LinkedHashMap<String,String> valueMap2 = new LinkedHashMap<String,String>();
		valueMap2.put("box","Box");
		valueMap2.put("envelope","Envelope");
		valueMap2.put("package","Package");
		valueMap2.put("digital","Digital");
		quantityTypeItem.setValueMap(valueMap2);
		fields.add(quantityTypeItem);
		
		SpinnerItem pageBoxQuantityItem = new SpinnerItem("pagebox_quantity","Estimated PaigeBox(s) On Shelf");
		pageBoxQuantityItem.setWidth(60);
		pageBoxQuantityItem.setMin(0);
		pageBoxQuantityItem.setMax(1000);
		fields.add(pageBoxQuantityItem);
		
		SelectItem priorityItem = new SelectItem("priority", "Priority");
		priorityItem.setWidth(75);
		LinkedHashMap<String,String> valueMap3 = new LinkedHashMap<String,String>();
		valueMap3.put("","");
		valueMap3.put("low","Low");
		valueMap3.put("normal","Normal");
		valueMap3.put("high","High");
		priorityItem.setValueMap(valueMap3);
		fields.add(priorityItem);
		
		SpinnerItem packagesItem = new SpinnerItem("estimated_packages","Estimated Package(s) On Shelf");
		packagesItem.setWidth(60);
		packagesItem.setMin(0);
		packagesItem.setMax(1000);
		fields.add(packagesItem);
		
		CheckboxItem paidItem = new CheckboxItem("paid", "Previously Paid");
		paidItem.setLabelAsTitle(true);
		fields.add(paidItem);
		
		SpinnerItem linearFeetItem = new SpinnerItem("linear_feet","Linear Feet");
		linearFeetItem.setWidth(60);
		linearFeetItem.setMin(0);
		linearFeetItem.setMax(1000);
		fields.add(linearFeetItem);
		
		CheckboxItem appraisalItem = new CheckboxItem("appraisal", "Donor Wants Appraisal");
		appraisalItem.setLabelAsTitle(true);
		fields.add(appraisalItem);
		
		CheckboxItem newCollectionItem = new CheckboxItem("new_collection", "New Collection");
		newCollectionItem.setLabelAsTitle(true);
		fields.add(newCollectionItem);
		
		CheckboxItem acknowledgedItem = new CheckboxItem("acknowledged", "Acknowledgement Letter Sent");
		acknowledgedItem.setLabelAsTitle(true);
		fields.add(acknowledgedItem);
		
		CheckboxItem existingItem = new CheckboxItem("existing_collection", "Existing Collection");
		existingItem.setLabelAsTitle(true);
		fields.add(existingItem);
		
		TextItem booksItem = new TextItem("books", "Books");
		booksItem.setWidth(400);
		booksItem.setColSpan(2);
		fields.add(booksItem);
		
		TextAreaItem generalNoteItem = new TextAreaItem("general_note", "Contents");
		generalNoteItem.setHeight(90);
		generalNoteItem.setWidth(400);
		generalNoteItem.setColSpan(2);
		fields.add(generalNoteItem);
		
		FormItem[] itemArray = fields.toArray(new FormItem[fields.size()]);		
		setFields(itemArray);
	}
	
	
	public void select(Record record) {		
		editRecord(record);
	}
	@SuppressWarnings("deprecation")
	@Override
	public Record getValuesAsRecord() {
		Record record = new Record();
		record.setAttribute("id", getValue("id"));
		record.setAttribute("qname", getValue("qname"));
		record.setAttribute("name", DateTimeFormat.getShortDateFormat().format(dateItem.getValueAsDate()));
		for(FormItem item : getFields()) {
			if(item.getForm().isVisible() && item.isVisible()) {
				if(item instanceof DateItem) {
					Date value = ((DateItem)item).getValueAsDate();
					if(value != null) {
						record.setAttribute(item.getName(), DateTimeFormat.getShortDateFormat().format(value));
					}
				} else {
					if(item.getValue() != null) record.setAttribute(item.getName(), item.getValue());
				}
			}
		}		
		return record;
	}
	public Record getData() {
		return getValuesAsRecord();
	}
}
