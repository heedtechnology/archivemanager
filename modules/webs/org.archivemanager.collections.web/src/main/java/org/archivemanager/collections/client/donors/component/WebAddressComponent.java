package org.archivemanager.collections.client.donors.component;
import java.util.LinkedHashMap;

import org.archivemanager.collections.client.ArchiveManager;
import org.heed.openapps.gwt.client.data.EntityServiceDS;

import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class WebAddressComponent extends VLayout {
	private ArchiveManager settings = new ArchiveManager();
	private AddNoteWindow addNoteWindow;
	private ListGrid grid;
	private Record selection;
	private HLayout buttons;
	
	
	public WebAddressComponent(String width) {
		
		HLayout toolstrip = new HLayout();
		toolstrip.setWidth(width);
		toolstrip.setHeight(20);
		//toolstrip.setBackgroundImage("[SKIN]/cssButton/button_stretch.png");
		//toolstrip.setBorder("1px solid #A7ABB4");
		addMember(toolstrip);
		
		Label label = new Label("<label style='font-size:13px;font-weight:bold;color:#4C4C4C;font-family:Arial,Verdana,sans-serif;'>Web Addresses</label>");
		label.setHeight("100%");
		label.setWidth("100%");
		toolstrip.addMember(label);
		
		buttons = new HLayout();
		buttons.setHeight(16);
		buttons.setMargin(2);
		buttons.setMembersMargin(3);
		buttons.hide();
		toolstrip.addMember(buttons);
		
		ImgButton addButton = new ImgButton();
		addButton.setWidth(16);
		addButton.setHeight(16);
		addButton.setSrc("/theme/images/icons16/add.png");
		addButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				addNoteWindow.show();
			}
		});
		buttons.addMember(addButton);
		
		final ImgButton delButton = new ImgButton();
		delButton.setWidth(16);
		delButton.setHeight(16);
		delButton.setSrc("/theme/images/icons16/delete.png");
		delButton.hide();
		delButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Record rec = new Record();
				String id = grid.getSelectedRecord().getAttribute("target_id");
				if(id == null) id = grid.getSelectedRecord().getAttribute("id");
				rec.setAttribute("id", id);
				EntityServiceDS.getInstance().removeEntity(rec, new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						grid.removeSelectedData();	
					}						
				});	
			}
		});
		buttons.addMember(delButton);
		
		addNoteWindow = new AddNoteWindow();
		
		grid = new ListGrid();
		grid.setWidth(width);
		grid.setHeight(40);
		grid.setAutoFitData(Autofit.VERTICAL);
		grid.setAutoFitMaxHeight(200);
		grid.setShowHeader(false);
		grid.setWrapCells(true);
		grid.setFixedRecordHeights(false);
		ListGridField typeField = new ListGridField("type","Type");
		typeField.setWidth(60);
		ListGridField contentField = new ListGridField("address");
		ListGridField primaryField = new ListGridField("primary");
		primaryField.setWidth(50);
		grid.setFields(typeField, contentField,primaryField);
		
		grid.addCellClickHandler(new CellClickHandler() {
			@Override
			public void onCellClick(CellClickEvent event) {
				delButton.show();
			}	
		});
		addMember(grid);
				
	}
	public void select(Record record) {
		this.selection = record;
		try {
			Record[] notes = new Record[0];
			Record[] source_associations = record.getAttributeAsRecordArray("source_associations");
			if(source_associations != null) {				
				for(Record source_assoc : source_associations) {					
					Record notesRecord = source_assoc.getAttributeAsRecord("webs");
					if(notesRecord != null) {
						notes = source_assoc.getAttributeAsRecordArray("webs");
					}
				}
				setData(notes);
			}
			if(settings.getUser().isArchiveManager()) {
				buttons.show();
			}
		} catch(ClassCastException e) {
			setData(new Record[0]);
		}
	}
	public void setData(Record[] records) {
		for(Record record : records) {
			String primary = record.getAttribute("primary");
			if(primary != null && primary.equals("true")) record.setAttribute("primary", "primary");
			else record.setAttribute("primary", "");
		}
		grid.setData(records);
	}
	
	public class AddNoteWindow extends Window {
		private static final int FIELD_WIDTH = 300;
		public AddNoteWindow() {
			setWidth(420);
			setHeight(160);
			setTitle("Add A Phone Number");
			setAutoCenter(true);
			setIsModal(true);
			
			final DynamicForm addForm = new DynamicForm();
			addForm.setCellPadding(5);
			final SelectItem typeItem = new SelectItem("type", "Type");
			LinkedHashMap<String,String> valueMap = new LinkedHashMap<String,String>();
			valueMap.put("personal","Personal");
			valueMap.put("work","Work");
			typeItem.setValueMap(valueMap);
			typeItem.setWidth(FIELD_WIDTH/2);
			
			TextItem phoneItem = new TextItem("address","Address");
			phoneItem.setWidth(FIELD_WIDTH);
			
			CheckboxItem primaryItem = new CheckboxItem("primary", "Is Primary");
			
			ButtonItem submitItem = new ButtonItem("submit", "Add");
			submitItem.setIcon("/theme/images/icons16/add.png");
			submitItem.setStartRow(false);
			submitItem.setEndRow(false);
			submitItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					Record record = new Record();
					record.setAttribute("assoc_qname", "openapps_org_contact_1_0_webs");
					record.setAttribute("entity_qname", "openapps_org_contact_1_0_web");
					record.setAttribute("source", selection.getAttribute("id"));
					record.setAttribute("type", addForm.getValue("type"));
					record.setAttribute("address", addForm.getValue("address"));
					record.setAttribute("primary", addForm.getValue("primary"));
					record.setAttribute("name", addForm.getValue("address"));
					EntityServiceDS.getInstance().addAssociation(record, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							if(response.getData() != null && response.getData().length > 0) {
								select(response.getData()[0]);
								addForm.clearValues();
								addNoteWindow.hide();
							}
						}						
					});
				}			
			});
			
			addForm.setFields(phoneItem,typeItem,primaryItem,submitItem);
			addItem(addForm);
		}
	}
	
}