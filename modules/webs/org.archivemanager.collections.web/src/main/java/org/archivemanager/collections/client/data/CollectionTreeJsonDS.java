package org.archivemanager.collections.client.data;
import java.util.Date;

import org.archivemanager.collections.client.ArchiveManager;
import org.heed.openapps.gwt.client.OpenApps;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.DSDataFormat;

public class CollectionTreeJsonDS extends RestDataSource {
	private ArchiveManager settings = new ArchiveManager();	
	private static CollectionTreeJsonDS instance = null; 
	private static OpenApps oa = new OpenApps();
	private static DateTimeFormat dateParser = DateTimeFormat.getFormat("yyyy-MM-dd");
	private static DateTimeFormat dateFormatter = DateTimeFormat.getFormat("EEEE MMMM d, yyyy");
	
	
    public static CollectionTreeJsonDS getInstance() {  
        if (instance == null) {  
            instance = new CollectionTreeJsonDS("collectionTreeJsonDS");  
        }  
        return instance;  
    }
    
	public CollectionTreeJsonDS(String id) {
		setDataFormat(DSDataFormat.JSON);
		setID(id);  
        setTitleField("name");	        
        DataSourceTextField nameField = new DataSourceTextField("name", "Name");     
        
        DataSourceTextField idField = new DataSourceTextField("id", "ID",50);  
        idField.setPrimaryKey(true);  
        idField.setRequired(true);
        
        DataSourceTextField parentField = new DataSourceTextField("parent", "Parent");  
        parentField.setRequired(true);  
        parentField.setForeignKey(id + ".id");  
        parentField.setRootValue("null");  
        
        setFields(idField,nameField,parentField);
        
        String ticket = settings.getTicket();
		if(ticket != null) {
			setFetchDataURL(oa.getServiceUrl() + "service/archivemanager/collection/fetch.json?api_key="+ticket+"&sources=true");
	        setAddDataURL(oa.getServiceUrl() + "service/archivemanager/collection/add.json?api_key="+ticket);
	        setUpdateDataURL(oa.getServiceUrl() + "service/archivemanager/collection/update.json?api_key="+ticket);
	        setRemoveDataURL(oa.getServiceUrl() + "service/archivemanager/collection/remove.json?api_key="+ticket);
		} else {
	        setFetchDataURL(oa.getServiceUrl() + "service/archivemanager/collection/fetch.json?sources=true");
	        setAddDataURL(oa.getServiceUrl() + "service/archivemanager/collection/add.json");
	        setUpdateDataURL(oa.getServiceUrl() + "service/archivemanager/collection/update.json");
	        setRemoveDataURL(oa.getServiceUrl() + "service/archivemanager/collection/remove.json");
		}
	}
	
	@Override
	protected void transformResponse(DSResponse response, DSRequest request,Object data) {
		try {
			if(response.getData() != null && response.getData().length > 0) {
				for(int i=0; i < response.getData().length; i++) {
					Record entity = response.getData()[i];
					if(entity != null) {
						String type = entity.getAttribute("localName");
						if(type != null && !type.equals("collection") && !type.equals("category")) {
							entity.setAttribute("icon", "/theme/images/tree_icons/"+type+".png");
						}
					}
				}
			}
		} catch(ClassCastException e) {
				
		}
		super.transformResponse(response, request, data);
	}
	/*
	@Override
	protected Object transformRequest(DSRequest dsRequest) {
		Map<String, String> httpHeaders = new HashMap<String, String>();
	    //httpHeaders.put("Content-Type", "application/json");
	    //httpHeaders.put("Accept", "application/json");
	    dsRequest.setHttpHeaders(httpHeaders);
	    
		return super.transformRequest(dsRequest);
	}
	*/
	public static String getShortFormattedDate(Date in) {
		return dateParser.format(in);
	}
	public static String getFormattedDate(Date in) {
		return dateFormatter.format(in);
	}
	public static Date getDate(String in) {
		try {
			return dateParser.parse(in);
		} catch(IllegalArgumentException e) {
			e.printStackTrace();
		}
		return null;
	}
	public static String getFormattedDate(String date) {
		try {
			return dateFormatter.format(dateParser.parse(date));
		} catch(IllegalArgumentException e) {
			e.printStackTrace();
		}
		return "";
	}
}