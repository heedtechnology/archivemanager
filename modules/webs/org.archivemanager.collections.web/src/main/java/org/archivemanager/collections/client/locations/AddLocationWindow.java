package org.archivemanager.collections.client.locations;

import org.heed.openapps.gwt.client.data.EntityServiceDS;

import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.TextItem;

public class AddLocationWindow extends Window {
	private DSCallback callback;
	
	
	public AddLocationWindow() {
		setWidth(275);
		setHeight(180);
		setTitle("Add Location");
		setAutoCenter(true);
		setIsModal(true);
		
		final DynamicForm addForm = new DynamicForm();
		addForm.setMargin(5);
		addForm.setCellPadding(3);
		addForm.setNumCols(2);
		addForm.setColWidths(65, "*");
		final TextItem buildingItem = new TextItem("building", "Building");
		buildingItem.setWidth("*");
		
		final TextItem floorItem = new TextItem("floor", "Floor");
		floorItem.setWidth("*");
		
		final TextItem aisleItem = new TextItem("aisle", "Aisle");
		aisleItem.setWidth("*");
		
		final TextItem bayItem = new TextItem("bay", "Bay");
		bayItem.setWidth("*");
		
		ButtonItem submitItem = new ButtonItem("submit", "Add");
		submitItem.setIcon("/theme/images/icons16/add.png");
		submitItem.setStartRow(false);
		submitItem.setEndRow(false);
		submitItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
				Record record = new Record();
				record.setAttribute("qname", "openapps_org_repository_1_0_location");
				record.setAttribute("building", buildingItem.getValue());
				record.setAttribute("floor", floorItem.getValue());
				record.setAttribute("aisle", aisleItem.getValue());
				record.setAttribute("bay", bayItem.getValue());
				String name = "";
				if(buildingItem.getValue() != null) name += buildingItem.getValue();
				if(floorItem.getValue() != null) name += " "+floorItem.getValue();
				if(buildingItem.getValue() != null) name += " "+buildingItem.getValue();
				if(bayItem.getValue() != null) name += " "+bayItem.getValue();
				record.setAttribute("name", name.trim());
				EntityServiceDS.getInstance().addEntity(record, new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						if(callback != null) callback.execute(response, rawData, request);
						addForm.clearValues();
						hide();
					}
				});	
			}
		});
		
		addForm.setFields(buildingItem,floorItem, aisleItem, bayItem, submitItem);
		addItem(addForm);
	}
	public void setCallback(DSCallback callback) {
		this.callback = callback;
	}
}