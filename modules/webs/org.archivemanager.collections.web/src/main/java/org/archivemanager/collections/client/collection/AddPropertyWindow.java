package org.archivemanager.collections.client.collection;
import java.util.LinkedHashMap;

import org.archivemanager.collections.client.data.NativeCollectionModel;
import org.heed.openapps.gwt.client.data.NativeContentTypes;
import org.heed.openapps.gwt.client.event.SaveListener;
import org.heed.openapps.gwt.client.form.ContainerItem;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class AddPropertyWindow extends Window {
	private NativeContentTypes model = new NativeContentTypes();
	private NativeCollectionModel collectionTypes = new NativeCollectionModel();
	
	private SelectItem levelItem;
	private SelectItem contentTypeItem;
	private ContainerItem containerItem;
	private DynamicForm addForm;
	
	private SaveListener saveListener;
	private Label message;
	
	
	public AddPropertyWindow() {
		setWidth(375);
		setHeight(110);
		setTitle("Set Child Property");
		setAutoCenter(true);
		setIsModal(true);
		
		VLayout mainLayout = new VLayout();
		mainLayout.setWidth100();
		mainLayout.setHeight100();
		addItem(mainLayout);
		
		addForm = new DynamicForm();
		addForm.setMargin(3);
		addForm.setCellPadding(5);
		addForm.setColWidths(100, "*");
		
		levelItem = new SelectItem("level", "Level");
		LinkedHashMap<String,String> valueMap = new LinkedHashMap<String,String>();
		valueMap.put("content_type","Content Type");
		valueMap.put("container","Container");
		levelItem.setValueMap(valueMap);
		levelItem.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				if(event.getItem().getValue().equals("content_type")) {
					contentTypeItem.show();
					containerItem.hide();
					setHeight(135);
				} else {
					containerItem.show();
					contentTypeItem.hide();
					setHeight(135);
				}
			}				
		});			
		contentTypeItem = new SelectItem("type", "Content Type");
		contentTypeItem.setValueMap(model.getContentTypes());
		contentTypeItem.setVisible(false);
		
		containerItem = new ContainerItem("container", "Container", collectionTypes.getContainerTypes());
		containerItem.setHeight(22);		
		containerItem.setVisible(false);
		
		addForm.setFields(levelItem,contentTypeItem,containerItem);
		mainLayout.addMember(addForm);
		
		
		HLayout buttonLayout = new HLayout();
		buttonLayout.setWidth100();
		buttonLayout.setHeight(25);
		buttonLayout.setMargin(5);
		buttonLayout.setMembersMargin(17);
		mainLayout.addMember(buttonLayout);
		
		IButton saveButton = new IButton("Save");
		saveButton.setHeight(25);
		saveButton.setWidth(70);
		saveButton.setIcon("/theme/images/icons16/save.png");
		saveButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				String level = levelItem.getValueAsString();
				Record record = new Record();				
				if(level.equals("content_type")) {
					record.setAttribute("qname", "qname");
					record.setAttribute("value", contentTypeItem.getValue());
				} else {
					record.setAttribute("qname", "openapps_org_repository_1_0_"+levelItem.getValue());
					record.setAttribute("value", containerItem.getValue());
				}					
				saveListener.save(record);
			}
		});
		buttonLayout.addMember(saveButton);
		
		message = new Label();
		message.setHeight(25);
		message.setWidth100();
		buttonLayout.addMember(message);
	}
	@Override
	public void show() {
		levelItem.clearValue();
		contentTypeItem.clearValue();
		containerItem.clearValue();
		message.setContents("");
		contentTypeItem.setVisible(false);
		containerItem.setVisible(false);
		setHeight(110);
		super.show();
	}
	public void setSaveListener(SaveListener saveListener) {
		this.saveListener = saveListener;
	}
	public void setMessage(String msg) {
		this.message.setContents(msg);
	}
}