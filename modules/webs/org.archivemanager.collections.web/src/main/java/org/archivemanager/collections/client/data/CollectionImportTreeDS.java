package org.archivemanager.collections.client.data;

import org.archivemanager.collections.client.ArchiveManager;

import com.google.gwt.core.client.JavaScriptObject;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.DSDataFormat;


public class CollectionImportTreeDS extends RestDataSource {
	private ArchiveManager am = new ArchiveManager();	
	private static CollectionImportTreeDS instance = null;  
	  
    public static CollectionImportTreeDS getInstance() {  
        if (instance == null) {  
            instance = new CollectionImportTreeDS("importTreeDS");  
        }  
        return instance;  
    }
    
	public CollectionImportTreeDS(String id) {
		setDataFormat(DSDataFormat.JSON);
		setID(id);  
        setTitleField("name");	        
        DataSourceTextField nameField = new DataSourceTextField("name", "Name");     
        
        DataSourceTextField idField = new DataSourceTextField("id", "ID",50);  
        idField.setPrimaryKey(true);  
        idField.setRequired(true);
        
        DataSourceTextField parentField = new DataSourceTextField("parent", "Parent");  
        parentField.setRequired(true);  
        parentField.setForeignKey(id + ".id");  
        parentField.setRootValue("null");  
        
        setFields(idField,nameField,parentField);
        
        String ticket = am.getTicket();
		if(ticket != null) {
	        setFetchDataURL(am.getServiceUrl() + "service/archivemanager/collection/import/fetch.json?api_key="+ticket);
	        setAddDataURL(am.getServiceUrl() + "service/archivemanager/collection/import/add.json?api_key="+ticket); 
		} else {
			setFetchDataURL(am.getServiceUrl() + "service/archivemanager/collection/import/fetch.json");
	        setAddDataURL(am.getServiceUrl() + "service/archivemanager/collection/import/add.json"); 
		}
	}
	
	@Override
	protected Object transformRequest(DSRequest dsRequest) {
		JavaScriptObject data = dsRequest.getData();
		
		//JSOHelper.setAttribute(data, "ticket", security.getTicket());
		
		return data;
	}
}