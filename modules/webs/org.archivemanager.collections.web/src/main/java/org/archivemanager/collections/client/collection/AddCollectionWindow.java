package org.archivemanager.collections.client.collection;

import org.heed.openapps.gwt.client.event.AddListener;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.TextItem;


public class AddCollectionWindow extends Window {
	private AddListener addListener;
	
	
	public AddCollectionWindow() {
		setWidth(370);
		setHeight(115);
		setTitle("Add Collection");
		setAutoCenter(true);
		setIsModal(true);
		
		final DynamicForm addForm = new DynamicForm();
		addForm.setMargin(10);
		addForm.setCellPadding(7);
		addForm.setColWidths("60", "*");
		
		final TextItem nameItem = new TextItem("name", "Name");
		nameItem.setWidth(275);
					
		ButtonItem submitItem = new ButtonItem("submit", "Add");
		submitItem.setIcon("/theme/images/icons16/add.png");
		submitItem.setStartRow(false);
		submitItem.setEndRow(false);
		submitItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
				Record record = new Record();
				record.setAttribute("qname", "openapps_org_repository_1_0_collection");
				record.setAttribute("name", addForm.getValueAsString("name"));				
				addForm.clearValues();
				addListener.add(record);
				hide();
			}
		});
		
		addForm.setFields(nameItem,submitItem);
		addItem(addForm);
	}
	public void setAddListener(AddListener addListener) {
		this.addListener = addListener;
	}
	
}