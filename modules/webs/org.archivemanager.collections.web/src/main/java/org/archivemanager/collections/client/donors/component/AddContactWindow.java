package org.archivemanager.collections.client.donors.component;
import java.util.LinkedHashMap;

import org.heed.openapps.gwt.client.data.EntityServiceDS;

import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;

public class AddContactWindow extends Window {
	private static final int FIELD_WIDTH = 250;
	private ButtonItem submitItem;
	private SelectItem levelItem;
	private TextItem nameField;
	private TextItem firstNameField;
	private TextItem middleNameField;
	private TextItem lastNameField;
	
	private DynamicForm addForm;
	private DSCallback callback;
	
	
	public AddContactWindow() {
		setWidth(385);
		setHeight(200);
		setTitle("Add Contact");
		setAutoCenter(true);
		setIsModal(true);
		addForm = new DynamicForm();
		addForm.setMargin(3);
		addForm.setCellPadding(5);
		addForm.setColWidths(100, "*");
					
		levelItem = new SelectItem("qname", "Contact Type");
		LinkedHashMap<String,String> valueMap = new LinkedHashMap<String,String>();
		valueMap.put("openapps_org_contact_1_0_individual","Individual");
		valueMap.put("openapps_org_contact_1_0_organization","Organization");
		levelItem.setValueMap(valueMap);
		levelItem.setDefaultValue("openapps_org_contact_1_0_individual");
		levelItem.addChangedHandler(new ChangedHandler() {
			public void onChanged(ChangedEvent event) {
				if(event.getItem().getValue().equals("openapps_org_contact_1_0_individual")) {
					nameField.hide();
					firstNameField.show();
					middleNameField.show();
					lastNameField.show();
					setTitle("Add Individual");
					setHeight(200);
				} else {
					firstNameField.hide();
					middleNameField.hide();
					lastNameField.hide();
					nameField.show();
					setTitle("Add Organization");
					setHeight(130);
				}
			}				
		});			
		
		nameField = new TextItem("name", "Company Name");
		nameField.setVisible(false);
		nameField.setWidth(FIELD_WIDTH);
		
		firstNameField = new TextItem("first_name", "First Name");
		//firstNameField.setVisible(false);
		firstNameField.setWidth(FIELD_WIDTH);
		
		middleNameField = new TextItem("middle_name", "Middle Name");
		//middleNameField.setVisible(false);
		middleNameField.setWidth(FIELD_WIDTH);
		
		lastNameField = new TextItem("last_name", "Last Name");
		//lastNameField.setVisible(false);
		lastNameField.setWidth(FIELD_WIDTH);
		
		submitItem = new ButtonItem("submit", "Add");
		//submitItem.setIcon("/theme/images/icons16/add.png");
		submitItem.setStartRow(false);
		submitItem.setEndRow(false);
		submitItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
				String qname = levelItem.getValueAsString();
				String firstName = addForm.getValue("first_name") != null ? addForm.getValueAsString("first_name") : "";
				String lastName = addForm.getValue("last_name") != null ? addForm.getValueAsString("last_name") : "";
				Record record = addForm.getValuesAsRecord();
				record.setAttribute("qname", qname);
				record.setAttribute("status", "active");
				if(qname.equals("openapps_org_contact_1_0_individual")) 
					record.setAttribute("name", lastName + ", " + firstName);
				EntityServiceDS.getInstance().addEntity(record, new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						if(callback != null) callback.execute(response, rawData, request);
						addForm.clearValues();
						hide();
					}
				});				
			}
		});
		
		addForm.setFields(nameField,firstNameField,middleNameField,lastNameField,levelItem,submitItem);
		addItem(addForm);
	}
	public void selectRecord(Record record) {
		//String type = record.getAttribute("localName");
		
	}
	public void setCallback(DSCallback callback) {
		this.callback = callback;
	}
	
}