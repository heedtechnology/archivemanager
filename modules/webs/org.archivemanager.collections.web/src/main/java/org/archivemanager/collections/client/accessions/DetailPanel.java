package org.archivemanager.collections.client.accessions;

import org.archivemanager.collections.client.ArchiveManager;
import org.heed.openapps.gwt.client.event.SaveListener;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class DetailPanel extends VLayout {
	private ArchiveManager settings = new ArchiveManager();
	private AccessionForm form;
	
	private HLayout buttonLayout;
	private Label message;
		
	private SaveListener saveListener;
	
	
	public DetailPanel() {
		setWidth100();
		setHeight100();
				
		form = new AccessionForm();
		addMember(form);
		
		buttonLayout = new HLayout();
		buttonLayout.setWidth100();
		buttonLayout.setHeight(25);
		buttonLayout.setMargin(5);
		buttonLayout.setMembersMargin(17);
		buttonLayout.hide();
		addMember(buttonLayout);
		
		IButton saveButton = new IButton("Save");
		saveButton.setHeight(25);
		saveButton.setWidth(75);
		saveButton.setIcon("/theme/images/icons16/save.png");
		saveButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Record record = form.getValuesAsRecord();
				saveListener.save(record);
			}
		});
		buttonLayout.addMember(saveButton);
		
		message = new Label();
		message.setHeight(25);
		message.setWidth100();
		buttonLayout.addMember(message);
	}
	public void select(Record record) {
		form.editRecord(record);
		message.setContents("");		
	}
	public void setMessage(String msg) {
		message.setContents(msg);
	}
	public void setSaveListener(SaveListener saveListener) {
		this.saveListener = saveListener;
	}
	
}
