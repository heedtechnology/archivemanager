package org.archivemanager.collections.client.collection;

import org.archivemanager.collections.client.ArchiveManager;
import org.archivemanager.collections.client.collection.form.CategoryForm;
import org.archivemanager.collections.client.collection.form.CollectionForm;
import org.archivemanager.collections.client.collection.form.ItemForm;
import org.heed.openapps.gwt.client.data.EntityServiceDS;
import org.heed.openapps.gwt.client.event.SaveListener;

import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class DetailPanel extends VLayout {
	private ArchiveManager settings = new ArchiveManager();	
	
	private CollectionForm collectionForm;
	private CategoryForm categoryForm;
	private ItemForm itemForm;
	
	private Label message;
	private HLayout buttonLayout;
	
	private SaveListener saveListener;
	
	public DetailPanel() {
		setWidth100();
		setHeight100();
		
		collectionForm = new CollectionForm();
		addMember(collectionForm);
		
		categoryForm = new CategoryForm();
		categoryForm.hide();
		addMember(categoryForm);
		
		itemForm = new ItemForm();
		itemForm.hide();
		addMember(itemForm);
		
		buttonLayout = new HLayout();
		buttonLayout.setWidth100();
		buttonLayout.setHeight(25);
		buttonLayout.setMargin(5);
		buttonLayout.setMembersMargin(17);
		buttonLayout.hide();
		addMember(buttonLayout);
		
		IButton saveButton = new IButton("Save");
		saveButton.setHeight(25);
		saveButton.setWidth(75);
		saveButton.setIcon("/theme/images/icons16/save.png");
		saveButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Record record = null;
				if(collectionForm.isVisible()) {
					record = collectionForm.getValuesAsRecord();					
				} else if(categoryForm.isVisible()) {
					record = categoryForm.getValuesAsRecord();
				} else if(itemForm.isVisible()) {
					record = itemForm.getValuesAsRecord();
				}
				if(record != null) {
					EntityServiceDS.getInstance().updateEntity(record, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							Record[] resultData = response.getData();
							if(resultData != null && resultData.length > 0 && resultData[0].getAttribute("id") != null) {
								saveListener.save(resultData[0]);
								message.setContents("collection saved successfully");
							} else {
								message.setContents("there was a problem saving the collection");
							}
						}
					});
				}
			}
		});
		buttonLayout.addMember(saveButton);
		
		message = new Label();
		message.setHeight(25);
		message.setWidth100();
		buttonLayout.addMember(message);
	}
	public void select(Record record) {
		String qname = record.getAttribute("qname");
    	if(qname != null) {
    		message.setContents("");
    		if(qname.equals("openapps_org_repository_1_0_collection")) {
    	   		collectionForm.editRecord(record);
    	   		categoryForm.hide();
    	   		itemForm.hide();
    	   		collectionForm.show();
    		} else if(qname.equals("openapps_org_repository_1_0_category")) {
    			categoryForm.editRecord(record);
    			collectionForm.hide();
    			itemForm.hide();
    			categoryForm.show();
    		} else {
    			itemForm.editRecord(record);
    			collectionForm.hide();
    			categoryForm.hide();
    			itemForm.show();
    		}
    		if(settings.getUser().isAdmin() || settings.getUser().isArchiveManager()) {
    			buttonLayout.show();
    		}
    	}    	
	}
	public void setSaveListener(SaveListener saveListener) {
		this.saveListener = saveListener;
	}
}
