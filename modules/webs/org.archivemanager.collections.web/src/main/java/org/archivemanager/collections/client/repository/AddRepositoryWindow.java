package org.archivemanager.collections.client.repository;

import org.heed.openapps.gwt.client.data.EntityServiceDS;

import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.TextItem;

public class AddRepositoryWindow extends Window {
	private DSCallback callback;
	
	public AddRepositoryWindow() {
		setWidth(400);
		setHeight(110);
		setTitle("Add Repository");
		setAutoCenter(true);
		setIsModal(true);
		DynamicForm addForm = new DynamicForm();
		addForm.setHeight100();
		addForm.setColWidths("50","*");
		addForm.setCellPadding(7);
		final TextItem nameItem = new TextItem("name", "Name");
		nameItem.setWidth(320);
					
		ButtonItem submitItem = new ButtonItem("submit", "Add");
		submitItem.setIcon("/theme/images/icons16/add.png");
		submitItem.setStartRow(false);
		submitItem.setEndRow(false);
		submitItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
			public void onClick(final com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
				Record record = new Record();
				record.setAttribute("qname", "openapps_org_repository_1_0_repository");
				record.setAttribute("name", nameItem.getValue());
				EntityServiceDS.getInstance().addEntity(record, new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						callback.execute(response, rawData, request);
					}
				});
			}
		});
		
		addForm.setFields(nameItem,submitItem);
		addItem(addForm);
	}

	public void setCallback(DSCallback callback) {
		this.callback = callback;
	}
	
}	