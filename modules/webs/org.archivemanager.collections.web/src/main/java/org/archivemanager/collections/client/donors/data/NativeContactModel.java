package org.archivemanager.collections.client.donors.data;

import java.util.LinkedHashMap;

import com.google.gwt.core.client.JavaScriptObject;
import com.smartgwt.client.util.JSOHelper;

public class NativeContactModel {
	
	public String getDefaultSearchType() { 
		return getNativeDefaultSearchType();
	}	
	private final native String getNativeDefaultSearchType() /*-{
        return $wnd.default_search_type;
	}-*/;
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,String> getSearchTypes() { 
		JavaScriptObject obj = getNativeSearchTypes();
		if(obj != null) return (LinkedHashMap<String,String>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,String>();
	}	
	private final native JavaScriptObject getNativeSearchTypes() /*-{
        return $wnd.search_types;
	}-*/;
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,String> getNoteTypes() { 
		JavaScriptObject obj = getNativeNoteTypes();
		if(obj != null) return (LinkedHashMap<String,String>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,String>();
	}	
	private final native JavaScriptObject getNativeNoteTypes() /*-{
        return $wnd.contact_note_types;
	}-*/;
	
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,String> getActivityTypes() { 
		JavaScriptObject obj = getNativeActivityTypes();
		if(obj != null) return (LinkedHashMap<String,String>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,String>();
	}	
	private final native JavaScriptObject getNativeActivityTypes() /*-{
        return $wnd.activity_types;
	}-*/;
	
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,String> getTaskTypes() { 
		JavaScriptObject obj = getNativeTaskTypes();
		if(obj != null) return (LinkedHashMap<String,String>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,String>();
	}	
	private final native JavaScriptObject getNativeTaskTypes() /*-{
        return $wnd.task_types;
	}-*/;
}
