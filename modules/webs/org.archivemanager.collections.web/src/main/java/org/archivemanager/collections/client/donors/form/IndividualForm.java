package org.archivemanager.collections.client.donors.form;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import org.archivemanager.collections.client.donors.data.NativeContactModel;

import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.SpacerItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextItem;


public class IndividualForm extends DynamicForm {
	private static final int FIELD_WIDTH = 225;
	private NativeContactModel model = new NativeContactModel();
	
		
	@SuppressWarnings("deprecation")
	public IndividualForm() {
		setWidth100();
		setNumCols(4);
		setCellPadding(4);
		setColWidths(100, "*", 125, "*");
		
		List<FormItem> fields = new ArrayList<FormItem>();
		
		StaticTextItem idField = new StaticTextItem("id", "ID");
		fields.add(idField);
		
		SpacerItem spacer = new SpacerItem();
		fields.add(spacer);
		
		SelectItem statusField = new SelectItem("status", "Status");
		statusField.setWidth(90);
		LinkedHashMap<String,String> valueMap = new LinkedHashMap<String,String>();
		valueMap.put("active", "Active");
		valueMap.put("inactive", "Inactive");
		valueMap.put("general", "General");
		statusField.setValueMap(valueMap);
		fields.add(statusField);
		
		SelectItem salutationField = new SelectItem("salutation", "Salutation");
		salutationField.setWidth(60);
		valueMap = new LinkedHashMap<String,String>();
		valueMap.put("mr", "Mr.");
		valueMap.put("mrs", "Mrs.");
		valueMap.put("ms", "Ms.");
		valueMap.put("dr", "Dr.");
		valueMap.put("prof", "Prof.");
		salutationField.setValueMap(valueMap);
		fields.add(salutationField);
		
		SelectItem typeField = new SelectItem("type", "Type");
		typeField.setWidth(120);
		valueMap = new LinkedHashMap<String,String>();
		valueMap.put("collectee", "Active");
		valueMap.put("donor", "Donor");
		valueMap.put("book_collection", "Book Collection");
		typeField.setValueMap(valueMap);
		fields.add(typeField);
		
		TextItem greetingField = new TextItem("greeting", "Greeting");
		greetingField.setWidth(FIELD_WIDTH);
		fields.add(greetingField);
				
		SelectItem qnameField = new SelectItem("qname", "Contact Type");
		qnameField.setWidth(120);
		qnameField.setValueMap(model.getSearchTypes());
		fields.add(qnameField);
		
		TextItem altsalutationField = new TextItem("alt_salutation", "Alternative Salutation");
		altsalutationField.setWidth(FIELD_WIDTH);
		fields.add(altsalutationField);
		
		TextItem firstNameField = new TextItem("first_name", "First Name");
		firstNameField.setWidth(FIELD_WIDTH);
		fields.add(firstNameField);
		
		TextItem dateListField = new TextItem("dateList", "Date List");
		dateListField.setWidth(FIELD_WIDTH);
		fields.add(dateListField);
		
		TextItem middleNameField = new TextItem("middle_name", "Middle Name");
		middleNameField.setWidth(FIELD_WIDTH);
		fields.add(middleNameField);
		
		TextItem roleField = new TextItem("role", "Role");
		roleField.setWidth(FIELD_WIDTH);
		fields.add(roleField);
		
		TextItem lastNameField = new TextItem("last_name", "Last Name");
		lastNameField.setWidth(FIELD_WIDTH);
		fields.add(lastNameField);
		
		SelectItem yearSelector = new SelectItem();
		int year = new Date().getYear() + 1900;
		LinkedHashMap<Integer, Integer> years = new LinkedHashMap<Integer, Integer>();
		for(int i=0; i < 136; i++) {
			years.put(year, year);
			year--;
		}
		yearSelector.setValueMap(years);
		
		DateItem birthDateItem = new DateItem("birth_date", "Birth Date");
		birthDateItem.setDateFormatter(DateDisplayFormat.TOSERIALIZEABLEDATE);
		birthDateItem.setStartDate(new Date(1885, 1, 1));
		birthDateItem.setYearSelectorProperties(yearSelector);
		fields.add(birthDateItem);
				
		TextItem suffixField = new TextItem("suffix", "Suffix");
		suffixField.setWidth(FIELD_WIDTH);
		fields.add(suffixField);
		
		DateItem deathDateItem = new DateItem("death_date", "Death Date");
		deathDateItem.setDateFormatter(DateDisplayFormat.TOSERIALIZEABLEDATE);
		deathDateItem.setStartDate(new Date(1885, 1, 1));
		deathDateItem.setYearSelectorProperties(yearSelector);
		fields.add(deathDateItem);
				
		TextItem nickNameField = new TextItem("nick_name", "Nick Name");
		nickNameField.setWidth(FIELD_WIDTH);
		fields.add(nickNameField);
		
		DateItem lastWroteItem = new DateItem("last_wrote", "Last Wrote");
		lastWroteItem.setDateFormatter(DateDisplayFormat.TOSERIALIZEABLEDATE);
		fields.add(lastWroteItem);
				
		TextItem spouseField = new TextItem("spouse", "Spouse");
		spouseField.setWidth(FIELD_WIDTH);
		fields.add(spouseField);
		
		DateItem lastShipItem = new DateItem("last_ship", "Last Shipped");
		lastShipItem.setDateFormatter(DateDisplayFormat.TOSERIALIZEABLEDATE);
		fields.add(lastShipItem);
		
	    FormItem[] itemArray = fields.toArray(new FormItem[fields.size()]);		
		setFields(itemArray);
	}
		
}
