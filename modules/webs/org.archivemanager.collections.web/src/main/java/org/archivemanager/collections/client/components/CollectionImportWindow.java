package org.archivemanager.collections.client.components;

import org.archivemanager.collections.client.data.CollectionImportTreeDS;
import org.archivemanager.collections.client.data.NativeImportModel;
import org.archivemanager.gwt.client.data.ArchiveManagerCollectionImportDS;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.EventTypes;
import org.heed.openapps.gwt.client.OpenApps;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.OpenAppsEventHandler;
import org.heed.openapps.gwt.client.component.FileUploadComponent;
import org.heed.openapps.gwt.client.event.UploadListener;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.tree.TreeGrid;
import com.smartgwt.client.widgets.tree.events.DataArrivedEvent;
import com.smartgwt.client.widgets.tree.events.DataArrivedHandler;


public class CollectionImportWindow extends Window {
	private OpenApps security = new OpenApps();
	private NativeImportModel model = new NativeImportModel();
	private TreeGrid tree;
	private FileUploadComponent upload;
	private DynamicForm propertyForm;
	private ListGrid propertyGrid;
	
	private Record repository;
	private String sessionKey;
	
	public CollectionImportWindow() {
		setWidth(900);
		setHeight(600);
		setTitle("Collection Import");
		setAutoCenter(true);
		setIsModal(true);
		
		HLayout topLayout = new HLayout();
		topLayout.setHeight(40);
		topLayout.setWidth100();
		topLayout.setMargin(5);
		upload = new FileUploadComponent(model.getImportProcessors(), FileUploadComponent.Mode.SIMPLE);
		upload.setAction(security.getServiceUrl() + "/service/archivemanager/repository/collection/import/upload.json");
		upload.setWidth(500);
		upload.setHeight("100%");
		upload.addUploadListener(new UploadListener() {
			public void uploadComplete(String key) {
				sessionKey = key;
				Criteria criteria = new Criteria();
				criteria.addCriteria("session", sessionKey);
				tree.fetchData(criteria);
				SC.say("Collection Imported Successfully");
			}
		});
		topLayout.addMember(upload);		
		addItem(topLayout);
		
		HLayout bottomLayout = new HLayout();		
		addItem(bottomLayout);
		
		tree = new TreeGrid();
		tree.setDataSource(CollectionImportTreeDS.getInstance());
		tree.setWidth(350);
		tree.setHeight100();
		tree.setAutoFetchData(false);
		tree.setShowHeader(false);
		tree.setShowResizeBar(true);
		tree.setLeaveScrollbarGap(false);
		tree.setSelectionType(SelectionStyle.SINGLE);
		tree.addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				String id = event.getRecord().getAttribute("id");
				Criteria criteria = new Criteria();
				criteria.setAttribute("_dataSource", "node");
				criteria.setAttribute("id", id);
				criteria.setAttribute("session", sessionKey);
				ArchiveManagerCollectionImportDS.getInstance().fetchData(criteria, new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						if(response.getData() != null && response.getData().length > 0) {
							Record collection = response.getData()[0];
							if(collection != null) {
								propertyForm.editRecord(collection);
								propertyGrid.setData(collection.getAttributeAsRecord("properties").getAttributeAsRecordArray("property"));
							}
						}
					}
				});
			}			
		});
		tree.addDataArrivedHandler(new DataArrivedHandler() {
			public void onDataArrived(DataArrivedEvent event) {
				//Tree treeData = tree.getData();
		        //TreeNode node = (TreeNode) treeData.find("0");
		        //treeData.openFolder(node);
			}			
		});
		bottomLayout.addMember(tree);
		
		TabSet tabs = new TabSet();
		Tab tab1 = new Tab("Properties");
		VLayout propertyLayout = new VLayout();
		tab1.setPane(propertyLayout);
		tabs.addTab(tab1);
		
		propertyForm = new DynamicForm();
		propertyLayout.addMember(propertyForm);
		StaticTextItem nameItem = new StaticTextItem("localName", "Type");
		propertyForm.setFields(nameItem);
		
		propertyGrid = new ListGrid();
		propertyLayout.addMember(propertyGrid);
		ListGridField nameField = new ListGridField("name", "Name");
		ListGridField valueField = new ListGridField("value", "Value");
		propertyGrid.setFields(nameField, valueField);
		
		bottomLayout.addMember(tabs);
		
		EventBus.addHandler(org.heed.openapps.gwt.client.OpenAppsEvent.TYPE, new OpenAppsEventHandler()     {
	        @Override
	        public void onEvent(OpenAppsEvent event) {
	            if(event.isType(EventTypes.IMPORT_START)) {
	            	String repo = repository.getAttribute("id");
	            	Record record = new Record();
	            	record.setAttribute("repository", repo);
	            	record.setAttribute("session", sessionKey);
	            	ArchiveManagerCollectionImportDS.getInstance().addData(record, new DSCallback() {
	        			public void execute(DSResponse response, Object rawData, DSRequest request) {
	        				if(response.getData() != null && response.getData().length > 0) {
	        					Record collection = response.getData()[0];
	        					if(collection != null) {
	        						propertyForm.editRecord(collection);
	        						propertyGrid.setData(collection.getAttributeAsRecord("properties").getAttributeAsRecordArray("property"));
	        					}
	        				}
	        			}
	        		});
	            }
	        }
	    });
	}
	
	public void select(Record repository) {
		this.repository = repository;
		upload.setHiddenItem("repository", repository.getAttribute("id"));
	}
	
}
