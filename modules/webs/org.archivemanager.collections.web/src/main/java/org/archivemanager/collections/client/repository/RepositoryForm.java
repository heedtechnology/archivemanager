package org.archivemanager.collections.client.repository;

import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.SpacerItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextItem;


public class RepositoryForm extends DynamicForm {
	
	private static final int fieldWidth = 300;
	
	
	public RepositoryForm() {
		setWidth100();
		setHeight100();
		setMargin(5);
		setCellPadding(3);
		setNumCols(2);
		setColWidths("100","*");
		
		SpacerItem spacer = new SpacerItem();
		spacer.setHeight(20);
		
		StaticTextItem idItem = new StaticTextItem("id", "ID");
		TextItem nameItem = new TextItem("name", "Name");
		nameItem.setWidth(fieldWidth);		
		TextItem shortNameItem = new TextItem("short_name", "Short Name");
		shortNameItem.setWidth(fieldWidth);
		TextItem urlItem = new TextItem("url", "URL");
		urlItem.setWidth(fieldWidth);
		TextItem countryItem = new TextItem("country_code", "Country Code");
		countryItem.setWidth(fieldWidth);
		TextItem agencyItem = new TextItem("agency_code", "Agency Code");
		agencyItem.setWidth(fieldWidth);
		TextItem ncesItem = new TextItem("nces", "NCES");
		ncesItem.setWidth(fieldWidth);
		TextItem brandingItem = new TextItem("branding", "Branding");
		brandingItem.setWidth(fieldWidth);
		TextItem languageItem = new TextItem("lang", "Language");
		languageItem.setWidth(fieldWidth);
		CheckboxItem publicItem = new CheckboxItem("isPublic","Public");
				
		setFields(spacer,idItem,nameItem,shortNameItem,urlItem,countryItem,agencyItem,ncesItem,brandingItem,languageItem,publicItem);
	}
	
}
