package org.archivemanager.collections.client.collection;

import org.archivemanager.collections.client.ArchiveManager;
import org.archivemanager.collections.client.data.CollectionImportTreeDS;
import org.archivemanager.collections.client.data.NativeImportModel;
import org.archivemanager.gwt.client.data.AMServiceDS;
import org.heed.openapps.gwt.client.component.FileUploadComponent;
import org.heed.openapps.gwt.client.event.SaveListener;
import org.heed.openapps.gwt.client.event.UploadListener;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.tree.Tree;
import com.smartgwt.client.widgets.tree.TreeGrid;
import com.smartgwt.client.widgets.tree.TreeNode;
import com.smartgwt.client.widgets.tree.events.DataArrivedEvent;
import com.smartgwt.client.widgets.tree.events.DataArrivedHandler;


public class ImportPanel extends VLayout {
	private ArchiveManager am = new ArchiveManager();
	private DataSource ds = CollectionImportTreeDS.getInstance();
	private NativeImportModel model = new NativeImportModel();
	private TreeGrid tree;
	private FileUploadComponent upload;
	private DynamicForm propertyForm;
		
	private String sessionKey;
	private String collectionId;
	
	
	public ImportPanel() {
		setWidth100();
		setHeight100();
		
		HLayout topLayout = new HLayout();
		topLayout.setHeight(30);
		topLayout.setWidth100();
		topLayout.setMargin(5);
		upload = new FileUploadComponent(model.getImportProcessors(), FileUploadComponent.Mode.PROCESSED);
		String ticket = am.getTicket();
		if(ticket != null) {
			upload.setAction(am.getServiceUrl() + "service/archivemanager/collection/import/upload.json?api_key="+ticket);
		} else {
			upload.setAction(am.getServiceUrl() + "service/archivemanager/collection/import/upload.json");
		}
		upload.setWidth(500);
		upload.setHeight("100%");
		upload.addUploadListener(new UploadListener() {
			public void uploadComplete(String key) {
				sessionKey = key;
				upload.setSessionKey(key);
				Criteria criteria = new Criteria();
				criteria.setAttribute("source", "nodes");
				criteria.addCriteria("session", sessionKey);
				tree.fetchData(criteria);
				upload.setMessage("Collection Loaded Successfully");
			}
		});
		upload.addSaveListener(new SaveListener() {
			@Override
			public void save(Record record) {
				record.setAttribute("assoc_qname", "openapps_org_repository_1_0_categories");
				record.setAttribute("entity_qname", "openapps_org_repository_1_0_category");
				record.setAttribute("source", collectionId);
				record.setAttribute("format", "tree");
				upload.setMessage("Collection Import In Process...");
				AMServiceDS.getInstance().collectionAdd(record, new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						upload.setMessage("Collection Imported Successfully.");
					}
				});
			}			
		});
		topLayout.addMember(upload);		
		addMember(topLayout);
		
		HLayout bottomLayout = new HLayout();		
		addMember(bottomLayout);
		
		tree = new TreeGrid();
		tree.setDataSource(ds);
		tree.setWidth("50%");
		tree.setHeight100();
		tree.setAutoFetchData(false);
		tree.setShowHeader(false);
		tree.setLeaveScrollbarGap(false);
		tree.setSelectionType(SelectionStyle.SINGLE);
		tree.addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				propertyForm.editRecord(event.getRecord());
			}			
		});
		tree.addDataArrivedHandler(new DataArrivedHandler() {
			public void onDataArrived(DataArrivedEvent event) {
				Tree treeData = tree.getData();
		        TreeNode node = (TreeNode)treeData.find("null");
		        treeData.openFolder(node);
			}			
		});
		bottomLayout.addMember(tree);
		
		TabSet tabs = new TabSet();
		Tab tab1 = new Tab("Properties");
		VLayout propertyLayout = new VLayout();
		tab1.setPane(propertyLayout);
		tabs.addTab(tab1);
		
		propertyForm = new DynamicForm();
		propertyForm.setMargin(10);
		propertyForm.setCellPadding(8);
		propertyForm.setColWidths("50", "*");
		propertyLayout.addMember(propertyForm);
		
		StaticTextItem nameItem = new StaticTextItem("name", "Name");
		StaticTextItem levelItem = new StaticTextItem("level", "Level");
		StaticTextItem containerItem = new StaticTextItem("container", "Container");
		
		propertyForm.setFields(levelItem,nameItem,containerItem);
				
		bottomLayout.addMember(tabs);
		
	}
	public void select(Record collection) {
		collectionId = collection.getAttribute("id");
	}
	public void canEdit(boolean editor) {
		
	}
}
