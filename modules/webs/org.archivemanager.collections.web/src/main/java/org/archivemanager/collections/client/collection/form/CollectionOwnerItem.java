package org.archivemanager.collections.client.collection.form;

import org.heed.openapps.gwt.client.data.EntityServiceDS;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.FormItemIcon;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.IconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.IconClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.layout.VLayout;


public class CollectionOwnerItem extends StaticTextItem {
	private CollectionOwnerUpdateWindow window;
	
	private long userId;
	
	public CollectionOwnerItem(String name, String title) {
		super(name, title);
		setHeight(16);
		setWrap(false);
		FormItemIcon formItemIcon = new FormItemIcon();
        setIcons(formItemIcon);
        
        addIconClickHandler(new IconClickHandler() {            
            public void onIconClick(IconClickEvent event) {
                window.show();
            }
        });
        window = new CollectionOwnerUpdateWindow();
	}
	
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}

	public class CollectionOwnerUpdateWindow extends Window {
		private final ListGrid addGrid;
		
		public CollectionOwnerUpdateWindow() {
			setWidth(350);
			setHeight(260);
			setTitle("Add Permission");
			setAutoCenter(true);
			setIsModal(true);
			
			VLayout mainLayout = new VLayout();
			mainLayout.setWidth100();
			mainLayout.setHeight100();
			addItem(mainLayout);
			
			final DynamicForm searchForm = new DynamicForm();
			searchForm.setWidth("100%");
			searchForm.setHeight(25);
			searchForm.setMargin(2);
			searchForm.setNumCols(3);
			searchForm.setCellPadding(2);
			
			final TextItem searchField = new TextItem("query");
			searchField.setWidth(275);
			searchField.setShowTitle(false);
			
			ButtonItem searchButton = new ButtonItem("search", "search");
			searchButton.setWidth(50);
			searchButton.setStartRow(false);
			searchButton.setEndRow(false);
			searchButton.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					Criteria criteria = new Criteria();
					criteria.setAttribute("qname", "openapps_org_system_1_0_user");
					if(searchField.getValue() != null) {
						criteria.setAttribute("query", searchField.getValueAsString());
					} else {
						criteria.setAttribute("_startRow", "0");
						criteria.setAttribute("_endRow", "75");
					}
					EntityServiceDS.getInstance().search(criteria, new DSCallback() {
						@Override
						public void execute(DSResponse dsResponse, Object data,	DSRequest dsRequest) {
							addGrid.setData(dsResponse.getData());
						}						
					});
				}
			});
			searchForm.setFields(searchField,searchButton);
			mainLayout.addMember(searchForm);
			
			addGrid = new ListGrid();
			addGrid.setWidth("100%");
			addGrid.setHeight(170);
			addGrid.setMargin(2);
			addGrid.setShowHeader(false);
			
			ListGridField nameField = new ListGridField("name","Name");
			nameField.setWidth(100);
			
			ListGridField emailField = new ListGridField("email","Email");
			
			addGrid.setFields(nameField, emailField);
			
			mainLayout.addMember(addGrid);
			
			final DynamicForm levelForm = new DynamicForm();
			levelForm.setNumCols(3);
			levelForm.setMargin(2);
			levelForm.setColWidths("50","100","*");
			
			addItem(levelForm);
			
			ButtonItem linkItem = new ButtonItem("Link");
			linkItem.setIcon("/theme/images/icons16/link.png");
			linkItem.setWidth(50);
			linkItem.setStartRow(false);
			linkItem.setEndRow(false);
			linkItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					if(addGrid.anySelected()) {						
						String userid = addGrid.getSelectedRecord().getAttribute("id");
						if(userid != null && userid.length() > 0) {
							userId = Long.valueOf(userid);
							String username = addGrid.getSelectedRecord().getAttribute("name");
							setValue(username);
						}
						levelForm.clearValues();
						addGrid.removeSelectedData();
						window.hide();
					}
				}			
			});
			levelForm.setFields(linkItem);
		}		
	}
}
