<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Collections" %>
<%@ page import="org.heed.openapps.entity.EntityService" %>
<%@ page import="org.archivemanager.repository.data.ContainerSorter" %>
<%@ page import="org.heed.openapps.RepositoryModel" %>
<%@ page import="org.heed.openapps.SystemModel" %>
<%@ page import="org.heed.openapps.util.HTMLUtility" %>
<%@ page import="org.heed.openapps.util.NumberUtility" %>
<%@ page import="org.heed.openapps.entity.Entity" %>
<%@ page import="org.heed.openapps.entity.Association" %>
<%@ page import="org.archivemanager.repository.server.FindingAidUtil" %>
<%
int width = 790;

EntityService entityService = (EntityService)request.getAttribute("entityService");
ContainerSorter containerSorter = (ContainerSorter)request.getAttribute("containerSorter");
Entity collection = (Entity)request.getAttribute("collection");
FindingAidUtil util = new FindingAidUtil();

String primarContainer = "";
String secondaryContainer = "";

boolean printLevel2 = true;
boolean printLevel3 = false;
boolean printLevel4 = false;
%>
<div style="text-align:center;"><%=collection.getPropertyValue(SystemModel.OPENAPPS_NAME) %></div>
<div style="text-align:center;">#<%=HTMLUtility.removeTags(collection.getPropertyValue(RepositoryModel.COLLECTION_IDENTIFIER))%></div>
<div style="text-align:center;"><%=HTMLUtility.removeTags(collection.getPropertyValue(RepositoryModel.ACCESSION_DATE)) %></div>
<div style="text-align:center;">Preliminary Listing</div>
<div style="height:100%;width:<%=width%>px;margin:0 auto;border:0px solid black;">
<%
entityService.hydrate(collection);
List<Association> level1Children = collection.getSourceAssociations(RepositoryModel.CATEGORIES, RepositoryModel.ITEMS);
Collections.sort(level1Children, containerSorter);
for(int i=0; i < level1Children.size(); i++) {
	Association assoc1 = level1Children.get(i);
	Entity level1 = assoc1.getTargetEntity();	
	String level1Summary = level1.getPropertyValue(RepositoryModel.SUMMARY);
	if(level1Summary != null && level1Summary.length() > 0) level1Summary = "["+level1Summary+"]";
	else level1Summary = "";
	String container1 = level1.getPropertyValue(RepositoryModel.CONTAINER);
	String[] containers1 = container1.split(",");
	String c1 = containers1[0] != null && containers1[0].length() > 0 ? containers1[0] : "";
%>
	<div style="float:left;width:100%;border:0px solid black;padding:5px;">
		<div style="margin-left:75px;width:50px;float:left;border:0px solid black;"><%=util.getNextMarker(1)%>.</div>
		<div style="float:left;width:<%=width-135%>px;border:0px solid black;">
			<%=level1.getPropertyValue(SystemModel.OPENAPPS_NAME) %>
			<%=level1.getPropertyValue(RepositoryModel.DESCRIPTION) %>
			<%=level1Summary %>
		</div>
	</div>
	<%
	if(printLevel2) {
	entityService.hydrate(level1);
	List<Association> level2Children = level1.getSourceAssociations(RepositoryModel.CATEGORIES, RepositoryModel.ITEMS);
	Collections.sort(level2Children, containerSorter);
	for(int j=0; j < level2Children.size(); j++) {
		Association assoc2 = level2Children.get(j);
		Entity level2 = assoc2.getTargetEntity();		
		String level2Summary = level2.getPropertyValue(RepositoryModel.SUMMARY);
		if(level2Summary != null && level2Summary.length() > 0) level2Summary = "["+level2Summary+"]";
		else level2Summary = "";
		String container2 = level2.getPropertyValue(RepositoryModel.CONTAINER);
		String[] containers2 = container2.split(",");
		String c2 = containers2[0] != null && containers2[0].length() > 0 ? containers2[0] : "&nbsp;";
		String c22 = "";
		if(containers2.length == 4) {
			if(containers2[2].equals("folder")) c22 = "[F. "+containers2[3]+"]";
			if(containers2[2].equals("package")) c22 = "[P. "+containers2[3]+"]";
		}
		if(c2.equals(primarContainer)) c2 = "";
		else primarContainer = c2;
	%>
		<div style="float:left;width:100%;border:0px solid black;padding:5px;">
			<div style="width:75px;float:left;"><%=c2 %></div>
			<div style="width:50px;margin-left:50px;float:left;border:0px solid black;"><%=util.getNextMarker(2)%>.</div>
			<div style="float:left;width:<%=width-185%>px;border:0px solid black;">
				<%=level2.getPropertyValue(SystemModel.OPENAPPS_NAME) %>
				<%=level2.getPropertyValue(RepositoryModel.DESCRIPTION) %>
				<%=level2Summary %>
			</div>
			<div style="float:right;"><%=c22 %></div>
		</div>
		<%
		if(printLevel3) {
		entityService.hydrate(level2);
		List<Association> level3Children = level2.getSourceAssociations(RepositoryModel.CATEGORIES, RepositoryModel.ITEMS);
		Collections.sort(level3Children, containerSorter);
		for(int k=0; k < level3Children.size(); k++) {
			Association assoc3 = level3Children.get(k);
			Entity level3 = assoc3.getTargetEntity();			
			String level3Summary = level3.getPropertyValue(RepositoryModel.SUMMARY);
			if(level3Summary != null && level3Summary.length() > 0) level3Summary = "["+level3Summary+"]";
			else level3Summary = "";
			String container3 = level3.getPropertyValue(RepositoryModel.CONTAINER);
			String[] containers3 = container3.trim().replace("  ", " ").split(" ");
			String c3 = containers3[0] != null && containers3[0].length() > 0 ? containers3[0] : "&nbsp;";
			String c33 = "";
			if(containers3.length == 4) {
				if(containers3[2].equals("Folder")) c33 = "[F. "+containers3[3]+"]";
				if(containers3[2].equals("Package")) c33 = "[P. "+containers3[3]+"]";
			}
			if(c33.equals(secondaryContainer)) c33 = "";
			else secondaryContainer = c33;
		%>
			<div style="float:left;width:100%;border:0px solid black;padding:5px;">
				<div style="width:50px;margin-left:175px;float:left;"><%=util.getNextMarker(3)%>.</div>
				<div style="float:left;width:<%=width-310%>px;">
					<%=level3.getPropertyValue(SystemModel.OPENAPPS_NAME) %>
					<%=level3.getPropertyValue(RepositoryModel.DESCRIPTION) %>
					<%=level3Summary %>
				</div>
				<div style="float:right;"><%=c33 %></div>
			</div>
			<%
			if(printLevel4) {
			entityService.hydrate(level3);
			List<Association> level4Children = level3.getSourceAssociations(RepositoryModel.CATEGORIES, RepositoryModel.ITEMS);
			Collections.sort(level4Children, containerSorter);
			for(int l=0; l < level4Children.size(); l++) {
				Association assoc4 = level4Children.get(l);
				Entity level4 = assoc4.getTargetEntity();
				
				String level4Summary = level4.getPropertyValue(RepositoryModel.SUMMARY);
				if(level4Summary != null && level4Summary.length() > 0) level4Summary = "["+level4Summary+"]";
				else level4Summary = "";
				String container4 = level4.getPropertyValue(RepositoryModel.CONTAINER);
				String[] containers4 = container4.trim().replace("  ", " ").split(" ");
				String c4 = containers4[0] != null && containers4[0].length() > 0 ? containers4[0] : "&nbsp;";
				String c44 = "";
				if(containers4.length == 4) {
					if(containers4[2].equals("folder")) c44 = "[F. "+containers4[3]+"]";
					if(containers4[2].equals("package")) c44 = "[P. "+containers4[3]+"]";
				}
				if(c44.equals(secondaryContainer)) c44 = "";
				else secondaryContainer = c44;
				%>
				<div style="float:left;width:100%;border:0px solid black;padding:5px;">
					<div style="width:50px;margin-left:225px;float:left;"><%=util.getNextMarker(4)%>.</div>
					<div style="float:left;width:<%=width-285%>px;">
						<%=level4.getPropertyValue(SystemModel.OPENAPPS_NAME) %>
						<%=level4.getPropertyValue(RepositoryModel.DESCRIPTION) %>
						<%=level4Summary %>
						<%=c44 %>
					</div>
				</div>
				<%
				entityService.hydrate(level4);
				List<Association> level5Children = level4.getSourceAssociations(RepositoryModel.CATEGORIES, RepositoryModel.ITEMS);
				Collections.sort(level5Children, containerSorter);
				for(int m=0; m < level5Children.size(); m++) {
					Association assoc5 = level5Children.get(m);
					Entity level5 = assoc5.getTargetEntity();
				
					String level5Summary = level5.getPropertyValue(RepositoryModel.SUMMARY);
					if(level5Summary != null && level5Summary.length() > 0) level5Summary = "["+level5Summary+"]";
					else level5Summary = "";
					String container5 = level5.getPropertyValue(RepositoryModel.CONTAINER);
					String[] containers5 = container5.trim().replace("  ", " ").split(" ");
					String c5 = containers5[0] != null && containers5[0].length() > 0 ? containers5[0] : "&nbsp;";
					String c55 = "";
					if(containers5.length == 4) {
						if(containers5[2].equals("folder")) c55 = "[F. "+containers5[3]+"]";
						if(containers5[2].equals("package")) c55 = "[P. "+containers5[3]+"]";
					}
					if(c55.equals(secondaryContainer)) c55 = "";
					else secondaryContainer = c55;
				%>
				<div style="float:left;width:100%;border:0px solid black;padding:5px;">
					<div style="width:50px;margin-left:275px;float:left;"><%=util.getNextMarker(5)%>.</div>
					<div style="float:left;width:<%=width-335%>px;">
						<%=level5.getPropertyValue(SystemModel.OPENAPPS_NAME) %>
						<%=level5.getPropertyValue(RepositoryModel.DESCRIPTION) %>
						<%=level5Summary %>
						<%=c55 %>
					</div>
				</div>
		
				<%
				}
				%>
			<%
			}}
			%>
		<%
		}}
		%>
	<%
	}}
	%>		
<%
}
%>
</div>