<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.List" %>
<%@ page import="org.heed.openapps.RepositoryModel" %>
<%@ page import="org.heed.openapps.SystemModel" %>
<%@ page import="org.heed.openapps.entity.Entity" %>
<%
Entity item = (Entity)request.getAttribute("item");
String name = item.getPropertyValue(SystemModel.OPENAPPS_NAME);
String date = item.getPropertyValue(RepositoryModel.DATE_EXPRESSION);
%>
<table style="width:500px;margin:10px;">
	<tr>
		<td colspan="2" style="font-size:20px;font-weight:bold;"><c:out value="${item.qname.localName}" /></td>
	</tr>
	<tr>
		<td><span style="margin-left:25px;"><%=name %></span></td>
		<td align="right"><span style="margin:0 10px 0 25px;"><%=date %></span></td>
	</tr>
	<%
		List<Entity> notes = (List<Entity>)request.getAttribute("notes");
		if(notes != null) {
			for(Entity note : notes) {
				String type = note.getPropertyValue(SystemModel.NOTE_TYPE);
				String content = note.getPropertyValue(SystemModel.NOTE_CONTENT);
	%>
			<tr><td colspan="2">
				<div style="margin-left:25px;padding-bottom:1px;">
					<u><%=type%></u>
					<span> - </span> 
					<%=content%>
				</div>
			</td></tr>
	<%
		}}
	%>	
</table>