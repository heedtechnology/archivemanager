<script src="/smartclient/system/modules/ISC_PluginBridges.js"></script>
<script src="/repository/script/repository_library.js"></script>
<script src="/classification/script/classification_library.js"></script>
<script src="/digital/script/digital_library.js"></script>
<script src="/repository/script/collection_import.js"></script>
<script src="/repository/script/export.js"></script>
<script src="/flowplayer/flowplayer.min.js"></script>
<script type="text/javascript">
	var modesData = ${modes};
	var repositories = ${repositories};
</script>
<div class="body_container">
	<table style="width:100%;">
		<tr><td class="top-left"></td><td class="top-mid"></td><td class="top-right"></td></tr>
		<tr>
			<td class="mid-left"></td>
			<td class="mid-mid">
				<div style="width:100%;">
					<script src="../script/collections.js"></script>
        		</div> 
			</td>
			<td class="mid-right"></td>
		</tr>
		<tr><td class="bot-left"></td><td class="bot-mid"></td><td class="bot-right"></td></tr>
	</table>
</div>