var activityValueMap = {'Processing Beginning':'Processing Beginning','Processing Ending':'Processing Ending'};
var noteValueMap = {'General':'General'};
var taskValueMap = {'General':'General'};
var typeValueMap = {'building':'Building','floor':'Floor','room':'Room','area':'Area'};
var currCompNode = 0;
function updateEntityData(xmlDoc, xmlText) {
	locationXml = ds.recordsFromXML(isc.XMLTools.selectNodes(xmlDoc, "//node"));
	locationTree.getSelectedRecord()['title'] = locationXml[0]['title'];
	locationTree.refreshFields();
	locationForm1.setValues(locationXml);
	itemXml = ds.recordsFromXML(isc.XMLTools.selectNodes(xmlDoc, "//node/source_associations/items/node"));
	collectionItemRelationList.setData(itemXml);
	noteXml = ds.recordsFromXML(isc.XMLTools.selectNodes(xmlDoc, "//node/source_associations/notes/node"));
	noteList.setData(noteXml);
	activityXml = ds.recordsFromXML(isc.XMLTools.selectNodes(xmlDoc, "//node/source_associations/activities/node"));
	activityList.setData(activityXml);
	taskXml = ds.recordsFromXML(isc.XMLTools.selectNodes(xmlDoc, "//node/source_associations/tasks/node"));
	taskList.setData(taskXml);
}
function save(reload) {
	if(locationForm1.valuesHaveChanged()) {
		var parms = locationForm1.getValues();
  		parms['qname'] = '{openapps.org_repository_1.0}location';
 		isc.XMLTools.loadXML("/core/entity/update", function(xmlDoc, xmlText) {
 			updateEntityData(xmlDoc, xmlText)
 		},{httpMethod:"POST",params:parms});
	}
}
function load(node) {
	save(false);
	entityId = node.id;
	if(currCompNode !== null && node.id !== currCompNode.id) {
		isc.XMLTools.loadXML("/core/entity/get/"+node.id, "updateEntityData(xmlDoc, xmlText)");
		currCompNode = node;
	}
}
function displaySpace() {
	if(spaceAvailable) {
		//alert(spaceFilled+' / '+spaceAvailable);
		spaceViewer.removeMembers(spaceViewer.getMembers());
		spaceViewer.addMember(isc.Label.create({ID:"label1",height:"100%",width:100,margin:12,layoutAlign:"left",valign:"center",wrap:false,showEdges:false,contents:"<b>Spaces "+spaceFilled+" of "+spaceAvailable+"</b>"}));
		for(var i=0; i < spaceAvailable; i++) {
			if(i < spaceFilled) spaceViewer.addMember(isc.Canvas.create({width:20,height:50,border:"1px solid black",backgroundColor:"red"}));
			else spaceViewer.addMember(isc.Canvas.create({width:20,height:50,border:"1px solid black"}));
		}
	}
}
isc.MessagingDataSource.create({ID:"locationDS",fetchDataURL:"/repository/locations.xml",addDataURL:"/repository/location/add.xml",updateDataURL:"/core/entity/update.xml",removeDataURL:"/core/entity/remove.xml",
	fields:[
		{name:"id", title:"ID", type:"text", primaryKey:true, required:true},
    	{name:"title", title:"Title", type:"text", length:"128", required:true},
    	{name:"idx", title:"Index", type:"integer"},
    	{name:"parent", title:"ParentID", type:"text", required:true, foreignKey:"repositories.id", rootValue:"0"}
	]
});

isc.HLayout.create({ID:'pageLayout',width:smartWidth,height:smartHeight,position:'relative',visibility:'visible',
	members:[
	    isc.VLayout.create({ID:'leftCanvas',visibility:'visible',position:'absolute',width:'350',height:'100%',
	    	members:[
	    	    isc.HLayout.create({ID:'buttonPanel',height:'25',width:'100%',autoDraw:false,layoutLeftMargin:2,membersMargin:5,
	    	    	members:[
	    	    	    isc.IButton.create({autoDraw:false,title:'import',width:70,icon:"/theme/images/icons16/database.png",click:'importWindow.show();'}),
	    	    	    isc.IButton.create({autoDraw:false,title:'add',width:60,icon:"/theme/images/icons16/add.png",
	    	    	    	click:function() {
	    	    	    		addLocationWindow.show();	    	    	    	
	    	    	    	}
	    	    	    }),
	    	    	    isc.IButton.create({autoDraw:false,title:'delete',width:70,icon:"/theme/images/icons16/remove.png",click:'locationTree.removeSelectedData();'}),
	    	    	    isc.IButton.create({autoDraw:false,title:'save',width:60,icon:"/theme/images/icons16/save.png",click:'save();'})
	    	    	]
	    	    }),
	    	    isc.DynamicForm.create({ID:'repositoryListSearch',width:'100%',margin:0,numCols:3,cellPadding:2,
	    	    	fields:[
	    	    	    {name:'type',type:'select',width:'100',showTitle:false,defaultValue:'building',valueMap:{'building':'Building','floor':'Floor','room':'Room'}},
   	    	    	    {name:'query',width:'195',type:'text',showTitle:false},
	    	    	    {name:'validateBtn',title:'search',type:'button',width:50,startRow:false,endRow:false,
	    	    	    	click:function() {
	    	    	    		query = repositoryListSearch.getValue('query');
	    	    	    		type = repositoryListSearch.getValue('type')
	    	    	    		if(query) query = 'name:'+query+' and type:'+type;
	    	    	    		else query = 'type:'+type;
	    	    	    		locationTree.fetchData({qname:'{openapps.org_repository_1.0}location',query:query});
	    	    	    	}
	    	    	    }
	    	    	]
	    	    }),
	    	    isc.TreeGrid.create({ID:'locationTree',dataSource:'locationDS',height:'100%',width:'100%',showHeader:false,animateFolders:true,
	    	    	canAcceptDroppedRecords:true,canReparentNodes:true,canReorderRecords:true,canDropOnLeaves:true,selectionType:'single',autoDraw:false,
	    	    	dragDataAction:'move',canDragRecordsOut:true,recordClick:'load(record)'
	    	    })	    
	    	]
	    }),
	    isc.TabSet.create({ID:'rightTabSet',tabBarPosition:'top',width:'65%',height:'100%',autoDraw:false,
	    	tabs:[
	    	    {title:'Basic Information',
	    	    	pane:isc.VLayout.create({ID:'mainCanvas',width:'100%',height:'100%',visibility:'hidden',width:'100%',height:'100%',
	    	    		members:[
	    	    		    isc.DynamicForm.create({ID:'locationForm1',dataSource:'accession',width:'50%',height:'50%',margin:'10',cellPadding:5,numCols:'2',colWidths:[50,'*'],visibility:'visible',autoFocus:false,
	    	    		    	fields:[
	    	    		    	    {name:'name',width:'*',required:true,title:'Name'},
	    	    		    	    {name:'type',title:'Type',type:'select',width:'100',align:'left',defaultValue:'building',valueMap:typeValueMap},
	    	    		    	    {name:'space',width:'*',type:'spinner',min:'0',max:'100',width:'75',title:'Space'}
	    	    		    	]
	    	    		    }),
	    	    		    isc.HLayout.create({ID:"spaceViewer",width:"100%",height:50,border:"0px solid black",layoutTopMargin:10,membersMargin:1,members:[]}),
	    	    		    getCollectionItemRelationPanel('100%','100%', true)
	    	    		]
	    	    	})
	    	    },
	    	    {title:'Notes, Activities and Tasks',
	    	    	pane:isc.VLayout.create({ID:"notesAndActivitiesPanel",autoDraw:false,height:'100%',width:'100%',margin:5,membersMargin:5,
	    	    		members:[
	    	    		    getNotesPanel(noteValueMap),
	    	    		 	getActivitiesPanel(activityValueMap),
	    	    		 	getTasksPanel(taskValueMap)
	    	    		]
	    	    	})
	    	    }
	    	]
	    })
	]
});
isc.Window.create({ID:"addLocationWindow",title:"New Location",width:375,height:140,membersMargin:10,autoCenter:true,isModal:true,showModalMask:true,autoDraw:false,
    items: [
	    isc.DynamicForm.create({ID:"addLocationForm",height:'30',width:"100%",numCols:"3",cellPadding:5,autoDraw:false,colWidths:[100,'*'],
	       	fields: [
	       	    {name:'name',title:'Name',width:'300',align:'left'},
	       	    {name:'type',title:'Type',type:'select',width:'150',align:'left',defaultValue:'building',valueMap:typeValueMap},
		       	{name:"submit",title:"Save",type:"button",
	 	       	   	click:function () {
	 	       	   		parms = {};
	 	       	   		parms['name'] = addLocationForm.getValue('name');
	 	       	   		parms['type'] = addLocationForm.getValue('type');
	 	       	   		parms['source'] = entityId;
	 	       	   		locationTree.addData(parms, function(xmlDoc, xmlText) {
	 	       	   			addLocationForm.clearValues();
	 	       	   			addLocationWindow.hide();
	 	       	   		});
	 	       	   	}
		       	}
	     	]
	    })
    ]
});
isc.Window.create({ID:"importWindow",title:"Import Locations",width:1020,height:630,autoCenter:true,isModal:true,showModalMask:true,autoDraw:false,
    items: [
        //getImportApplication()
    ]
});