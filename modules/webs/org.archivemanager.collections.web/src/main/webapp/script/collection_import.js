function populateNodeDS(xmlDoc, xmlText) {
	nodeXml = properties.recordsFromXML(isc.XMLTools.selectNodes(xmlDoc, "//node"));
	isc.ResultSet.create({ID:"nodeRS",dataSource:"node",initialData:nodeXml});
	nodeForm.setData(nodeXml);
	propXml = properties.recordsFromXML(isc.XMLTools.selectNodes(xmlDoc, "//node/properties/property"));
	isc.ResultSet.create({ID:"propRS",dataSource:"properties",initialData:propXml});
	propsList.setData(propXml);
	noteXml = properties.recordsFromXML(isc.XMLTools.selectNodes(xmlDoc, "//node/notes/node"));
	isc.ResultSet.create({ID:"noteRS",dataSource:"notes",initialData:noteXml});
	notesList.setData(noteXml);
	nameXml = properties.recordsFromXML(isc.XMLTools.selectNodes(xmlDoc, "//node/names/node"));
	isc.ResultSet.create({ID:"nameRS",dataSource:"names",initialData:nameXml});
	namesList.setData(nameXml);
	subjectXml = properties.recordsFromXML(isc.XMLTools.selectNodes(xmlDoc, "//node/subjects/node"));
	isc.ResultSet.create({ID:"subjectRS",dataSource:"subjects",initialData:subjectXml});
	subjectsList.setData(subjectXml);
}
function load(record) {
	isc.XMLTools.loadXML("import/fetch.xml?_dataSource=node&id="+record.id, "populateNodeDS(xmlDoc, xmlText)");
}
function saveImport() {
	treeData.addData({repository:uploadForm.getValue('repository'),title:uploadForm.getValue('title')});
}
function getImportApplication() {
	isc.RestDataSource.create({ID:"treeData",fetchDataURL:"import/fetch.xml",addDataURL:"import/add.xml",
	fields:[
		{name:"id", title:"ID", type:"text", primaryKey:true, required:true},
    	{name:"title", title:"Title", type:"text", length:"128", required:true},
    	{name:"parent", title:"ParentID", type:"text", required:true, foreignKey:"treeData.id", rootValue:"null"}
	]	
	});
	isc.DataSource.create({ID:"node"});
	isc.DataSource.create({ID:"properties"});
	isc.DataSource.create({ID:"notes"});
	isc.DataSource.create({ID:"names"});
	isc.DataSource.create({ID:"subjects"});
	isc.DataSource.create({ID:"containers"});
	isc.RestDataSource.create({ID:"repositories",fetchDataURL:"fetch.xml"});

	isc.TreeGrid.create({
	ID:"importTree",
	dataSource:"treeData",
	width: 350,height: "100%",
	nodeClick:"load(node)",
	autoFetchData: false,
	showHeader:false,
	showResizeBar: true,
	leaveScrollbarGap:false,
	animateFolders:true,
	canAcceptDroppedRecords:true,
	canReparentNodes:true,
	canReorderRecords:true,
	canDropOnLeaves:true,
	selectionType:"single",
	//sortFieldNum:2,
	//sortDirection:"descending",
	autoDraw:false,
	dataProperties: {
        dataArrived: function(){
			var node = this.find('id','0');                
            this.openFolder(node);                                
        }
    }
	});
	UploadForm.create({ID:"uploadForm",action:"import/upload.xml",numCols:4,cellPadding:5,
	  fields: [
	    {name:"repository",title:"Respository",width:"*",type:"select",displayField:"full_name",valueField:"id",valueMap:repositories,
	    	changed : function(form,item,value) {
	    		//createCookie('import.tree.repo',value,30);
	    	}
	    },
	    {name: "mode",title:"Processor",type:"select",valueMap:modesData},
	    {name: "title",title:"Title",type:"text",width:"*"},
	    {name: "file",title:"File",type:"UploadItem", height:35}//,
	    //{type: "submit"}
	  ]
	});
	//Main Body
	isc.TabSet.create({ID:"tabSet",tabBarPosition:"top",width:"60%",height:"100%",autoDraw:false,
    tabs: [
        {title: "Properties", pane: 
        	isc.VLayout.create({ID:"basicDescription",width:"100%", height:"100%",autoDraw:false,children:[
        	    isc.DynamicForm.create({ID:"nodeForm",
        	    	fields:[
        	    	    {name:"localName",title:"Type",type:"staticText"},
        	    	]
        	    }),
        	    isc.ListGrid.create({ID:"propsList",dataSource:"properties",width:"100%",height:"100%",alternateRecordStyles:true,autoDraw:false,wrapCells:true,fixedRecordHeights:false,sortFieldNum:0,autoFetchData:false,
        	    	fields:[
        	    	    {name:"name",title:"Name",width:250},
        	    	    {name:"value",title:"Value"}
        	    	]//,
        	    	//recordClick:"nodesList.fetchData({id:record.id})"
        	    })
        	]})
        },
        {title: "Notes", pane: 
        	isc.ListGrid.create({ID:"notesList",dataSource:"notes",width:"100%",height:"100%",alternateRecordStyles:true,autoDraw:false,wrapCells:true,fixedRecordHeights:false,sortFieldNum:0,autoFetchData:false,
    	    	fields:[
    	    	    {name:"name",title:"Name",width:250},
    	    	    {name:"value",title:"Value"}
    	    	]//,
    	    	//recordClick:"nodesList.fetchData({id:record.id})"
    	    })
        },
        {title: "Names", pane: 
        	isc.ListGrid.create({ID:"namesList",dataSource:"names",width:"100%",height:"100%",alternateRecordStyles:true,autoDraw:false,wrapCells:true,fixedRecordHeights:false,sortFieldNum:0,autoFetchData:false,
    	    	fields:[
    	    	    {name:"name",title:"Name",width:250},
    	    	    {name:"value",title:"Value"}
    	    	]//,
    	    	//recordClick:"nodesList.fetchData({id:record.id})"
    	    })
        },
        {title: "Subjects", pane: 
        	isc.ListGrid.create({ID:"subjectsList",dataSource:"subjects",width:"100%",height:"100%",alternateRecordStyles:true,autoDraw:false,wrapCells:true,fixedRecordHeights:false,sortFieldNum:0,autoFetchData:false,
    	    	fields:[
    	    	    {name:"name",title:"Name",width:250},
    	    	    {name:"value",title:"Value"}
    	    	]//,
    	    	//recordClick:"nodesList.fetchData({id:record.id})"
    	    })
        },
    ]
	});
	isc.IButton.create({ID:"saveButton",title:"Save",click:"saveImport();"});
	isc.IButton.create({ID:"submitButton",title:"Process",click:"uploadForm.submit();"});
	isc.HLayout.create({ID:"menuLayout",width:"100%",height:25,members:[submitButton,saveButton]});
	isc.HLayout.create({ID:"pageLayout1",width:"100%",height:"100%",members:[importTree,tabSet]});
	return isc.VLayout.create({ID:"importPageLayout",width:1000,height:600,margin:5,membersMargin:5,members:[uploadForm,menuLayout,pageLayout1]});
}