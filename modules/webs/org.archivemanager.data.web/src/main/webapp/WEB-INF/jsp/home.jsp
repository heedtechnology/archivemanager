<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script>
	var mapping_processors = {'none':'Not Included','text':'Text/String','number':'Number','relation':'Relationship'};
	var file_formats = {'spreadsheet':'Spreadsheet','oaxml':'OpenAppsXML'}
	
	var service_path = '<c:out value="${openappsUrl}" />';
</script>

<script src="/theme/script/sc/modules/ISC_Core.js" language="javascript"></script>
<script src="/theme/script/sc/modules/ISC_Foundation.js" language="javascript"></script>
<script src="/theme/script/sc/modules/ISC_Containers.js" language="javascript"></script>
<script src="/theme/script/sc/modules/ISC_Grids.js" language="javascript"></script>
<script src="/theme/script/sc/modules/ISC_Forms.js" language="javascript"></script>
<script src="/theme/script/sc/modules/ISC_RichTextEditor.js" language="javascript"></script>
<script src="/theme/script/sc/modules/ISC_Calendar.js" language="javascript"></script>
<script src="/theme/script/sc/modules/ISC_DataBinding.js" language="javascript"></script>
<script src="/theme/script/sc/skins/Enterprise/load_skin.js" language="javascript"></script>

<link href="/theme/script/sc/skins/Enterprise/skin_styles.css" type="text/css" rel="stylesheet">

<iframe src="javascript:''" id="__gwt_historyFrame" tabIndex='-1' style="position:absolute;width:0;height:0;border:0"></iframe>
<div id="gwt" style="width:100%;margin-bottom:10px;min-height:500px;"></div>
<script type="text/javascript" language="javascript" src="EntityManager/EntityManager.nocache.js"></script>