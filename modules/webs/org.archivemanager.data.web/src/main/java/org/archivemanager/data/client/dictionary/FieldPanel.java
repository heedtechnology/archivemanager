package org.archivemanager.data.client.dictionary;

import java.util.ArrayList;
import java.util.List;

import org.archivemanager.data.client.EntityEventTypes;
import org.archivemanager.data.client.form.FieldForm;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.component.Toolbar;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.layout.VLayout;


public class FieldPanel extends VLayout {
	private DynamicForm form;
	private ListGrid list;
	
	
	public FieldPanel() {
		setWidth100();
		setHeight100();
		
		//setBorder("1px solid #A7ABB4");
		
		Canvas header = new Canvas();
		header.setWidth100();
		header.setHeight(15);
		addMember(header);
		
		form = new FieldForm(false, true);
		form.setWidth("75%");
		addMember(form);
		
		VLayout valuePanel = new VLayout();
		valuePanel.setWidth100();
		valuePanel.setHeight100();
		valuePanel.setMargin(5);
		valuePanel.setMembersMargin(1);
		addMember(valuePanel);
		
		Toolbar toolbar = new Toolbar(30);
		toolbar.setBorder("1px solid #A7ABB4");
		valuePanel.addMember(toolbar);
		
		toolbar.addButton("add", "/theme/images/icons32/add.png", "Add", new com.smartgwt.client.widgets.events.ClickHandler() {  
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				EventBus.fireEvent(new OpenAppsEvent(EntityEventTypes.CREATE_FIELD_VALUE));
			}  
		});
		toolbar.addButton("delete", "/theme/images/icons32/delete.png", "Delete", new com.smartgwt.client.widgets.events.ClickHandler() {  
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				EventBus.fireEvent(new OpenAppsEvent(EntityEventTypes.DELETE_FIELD_VALUE));
			}  
		});
		
		list = new ListGrid();
		list.setWidth100();
		list.setHeight100();
		valuePanel.addMember(list);
		
		List<ListGridField> items = new ArrayList<ListGridField>();		
		ListGridField columnA = new ListGridField("label", "Label");
		items.add(columnA);
		ListGridField columnB = new ListGridField("value", "Value");
		items.add(columnB);
		ListGridField[] fitems = new ListGridField[items.size()];
		list.setFields(items.toArray(fitems));
	}
	
	public void select(Record record) {
		form.editRecord(record);
		Record[] values = record.getAttributeAsRecordArray("values");
		list.setData(values);
		show();
	}
}