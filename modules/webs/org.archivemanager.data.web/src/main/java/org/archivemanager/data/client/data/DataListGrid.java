package org.archivemanager.data.client.data;
import java.util.ArrayList;
import java.util.List;

import org.heed.openapps.gwt.client.event.SelectionListener;

import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;

public class DataListGrid extends ListGrid {
		
	private SelectionListener selectionListener;
	
	
	public DataListGrid() {
		setHeight100();
		setWidth100();
		
		setShowHeader(false);
		addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				//EventBus.fireEvent(new OpenAppsEvent(EntityEventTypes.MODEL_SELECT, event.getRecord()));
				selectionListener.select(event.getRecord());
			}			
		});
        
		List<ListGridField> items = new ArrayList<ListGridField>();		
		ListGridField columnA = new ListGridField("title", "Title");
		items.add(columnA);		
		ListGridField[] fitems = new ListGridField[items.size()];
		setFields(items.toArray(fitems));
		
	}

	public void setSelectionListener(SelectionListener selectionListener) {
		this.selectionListener = selectionListener;
	}	
}
