package org.archivemanager.data.client.dictionary;

import org.archivemanager.data.client.EntityEventTypes;
import org.archivemanager.data.client.form.ModelForm;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.OpenAppsEventHandler;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;

public class ModelPanel extends VLayout {
	private ModelForm editForm;
	private FieldPanel fieldPanel;
	private RelationPanel relationPanel;
	private TabSet list;
	private RelationList relationList;
	private FieldList fieldList;
	
	
	public ModelPanel() {
		setWidth100();
		setHeight100();
		setMembersMargin(1);
		
		HLayout row1 = new HLayout();
		row1.setWidth100();
		//row1.setHeight(200);
		row1.setBorder("1px solid #A7ABB4");
		addMember(row1);
		
		editForm = new ModelForm(false, true);
		editForm.setWidth(500);
		editForm.setHeight100();
		row1.addMember(editForm);
						
		HLayout bottomLayout = new HLayout();
		bottomLayout.setWidth100();
		bottomLayout.setHeight100();
		addMember(bottomLayout);
		
		list = new TabSet();
		list.setWidth(275);
		bottomLayout.addMember(list);
		
		fieldList = new FieldList();
		Tab fieldsTab = new Tab("Fields");
		fieldsTab.setPane(fieldList);
		list.addTab(fieldsTab);
		
		relationList = new RelationList();
		Tab relationsTab = new Tab("Relations");
		relationsTab.setPane(relationList);
		list.addTab(relationsTab);
		
		Canvas mainLayout = new Canvas();
		bottomLayout.addMember(mainLayout);
					
		fieldPanel = new FieldPanel();
		fieldPanel.setVisible(false);
		mainLayout.addChild(fieldPanel);
		
		relationPanel = new RelationPanel();
		relationPanel.setVisible(false);
		mainLayout.addChild(relationPanel);
		
		EventBus.addHandler(org.heed.openapps.gwt.client.OpenAppsEvent.TYPE, new OpenAppsEventHandler() {
	        @Override
	        public void onEvent(OpenAppsEvent event) {
	        	if(event.isType(EntityEventTypes.FIELD_SELECTION)) {
	        		relationPanel.hide();
	        		fieldPanel.show();
	        		fieldPanel.select(event.getRecord());
	        	} else if(event.isType(EntityEventTypes.RELATION_SELECTION)) {
	        		fieldPanel.hide();
	        		relationPanel.show();
	        		relationPanel.select(event.getRecord());
	        	} 
	        }
		});
	}
	
	public void select(Record record) {
		editForm.editRecord(record);
		fieldList.select(record);
		relationList.select(record);
		show();
	}
	public Record getData() {
		return editForm.getValuesAsRecord();
	}
}
