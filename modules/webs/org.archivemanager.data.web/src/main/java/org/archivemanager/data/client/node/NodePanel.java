package org.archivemanager.data.client.node;


import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class NodePanel extends VLayout {
	private VLayout propertyLayout;
	private VLayout relationLayout;
	private ListGrid outgoingList;
	private ListGrid incomingList;
	
	
	public NodePanel() {
		setWidth("50%");
		setMinHeight(200);
		setBorder("1px solid #A7ABB4");
				
		HLayout mainLayout = new HLayout();
		mainLayout.setWidth100();
		mainLayout.setWidth100();
		addMember(mainLayout);	
		
		propertyLayout = new VLayout();
		propertyLayout.setWidth100();
		propertyLayout.setHeight100();
		propertyLayout.setOverflow(Overflow.AUTO);
		mainLayout.addMember(propertyLayout);
		
		relationLayout = new VLayout();
		relationLayout.setWidth100();
		relationLayout.setWidth100();
		mainLayout.addMember(relationLayout);
		
		Label outgoingLabel = new Label("<label style='font-size:11px;font-weight:bold;color:#4C4C4C;font-family:Arial,Verdana,sans-serif;'>Outgoing Relationships</label>");
		outgoingLabel.setHeight(25);
		relationLayout.addMember(outgoingLabel);
		
		outgoingList = new ListGrid();
		ListGridField idField = new ListGridField("id", "ID", 60);
		ListGridField startField = new ListGridField("start", "Start", 60);
		ListGridField endField = new ListGridField("end", "End", 60);
		ListGridField typeField = new ListGridField("type", "Type");
		typeField.setWidth("*");
		outgoingList.setFields(idField,startField,endField,typeField);
		relationLayout.addMember(outgoingList);
		
		Label incomingLabel = new Label("<label style='font-size:11px;font-weight:bold;color:#4C4C4C;font-family:Arial,Verdana,sans-serif;'>Incoming Relationships</label>");
		incomingLabel.setHeight(25);
		relationLayout.addMember(incomingLabel);
		
		incomingList = new ListGrid();
		ListGridField idField2 = new ListGridField("id", "ID", 60);
		ListGridField startField2 = new ListGridField("start", "Start", 60);
		ListGridField endField2 = new ListGridField("end", "End", 60);
		ListGridField typeField2 = new ListGridField("type", "Type");
		typeField.setWidth("*");
		incomingList.setFields(idField2,startField2,endField2,typeField2);
		relationLayout.addMember(incomingList);
	}
	
	public void select(Record record) {
		clearValues();
		try {
			addEntry("id", record.getAttributeAsString("id"));
			addEntry("uid", record.getAttributeAsString("uid"));
			addEntry("created", record.getAttributeAsString("created"));
			addEntry("modified", record.getAttributeAsString("modified"));
			addEntry("deleted", record.getAttributeAsString("deleted"));
			addEntry("children", record.getAttributeAsString("children"));
			for(String key : record.getAttributes()) {
				if(key.equals("outgoing")) {
					outgoingList.setData(record.getAttributeAsRecordArray("outgoing"));				
				} else if(key.equals("incoming")) {
					incomingList.setData(record.getAttributeAsRecordArray("incoming"));
				} else if(!key.equals("id") && !key.equals("uid") && !key.equals("created") && 
						!key.equals("modified") && !key.equals("deleted") && !key.equals("children")){
					addEntry(key, record.getAttributeAsString(key));
				}
			}			
		} catch(ClassCastException e) {
			
		}
	}
	protected void addEntry(String title, String value) {
		Label close = new Label("<label style=''>"+title+" : "+value+"</label>");
		//close.setBorder("1px solid #a8c298");
		close.setWidth100();
		close.setHeight(25);
		close.setMargin(2);
		//close.setAlign(Alignment.RIGHT);
		close.setValign(VerticalAlignment.CENTER);
		propertyLayout.addMember(close);
	}
	public void clearValues() {
		for(Canvas comp : propertyLayout.getChildren()) {
			propertyLayout.removeChild(comp);
		}
	}
}
