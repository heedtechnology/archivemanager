package org.archivemanager.data.client.dictionary;

import java.util.ArrayList;
import java.util.List;

import org.archivemanager.data.client.form.RelationForm;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.layout.VLayout;


public class RelationPanel extends VLayout {
	private RelationForm form;
	private ListGrid list;
	
	
	public RelationPanel() {
		setWidth100();
		setHeight100();
		
		Canvas header = new Canvas();
		header.setWidth100();
		header.setHeight(15);
		addMember(header);
		
		form = new RelationForm(false, true);
		form.setWidth(500);
		form.setHeight100();
		form.setMargin(5);	
		addMember(form);	
		
		list = new ListGrid();
		list.setWidth100();
		list.setHeight100();
		list.setMargin(10);
		addMember(list);
		
		List<ListGridField> items = new ArrayList<ListGridField>();		
		ListGridField columnA = new ListGridField("label", "Label");
		items.add(columnA);
		ListGridField columnB = new ListGridField("value", "Value");
		items.add(columnB);
		ListGridField[] fitems = new ListGridField[items.size()];
		list.setFields(items.toArray(fitems));
	}
	
	public void select(Record record) {
		form.editRecord(record);
		Record[] values = record.getAttributeAsRecordArray("values");
		list.setData(values);
		show();
	}
}