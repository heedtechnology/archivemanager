package org.archivemanager.data.client.data;
import java.util.ArrayList;
import java.util.List;

import org.archivemanager.data.client.EntityEventTypes;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.component.Toolbar;
import org.heed.openapps.gwt.client.data.EntityServiceDS;

import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONObject;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.SortSpecifier;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.types.SortDirection;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class SearchResultGrid extends VLayout {
	private Toolbar toolbar2;
	private ListGrid grid;
	private ListGrid propertyGrid;
	
	
	public SearchResultGrid() {		
		grid = new ListGrid(){
			@Override  
            protected Canvas createRecordComponent(final ListGridRecord record, Integer colNum) {   
                String fieldName = this.getFieldName(colNum);    
                if(fieldName.equals("iconField")) { 
                	HLayout recordCanvas = new HLayout(3);  
                    recordCanvas.setHeight(22);
                    recordCanvas.setWidth100(); 
                    recordCanvas.setAlign(Alignment.CENTER);
                	ImgButton chartImg = new ImgButton();  
                    chartImg.setShowDown(false);  
                    chartImg.setShowRollOver(false);  
                    chartImg.setAlign(Alignment.CENTER);  
                    chartImg.setSrc("/theme/images/icons16/delete.png");  
                    chartImg.setPrompt("Delete");  
                    chartImg.setHeight(16);  
                    chartImg.setWidth(16);  
                    chartImg.addClickHandler(new ClickHandler() {  
                        public void onClick(ClickEvent event) {
                        	EventBus.fireEvent(new OpenAppsEvent(EntityEventTypes.DELETE_ENTITY, record));
                        }  
                    });
                    recordCanvas.addMember(chartImg);
                    return recordCanvas;
                } else return null;
			}
		};		
		grid.setWidth100();
		grid.setHeight100();
		grid.setWrapCells(true);
		grid.setCanSelectAll(true);
		grid.setShowRecordComponents(true);          
        grid.setShowRecordComponentsByCell(true);
		grid.setSelectionType(SelectionStyle.SIMPLE);  
        grid.setSelectionAppearance(SelectionAppearance.CHECKBOX);
		grid.setSort(new SortSpecifier[]{new SortSpecifier("name", SortDirection.ASCENDING),new SortSpecifier("id", SortDirection.ASCENDING)});
		List<ListGridField> items = new ArrayList<ListGridField>();
		
		ListGridField columnB = new ListGridField("id", "ID", 50);
		columnB.setAlign(Alignment.CENTER);
		items.add(columnB);
		
		ListGridField columnC = new ListGridField("uid", "UID", 230);
		items.add(columnC);
		
		ListGridField columnA = new ListGridField("name", "Name");
		items.add(columnA);		
		
		ListGridField iconField = new ListGridField("iconField", "");  
        iconField.setWidth(30); 
        iconField.setShowTitle(false);
        items.add(iconField);
        
		ListGridField[] fitems = new ListGridField[items.size()];
		grid.setFields(items.toArray(fitems));
		
		grid.addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				ListGridRecord record = event.getRecord();
				select(record);
			}			
		});
		grid.setShowResizeBar(true);
		addMember(grid);
				
		toolbar2 = new Toolbar(30);
		toolbar2.setWidth100();
		toolbar2.setMargin(1);
		toolbar2.setBorder("1px solid #A7ABB4");
		addMember(toolbar2);
		
		propertyGrid = new ListGrid();
		propertyGrid.setWidth100();
		propertyGrid.setHeight100();
		propertyGrid.setWrapCells(true);
		propertyGrid.setFixedRecordHeights(false);
		propertyGrid.setShowHeader(false);
		ListGridField propertyKey = new ListGridField("name", "Name", 250);				
		ListGridField propertyValue = new ListGridField("value", "Value");
		propertyGrid.setFields(propertyKey, propertyValue);
		addMember(propertyGrid);
		
	}
	
	public ListGridRecord[] getSelections() {
		return grid.getSelectedRecords();
	}
	public void search(Record record) {
		Criteria criteria = new Criteria();
		for(String key : record.getAttributes()) {
			criteria.addCriteria(key, record.getAttribute(key));
		}
		EntityServiceDS.getInstance().search(criteria, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				grid.setData(response.getData());
			}			
		});		
	}
	public void select(Record record) {
		List<ListGridRecord> properties = new ArrayList<ListGridRecord>();
		try {
			for(String key : record.getAttributes()) {
				if(!key.equals("source_associations") && !key.equals("target_associations") && !key.equals("id") && !key.equals("uid") && !key.equals("name") && !key.equals("title") && 
					!key.equals("localName") && !key.startsWith("_") && !key.equals("qname") && !key.equals("deleted")) {
					ListGridRecord property = new ListGridRecord();
					property.setAttribute("name", key);
					property.setAttribute("value", record.getAttributeAsString(key));
					properties.add(property);
				}
			}
			JSONObject json = new JSONObject(record.getJsObj());
			JSONObject source_associations = (JSONObject)json.get("source_associations");
			String sourceLabel = "source associations";
			if(source_associations != null) {
				for(String key2 : source_associations.keySet()) {
				//System.out.println("key:"+key);
				JSONObject relationship = (JSONObject)source_associations.get(key2);
				JSONArray nodes = relationship.get("node").isArray();
				for(int i=0; i < nodes.size(); i++) {
					JSONObject node = (JSONObject)nodes.get(i);
					ListGridRecord property = new ListGridRecord();
					property.setAttribute("name", sourceLabel);
					property.setAttribute("value", "("+key2+") "+node.get("name"));
					properties.add(property);
					sourceLabel = "";
				}
				}
			}
			JSONObject target_associations = (JSONObject)json.get("target_associations");
			String targetLabel = "target associations";
			if(target_associations != null) {
				for(String key2 : target_associations.keySet()) {
				//System.out.println("key:"+key);
				JSONObject relationship = (JSONObject)source_associations.get(key2);
				JSONArray nodes = relationship.get("node").isArray();
				for(int i=0; i < nodes.size(); i++) {
					JSONObject node = (JSONObject)nodes.get(i);
					ListGridRecord property = new ListGridRecord();
					property.setAttribute("name", targetLabel);
					property.setAttribute("value", node.get("name"));
					properties.add(property);
					targetLabel = "";
				}
				}
			}
		} catch(ClassCastException e) {
			e.printStackTrace();
		}
		propertyGrid.setData(properties.toArray(new ListGridRecord[properties.size()]));
	}
	
}
