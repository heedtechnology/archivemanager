package org.archivemanager.data.client.data.cleaning;

import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.RadioGroupItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.layout.VLayout;

public class CleaningChoiceWizardView extends VLayout {
	private DynamicForm form;
	
	public CleaningChoiceWizardView(final CleaningWizardPanel wizard) {
		setWidth100();
		setHeight100();
		hide();
		
		Label label = new Label("<div style='font-weight:bold;font-size:13px;margin:10px;'>What type of cleaning?</div>");
		label.setWidth100();
		label.setHeight(30);
		addMember(label);
		
		form = new DynamicForm();
		form.setWidth100();
		form.setNumCols(2);
		RadioGroupItem radioGroupItem = new RadioGroupItem("cleaningType");  
        radioGroupItem.setShowTitle(false);  
        radioGroupItem.setValueMap("Merge To First", "Merge To Last");
        radioGroupItem.addChangedHandler(new ChangedHandler() {
			@Override
			public void onChanged(ChangedEvent event) {
				wizard.hideAll();
				wizard.getPane2().show();
				wizard.getView2().show();
			}	        	
        });
		form.setFields(radioGroupItem);
		addMember(form);
	}
	public String getValue() {
		return String.valueOf(form.getValue("cleaningType"));
				
	}
}