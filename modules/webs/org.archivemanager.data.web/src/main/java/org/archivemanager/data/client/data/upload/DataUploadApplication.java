package org.archivemanager.data.client.data.upload;
import java.util.LinkedHashMap;

import org.archivemanager.data.client.EntityEventTypes;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.EventTypes;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.OpenAppsEventHandler;
import org.heed.openapps.gwt.client.event.UploadListener;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class DataUploadApplication extends VLayout {
	private DataMappingPanel mapper;
	private UploadSelectionPanel uploadSelectionPanel;
	private DataGrid grid;
	
	private LinkedHashMap<String,Object> processors = new LinkedHashMap<String, Object>();
	private LinkedHashMap<String,Object> fields = new LinkedHashMap<String, Object>();
	private LinkedHashMap<String,Object> relationships = new LinkedHashMap<String, Object>();
	
	private String qname;
	private String sessionKey;
	
	public DataUploadApplication() {
		setWidth100();
		setHeight100();
				
		HLayout mainLayout = new HLayout();
		mainLayout.setWidth100();
		mainLayout.setHeight(320);
		mainLayout.setMembersMargin(5);
		addMember(mainLayout);
		
		uploadSelectionPanel = new UploadSelectionPanel();
		uploadSelectionPanel.setWidth(470);
		uploadSelectionPanel.addUploadListener(new UploadListener() {
			public void uploadComplete(String key) {
				sessionKey = key;
				Record criteria = new Record();
				criteria.setAttribute("session", sessionKey);
				grid.fetchData(criteria, new DSCallback() {
					@Override
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						EventBus.fireEvent(new OpenAppsEvent(EventTypes.MESSAGE, "Collection Loaded Successfully"));
					}					
				});
			}
		});
		mainLayout.setShowResizeBar(true);
		mainLayout.addMember(uploadSelectionPanel);
		
		mapper = new DataMappingPanel();
		mainLayout.addMember(mapper);
		
		grid = new DataGrid();
		addMember(grid);
		
		EventBus.addHandler(org.heed.openapps.gwt.client.OpenAppsEvent.TYPE, new OpenAppsEventHandler() {
	        @Override
	        public void onEvent(OpenAppsEvent event) {
	        	if(event.isType(EntityEventTypes.UPLOAD_ENTITY)) {
	        		Record record = mapper.getData();
	        		record.setAttribute("qname", qname);
	        		uploadSelectionPanel.submitUpload(record);	 
	        	} else if(event.isType(EntityEventTypes.SAVE_UPLOADED_DATA)) {
	        		Record record = new Record();
	        		record.setAttribute("session", sessionKey);
	        		grid.saveData(record, new DSCallback() {
						@Override
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							EventBus.fireEvent(new OpenAppsEvent(EntityEventTypes.MESSAGE, "Data saved successfully."));
						}	        			
	        		});	        		
	        	} else if(event.isType(EntityEventTypes.MODEL_SELECT)) {
	        		Criteria criteria = new Criteria();
	        		criteria.addCriteria("id", event.getRecord().getAttribute("id"));
	        		/*
	        		DictionaryServiceDS.getInstance().getModel(criteria, new DSCallback() {
	        			@SuppressWarnings("unchecked")
	        			@Override
	        			public void execute(DSResponse response, Object rawData, DSRequest request) {
	        				if(response.getData() != null && response.getData().length > 0) {
	        					Record rec = response.getData()[0];
	        					Map<String,Object> qnameMap = rec.getAttributeAsMap("qname");
	        					if(qnameMap != null) {
	        						rec.setAttribute("namespace", qnameMap.get("namespace"));
	        						rec.setAttribute("localName", qnameMap.get("localName"));
	        						qname = "{"+qnameMap.get("namespace")+"}"+qnameMap.get("localName");
	        					}
	        					processors.clear();
	        					Record[] processorRecords = rec.getAttributeAsRecordArray("processors");
	        					for(int i=0; i < processorRecords.length; i++) {
	        						Record processor = processorRecords[i];
	        						processors.put("processor"+i, processor.getAttribute("name"));
	        						
	        					}
	        					Record[] fieldRecords = rec.getAttributeAsRecordArray("fields");
	        					for(Record field : fieldRecords) {
	        						Map<String,Object> fieldQname = field.getAttributeAsMap("qname");
	        						if(fieldQname != null) {
	        							field.setAttribute("namespace", fieldQname.get("namespace"));
	        							field.setAttribute("localName", fieldQname.get("localName"));
	        							fields.put((String)fieldQname.get("localName"), fieldQname.get("localName"));
	        						}
	        					}
	        					Record[] relationRecords = rec.getAttributeAsRecordArray("relations");									
	        					for(Record relation : relationRecords) {
	        						Map<String,Object> relationQname = relation.getAttributeAsMap("qname");
	        						if(relationQname != null) {
	        							relation.setAttribute("namespace", relationQname.get("namespace"));
	        							relation.setAttribute("localName", relationQname.get("localName"));
	        						}
	        						Map<String,Object> startQname = relation.getAttributeAsMap("startName");
	        						if(startQname != null) {
	        							relation.setAttribute("startNamespace", startQname.get("namespace"));
	        							relation.setAttribute("startlocalName", startQname.get("localName"));
	        						}
	        						Map<String,Object> endQname = relation.getAttributeAsMap("endName");
	        						if(endQname != null) {
	        							relation.setAttribute("endNamespace", endQname.get("namespace"));
	        							relation.setAttribute("endlocalName", endQname.get("localName"));
	        						}
	        						relationships.put((String)relationQname.get("localName"), relationQname.get("localName"));
	        					}	        					
	        				}
	        			}	        			
	        		});
	        		*/
	        		mapper.setFields(fields);
	        		mapper.setRelationships(relationships);
	        		uploadSelectionPanel.setProcessors(processors);
	        	}
	        }
		});
	}    
}
