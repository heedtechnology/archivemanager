package org.archivemanager.data.client.component;

import java.util.ArrayList;
import java.util.List;

import org.heed.openapps.gwt.client.data.DictionaryServiceDS;
import org.heed.openapps.gwt.client.event.SelectionListener;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.widgets.HTMLFlow;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.events.ClickHandler;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;


public class EntityManagerList extends ListGrid {
	private Menu fullContextMenu;
	private Menu emptyContextMenu;
	//private AboutWindow aboutWindow;
	
	private SelectionListener selectionListener;
	
	
	public EntityManagerList() {
		setHeight100();
		setWidth100();
		setShowHeader(false);
		setWrapCells(true);
		setFixedRecordHeights(false);
		setCanReorderRecords(true);  
        setCanAcceptDroppedRecords(true);
		RestDataSource ds = DictionaryServiceDS.getInstance().getDictionaryDatasource();
        setDataSource(ds);
        
        List<ListGridField> items = new ArrayList<ListGridField>();
		
		ListGridField columnA = new ListGridField("title", "Title");
		items.add(columnA);
		
		ListGridField[] fitems = new ListGridField[items.size()];
		setFields(items.toArray(fitems));
        
        addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				Record record = event.getRecord();
				//String type = record.getAttribute("type");
				//if(type.equals("namespace")) setContextMenu(fullContextMenu);
				//else setContextMenu(emptyContextMenu);
				selectionListener.select(record);
			}			
		});
        
        MenuItem aboutItem = new MenuItem("About Entity Manager");
        aboutItem.addClickHandler(new ClickHandler() {
        	public void onClick(MenuItemClickEvent event) {
        		//aboutWindow.show();
			}
        });
        MenuItem delItem = new MenuItem("Delete Namespace");
        delItem.addClickHandler(new ClickHandler() {
        	public void onClick(MenuItemClickEvent event) {
        		removeSelectedData();
			}
        });
        emptyContextMenu = new Menu();
        emptyContextMenu.setItems(aboutItem);
        fullContextMenu = new Menu();
        fullContextMenu.setItems(delItem,aboutItem);
        setContextMenu(emptyContextMenu);
        
        //aboutWindow = new AboutWindow();
	}
	
	public class AboutWindow extends Window {
		
		public AboutWindow() {
			setTitle("About The Entity Manager");
			setWidth(300);
			setHeight(150);
			setAutoCenter(true);
			
			HTMLFlow flow = new HTMLFlow();
			flow.setMargin(12);
			flow.setContentsURL("/administration/entity/about");
			addItem(flow);
		}
	}

	public void setSelectionListener(SelectionListener selectionListener) {
		this.selectionListener = selectionListener;
	}
	
}
