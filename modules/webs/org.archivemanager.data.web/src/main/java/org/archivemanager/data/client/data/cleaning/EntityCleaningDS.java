package org.archivemanager.data.client.data.cleaning;

import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.DSDataFormat;


public class EntityCleaningDS extends RestDataSource {
	private static EntityCleaningDS instance = null;  
	  
    public static EntityCleaningDS getInstance() {  
        if (instance == null) {  
            instance = new EntityCleaningDS("entityCDS");  
        }  
        return instance;  
    }
    
	public EntityCleaningDS(String id) {
		setDataFormat(DSDataFormat.JSON);
		setID(id);  
        setTitleField("Title");	        
        DataSourceTextField nameField = new DataSourceTextField("title", "Title");     
        
        DataSourceTextField idField = new DataSourceTextField("id", "ID",50);  
        idField.setPrimaryKey(true);  
        idField.setRequired(true);
                
        setFields(idField,nameField);
        
        setFetchDataURL("/service/entity/clean.json");
        //setAddDataURL("/service/entity/dictionary/model/add.json"); 
        //setRemoveDataURL("/service/entity/dictionary/model/remove.json");
        setUpdateDataURL("/service/entity/clean.json");
	}
	
}
