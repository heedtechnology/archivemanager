package org.archivemanager.data.client.data.upload;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class DataMappingPanel extends VLayout {
	private NativeEntityModel model = new NativeEntityModel();
	private DynamicForm form;
	
	private SelectItem selection1;
	private SelectItem selection1a;
	private CheckboxItem selection1b;
	
	private SelectItem selection2;
	private SelectItem selection2a;
	private CheckboxItem selection2b;
	
	private SelectItem selection3;
	private SelectItem selection3a;
	private CheckboxItem selection3b;
	
	private SelectItem selection4;
	private SelectItem selection4a;
	private CheckboxItem selection4b;
	
	private SelectItem selection5;
	private SelectItem selection5a;
	private CheckboxItem selection5b;
	
	private SelectItem selection6;
	private SelectItem selection6a;
	private CheckboxItem selection6b;
	
	private SelectItem selection7;
	private SelectItem selection7a;
	private CheckboxItem selection7b;
	
	private SelectItem selection8;
	private SelectItem selection8a;
	private CheckboxItem selection8b;
	
	private SelectItem selection9;
	private SelectItem selection9a;
	private CheckboxItem selection9b;
	
	private SelectItem selection10;
	private SelectItem selection10a;
	private CheckboxItem selection10b;
	
	private List<FormItem> items = new ArrayList<FormItem>();
	
	
	public DataMappingPanel() {
		setWidth100();
		setHeight100();
		setOverflow(Overflow.AUTO);
		setBorder("1px solid #A7ABB4");
		
		HLayout header = new HLayout();
		header.setWidth100();
		addMember(header);
		
		Label label = new Label("<div style='padding-left:55px;font-weight:bold;font-size:13px;margin:5px;text-align:center;'>Data Type</div>");
		label.setWidth100();
		label.setHeight(20);
		header.addMember(label);
		
		Label label2 = new Label("<div style='padding-left:45px;font-weight:bold;font-size:13px;margin:5px;text-align:center;'>Field/Relationship</div>");
		label2.setWidth100();
		label2.setHeight(20);
		header.addMember(label2);		
		
		Label label3 = new Label("<div style='padding-left:20px;font-weight:bold;font-size:13px;margin:5px;'>Allow Nulls</div>");
		label3.setWidth100();
		label3.setHeight(20);
		header.addMember(label3);
		/*
		Label label4 = new Label("<div style='font-weight:bold;font-size:13px;margin:5px;text-align:center;'></div>");
		label4.setWidth100();
		label4.setHeight(20);
		header.addMember(label4);
		*/
		form = new DynamicForm();
		form.setWidth100();
		form.setHeight100();
		form.setColWidths("70","130","150","*");
		form.setCellPadding(3);
		form.setNumCols(4);
				
		selection1 = new SelectItem("atype","Column A");
		items.add(selection1);
		selection1a = new SelectItem("afield");
		selection1a.setShowTitle(false);
		items.add(selection1a);
		selection1b = new CheckboxItem("nulls1");
		selection1b.setShowLabel(false);
		items.add(selection1b);
		
		selection2 = new SelectItem("btype","Column B");
		items.add(selection2);
		selection2a = new SelectItem("bfield");
		selection2a.setShowTitle(false);
		items.add(selection2a);
		selection2b = new CheckboxItem("nulls2");
		selection2b.setShowLabel(false);
		items.add(selection2b);
		
		selection3 = new SelectItem("ctype","Column C");
		items.add(selection3);
		selection3a = new SelectItem("cfield");
		selection3a.setShowTitle(false);
		items.add(selection3a);
		selection3b = new CheckboxItem("nulls3");
		selection3b.setShowLabel(false);
		items.add(selection3b);
		
		selection4 = new SelectItem("dtype","Column D");
		items.add(selection4);
		selection4a = new SelectItem("dfield");
		selection4a.setShowTitle(false);
		items.add(selection4a);
		selection4b = new CheckboxItem("nulls4");
		selection4b.setShowLabel(false);
		items.add(selection4b);
		
		selection5 = new SelectItem("etype","Column E");
		items.add(selection5);
		selection5a = new SelectItem("efield");
		selection5a.setShowTitle(false);
		items.add(selection5a);
		selection5b = new CheckboxItem("nulls5");
		selection5b.setShowLabel(false);
		items.add(selection5b);
		
		selection6 = new SelectItem("ftype","Column F");
		items.add(selection6);
		selection6a = new SelectItem("ffield");
		selection6a.setShowTitle(false);
		items.add(selection6a);
		selection6b = new CheckboxItem("nulls6");
		selection6b.setShowLabel(false);
		items.add(selection6b);
		
		selection7 = new SelectItem("gtype","Column G");
		items.add(selection7);
		selection7a = new SelectItem("gfield");
		selection7a.setShowTitle(false);
		items.add(selection7a);
		selection7b = new CheckboxItem("nulls7");
		selection7b.setShowLabel(false);
		items.add(selection7b);
		
		selection8 = new SelectItem("htype","Column H");
		items.add(selection8);
		selection8a = new SelectItem("hfield");
		selection8a.setShowTitle(false);
		items.add(selection8a);
		selection8b = new CheckboxItem("nulls8");
		selection8b.setShowLabel(false);
		items.add(selection8b);
		
		selection9 = new SelectItem("itype","Column I");
		items.add(selection9);
		selection9a = new SelectItem("ifield");
		selection9a.setShowTitle(false);
		items.add(selection9a);
		selection9b = new CheckboxItem("nulls9");
		selection9b.setShowLabel(false);
		items.add(selection9b);
		
		selection10 = new SelectItem("jtype","Column J");
		items.add(selection10);
		selection10a = new SelectItem("jfield");
		selection10a.setShowTitle(false);
		items.add(selection10a);
		selection10b = new CheckboxItem("nulls10");
		selection10b.setShowLabel(false);
		items.add(selection10b);
		
		for(FormItem item : items) {
			if(item.getName().endsWith("type")) item.setValueMap(model.getMappingProcessors());
			else if(item.getName().startsWith("nulls")) {
				item.setShowTitle(false);
			}
			item.setDefaultValue("none");
			item.setWidth(100);
		}
		
		FormItem[] fitems = new FormItem[items.size()];
		form.setFields(items.toArray(fitems));
		addMember(form);
		
	}
	public Record getData() {
		Record data = new Record();
		data.setAttribute("type1", selection1.getValue());
		data.setAttribute("type2", selection2.getValue());
		data.setAttribute("type3", selection3.getValue());
		data.setAttribute("type4", selection4.getValue());
		data.setAttribute("type5", selection5.getValue());
		data.setAttribute("type6", selection6.getValue());
		data.setAttribute("type7", selection7.getValue());
		data.setAttribute("type8", selection8.getValue());
		data.setAttribute("type9", selection9.getValue());
		data.setAttribute("type10", selection10.getValue());
		return data;
	}
	public void setFields(LinkedHashMap<String,Object> fields) {
		for(FormItem item : items) {
			if(item.getName().endsWith("field")) item.setValueMap(fields);
		}
	}
	public void setRelationships(LinkedHashMap<String,Object> relationhips) {
		for(FormItem item : items) {
			//if(item.getName().endsWith("field")) item.setValueMap(relationhips);
		}
	}
	
}
