package org.archivemanager.data.client.data.cleaning;

import org.archivemanager.data.client.component.NodeSearchForm;
import org.archivemanager.data.client.data.SearchResultGrid;
import org.heed.openapps.gwt.client.component.Toolbar;

import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.layout.VLayout;

public class CleaningApplication extends VLayout {
	private Toolbar toolbar;
	private CleaningWizardPanel wizard;
	private NodeSearchForm searchForm;
	private SearchResultGrid searchGrid;
	private EntityCleaningDS cleaningDS = EntityCleaningDS.getInstance();
	
	
	public CleaningApplication() {
		setWidth100();
		setHeight100();
		
		wizard = new CleaningWizardPanel();
		wizard.setWidth100();
		wizard.setHeight(150);
		wizard.hide();
		addMember(wizard);
		
		toolbar = new Toolbar(30);
		toolbar.setWidth100();
		//toolbar.setMargin(1);
		toolbar.setBorder("1px solid #A7ABB4");
		
		searchForm = new NodeSearchForm();
		//searchForm.setHeight(40);
		toolbar.addToLeftCanvas(searchForm);
		/*
		toolbar.addButton("data", "/theme/images/icons32/broom.png", "Cleaning", new com.smartgwt.client.widgets.events.ClickHandler() {  
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				wizard.show(CleaningWizardPanel.MODE_CLEAN);
			}  
		});
		*/
		addMember(toolbar);
		
		searchGrid = new SearchResultGrid();
		searchGrid.setWidth100();
		searchGrid.setHeight100();
		addMember(searchGrid);
	}
	
	public void showMessage(String icon, String message) {
		toolbar.showMessage(icon, message);
	}
	public void hideMessage(int waitMilliseconds) {
		toolbar.hideMessage(waitMilliseconds);
	}
	public String getQuery() {
		return searchForm.getQuery();
	}
	public void clean(final String qname) {
		Record args = new Record();
		args.setAttribute("qname", qname);
		String[] targets = new String[searchGrid.getSelections().length];
		for(int i=0; i < searchGrid.getSelections().length; i++) {
			targets[i] = searchGrid.getSelections()[i].getAttribute("id");
		}
		args.setAttribute("targets", targets);
		cleaningDS.updateData(args, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				if(response.getStatus() == -1) {
					for(Object key : response.getErrors().keySet()) {
						String message = (String)response.getErrors().get(key);
						wizard.addMessage(response.getStatus(), message);
					}
				} else {
					wizard.addMessage(response.getStatus(), "entities cleaned successfully...");
					Record record = new Record();
					record.setAttribute("qname", qname);
        			record.setAttribute("query", searchForm.getQuery());
					searchGrid.search(record);
				}
			}						
		}); 
	}
	public void search(Record record) {
		searchGrid.search(record);
	}
}
