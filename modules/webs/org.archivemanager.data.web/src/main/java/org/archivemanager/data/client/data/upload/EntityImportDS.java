package org.archivemanager.data.client.data.upload;

import org.heed.openapps.gwt.client.OpenApps;

import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.DSDataFormat;


public class EntityImportDS extends RestDataSource {
	private static OpenApps oa = new OpenApps();
	private static EntityImportDS instance = null;  
	  
    public static EntityImportDS getInstance() {  
        if (instance == null) {  
            instance = new EntityImportDS("entityImportDS");  
        }  
        return instance;  
    }
    
	public EntityImportDS(String id) {
		setDataFormat(DSDataFormat.JSON);
		setID(id);  
        setTitleField("Title");	        
        DataSourceTextField nameField = new DataSourceTextField("title", "Title");     
        
        DataSourceTextField idField = new DataSourceTextField("id", "ID",50);  
        idField.setPrimaryKey(true);  
        idField.setRequired(true);
                
        setFields(idField,nameField);
        
        setFetchDataURL(oa.getServiceUrl()+"/service/entity/import/fetch.json");
        setAddDataURL(oa.getServiceUrl()+"/service/entity/import/add.json"); 
        //setRemoveDataURL("/service/entity/dictionary/model/remove.json");
        //setUpdateDataURL("/service/entity/dictionary/model/update.json");
	}
	
}
