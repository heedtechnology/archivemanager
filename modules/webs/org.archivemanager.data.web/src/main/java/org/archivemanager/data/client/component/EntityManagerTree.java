package org.archivemanager.data.client.component;

import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.EventTypes;
import org.heed.openapps.gwt.client.OpenAppsEvent;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.HTMLFlow;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.menu.Menu;
import com.smartgwt.client.widgets.menu.MenuItem;
import com.smartgwt.client.widgets.menu.events.ClickHandler;
import com.smartgwt.client.widgets.menu.events.MenuItemClickEvent;
import com.smartgwt.client.widgets.tree.TreeGrid;


public class EntityManagerTree extends TreeGrid {
	private Menu fullContextMenu;
	private Menu emptyContextMenu;
	//private AboutWindow aboutWindow;
	
		
	public EntityManagerTree() {
		setHeight100();
		setWidth100();
		setShowHeader(false);
		setBorder("0px");
		setWrapCells(true);
		setFixedRecordHeights(false);
		setCanReorderRecords(true);  
        setCanAcceptDroppedRecords(true);
		//EntityTreeDS ds = EntityTreeDS.getInstance();
        //setDataSource(ds);
        
        addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				Record record = event.getRecord();
				String type = record.getAttribute("type");
				if(type.equals("namespace")) setContextMenu(fullContextMenu);
				else setContextMenu(emptyContextMenu);
				EventBus.fireEvent(new OpenAppsEvent(EventTypes.SELECTION, record));
			}			
		});
        
        MenuItem aboutItem = new MenuItem("About Entity Manager");
        aboutItem.addClickHandler(new ClickHandler() {
        	public void onClick(MenuItemClickEvent event) {
        		//aboutWindow.show();
			}
        });
        MenuItem delItem = new MenuItem("Delete Namespace");
        delItem.addClickHandler(new ClickHandler() {
        	public void onClick(MenuItemClickEvent event) {
        		removeSelectedData();
			}
        });
        emptyContextMenu = new Menu();
        emptyContextMenu.setItems(aboutItem);
        fullContextMenu = new Menu();
        fullContextMenu.setItems(delItem,aboutItem);
        setContextMenu(emptyContextMenu);
        
        //aboutWindow = new AboutWindow();
	}
	public void select(Record record) {
		
	}
	public class AboutWindow extends Window {
		
		public AboutWindow() {
			setTitle("About The Entity Manager");
			setWidth(300);
			setHeight(150);
			setAutoCenter(true);
			
			HTMLFlow flow = new HTMLFlow();
			flow.setMargin(12);
			flow.setContentsURL("/administration/entity/about");
			addItem(flow);
		}
	}
}
