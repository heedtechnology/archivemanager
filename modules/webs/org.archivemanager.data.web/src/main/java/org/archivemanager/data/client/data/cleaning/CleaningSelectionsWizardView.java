package org.archivemanager.data.client.data.cleaning;

import org.archivemanager.data.client.EntityEventTypes;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.OpenAppsEvent;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.layout.VLayout;


public class CleaningSelectionsWizardView extends VLayout {		
	
	
	public CleaningSelectionsWizardView(final CleaningWizardPanel wizard) {
		setWidth100();
		setHeight100();
		hide();
		
		Label label = new Label("<div style='font-weight:bold;font-size:13px;margin:10px;'>Please select the nodes to clean below.</div>");
		label.setWidth100();
		label.setHeight(25);
		addMember(label);
		
		Canvas spacer = new Canvas();
		spacer.setHeight100();
		addMember(spacer);
		
		DynamicForm form = new DynamicForm();
		form.setWidth(110);
		form.setNumCols(2);
		
		ButtonItem cancelButton = new ButtonItem("cancel", "cancel");
		cancelButton.setWidth(50);
		//submitButton.setHeight(35);
		cancelButton.setStartRow(false);
		cancelButton.setEndRow(false);
		cancelButton.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
				wizard.getPane2().hide();
				wizard.getPane3().hide();
			}
		});
		
		ButtonItem submitButton = new ButtonItem("search", "select");
		submitButton.setWidth(50);
		//submitButton.setHeight(35);
		submitButton.setAlign(Alignment.LEFT);
		submitButton.setStartRow(false);
		submitButton.setEndRow(false);
		submitButton.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
				Record record = new Record();
				record.setAttribute("type", wizard.getView1().getValue());
				wizard.getTracker().show();
				wizard.getPane3().show();
				EventBus.fireEvent(new OpenAppsEvent(EntityEventTypes.CLEAN, record));
			}
		});
		form.setFields(cancelButton, submitButton);
		addMember(form);
	}
}