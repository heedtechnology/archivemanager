package org.archivemanager.data.client.data.upload;
import java.util.ArrayList;
import java.util.List;

import org.archivemanager.data.client.EntityEventTypes;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.OpenAppsEvent;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class DataGrid extends VLayout {
	private ListGrid grid;
	
	public DataGrid() {
		setWidth100();
		setHeight100();
		setMargin(5);
		
		grid = new ListGrid(){
			@Override  
            protected Canvas createRecordComponent(final ListGridRecord record, Integer colNum) {   
                String fieldName = this.getFieldName(colNum);    
                if(fieldName.equals("iconField")) { 
                	HLayout recordCanvas = new HLayout(3);  
                    recordCanvas.setHeight(22);
                    recordCanvas.setWidth100(); 
                    recordCanvas.setAlign(Alignment.CENTER);
                	ImgButton chartImg = new ImgButton();  
                    chartImg.setShowDown(false);  
                    chartImg.setShowRollOver(false);  
                    chartImg.setAlign(Alignment.CENTER);  
                    chartImg.setSrc("/theme/images/icons16/delete.png");  
                    chartImg.setPrompt("Delete");  
                    chartImg.setHeight(16);  
                    chartImg.setWidth(16);  
                    chartImg.addClickHandler(new ClickHandler() {  
                        public void onClick(ClickEvent event) {
                        	EventBus.fireEvent(new OpenAppsEvent(EntityEventTypes.DELETE_ENTITY, record));
                        }  
                    });
                    recordCanvas.addMember(chartImg);
                    return recordCanvas;
                } else return null;
			}
		};		
		//grid.setWidth100();
		grid.setHeight100();
		grid.setWrapCells(false);
		grid.setCanSelectAll(true);
		grid.setShowRecordComponents(true);          
        grid.setShowRecordComponentsByCell(true);
		grid.setSelectionType(SelectionStyle.SIMPLE);  
        grid.setSelectionAppearance(SelectionAppearance.CHECKBOX);
		grid.setDataSource(EntityImportDS.getInstance());
		//grid.setSort(new SortSpecifier[]{new SortSpecifier("name", SortDirection.ASCENDING),new SortSpecifier("id", SortDirection.ASCENDING)});
		List<ListGridField> items = new ArrayList<ListGridField>();
		
		ListGridField columnA = new ListGridField("value0", "A");
		columnA.setAlign(Alignment.CENTER);		
		items.add(columnA);
		
		ListGridField columnB = new ListGridField("value1", "B");
		columnB.setAlign(Alignment.CENTER);
		items.add(columnB);
		
		ListGridField columnC = new ListGridField("value2", "C");
		columnC.setAlign(Alignment.CENTER);
		items.add(columnC);	
		
		ListGridField columnD = new ListGridField("value3", "D");
		columnD.setAlign(Alignment.CENTER);
		items.add(columnD);
		
		ListGridField columnE = new ListGridField("value4", "E");
		columnE.setAlign(Alignment.CENTER);
		items.add(columnE);
		
		ListGridField columnF = new ListGridField("value5", "F");
		columnF.setAlign(Alignment.CENTER);
		items.add(columnF);
		
		ListGridField columnG = new ListGridField("value6", "G");
		columnG.setAlign(Alignment.CENTER);
		items.add(columnG);
		
		ListGridField columnH = new ListGridField("value7", "H");
		columnH.setAlign(Alignment.CENTER);
		items.add(columnH);
		
		ListGridField columnI = new ListGridField("value8", "I");
		columnI.setAlign(Alignment.CENTER);
		items.add(columnI);
		
		ListGridField columnJ = new ListGridField("value9", "J");
		columnJ.setAlign(Alignment.CENTER);
		items.add(columnJ);
		
		ListGridField iconField = new ListGridField("iconField", " ", 60);
		iconField.setCanSort(false);
		iconField.setCanFreeze(false);
		iconField.setCanGroupBy(false);
		iconField.setCanFilter(false);
		items.add(iconField);
		
		ListGridField[] fitems = new ListGridField[items.size()];
		grid.setFields(items.toArray(fitems));
		
		grid.addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				//ListGridRecord record = event.getRecord();
				//select(record);
			}			
		});
		grid.setShowResizeBar(true);
		addMember(grid);
	}
	public void fetchData(Record record, DSCallback callback) {
		Criteria criteria = new Criteria();
		for(String key : record.getAttributes()) {
			criteria.addCriteria(key, record.getAttribute(key));
		}
		grid.fetchData(criteria, callback);
	}
	public void saveData(Record record, DSCallback callback) {
		Criteria criteria = new Criteria();
		for(String key : record.getAttributes()) {
			criteria.addCriteria(key, record.getAttribute(key));
		}
		EntityImportDS.getInstance().addData(record);
	}
}
