package org.archivemanager.data.client.window;
import java.util.LinkedHashMap;

import org.heed.openapps.gwt.client.event.UploadListener;

import com.google.gwt.user.client.ui.NamedFrame;
import com.smartgwt.client.types.Encoding;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.UploadItem;


public class DataUploadWindow extends Window {
	private DynamicForm uploadForm;
	private UploadListener listener;
	private SelectItem modeItem;
	private UploadItem fileItem;
	
	
	public DataUploadWindow() {
		setTitle("Data Upload");
		setWidth(300);
		setHeight(140);
		setAutoCenter(true);
		initComplete(this);
		
		Canvas canvas = new Canvas();
		canvas.setWidth100();
		canvas.setHeight100();
		addItem(canvas);
		
		NamedFrame frame = new NamedFrame("uploadTarget");
		frame.setWidth("1");
		frame.setHeight("1");
		frame.setVisible(false);
		canvas.addChild(frame);	
		
		//ValuesManager vm = new ValuesManager();
		uploadForm = new DynamicForm();
		uploadForm.setHeight100();
		uploadForm.setWidth100();
		uploadForm.setMargin(5);
		//uploadForm.setValuesManager(vm);
		uploadForm.setEncoding(Encoding.MULTIPART);
		uploadForm.setTarget("uploadTarget");
		uploadForm.setNumCols(2);
		uploadForm.setCellPadding(5);
		uploadForm.setColWidths("50","*");
		uploadForm.setAction("/administration/entity/import/upload.xml");
		
		modeItem = new SelectItem("mode","Mode");
		modeItem.setWidth(200);
		//modeItem.setValueMap(modes);
				
		fileItem = new UploadItem("file", "File");
		fileItem.setWidth(200);				
				
		ButtonItem uploadButton = new ButtonItem("upload","Upload");
		uploadButton.setStartRow(false);
		uploadButton.setEndRow(false);
		uploadButton.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler(){
			public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent e) {
				Object obj = fileItem.getDisplayValue();
				if (obj != null) {
					uploadForm.submitForm();
				} else
					SC.say("Please select a file.");
			}
		});
		/*		
		ButtonItem saveButton = new ButtonItem("save", "Save");
		saveButton.setStartRow(false);
		saveButton.setEndRow(false);
		saveButton.setPrompt("Save");		
		saveButton.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
				EventBus.fireEvent(new OpenAppsEvent(EventTypes.IMPORT_START));
			}
		});
		*/		
		uploadForm.setItems(modeItem,fileItem,uploadButton/*,saveButton*/);
		canvas.addChild(uploadForm);
		
		addUploadListener(new UploadListener() {
			public void uploadComplete(String key) {				
				hide();
				//SC.say("Collection Imported Successfully");
			}
		});
	}
	
	public void setAction(String url) {
		uploadForm.setAction(url);
	}
	
	public void addUploadListener(UploadListener listener) {
		this.listener = listener;
	}
	
	public void uploadComplete(String fileName) {
		if (listener != null)
			listener.uploadComplete(fileName);
	}
	public void setModel(LinkedHashMap<String,Object> modes) {
		modeItem.setValueMap(modes);
	}
	
	private native void initComplete(DataUploadWindow upload) /*-{
	   $wnd.uploadComplete = function (fileName) {
	       upload.@org.heed.openapps.entity.client.window.DataUploadWindow::uploadComplete(Ljava/lang/String;)(fileName);
	   };
	}-*/;
}
