package org.archivemanager.data.client.component;
import java.util.Stack;

import org.archivemanager.data.client.EntityEventTypes;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.EventTypes;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.OpenAppsEventHandler;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;

public class BreadcrumbComponent extends HLayout {
	private Stack<String> uids = new Stack<String>();
	private Stack<String> names = new Stack<String>();
	
	
	public BreadcrumbComponent() {
		setHeight(20);
		setWidth100();
		setMembersMargin(2);
				
		uids.add("null");
		names.add("Home");
		addCrumb("null", "Home");
		
		EventBus.addHandler(org.heed.openapps.gwt.client.OpenAppsEvent.TYPE, new OpenAppsEventHandler()     {
	        @Override
	        public void onEvent(OpenAppsEvent event) {
	        	if(event.isType(EventTypes.BROWSE) || event.isType(EntityEventTypes.NODE_BROWSE)) {
	        		if(event.getRecord() != null) {
	        			String uid = event.getRecord().getAttribute("parent");
	        			String name = event.getRecord().getAttribute("name");
	        			navigate(uid, name);
	        		}
	        	}
	        }
		});
	}

	public void navigate(String uid, String name) {	
		if(uids.contains(uid)) {
			int index = uids.indexOf(uid);
			if(index < uids.size()-1) {
				for(int i=index; i < uids.size(); i++) {
					uids.pop();
					names.pop();
				}
			}
		} else {
			uids.add(uid);
			names.add(name);
		}
		for(Canvas comp : getChildren()) {
			removeChild(comp);
		}
		for(int i=0; i < uids.size(); i++) {
			String u = uids.get(i);
			String n = names.get(i);
			addCrumb(u, n);
		}
	}
	
	protected void addCrumb(final String uid, final String name) {	
		IButton button = getButton(name, new ClickHandler() {  
			public void onClick(ClickEvent event) {
              	Record record = new Record();
               	record.setAttribute("uid", uid);
               	record.setAttribute("parent", uid);
    			record.setAttribute("name", name);
               	EventBus.fireEvent(new OpenAppsEvent(EntityEventTypes.NODE_BROWSE, record));
            }
        });
		addMember(button);
		/*
		Label label = getLink(name, new ClickHandler() {  
			public void onClick(ClickEvent event) {
              	Record record = new Record();
               	record.setAttribute("uid", uid);
               	record.setAttribute("parent", uid);
    			record.setAttribute("name", name);
               	EventBus.fireEvent(new OpenAppsEvent(EventTypes.SEARCH, record));
            }
        });
		addMember(label);
		*/
	}
	public static Label getLink(String message, ClickHandler handler) {
	   Label link = new Label();
	   link = new Label(message);
	   link.setStyleName("clickable");
	   link.setHeight(20);
	   link.setAlign(Alignment.CENTER);
	   //Set the width to the length of the text.
	   int width = message != null ? message.length()*10 : 0;
	   link.setWidth(width);
	   link.addClickHandler(handler);
	   return link;
	}
	public static IButton getButton(String message, ClickHandler handler) {
	   IButton link = new IButton(message);
	   link.setAutoFit(true);
	   link.setIcon("/theme/images/icons16/folder.png");
	   link.setHeight(20);
	   link.setAlign(Alignment.CENTER);
	   link.addClickHandler(handler);
	   return link;
	}
}
