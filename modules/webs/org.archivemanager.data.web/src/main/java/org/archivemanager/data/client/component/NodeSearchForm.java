package org.archivemanager.data.client.component;

import org.archivemanager.data.client.EntityEventTypes;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.OpenAppsEvent;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.layout.HLayout;


public class NodeSearchForm extends HLayout {
	private TextItem searchField;
	
	
	public NodeSearchForm() {
		setWidth100();
		setHeight100();
		DynamicForm searchForm = new DynamicForm();
		searchForm.setWidth(300);
		searchForm.setNumCols(4);
		//searchForm.setCellPadding(1);
		//RowSpacerItem spacer = new RowSpacerItem();
		//spacer.setHeight(1);
		searchField = new TextItem("query");
		searchField.setShowTitle(false);
		searchField.setWidth(255);
		searchField.addKeyPressHandler(new KeyPressHandler() {
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if(event.getKeyName().equals("Enter")) {
					Record record = new Record();
					record.setAttribute("query", searchField.getValueAsString());
					EventBus.fireEvent(new OpenAppsEvent(EntityEventTypes.SEARCH_ENTITY, record));
				}				
			}
			
		});
		ButtonItem searchButton = new ButtonItem("search", "search");
		searchButton.setWidth(50);
		//searchButton.setHeight(35);
		searchButton.setStartRow(false);
		searchButton.setEndRow(false);
		searchButton.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
				Record record = new Record();
				record.setAttribute("query", searchField.getValueAsString());
				EventBus.fireEvent(new OpenAppsEvent(EntityEventTypes.SEARCH_ENTITY, record));
			}
		});
		searchForm.setFields(searchField,searchButton);
		addMember(searchForm);
	}
	public String getQuery() {
		return searchField.getValueAsString();
	}
}
