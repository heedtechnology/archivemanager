package org.archivemanager.data.client;

import org.archivemanager.data.client.component.BreadcrumbComponent;
import org.archivemanager.data.client.component.NodeBrowserGrid;
import org.archivemanager.data.client.node.NodeBrowserDetailDS;
import org.archivemanager.data.client.node.NodePanel;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.OpenAppsEventHandler;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class NodeManager extends VLayout {
	private NodeBrowserGrid grid;
	private NodeBrowserDetailDS ds = NodeBrowserDetailDS.getInstance("nodebrowser");
	private NodePanel nodePanel;
	
	public NodeManager() {
		setWidth100();
		setHeight100();
		setMembersMargin(1);
		
		BreadcrumbComponent breadcrumb = new BreadcrumbComponent();	
		breadcrumb.setBorder("1px solid #A7ABB4");
		breadcrumb.setHeight(32);
		addMember(breadcrumb);
		
		HLayout mainLayout = new HLayout();
	    mainLayout.setWidth100();
	    mainLayout.setHeight100();
	    addMember(mainLayout);
	    
	    grid = new NodeBrowserGrid();
	    grid.setWidth(300);
	    grid.setShowResizeBar(true);
        mainLayout.addMember(grid);
	    
        nodePanel = new NodePanel();
        nodePanel.setWidth100();
        nodePanel.setHeight100();
        mainLayout.addMember(nodePanel);
		        
        EventBus.addHandler(org.heed.openapps.gwt.client.OpenAppsEvent.TYPE, new OpenAppsEventHandler()     {
	        @Override
	        public void onEvent(OpenAppsEvent event) {
	        	if(event.isType(EntityEventTypes.NODE_BROWSE)) {
	        		grid.fetchData(event.getRecord(), new DSCallback() {
						@Override
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							
						}	            		
	            	});
	        	} else if(event.isType(EntityEventTypes.NODE_SELECTION)) {
	        		Criteria criteria = new Criteria();
	        		criteria.setAttribute("id", event.getRecord().getAttribute("id"));
	        		ds.fetchData(criteria, new DSCallback() {
						@Override
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							if(response.getData() != null && response.getData().length > 0)
								nodePanel.select(response.getData()[0]);
							else nodePanel.select(new Record());
						}	        			
	        		});
	        	}
	        }
        });
	}
	public void add() {
		
	}
	public void delete() {
		
	}
	public void update() {
	
	}
}
