package org.archivemanager.data.client.data.upload;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.archivemanager.data.client.EntityEventTypes;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.event.UploadListener;

import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.user.client.ui.NamedFrame;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.Encoding;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.HiddenItem;
import com.smartgwt.client.widgets.form.fields.RadioGroupItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.SpinnerItem;
import com.smartgwt.client.widgets.form.fields.UploadItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.layout.VLayout;


public class UploadSelectionPanel extends VLayout {
	private NativeEntityModel model = new NativeEntityModel();
	private DynamicForm uploadForm;
	private UploadListener listener;
	private UploadItem fileItem;
	private SelectItem modeItem;
	
	public UploadSelectionPanel() {
		setWidth100();
		setHeight100();
		setBorder("1px solid #A7ABB4");
		initComplete(this);
		
				
		/** Cell 1 */
		VLayout cell1 = new VLayout();
		addMember(cell1);
		
		Label label = new Label("<div style='font-weight:bold;font-size:13px;margin:5px;'>Select a file format.</div>");
		label.setWidth100();
		label.setHeight(20);
		cell1.addMember(label);
				
		DynamicForm form1 = new DynamicForm();
		form1.setNumCols(4);
		form1.setWidth100();
		form1.setCellPadding(5);
		cell1.addMember(form1);
		RadioGroupItem formatItem = new RadioGroupItem("format", "File Format");
		formatItem.setShowTitle(false);  
		formatItem.setValueMap(model.getFileFormats());
		formatItem.addChangedHandler(new ChangedHandler() {
			@Override
			public void onChanged(ChangedEvent event) {
				//wizard.getPane1().show();
				//wizard.getPane2().show();
				//wizard.getView4().show();
			}	        	
        });
		form1.setFields(formatItem);
		
		
		/** Cell 2 */
		VLayout cell2 = new VLayout();
		addMember(cell2);
		
		Label label2 = new Label("<div style='font-weight:bold;font-size:13px;margin:5px;'>Set Properties.</div>");
		label2.setWidth100();
		label2.setHeight(20);
		cell2.addMember(label2);
		
		DynamicForm form2 = new DynamicForm();
		form2.setNumCols(4);
		//form2.setCellPadding(5);
		cell2.addMember(form2);
		SpinnerItem rowItem = new SpinnerItem("row","Row Number");
		rowItem.setWidth(50);
		rowItem.setDefaultValue(0);
		
		form2.setFields(rowItem);
						
		
		/** Cell 3 */
		VLayout cell3 = new VLayout();
		cell3.setWidth100();
		addMember(cell3);
		
		Label label3 = new Label("<div style='font-weight:bold;font-size:13px;margin:5px;'>Select a file to upload.</div>");
		label3.setWidth100();
		label3.setHeight(20);
		cell3.addMember(label3);
		
		Canvas uploadPane = new Canvas();
		
		cell3.addMember(uploadPane);
		NamedFrame frame = new NamedFrame("uploadTarget");
		frame.setWidth("1");
		frame.setHeight("5");
		frame.setVisible(false);
		uploadPane.addChild(frame);	
		
		uploadForm = new DynamicForm();
		uploadForm.setHeight100();
		uploadForm.setWidth100();
		//uploadForm.setMargin(5);
		//uploadForm.setValuesManager(vm);
		uploadForm.setEncoding(Encoding.MULTIPART);
		uploadForm.setTarget("uploadTarget");
		uploadForm.setNumCols(2);
		uploadForm.setCellPadding(5);
		uploadForm.setColWidths("40","*");
		uploadForm.setAction("/service/entity/import/upload.json");
		
		List<FormItem> items = new ArrayList<FormItem>();
		HiddenItem type = new HiddenItem("data");
		items.add(type);
				
		HiddenItem qname = new HiddenItem("qname");
		items.add(qname);
		
		modeItem = new SelectItem("mode","Processor");
		modeItem.setWidth(200);
		items.add(modeItem);
		
		fileItem = new UploadItem("file", "File");
		fileItem.setWidth(100);
		fileItem.addChangedHandler(new ChangedHandler() {
			@Override
			public void onChanged(ChangedEvent event) {
				// TODO Auto-generated method stub
				
			}			
		});
		fileItem.setWidth(200);				
		items.add(fileItem);
		
		ButtonItem uploadButton = new ButtonItem("upload","Upload");
		uploadButton.setStartRow(false);
		uploadButton.setEndRow(false);
		uploadButton.setAlign(Alignment.RIGHT);
		uploadButton.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler(){
			public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent e) {
				Object obj = fileItem.getValue();
				if (obj != null) {
					EventBus.fireEvent(new OpenAppsEvent(EntityEventTypes.UPLOAD_ENTITY));
				} else
					SC.say("Please select a file.");
			}
		});
		items.add(uploadButton);
		
		ButtonItem saveButton = new ButtonItem("save","Save");
		saveButton.setStartRow(false);
		saveButton.setEndRow(false);
		saveButton.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler(){
			public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent e) {
				EventBus.fireEvent(new OpenAppsEvent(EntityEventTypes.SAVE_UPLOADED_DATA));
			}
		});
		items.add(saveButton);
		
		FormItem[] fitems = new FormItem[items.size()];
		uploadForm.setFields(items.toArray(fitems));
		uploadPane.addChild(uploadForm);
				
	}
	
	public void setProcessors(LinkedHashMap<String,Object> processors) {
		modeItem.setValueMap(processors);
	}
	public void setAction(String url) {
		uploadForm.setAction(url);
	}
	
	public void addUploadListener(UploadListener listener) {
		this.listener = listener;
	}
	
	public void uploadComplete(String fileName) {
		if (listener != null)
			listener.uploadComplete(fileName);
	}
	public void submitUpload(Record record) {
		JSONObject payload = new JSONObject();
		for(String key : record.getAttributes()) {
			payload.put(key, new JSONString(record.getAttribute(key)));
		}
		uploadForm.setValue("data", payload.toString());
		uploadForm.submitForm();
	}
	
	private native void initComplete(UploadSelectionPanel upload) /*-{
	   $wnd.uploadComplete = function (fileName) {
	       upload.@org.archivemanager.data.client.data.upload.UploadSelectionPanel::uploadComplete(Ljava/lang/String;)(fileName);
	   };
	}-*/;
}
