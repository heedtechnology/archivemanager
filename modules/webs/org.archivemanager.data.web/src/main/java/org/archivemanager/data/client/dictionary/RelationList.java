package org.archivemanager.data.client.dictionary;

import java.util.ArrayList;
import java.util.List;

import org.archivemanager.data.client.EntityEventTypes;
import org.archivemanager.data.client.window.AddRelationWindow;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.EventTypes;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.OpenAppsEventHandler;
import org.heed.openapps.gwt.client.QualifiedName;
import org.heed.openapps.gwt.client.component.Toolbar;
import org.heed.openapps.gwt.client.data.DictionaryServiceDS;

import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.layout.VLayout;

public class RelationList extends VLayout {
	private ListGrid relations;
	
	private AddRelationWindow addRelationWindow;
	
	private Record model;
	
	
	public RelationList() {
		Toolbar toolbar2 = new Toolbar(30);
		toolbar2.setBorder("1px solid #A7ABB4");
		toolbar2.setMargin(1);
		addMember(toolbar2);
		
		toolbar2.addButton("add", "/theme/images/icons32/add.png", "Add", new com.smartgwt.client.widgets.events.ClickHandler() {  
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				//EventBus.fireEvent(new OpenAppsEvent(EventTypes.ADD, new Record()));
				EventBus.fireEvent(new OpenAppsEvent(EntityEventTypes.CREATE_RELATION));
			}  
		});
		toolbar2.addButton("delete", "/theme/images/icons32/delete.png", "Delete", new com.smartgwt.client.widgets.events.ClickHandler() {  
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				EventBus.fireEvent(new OpenAppsEvent(EntityEventTypes.DELETE_RELATION, relations.getSelectedRecord()));
			}  
		});
		
		relations = new ListGrid();
		relations.addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				Record record = event.getRecord();
				EventBus.fireEvent(new OpenAppsEvent(EntityEventTypes.RELATION_SELECTION, record));
			}			
		});
		addMember(relations);
		List<ListGridField> items = new ArrayList<ListGridField>();	
		ListGridField columnA = new ListGridField("localName", "Local Name");
		items.add(columnA);
		ListGridField[] fitems = new ListGridField[items.size()];
		relations.setFields(items.toArray(fitems));
		
		addRelationWindow = new AddRelationWindow();
		
		EventBus.addHandler(org.heed.openapps.gwt.client.OpenAppsEvent.TYPE, new OpenAppsEventHandler()     {
	        public void onEvent(OpenAppsEvent event) {
	        	if(event.isType(EventTypes.SELECTION)) {
	        		model = event.getRecord();        		
	        	} else if(event.isType(EntityEventTypes.CREATE_RELATION)) {
	        		if(model == null) SC.warn("Please select a model to add this relation to.");
	        		else {
	        			String qnameStr = model.getAttribute("qname");
						if(qnameStr != null) {
							try {
								QualifiedName qname = QualifiedName.createQualifiedName(qnameStr);
								addRelationWindow.setNamespace(qname.getNamespace());
							} catch(Exception e) {
								e.printStackTrace();
							}
						}	        			
						addRelationWindow.show();
	        		}
	        	} else if(event.isType(EntityEventTypes.SAVE_RELATION)) {
	        		DictionaryServiceDS.getInstance().updateRelation(event.getRecord(), new DSCallback() {
	        			public void execute(DSResponse response, Object rawData, DSRequest request) {
	        				//fields.addData(response.getData()[0]);
	        			}	        			
	        		});
	        	} else if(event.isType(EntityEventTypes.ADD_RELATION)) {
	        		if(model == null) SC.warn("Please select a model to add this relation to.");
	        		Record record = event.getRecord();
	        		record.setAttribute("model", model.getAttribute("id"));
	        		DictionaryServiceDS.getInstance().addRelation(event.getRecord(), new DSCallback() {
	        			public void execute(DSResponse response, Object rawData, DSRequest request) {
	        				relations.addData(response.getData()[0]);
	        			}	        			
	        		});
	        		addRelationWindow.hide();
	        	} else if(event.isType(EntityEventTypes.DELETE_RELATION)) {
	        		DictionaryServiceDS.getInstance().removeRelation(event.getRecord(), new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							if(response.getData() != null && response.getData().length > 0) {
								relations.removeData(response.getData()[0]);
							}
						}	        			
	        		});	 
	        	}
	        }
		});
	}
	
	public void select(Record record) {
		relations.setData(record.getAttributeAsRecordArray("relations"));
	}
}
