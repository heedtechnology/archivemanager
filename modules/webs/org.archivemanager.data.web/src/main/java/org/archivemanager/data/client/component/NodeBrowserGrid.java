package org.archivemanager.data.client.component;

import org.archivemanager.data.client.EntityEventTypes;
import org.archivemanager.data.client.node.NodeBrowserGridDS;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.OpenApps;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.data.RestUtility;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class NodeBrowserGrid extends VLayout {
	private static OpenApps oa = new OpenApps();
	private ListGrid grid;
	
	
	public NodeBrowserGrid() {
		setWidth100();
		setHeight100();
		grid = new ListGrid() {
	    	@Override  
            protected Canvas createRecordComponent(final ListGridRecord record, Integer colNum) {   
                String fieldName = this.getFieldName(colNum); 
                String children = record.getAttributeAsString("children");
                if(fieldName.equals("iconField") && (children != null && children.equals("true"))) { 
                	HLayout recordCanvas = new HLayout(3);  
                    recordCanvas.setHeight(16);
                    recordCanvas.setWidth100(); 
                    recordCanvas.setAlign(Alignment.CENTER);
                	ImgButton chartImg = new ImgButton();  
                    chartImg.setShowDown(false);  
                    chartImg.setShowRollOver(false);  
                    chartImg.setAlign(Alignment.CENTER);  
                    chartImg.setSrc("/theme/images/icons16/folder_go.png");  
                    chartImg.setHeight(16);  
                    chartImg.setWidth(16); 
                    chartImg.addClickHandler(new ClickHandler() {  
                        public void onClick(ClickEvent event) {
                        	Record rec = new Record();
                        	rec.setAttribute("name", record.getAttribute("name"));
            				rec.setAttribute("parent", record.getAttribute("id"));
            				rec.setAttribute("sources", "false");
                        	EventBus.fireEvent(new OpenAppsEvent(EntityEventTypes.NODE_BROWSE, rec));
                        }  
                    });
                    recordCanvas.addMember(chartImg);
                    return recordCanvas;
                } else return null;
			}
	    };
	    grid.setWidth100();
	    grid.setAutoFetchData(true);
	    grid.setShowRecordComponents(true);          
        grid.setShowRecordComponentsByCell(true);
        grid.setCanSelectCells(true);
        grid.setWrapCells(true);
	    grid.setShowHeader(false);
	    grid.setDataSource(NodeBrowserGridDS.getInstance("nodeBrowserGridDS", false));
	    
        ListGridField nameField = new ListGridField("name", "Name");
		nameField.setWidth("*");
		nameField.addRecordClickHandler(new RecordClickHandler() {
			@Override
			public void onRecordClick(RecordClickEvent event) {
				//select(event.getRecord());
				EventBus.fireEvent(new OpenAppsEvent(EntityEventTypes.NODE_SELECTION, event.getRecord()));
			}	
		});
		ListGridField iconField = new ListGridField("iconField", "");  
        iconField.setWidth(30); 
        iconField.setShowTitle(false);
                
        grid.setFields(nameField, iconField);
        addMember(grid);
	}
	
	public void fetchData(Record record, final DSCallback callback) {
		/*
		grid.setEmptyMessage("${loadingImage}&nbsp;Loading data...");
		Record criteria = new Record();
		criteria.setAttribute("parent", record.getAttribute("parent"));
		RestUtility.get("/service/archivemanager/datasource/folders/browse.xml", criteria, new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				if(response.getData() != null && response.getData().length > 0) {
					grid.setData(response.getData());
				} else grid.setData(new Record[0]);
				grid.setEmptyMessage("No items to show.");
				if(callback != null) callback.execute(response, rawData, request);
			}						
		});	
		*/
		Criteria criteria = new Criteria();
		for(String key : record.getAttributes()) {
			criteria.setAttribute(key, record.getAttribute(key));
		}
		grid.fetchData(criteria);
	}
	public void setData(Record[] data) {
		grid.setEmptyMessage("${loadingImage}&nbsp;Loading data...");
		grid.setData(data);
		if(data != null && data.length > 0) {
			grid.setData(data);
		} else {
			grid.setData(new Record[0]);
			grid.setEmptyMessage("No items to show.");
		}
	}
	public Record getSelectedRecord() {
		return grid.getSelectedRecord();
	}
	public void addData(Record record, DSCallback callback) {
		grid.addData(record, callback);
	}
	public void updateData(Record record) {
		grid.updateData(record);
	}
	public void removeData(Record record, DSCallback callback) {
		grid.removeData(record, callback);
	}
	public void removeSelectedData() {
		if(grid.anySelected()) {
			RestUtility.post(oa.getServiceUrl() + "/service/entity/remove.xml", grid.getSelectedRecord(), new DSCallback() {
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					grid.removeSelectedData();
				}						
			});				
		}
	}
}
