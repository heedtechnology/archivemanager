package org.archivemanager.data.client.data.upload;

import java.util.LinkedHashMap;

import com.google.gwt.core.client.JavaScriptObject;
import com.smartgwt.client.util.JSOHelper;

public class NativeEntityModel {

	
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getMappingProcessors() { 
		JavaScriptObject obj = getNativeMappingProcessors();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>();
	}	
	private final native JavaScriptObject getNativeMappingProcessors() /*-{
        return $wnd.mapping_processors;
	}-*/;
	
	@SuppressWarnings("unchecked")
	public LinkedHashMap<String,Object> getFileFormats() { 
		JavaScriptObject obj = getNativeFileFormats();
		if(obj != null) return (LinkedHashMap<String,Object>)JSOHelper.convertToMap(obj);
		return new LinkedHashMap<String,Object>();
	}	
	private final native JavaScriptObject getNativeFileFormats() /*-{
        return $wnd.file_formats;
	}-*/;
}
