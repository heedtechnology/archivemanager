package org.archivemanager.data.client.dictionary.data;

import org.heed.openapps.gwt.client.OpenApps;

import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.DSDataFormat;


public class EntityTreeDS extends RestDataSource {
	private static OpenApps oa = new OpenApps();
	private static EntityTreeDS instance = null;  
	  
    public static EntityTreeDS getInstance() {  
        if (instance == null) {  
            instance = new EntityTreeDS("entityTreeDS");  
        }  
        return instance;  
    }
    
	public EntityTreeDS(String id) {
		setDataFormat(DSDataFormat.JSON);
		setID(id);  
        setTitleField("Name");	        
        DataSourceTextField nameField = new DataSourceTextField("name", "Name");     
        
        DataSourceTextField idField = new DataSourceTextField("uid", "UID",50);  
        idField.setPrimaryKey(true);  
        idField.setRequired(true);
        
        DataSourceTextField parentField = new DataSourceTextField("parent", "Parent");  
        parentField.setRequired(true);  
        parentField.setForeignKey(id + ".uid");  
        parentField.setRootValue("null");  
        
        setFields(idField,nameField,parentField);
        
        setFetchDataURL(oa.getServiceUrl()+"/service/entity/dictionary/fetch.json");
        setAddDataURL(oa.getServiceUrl()+"/service/entity/dictionary/add.json"); 
        setRemoveDataURL(oa.getServiceUrl()+"/service/entity/dictionary/remove.json");
        setUpdateDataURL(oa.getServiceUrl()+"/service/entity/update.json");
	}
}
