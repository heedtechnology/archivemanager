package org.archivemanager.data.client.dictionary.data;

import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;

public class EntityImportDS extends RestDataSource {
	private static EntityImportDS instance = null;  
	  
    public static EntityImportDS getInstance() {  
        if (instance == null) {  
            instance = new EntityImportDS("EntityImportDS");  
        }  
        return instance;  
    }
    
	public EntityImportDS(String id) {
		setID(id);  
        setTitleField("title");	        
        DataSourceTextField nameField = new DataSourceTextField("title", "Title");     
        
        DataSourceTextField idField = new DataSourceTextField("uid", "ID",50);  
        idField.setPrimaryKey(true);  
        idField.setRequired(true);
        
        DataSourceTextField parentField = new DataSourceTextField("parent", "Parent");  
        parentField.setRequired(true);  
        parentField.setForeignKey(id + ".uid");  
        //parentField.setRootValue("null");  
        
        setFields(idField,nameField,parentField);
        
        setFetchDataURL("/administration/entity/import/fetch.xml");
        setAddDataURL("/administration/entity/import/add.xml"); 
        //setRemoveDataURL("/core/entity/remove.xml");
        //setUpdateDataURL("/repository/accession/update.xml");
	}
}