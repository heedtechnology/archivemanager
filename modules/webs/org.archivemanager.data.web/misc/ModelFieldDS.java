package org.archivemanager.data.client.dictionary.data;

import org.heed.openapps.gwt.client.OpenApps;

import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.DSDataFormat;


public class ModelFieldDS extends RestDataSource {
	private static OpenApps oa = new OpenApps();
	private static ModelFieldDS instance = null;  
	  
    public static ModelFieldDS getInstance() {  
        if (instance == null) {  
            instance = new ModelFieldDS("modelFieldDS");  
        }  
        return instance;  
    }
    
	public ModelFieldDS(String id) {
		setDataFormat(DSDataFormat.JSON);
		setID(id);  
        setTitleField("Title");	        
        DataSourceTextField nameField = new DataSourceTextField("title", "Title");     
        
        DataSourceTextField idField = new DataSourceTextField("id", "ID",50);  
        idField.setPrimaryKey(true);  
        idField.setRequired(true);
                
        setFields(idField,nameField);
        
        setFetchDataURL(oa.getServiceUrl()+"/service/entity/dictionary/field/fetch.json");
        setAddDataURL(oa.getServiceUrl()+"/service/entity/dictionary/field/add.json"); 
        setRemoveDataURL(oa.getServiceUrl()+"/service/entity/dictionary/field/remove.json");
        setUpdateDataURL(oa.getServiceUrl()+"/service/entity/dictionary/field/update.json");
	}
}
