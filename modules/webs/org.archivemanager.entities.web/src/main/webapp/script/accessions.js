var repositoryId;
var collectionId;
var activityValueMap = {'acknowledgement_sent':'Acknowledgement Sent','arranged_sorted':'Arranged and Sorted','listed':'Listed','processing_beginning':'Processing Beginning','processing_ending':'Processing Ending','rights_transferred':'Rights Transferred','shelved':'Shelved'};
var noteValueMap = {'General':'General','Description':'Description','Inventory':'Inventory','Condition':'Condition','Access Restriction':'Access Restriction','Use Restriction':'Use Restriction'};
var taskValueMap = {'Processing Beginning':'Processing Beginning','Processing Ending':'Processing Ending'};
var contributorValueMap = {'Manager':'Manager','Lead Processor':'Lead Processor','Processor':'Processor'};
var donorMap = {'Donor':'Donor','Co-Donor':'Co-Donor'};
var extentType = {'':'','box':'Box','envelope':'Envelope','package':'Package','digital':'Digital'};
var contentType = {'paige_box':'Paige Box','manuscript_box':'Manuscript Box','oversized_box':'Oversized Box','package':'Package','film_canister':'Film Canister'};
function updateEntityData(xmlDoc, xmlText) {
	accessionXml = accession.recordsFromXML(isc.XMLTools.selectNodes(xmlDoc, "//node"));
	isc.ResultSet.create({ID:"accessionRS",dataSource:"accession",initialData:accessionXml});
	repositoryTree.getSelectedRecord()['title'] = accessionXml[0]['title'];
	repositoryTree.refreshFields();
	accessionForm1.setValues(accessionXml);
	if(accessionXml[0]['paid'] && accessionXml[0]['paid'] == 'true') accessionForm1.setValue('paid', true);
	else accessionForm1.setValue('paid', false);
	if(accessionXml[0]['appraisal'] && accessionXml[0]['appraisal'] == 'true') accessionForm1.setValue('appraisal', true);
	else accessionForm1.setValue('appraisal', false);
	if(accessionXml[0]['new_collection'] && accessionXml[0]['new_collection'] == 'true') accessionForm1.setValue('new_collection', true);
	else accessionForm1.setValue('new_collection', false);
	if(accessionXml[0]['existing_collection'] && accessionXml[0]['existing_collection'] == 'true') accessionForm1.setValue('existing_collection', true);
	else accessionForm1.setValue('existing_collection', false);
	if(accessionXml[0]['acknowledged'] && accessionXml[0]['acknowledged'] == 'true') accessionForm1.setValue('acknowledged', true);
	else accessionForm1.setValue('acknowledged', false);
	noteXml = ds.recordsFromXML(isc.XMLTools.selectNodes(xmlDoc, "//node/source_associations/notes/node"));
	noteList.setData(noteXml);
	activityXml = ds.recordsFromXML(isc.XMLTools.selectNodes(xmlDoc, "//node/source_associations/activities/node"));
	activityList.setData(activityXml);
	taskXml = ds.recordsFromXML(isc.XMLTools.selectNodes(xmlDoc, "//node/source_associations/tasks/node"));
	taskList.setData(taskXml);
	itemXml = ds.recordsFromXML(isc.XMLTools.selectNodes(xmlDoc, "//node/source_associations/items/node"));
	collectionItemRelationList.setData(itemXml);
	extentXml = ds.recordsFromXML(isc.XMLTools.selectNodes(xmlDoc, "//node/source_associations/extents/node"));
	contentsList.setData(extentXml);
	var contactsXml = ds.recordsFromXML(isc.XMLTools.selectNodes(xmlDoc, "//node/source_associations/contacts/node"));
	for(var i=0; i<contactsXml.length; i++) {
		contactsXml[i]['full_name'] = contactsXml[i]['first_name']+' '+contactsXml[i]['last_name'];
	}
	contactRelationList.setData(contactsXml);
	collectionItemRelationPanelAddButton.hide();
}
function load_accession(node) {
	entityId = node.id;
	if(node.qname == '{openapps.org_repository_1.0}accession') {
		collectionItemRelationshipTree.fetchData({collection:node.parent});
		isc.XMLTools.loadXML("/core/entity/get/"+node.id, "updateEntityData(xmlDoc, xmlText)");
	} else {
		collectionItemRelationshipTree.fetchData({collection:node.id});
	}
}
function save(reload) {
	if(accessionForm1.valuesHaveChanged()) {
		var parms = accessionForm1.getValues();
  		parms['qname'] = '{openapps.org_repository_1.0}accession';
 		isc.XMLTools.loadXML("/core/entity/update", function(xmlDoc, xmlText) {
 			if(reload) updateEntityData(xmlDoc, xmlText)
 		},{httpMethod:"POST",params:parms});
	}
}
function remove() {
	accessionList.removeSelectedData();
	accessionForm1.clearValues();
}
function transformDonorRequest() {
	if(accessionList.getSelectedRecord()) {
		var params = {
				accession_id : accessionList.getSelectedRecord().id
		};
		return params;
	}
}
function add_accessions() {
	var parms = accessionsAddWindowForm.getValues();
	parms['pid'] = accessionList.getSelectedRecord().id;
	accessionsList.addData(parms);
	accessionsAddWindow.hide();
	//if(!repositoryTree.data.find('id', parms['collectee'])) repositoryTree.fetchData({time:Math.random()});
}
function load_donorPanel(node) {
	donorPanelForm.editRecord(node);
}
function remove_donorPanel() {
	var donor_id = donorPanelList.getSelectedRecord().id;
	var accession_id = accessionList.getSelectedRecord().id;
	isc.XMLTools.loadXML("remove.xml?_dataSource=donors&id="+donor_id+"&aid="+accession_id, "updateEntityData(xmlDoc, xmlText)");
}
function add_donorPanel() {
	var accession = accessionList.getSelectedRecord();
	if(accession && accession.localName == 'accession') {
		var contact_id = donorPanelAddWindowList.getSelectedRecord().id;
		isc.XMLTools.loadXML("add.xml?_dataSource=donors&contact="+contact_id+"&accession="+accession.id, "updateEntityData(xmlDoc, xmlText)");
	} else isc.warn("Please select an accession on the left side to associate this Donor with.");
	donorPanelAddWindow.hide();
}
function stopUpload(id) {
	importTree.fetchData({op:"clear"},function(dsResponse, data, dsRequest) {
		importTree.fetchData(Math.random(),function(dsResponse, data, dsRequest) {
			//var obj = data.get(0).title;
			//uploadForm.setValue('title', obj);
		});
	});
}
function addAccession() {
	if(!repositoryTree.anySelected() || repositoryTree.getSelectedRecord().localName != 'collection')
		isc.warn('Please select a collection to add the accession to.');
	else addAccessionWin.show();
}
function repositoryDrop(dragRecords,dropFolder,index) {
	var child = dragRecords.get(0);
	repositoryTree.updateData({id:dragRecords.get(0).id,parent:dropFolder.id},function(dsResponse, data, dsRequest) {});
}
isc.MessagingDataSource.create({ID:"containers",fetchDataURL:"fetch.xml",addDataURL:"add.xml",updateDataURL:"update.xml",removeDataURL:"remove.xml",
	fields:[
		{name:"id", title:"ID", type:"text", primaryKey:true, required:true},
    	{name:"title", title:"Title", type:"text", length:"128", required:true},
    	{name:"idx", title:"Index", type:"integer"},
    	{name:"parent", title:"ParentID", type:"text", required:true, foreignKey:"containers.id", rootValue:"null"}
	]
});
isc.MessagingDataSource.create({ID:"accessions",fetchDataURL:"../accessions",addDataURL:"/core/entity/associate",updateDataURL:"/core/entity/update",removeDataURL:"/core/entity/remove.xml",fields:[{name:"id", title:"ID", type:"text", primaryKey:true, required:true},{name:'date', type:'date'}]});
isc.MessagingDataSource.create({ID:'accession',fetchDataURL:'fetch.xml',addDataURL:'add.xml',updateDataURL:'update.xml',removeDataURL:'remove.xml',fields:[{name:'date', type:'date'}]});
isc.MessagingDataSource.create({ID:'accessionsDS',fetchDataURL:'fetch.xml',addDataURL:'add.xml',updateDataURL:'update.xml',removeDataURL:'remove.xml'});
isc.MessagingDataSource.create({ID:'donors',fetchDataURL:'fetch.xml',addDataURL:'add.xml',updateDataURL:'update.xml',removeDataURL:'remove.xml',transformRequest:function(dsRequest) {var superClassArguments = this.Super('transformRequest', dsRequest);var params = transformDonorRequest();return isc.addProperties({}, superClassArguments, params);},fields:[{name:'last_update', type:'date'},{name:'last_wrote', type:'date'},{name:'birth_date', type:'date'},{name:'death_date', type:'date'},{name:'last_ship', type:'date'},{name:'id',title:'ID',type:'text',primaryKey:true,required:true}]});
isc.MessagingDataSource.create({ID:'collections',fetchDataURL:'fetch.xml',addDataURL:'add.xml',updateDataURL:'update.xml',removeDataURL:'remove.xml',fields:[{name:'restrictions', type:'boolean'},{name:'internal', type:'boolean'},{name:'id',title:'ID',type:'text',primaryKey:true,required:true}]});
isc.MessagingDataSource.create({ID:'locations',fetchDataURL:'fetch.xml',addDataURL:'add.xml',updateDataURL:'update.xml',removeDataURL:'remove.xml',fields:[{name:'id',title:'ID',type:'text',primaryKey:true,required:true}]});
isc.MessagingDataSource.create({ID:"repositoryDS",fetchDataURL:"/repository/accession",addDataURL:"/repository/accession/add.xml",updateDataURL:"/repository/accession/update.xml",removeDataURL:"/core/entity/remove.xml",
	fields:[
		{name:"id", title:"ID", type:"text", primaryKey:true, required:true},
    	{name:"title", title:"Title", type:"text", length:"128", required:true},
    	{name:"idx", title:"Index", type:"integer"},
    	{name:"parent", title:"ParentID", type:"text", required:true, foreignKey:"repositories.id", rootValue:"0"}
	]
});
isc.VLayout.create({ID:'leftCanvas',visibility:'visible',position:'absolute',width:'300',height:'100%',
	members:[
	    isc.HLayout.create({height:'25',width:'100%',autoDraw:false,layoutLeftMargin:2,membersMargin:5,
	    	members:[
	    	    isc.IButton.create({autoDraw:false,title:'import',width:70,icon:"/theme/images/icons16/database.png",click:'importWindow.show();'}),
	    	    isc.IButton.create({autoDraw:false,title:'add',width:60,icon:"/theme/images/icons16/add.png",click:'addAccession();'}),
	    	    isc.IButton.create({autoDraw:false,title:'delete',width:75,icon:"/theme/images/icons16/remove.png",click:'repositoryTree.removeSelectedData()'}),
	    	    isc.IButton.create({autoDraw:false,title:'save',width:65,icon:"/theme/images/icons16/save.png",click:'save();'})
	    	]
	    }),
	    isc.DynamicForm.create({ID:'repositoryListSearch',width:'100%',margin:0,numCols:3,cellPadding:2,
		    fields:[
		        {name:'query',width:'245',colSpan:2,type:'text',showTitle:false},
		        {name:'validateBtn',title:'search',type:'button',width:50,startRow:false,endRow:false,
		        	click:function() {
		        		var searchType = repositoryListSearch.getValue('searchDate');
		        		if(searchType == undefined || searchType == false) {
		        			query = repositoryListSearch.getValue('query');
		        			repositoryTree.fetchData({qname:'{openapps.org_repository_1.0}collection',query:query,field:'name',sort:'name_e'});
		        		} else {
		        			date = repositoryListSearch.getValue('date');
		        			repositoryTree.fetchData({qname:'{openapps.org_repository_1.0}accession',query:date,field:'date',sort:'name_e'});
		        		}
		        	}
		        },
		        {name:'date',width:'100',type:'date',displayFormat:'toUSShortDate',showTitle:false,disabled:true},
		        {name:'searchDate',width:'25',type:'checkbox',showTitle:false,showLabel:false,
		        	change:function() {
		        		if(repositoryListSearch.getValue('searchDate') == true) {
		        			repositoryListSearch.getField('date').setDisabled(true);
		        			repositoryListSearch.getField('query').setDisabled(false);
		        		} else {
		        			repositoryListSearch.getField('date').setDisabled(false);
		        			repositoryListSearch.getField('query').setDisabled(true);
		        		}
		        	}
		        },
		        {type:'spacer'}	        
		    ]
		}),
		isc.TreeGrid.create({ID:'repositoryTree',dataSource:'repositoryDS',height:'100%',width:'100%',showHeader:false,animateFolders:true,
			canAcceptDroppedRecords:true,canReparentNodes:true,canReorderRecords:true,canDropOnLeaves:true,selectionType:'single',autoDraw:false,
			dragDataAction:'move',canDragRecordsOut:true,folderDrop:function(dragRecords,dropFolder,index){repositoryDrop(dragRecords, dropFolder, index);},
			recordClick:'load_accession(record)'
		})
	]
});
isc.VLayout.create({ID:'informationPanel',autoDraw:false,height:'100%',width:'100%',
	members:[
	     isc.DynamicForm.create({ID:'accessionForm1',dataSource:'accession',width:'100%',height:'50%',margin:'10',cellPadding:5,numCols:'4',colWidths:[175,'*'],visibility:'visible',autoFocus:false,
	    	 fields:[
	    	     {name:'collection',type:'staticText',title:'Collection'},
	    	     {name:'date',width:'*',colSpan:'1',type:'date',startDate:'1/1/1900',displayFormat:'toUSShortDate',title:'Date'},
	    	     {name:'cost',width:'125',title:'Cost'},
	    	     {name:'aquisition_type',width:'*',type:'select',valueMap:{'':'','Deposit':'Deposit','Gift':'Gift','Purchase':'Purchase','Transfer':'Transfer'},width:'100',title:'Acquisition Type'},
	    	     {name:'extent_number',width:'*',editorType:'spinner',defaultValue:0,min:0,max:1000,step:1,width:75,title:'Quantity'},
	    	     {name:'extent_type',width:'*',type:'select',valueMap:extentType,width:'100',title:'Quantity Type'},
	    	     {name:'pagebox_quantity',width:'*',colSpan:'1',editorType:'spinner',defaultValue:0,min:0,max:1000,step:1,width:75,title:'Estimated PaigeBox(s) On Shelf'},
	    	     {name:'priority',width:'*',type:'select',valueMap:{'':'','low':'Low','normal':'Normal','high':'High'},width:'100',title:'Priority'},
	    	     {name:'estimated_packages',width:'*',editorType:'spinner',defaultValue:0,min:0,max:1000,step:1,width:75,title:'Estimated Package(s) On Shelf'},
	    	     {name:'paid',type:'checkbox',labelAsTitle:true,title:'Previously Paid'},	    	     
	    	     {name:'linear_feet',width:'*',colSpan:'1',editorType:'spinner',defaultValue:0,min:0,max:1000,step:.1,width:75,title:'Linear Feet'},
	    	     {name:'appraisal',width:'*',type:'checkbox',labelAsTitle:true,title:'Donor Wants Appraisal'},
	    	     {name:'new_collection',width:'*',type:'checkbox',labelAsTitle:true,title:'New Collection'},
	    	     {name:'acknowledged',width:'*',type:'checkbox',labelAsTitle:true,title:'Acknowledgement Letter Sent'},
	    	     {name:'existing_collection',width:'*',type:'checkbox',labelAsTitle:true,title:'Existing Collection'},	    	     
	    	     {name:'general_note',width:'600',type:'textArea',showTitle:true,colSpan:'4',height:'150',title:'Contents'},
	    	     {name:'books',width:'600',colSpan:'4',title:'Books'},
	    	 ]
	     }),
	     isc.HLayout.create({width:'100%',layoutLeftMargin:100,membersMargin:20,
	    	 members:[
	    	     getAccessionContentPanel('300','150'),
	    	     getContactRelationPanel('Donors', donorMap,'400','200')
	    	 ]
	     })
	]
});
isc.VLayout.create({ID:"notesAndActivitiesPanel",autoDraw:false,height:'100%',width:'100%',margin:5,membersMargin:5,
	members:[
	    getNotesPanel(noteValueMap),
	    getActivitiesPanel(activityValueMap),
	    getTasksPanel(taskValueMap),
	    getAddressPanel('100%','200'),
	    getCollectionItemRelationPanel('500','150')
	]
});
isc.TabSet.create({ID:'rightTabSet',tabBarPosition:'top',width:'100%',height:'100%',autoDraw:false,
	tabs:[
	    {title:'Basic Information',pane:informationPanel},
	    {title:'Additional Information',pane:notesAndActivitiesPanel}
	]
});
isc.HLayout.create({ID:'pageLayout',width:smartWidth,height:smartHeight,position:'relative',visibility:'visible',members:[leftCanvas,rightTabSet]});

isc.Window.create({ID:"addAccessionWin",title:"Add Accession",width:400,height:100,autoCenter:true,isModal:true,showModalMask:true,autoDraw:false,
    items: [
        isc.DynamicForm.create({ID:"addAccessionWin2Form",numCols:"2",cellPadding:7,autoDraw:false,colWidths:[100,"*"],
         	fields: [
         	    {name:"date", title:"Date", type:"date",displayFormat:"toUSShortDate",required:true},
         	    //{name:"collectee", title:"Collectee", type:"text",width:"*",required:true},
         	    {name:"submit",title:"Save",type:"button",
         	    	click:function () {
         	    		if(addAccessionWin2Form.validate()) {
         	    			var parms = addAccessionWin2Form.getValues();
	         	    		parms['assoc_qname'] = '{openapps.org_repository_1.0}accessions';
	         	    		parms['entity_qname'] = '{openapps.org_repository_1.0}accession';
	         	    		parms['source_id'] = entityId;
	         	    		repositoryTree.addData(parms, function(xmlDoc, xmlText) {
	         	    			addAccessionWin2Form.clearValues();
	         	    			addAccessionWin.hide();
	         	    		});        	    			
         	    		}
         	    	}
         	    }
           	]
        })
    ]
});
function getAddressPanel(width,height) {
	isc.Window.create({ID:"addAddressWin",title:"Add Address",width:750,height:250,autoCenter:true,isModal:true,showModalMask:true,autoDraw:false,
	    items: [
	        isc.ListGrid.create({ID:'addAddressWinList',height:'100%',width:'100%',alternateRecordStyles:true,autoDraw:false,autoFetchData:false,
     	    	fields:[
     	    	    {name:'address1',title:'Address',width:'*',align:'center'},
     	    	    {name:'city',title:'City',width:'100',align:'center'},
     	    	    {name:'state',title:'State',width:'100',align:'center'},
     	    	    {name:'country',title:'Country',width:'100',align:'center'},
     	    	    {name:'zip',title:'Zip',width:'100',align:'center'}
     	    	]
     	    }),
     	    isc.HLayout.create({ID:'accessionListButtons',height:'25',width:'100%',autoDraw:false,layoutLeftMargin:2,membersMargin:5,
     	    	members:[
     	    	    isc.Button.create({title:"Link",icon:"/theme/images/icons16/link.png",
     	    	    	click:function () {
     	    	    		if(entityId && addAddressWinList.anySelected()) {
     	    	    			save(false);
     	    	    			var target = addAddressWinList.getSelectedRecord().id;
     	    	    			parms = {};
     	    	    			associate('{openapps.org_contact_1.0}addresses', entityId, target, parms, function(xmlDoc, xmlText) {
     	    	    				updateEntityData(xmlDoc, xmlText);
     	    	    				addAddressWin.hide();
     	    	    			});
     	    	    		}
     	    	    	}
     	    	    })
     	    	]
     	    })
	    ]
	});
	return isc.VLayout.create({ID:"addressPanel",autoDraw:false,width:width,
   	 	members:[	        	          
   	 	    isc.ToolStrip.create({width: "100%", height:24,membersMargin:5,
   	 	    	members: [
   	 	    	    isc.HTMLFlow.create({width:"100%",contents:"<span style='font-weight:bold;font-size:14px;'>Addresses</span>"}),
   	 	    	    isc.Button.create({width:75,height:20,icon:"/theme/images/icons16/add.png",title:"add",
   	 	    	    	click:function() {
   	 	    	    		if(contactRelationList.anySelected()) {
   	 	    	    			var donorId = contactRelationList.getSelectedRecord().target_id;
   	 	    	    			isc.XMLTools.loadXML("/core/entity/children/"+donorId+"?targets=false&sources=false", function(xmlDoc, xmlText) {
   	 	    	    				var addressesXml = ds.recordsFromXML(isc.XMLTools.selectNodes(xmlDoc, "//node"));
   	 	    	    				addAddressWinList.setData(addressesXml);
   	 	    	    				addAddressWin.show();
   	 	    	    			});   	 	    	    			
   	 	    	    		}
   	 	    	    	}
   	 	    	    }),   	 	    	    	
   	 	    	    isc.Button.create({width:75,height:20,icon:"/theme/images/icons16/remove.png",title:"delete",
   	 	    	    	click:function() {
   	 	    	    		record = addAddressList.getSelectedRecord();
   	 	    	    		id = record.target_id ? record.target_id : record.id;
   	 	    	    		removeEntity(id, function(xmlDoc, xmlText) {
   	 	    	    			addAddressList.removeSelectedData();
   	 	    	    		});	    		        	        	
   	 	    	    	}
   	 	    	    })
   	 	    	]
   	 	    }),
   	 	    isc.ListGrid.create({ID:'addAddressList',autoFitData:"vertical",autoFitMaxHeight:200,width:'100%',height:60,width:'100%',alternateRecordStyles:true,autoDraw:false,autoFetchData:false,
   	 	    	fields:[
   	 	    	    {name:'address1',title:'Address',width:'*',align:'center'},
   	    	        {name:'city',title:'City',width:'200',align:'center'},
   	    	        {name:'state',title:'State',width:'100',align:'center'},
   	    	        {name:'country',title:'Country',width:'100',align:'center'},
   	    	        {name:'zip',title:'Zip',width:'100',align:'center'}
	            ]
   	 	    })
   	 	]
    });
}
function getAccessionContentPanel(width,height) {
	isc.Window.create({ID:"editContentsWindow",title:"Add Contents",width:250,height:125,autoCenter:true,isModal:true,showModalMask:true,autoDraw:false,
		items: [
		    isc.DynamicForm.create({ID:"editContentsWindowForm",height:'30',width:"100%",numCols:"2",cellPadding:5,autoDraw:false,colWidths:[100,'*'],
	        	fields: [
	        	    {name:'value',title:'Value',editorType:'spinner',defaultValue:0,min:0,max:1000,step:.1,width:75},
	        	    {name:'type',width:'*',type:'select',valueMap:contentType,width:'125',title:'Unit of Measure'},
	        	    {name:"submit",title:"Add",type:"button",icon:"/theme/images/icons16/add.png",startRow:false,endRow:false,
	 	         	   	click:function () {
	 	         	   		if(entityId) {
	 	         	   			parms = editContentsWindowForm.getValues();
	 	         	   			addAssociateEntity('{openapps.org_repository_1.0}extents', entityId, '{openapps.org_repository_1.0}extent', parms, function(xmlDoc, xmlText) {
	 	         	   				var data = ds.recordsFromXML(isc.XMLTools.selectNodes(xmlDoc, "//node"));
	 	         	   				var updatedRecord = isc.addProperties(noteList.getSelectedRecord(),data[0]); 
	 	         	   				contentsList.addData(updatedRecord);
	 	         	   				editContentsWindowForm.clearValues();
	 	         	   				editContentsWindow.hide();
	 	         	   			});
	 	         	   		}
	 	         	   	}
		        	}
	        	]
	        })
	    ]
	});
	return isc.VLayout.create({ID:"contentsPanel",autoDraw:false,width:width,
   	 	members:[	        	          
   	 	    isc.ToolStrip.create({width: "100%", height:24,membersMargin:5,
   	 	    	members: [
   	 	    	    isc.HTMLFlow.create({width:"100%",contents:"<span style='font-weight:bold;font-size:14px;'>Contents</span>"}),
   	 	    	    isc.Button.create({width:60,height:20,icon:"/theme/images/icons16/add.png",title:"add",click:"editContentsWindow.show()"}),
   	 	    	    isc.Button.create({width:75,height:20,icon:"/theme/images/icons16/remove.png",title:"delete",
   	    	    	 click:function() {
   	    	    		 record = contentsList.getSelectedRecord();
   	    	    		 id = record.target_id ? record.target_id : record.id;
   	    	    		 removeEntity(id, function(xmlDoc, xmlText) {
   	    	    			 contentsList.removeSelectedData();
   	    	    		 });	    		        	        	
   	    	    	 }
   	 	    	    })
   	 	    	]
   	 	    }),
   	 	    isc.ListGrid.create({ID:'contentsList',autoFitData:"vertical",autoFitMaxHeight:200,width:'100%',height:60,width:'100%',alternateRecordStyles:true,autoDraw:false,autoFetchData:false,
   	 	    	fields:[
   	 	    	    {name:'value',title:'Value',width:'50',align:'center'},
   	    	        {name:'type',title:'Unit of Measure',type:'select',width:'*',align:'center',valueMap:extentType}
	            ]
   	 	    })
   	 	]
    });
}
isc.Window.create({ID:"importWindow",title:"Import Accession",width:1020,height:630,autoCenter:true,isModal:true,showModalMask:true,autoDraw:false,
    items: [
        getImportApplication()
    ]
});