package org.archivemanager.entities.client.classification.form;

import org.archivemanager.entities.client.data.ClassificationDS;
import org.heed.openapps.gwt.client.data.NativeClassificationModel;

import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;

public class SubjectForm extends DynamicForm {
	private NativeClassificationModel model = new NativeClassificationModel();
	
	private static final int fieldWidth = 300;
	
	public SubjectForm() {
		setWidth100();
		setMargin(5);
		setCellPadding(5);
		setNumCols(2);
		setColWidths("100","*");
		//setBorder("1px solid #C0C3C7");
		setDataSource(ClassificationDS.getInstance());
		
		StaticTextItem idItem = new StaticTextItem("id", "ID");
		idItem.setColSpan(2);
		idItem.setWidth(fieldWidth);
		
		TextItem nameItem = new TextItem("name", "Term");
		nameItem.setWidth(fieldWidth);		
		
		TextAreaItem descItem = new TextAreaItem("description", "Description");
		descItem.setWidth(500);
		
		SelectItem sourceItem = new SelectItem("source", "Source");
		sourceItem.setValueMap(model.getSubjectSource());
		sourceItem.setWidth(fieldWidth);
		
		SelectItem typeItem = new SelectItem("type", "Type");
		typeItem.setValueMap(model.getSubjectType());
		typeItem.setWidth(fieldWidth/2);
		
		TextItem urlItem = new TextItem("url", "URL");
		urlItem.setWidth(500);
		
		TextAreaItem noteItem = new TextAreaItem("note", "Note");
		noteItem.setWidth(500);
		
		setFields(idItem,nameItem,typeItem,sourceItem,urlItem,descItem,noteItem);
	}
}
