package org.archivemanager.entities.client.data;

import org.heed.openapps.gwt.client.OpenApps;

import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.types.DSDataFormat;


public class ClassificationDS extends RestDataSource {
	private static OpenApps oa = new OpenApps();
	private static ClassificationDS instance = null;  
	  
    public static ClassificationDS getInstance() {  
        if (instance == null) {  
            instance = new ClassificationDS("classificationDS");  
        }  
        return instance;  
    }
    
	public ClassificationDS(String id) {
		setDataFormat(DSDataFormat.JSON);
		setID(id);
				
		setAddDataURL(oa.getServiceUrl() + "/service/archivemanager/repository/collection/add.json");
		setFetchDataURL(oa.getServiceUrl() + "/service/entity/get.json");
		setRemoveDataURL(oa.getServiceUrl() + "/service/entity/remove.json");
		setUpdateDataURL(oa.getServiceUrl() + "/service/entity/update.json");
	}
	
	@Override
	protected void transformResponse(DSResponse response, DSRequest request,Object data) {
		// TODO Auto-generated method stub
		super.transformResponse(response, request, data);
	}
}