package org.archivemanager.entities.client.classification;

import org.heed.openapps.gwt.client.OpenApps;
import org.heed.openapps.gwt.client.component.Toolbar;
import org.heed.openapps.gwt.client.data.RestUtility;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.SpacerItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class EntityEntryPanel extends VLayout {
	private static OpenApps oa = new OpenApps();
	private ListGrid grid;
	private DynamicForm searchForm;
	
	private DynamicForm addForm;
	private ButtonItem submitItem;
	//private ButtonItem saveItem;
	
	private Record selection;
	
	
	public EntityEntryPanel() {
		setWidth100();
		setHeight100();
		setMargin(10);
		
		HLayout headerLayout = new HLayout();
		headerLayout.setWidth100();
		headerLayout.setHeight(5);
		addMember(headerLayout);
		
		Toolbar toolbar = new Toolbar(35);
		/*
		searchForm = new DynamicForm();
		searchForm.setWidth(300);
		searchForm.setHeight(25);
		searchForm.setMargin(0);
		searchForm.setNumCols(4);
		searchForm.setCellPadding(2);
		final TextItem searchField = new TextItem("query");
		searchField.setShowTitle(false);
		searchField.setWidth(255);
		searchField.addKeyPressHandler(new KeyPressHandler() {
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if(event.getKeyName().equals("Enter")) {
					search(searchField.getValueAsString());
				}				
			}
			
		});
		ButtonItem searchButton = new ButtonItem("search", "search");
		searchButton.setWidth(50);
		searchButton.setStartRow(false);
		searchButton.setEndRow(false);
		searchButton.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
				search(searchField.getValueAsString());
			}
		});
		toolbar.addToLeftCanvas(searchForm);
		
		searchForm.setFields(searchField,searchButton);
		*/
		toolbar.addButton("add", "/theme/images/icons32/add.png", "Add", new com.smartgwt.client.widgets.events.ClickHandler() {  
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				addForm.editNewRecord();
				addForm.show();
			}  
		});
		
		addMember(toolbar);
		
		grid = new ListGrid() {
			@Override  
            protected Canvas createRecordComponent(final ListGridRecord record, Integer colNum) {  
				String fieldName = this.getFieldName(colNum);  
                if(fieldName.equals("iconField")) {  
                    HLayout recordCanvas = new HLayout(8);  
                    recordCanvas.setHeight100();
                    recordCanvas.setWidth100();
                    recordCanvas.setAlign(Alignment.CENTER);
                    
                    ImgButton editImg = new ImgButton();  
                    editImg.setShowDown(false);  
                    editImg.setShowRollOver(false);  
                    editImg.setLayoutAlign(Alignment.CENTER);  
                    editImg.setSrc("/theme/images/icons16/delete.png");  
                    editImg.setPrompt("Delete Entry");  
                    editImg.setHeight(16);  
                    editImg.setWidth(16);  
                    editImg.addClickHandler(new ClickHandler() {  
                        public void onClick(ClickEvent event) {  
                        	SC.confirm("Do you really wish to remove this permanently?", new BooleanCallback() {
            					public void execute(Boolean value) {
            						if(value) {
            							Record rec = new Record();
            							String id = record.getAttribute("target_id");
            							if(id == null) id = record.getAttribute("id");
            							rec.setAttribute("id", id);
            							RestUtility.post("/service/entity/remove.xml", rec, new DSCallback() {
            								public void execute(DSResponse response, Object rawData, DSRequest request) {
            									grid.removeData(record);
            									addForm.clearValues();
            									addForm.hide();
            								}						
            							});
            						}
            					}			
            				});
                        }  
                    });
                    recordCanvas.addMember(editImg);
                    
                    return recordCanvas;
                }
                return null;
			};
		};
		grid.setWidth("100%");
		grid.setMinHeight(350);
		grid.setAutoFitData(Autofit.VERTICAL);
		grid.setAutoFitMaxHeight(750);
		grid.setShowRecordComponents(true);          
        grid.setShowRecordComponentsByCell(true);
		//grid.setBorder("1px");
		grid.setWrapCells(true);
		grid.setFixedRecordHeights(false);
		//grid.setShowHeader(false);
		
		ListGridField dateField = new ListGridField("date", "Date", 125);
		
		ListGridField collectionField = new ListGridField("collection_name", "Collection");
		
		ListGridField nameField = new ListGridField("name", "Name");
		
		ListGridField iconField = new ListGridField("iconField", " ", 60);
		iconField.setCanSort(false);
		iconField.setCanFreeze(false);
		iconField.setCanGroupBy(false);
		iconField.setCanFilter(false);
		
		grid.setFields(dateField, collectionField, nameField, iconField);
		grid.addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				addForm.editRecord(event.getRecord());
				addForm.show();
			}
		});
        addMember(grid);
        
        addForm = new DynamicForm();
		addForm.setNumCols(3);
		addForm.setCellPadding(5);
		addForm.setMargin(5);
		addForm.setColWidths("75", "50", "*");
		
		final TextItem dateItem = new TextItem("date", "Date");
		dateItem.setWidth(200);
		dateItem.setColSpan(3);
		
		final TextItem collectionItem = new TextItem("collection_name", "Collection");
		collectionItem.setWidth(400);
		collectionItem.setColSpan(3);
		
		final TextItem nameItem = new TextItem("name", "Name");
		nameItem.setWidth(400);
		nameItem.setColSpan(3);
		
		TextAreaItem contentItem = new TextAreaItem("items","Items");
		contentItem.setColSpan(3);
		contentItem.setHeight(150);
		contentItem.setWidth(400);
		
		SpacerItem spacer = new SpacerItem();
        
		submitItem = new ButtonItem("submit", "Save");
		submitItem.setIcon("/theme/images/icons16/disk.png");
		submitItem.setStartRow(false);
		submitItem.setEndRow(false);
		submitItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
				final Record record = addForm.getValuesAsRecord();
								
				if(addForm.isNewRecord()) {
					String id = selection.getAttribute("id");
					record.setAttribute("assoc_qname", "{openapps.org_classification_1.0}entries");
					record.setAttribute("entity_qname", "{openapps.org_classification_1.0}entry");	
					record.setAttribute("source", id);
					RestUtility.postJSON(oa.getServiceUrl() + "/service/entity/associate.json",record,new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							if(response.getData() != null && response.getData().length > 0) {
								grid.addData(response.getData()[0]);
							}
						}						
					});
				} else {
					String id = grid.getSelectedRecord().getAttribute("target_id");
					if(id == null) id = grid.getSelectedRecord().getAttribute("id");
					record.setAttribute("id", id);
					record.setAttribute("sources", "false");
					record.setAttribute("targets", "false");
					record.setAttribute("qname", "{openapps.org_classification_1.0}entry");
					RestUtility.postJSON(oa.getServiceUrl() + "/service/entity/update.json",record,new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							if(response.getData() != null && response.getData().length > 0) {
								Record resp = response.getData()[0];
								grid.getSelectedRecord().setAttribute("date", resp.getAttribute("date"));
								grid.getSelectedRecord().setAttribute("items", resp.getAttribute("items"));
								grid.getSelectedRecord().setAttribute("name", resp.getAttribute("name"));
								grid.getSelectedRecord().setAttribute("collection_name", resp.getAttribute("collection_name"));
								grid.redraw();
							}
						}						
					});					
				}				
			}			
		});
		addForm.setFields(dateItem, collectionItem,nameItem,contentItem,spacer,submitItem/*,saveItem*/);
		addMember(addForm);
		addForm.hide();
	}
	
	public void select(Record record) {
		this.selection = record;
		try {
			Record source_associations = record.getAttributeAsRecord("source_associations");
			if(source_associations != null) {
				Record notes = source_associations.getAttributeAsRecord("entries");
				if(notes != null) {
					grid.setData(notes.getAttributeAsRecordArray("node"));
				} else grid.setData(new Record[0]);
			} else grid.setData(new Record[0]);
		} catch(ClassCastException e) {
			grid.setData(new Record[0]);
		}
		addForm.clearValues();
		addForm.hide();
	}
	public void search(String query) {
		Criteria criteria = new Criteria();
		criteria.setAttribute("qname", searchForm.getValue("classification"));
		criteria.setAttribute("query", query);
		criteria.setAttribute("field", "freetext");
		criteria.setAttribute("sort", "name_e");
		grid.fetchData(criteria);
	}
}
