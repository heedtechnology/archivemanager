package org.archivemanager.entities.client.classification;

import org.archivemanager.entities.client.classification.form.CorporationForm;
import org.archivemanager.entities.client.classification.form.PersonForm;
import org.heed.openapps.gwt.client.component.SearchTermComponent;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.layout.VLayout;

public class NamedEntityPanel extends VLayout {
	private PersonForm personForm;
	private CorporationForm corpForm;
	private SearchTermComponent searchTerms;
	
	private Record selection;
	
	public NamedEntityPanel() {
		setWidth100();
		Canvas formCanvas = new Canvas();
		personForm = new PersonForm();
		corpForm = new CorporationForm();
		formCanvas.addChild(personForm);
		formCanvas.addChild(corpForm);
		personForm.hide();
		corpForm.hide();
				
		addMember(formCanvas);
		
		VLayout layout = new VLayout();
		addMember(layout);
		layout.setLayoutLeftMargin(105);
		searchTerms = new SearchTermComponent("500");
		searchTerms.setMargin(5);
		layout.addMember(searchTerms);
		Canvas spacer = new Canvas();
		spacer.setWidth(500);
		spacer.setHeight100();
		layout.addMember(spacer);		
	}
	
	public void fetchData(Criteria criteria, DSCallback callback) {
		if(selection != null) {
			String type = selection.getAttribute("localName");
			if(type != null) {				
				if(type.equals("person")) {
					personForm.fetchData(criteria, callback);
					corpForm.hide();
					personForm.show();
				} else if(type.equals("corporation")) {
					corpForm.fetchData(criteria, callback);
					personForm.hide();
					corpForm.show();
				}
			}
		}
	}
	public void select(Record selection) {
		this.selection = selection;
		searchTerms.select(selection);
	}
	public void save(DSCallback callback) {
		if(selection != null) {
			String type = selection.getAttribute("localName");
			if(type != null) {
				if(type.equals("person")) {
					personForm.saveData(callback);
				} else if(type.equals("corporation")) {
					corpForm.saveData(callback);
				}
			}
		}
	}
}
