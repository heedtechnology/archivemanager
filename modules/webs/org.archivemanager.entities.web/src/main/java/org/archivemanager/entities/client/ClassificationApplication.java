package org.archivemanager.entities.client;

import java.util.LinkedHashMap;

import org.archivemanager.entities.client.classification.EntityEntryPanel;
import org.archivemanager.entities.client.classification.NamedEntityPanel;
import org.archivemanager.entities.client.classification.SubjectPanel;
import org.archivemanager.entities.client.data.ClassificationListDS;
import org.archivemanager.gwt.client.component.ContentAssociationPanel;
import org.heed.openapps.gwt.client.component.Toolbar;
import org.heed.openapps.gwt.client.data.NativeClassificationModel;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;

public class ClassificationApplication extends VLayout {
	private NativeClassificationModel model = new NativeClassificationModel();
	private Toolbar toolbar;
	private DynamicForm searchForm;
	private ListGrid grid;
	private TabSet tabs;
	private NamedEntityPanel classificationPanel;
	private SubjectPanel subjectPanel;
	private ContentAssociationPanel contentPanel;
	private EntityEntryPanel entriesPanel;
	
	private AddClassificationWindow addWindow;
	
	public ClassificationApplication() {
		setHeight100();  
		setWidth100();
		//setMembersMargin(2);
		//setBorder("1px solid #BFBFBF");
		
		toolbar = new Toolbar(32);
		toolbar.setMargin(2);
		toolbar.setBorder("1px solid #a8c298;");
        //toolbar.setLayoutLeftMargin(32);
        
        searchForm = new DynamicForm();
		searchForm.setWidth(300);
		searchForm.setHeight(25);
		searchForm.setMargin(0);
		searchForm.setNumCols(4);
		searchForm.setCellPadding(2);
		final TextItem searchField = new TextItem("query");
		searchField.setShowTitle(false);
		searchField.setWidth(255);
		searchField.addKeyPressHandler(new KeyPressHandler() {
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if(event.getKeyName().equals("Enter")) {
					search(searchField.getValueAsString());
				}				
			}
			
		});
		ButtonItem searchButton = new ButtonItem("search", "search");
		searchButton.setWidth(50);
		searchButton.setStartRow(false);
		searchButton.setEndRow(false);
		searchButton.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
				search(searchField.getValueAsString());
			}
		});
		toolbar.addToLeftCanvas(searchForm);
		
		SelectItem classificationSelect = new SelectItem("classification");
		classificationSelect.setWidth(100);
		LinkedHashMap<String,String> valueMap1 = new LinkedHashMap<String,String>();
		valueMap1.put("{openapps.org_classification_1.0}person","Person");
		valueMap1.put("{openapps.org_classification_1.0}corporation","Corporate");
		valueMap1.put("{openapps.org_classification_1.0}subject","Subject");
		classificationSelect.setValueMap(valueMap1);
		classificationSelect.setDefaultValue("{openapps.org_classification_1.0}person");
		classificationSelect.setShowTitle(false);
		
		searchForm.setFields(searchField,searchButton,classificationSelect);
		
		toolbar.addButton("add", "/theme/images/icons32/add.png", "Add", new com.smartgwt.client.widgets.events.ClickHandler() {  
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				addWindow.show();
			}  
		});
		toolbar.addButton("delete", "/theme/images/icons32/delete.png", "Delete", new com.smartgwt.client.widgets.events.ClickHandler() {  
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				SC.confirm("Do you really wish to remove this permanently?", new BooleanCallback() {
					public void execute(Boolean value) {
						if(value) grid.removeSelectedData();
					}			
				});
			}  
		});
		toolbar.addButton("save", "/theme/images/icons32/disk.png", "Save", new com.smartgwt.client.widgets.events.ClickHandler() {  
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				String localName = grid.getSelectedRecord().getAttribute("localName");
				if(localName.equals("subject")) {
					subjectPanel.save(new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							if(response.getData() != null && response.getData().length > 0) {
								Record accession = response.getData()[0];
								if(accession != null) {
									String date = accession.getAttribute("name");
									String title = grid.getSelectedRecord().getAttribute("name");
									if(title != null && !title.equals(date)) {
										grid.getSelectedRecord().setAttribute("name", date);
										grid.refreshFields();
									}
								}
							}
						}
					});	
				} else if(localName.equals("person") || localName.equals("corporation")) {
					classificationPanel.save(new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							if(response.getData() != null && response.getData().length > 0) {
								Record accession = response.getData()[0];
								if(accession != null) {
									String date = accession.getAttribute("name");
									String title = grid.getSelectedRecord().getAttribute("name");
									if(title != null && !title.equals(date)) {
										grid.getSelectedRecord().setAttribute("name", date);
										grid.refreshFields();
									}
								}
							}
						}
					});	
				} 
			}  
		});
		
        addMember(toolbar);
        
        HLayout mainLayout = new HLayout();
        mainLayout.setWidth100();
        mainLayout.setHeight100();
        mainLayout.setMembersMargin(2);
        mainLayout.setMargin(2);
        mainLayout.setBorder("1px solid #A7ABB4");
        addMember(mainLayout);
        
		grid = new ListGrid();
		grid.setWidth(300);
		grid.setHeight("100%");
		grid.setBorder("1px solid #BFBFBF");
		grid.setShowHeader(false);
		grid.setShowResizeBar(true);
		ListGridField nameField = new ListGridField("name","Name");
		grid.setDataSource(ClassificationListDS.getInstance());
		grid.setFields(nameField);
		grid.addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				select(event.getRecord());
			}
		});
		mainLayout.addMember(grid);
                
        tabs = new TabSet();
		tabs.setWidth100();
		tabs.setHeight100();
		mainLayout.addMember(tabs);
		        
        Tab homeTab = new Tab("Detail");
        Canvas canvas = new Canvas();
        canvas.setWidth100();
        canvas.setHeight100();        
        classificationPanel = new NamedEntityPanel();
        canvas.addChild(classificationPanel);
        subjectPanel = new SubjectPanel();
        canvas.addChild(subjectPanel);
        homeTab.setPane(canvas);
        tabs.addTab(homeTab);
        
        Tab entryTab = new Tab("Entries");
        entriesPanel = new EntityEntryPanel();
        entryTab.setPane(entriesPanel);
        tabs.addTab(entryTab);
        
        Tab contentTab = new Tab("Content");
        contentPanel = new ContentAssociationPanel();
        contentTab.setPane(contentPanel);
        tabs.addTab(contentTab);
        
        tabs.hide();
        
        addWindow = new AddClassificationWindow();
	}
	
	public void select(Record record) {		
		Criteria criteria = new Criteria();
		criteria.setAttribute("id", record.getAttribute("id"));
		String type = record.getAttribute("localName");
		if(type.equals("person") || type.equals("corporation")) {
			criteria.addCriteria("sources", "true");
			criteria.addCriteria("targets", "true");
			classificationPanel.select(record);
			classificationPanel.fetchData(criteria, new DSCallback() {
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					if(response.getData() != null && response.getData().length > 0) {
						Record accession = response.getData()[0];
						if(accession != null) {
							String date = accession.getAttribute("name");
							String title = grid.getSelectedRecord().getAttribute("name");
							if(title != null && !title.equals(date)) {
								grid.getSelectedRecord().setAttribute("name", date);
								grid.refreshFields();
							}
							subjectPanel.hide();
							classificationPanel.show();							
						}
						contentPanel.select(accession);
						entriesPanel.select(accession);
					}
				}
			});	
		} else if(type.equals("subject")) {
			subjectPanel.select(record);
			subjectPanel.fetchData(criteria, new DSCallback() {
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					if(response.getData() != null && response.getData().length > 0) {
						Record accession = response.getData()[0];
						if(accession != null) {
							String date = accession.getAttribute("name");
							String title = grid.getSelectedRecord().getAttribute("name");
							if(title != null && !title.equals(date)) {
								grid.getSelectedRecord().setAttribute("name", date);
								grid.refreshFields();
							}
							classificationPanel.hide();
							subjectPanel.show();							
						}
						contentPanel.select(accession);
					}
				}
			});	
		}
		tabs.show();
		//toolbar.showAddButton(true);
		//mgr.getToolbar().showDeleteButton(true);
		//mgr.getToolbar().showSaveButton(true);
	}
	
	public void search(String query) {
		Criteria criteria = new Criteria();
		criteria.setAttribute("qname", searchForm.getValue("classification"));
		criteria.setAttribute("query", query);
		criteria.setAttribute("field", "name");
		criteria.setAttribute("sort", "name_e");
		grid.fetchData(criteria);
	}

	public class AddClassificationWindow extends Window {
		public AddClassificationWindow() {
			setWidth(400);
			setHeight(140);
			setTitle("Add Classification");
			setAutoCenter(true);
			setIsModal(true);
			
			/*
			 * isc.DynamicForm.create({ID:'contactsListAddWindowForm',width:'100%',datasource:'contacts',margin:10,numCols:2,cellPadding:7,
	    	fields:[
	    	    {name:'name',width:'*',required:true,title:'Name'},
	    	    {name:'type',width:'*',required:true,title:'Type',type:'select',valueMap:{'person':'Person','corporation':'Corporation','subject':'Subject'}},
	    	    {name:'validateBtn',title:'Save',type:'button',
	    	    	click:function() {
	    	    		var parms = contactsListAddWindowForm.getValues();
	    	    		parms['qname'] = '{openapps.org_classification_1.0}'+contactsListAddWindowForm.getValue('type');
	    	    		contactsListList.addData(parms);
	    	    		contactsListAddWindow.hide();
	    	    	}
	    	    }
	    	]
	    })
			 */
			final DynamicForm addForm = new DynamicForm();
			addForm.setCellPadding(7);
			final TextItem nameItem = new TextItem("name", "Name");
			nameItem.setWidth("*");
			
			SelectItem typeItem = new SelectItem("type");
			typeItem.setValueMap(model.getClassificationType());
			
			ButtonItem submitItem = new ButtonItem("submit", "Add");
			submitItem.setIcon("/theme/images/icons16/add.png");
			submitItem.setStartRow(false);
			submitItem.setEndRow(false);
			submitItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					Record record = new Record();
					record.setAttribute("qname", "{openapps.org_classification_1.0}"+addForm.getValueAsString("type"));
					record.setAttribute("name", nameItem.getValueAsString());
					grid.addData(record, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							addWindow.hide();
						}
					});
				}
			});
			
			addForm.setFields(nameItem,typeItem,submitItem);
			addItem(addForm);
		}
	}

}