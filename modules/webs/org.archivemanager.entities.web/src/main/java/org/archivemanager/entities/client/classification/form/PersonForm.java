package org.archivemanager.entities.client.classification.form;

import org.archivemanager.entities.client.data.ClassificationDS;
import org.heed.openapps.gwt.client.data.NativeClassificationModel;

import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;


public class PersonForm extends DynamicForm {
	private NativeClassificationModel model = new NativeClassificationModel();
	
	private static final int fieldWidth = 300;
	
	public PersonForm() {
		setWidth100();
		setMargin(5);
		setCellPadding(5);
		setNumCols(4);
		setColWidths("100","*","100","*");
		setDataSource(ClassificationDS.getInstance());
		
		StaticTextItem idItem = new StaticTextItem("id", "ID");
		idItem.setColSpan(2);
		idItem.setWidth(fieldWidth);	
		
		TextItem nameItem = new TextItem("name", "Name");
		nameItem.setWidth(fieldWidth);		
		
		TextItem primaryNameItem = new TextItem("primary_name", "Primary Name");
		primaryNameItem.setWidth(fieldWidth);
		
		TextItem secondaryNameItem = new TextItem("secondary_name", "Secondary Name");
		secondaryNameItem.setWidth(fieldWidth);
		
		TextItem fullerItem = new TextItem("fuller_form_name", "Fuller Form");
		fullerItem.setWidth(fieldWidth);
		
		TextItem prefixItem = new TextItem("prefix", "Prefix");
		prefixItem.setWidth(fieldWidth);
		
		TextItem suffixItem = new TextItem("suffix", "Suffix");
		suffixItem.setWidth(fieldWidth);
		
		TextItem datesItem = new TextItem("dates", "Dates");
		datesItem.setWidth(fieldWidth);
		
		SelectItem sourceItem = new SelectItem("source", "Source");
		sourceItem.setValueMap(model.getSubjectSource());
		sourceItem.setWidth(fieldWidth);
		
		SelectItem typeItem = new SelectItem("note_type", "Type");
		typeItem.setValueMap(model.getNamedEntityType());
		typeItem.setWidth(fieldWidth/2);
		
		SelectItem ruleItem = new SelectItem("rule", "Rule");
		ruleItem.setValueMap(model.getNamedEntityRule());
		ruleItem.setWidth(fieldWidth-50);
		
		TextItem urlItem = new TextItem("url", "URL");
		urlItem.setWidth(425);
		
		TextAreaItem descItem = new TextAreaItem("description", "Description");
		descItem.setColSpan(4);
		descItem.setWidth(500);
		
		TextAreaItem citationItem = new TextAreaItem("citation", "Citation");
		citationItem.setColSpan(4);
		citationItem.setWidth(500);
		
		TextAreaItem noteItem = new TextAreaItem("note", "Note");
		noteItem.setColSpan(4);
		noteItem.setWidth(500);
		
		setFields(idItem,nameItem,primaryNameItem,secondaryNameItem,fullerItem,prefixItem,sourceItem,
				suffixItem,ruleItem,datesItem,typeItem,urlItem,descItem,citationItem,noteItem);
	}
}
