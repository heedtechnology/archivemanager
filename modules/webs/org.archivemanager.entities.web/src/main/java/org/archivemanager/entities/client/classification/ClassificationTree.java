package org.archivemanager.entities.client.classification;

import org.archivemanager.entities.client.data.ClassificationTreeDS;

import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.tree.TreeGrid;

public class ClassificationTree extends TreeGrid {
	
	
	public ClassificationTree() {
		setHeight100();
		setWidth100();
		setShowHeader(false);
		setBorder("0px");
		
		ClassificationTreeDS ds = ClassificationTreeDS.getInstance();
        setDataSource(ds);
        
        addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				//mgr.select(event.getRecord());	
			}			
		});
	}
	
}
