package org.archivemanager.entities.client.data;

import org.heed.openapps.gwt.client.OpenApps;

import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.DSDataFormat;

public class ClassificationListDS extends RestDataSource {
	private static OpenApps oa = new OpenApps();
	private static ClassificationListDS instance = null;  
	  
    public static ClassificationListDS getInstance() {  
        if (instance == null) {  
            instance = new ClassificationListDS("classificationListDS");  
        }  
        return instance;  
    }
    
	public ClassificationListDS(String id) {
		setDataFormat(DSDataFormat.JSON);
		setID(id);
		
		DataSourceTextField idField = new DataSourceTextField("id", "ID",50);  
        idField.setPrimaryKey(true);  
        idField.setRequired(true);
        
        setFields(idField);
        
		setAddDataURL(oa.getServiceUrl() + "/service/entity/create.json");
		setFetchDataURL(oa.getServiceUrl() + "/service/entity/search.json");
		setRemoveDataURL(oa.getServiceUrl() + "/service/entity/remove.json");
		setUpdateDataURL(oa.getServiceUrl() + "/service/entity/update.json");
	}
	
	@Override
	protected void transformResponse(DSResponse response, DSRequest request,Object data) {
		// TODO Auto-generated method stub
		super.transformResponse(response, request, data);
	}
}