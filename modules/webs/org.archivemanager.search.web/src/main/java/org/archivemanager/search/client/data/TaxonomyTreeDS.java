package org.archivemanager.search.client.data;

import org.heed.openapps.gwt.client.data.NativeSecurityModel;

import com.google.gwt.core.client.JavaScriptObject;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.util.JSOHelper;

public class TaxonomyTreeDS extends RestDataSource {
	private NativeSecurityModel security = new NativeSecurityModel();
	private static TaxonomyTreeDS instance = null;  
	
	
    public static TaxonomyTreeDS getInstance() {  
        if (instance == null) {  
            instance = new TaxonomyTreeDS("taxonomyTreeDS");  
        }  
        return instance;  
    }
    
	public TaxonomyTreeDS(String id) {
		setID(id);  
        setTitleField("Name");	        
        DataSourceTextField nameField = new DataSourceTextField("title", "Title");     
        
        DataSourceTextField idField = new DataSourceTextField("id", "ID",50);  
        idField.setPrimaryKey(true);  
        idField.setRequired(true);
        
        DataSourceTextField parentField = new DataSourceTextField("parent", "Parent");  
        parentField.setRequired(true);  
        parentField.setForeignKey(id + ".id");  
        parentField.setRootValue("null");  
        
        setFields(idField,nameField,parentField);
        
        setFetchDataURL("/service/search/taxonomy.xml");
	}
	
	@Override
	protected Object transformRequest(DSRequest dsRequest) {
		JavaScriptObject data = dsRequest.getData();
		
		JSOHelper.setAttribute(data, "ticket", security.getTicket());
		
		return data;
	}
}
