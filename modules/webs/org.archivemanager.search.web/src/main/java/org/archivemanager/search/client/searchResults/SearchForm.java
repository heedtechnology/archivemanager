package org.archivemanager.search.client.searchResults;

import java.util.LinkedHashMap;

import org.archivemanager.search.client.SearchEventTypes;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.OpenAppsEvent;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.SpacerItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;

public class SearchForm extends DynamicForm {

	
	
	public SearchForm() {
		setWidth(800);
		setNumCols(6);
		setMargin(1);
		
		SpacerItem spacer = new SpacerItem();
		
		final TextItem queryItem = new TextItem("query");
		queryItem.setShowTitle(false);
		queryItem.setWidth(300);
		queryItem.addKeyPressHandler(new KeyPressHandler() {
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if(event.getKeyName().equals("Enter")) {
					EventBus.fireEvent(new OpenAppsEvent(SearchEventTypes.SEARCH, getQuery()));
				}				
			}
			
		});
		//queryItem.setColSpan(2);
		
		ButtonItem submitItem = new ButtonItem("submit", "Search");
		submitItem.setStartRow(false);
		submitItem.setEndRow(false);
		/*
		final SelectItem repoItem = new SelectItem("repository", "Repositories");
		repoItem.setColSpan(4);
		repoItem.setWidth(300);
		
		final SelectItem collItem = new SelectItem("collection", "Collections");
		collItem.setColSpan(4);
		collItem.setWidth(300);
		*/
		final SelectItem qnameItem = new SelectItem("qname");
		LinkedHashMap<String,String> valueMap1 = new LinkedHashMap<String,String>();
		valueMap1.put("{openapps.org_repository_1.0}item", "Collections");
		qnameItem.setValueMap(valueMap1);
		qnameItem.setDefaultValue("{openapps.org_repository_1.0}item");
		qnameItem.setShowTitle(false);
		qnameItem.setWidth(150);
		
		final CheckboxItem sisItem = new CheckboxItem("sis", "Search-In-Search");
		
		submitItem.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				//record.setAttribute("collection", collItem.getValue());
				EventBus.fireEvent(new OpenAppsEvent(SearchEventTypes.SEARCH, getQuery()));
			}			
		});
		
		setFields(spacer,queryItem,submitItem,qnameItem,sisItem);
	}
	public Record getQuery() {
		Record record = new Record();
		record.setAttribute("query", this.getValue("query"));
		record.setAttribute("sis", this.getValue("sis"));
		record.setAttribute("qname", this.getValue("qname"));
		return record;
	}
}
