package org.archivemanager.search.server;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.heed.openapps.dictionary.ClassificationModel;
import org.heed.openapps.QName;
import org.heed.openapps.dictionary.RepositoryModel;
import org.heed.openapps.SystemModel;
import org.heed.openapps.entity.Association;
import org.heed.openapps.entity.BaseEntityQuery;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityQuery;
import org.heed.openapps.entity.EntityResultSet;
import org.heed.openapps.entity.EntityService;
import org.heed.openapps.entity.Property;
import org.heed.openapps.theme.ThemeVariables;
import org.heed.openapps.util.HTMLUtility;
import org.heed.openapps.util.JSONUtility;
import org.heed.openapps.util.IDName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class SearchClientController {
	@Autowired protected EntityService entityService;
	
	
	@RequestMapping(value="/client", method = RequestMethod.GET)
	public ModelAndView client(HttpServletRequest req, HttpServletResponse res) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		ThemeVariables vars = new ThemeVariables("smartclient");
		parms.put("theme", vars);
		
		
		return new ModelAndView("client", parms);
	}
	/*
	@RequestMapping(value="/client", method = RequestMethod.GET)
	public ModelAndView client(HttpServletRequest req, HttpServletResponse res) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		ThemeVariables vars = new ThemeVariables("smartclient");
		parms.put("theme", vars);
		parms.put("repositories", getRepositories());
		String query = req.getParameter("query");
		if(query != null && !query.equals("")) {
			String size = req.getParameter("size");  //page size for result set (default 10)
			String pageStr = req.getParameter("page");
			String repo = req.getParameter("repo");
			String coll = req.getParameter("coll");
			String sort = req.getParameter("sort");
			SearchRequest sQuery = new SearchRequest(RepositoryModel.ITEM, query);
			if(sort != null) {
				Sort lSort = null;
				String[] s = sort.split("_");
				if(s.length == 2) {
					boolean reverse = s[1].equals("a") ? true : false;
					if(s[0].equals("date")) lSort = new Sort(new SortField("date_expression_numeric", SortField.INT, reverse));
					else lSort = new Sort(new SortField(s[0], SortField.STRING, reverse));						
				} else if(s.length == 1) {
					if(s[0].equals("date")) lSort = new Sort(new SortField("date_expression_numeric", SortField.INT, false));
					else lSort = new Sort(new SortField(sort, SortField.STRING, false));
				}
				//sQuery.setSort(lSort);
			}
			sQuery.setPage((NumberUtility.isInteger(pageStr)) ? Integer.valueOf(pageStr) : 1);
			if(NumberUtility.isInteger(size)) sQuery.setPageSize(Integer.valueOf(size));
			if(repo != null && !repo.equals("")) {
				parms.put("repo", repo);
				sQuery.addParameter("path", repo);
			}
			if(coll != null && !coll.equals("")) {
				parms.put("coll", coll);
				sQuery.addParameter("path", coll);
			}
			SearchResponse result = searchService.search(sQuery);
			parms.put("query", query);
			parms.put("explanation", result.getQueryExplanation());
			parms.put("request", sQuery);
			parms.put("results", result);
		}
		return new ModelAndView("search", parms);
	}
	*/
	@RequestMapping(value="/category", method = RequestMethod.GET)
	public ModelAndView category(HttpServletRequest req, HttpServletResponse res) throws Exception {
		return component(req, res);
	}
	@RequestMapping(value="/detail", method = RequestMethod.GET)
	public ModelAndView component(HttpServletRequest req, HttpServletResponse res) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		ThemeVariables vars = new ThemeVariables("base");
		parms.put("theme", vars);
		String id = req.getParameter("id");
		//String fmt = req.getRequestURL().substring(req.getRequestURL().lastIndexOf("."));
		Entity comp = entityService.getEntity(Long.valueOf(id));
		parms.put("title", HTMLUtility.removeTags(comp.getPropertyValue(SystemModel.NAME)));
		parms.put("dateExpression", comp.getPropertyValue(RepositoryModel.DATE_EXPRESSION));
		parms.put("level1", comp.getPropertyValue(RepositoryModel.CATEGORY_LEVEL));
		parms.put("level2", comp.getQName().getLocalName());
		parms.put("container", comp.getPropertyValue(RepositoryModel.CONTAINER));
		parms.put("pathData", getComponentPath(req,comp));
		//parms.put("containerData", getContainerPath(comp));
		List<Association> childDocs = comp.getSourceAssociations(RepositoryModel.CATEGORIES);
		if(childDocs.size() > 0) {
			List<IDName> notes = new ArrayList<IDName>();
			for(Association doc : childDocs) {
				Entity child = entityService.getEntity(doc.getTarget());
				String name = child.getPropertyValue(SystemModel.NAME);
				if(name == null) name = child.getPropertyValue(RepositoryModel.DATE_EXPRESSION);
				if(name != null) notes.add(new IDName(String.valueOf(child.getId()), HTMLUtility.removeTags(name)));
			}
			parms.put("children", notes);
		}
		List<Association> noteDocs = comp.getSourceAssociations(SystemModel.NOTES);
		if(noteDocs.size() > 0) {
			List<Entity> notes = new ArrayList<Entity>();
			for(Association doc : noteDocs) {
				Entity note = entityService.getEntity(doc.getTarget());
				notes.add(note);
			}
			parms.put("notes", notes);
		}
		List<Association> nameDocs = comp.getSourceAssociations(ClassificationModel.NAMED_ENTITIES);
		if(nameDocs.size() > 0) {
			List<IDName> names = new ArrayList<IDName>();
			for(Association doc : nameDocs) {
				Entity note = entityService.getEntity(doc.getTarget());
				names.add(new IDName(String.valueOf(note.getId()), HTMLUtility.removeTags(note.getPropertyValue(SystemModel.NAME))));
			}
			parms.put("names", names);
		}
		List<Association> subjDocs = comp.getSourceAssociations(ClassificationModel.SUBJECTS);
		if(subjDocs.size() > 0) {
			List<IDName> subjs = new ArrayList<IDName>();
			for(Association doc : subjDocs) {
				Entity note = entityService.getEntity(doc.getTarget());
				subjs.add(new IDName(String.valueOf(note.getId()), HTMLUtility.removeTags(note.getPropertyValue(SystemModel.NAME))));
			}
			parms.put("subjects", subjs);
		}
		parms.put("images", "[]");
		/*
		List<Association> objDocs = comp.getSourceAssociations(DigitalObjectModel.COMPONENT_DIGITAL_OBJECTS);
		if(objDocs.size() > 0) {
			List<IDName> subjs = new ArrayList<IDName>();
			for(Document doc : objDocs) {
				Document note = searchSvc.document(doc.get("target_id"));
				String name = removeTags(null, note.get(ContentModel.DIGITAL_OBJECT_NAME.getLocalName()));
				if(name.length() == 0) name = "Name not available";
				String mimetype = note.get(ContentModel.DIGITAL_OBJECT_MIMETYPE.getLocalName());
				subjs.add(new IDName(note.get("id"), name, mimetype));
			}
			parms.put("objects", subjs);
		}
		*/
		return new ModelAndView("item", parms);
	}
	@RequestMapping(value="/service/{query}", method = RequestMethod.GET)
	public ModelAndView search(HttpServletRequest req, HttpServletResponse res,
			@PathVariable("query") String query) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		if(query != null && !query.equals("")) {
			
		}
		return new ModelAndView("results_xml", parms);
	}
	@RequestMapping(value="/service/{start}/{query}", method = RequestMethod.GET)
	public ModelAndView search(HttpServletRequest req, HttpServletResponse res,
			@PathVariable("start") long start, @PathVariable("query") String query) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		if(query != null && !query.equals("")) {
			
		}
		return new ModelAndView("results_xml", parms);
	}
	@RequestMapping(value="/service/{query}/{page}", method = RequestMethod.GET)
	public ModelAndView search(HttpServletRequest req, HttpServletResponse res,
			@PathVariable("query") String query, @PathVariable("page") int page) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		if(query != null && !query.equals("")) {
			
		}
		return new ModelAndView("results_xml", parms);
	}
	@RequestMapping(value="/service/{start}/{query}/{page}", method = RequestMethod.GET)
	public ModelAndView searchQueryPage(HttpServletRequest req, HttpServletResponse res,
			@PathVariable("start") long start, @PathVariable("query") String query, @PathVariable("page") int page) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		if(query != null && !query.equals("")) {
			
		}
		return new ModelAndView("results_xml", parms);
	}
	@RequestMapping(value="/service/{query}/{page}/{size}", method = RequestMethod.GET)
	public ModelAndView search(HttpServletRequest req, HttpServletResponse res,
			@PathVariable("query") String query, @PathVariable("page") int page, @PathVariable("size") int size) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		if(query != null && !query.equals("")) {
			
		}
		return new ModelAndView("results_xml", parms);
	}
	@RequestMapping(value="/service/{start}/{query}/{page}/{size}", method = RequestMethod.GET)
	public ModelAndView search(HttpServletRequest req, HttpServletResponse res,
			@PathVariable("start") long start, @PathVariable("query") String query, 
			@PathVariable("page") int page, @PathVariable("size") int size) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		if(query != null && !query.equals("")) {
			
		}
		return new ModelAndView("results_xml", parms);
	}
	@RequestMapping(value="/service/{query}/{page}/{size}/{sort}", method = RequestMethod.GET)
	public ModelAndView searchQueryPageSizeSort(HttpServletRequest req, HttpServletResponse res,
			@PathVariable("query") String query) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		if(query != null && !query.equals("")) {
			
		}
		return new ModelAndView("results_xml", parms);
	}
		
	public ModelAndView subject(HttpServletRequest req, HttpServletResponse res) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		ThemeVariables vars = new ThemeVariables("base");
		parms.put("theme", vars);
		String id = req.getParameter("id");
		String fmt = req.getRequestURL().substring(req.getRequestURL().lastIndexOf("."));
		Entity comp = entityService.getEntity(Long.valueOf(id));
		parms.put("comp", comp);
		List<Association> docs = comp.getTargetAssociations();
		List<IDName> comps = new ArrayList<IDName>();
		for(Association doc : docs) {
			Entity child = entityService.getEntity(doc.getSource());
			String name = child.getPropertyValue(SystemModel.NAME);
			if(name == null) name = child.getPropertyValue(RepositoryModel.DATE_EXPRESSION);
			comps.add(new IDName(String.valueOf(child.getId()), HTMLUtility.removeTags(name)));
		}
		parms.put("components", comps);
		return new ModelAndView("search/subject", parms);
	}
	public ModelAndView name(HttpServletRequest req, HttpServletResponse res) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		ThemeVariables vars = new ThemeVariables("base");
		parms.put("theme", vars);
		String id = req.getParameter("id");
		String fmt = req.getRequestURL().substring(req.getRequestURL().lastIndexOf("."));
		Entity comp = entityService.getEntity(Long.valueOf(id));
		parms.put("id", comp.getId());
		parms.put("comp", comp);
		if(comp.getQName().equals(ClassificationModel.PERSON)) parms.put("qname", "Person");
		else if(comp.getQName().equals(ClassificationModel.CORPORATION)) parms.put("qname", "Corporation");
		/*
		List<Document> docs = result.getTargetAssociations();
		List<IDName> comps = new ArrayList<IDName>();
		for(Document doc : docs) {
			Document child = searchSvc.document(doc.get("source_id"));
			String name = child.get(ContentModel.TITLE.getLocalName());
			if(name == null) child.get(ContentModel.COMPONENT_DATE_EXPRESSION.getLocalName());
			comps.add(new IDName(child.get("id"), removeTags(null, name)));
		}
		parms.put("components", comps);
		*/
		return new ModelAndView("name", parms);
	}
	
	protected String getComponentPath(HttpServletRequest req, Entity comp) throws Exception {
		StringBuffer buff = new StringBuffer("[");
		List<Entity> path = new ArrayList<Entity>();
		Association parent = comp.getTargetAssociation(RepositoryModel.CATEGORIES);
		while(parent != null) {
			Entity p = entityService.getEntity(parent.getSource());
			path.add(p);
			parent = p.getTargetAssociation(RepositoryModel.CATEGORIES);
		}
		Collections.reverse(path);
		if(path.size() > 0) {
			Long parentId = null;
			for(int i=0; i < path.size(); i++) {
				Entity node = path.get(i);
				String title = node.getPropertyValue(SystemModel.NAME);
				String dateExpression = node.getPropertyValue(RepositoryModel.DATE_EXPRESSION);
				if(!node.getQName().equals(RepositoryModel.REPOSITORY)) {
					if(title != null  && !title.equals("")) buff.append("{id:'"+node.getId()+"',title:'"+toJson(title)+"',type:'"+node.getQName().getLocalName()+"',parent:'"+parentId+"'},");
					else if(dateExpression != null) buff.append("{id:'"+node.getId()+"',title:'"+toJson(dateExpression)+"',type:'"+node.getQName().getLocalName()+"',parent:'"+parentId+"'},");
					parentId = node.getId();
				}
			}
			String title = comp.getPropertyValue(SystemModel.NAME);
			String dateExpression = comp.getPropertyValue(RepositoryModel.DATE_EXPRESSION);
			if(title != null && !title.equals("")) buff.append("{id:'"+comp.getId()+"',title:'<b>"+toJson(title)+"</b>',type:'"+comp.getQName().getLocalName()+"',parent:'"+parentId+"'},");
			else if(dateExpression != null) buff.append("{id:'"+comp.getId()+"',title:'<b>"+toJson(dateExpression)+"</b>',type:'"+comp.getQName().getLocalName()+"',parent:'"+parentId+"'},");
			parentId = comp.getId();
			for(Association node : comp.getSourceAssociations(RepositoryModel.CATEGORIES)) {
				Entity e = entityService.getEntity(node.getSource());
				title = e.getPropertyValue(SystemModel.NAME);
				dateExpression = e.getPropertyValue(RepositoryModel.DATE_EXPRESSION);
				if(title != null && !title.equals("")) buff.append("{id:'"+e.getId()+"',title:'"+toJson(title)+"',type:'"+e.getQName().getLocalName()+"',parent:'"+parentId+"'},");
				else if(dateExpression != null) buff.append("{id:'"+e.getId()+"',title:'"+toJson(dateExpression)+"',type:'"+e.getQName().getLocalName()+"',parent:'"+parentId+"'},");
			}			
		}
		return buff.append("]").toString();
	}
	protected String getRepositories() throws Exception {
		StringBuffer buff = new StringBuffer("{'0':'',");
		EntityQuery eQuery = new BaseEntityQuery(new QName("openapps.org_repository_1.0","repository"));
		eQuery.getFields().add("name");
		EntityResultSet results = entityService.search(eQuery);
		for(Entity e : results.getResults()) {
			Property repoName = e.getProperty(SystemModel.NAME);
			if(repoName != null) buff.append("'"+e.getId()+"':'"+repoName.toString()+"',");
		}
		return buff.substring(0, buff.length()-1).toString()+"}";
	}
	public String toJson(Object o) {
		if(o == null) return "";
		else return JSONUtility.escape(o.toString());
	}
	protected ModelAndView response(String message, String data, PrintWriter out) {
		out.write("<response><status>0</status><message>"+message+"</message><data>"+data+"</data></response>");
		return null;
	}
}
