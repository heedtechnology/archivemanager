package org.archivemanager.search.client;

import com.smartgwt.client.types.ImageStyle;
import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.layout.VLayout;

public class SplashScreen extends VLayout {
	private Img logoImage;
	private Img loadingImage;
	
	public SplashScreen() {
		setWidth100();
        setHeight100();
        setBorder("1px solid #A6ABB4");
                
        logoImage = new Img("/theme/images/logo/ArchiveManager500.png", 500, 91);  
        logoImage.setImageType(ImageStyle.NORMAL); 
        logoImage.setLeft(350);
        logoImage.setTop(300);
        logoImage.setVisible(true);
        addChild(logoImage);
        
        loadingImage = new Img("/theme/images/loading.gif", 100, 100);  
        loadingImage.setImageType(ImageStyle.NORMAL); 
        loadingImage.setLeft(550);
        loadingImage.setTop(300);
        loadingImage.setVisible(false);
        addChild(loadingImage);
	}
	public void logo() {
		loadingImage.hide();
		logoImage.show();
		show();
	}
	public void loading() {
		logoImage.hide();
		loadingImage.show();
		show();
	}
}
