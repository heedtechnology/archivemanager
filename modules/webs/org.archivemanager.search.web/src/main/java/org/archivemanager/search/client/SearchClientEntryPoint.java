package org.archivemanager.search.client;

import org.archivemanager.search.client.searchResults.SearchForm;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.OpenAppsEventHandler;
import org.heed.openapps.gwt.client.data.RestUtility;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.http.client.URL;
import com.google.gwt.user.client.DOM;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.XMLTools;
import com.smartgwt.client.types.Positioning;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class SearchClientEntryPoint extends VLayout implements EntryPoint {
	private static final int height = 900;
	private static final int width = 1200;
	
	private SplashScreen splash;
	private SearchForm searchForm;
	private SearchResultsModule searchResults;
	private SearchDetailModule searchDetail;
		
	private int pageNumber = 1;
	private int pageCount = 1;
	private String query;
	
	public void onModuleLoad() {
		setHeight(height);  
        setWidth(width);
        
        HLayout search = new HLayout();
        search.setWidth100();
        search.setHeight(35);
        search.setMargin(1);
        search.setBorder("1px solid #A6ABB4");
        addMember(search);
        
        searchForm = new SearchForm();
        searchForm.setWidth(500);
        searchForm.setMargin(5);
        search.addMember(searchForm);
        
        Canvas canvas = new Canvas();
        addMember(canvas);
        
        splash = new SplashScreen();        
        canvas.addChild(splash);
        
        searchResults = new SearchResultsModule();
        searchResults.hide();
        canvas.addChild(searchResults);
        
        searchDetail = new SearchDetailModule();
        searchDetail.hide();
        canvas.addChild(searchDetail);
                
        startup();
        
        setHtmlElement(DOM.getElementById("gwt"));
        setPosition(Positioning.RELATIVE);
        draw();
	}

	protected void startup() {
		EventBus.addHandler(org.heed.openapps.gwt.client.OpenAppsEvent.TYPE, new OpenAppsEventHandler()     {
	        @Override
	        public void onEvent(OpenAppsEvent event) {
	        	if(event.isType(SearchEventTypes.SEARCH)) {
	        		searchDetail.hide();
		        	searchResults.hide();
		        	splash.loading();
	            	pageNumber = 1;
	            	boolean sis = event.getRecord().getAttributeAsBoolean("sis");
	            	event.getRecord().setAttribute("size", "15");
	            	if(sis) {
	            		query = query+"//"+event.getRecord().getAttribute("query");
	            		event.getRecord().setAttribute("query", query);	            		
	            	} else query = event.getRecord().getAttribute("query");
	            	get("/service/opensearch/search.xml", event.getRecord(), new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							String count = XMLTools.selectString(rawData, "//response/data/results/@pageCount");
							if(count != null && !count.equals("")) pageCount = Integer.valueOf(count);
							searchResults.setRawAttributeData(rawData);
							searchResults.setRawResultData(rawData);
							searchResults.setRawBreadcrumbData(rawData);
							searchDetail.hide();							
							searchResults.show();
							splash.hide();
						}						
					});
	            } else if(event.isType(SearchEventTypes.PAGING)) {
	            	//searchDetail.hide();
		        	//searchResults.hide();
		        	//splash.loading();
	            	String action = event.getRecord().getAttributeAsString("action");
	            	if(action != null) {
	            		if(action.equals("next")) pageNumber++;
	            		else if(action.equals("prev")) pageNumber--;
	            	} else {
	            		pageNumber = event.getRecord().getAttributeAsInt("pageNumber");
	            	}
	            	Record record = searchForm.getQuery();
					record.setAttribute("page", pageNumber);
	            	record.setAttribute("size", "15");
	            	get("/service/opensearch/search.xml", record, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							searchResults.setRawResultData(rawData);
							//splash.hide();
							searchResults.show();
						}						
					});
	            } else if(event.isType(SearchEventTypes.PAGING_FIRST)) {
	            	//searchDetail.hide();
		        	//searchResults.hide();
		        	//splash.loading();
	            	pageNumber = 1;
	            	Record record = searchForm.getQuery();
	            	record.setAttribute("page", pageNumber);
	            	record.setAttribute("size", "15");
	            	get("/service/opensearch/search.xml", record, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							searchResults.setRawResultData(rawData);
							//splash.hide();
							searchResults.show();
						}						
					});
	            } else if(event.isType(SearchEventTypes.PAGING_LAST)) {
	            	//searchDetail.hide();
		        	//searchResults.hide();
		        	//splash.loading();
	            	pageNumber = pageCount;
	            	Record record = searchForm.getQuery();
	            	record.setAttribute("page", pageNumber);
	            	record.setAttribute("size", "15");
	            	get("/service/opensearch/search.xml", record, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							searchResults.setRawResultData(rawData);
							//splash.hide();
							searchResults.show();
						}						
					});
	            } else if(event.isType(SearchEventTypes.ATTRIBUTE)) {
	            	//searchDetail.hide();
		        	//searchResults.hide();
		        	//splash.loading();
	            	pageNumber = 1;
	            	String attrQuery = event.getRecord().getAttribute("query");
	            	query = query+"//"+attrQuery;
	            	event.getRecord().setAttribute("query", query);
	            	event.getRecord().setAttribute("size", "15");
	            	get("/service/opensearch/search.xml", event.getRecord(), new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							String count = XMLTools.selectString(rawData, "//response/data/results/@pageCount");
							pageCount = Integer.valueOf(count);
							searchResults.setRawAttributeData(rawData);
							searchResults.setRawResultData(rawData);
							searchResults.setRawBreadcrumbData(rawData);
							searchDetail.hide();
							searchResults.show();
							//splash.hide();
						}						
					});	
	            } else if(event.isType(SearchEventTypes.DETAIL)) {
	            	//searchDetail.hide();
		        	//searchResults.hide();
		        	//splash.loading();
	            	RestUtility.get("/service/entity/get/"+event.getRecord().getAttribute("id")+".xml", new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							if(response.getData() != null && response.getData().length > 0) {
								searchDetail.setData(response.getData()[0]);
								searchResults.hide();
								searchDetail.show();
								//splash.hide();
							} else {
								//display error message
							}
						}						
					});
	            } else if(event.isType(SearchEventTypes.RESULTS)) {
	            	searchDetail.hide();
	            	searchResults.show();
	            	splash.hide();
	            }
	        }
	    });
	}
	
	public void get(String url, Record record, final DSCallback callback) {
		RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, URL.encode(url)+"?"+toParameterString(record));
		//builder.setHeader("Content-Type", "application/x-www-form-urlencoded");
		try {
			builder.sendRequest(null, new RequestCallback() {
				public void onError(Request request, Throwable exception) {}
				public void onResponseReceived(Request request, Response response) {
					if (200 == response.getStatusCode()) {
						String data = response.getText();
						String message = XMLTools.selectString(data, "//message");
						DSResponse res = new DSResponse();
						res.setAttribute("message", message);
						DSRequest req = new DSRequest();
						callback.execute(res, data, req);
					} else {
						// Handle the error.  Can get the status text from response.getStatusText()
					}
				}       
			});
		} catch (RequestException e) {
		  // Couldn't connect to server        
		}	
	}
	public String toParameterString(Record record) {
		String requestData = new String();
		for(String key : record.getAttributes()) {
			if(!key.equals("__ref")) requestData += key+"="+record.getAttribute(key)+"&";
		}
		if(requestData.endsWith("&")) requestData = requestData.substring(0, requestData.length()-1);
		return URL.encode(requestData);
	}
}
