package org.archivemanager.search.client;

import org.archivemanager.search.client.searchResults.AttributeDisplay;
import org.archivemanager.search.client.searchResults.BreadcrumbDisplay;
import org.archivemanager.search.client.searchResults.PagingDisplay;
import org.archivemanager.search.client.searchResults.ResultsDisplay;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.user.client.Element;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.XMLTools;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class SearchResultsModule extends VLayout {
	private DataSource ds = new DataSource();
	private AttributeDisplay attributeDisplay;
	private ResultsDisplay resultDisplay;
	private BreadcrumbDisplay breadcrumb;
	private PagingDisplay paging;
		
	
	public SearchResultsModule() {
		setWidth100();
		setHeight100();
		        
        breadcrumb = new BreadcrumbDisplay();
        addMember(breadcrumb);
                
        paging = new PagingDisplay();
        addMember(paging);
        
        HLayout body = new HLayout();
        attributeDisplay = new AttributeDisplay();
        //attributeDisplay.setShowResizeBar(true);
        body.addMember(attributeDisplay);
        resultDisplay = new ResultsDisplay();
        body.addMember(resultDisplay);
        addMember(body);
	}
	
	public void setRawAttributeData(Object rawData) {
		try {
			JsArray<Element> nodes = ((JavaScriptObject)XMLTools.selectNodes(rawData, "//response/data/attributes/attribute")).cast();
			Record[] attributes = ds.recordsFromXML(nodes);
			if(attributes != null) attributeDisplay.setAttributes(attributes);
		} catch(ClassCastException e) {	}
	}
	public void setRawResultData(Object rawData) {
		try {
			JsArray<Element> nodes = ((JavaScriptObject)XMLTools.selectNodes(rawData, "//response/data/results/result")).cast();
			Record[] results = ds.recordsFromXML(nodes);
			if(results != null && results.length > 0) {
				resultDisplay.setResults(results);
				String page = XMLTools.selectString(rawData, "//response/data/request/@page");
				String pageCount = XMLTools.selectString(rawData, "//response/data/results/@pageCount");
				String resultCount = XMLTools.selectString(rawData, "//response/data/results/@resultCount");
				String time = XMLTools.selectString(rawData, "//response/data/results/@time");
				paging.setPagingData(Integer.valueOf(page), Integer.valueOf(pageCount), Integer.valueOf(resultCount), Long.valueOf(time));
			}
		} catch(ClassCastException e) {	}
	}
	public void setRawBreadcrumbData(Object rawData) {
		breadcrumb.setRawBreadcrumbData(rawData);
	}
}
