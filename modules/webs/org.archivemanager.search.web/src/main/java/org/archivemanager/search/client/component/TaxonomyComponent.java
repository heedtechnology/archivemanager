package org.archivemanager.search.client.component;

import org.archivemanager.search.client.data.TaxonomyTreeDS;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.widgets.tree.TreeGrid;

public class TaxonomyComponent extends TreeGrid {
	
	
	public TaxonomyComponent() {
		setHeight100();
		setWidth100();
		setShowHeader(false);
		setBorder("0px");
		setWrapCells(true);
		setFixedRecordHeights(false);
		setCanReorderRecords(false);  
        setCanAcceptDroppedRecords(false);
        setSelectionType(SelectionStyle.NONE);
        setDataSource(TaxonomyTreeDS.getInstance());
        
	}
	public void select(Record record) {
		Criteria criteria = new Criteria();
		criteria.addCriteria("id", record.getAttribute("id"));
		fetchData(criteria, new DSCallback() {
			public void execute(DSResponse response, Object rawData,DSRequest request) {
				getData().openAll();
			}			
		});
	}
	
}
