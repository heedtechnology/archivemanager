package org.archivemanager.search.client.searchResults;

import org.archivemanager.search.client.SearchEventTypes;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.OpenAppsEvent;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.user.client.Element;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.XMLTools;
import com.smartgwt.client.types.Cursor;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.MouseOutEvent;
import com.smartgwt.client.widgets.events.MouseOutHandler;
import com.smartgwt.client.widgets.events.MouseOverEvent;
import com.smartgwt.client.widgets.events.MouseOverHandler;
import com.smartgwt.client.widgets.layout.HLayout;

public class BreadcrumbDisplay extends HLayout {
	private DataSource ds = new DataSource();
	
	
	public BreadcrumbDisplay() {
		setWidth100();
		setHeight(35);
		setMargin(1);
        setBorder("1px solid #A6ABB4");
	}
	
	public void setRawBreadcrumbData(Object rawData) {
		try {
			JsArray<Element> nodes = ((JavaScriptObject)XMLTools.selectNodes(rawData, "//response/data/breadcrumb/crumb")).cast();
			Record[] crumbs = ds.recordsFromXML(nodes);
			removeMembers(getMembers());
			if(crumbs != null) {				
				for(int i=0; i < crumbs.length; i++) {
					final Record crumbRec = crumbs[i];
					final Label crumb = new Label("<div style='color:#A6ABB4;font-weight:bold;font-size:16px;'><li>"+crumbRec.getAttribute("label")+"</li></div>");
					crumb.setWidth(20);
					crumb.setMargin(5);
					crumb.setWrap(false);
					if(i < crumbs.length-1) {
						crumb.addClickHandler(new ClickHandler() {
							public void onClick(ClickEvent event) {
								Record record = new Record();
								record.setAttribute("query", crumbRec.getAttribute("query"));
								EventBus.fireEvent(new OpenAppsEvent(SearchEventTypes.SEARCH, record));
							}
						});
						crumb.addMouseOverHandler(new MouseOverHandler() {
							public void onMouseOver(MouseOverEvent event) {
								crumb.setStyleName("titleOver");
								crumb.setCursor(Cursor.HAND);
							}			
						});
						crumb.addMouseOutHandler(new MouseOutHandler() {
							public void onMouseOut(MouseOutEvent event) {
								crumb.setStyleName("titleOut");
							}			
						});
					}
					addMember(crumb);
				}
			}
		} catch(ClassCastException e) {}
	}
}
