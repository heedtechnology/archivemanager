package org.archivemanager.search.client.component;

import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.EventTypes;
import org.heed.openapps.gwt.client.OpenAppsEvent;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Cursor;
import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.MouseOverEvent;
import com.smartgwt.client.widgets.events.MouseOverHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class ThumbnailComponent extends HLayout {
	
	
	public ThumbnailComponent(String width, String height) {
		setWidth(width);
		setHeight(height);
		setBorder("1px solid #A6ABB4");
	}
	
	public void setData(Record record) {
		try {
			Record source_associations = record.getAttributeAsRecord("source_associations");
			if(source_associations != null) {
				Record files = source_associations.getAttributeAsRecord("files");
				if(files != null) {
					setData(files.getAttributeAsRecordArray("node"));
				} else setData(new Record[0]);
			}
		} catch(ClassCastException e) {
			setData(new Record[0]);
		}
	}
	public void setData(Record[] data) {
		removeMembers(getMembers());
		for(final Record record : data) {
			VLayout layout = new VLayout();
			String name = record.getAttribute("name");
			String extension = getExtension(name);
			final Img thumb = new Img("/theme/images/icons32/file_extension/file_extension_"+extension+".png");
			thumb.setPrompt(name);
			thumb.setWidth(50);
			thumb.setHeight(50);
			thumb.setMargin(5); 
			thumb.setVisible(true);
			layout.addMember(thumb);
			thumb.addMouseOverHandler(new MouseOverHandler() {
				public void onMouseOver(MouseOverEvent event) {
					thumb.setCursor(Cursor.HAND);
				}			
			});
			thumb.addClickHandler(new ClickHandler() {
				public void onClick(ClickEvent event) {
					EventBus.fireEvent(new OpenAppsEvent(EventTypes.SELECTION, record));
				}
			});
	        addMember(layout);	        
		}
	}
	
	protected String getExtension(String filename) {
		if (filename == null) {
			return null;
		}
		// get extension
		int pos = filename.lastIndexOf('.');
		if (pos == -1 || pos == filename.length() - 1)
			return null;
		String ext = filename.substring(pos + 1, filename.length());
		ext = ext.toLowerCase();
		return ext;
	}
}
