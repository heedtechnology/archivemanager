package org.archivemanager.search.client.searchResults;

import org.archivemanager.search.client.SearchEventTypes;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.OpenAppsEvent;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Cursor;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.HTMLFlow;
import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.MouseOutEvent;
import com.smartgwt.client.widgets.events.MouseOutHandler;
import com.smartgwt.client.widgets.events.MouseOverEvent;
import com.smartgwt.client.widgets.events.MouseOverHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class ResultsDisplay extends VLayout {

	
	
	public ResultsDisplay() {
		setWidth100();
		setHeight100();
		setMargin(1);
		setBorder("1px solid #A6ABB4");
	}
	
	public void setResults(Record[] results) {
		removeMembers(getMembers());
		if(results != null) {
			for(Record result : results) {
				String score = result.getAttribute("score");
				Record node = result.getAttributeAsRecord("node");
				String id = node.getAttribute("id");
				String name = node.getAttribute("name");
				if(name == null || name.length() == 0) name = node.getAttribute("date_expression");
				String description = node.getAttribute("description");
				int subjects = 0;
				int names = 0;
				try {
					Record source_associations = node.getAttributeAsRecord("source_associations");
					Record notes = source_associations.getAttributeAsRecord("notes");
					Record[] note_nodes = notes.getAttributeAsRecordArray("node");
					if(description == null || description.length() == 0) {
						for(Record note_node : note_nodes) {
							String type = note_node.getAttribute("type");
							if(type.equals("General Physical Description note")) description = note_node.getAttribute("content");
						}
					}					
					Record named_entities = source_associations.getAttributeAsRecord("named_entities");
					if(named_entities != null) {
						Record[] named_entity_nodes = named_entities.getAttributeAsRecordArray("node");
						names = named_entity_nodes.length;
					}					
					Record subjects_ = source_associations.getAttributeAsRecord("subjects");
					if(subjects_ != null) {
						Record[] subject_nodes = subjects_.getAttributeAsRecordArray("node");
						subjects = subject_nodes.length;
					}
				} catch(Exception e) {}
				HLayout display = new HLayout();
				display.setMargin(3);
				display.setWidth100();
				display.setHeight(52);
				display.setBorder("1px solid #A6ABB4");
				addMember(display);
				
				Canvas relevance = getRelevanceImage(Integer.valueOf(score));
				display.addMember(relevance);
				
				VLayout bodyLayout = new VLayout();
				Canvas titleRow = getTitleRow(id, name, names, subjects);
				bodyLayout.addMember(titleRow);
				Canvas descriptionRow = getDescriptionRow(description);
				bodyLayout.addMember(descriptionRow);
				
				display.addMember(bodyLayout);
			}
		}
	}
	
	protected Canvas getRelevanceImage(int normalizedScore) {
		int score = 100;
		if(normalizedScore < 9) score = 8;
		else if(normalizedScore < 17) score = 16;
		else if(normalizedScore < 25) score = 24;
		else if(normalizedScore < 33) score = 32;
		else if(normalizedScore < 41) score = 40;
		else if(normalizedScore < 49) score = 48;
		else if(normalizedScore < 57) score = 56;
		else if(normalizedScore < 64) score = 64;
		else if(normalizedScore < 73) score = 72;
		else if(normalizedScore < 81) score = 80;
		else if(normalizedScore < 89) score = 88;
		else if(normalizedScore < 97) score = 96;
		Canvas canvas = new Canvas();
		canvas.setWidth(50);
		canvas.setMargin(5);
		Img img = new Img("/theme/images/progress/"+score+"percent.png", 30, 10);
		img.setPrompt(normalizedScore+" / 100");
		canvas.addChild(img);
		return canvas;
	}
	protected Canvas getTitleRow(final String id, String name, int names, int subjects) {
		HLayout layout = new HLayout();
		layout.setMembersMargin(5);
		final HTMLFlow canvas = new HTMLFlow("<b>"+name+"</b>");
		canvas.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Record record = new Record();
				record.setAttribute("id", id);
				EventBus.fireEvent(new OpenAppsEvent(SearchEventTypes.DETAIL, record));
			}
		});
		canvas.addMouseOverHandler(new MouseOverHandler() {
			public void onMouseOver(MouseOverEvent event) {
				canvas.setStyleName("titleOver");
				canvas.setCursor(Cursor.HAND);
			}			
		});
		canvas.addMouseOutHandler(new MouseOutHandler() {
			public void onMouseOut(MouseOutEvent event) {
				canvas.setStyleName("titleOut");
			}			
		});
		canvas.setWidth100();
		canvas.setHeight(10);
		canvas.setMargin(5);
		layout.addMember(canvas);
		
		if(names > 0) {
			Label label = new Label("Names ("+names+")");
			label.setWidth(75);
			layout.addMember(label);
		}
		if(subjects > 0) {
			Label label = new Label("Subjects ("+subjects+")");
			label.setWidth(75);
			layout.addMember(label);
		}
		//canvas.setBorder("1px solid black");
		return layout;
	}
	protected Canvas getDescriptionRow(String name) {
		HLayout layout = new HLayout();
		HTMLFlow canvas = new HTMLFlow(name);
		canvas.setWidth100();
		canvas.setHeight(10);
		//canvas.setBorder("1px solid black");
		Canvas leftSpacer = new Canvas();
		leftSpacer.setWidth(25);
		layout.addMember(leftSpacer);
		layout.addMember(canvas);
		return layout;
	}
}
