package org.archivemanager.search.client;

import org.archivemanager.search.client.component.TaxonomyComponent;
import org.archivemanager.search.client.component.ThumbnailComponent;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.EventTypes;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.OpenAppsEventHandler;
import org.heed.openapps.gwt.client.component.AdvancedContentViewer;
import org.heed.openapps.gwt.client.component.NamedEntityComponent;
import org.heed.openapps.gwt.client.component.NotesComponent;
import org.heed.openapps.gwt.client.component.SubjectComponent;
import org.heed.openapps.gwt.client.data.NativeClassificationModel;
import org.heed.openapps.gwt.client.data.NativeRepositoryModel;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Cursor;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.HTMLFlow;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.MouseOutEvent;
import com.smartgwt.client.widgets.events.MouseOutHandler;
import com.smartgwt.client.widgets.events.MouseOverEvent;
import com.smartgwt.client.widgets.events.MouseOverHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class SearchDetailModule extends VLayout {
	private NativeClassificationModel model = new NativeClassificationModel();
	private NativeRepositoryModel repositoryModel = new NativeRepositoryModel();
	
	private NamedEntityComponent nameComponent;
	private SubjectComponent subjectComponent;
	private DynamicForm detailForm;
	private NotesComponent notesComponent;
	private AdvancedContentViewer viewer;
	private ThumbnailComponent thumbnails;
	private TaxonomyComponent taxonomy;
	
	
	public SearchDetailModule() {
		setWidth100();
		setHeight100();
		setMargin(1);
		setBorder("1px solid #A6ABB4");
        
		HLayout topLayout = new HLayout();
		topLayout.setHeight(200);
		addMember(topLayout);
		
        VLayout topLeftLayout = new VLayout();
        topLeftLayout.setBorder("1px solid #A6ABB4");
        topLeftLayout.setMargin(5);
        topLayout.addMember(topLeftLayout);
        
        HTMLFlow title = new HTMLFlow("<span style='font-weight:bold;font-size:18px;'>Search Detail</span>");
        title.setWidth100();
        //title.setHeight(50);
        title.setMargin(5);
        
        topLeftLayout.addMember(title);
        
        final HTMLFlow backLink = getBackLink();
        topLeftLayout.addMember(backLink);
        
        detailForm = getDetailForm();
        topLeftLayout.addMember(detailForm);
        
        VLayout topRightLayout = new VLayout();
        topRightLayout.setHeight(257);
        topRightLayout.setBorder("1px solid #A6ABB4");
        topRightLayout.setMargin(5);
        topLayout.addMember(topRightLayout);
        
        taxonomy = new TaxonomyComponent();
        topRightLayout.addMember(taxonomy);
        
        HLayout middleLayout = new HLayout();
        addMember(middleLayout);
        
        VLayout middleLeftLayout = new VLayout();
        middleLayout.addMember(middleLeftLayout);
        
        notesComponent = new NotesComponent("search", "100%", false, repositoryModel.getCollectionNoteTypes());
		notesComponent.setMargin(5);
		middleLeftLayout.addMember(notesComponent);
		
		HLayout classificationLayout = new HLayout();
		middleLeftLayout.addMember(classificationLayout);
		
        subjectComponent = new SubjectComponent("100%", false);
		subjectComponent.setMargin(5);
		classificationLayout.addMember(subjectComponent);
		
		nameComponent = new NamedEntityComponent("100%", false, false, false, model.getNamedEntityFunction(), model.getNamedEntityRole());
		nameComponent.setMargin(5);
		classificationLayout.addMember(nameComponent);
		
		Canvas spacer = new Canvas();
		spacer.setHeight100();
		middleLeftLayout.addMember(spacer);
		
		VLayout middleRightLayout = new VLayout();
		middleRightLayout.setWidth(485);
        middleLayout.addMember(middleRightLayout);
        
        thumbnails = new ThumbnailComponent("100%", "50px");
        thumbnails.setMargin(5);
        middleRightLayout.addMember(thumbnails);
        
        viewer = new AdvancedContentViewer("100%", "550");
        viewer.setMargin(5);
        middleRightLayout.addMember(viewer);
                
        EventBus.addHandler(org.heed.openapps.gwt.client.OpenAppsEvent.TYPE, new OpenAppsEventHandler()     {
	        @Override
	        public void onEvent(OpenAppsEvent event) {
	        	if(event.isType(EventTypes.SELECTION)) {
	        		viewer.select(event.getRecord());
	        	}
	        }
        });
	}
	
	public void setData(Record data) {
		try {
			notesComponent.select(data);
			nameComponent.select(data);
			subjectComponent.select(data);
			detailForm.editRecord(data);
			thumbnails.setData(data);
			viewer.clearImage();
			taxonomy.select(data);
		} catch(ClassCastException e) {}
	}
	
	protected DynamicForm getDetailForm() {
		DynamicForm form = new DynamicForm();
		form.setWidth100();
		form.setHeight100();
		form.setColWidths(100,"*");
		form.setCellPadding(5);
		//form.setBorder("1px solid #A6ABB4");
		form.setMargin(10);
		StaticTextItem nameItem = new StaticTextItem("name", "Name");
		StaticTextItem typeItem = new StaticTextItem("localName", "Content Type");
		StaticTextItem dateItem = new StaticTextItem("date_expression", "Date");
		StaticTextItem languageItem = new StaticTextItem("language", "Language");
		StaticTextItem descriptionItem = new StaticTextItem("description", "Description");
		StaticTextItem summaryItem = new StaticTextItem("summary", "Summary");
		StaticTextItem containerItem = new StaticTextItem("container", "Container");
		form.setFields(nameItem,dateItem,typeItem,languageItem,containerItem,descriptionItem,summaryItem);
		return form;
	}
	protected HTMLFlow getBackLink() {
		final HTMLFlow backLink = new HTMLFlow("<span style='font-size:12px;'>(Back To Results)</span>");
        backLink.setWidth100();
        backLink.setMargin(5);
        
        backLink.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				EventBus.fireEvent(new OpenAppsEvent(SearchEventTypes.RESULTS));
			}
		});
        backLink.addMouseOverHandler(new MouseOverHandler() {
			public void onMouseOver(MouseOverEvent event) {
				backLink.setStyleName("titleOver");
				backLink.setCursor(Cursor.HAND);
			}			
		});
        backLink.addMouseOutHandler(new MouseOutHandler() {
			public void onMouseOut(MouseOutEvent event) {
				backLink.setStyleName("titleOut");
			}			
		});
        return backLink;
	}
}
