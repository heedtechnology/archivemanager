package org.archivemanager.search.client.searchResults;

import org.archivemanager.search.client.SearchEventTypes;
import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.OpenAppsEvent;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Cursor;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.widgets.HTMLFlow;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.MouseOutEvent;
import com.smartgwt.client.widgets.events.MouseOutHandler;
import com.smartgwt.client.widgets.events.MouseOverEvent;
import com.smartgwt.client.widgets.events.MouseOverHandler;
import com.smartgwt.client.widgets.layout.SectionStack;
import com.smartgwt.client.widgets.layout.SectionStackSection;
import com.smartgwt.client.widgets.layout.VLayout;

public class AttributeDisplay extends VLayout {
	private SectionStack stack;
	
	
	public AttributeDisplay() {
		setBorder("1px solid #A6ABB4");
		setMargin(1);
		stack = new SectionStack();
		stack.setScrollSectionIntoView(true);
        stack.setWidth(325);
        stack.setHeight100();
        stack.setBorder("0px");
        addMember(stack);
	}
	
	public void setAttributes(Record[] attributes) {
		stack.removeMembers(stack.getMembers());
		SectionStackSection[] sections = new SectionStackSection[attributes.length];
		for(int i=0; i < attributes.length; i++) {
			SectionStackSection section = new SectionStackSection(attributes[i].getAttribute("name"));
			VLayout sectionLayout = new VLayout();
			sectionLayout.setOverflow(Overflow.AUTO);
			Record[] values = attributes[i].getAttributeAsRecordArray("value");
			for(Record value : values) {
				String name = value.getAttribute("name");
				String count = value.getAttribute("count");
				final HTMLFlow display = new HTMLFlow(name+" ("+count+")");
				display.setMargin(5);
				final String query = value.getAttribute("query");
				display.setMargin(1);
				display.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						Record record = new Record();
						record.setAttribute("query", query);
						EventBus.fireEvent(new OpenAppsEvent(SearchEventTypes.ATTRIBUTE, record));
					}
				});
				display.addMouseOverHandler(new MouseOverHandler() {
					public void onMouseOver(MouseOverEvent event) {
						display.setStyleName("titleOver");
						display.setCursor(Cursor.HAND);
					}			
				});
				display.addMouseOutHandler(new MouseOutHandler() {
					public void onMouseOut(MouseOutEvent event) {
						display.setStyleName("titleOut");
					}			
				});
				sectionLayout.addMember(display);
			}
			section.addItem(sectionLayout);
			sections[i] = section;
		}
		stack.setSections(sections);
	}
}
