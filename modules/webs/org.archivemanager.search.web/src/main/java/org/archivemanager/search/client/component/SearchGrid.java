package org.archivemanager.search.client.component;

import java.util.LinkedHashMap;

import org.archivemanager.search.client.data.RepositoryDataSource;
import org.archivemanager.search.client.data.SearchResultDataSource;

import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.http.client.URL;
import com.google.gwt.user.client.Element;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.XMLTools;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.layout.VLayout;

public class SearchGrid extends VLayout {
	private ListGrid resultGrid;
	private final SelectItem repositoryField;
	private final SelectItem collectionField;
	
	
	public SearchGrid() {
		final DynamicForm searchBar = new DynamicForm();
		searchBar.setWidth("100%");
		searchBar.setHeight(25);
		searchBar.setColWidths(90,120,90,120,75,"*");
		searchBar.setNumCols(6);
		
		repositoryField = new SelectItem("repository", "Repository");
		repositoryField.setValueField("id");
		repositoryField.setDisplayField("name");
		repositoryField.setWidth(275);
		repositoryField.setOptionDataSource(RepositoryDataSource.getInstance());
		repositoryField.addChangedHandler(new ChangedHandler() {  
            public void onChanged(ChangedEvent event) {  
            	searchBar.clearValue("collection");
            	String repository = (String) repositoryField.getValue();  
            	RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, URL.encode("/repository/collections/"+repository));
        		try {
        			Request request = builder.sendRequest(null, new RequestCallback() {
        				public void onError(Request request, Throwable exception) {}
        				public void onResponseReceived(Request request, Response response) {
        					if (200 == response.getStatusCode()) {
        						String data = response.getText();
        						JsArray<Element> nodes = ((JavaScriptObject)XMLTools.selectNodes(data, "//data/node")).cast();
        						LinkedHashMap<String,String> valueMap = new LinkedHashMap<String,String>();
        						for(int i = 0; i < nodes.length(); i++) {
        							Element node = nodes.get(i);
        							String id = node.getAttribute("id");
        							String title = node.getFirstChildElement().getInnerText();
        							valueMap.put(id, title);
        						}
        						collectionField.setValueMap(valueMap);
        					} else {
        						// Handle the error.  Can get the status text from response.getStatusText()
        					}
        				}       
        			});
        		} catch (RequestException e) {
        		  // Couldn't connect to server        
        		}
            }  
        });
		
		collectionField = new SelectItem("collection", "Collection") {
			 
		};
		collectionField.setWidth(350);          
				
		ButtonItem searchButton = new ButtonItem("search");
		searchButton.setIcon("/theme/images/icons/search_icon.gif");
		searchButton.setWidth(65);
		searchButton.setStartRow(false);
		searchButton.setEndRow(false);
		searchButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				Criteria criteria = new Criteria();
				if(searchBar.getValue("repository") != null) {
					criteria.addCriteria("repository", searchBar.getValueAsString("repository"));
					if(searchBar.getValue("collection") != null) 
						criteria.addCriteria("collection", searchBar.getValueAsString("collection"));
					resultGrid.fetchData(criteria);			
				}
			}
		});		
		searchBar.setFields(repositoryField, collectionField, searchButton);
		addMember(searchBar);
		
		resultGrid = new ListGrid();
		resultGrid.setDataSource(new SearchResultDataSource());
		resultGrid.setWidth100();
		resultGrid.setHeight100();

		ListGridField idField = new ListGridField("id", 100);		
		ListGridField nameField = new ListGridField("name");
		
		resultGrid.setFields(idField, nameField);
		
		addMember(resultGrid);
	}
}
