package org.archivemanager.search.client.data;

import org.heed.openapps.gwt.client.data.NativeSecurityModel;

import com.google.gwt.core.client.JavaScriptObject;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.util.JSOHelper;

public class SearchResultDataSource extends RestDataSource {
	private NativeSecurityModel security = new NativeSecurityModel();
	
	public SearchResultDataSource() {
		setID(id);  
        setTitleField("Name");
        
        DataSourceTextField nameField = new DataSourceTextField("title", "Title");  
        
        DataSourceTextField idField = new DataSourceTextField("id", "ID",50);  
        idField.setPrimaryKey(true);  
        idField.setRequired(true); 
                
        setFields(idField,nameField);
        
        setFetchDataURL("/service/opensearch/search.xml");
        setAddDataURL("/service/entity/create.xml");
	}
	
	@Override
	protected Object transformRequest(DSRequest dsRequest) {
		JavaScriptObject data = dsRequest.getData();
		
		JSOHelper.setAttribute(data, "ticket", security.getTicket());
		
		return data;
	}
}
