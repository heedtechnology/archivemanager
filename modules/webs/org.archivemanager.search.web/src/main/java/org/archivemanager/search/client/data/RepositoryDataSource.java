package org.archivemanager.search.client.data;

import org.heed.openapps.gwt.client.data.NativeSecurityModel;

import com.google.gwt.core.client.JavaScriptObject;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.util.JSOHelper;

public class RepositoryDataSource extends RestDataSource {
	private NativeSecurityModel security = new NativeSecurityModel();
	private static RepositoryDataSource instance = null;  
	  
    public static RepositoryDataSource getInstance() {  
        if (instance == null) {  
            instance = new RepositoryDataSource("repositoryDS");  
        }  
        return instance;  
    }  
  
    public RepositoryDataSource(String id) {  
  
        setID(id);  
          
        DataSourceTextField itemNameField = new DataSourceTextField("name", "Name", 128, true);  
        
  
        DataSourceTextField idField = new DataSourceTextField("id", null);  
        idField.setPrimaryKey(true);  
        idField.setRequired(true);  
        //parentField.setRootValue("root");  
        idField.setForeignKey("repositoryDS.name");  
  
  
        setFields(itemNameField, idField);  
        
        setDataURL("/service/entity/search?qname=%7Bopenapps.org_repository_1.0%7Drepository&field=name&sort=name_e");  
          
        setClientOnly(true);
    }
    
    @Override
	protected Object transformRequest(DSRequest dsRequest) {
		JavaScriptObject data = dsRequest.getData();
		
		JSOHelper.setAttribute(data, "ticket", security.getTicket());
		
		return data;
	}
}
