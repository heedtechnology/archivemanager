package org.archivemanager.locations.client.form;


import org.archivemanager.locations.client.NativeLocationModel;
import org.archivemanager.locations.client.data.LocationDS;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class LocationForm extends VLayout {
	private NativeLocationModel locationModel = new NativeLocationModel();
	private DynamicForm form;
	private HLayout spacerViewer;
	private Label label;
	
	
	public LocationForm() {
		setWidth100();
		setHeight100();
		setBorder("1px solid #C0C3C7");
		setLayoutAlign(Alignment.CENTER);
		
		form = new DynamicForm();
		form.setWidth100();
		form.setHeight100();
		form.setMargin(5);
		form.setNumCols(4);
		form.setDataSource(LocationDS.getInstance());
		
		//TextItem nameItem = new TextItem("name", "Name");
		//nameItem.setWidth("*");
		//nameItem.setColSpan(4);
		final TextItem buildingItem = new TextItem("building", "Building");
		buildingItem.setWidth("*");
		
		final TextItem floorItem = new TextItem("floor", "Floor");
		floorItem.setWidth("*");
		
		final TextItem aisleItem = new TextItem("aisle", "Aisle");
		aisleItem.setWidth("*");
		
		final TextItem bayItem = new TextItem("bay", "Bay");
		bayItem.setWidth("*");
		
		TextAreaItem descriptionItem = new TextAreaItem("description", "Description");
		descriptionItem.setWidth("*");
		descriptionItem.setColSpan(4);
		
		TextItem collectionItem = new TextItem("collection", "Collection");
		collectionItem.setColSpan(4);
		collectionItem.setWidth("*");
		
		TextItem containerItem = new TextItem("container", "Container");
		containerItem.setColSpan(4);
		containerItem.setWidth("*");
		
		TextItem accessionItem = new TextItem("accession", "Accession #");
		accessionItem.setWidth("*");
		
		TextItem codeItem = new TextItem("code", "Code");
		codeItem.setWidth("*");
				
		form.setFields(buildingItem,floorItem, aisleItem, bayItem, descriptionItem,collectionItem,containerItem,accessionItem,codeItem);
		addMember(form);
		
		HLayout spacerLayout = new HLayout();
		spacerLayout.setMargin(5);
		addMember(spacerLayout);
		
		label = new Label();
		label.setWidth(103);
		label.setHeight(50);
		label.setAlign(Alignment.CENTER);
		spacerLayout.addMember(label);
		
		spacerViewer = new HLayout();
		spacerViewer.setWidth100();
		spacerViewer.setHeight(50);
		spacerViewer.setBorder("0px solid black");
		spacerViewer.setLayoutTopMargin(10);
		spacerViewer.setMembersMargin(1);
		spacerLayout.addMember(spacerViewer);
	}
	
	public void editRecord(Record record) {
		form.editRecord(record);
		spacerViewer.removeMembers(spacerViewer.getMembers());
		int space = 0;
		try {
			Record source_associations = record.getAttributeAsRecord("source_associations");
			if(source_associations != null) {
				Record itemsNode = source_associations.getAttributeAsRecord("items");
				if(itemsNode != null) {
					Record[] items = itemsNode.getAttributeAsRecordArray("node");
					for(Record item : items) {
						String size = item.getAttribute("size");
						if(size != null) space += Integer.parseInt(size);
					}
				}
			}
		} catch(ClassCastException e) {
			//setData(new Record[0]);
		}
		label.setContents("<b>Spaces</b><br/>"+space+" % filled");
		for(int i=0; i < 20; i++) {
			Canvas c = new Canvas();
			c.setWidth(20);
			c.setHeight(50);
			c.setBorder("1px solid black");
			if(i*5 >= space)	c.setBackgroundColor("red");
			else c.setBackgroundColor("green");
			spacerViewer.addMember(c);
		}		
	}
	public void fetchData(Criteria criteria, DSCallback callback) {
		form.fetchData(criteria, callback);
	}
	public void saveData(DSCallback callback) {
		form.saveData(callback);
	}
}
