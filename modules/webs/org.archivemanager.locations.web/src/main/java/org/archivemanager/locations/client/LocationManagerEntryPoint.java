package org.archivemanager.locations.client;

import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.OpenAppsEventHandler;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.DOM;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.rpc.HandleErrorCallback;
import com.smartgwt.client.rpc.RPCManager;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.Positioning;
import com.smartgwt.client.util.Page;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.layout.VLayout;


public class LocationManagerEntryPoint implements EntryPoint {
	private static final int height = Page.getHeight()-85;
	private static final int width = 1200;
	
	private LocationApplication locationApplication;
	
	private Record selection;
	
	private String mode = "classification";
	
	public void onModuleLoad() {
		VLayout mainLayout = new VLayout();
		mainLayout.setHeight(1200);  
		mainLayout.setWidth(1200);
		mainLayout.setOverflow(Overflow.HIDDEN);
		mainLayout.setMembersMargin(2);
        
        //LoadingScreen loader = new LoadingScreen("Loading....");
        //addMember(loader);
               
		locationApplication = new LocationApplication();
        mainLayout.addMember(locationApplication);
        
        EventBus.addHandler(org.heed.openapps.gwt.client.OpenAppsEvent.TYPE, new OpenAppsEventHandler()     {
	        @Override
	        public void onEvent(OpenAppsEvent event) {
	        	
	        }
        });
        
        RPCManager.setHandleErrorCallback(new HandleErrorCallback() {
			@Override
			public void handleError(DSResponse response, DSRequest request) {
				int httpCode = response.getHttpResponseCode();
				if(httpCode == 404) SC.warn("Error contacting the server, please check your internet connection and try again.");
			}
		});
        
        mainLayout.setHtmlElement(DOM.getElementById("gwt"));
        mainLayout.setPosition(Positioning.RELATIVE);
        mainLayout.draw();
        //loader.draw();
        //closeLoader();
        //DOM.removeChild(RootPanel.getBodyElement(), DOM.getElementById("loader"));
        //DOM.setStyleAttribute(RootPanel.get("gwt").getElement(), "display", "block");
	}
	
	protected void startup() {
		
	}
	public void search(String query) {
		locationApplication.search(query);
	}
	public void add() {
		locationApplication.add();
	}
	public void delete() {
		locationApplication.delete();
	}
	public void save() {
		locationApplication.save();
	}
	public void crawl() {
		
	}
	public Record getSelection() {
		return selection;
	}
	public void select(Record record) {
		selection = record;
		String type = selection.getAttribute("localName");
		locationApplication.select(record);
		
	}
	
	private final native void closeLoader() /*-{
    	return $wnd.closeLoader();
	}-*/;
}
