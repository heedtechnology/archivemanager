package org.archivemanager.locations.client;

import org.archivemanager.locations.client.data.LocationTreeDS;
import org.heed.openapps.gwt.client.component.Toolbar;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class LocationApplication extends VLayout {
	private Toolbar toolbar;
	private DynamicForm searchForm;
	private ListGrid grid;
	private LocationPanel locationPanel;
	private Canvas canvas;
	
	private AddLocationWindow addWindow;
	
	public LocationApplication() {
		setHeight100();  
		setWidth100();
		setMembersMargin(2);
		setBorder("1px solid #BFBFBF");
		
		toolbar = new Toolbar(32);
		toolbar.setMargin(1);
		toolbar.setBorder("1px solid #a8c298;");
        //toolbar.setLayoutLeftMargin(32);
        
        searchForm = new DynamicForm();
		searchForm.setWidth(300);
		searchForm.setHeight(25);
		searchForm.setMargin(0);
		searchForm.setNumCols(4);
		searchForm.setCellPadding(2);
		final TextItem searchField = new TextItem("query");
		searchField.setShowTitle(false);
		searchField.setWidth(255);
		searchField.addKeyPressHandler(new KeyPressHandler() {
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if(event.getKeyName().equals("Enter")) {
					search(searchField.getValueAsString());
				}				
			}
			
		});
		ButtonItem searchButton = new ButtonItem("search", "search");
		searchButton.setWidth(50);
		searchButton.setStartRow(false);
		searchButton.setEndRow(false);
		searchButton.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
			public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
				search(searchField.getValueAsString());
			}
		});
		searchForm.setFields(searchField,searchButton);
		
		toolbar.addButton("add", "/theme/images/icons32/add.png", "Add", new com.smartgwt.client.widgets.events.ClickHandler() {  
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				add();
			}  
		});
		toolbar.addButton("delete", "/theme/images/icons32/delete.png", "Delete", new com.smartgwt.client.widgets.events.ClickHandler() {  
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				delete();
			}  
		});
		toolbar.addButton("save", "/theme/images/icons32/disk.png", "Save", new com.smartgwt.client.widgets.events.ClickHandler() {  
			public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
				save();
			}  
		});	
		toolbar.addToLeftCanvas(searchForm);
		addMember(toolbar);
		
		HLayout mainLayout = new HLayout();
        mainLayout.setWidth100();
        mainLayout.setHeight100();
        mainLayout.setMembersMargin(2);
        addMember(mainLayout);
        
		grid = new ListGrid();
		grid.setWidth(300);
		grid.setHeight("100%");
		grid.setBorder("0px");
		grid.setShowHeader(false);
		grid.setShowResizeBar(true);
		grid.setSortField("name");
		ListGridField nameField = new ListGridField("name","Name");
		grid.setDataSource(LocationTreeDS.getInstance());
		grid.setFields(nameField);
		grid.addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				select(event.getRecord());
			}
		});
		mainLayout.addMember(grid);
        /*
		grid = new TreeGrid();
		grid.setDataSource(LocationTreeDS.getInstance());
		grid.setWidth(300);
		grid.setHeight("100%");
		grid.setBorder("0px");
		grid.setShowHeader(false);
		grid.setShowResizeBar(true);
		grid.addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				select(event.getRecord());
			}
		});
        addMember(grid);
		*/
		canvas = new Canvas();
        canvas.setWidth100();
        canvas.setHeight100();
        mainLayout.addMember(canvas);
        
        addWindow = new AddLocationWindow();
        
	}
	
	public void select(Record record) {
		String type = record.getAttribute("localName");
		if(type.equals("location")) {
			if(locationPanel == null) {
				locationPanel = new LocationPanel();
				canvas.addChild(locationPanel);
			}
			Criteria criteria = new Criteria();
			criteria.setAttribute("id", record.getAttribute("id"));
			locationPanel.fetchData(criteria, new DSCallback() {
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					if(response.getData() != null && response.getData().length > 0) {
						Record accession = response.getData()[0];
						if(accession != null) {
							String date = accession.getAttribute("name");
							String title = grid.getSelectedRecord().getAttribute("name");
							if(title != null && !title.equals(date)) {
								grid.getSelectedRecord().setAttribute("name", date);
								grid.refreshFields();
							}
							locationPanel.select(accession);
						}
					}
				}
			});
			locationPanel.select(record);
			locationPanel.show();
			//mgr.getToolbar().showAddButton(true);
			//mgr.getToolbar().showDeleteButton(true);
			//mgr.getToolbar().showSaveButton(true);
		} else {
			if(locationPanel != null) locationPanel.hide();
			//mgr.getToolbar().showAddButton(true);
			//mgr.getToolbar().showDeleteButton(false);
			//mgr.getToolbar().showSaveButton(false);
		}
	}
	
	public void save() {
		locationPanel.save(new DSCallback() {
			public void execute(DSResponse response, Object rawData, DSRequest request) {
				if(response.getData() != null && response.getData().length > 0) {
					Record accession = response.getData()[0];
					if(accession != null) {
						String date = accession.getAttribute("name");
						String title = grid.getSelectedRecord().getAttribute("name");
						if(title != null && !title.equals(date)) {
							grid.getSelectedRecord().setAttribute("name", date);
							grid.refreshFields();
						}
					}
				}
			}
		});		
	}
	
	public void add() {
		addWindow.show();
	}
	
	public void delete() {
		SC.confirm("Do you really wish to remove this permanently?", new BooleanCallback() {
			public void execute(Boolean value) {
				if(value) grid.removeSelectedData();
			}			
		});	
	}
	
	public void search(String query) {		
		Criteria criteria = new Criteria();
		criteria.setAttribute("qname", "{openapps.org_repository_1.0}location");
		criteria.setAttribute("query", query);
		criteria.setAttribute("field", "name");
		criteria.setAttribute("sort", "name_e");
		criteria.setAttribute("_startRow", "0");
		criteria.setAttribute("_endRow", "75");
		grid.clearCriteria();
		grid.fetchData(criteria);
	}

	public class AddLocationWindow extends Window {
		public AddLocationWindow() {
			setWidth(300);
			setHeight(180);
			setTitle("Add Location");
			setAutoCenter(true);
			setIsModal(true);
			DynamicForm addForm = new DynamicForm();
			addForm.setMargin(5);
			addForm.setCellPadding(3);
			addForm.setNumCols(2);
			final TextItem buildingItem = new TextItem("building", "Building");
			buildingItem.setWidth("*");
			
			final TextItem floorItem = new TextItem("floor", "Floor");
			floorItem.setWidth("*");
			
			final TextItem aisleItem = new TextItem("aisle", "Aisle");
			aisleItem.setWidth("*");
			
			final TextItem bayItem = new TextItem("bay", "Bay");
			bayItem.setWidth("*");
			
			ButtonItem submitItem = new ButtonItem("submit", "Add");
			submitItem.setIcon("/theme/images/icons16/add.png");
			submitItem.setStartRow(false);
			submitItem.setEndRow(false);
			submitItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {
				public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
					Record record = new Record();
					record.setAttribute("qname", "{openapps.org_repository_1.0}location");
					record.setAttribute("building", buildingItem.getValue());
					record.setAttribute("floor", floorItem.getValue());
					record.setAttribute("aisle", aisleItem.getValue());
					record.setAttribute("bay", bayItem.getValue());
					grid.addData(record, new DSCallback() {
						public void execute(DSResponse response, Object rawData, DSRequest request) {
							addWindow.hide();
						}
					});
				}
			});
			
			addForm.setFields(buildingItem,floorItem, aisleItem, bayItem, submitItem);
			addItem(addForm);
		}
	}

}