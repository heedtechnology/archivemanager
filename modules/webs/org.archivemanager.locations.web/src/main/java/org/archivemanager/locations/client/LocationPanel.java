package org.archivemanager.locations.client;

import java.util.LinkedHashMap;

import org.archivemanager.gwt.client.component.AccessionComponent;
import org.archivemanager.gwt.client.component.CollectionItemsComponent;
import org.archivemanager.locations.client.form.LocationForm;
import org.heed.openapps.gwt.client.component.ActivitiesComponent;
import org.heed.openapps.gwt.client.component.NotesComponent;
import org.heed.openapps.gwt.client.data.NativeRepositoryModel;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class LocationPanel extends VLayout {
	private NativeRepositoryModel repositoryModel = new NativeRepositoryModel();
	private LocationForm form;
	private CollectionItemsComponent collectionItemsComponent;
	private AccessionComponent accessionComponent;
	private NotesComponent notesComponent;
	private ActivitiesComponent activitiesComponent;
	
	public LocationPanel() {
		setWidth100();
		setHeight(250);
		setMembersMargin(5);
				
		form = new LocationForm();
		form.setWidth(535);
		addMember(form);
		
		VLayout midLayout = new VLayout();
		midLayout.setMargin(10);
		midLayout.setBorder("1px solid #C0C3C7");
		midLayout.setLayoutAlign(Alignment.CENTER);
		addMember(midLayout);
		
		notesComponent = new NotesComponent("location", "100%", true, repositoryModel.getCollectionNoteTypes());
		notesComponent.setMargin(10);
		midLayout.addMember(notesComponent);
								
		LinkedHashMap<String,String> valueMap4 = new LinkedHashMap<String,String>();
		valueMap4.put("acknowledgement_sent","Acknowledgement Sent");
		valueMap4.put("arranged_sorted","Arranged and Sorted");
		valueMap4.put("listed","Listed");
		valueMap4.put("processing_beginning","Processing Beginning");
		valueMap4.put("processing_ending","Processing Ending");
		valueMap4.put("rights_transferred","Rights Transferred");
		valueMap4.put("shelved","Shelved");
		activitiesComponent = new ActivitiesComponent("100%", valueMap4);
		activitiesComponent.setMargin(10);		
		midLayout.addMember(activitiesComponent);

		HLayout bottomLayout = new HLayout();
		midLayout.addMember(bottomLayout);
		
		VLayout column1 = new VLayout();
		column1.setWidth("100%");
		bottomLayout.addMember(column1);
		
		accessionComponent = new AccessionComponent("100%");
		accessionComponent.setMargin(10);
		column1.addMember(accessionComponent);
				
		VLayout column2 = new VLayout();
		column2.setWidth("100%");
		bottomLayout.addMember(column2);
		
		collectionItemsComponent = new CollectionItemsComponent("100%");
		collectionItemsComponent.setMargin(10);
		column2.addMember(collectionItemsComponent);
	}
	
	public void fetchData(Criteria criteria, DSCallback callback) {
		form.fetchData(criteria, callback);
	}
	public void select(Record location) {
		form.editRecord(location);
		collectionItemsComponent.select(location);
		notesComponent.select(location);
		activitiesComponent.select(location);
		accessionComponent.select(location);
	}
	public void save(DSCallback callback) {
		form.saveData(callback);
	}
}
