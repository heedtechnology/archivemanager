package org.archivemanager.locations.client.data;

import org.heed.openapps.gwt.client.data.NativeSecurityModel;

import com.google.gwt.core.client.JavaScriptObject;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.types.DSDataFormat;
import com.smartgwt.client.util.JSOHelper;

public class LocationDS extends RestDataSource {
	private static LocationDS instance = null;  
	private NativeSecurityModel security = new NativeSecurityModel();
	
    public static LocationDS getInstance() {  
        if (instance == null) {  
            instance = new LocationDS("locationDS");  
        }  
        return instance;  
    }
    
	public LocationDS(String id) {
		setID(id);
		setDataFormat(DSDataFormat.JSON);
		
		//setAddDataURL("/repository/accession/add.xml");
		setFetchDataURL("/service/entity/get.json");
		setRemoveDataURL("/service/entity/remove.json");
		setUpdateDataURL("/service/entity/update.json");
	}
	
	@Override
	protected Object transformRequest(DSRequest dsRequest) {
		JavaScriptObject data = dsRequest.getData();
		
		JSOHelper.setAttribute(data, "ticket", security.getTicket());
		
		return data;
	}
	
	@Override
	protected void transformResponse(DSResponse response, DSRequest request,Object data) {
		// TODO Auto-generated method stub
		super.transformResponse(response, request, data);
	}
}
