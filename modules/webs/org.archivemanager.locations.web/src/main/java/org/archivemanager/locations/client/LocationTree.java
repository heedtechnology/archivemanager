package org.archivemanager.locations.client;

import org.archivemanager.locations.client.data.LocationTreeDS;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.widgets.grid.events.CellClickEvent;
import com.smartgwt.client.widgets.grid.events.CellClickHandler;
import com.smartgwt.client.widgets.tree.TreeGrid;
import com.smartgwt.client.widgets.tree.TreeNode;
import com.smartgwt.client.widgets.tree.events.FolderDropEvent;
import com.smartgwt.client.widgets.tree.events.FolderDropHandler;

public class LocationTree extends TreeGrid {
	
	
	public LocationTree() {
		setHeight100();
		setWidth100();
		setShowHeader(false);
		setBorder("0px");
		setWrapCells(true);
		setFixedRecordHeights(false);
		setCanReorderRecords(true);  
        setCanAcceptDroppedRecords(true);
        setSelectionType(SelectionStyle.SINGLE);
        setDataSource(LocationTreeDS.getInstance());
        addCellClickHandler(new CellClickHandler() {
			public void onCellClick(CellClickEvent event) {
				//mgr.select(event.getRecord());	
			}			
		});
        addFolderDropHandler(new FolderDropHandler() {
        	public void onFolderDrop(FolderDropEvent event) {        		
        		TreeNode child = event.getNodes()[0];
        		TreeNode parent = event.getFolder();
        		Record record = new Record();
        		record.setAttribute("id", child.getAttribute("id"));
        		record.setAttribute("parent", parent.getAttribute("id"));
        		updateData(record);
			}
        });
	}
}
