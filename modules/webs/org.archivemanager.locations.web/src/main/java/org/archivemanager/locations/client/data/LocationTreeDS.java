package org.archivemanager.locations.client.data;

import org.heed.openapps.gwt.client.data.NativeSecurityModel;

import com.google.gwt.core.client.JavaScriptObject;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.RestDataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.DSDataFormat;
import com.smartgwt.client.util.JSOHelper;


public class LocationTreeDS extends RestDataSource {
	private static LocationTreeDS instance = null;  
	private NativeSecurityModel security = new NativeSecurityModel();
	
    public static LocationTreeDS getInstance() {  
        if (instance == null) {  
            instance = new LocationTreeDS("locationListDS");  
        }  
        return instance;  
    }
    
	public LocationTreeDS(String id) {
		setDataFormat(DSDataFormat.JSON);
		setID(id);  
        setTitleField("Name");	        
        DataSourceTextField nameField = new DataSourceTextField("name", "Name");     
        
        DataSourceTextField idField = new DataSourceTextField("id", "ID",50);  
        idField.setPrimaryKey(true);  
        idField.setRequired(true);
        /*
        DataSourceTextField parentField = new DataSourceTextField("parent", "Parent");  
        parentField.setRequired(true);  
        parentField.setForeignKey(id + ".id");  
        parentField.setRootValue("null");  
        */
        setFields(idField,nameField);
        
		setAddDataURL("/service/entity/create.json");
		setFetchDataURL("/service/archivemanager/locations.json");
		setRemoveDataURL("/service/entity/remove.json");
		setUpdateDataURL("/service/entity/update.json");	
	}
	
	@Override
	protected Object transformRequest(DSRequest dsRequest) {
		JavaScriptObject data = dsRequest.getData();
		
		JSOHelper.setAttribute(data, "ticket", security.getTicket());
		
		return data;
	}
}
