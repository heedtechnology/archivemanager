package org.archivemanager.locations.server;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.heed.openapps.theme.ThemeVariables;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class LocationController {

	
	@RequestMapping(value="/manager", method = RequestMethod.GET)
	public ModelAndView home(HttpServletRequest req, HttpServletResponse res) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		ThemeVariables vars = new ThemeVariables("smartclient");
		parms.put("theme", vars);
		//User user = securityService.currentUser();
		
		return new ModelAndView("home", parms);
	}
}