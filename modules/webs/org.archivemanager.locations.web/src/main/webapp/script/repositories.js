var activityValueMap = {'Processing Beginning':'Processing Beginning','Processing Ending':'Processing Ending','Rights Transferred':'Rights Transferred'};
var noteValueMap = {'General':'General','Description':'Description','Inventory':'Inventory','Condition':'Condition','Access Restriction':'Access Restriction','Use Restriction':'Use Restriction'};
var contactRelationshipMap = {'Donor':'Donor','Collectee':'Collectee','Assistant':'Assistant'};
var taskValueMap = {'Processing Beginning':'Processing Beginning','Processing Ending':'Processing Ending'};
function updateEntityData(xmlDoc, xmlText) {
	repoXml = repositoryDS.recordsFromXML(isc.XMLTools.selectNodes(xmlDoc, "//node"));
	isc.ResultSet.create({ID:"repoRS",dataSource:"repositoryDS",initialData:repoXml});
	if(repoXml) repositoryList.getSelectedRecord()['name'] = repoXml[0]['name'];
	repositoryList.refreshFields();
	descriptionForm.setValues(repoXml);
	noteXml = ds.recordsFromXML(isc.XMLTools.selectNodes(xmlDoc, "//node/source_associations/notes/node"));
	noteList.setData(noteXml);
	activityXml = ds.recordsFromXML(isc.XMLTools.selectNodes(xmlDoc, "//node/source_associations/activities/node"));
	activityList.setData(activityXml);
	collectionXml = ds.recordsFromXML(isc.XMLTools.selectNodes(xmlDoc, "//node/source_associations/collections/node"));
	collectionRelationList.setData(collectionXml);
	contactXml = ds.recordsFromXML(isc.XMLTools.selectNodes(xmlDoc, "//node/source_associations/contacts/node"));
	contactRelationList.setData(contactXml);
}
function save_repository() {
	if(descriptionForm.valuesHaveChanged()) 
		var parms = descriptionForm.getValues();
		parms['qname'] = '{openapps.org_repository_1.0}repository';
		isc.XMLTools.loadXML("/core/entity/update", function(xmlDoc, xmlText) {
			updateEntityData(xmlDoc, xmlText);
			repositoryAddWindowForm.clearValues();
		},{httpMethod:"POST",params:parms});
}
function save_notesPanel() {
	var parms = notesPanelForm.getValues();
	parms['repository'] = repositoryList.getSelectedRecord().id;
	if(!parms['id']) {
		if(repositoryList.anySelected()) {
			isc.XMLTools.loadXML("add.xml?_dataSource=note", "updateEntityData(xmlDoc, xmlText)",{httpMethod:"GET",params:parms});
		} else isc.warn('Please select a repository to attach this note to.')
	} else isc.XMLTools.loadXML("update.xml?_dataSource=note", "updateEntityData(xmlDoc, xmlText)",{httpMethod:"GET",params:parms});
}
function load_repository(node) {
	entityId = node.id;
	isc.XMLTools.loadXML("/core/entity/get/"+node.id, "updateEntityData(xmlDoc, xmlText)");
}
function load_notesPanel(node) {
	entityId = node.id;
	notesPanelForm.editRecord(node);
}
function add_notesList() {
	var note_label = notesListAddWindowForm.getValue("label");
	var repository_id = repositoryList.getSelectedRecord().id;
	isc.XMLTools.loadXML("add.xml?_dataSource=notes&label="+note_label+"&repository="+repository_id, "updateEntityData(xmlDoc, xmlText)");
	notesListAddWindow.hide();
}
function remove_notesList() {
	var note_id = notesPanelList.getSelectedRecord().target_id;
	var repository_id = repositoryList.getSelectedRecord().id;
	isc.XMLTools.loadXML("remove.xml?_dataSource=notes&id="+note_id+"&repository="+repository_id, "updateEntityData(xmlDoc, xmlText)");
	notesPanelForm.clearValues();
}
function search(query) {
	repositoryList.fetchData({qname:'{openapps.org_repository_1.0}repository',query:query,field:'name',sort:'name_e'});
}
isc.MessagingDataSource.create({ID:'repositories',fetchDataURL:'/core/entity/search',addDataURL:'/core/entity/create',updateDataURL:'/core/entity/update',removeDataURL:'/core/entity/remove.xml',fields:[{name:'id',title:'ID',type:'text',primaryKey:true,required:true}]});
isc.MessagingDataSource.create({ID:'repositoryDS',fetchDataURL:'fetch.xml',addDataURL:'add.xml',updateDataURL:'update.xml',removeDataURL:'remove.xml',fields:[{name:'id',title:'ID',type:'text',primaryKey:true,required:true}]});
isc.MessagingDataSource.create({ID:'notes',fetchDataURL:'fetch.xml',addDataURL:'add.xml',updateDataURL:'update.xml',removeDataURL:'remove.xml',fields:[{name:'id',title:'ID',type:'text',primaryKey:true,required:true}]});
isc.Window.create({ID:'repositoryAddWindow',title:'Add',width:400,height:110,autoCenter:true,isModal:true,showModalMask:true,autoDraw:false,
	items:[
	    isc.DynamicForm.create({ID:'repositoryAddWindowForm',width:'100%',datasource:'repositories',margin:10,numCols:2,cellPadding:7,
	    	fields:[
	    	    {name:'name',width:'*',required:true,title:'Full Name'},
	    	    {name:'validateBtn',title:'Save',type:'button',
	    	    	click:function() {
	    	    		var parms = repositoryAddWindowForm.getValues();
	    	    		parms['qname'] = '{openapps.org_repository_1.0}repository';
	    	    		repositoryList.addData(parms);
	    	    		repositoryAddWindow.hide();
	    	    	}
	    	    }
	    	]
	    })
	]
});

isc.ListGrid.create({ID:'repositoryList',dataSource:'repositories',height:'100%',width:'100%',alternateRecordStyles:true,autoDraw:false,autoFetchData:false,recordClick:'load_repository(record)',
	fields:[
	    {name:'title',title:'Full Name',width:'*',align:'left'}
	]
});
isc.HLayout.create({ID:'repositoryButtonPanel',height:'25',width:'100%',autoDraw:false,layoutLeftMargin:2,membersMargin:5,
	members:[
	    isc.IButton.create({autoDraw:false,title:'add',width:60,icon:"/theme/images/icons16/add.png",click:'repositoryAddWindow.show();'}),
	    isc.IButton.create({autoDraw:false,title:'delete',width:75,icon:"/theme/images/icons16/remove.png",click:'repositoryList.removeSelectedData();'}),
	    isc.IButton.create({autoDraw:false,title:'save',width:65,icon:"/theme/images/icons16/save.png",click:'save_repository();'})
	]
});
isc.VLayout.create({ID:'repository',autoDraw:false,height:'100%',width:'350',
	members:[
	     repositoryButtonPanel,
	     isc.DynamicForm.create({ID:'repositoryListSearch',width:'100%',margin:0,numCols:2,cellPadding:2,
		    fields:[
		        {name:'query',width:'245',type:'text',showTitle:false},
		        {name:'validateBtn',title:'search',type:'button',width:50,startRow:false,endRow:false,
		        	click:function() {
		        		search(repositoryListSearch.getValue('query'));
		        	}
		        }
		    ]
		 }),
	     repositoryList
	]
});

isc.VLayout.create({ID:'descriptionPanel',autoDraw:false,height:'100%',width:'100%',margin:5,membersMargin:10,
	members:[
	    isc.DynamicForm.create({ID:'descriptionForm',dataSource:repositoryDS,height:'50%',width:'100%',margin:'5',
	    	visibility:'visible',numCols:4,autoFocus:false,cellPadding:5,
	    	fields:[
		        {name:'name',width:'*',required:true,title:'Full Name'},
		        {name:'short_name',width:'*',title:'Short Name'},
		        {name:'url',width:'*',title:'URL'},
		        {name:'country_code',width:'*',title:'Country Code'},
		        {name:'agency_code',width:'*',title:'Agency Code'},
		        {name:'nces',width:'*',title:'NCES'},
		        {name:'branding',width:'*',title:'branding'},
		        {name:'lang',width:'*',title:'Language'}
		    ]
	    }),
	    isc.HLayout.create({height:'50%',width:'100%',membersMargin:5,
	    	members:[
	    	    getCollectionRelationPanel('{openapps.org_repository_1.0}collections', '50%','100%'),
	    	    getContactRelationPanel('Contacts', contactRelationshipMap,'50%','100%')
	    	]
	    })
	]
});
isc.VLayout.create({ID:"notesAndActivitiesPanel",autoDraw:false,height:'100%',width:'100%',margin:5,membersMargin:10,
	members:[
	    getNotesPanel(noteValueMap),
	    getActivitiesPanel(activityValueMap),
	    getTasksPanel(taskValueMap)
	]
});
isc.TabSet.create({ID:'topTabSet',tabBarPosition:'top',width:'100%',height:'100%',autoDraw:false,
	tabs:[
	    {title:'Details',pane:descriptionPanel},
	    {title:'Notes, Activities and Tasks',pane:notesAndActivitiesPanel}
	]
});
isc.HLayout.create({ID:'pageLayout',width:smartWidth,height:smartHeight,position:'relative',visibility:'visible',members:[repository,topTabSet]});
