package org.archivemanager.reporting.client;

import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.EventTypes;
import org.heed.openapps.gwt.client.OpenAppsEvent;

import com.smartgwt.client.data.Record;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.VLayout;

public class ReportMenu extends VLayout {

	
	public ReportMenu() {
		setWidth100();
		setHeight100();
		setMembersMargin(2);
		setBorder("1px solid #BFBFBF;");
		
		Label label = new Label("<label style='font-weight:bold;font-size:16px;padding-left:5px;'>Collection Reports</label>");
		label.setWidth100();
		label.setHeight(30);
		addMember(label);
		
		addButton("finding_aids", "Finding Aids");
	}

	protected void addButton(final String id, String title) {
		Label label = new Label("<label style='font-size:12px;padding-left:25px;'>"+title+"</label>");
		label.setWidth100();
		label.setHeight(25);
		setPadding(1);
		label.setBorder("1px solid #BFBFBF;");
		label.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Record record = new Record();
				record.setAttribute("id", id);
				EventBus.fireEvent(new OpenAppsEvent(EventTypes.SELECTION, record));
			}
			
		});
		addMember(label);
	}
}
