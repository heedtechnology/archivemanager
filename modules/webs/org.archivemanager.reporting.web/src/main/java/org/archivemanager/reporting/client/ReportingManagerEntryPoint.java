package org.archivemanager.reporting.client;

import org.heed.openapps.gwt.client.EventBus;
import org.heed.openapps.gwt.client.EventTypes;
import org.heed.openapps.gwt.client.OpenAppsEvent;
import org.heed.openapps.gwt.client.OpenAppsEventHandler;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.DOM;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.rpc.HandleErrorCallback;
import com.smartgwt.client.rpc.RPCManager;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.Positioning;
import com.smartgwt.client.util.Page;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


public class ReportingManagerEntryPoint implements EntryPoint {
	private static final int height = Page.getHeight()-85;
	private static final int width = 1200;
	
	private ReportMenu menu;
	private FindingAidModule findingAidModule;
	
	private Record selection;
	
	private String mode = "classification";
	
	public void onModuleLoad() {
		VLayout mainLayout = new VLayout();
		mainLayout.setHeight(1200);  
		mainLayout.setWidth(1200);
		mainLayout.setOverflow(Overflow.HIDDEN);
		mainLayout.setMembersMargin(2);
        
		HLayout bodyLayout = new HLayout();
		bodyLayout.setWidth100();
		bodyLayout.setHeight100();
		bodyLayout.setMembersMargin(2);
		mainLayout.addMember(bodyLayout);
		
		menu = new ReportMenu();
		menu.setHeight100();
		menu.setWidth(300);
		bodyLayout.addMember(menu);
		
		findingAidModule = new FindingAidModule();
		bodyLayout.addMember(findingAidModule);
        
        EventBus.addHandler(org.heed.openapps.gwt.client.OpenAppsEvent.TYPE, new OpenAppsEventHandler()     {
	        @Override
	        public void onEvent(OpenAppsEvent event) {
	        	if(event.isType(EventTypes.SELECTION)) {
	        		findingAidModule.select(event.getRecord());
	        	}
	        }
        });
        
        RPCManager.setHandleErrorCallback(new HandleErrorCallback() {
			@Override
			public void handleError(DSResponse response, DSRequest request) {
				int httpCode = response.getHttpResponseCode();
				if(httpCode == 404) SC.warn("Error contacting the server, please check your internet connection and try again.");
			}
		});
        
        
        
        mainLayout.setHtmlElement(DOM.getElementById("gwt"));
        mainLayout.setPosition(Positioning.RELATIVE);
        mainLayout.draw();
        //loader.draw();
        //closeLoader();
        //DOM.removeChild(RootPanel.getBodyElement(), DOM.getElementById("loader"));
        //DOM.setStyleAttribute(RootPanel.get("gwt").getElement(), "display", "block");
	}
	
	protected void startup() {
		
	}
	
	private final native void closeLoader() /*-{
    	return $wnd.closeLoader();
	}-*/;
}
