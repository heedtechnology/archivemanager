package org.archivemanager.reporting.client;

import org.heed.openapps.gwt.client.component.EntitySelectionComponent;
import org.heed.openapps.gwt.client.data.RestUtility;

import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.ContentsType;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.HTMLPane;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

public class FindingAidModule extends VLayout {
	private EntitySelectionComponent collectionComponent;
	private HTMLPane iframeCanvas;
	
	public FindingAidModule() {
		setWidth100();
		setHeight100();
		setBorder("1px solid #BFBFBF;");
		
		HLayout navLayout = new HLayout();
		navLayout.setWidth100();
		navLayout.setHeight(200);
		navLayout.setBorder("1px solid #BFBFBF;");
		navLayout.setMargin(5);
		addMember(navLayout);
		
		EntitySelectionComponent repositoryComponent = new EntitySelectionComponent("{openapps.org_repository_1.0}repository", "Repositories", 200, 200);
		repositoryComponent.setMargin(5);
		navLayout.addMember(repositoryComponent);
		
		collectionComponent = new EntitySelectionComponent("{openapps.org_repository_1.0}collection", "Collections", 200, 200);
		collectionComponent.setMargin(5);
		navLayout.addMember(collectionComponent);
		
		VLayout buttonLayout = new VLayout();
		buttonLayout.setWidth(65);
		buttonLayout.setHeight(230);
		buttonLayout.setMargin(5);
		buttonLayout.setMembersMargin(5);
		buttonLayout.setPadding(2);
		buttonLayout.setBorder("1px solid #BFBFBF;");
		navLayout.addMember(buttonLayout);
				
		Button generateButton = new Button("Generate");
		generateButton.setPrompt("Generate");
		generateButton.setWidth(60);
		generateButton.setHeight(24);
		generateButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				String format = "pdf";
				final String target_id = collectionComponent.getSelectedRecord().getAttribute("target_id");
				Record criteria = new Record();
				criteria.setAttribute("id", target_id);
	    		criteria.setAttribute("format", format);
	    		criteria.setAttribute("title", "finding_aid_"+target_id);
	    		criteria.setAttribute("schedule", "false");
				
	    		RestUtility.get("/service/archivemanager/reporting/finding_aid/generate.xml", criteria, new DSCallback() {
					public void execute(DSResponse response, Object rawData, DSRequest request) {
						SC.say("Finding Aid successfully generated.");
					}
				});
			}			
		});
		buttonLayout.addMember(generateButton);
		
		Button viewButton = new Button("View");
		viewButton.setPrompt("View");
		viewButton.setWidth(60);
		viewButton.setHeight(24);
		viewButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				final String target_id = collectionComponent.getSelectedRecord().getAttribute("target_id");
				iframeCanvas.setContentsURL("/service/archivemanager/reporting/stream/finding_aid_"+target_id+".pdf");
			}			
		});
		buttonLayout.addMember(viewButton);
		
		repositoryComponent.fetchData("");
		
		iframeCanvas = new HTMLPane();
        iframeCanvas.setWidth100();
        iframeCanvas.setHeight100();
        iframeCanvas.setMargin(5);
        iframeCanvas.setContentsType(ContentsType.PAGE);
        iframeCanvas.setShowEdges(false);
        iframeCanvas.setContentsURL("/theme/content/logo.pdf");
        addMember(iframeCanvas);
	}
	public void select(Record record) {
		final String id = record.getAttribute("id");
		String localName = record.getAttribute("localName");
		Record criteria = new Record();
		criteria.setAttribute("id", id);
		if(localName.equals("repository")) {			
			RestUtility.get("/service/entity/get.xml", criteria, new DSCallback() {
				public void execute(DSResponse response, Object rawData, DSRequest request) {
					if(response.getData() != null && response.getData().length > 0) {
						try {
							Record record = response.getData()[0];
							Record source_associations = record.getAttributeAsRecord("source_associations");
							if(source_associations != null) {
								Record notes = source_associations.getAttributeAsRecord("collections");
								if(notes != null) {
									collectionComponent.setData(notes.getAttributeAsRecordArray("node"));
								} else collectionComponent.setData(new Record[0]);
							} else collectionComponent.setData(new Record[0]);
						} catch(ClassCastException e) {
							collectionComponent.setData(new Record[0]);
						}
					}
				}						
			});
		} 
	}
}
