package org.archivemanager.reporting.server;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.heed.openapps.security.SecurityService;
import org.heed.openapps.security.User;
import org.heed.openapps.theme.ThemeVariables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;


@Controller
public class ReportingController {
	@Autowired protected SecurityService securityService;
	
	
	@RequestMapping(value="/dashboard", method = RequestMethod.GET)
	public ModelAndView dashboard(HttpServletRequest req, HttpServletResponse res) throws Exception {
		Map<String, Object> parms = new HashMap<String, Object>();
		ThemeVariables vars = new ThemeVariables("dashboard");
		parms.put("theme", vars);
		User user = securityService.currentUser();

		if(!user.isGuest()) return new ModelAndView("dashboard", parms);
		return new ModelAndView(new RedirectView("/login"));//new ModelAndView("dashboard_guest", parms);
	}
	
}
