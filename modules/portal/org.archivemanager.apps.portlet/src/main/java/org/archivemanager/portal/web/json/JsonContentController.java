package org.archivemanager.portal.web.json;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.archivemanager.portal.web.WebserviceSupport;
import org.heed.openapps.data.RestResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("/service/archivemanager/content")
public class JsonContentController extends WebserviceSupport {
	
	
	@RequestMapping(value="/upload.json", method = RequestMethod.POST)
	public void upload(HttpServletRequest req, HttpServletResponse res) throws Exception {
		getContentServiceSupport().importUpload(req, res);
	}
	@ResponseBody
	@RequestMapping(value="/remove.json", method = RequestMethod.POST)
	public RestResponse<Object> removeAssociation(HttpServletRequest request, HttpServletResponse response, 
			@RequestParam("id") Long id) throws Exception {		
		return getContentServiceSupport().removeDigitalObject(id);
	}
}