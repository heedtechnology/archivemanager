package org.archivemanager.portal.web.json;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.util.Streams;
import org.apache.commons.io.IOUtils;
import org.archivemanager.portal.web.WebserviceSupport;
import org.heed.openapps.InvalidQualifiedNameException;
import org.heed.openapps.QName;
import org.heed.openapps.SystemModel;
import org.heed.openapps.cache.TimedCache;
import org.heed.openapps.data.RestResponse;
import org.heed.openapps.data.Sort;
import org.heed.openapps.dictionary.RepositoryModel;
import org.heed.openapps.entity.Association;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityQuery;
import org.heed.openapps.entity.EntityResultSet;
import org.heed.openapps.entity.ExportProcessor;
import org.heed.openapps.entity.ImportProcessor;
import org.heed.openapps.entity.InvalidAssociationException;
import org.heed.openapps.entity.InvalidEntityException;
import org.heed.openapps.entity.InvalidPropertyException;
import org.heed.openapps.entity.ModelValidationException;
import org.heed.openapps.entity.Property;
import org.heed.openapps.entity.ValidationResult;
import org.heed.openapps.entity.data.FileImportProcessor;
import org.heed.openapps.entity.data.FormatInstructions;
import org.heed.openapps.entity.EntitySorter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping("/service/archivemanager")
public class JsonCollectionController extends WebserviceSupport {
	private TimedCache<String,Entity> entityCache = new TimedCache<String,Entity>(60);
	private TimedCache<String,FileImportProcessor> parserCache = new TimedCache<String,FileImportProcessor>(60);
	
	@ResponseBody
	@RequestMapping(value="/collection/items.json", method = RequestMethod.GET)
	public RestResponse<Object> fetchCollectionTreeData(HttpServletRequest req, HttpServletResponse res) throws Exception {
		RestResponse<Object> data = new RestResponse<Object>();
		String parent = req.getParameter("parent");
		String collectionId = req.getParameter("collection");
		if(parent == null || parent.equals("null")) parent = collectionId;
		if(parent == null || parent.equals("null")) {	
			String query = req.getParameter("query");
			EntityQuery eQuery = (query != null) ? new EntityQuery(RepositoryModel.COLLECTION, query, "openapps_org_system_1_0_name", true) : new EntityQuery(RepositoryModel.COLLECTION, null, "openapps_org_system_1_0_name", true);
			int startRow = (req.getParameter("_startRow") != null) ? Integer.valueOf(req.getParameter("_startRow")) : 0;
			int endRow = (req.getParameter("_endRow") != null) ? Integer.valueOf(req.getParameter("_endRow")) : 75;
			eQuery.setStartRow(startRow);
			eQuery.setEndRow(endRow);
			eQuery.getFields().add("openapps_org_system_1_0_name");
			EntityResultSet collections = getEntityService().search(eQuery);
			FormatInstructions instructions = new FormatInstructions(false, false, false);
			instructions.setFormat(FormatInstructions.FORMAT_JSON);
			List<Entity> entities = collections.getResults();
			
			EntitySorter entitySorter = new EntitySorter(new Sort(Sort.STRING, SystemModel.NAME.getLocalName(), true));
			
			Collections.sort(entities, entitySorter);
			for(Entity collection : entities) {
				//Property name = collection.getProperty(SystemModel.NAME);
				//buff.append("<node id='"+collection.getId()+"' qname='openapps_org_repository_1_0_collection' localName='collection' parent='null'><title><![CDATA["+name.toString()+"]]></title></node>");
				data.getResponse().getData().add(getEntityService().export(instructions, collection));
			}
			if(collections.getResultSize() >= collections.getEndRow()) data.getResponse().setEndRow(collections.getEndRow());
			else data.getResponse().setEndRow(collections.getResultSize());
		} else {
			Entity collection = getEntityService().getEntity(Long.valueOf(parent));
			List<Association> list = new ArrayList<Association>();
			List<Association> sourceAssociations = collection.getSourceAssociations(RepositoryModel.CATEGORIES, RepositoryModel.ITEMS);
			for(Association assoc : sourceAssociations) {
				if(assoc.getTargetEntity() == null) {
					Entity accession = getEntityService().getEntity(assoc.getTarget());
					assoc.setTargetEntity(accession);
				}
				list.add(assoc);
			}
			FormatInstructions instructions = new FormatInstructions(true, false, false);
			instructions.setFormat(FormatInstructions.FORMAT_JSON);
			for(Association component : list) {	
				data.getResponse().getData().add(getEntityService().export(instructions, component));
			}
			
		}
		return data;
	}
	@ResponseBody
	@RequestMapping(value="/collection/fetch.json", method = RequestMethod.GET)
	public RestResponse<Object> fetchCollectionAccessionTreeData(HttpServletRequest req, HttpServletResponse response) throws Exception {
		RestResponse<Object> data = new RestResponse<Object>();
		//res.setContentType("text/xml; charset=UTF-8");
		//res.setCharacterEncoding("UTF-8");
		String parent = req.getParameter("parent");
		String collectionId = req.getParameter("collection");
		if(parent == null || parent.equals("null")) parent = collectionId;
		int startRow = (req.getParameter("_startRow") != null) ? Integer.valueOf(req.getParameter("_startRow")) : 0;
		int endRow = (req.getParameter("_endRow") != null) ? Integer.valueOf(req.getParameter("_endRow")) : 75;
		//User user = getSecurityService().getCurrentUser(req);
		
		if(parent == null || parent.equals("null")) {
			String entityId = req.getParameter("entityId");
			if(entityId != null && entityId.length() > 0) {
				Entity collection = getEntityService().getEntity(Long.valueOf(entityId));
				data.getResponse().getData().add(getNodeData(String.valueOf(collection.getId()), "null", collection.getName(), "openapps_org_repository_1_0_collection", "collection"));
				data.getResponse().setTotalRows(1);
			} else {
				String query = req.getParameter("query");
				EntityQuery eQuery = (query != null) ? new EntityQuery(RepositoryModel.COLLECTION, query, "openapps_org_system_1_0_name_e", false) : new EntityQuery(RepositoryModel.COLLECTION, null, "openapps_org_system_1_0_name_e", true);
				eQuery.setType(EntityQuery.TYPE_LUCENE);
				eQuery.setStartRow(startRow);
				eQuery.setEndRow(endRow);
				eQuery.getFields().add("openapps_org_system_1_0_name");
				//if(user != null && !user.isAdministrator()) eQuery.setUser(user.getId());
				EntityResultSet collections = getEntityService().search(eQuery);
				if(collections != null) {
					for(Entity collection : collections.getResults()) {
						data.getResponse().getData().add(getNodeData(String.valueOf(collection.getId()), "null", collection.getName(), "openapps_org_repository_1_0_collection", "collection"));
					}
					data.getResponse().setTotalRows(collections.getResultSize());
				}
			}
		} else {
			Entity collection = getEntityService().getEntity(Long.valueOf(parent));
			List<Association> list = collection.getSourceAssociations(RepositoryModel.ITEMS,RepositoryModel.CATEGORIES);
			
			FormatInstructions instructions = new FormatInstructions(true);
			instructions.setFormat(FormatInstructions.FORMAT_JSON);
			for(Association component : list) {	
				if(component.getQName().equals(RepositoryModel.COLLECTION) || component.getQName().equals(RepositoryModel.ACCESSION) || component.getQName().equals(RepositoryModel.CATEGORY))
					data.getResponse().getData().add(getEntityService().export(instructions, component.getTargetEntity()));
				else {
					Entity targetEntity = component.getTargetEntity() != null ? component.getTargetEntity() : getEntityService().getEntity(component.getTarget());
					data.getResponse().getData().add(getNodeData(String.valueOf(targetEntity.getId()), parent, targetEntity.getName(), targetEntity.getQName().toString(), targetEntity.getQName().getLocalName()));
				}
			}
		}
		data.getResponse().setStartRow(startRow);
		data.getResponse().setEndRow(endRow);
		
		response.setHeader( "Pragma", "no-cache" );
		response.setHeader( "Cache-Control", "no-cache" );
		response.setDateHeader( "Expires", 0 );
		
		return data;
	}
	@ResponseBody
	@RequestMapping(value="/collection/add.json", method = RequestMethod.POST)
	public RestResponse<Object> add(HttpServletRequest request, HttpServletResponse response) throws Exception {
		RestResponse<Object> data = new RestResponse<Object>();
		String source = request.getParameter("source");
		String sessionKey = request.getParameter("sessionKey");
		if(source != null) {
			if(sessionKey != null && sessionKey.length() > 0) {
				Entity collection = getEntityService().getEntity(Long.valueOf(source));
				Entity root = entityCache.get(sessionKey);
				getEntityService().addEntity(root);
				ImportProcessor parser = parserCache.get(sessionKey);
				Collection<Entity> entities = parser.getEntities().values();
				getEntityService().addEntities(entities);
				Association assoc = new Association(RepositoryModel.CATEGORIES, collection.getId(), root.getId());
				getEntityService().addAssociation(assoc);
				root.setName(root.getPropertyValue(SystemModel.NAME) + "(import)");
				getEntityService().updateEntity(root);
				ExportProcessor processor = getEntityService().getExportProcessor(root.getQName().toString());
				data.getResponse().getData().add(processor.export(new FormatInstructions(true), assoc));
			} else {
				String assocQname = request.getParameter("assoc_qname");
				String entityQname = request.getParameter("entity_qname");
				QName aQname = QName.createQualifiedName(assocQname);
				QName eQname = QName.createQualifiedName(entityQname);
				Entity entity = getEntityService().getEntity(request, eQname);
				ValidationResult entityResult = getEntityService().validate(entity);
				if(entityResult.isValid()) {
					if(entity.getId() > 0) {
						getEntityService().updateEntity(entity);
						getSearchService().update(entity, false);
					} else {
						getEntityService().addEntity(Long.valueOf(source), null, aQname, null, entity);
						getSearchService().update(entity, false);
					}
					data.getResponse().getData().add(getNodeData(String.valueOf(entity.getId()), source, entity.getName(), entity.getQName().toString(), entity.getQName().getLocalName()));
				}
			}
		} else {
			String qnameStr = request.getParameter("qname");
			QName qname = QName.createQualifiedName(qnameStr);
			Entity entity = getEntityService().getEntity(request, qname);
			ValidationResult result = getEntityService().validate(entity);
			if(result.isValid()) {	
				getEntityService().addEntity(entity);							
				getSearchService().update(entity, false);
				data.getResponse().getData().add(getNodeData(String.valueOf(entity.getId()), "null", entity.getName(), entity.getQName().toString(), entity.getQName().getLocalName()));
									
				response.setHeader( "Pragma", "no-cache" );
				response.setHeader( "Cache-Control", "no-cache" );
				response.setDateHeader( "Expires", 0 );
			} 
		}
		return data;
	}
	@ResponseBody
	@RequestMapping(value="/collection/update.json", method = RequestMethod.POST)
	public RestResponse<Object> updateCollection(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("id") Long id, @RequestParam("parent") Long parent) throws Exception {
		RestResponse<Object> data = new RestResponse<Object>();
		Entity source = getEntityService().getEntity(id);
		Entity parentEntity = getEntityService().getEntity(parent);
		Association assoc = source.getTargetAssociation(RepositoryModel.CATEGORIES);
		if(assoc == null) assoc = source.getTargetAssociation(RepositoryModel.ITEMS);
		if(assoc == null) assoc = source.getTargetAssociation(RepositoryModel.ACCESSIONS);
		if(assoc != null && source != null && parentEntity != null) {
			assoc.setSource(parentEntity.getId());
			getEntityService().updateAssociation(assoc);
			getSearchService().update(source, false);
			getSearchService().update(parentEntity, false);
			
			data.getResponse().getData().add(getNodeData(String.valueOf(source.getId()), String.valueOf(parentEntity.getId()), source.getName(), source.getQName().toString(), source.getQName().getLocalName()));
		}
		
		response.setHeader( "Pragma", "no-cache" );
		response.setHeader( "Cache-Control", "no-cache" );
		response.setDateHeader( "Expires", 0 );
		
		return data;
	}
	@ResponseBody
	@RequestMapping(value="/collection/remove.json", method = RequestMethod.POST)
	public RestResponse<Object> removeEntity(HttpServletRequest request, HttpServletResponse response, @RequestParam("id") Long id) throws Exception {
		RestResponse<Object> data = new RestResponse<Object>();
		Entity entity = getEntityService().getEntity(id);
		getEntityService().removeEntity(id);
		getSearchService().remove(id);
		
		response.setHeader( "Pragma", "no-cache" );
		response.setHeader( "Cache-Control", "no-cache" );
		response.setDateHeader( "Expires", 0 );
		
		Map<String,Object> record = new HashMap<String,Object>();
		record.put("id", entity.getId());
		record.put("uid", entity.getUuid());
		data.getResponse().addData(record);
		
		return data;
	}
	@ResponseBody
	@RequestMapping(value="/collection/propagate.json", method = RequestMethod.POST)
	public RestResponse<Object> propagate(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("id") Long id, @RequestParam("qname") String type, @RequestParam("value") String value) throws Exception {
		RestResponse<Object> data = new RestResponse<Object>();
		if(type.equals("qname")) {
			cascadeQName(id, RepositoryModel.ITEMS, QName.createQualifiedName(value));
			cascadeQName(id, RepositoryModel.CATEGORIES, QName.createQualifiedName(value));
		} else {
			getEntityService().cascadeProperty(id, RepositoryModel.ITEMS, QName.createQualifiedName(type), (Serializable)value);
		}
		data.getResponse().addMessage("Successfully updated child properties.");
		return data;
	}
	/*
	@ResponseBody
	@RequestMapping(value="/reindex/{id}", method = RequestMethod.GET)
	public RestResponse<Object> reindex(HttpServletRequest req, HttpServletResponse res, @PathVariable("id") long id) throws Exception {
		RestResponse<Object> data = new RestResponse<Object>();
		
		EntityQuery query = new EntityQuery(RepositoryModel.ITEM);
		query.setNativeQuery(new TermQuery(new Term("path", String.valueOf(id))));
		query.setEndRow(10000);
		EntityResultSet results = getEntityService().search(query);
		getLoggingService().info(JsonCollectionController.class, "reindexing "+results.getResults().size()+" entities");
		for(Entity item : results.getResults()) {
			getSearchService().update(item.getId());
		}
		data.getResponse().getMessages().add("reindexed "+results.getResults().size()+" entities");
		getLoggingService().info(JsonCollectionController.class, "reindexed "+results.getResults().size()+" entities");
		return data;
	}
	*/
	@RequestMapping(value="/collection/import/upload.json", method = RequestMethod.POST)
	public ModelAndView upload(HttpServletRequest req, HttpServletResponse res) throws Exception {
		String mode = null;
		String sessionKey = "";
		try {
			// Create a new file upload handler
			ServletFileUpload upload = new ServletFileUpload();
			// Parse the request
			FileItemIterator iter = upload.getItemIterator(req);
			byte[] file = null;
			while(iter.hasNext()) {
			    FileItemStream item = iter.next();
			    String name = item.getFieldName();
			    InputStream stream = item.openStream();			    
			    if(item.isFormField()) {
			    	if(name.equals("mode")) mode = Streams.asString(stream);
			    	//System.out.println("Form field " + name + " with value " + Streams.asString(tmpStream) + " detected.");
			    } else {
			    	file = IOUtils.toByteArray(stream);
			    }
			    FileImportProcessor parser = mode != null ? (FileImportProcessor)getEntityService().getImportProcessors(mode).get(0) : null;
				if(parser != null && file != null) {
					parser.process(new ByteArrayInputStream(file), null);
					sessionKey = java.util.UUID.randomUUID().toString();
					entityCache.put(sessionKey, parser.getRoot());
					parserCache.put(sessionKey, parser);
				}
			}
		} catch(FileUploadException e) {
			e.printStackTrace();
		}		
		res.setContentType("text/html");
		res.getWriter().print("<script language='javascript' type='text/javascript'>window.top.window.uploadComplete('"+sessionKey+"');</script>");
		return null;
	}
	@ResponseBody
	@RequestMapping(value="/collection/import/fetch.json", method = RequestMethod.GET)
	public RestResponse<Object> fetch(HttpServletRequest req, HttpServletResponse res, @RequestParam("session") String sessionKey) throws Exception {
		RestResponse<Object> data = new RestResponse<Object>();
		String source = req.getParameter("source");
		ImportProcessor parser = parserCache.get(sessionKey);
		if(parser != null) {
			if(source == null || source.equals("nodes")) {
				Entity root = entityCache.get(sessionKey);
				if(root != null) {
					printNodeTaxonomy(data.getResponse().getData(), "null", root, parser); 
				}
			} else if(source.equals("node")) {
				String id = req.getParameter("id");
				if(id != null) {				
					Entity node = parser.getEntityById(id);
					if(node != null) {
						FormatInstructions instr = new FormatInstructions();
						instr.setFormat(FormatInstructions.FORMAT_JSON);
						instr.setPrintSources(true);
						instr.setPrintTargets(true);
						data.getResponse().addData(getEntityService().export(instr, node));						
					}
				}
			}
		}
		return data;
	}
	
	protected void printNodeTaxonomy(List<Object> list, String parent, Entity node, ImportProcessor parser) throws InvalidEntityException {
		Map<String,Object> entityMap = new HashMap<String,Object>();
		entityMap.put("id", node.getUuid());
		entityMap.put("name", node.getName());
		entityMap.put("parent", parent);
		for(Property property : node.getProperties()) {
			entityMap.put(property.getQName().getLocalName(), property.getValue());
		}
		if(node.getChildren().size() > 0) {				
			entityMap.put("isFolder", true);
		} else {
			entityMap.put("isFolder", false);
		}
		list.add(entityMap);
		for(Association assoc : node.getChildren()) {
			Entity child = parser.getEntityById(assoc.getTargetUid());
			printNodeTaxonomy(list, node.getUuid(), child, parser);
		}
	}
	protected void cascadeQName(Long id, QName association, QName qname) throws InvalidAssociationException, InvalidEntityException, InvalidPropertyException, ModelValidationException, InvalidQualifiedNameException {
		Entity entity = getEntityService().getEntity(id);
		for(Association assoc : entity.getSourceAssociations(association)) {
			Entity targetEntity = getEntityService().getEntity(assoc.getTarget());
			if(association.equals(RepositoryModel.CATEGORIES)) {
				assoc.setQname(RepositoryModel.ITEMS);
				getEntityService().updateAssociation(assoc);
				targetEntity.addProperty(RepositoryModel.CATEGORY_LEVEL, "item");
			}
			targetEntity.setQName(qname);
			getEntityService().updateEntity(targetEntity);
			cascadeQName(targetEntity.getId(), association, qname);
		}
	}	
	protected Map<String,Object> getNodeData(String id, String parent, String name, String qname, String localName) {
		Map<String,Object> nodeData = new HashMap<String,Object>();
		nodeData.put("id", id);
		nodeData.put("parent", parent);
		nodeData.put("name", name);
		nodeData.put("qname", qname);
		nodeData.put("localName", localName);		
		return nodeData;
	}
	
}