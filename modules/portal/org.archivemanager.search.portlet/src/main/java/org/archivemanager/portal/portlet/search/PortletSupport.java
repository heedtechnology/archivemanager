package org.archivemanager.portal.portlet.search;

import java.io.IOException;
import java.io.StringReader;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.GenericPortlet;
import javax.portlet.PortletException;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.heed.openapps.entity.EntityService;
import org.heed.openapps.security.SecurityService;
import org.heed.openapps.search.SearchService;
import org.json.JSONObject;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.liferay.portal.kernel.bean.BeanLocator;
import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;


public class PortletSupport extends GenericPortlet {
	private static Log log = LogFactoryUtil.getLog(PortletSupport.class);
	private SecurityService securityService;
	private EntityService entityService;
	private SearchService searchService;
	private static DocumentBuilderFactory factory;
	
	
	public void init() throws PortletException {
		
	}
	public void doDispatch(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		String jspPage = renderRequest.getParameter("jspPage");
		if (jspPage != null) {
			include(jspPage, renderRequest, renderResponse);
		}
		else {
			super.doDispatch(renderRequest, renderResponse);
		}
	}

	public void doEdit(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		if (renderRequest.getPreferences() == null) {
			super.doEdit(renderRequest, renderResponse);
		}
		else {
			include("", renderRequest, renderResponse);
		}
	}

	public void doHelp(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		include("", renderRequest, renderResponse);
	}
	public void processAction(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {}
	
	protected void include(String path, RenderRequest renderRequest, RenderResponse renderResponse)	throws IOException, PortletException {
		PortletRequestDispatcher portletRequestDispatcher =	getPortletContext().getRequestDispatcher(path);
		if (portletRequestDispatcher == null) {
			log.error(path + " is not a valid include");
		} else {
			portletRequestDispatcher.include(renderRequest, renderResponse);
		}
	}
	protected void includeResource(String path, ResourceRequest request, ResourceResponse response)	throws IOException, PortletException {
		PortletRequestDispatcher portletRequestDispatcher =	getPortletContext().getRequestDispatcher(path);
		if (portletRequestDispatcher == null) {
			log.error(path + " is not a valid include");
		} else {
			portletRequestDispatcher.include(request, response);
		}
	}
	public static JSONObject getJSONObjectFromString(String content) {
		try {
			return new JSONObject(content);
		} catch (Exception e) { e.printStackTrace(); }
		return null;
	}
	public static org.w3c.dom.Document getDOMDocumentFromString(String content) {
		try {
			if(factory == null) factory = DocumentBuilderFactory.newInstance();
			factory.setValidating(false);
			org.w3c.dom.Document doc = factory.newDocumentBuilder().parse(new InputSource(new StringReader(content)));
			return doc;
		} catch (SAXException e) { e.printStackTrace();
		} catch (ParserConfigurationException e) { e.printStackTrace();
		} catch (IOException e) { e.printStackTrace(); }
		return null;
	}
	public SearchService getSearchService() {
		if(searchService == null) {
			BeanLocator locator = PortletBeanLocatorUtil.getBeanLocator("archivemanager-portlet");
			searchService = (SearchService)locator.locate("searchService");
		}
		return searchService;
	}
	public EntityService getEntityService() {
		if(entityService == null) {
			BeanLocator locator = PortletBeanLocatorUtil.getBeanLocator("archivemanager-portlet");
			entityService = (EntityService)locator.locate("entityService");
		}
		return entityService;
	}
	public SecurityService getSecurityService() {
		if(securityService == null) {
			BeanLocator locator = PortletBeanLocatorUtil.getBeanLocator("archivemanager-portlet");
			securityService = (SecurityService)locator.locate("securityService");
		}
		return securityService;
	}
}
