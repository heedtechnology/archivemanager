package org.archivemanager.portal.portlet.search;

import java.io.IOException;

import javax.portlet.PortletException;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.archivemanager.model.Attribute;
import org.archivemanager.model.AttributeValue;
import org.archivemanager.model.ResultSet;

import com.liferay.portal.kernel.util.PrefsParamUtil;
import com.liferay.portal.util.PortalUtil;


public class AttributeDisplayPortlet extends PortletSupport {
	
	
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		HttpServletRequest httpReq = PortalUtil.getHttpServletRequest(renderRequest);
		HttpServletRequest httpReq2 = PortalUtil.getOriginalServletRequest(httpReq);
		
		PortletSession ps = renderRequest.getPortletSession();
		String view = PrefsParamUtil.getString(renderRequest.getPreferences(), renderRequest, "view", "");
		
		Boolean people = PrefsParamUtil.getBoolean(renderRequest.getPreferences(), renderRequest, "people", true);
		Boolean corporations = PrefsParamUtil.getBoolean(renderRequest.getPreferences(), renderRequest, "corporations", true);
		Boolean subjects = PrefsParamUtil.getBoolean(renderRequest.getPreferences(), renderRequest, "subjects", true);
		Boolean collections = PrefsParamUtil.getBoolean(renderRequest.getPreferences(), renderRequest, "collections", true);
		Boolean contentTypes = PrefsParamUtil.getBoolean(renderRequest.getPreferences(), renderRequest, "contentTypes", true);
		Integer width = PrefsParamUtil.getInteger(renderRequest.getPreferences(), renderRequest, "width", 0);
		
		String id = httpReq2.getParameter("id");
		
		ResultSet result = (ResultSet)ps.getAttribute("am_results", PortletSession.APPLICATION_SCOPE);
		if(result != null) {
			for(Attribute attribute : result.getAttributes()) {
				if(attribute.getName().equals("Collections") && !collections)
					attribute.setDisplay(false);
				if(attribute.getName().equals("Personal Entities") && !people)
					attribute.setDisplay(false);
				if(attribute.getName().equals("Corporate Entities") && !corporations)
					attribute.setDisplay(false);
				if(attribute.getName().equals("Subjects") && !subjects)
					attribute.setDisplay(false);
				if(attribute.getName().equals("Content Type") && !contentTypes)
					attribute.setDisplay(false);
				
				for(AttributeValue value : attribute.getValues()) {
					String name = value.getName();
					if(width > 0 && name.length() > width) 
						name = name.substring(0, width)+"...";
					if(name.contains("<br>")) 
						name = name.replace("<br>", "");
					value.setName(name);
				}
			}
			
			renderRequest.setAttribute("resultset", result);
			
			if(id != null && id.length() > 0) {				
				renderRequest.setAttribute("baseUrl", "?id="+id+"&");
			} else {
				renderRequest.setAttribute("baseUrl", "?");
			}
		}
		include("/jsp/attributes/"+view, renderRequest, renderResponse);
	}
	
}
