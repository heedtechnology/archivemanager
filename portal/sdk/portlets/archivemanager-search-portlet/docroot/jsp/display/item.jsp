<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>

<portlet:defineObjects />
<liferay-theme:defineObjects />

<c:if test="${result.contentType == 'video'}">
	<link href="http://vjs.zencdn.net/c/video-js.css" rel="stylesheet">
	<script src="http://vjs.zencdn.net/c/video.js"></script>
</c:if>

<div class="collection-detail">
	<div class="collection-detail-left">
		<c:if test="${result.title != null}">
			<div class="collection-detail-title">
				<c:out value="${result.title}" escapeXml="false" />
			</div>
		</c:if>
		
		<c:if test="${result.collectionName != null && displayCollectionName != 'false'}">
			<div>
				<div class="collection-detail-label" style="">Collection:</div>
				<div class="collection-detail-value">
					<a href="/collection?id=<c:out value='${result.collectionId}' />">
						<c:out value="${result.collectionName}" escapeXml="false" />
					</a>
				</div>
			</div>
		</c:if>
		
		<c:if test="${result.description != null && displayDescription != 'false'}">
			<div>
				<div class="collection-detail-label" style="">Description:</div>
				<div class="collection-detail-value">
					<c:out value="${result.description}" escapeXml="false" />
				</div>
			</div>
		</c:if>
		<c:if test="${result.summary != null && displaySummary != 'false'}">
			<div>
				<div class="collection-detail-label" style="">Summary:</div>
				<div class="collection-detail-value">
					<c:out value="${result.summary}" escapeXml="false" />
				</div>
			</div>
		</c:if>
			
		<c:if test="${missingImage != null && !(result.description == null && displayDescription != 'false') && !(result.summary != null && displaySummary != 'false')}">
			<div style="width:100%;text-align:center;padding:40px 0;">
				<img src="<c:out value="${missingImage}" />" />
			</div>
		</c:if>		
		
		<c:if test="${result.dateExpression != null && displayDateExpression != 'false'}">
			<div class="collection-detail-label" style="">Date:</div>
			<div class="collection-detail-value">
				<c:out value="${result.dateExpression}" escapeXml="false" />
			</div>
		</c:if>
		
		<c:if test="${result.container != null && displayContainer != 'false'}">
		<div>
			<div class="collection-detail-label" style="">Container:</div>
			<div class="collection-detail-value">
				<c:out value="${result.container}" escapeXml="false" />
			</div>
		</div>
		</c:if>
		
		<c:if test="${not empty result.notes}">
			<div>
				<div class="collection-detail-label">Notes:</div>
				<div class="collection-detail-value">
					<table>
						<c:forEach items="${result.notes}" var="note" varStatus="nstatus">									
							<tr style="border-bottom: 1px solid #395AC3;">
								<td style="padding-top:1.5em;padding-bottom:1.5em;">
									<span style="font-weight:bold;font-size:13px;color:#395AC3;text-decoration:underline;"><c:out value="${note.type}" /></span>
									<span class="detail-text"> - <c:out value="${note.content}" escapeXml="false" /></span>			
								</td>
							</tr>
						</c:forEach>
					</table>
				</div>
			</div>
		</c:if>
		<c:if test="${not empty result.path}">
			<div>
				<div class="collection-detail-label">Keywords:</div>
				<div class="collection-detail-value">
					<table>						
						<c:forEach items="${result.path}" var="node" varStatus="nstatus">
							<c:if test="${node.id != result.id}">
								<tr>
									<td>
										<p class="detail-text"><c:out value="${node.title}" escapeXml="false" /></p>
									</td>
								</tr>
							</c:if>
						</c:forEach>
					</table>
				</div>
			</div>
		</c:if>
	</div>
</div>
