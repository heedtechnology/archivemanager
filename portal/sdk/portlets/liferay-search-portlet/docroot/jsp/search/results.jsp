<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>

<portlet:defineObjects />
<liferay-theme:defineObjects />

<%
String header = (String)renderRequest.getAttribute("header");
if(header == null) header = "";
%>

<script type="text/javascript">
function loadPage(url) {
	$.ajax({	//create an ajax request to load_page.php
		type: "GET",
	    url: url,
	    dataType: "html",	//expect html to be returned
	    success: function(msg){
	    	if(parseInt(msg)!=0) {
	        	$('#liferay-search-results').html(msg);
	        }
	    }
	});
}
</script>

<div id="liferay-search-results">
	<table style="width:100%;">
		<tr>
			<td>
				<table id="search-table">
					<tbody>
						<c:forEach items="${resultset.results}" var="result" varStatus="rstatus">
							<tr>						
								<td class="result">
									<div class="type">
										<c:out value="${result.contentType}" />
									</div>
									<div class="title">
										<a class="detail-title" href="<c:out value="${resultset.baseUrl}" />/detail/<c:out value="${result.id}" />"><c:out value="${result.title}" escapeXml="false" /></a>
									</div>									
									<div class="description">
										<c:out value="${result.description}" />
									</div>
								</td>
							</tr>
							<tr>
								<td colspan="3">
									<div class="search-line-border"></div>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
	
				<table class="search-tools">
					<tbody><tr>
						<td></td>
						<td class="tools-right">
							<c:forEach items="${resultset.paging}" var="page" varStatus="pstatus">
								<a onclick="loadPage('<c:out value="${baseUrl}" />?<c:out value="${page.query}" />');" href="#"><c:out value="${page.name}" /></a>
							</c:forEach>	
						</td>
					</tr></tbody>
				</table>
			</td>
		</tr>
	</table>
</div>