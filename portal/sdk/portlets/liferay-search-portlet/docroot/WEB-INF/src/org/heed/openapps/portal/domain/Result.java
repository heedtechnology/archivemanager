package org.heed.openapps.portal.domain;

import org.dom4j.Element;

public class Result {
	private String id;
	private String title;
	private String description;
	private String contentType;
		
	
	public Result() {}
	public Result(Element entity) {
		try {
			if(entity.attribute("id") != null) this.id = entity.attribute("id").getText();			
			if(entity.element("name") != null) this.title = entity.element("name").getText();
			if(entity.element("description") != null) this.description = entity.element("description").getText();
			if(entity.attribute("localName") != null) this.contentType = entity.attribute("localName").getText();
			//if(entity.element("description") != null && !entity.element("description").getTextTrim().equals(this.title)) 
				//notes.add(new Note("", "Description", entity.element("description").getTextTrim()));
			
			if(title != null) title = title.replace("<br>", "");
			if(description != null) description = description.replace("<br>", "");
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
		
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}	
}
