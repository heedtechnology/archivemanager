package org.heed.openapps.portal.portlet.search;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.heed.openapps.portal.domain.Document;
import org.heed.openapps.portal.domain.Result;
import org.heed.openapps.portal.domain.ResultSet;
import org.heed.openapps.portal.portlet.PortletSupport;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.BooleanQueryFactoryUtil;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchEngineUtil;
import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.search.SortFactoryUtil;
import com.liferay.portal.kernel.util.PrefsParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.documentlibrary.service.DLAppLocalServiceUtil;
import com.liferay.portlet.dynamicdatamapping.model.DDMStructure;
import com.liferay.portlet.dynamicdatamapping.service.DDMStructureLocalServiceUtil;
import com.liferay.portlet.journal.model.JournalArticle;


public class LiferaySearchPortlet extends PortletSupport {
	private static Log log = LogFactoryUtil.getLog(LiferaySearchPortlet.class);
	private static DocumentBuilderFactory factory;
	
	
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		String structures = PrefsParamUtil.getString(renderRequest.getPreferences(), renderRequest, "structures", "");
		String fields = PrefsParamUtil.getString(renderRequest.getPreferences(), renderRequest, "fields", "");
		String view = PrefsParamUtil.getString(renderRequest.getPreferences(), renderRequest, "view", "");
		String sort = PrefsParamUtil.getString(renderRequest.getPreferences(), renderRequest, "sort", "");
		
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		
		HttpServletRequest httpReq = PortalUtil.getHttpServletRequest(renderRequest);
		HttpServletRequest httpReq2 = PortalUtil.getOriginalServletRequest(httpReq);
		String query = httpReq2.getParameter("query") != null ? httpReq2.getParameter("query") : "";
		String page = httpReq2.getParameter("page") != null ? httpReq2.getParameter("page") : "1";
		String size = httpReq2.getParameter("size") != null ? httpReq2.getParameter("size") : "10";
			
		String baseUrl = themeDisplay.getURLCurrent();
		if(baseUrl.contains("?")) baseUrl = baseUrl.substring(0, baseUrl.indexOf("?"));
		if(baseUrl.contains(";jsessionid=")) baseUrl.substring(0, baseUrl.indexOf(";jsessionid="));
			
		renderRequest.setAttribute("baseUrl", baseUrl);
		renderRequest.setAttribute("query", query);
			
		ResultSet results = new ResultSet();
		
		if(view.equals("forms.jsp")) {
			List<Document> liferayResults = getLiferayDocumentResults(themeDisplay, structures, fields, query, page, sort, size);
			if(liferayResults != null && liferayResults.size() > 0) {
				results.getResults().addAll(liferayResults);
				results.setResultCount(results.getResults().size());
			}
		} else {
			List<Result> liferayResults = getLiferayWebContentResults(themeDisplay, structures, fields, query, page, sort, size);
			if(liferayResults != null && liferayResults.size() > 0) {
				results.getResults().addAll(liferayResults);
				results.setResultCount(results.getResults().size());
			}
		}
		
		if(results != null) {
			log.info(results.getResultCount()+" result returned for query:"+query);
			renderRequest.setAttribute("resultset", results);
			PortletSession session = renderRequest.getPortletSession();
			session.setAttribute("lr_results", results, PortletSession.APPLICATION_SCOPE);
		}
		
		include("/jsp/search/"+view, renderRequest, renderResponse);
	}
	
	protected List<Document> getLiferayDocumentResults(ThemeDisplay themeDisplay, String structures, String fields, String query, String page, String sort, String size) throws PortletException {
		List<Document> results = new ArrayList<Document>();
		
		SearchContext searchContext = new SearchContext();			
		try {
			BooleanQuery searchQuery = BooleanQueryFactoryUtil.create(searchContext);				
			if(structures != null && structures.length() > 0) {				
				BooleanQuery structureQuery = BooleanQueryFactoryUtil.create(searchContext);
				String[] ids = structures.split(",");
				for(String id : ids) {
					structureQuery.addTerm("classTypeId", id);
				}
				searchQuery.add(structureQuery, BooleanClauseOccur.MUST);
				
			}
			if(query != null && query.length() > 0) {
				BooleanQuery queryQuery = BooleanQueryFactoryUtil.create(searchContext);
				if(fields != null) {
					String[] fieldNames = fields.split(",");
					for(String field : fieldNames) {
						queryQuery.addTerm(field, query);
					}
				} else queryQuery.addTerm("content", query);
				searchQuery.add(queryQuery, BooleanClauseOccur.MUST);
			}
			searchQuery.addRequiredTerm(Field.STATUS, WorkflowConstants.STATUS_APPROVED);
						    
		    Sort[] querySorts = { SortFactoryUtil.getSort(JournalArticle.class, "expando/custom_fields/Order", "desc") };
			searchContext.setSorts(querySorts);
				
			log.info(searchQuery.toString());
			
		    Hits hits = SearchEngineUtil.search(searchContext.getSearchEngineId(), themeDisplay.getCompanyId(), searchQuery, querySorts, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
			
		    com.liferay.portal.kernel.search.Document[] docs = hits.getDocs();
			if (docs != null) {
				for(int i = 0; i < docs.length; i++) {					
					com.liferay.portal.kernel.search.Document doc = docs[i];
					String entryClassPK = doc.get("entryClassPK");
					FileEntry entry = DLAppLocalServiceUtil.getFileEntry(Long.valueOf(entryClassPK));
					
					Document result = new Document(entry);					
					String fileUrl = themeDisplay.getPortalURL()+"/c/document_library/get_file?uuid="+entry.getUuid()+"&groupId="+themeDisplay.getScopeGroupId();
					result.setUrl(fileUrl);
					
					results.add(result);
				}
			}
		} catch(Exception e) {
			throw new PortletException(e);
		}
		
		return results;
	}
	protected List<Result> getLiferayWebContentResults(ThemeDisplay themeDisplay, String structures, String fields, String query, String page, String sort, String size) throws PortletException {
		List<Result> results = new ArrayList<Result>();
		Map<String,String> contentTypes = new HashMap<String,String>();
		
		SearchContext searchContext = new SearchContext();			
		try {
			BooleanQuery searchQuery = BooleanQueryFactoryUtil.create(searchContext);				
			if(structures != null && structures.length() > 0) {				
				BooleanQuery structureQuery = BooleanQueryFactoryUtil.create(searchContext);
				String[] ids = structures.split(",");
				for(String id : ids) {
					structureQuery.addTerm("classTypeId", id);
					DDMStructure structure = DDMStructureLocalServiceUtil.getDDMStructure(Long.valueOf(id));
					if(structure != null) {
						contentTypes.put(id, structure.getName(themeDisplay.getLocale()));
					}
				}
				searchQuery.add(structureQuery, BooleanClauseOccur.MUST);
				
			}
			if(query != null) {
				BooleanQuery queryQuery = BooleanQueryFactoryUtil.create(searchContext);
				if(fields != null) {
					String[] fieldNames = fields.split(",");
					for(String field : fieldNames) {
						queryQuery.addTerm(field, query);
					}
				} else queryQuery.addTerm("content", query);
				searchQuery.add(queryQuery, BooleanClauseOccur.MUST);
			}
			
			searchQuery.addRequiredTerm(Field.STATUS, WorkflowConstants.STATUS_APPROVED);
			searchQuery.addRequiredTerm("head", "true");
			    
		    Sort[] querySorts = { SortFactoryUtil.getSort(JournalArticle.class, "expando/custom_fields/Publish_Date", "desc") };
			searchContext.setSorts(querySorts);
				
			log.info(searchQuery.toString());
			
		    Hits hits = SearchEngineUtil.search(searchContext.getSearchEngineId(), themeDisplay.getCompanyId(), searchQuery, querySorts, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
			
		    com.liferay.portal.kernel.search.Document[] docs = hits.getDocs();
			if (docs != null) {
				log.debug("Docs: " + docs.length);
				for(int i = 0; i < docs.length; i++) {
					Result result = new Result();
					com.liferay.portal.kernel.search.Document doc = docs[i];
					
					String structureKey = doc.get("classTypeId");
					String structureTitle = contentTypes.get(structureKey);
					result.setContentType(structureTitle);					
					
					result.setTitle(doc.get("title"));
					result.setDescription(doc.get("ddm/"+structureKey+"/Description_en_US"));
					
					results.add(result);
				}
			}
		} catch(Exception e) {
			throw new PortletException(e);
		}
				
	    return results;
	}

	public void init() throws PortletException {
		
	}
	public void doDispatch(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		String jspPage = renderRequest.getParameter("jspPage");
		if (jspPage != null) {
			include(jspPage, renderRequest, renderResponse);
		}
		else {
			super.doDispatch(renderRequest, renderResponse);
		}
	}

	public void doEdit(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		if (renderRequest.getPreferences() == null) {
			super.doEdit(renderRequest, renderResponse);
		}
		else {
			include("", renderRequest, renderResponse);
		}
	}

	public void doHelp(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		include("", renderRequest, renderResponse);
	}
	public void processAction(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException {}
	
	protected void include(String path, RenderRequest renderRequest, RenderResponse renderResponse)	throws IOException, PortletException {
		PortletRequestDispatcher portletRequestDispatcher =	getPortletContext().getRequestDispatcher(path);
		if (portletRequestDispatcher == null) {
			log.error(path + " is not a valid include");
		} else {
			portletRequestDispatcher.include(renderRequest, renderResponse);
		}
	}
	protected void includeResource(String path, ResourceRequest request, ResourceResponse response)	throws IOException, PortletException {
		PortletRequestDispatcher portletRequestDispatcher =	getPortletContext().getRequestDispatcher(path);
		if (portletRequestDispatcher == null) {
			log.error(path + " is not a valid include");
		} else {
			portletRequestDispatcher.include(request, response);
		}
	}
	public static org.w3c.dom.Document getDOMDocumentFromURI(String uri) {
		try {
			if(factory == null) factory = DocumentBuilderFactory.newInstance();
			factory.setValidating(false);
			org.w3c.dom.Document doc = factory.newDocumentBuilder().parse(uri);
			return doc;
		} catch (SAXException e) { e.printStackTrace();
		} catch (ParserConfigurationException e) { e.printStackTrace();
		} catch (IOException e) { e.printStackTrace(); }
		return null;
	}
	public static org.w3c.dom.Document getDOMDocumentFromString(String content) {
		try {
			if(factory == null) factory = DocumentBuilderFactory.newInstance();
			factory.setValidating(false);
			org.w3c.dom.Document doc = factory.newDocumentBuilder().parse(new InputSource(new StringReader(content)));
			return doc;
		} catch (SAXException e) { e.printStackTrace();
		} catch (ParserConfigurationException e) { e.printStackTrace();
		} catch (IOException e) { e.printStackTrace(); }
		return null;
	}
	public static String httpGet(String url) throws ClientProtocolException, IOException {		
		String result = "";	
		HttpClient httpclient = new DefaultHttpClient();
		ResponseHandler<String> responseHandler = new BasicResponseHandler();
		HttpGet httpget = new HttpGet(url);
		try {	
			HttpParams httpParameters = httpclient.getParams();
			HttpConnectionParams.setConnectionTimeout(httpParameters, 10000);
			HttpConnectionParams.setSoTimeout        (httpParameters, 10000);
			System.out.println("executing request " + httpget.getURI());

			result = httpclient.execute(httpget, responseHandler);
			//HttpResponse response = httpclient.execute(httpget);
			//result = IOUtil.convertStreamToString(response.getEntity().getContent());
			
			//LOG.info("----------------------------------------");
			//LOG.info(result);
			//LOG.info("----------------------------------------");
		} catch(HttpResponseException e) {
			int status = e.getStatusCode();
			String msg = e.getMessage();
			log.error("status:"+status+" - "+msg);
		} finally {
			httpclient.getConnectionManager().shutdown();
		}	
		return result;
	}
}
